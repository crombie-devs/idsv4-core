<?php
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use \App\Console\Commands\AutomatizacionCalculo;
use \App\Console\Commands\UpdateProductsData;
use App\Console\Commands\SitesByCustomer;
use App\Console\Commands\CamerasByCustomer;
use App\Console\Commands\ArchiveExpiredAssets;

use \App\Console\Commands\BuildPlayList;
use \App\Console\Commands\BuildExternalPlayList;
use \App\Console\Commands\GenerateAlertSupport;
use \App\Console\Commands\RecomendationsTrendsLoader;
use \App\Console\Commands\UpdateHasSocioeconomic;
use \App\Console\Commands\UpdateDates;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('automatizacion:calculo', function () {
    $this->comment(AutomatizacionCalculo::autoGenData());
})->describe('genera Json para calculo');

Artisan::command('automatizacion:filesintegration {idcustomer}', function ($idcustomer) {
    $this->comment(AutomatizacionCalculo::generateFilesintegration($idcustomer));
})->describe('genera Json para calculo');

Artisan::command('automatizacion:resumen {ejercicio} {month} {idcustomer}', function ($ejercicio, $month, $idcustomer) {
    $this->comment(AutomatizacionCalculo::saveResumen($ejercicio, $month, $idcustomer));
})->describe('genera Json para calculo');

Artisan::command('automatizaciondaily:resumencontent', function () {
    $this->comment(AutomatizacionCalculo::autoGenDataDaily());
})->describe('add contenido diario al Json para calculo');


Artisan::command('automatizacion:calculonum {num}', function ($num) {
    $auto = new AutomatizacionCalculo();
    $this->comment($auto->autoGenDataNum($num));
})->describe('Genera json con data para el calculo');

Artisan::command('updatedata:products', function () {
    $auto = new UpdateProductsData();
    $this->comment($auto->updateProducts());
})->describe('Actualiza los datos de la tabla "products"');

Artisan::command('updatedata:categories', function () {
    $auto = new UpdateProductsData();
    $this->comment($auto->updateProductCategories());
})->describe('Actualiza los datos de la tabla "product_categories"');

Artisan::command('updatedata:contents', function () {
    $auto = new UpdateProductsData();
    $this->comment($auto->updateContentsProducts());
})->describe('Actualiza los datos de la tabla "contents_has_products"');

Artisan::command('updatedata:tags', function () {
    $auto = new UpdateProductsData();
    $this->comment($auto->updateProductsTags());
})->describe('Actualiza los datos de la tabla "`products_has_tags"');

Artisan::command('updatedata:categories', function () {
    $auto = new UpdateProductsData();
    $this->comment($auto->updateProductCategory());
})->describe('Actualiza las categorías de los productos "products"');

Artisan::command('contents:generate', function () {
    $gen = new BuildPlayList();
    $date = Carbon::today();
    $this->comment("Número de Ficheros con errores: ".$gen->getContents($date, true));
})->describe('Genera json de los contenidos');

Artisan::command('contents:generate:tomorrow', function () {
    $gen = new BuildPlayList();
    $date = Carbon::tomorrow();
    $this->comment("Número de Ficheros con errores: ".$gen->getContents($date, true));
})->describe('Genera json de los contenidos');

Artisan::command('contents:generate:leonisa', function () {
    $gen = new BuildPlayList();
    $this->comment($gen->getContentsByCustomerLeonisa(75));
})->describe('Genera json de los contenidos');

Artisan::command('circuits:generate', function () {
    $gen = new BuildPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getCircuits(true));
})->describe('Genera json de los circuitos');

Artisan::command('players:generate', function () {
    $gen = new BuildPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getPlayers(true));
})->describe('Genera json de los player');

Artisan::command('playlogics:generate', function () {
    $gen = new BuildPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getPlayLogics(true));
})->describe('Genera json de los playlogics');

Artisan::command('otis:generate:playlist', function () {
    // OTIS: idcustomer: 55
    $gen = new BuildExternalPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getContentsByCustomer(55));
})->describe('Genera playlist de OTIS');

Artisan::command('otis:generate:players', function () {
    // OTIS: idcustomer: 55
    $gen = new BuildExternalPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getPlayersByCustomer(55));
})->describe('Genera players de OTIS');

Artisan::command('sonae:generate:playlist', function () {
    // OTIS: idcustomer: 55
    $gen = new BuildExternalPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getContentsByCustomer(128));
})->describe('Genera playlist de OTIS');

Artisan::command('sonae:generate:players', function () {
    // OTIS: idcustomer: 55
    $gen = new BuildExternalPlayList();
    $this->comment("Número de Ficheros con errores: ".$gen->getPlayersByCustomer(128));
})->describe('Genera players de OTIS');

Artisan::command('alertsupport:generate', function () {
    $gen = new GenerateAlertSupport();
    $this->comment($gen->getAlertSupportPlayer());
})->describe('Genera alertas de support');

Artisan::command('alertcameras:generate', function () {
    $gen = new GenerateAlertSupport();
    $this->comment($gen->getAlertSupportCamera());
})->describe('Genera alertas de support');

Artisan::command('recomendations-trends:load', function () {
    $trends = new RecomendationsTrendsLoader();
    $this->comment($trends->loadTrends());
})->describe('Carga datos para tendencias de recomendaciones');

Artisan::command('update:hassocioeconomic', function () {
    $gen= new UpdateHasSocioeconomic();
    $this->comment($gen->load());
})->describe('Actualiza el campo has_socioeconomic de la tabla site atendiendo si hay datos en las tablas profile-demografic y profile_economic ');

Artisan::command('update:dates', function () {
    $gen= new UpdateDates();
    $this->comment($gen->load());
})->describe('Actualiza las fechas de los contenidos para permitir varias ');

Artisan::command('generate:sitesbycustomer', function () {
    $gen= new SitesByCustomer();
    $this->comment($gen->generatedata());
})->describe('Crea json de los sites por customer');

Artisan::command('generate:camerasbycustomer', function () {
    $gen= new CamerasByCustomer();
    $this->comment($gen->generatedata());
})->describe('Crea json de las cámaras por customer');

Artisan::command('archive:expiredassets', function () {
    $gen = new ArchiveExpiredAssets();
    $this->comment($gen->archiveExpiredAssets());
})->describe('Archiva los assets expirados');
