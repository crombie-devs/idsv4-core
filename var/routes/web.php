<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('https://ladorianids.com/password-reset')->name('password.reset');

Route::get('/generate-api-doc', function() {
    Artisan::call('apidoc:generate');
    return "La documentación del api se ha generado correctamente.";
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Se ha limpiado la cache de servidor.";
});
