<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
    Route::group(['prefix' => 'protected', 'namespace' => 'ws', 'middleware' => ['ws']], function () { //
        /**
         * Auth de player para recuperar el detalle del player
         */
        Route::get('player/findByCode', 'WSPlayerController@findByCode')->middleware('ws');
        Route::get('player/auth', 'WSPlayerController@auth')->middleware('ws');
        Route::get('player/playlogics', 'WSPlayerController@playlogics')->middleware('ws');
        Route::get('player/contents', 'WSPlayerController@contents')->middleware('ws');
        Route::get('camera/auth', 'WSCameraController@auth')->middleware('ws');
        Route::get('customer/circuits', 'WSCustomerController@getCircuit')->middleware('ws');
        Route::get('getTokenGenerate', 'WSPlayerController@getTokenGenerate');
        Route::get('tags/events/{idplayer}', 'WSPlayerEventsTagController@tagsPlayerEvent')->middleware(('ws'));

        Route::post('player/{idplayer}/emissions', 'WSPlayerController@saveEmissions');
        /**
         * Sistema de notificaciones desde aplicaciones externas
         */
        Route::post('notificate', "WSNotificationsController@notificate");

        Route::post('difendif', "WSDifendifController@list")->middleware('ws');
        Route::post('difendif/add', "WSDifendifController@add")->middleware('ws');
        Route::post('difendif/del', "WSDifendifController@delete")->middleware('ws');

        Route::get('generarplaylist/customer/{idcustomer}', 'BuildPlaylistController@generatePlaylistByCustomer')->middleware('ws');
        Route::get('sendNotificacion/customer/{idcustomer}', 'BuildPlaylistController@sendNotificacionByCustomer')->middleware('ws');

        /**
         * Sistema audios
         */

        Route::get('customer/audios/categories/{idcustomer}', 'WSAudiosController@getAudiosCategoriesCustomer')->middleware('ws');
        Route::get('customer/audios/{idcustomer}', 'WSAudiosController@getAudiosCustomer')->middleware('ws');

        /**
         * Llamadas por consola
         */
        Route::get('command/check/players/failures', function () {
            Artisan::call('player:failure');
            return "La accion se realizo con exito";
        });
        Route::get('command/build/players/actives', function () {
            Artisan::call('generate:players-json');
            return "Se han generado un json con los players";
        });
        Route::get('command/build/sites/bycustomer', function () {
            Artisan::call('generate:sitesbycustomer');
            return "Se han generado las playlists";
        });
        Route::get('command/build/cameras/bycustomer', function () {
            Artisan::call('generate:camerasbycustomer');
            return "Se han generado las playlists";
        });
        Route::get('command/build/cameras/actives', function () {
            Artisan::call('generate:cameras-actives-json');
            return "Se han generado un json con las camaras activas";
        });
    });

    Route::group(['prefix' => 'protected', 'namespace' => 'ws'], function () { //
        /**
         * Auth de player para recuperar el detalle del player de otis
         */
        Route::post('otis/auth', 'OtisController@auth')->middleware('otis');
        Route::post('otis/update-devices', 'OtisController@updateDevices')->middleware('otis');
        Route::post('otis/check-devices', 'OtisController@checkDevices')->middleware('otis');
        Route::get('otis/generarplaylist', 'BuildPlaylistController@generateContentsByOtis')->middleware('otis');
        Route::get('otis/generarplayers', 'BuildPlaylistController@generatePlayersByOtis')->middleware('otis');
    });

    Route::group(['prefix' => 'protected', 'namespace' => 'support'], function () {

        /**
         * Support
         */
        Route::get('player/getplayer', 'SupportPlayerController@getPlayer')->middleware('ws');
        Route::get('player/getplayerbysite/{idsite}', 'SupportPlayerController@getPlayerBySite')->middleware('ws');
        Route::get('player/getplayerbycustomer/{idcustomer}', 'SupportPlayerController@getPlayerByCustomer')->middleware('ws');
        Route::get('player/totalplayergroupbycustomer', 'SupportPlayerController@totalPlayerGroupByCustomer')->middleware('ws');
        Route::get('player/totalplayergroupbysite/{idcustomer}', 'SupportPlayerController@totalPlayerGroupBySite')->middleware('ws');

        Route::get('camera/getcamera', 'SupportCameraController@getCamera')->middleware('ws');
        Route::get('camera/getcamerabysite/{idsite}', 'SupportCameraController@getCameraBySite')->middleware('ws');
        Route::get('camera/getcamerabycustomer/{idcustomer}', 'SupportCameraController@getCameraByCustomer')->middleware('ws');
        Route::get('camera/totalcameragroupbycustomer', 'SupportCameraController@totalCameraGroupByCustomer')->middleware('ws');
        Route::get('camera/totalcameragroupbysite/{idcustomer}', 'SupportCameraController@totalCameraGroupBySite')->middleware('ws');
    });

    Route::group(['prefix' => 'public'], function () {
        Route::post('login', 'UserController@login');
        Route::post('ticket/support', 'TicketController@support');
        Route::post('messagebird', 'MessageBirdController@status');
        Route::post('support/login', 'SupportController@login');
        /**
         * Llamadas para recuperar contraseña
         */
        Route::post('remember', 'PasswordController@remember');
        Route::put('update', 'PasswordController@update');
        /**
         * Lamada para testar el servicio
         */
        Route::get('test', function(){
            return true;
        });
    });



    Route::group(['prefix' => 'private'], function () {

        Route::group(['middleware' => ['auth:api'], 'namespace' => 'ws'], function () {



            Route::get('generateplaylist/customer/{idcustomer}', 'BuildPlaylistController@generatePlaylistByCustomer');
            Route::get('generateplaylist/site/{idsite}', 'BuildPlaylistController@generatePlaylistBySite');
            Route::get('generateplaylist/player/{idplayer}', 'BuildPlaylistController@generatePlaylistByPlayer');
            Route::get('generateplaylist', 'BuildPlaylistController@generatePlaylist');

            Route::get('generatecontents/customer/{idcustomer}', 'BuildPlaylistController@generateContentsByCustomer');
            Route::get('generatecontents/site/{idsite}', 'BuildPlaylistController@generateContentsBySite');
            Route::get('generatecircuits/customer/{idcustomer}', 'BuildPlaylistController@generateCircuitsByCustomer');
            Route::get('generatecircuits/site/{idsite}', 'BuildPlaylistController@generateCircuitsBySite');
            Route::get('generateplayers/customer/{idcustomer}', 'BuildPlaylistController@generatePlayersByCustomer');
            Route::get('generateplayers/site/{idsite}', 'BuildPlaylistController@generatePlayersBySite');
            Route::get('generateplayers/player/{idplayer}', 'BuildPlaylistController@generatePlayersByPlayer');
            Route::get('generateplaylogics/customer/{idcustomer}', 'BuildPlaylistController@generatePlayLogicsByCustomer');
            Route::get('generateplaylogics/site/{idsite}', 'BuildPlaylistController@generatePlayLogicsBySite');
            Route::get('generateplaylogics/player/{idplayer}', 'BuildPlaylistController@generatePlayLogicsByPlayer');
            Route::get('generateplaylogics/area/{idarea}', 'BuildPlaylistController@generatePlayLogicsByArea');

            Route::get('sendNotificacion/player/{idplayer}', 'BuildPlaylistController@sendNotificacionByPlayer');
            Route::get('sendNotificacion/refresh/site/{idsite}', 'BuildPlaylistController@sendNotificacionRefreshBySite');
            Route::get('sendNotificacion/update/site/{idsite}', 'BuildPlaylistController@sendNotificacionUpdateBySite');
            Route::get('sendNotificacion/area/{idarea}', 'BuildPlaylistController@sendNotificacionByArea');
            Route::get('sendNotificacion/refresh/customer/{idcustomer}', 'BuildPlaylistController@sendNotificacionRefreshByCustomer');
            Route::get('sendNotificacion/update/customer/{idcustomer}', 'BuildPlaylistController@sendNotificacionUpdateByCustomer');

            Route::get('sendalertemailplaylist/{idcontent}', 'BuildPlaylistController@sendMail');
            // Audios 
            Route::get('generaraudiolist/customer/{idcustomer}', 'BuildAudioController@generateAudiolistByCustomer');
        });

        Route::group(['middleware' => ['auth:api']], function () {
            Route::group(['namespace' => 'ws'], function () {
                Route::put('send/push', 'WSNotificationsController@sendPush');
            });
        });




        Route::group(['middleware' => ['auth:api']], function () {

            Route::get('authenticated', 'SupportController@authenticated');
            Route::get('tags/bytype/{type}', 'TagController@byType');

            Route::get('logs', 'DashboardMessageController@list');
            Route::post('logs', 'DashboardMessageController@create');
            Route::get('logs/{id}', 'DashboardMessageController@get');

            Route::get('dashboard-messages', 'DashboardMessageController@list');
            Route::get('dashboard-messages/latest', 'DashboardMessageController@latest');
            Route::get('dashboard-messages/{id}', 'DashboardMessageController@get');

            // Route::get('tickets', 'TicketController@list');
            Route::get('tickets', 'TicketController@getAll');
            Route::get('tickets/groupby', 'TicketController@groupBy');
            Route::get('ticket/{id}', 'TicketController@getTicket');
            Route::get('tickets/toselect', 'TicketController@toselect');
            Route::post('ticket', 'TicketController@create');
            Route::put('ticket', 'TicketController@update');
            Route::put('ticket/delete', 'TicketController@delete');
            Route::delete('ticket/delete-images/{id}', 'TicketController@deleteImages');
            Route::get('tickets/category', 'TicketController@category');
            Route::post('ticket/category', 'TicketController@createCategory');
            Route::put('ticket/category', 'TicketController@updateCategory');
            Route::put('ticket/category/delete', 'TicketController@deleteCategory');
            Route::get('tickets/priority', 'TicketController@priority');
            Route::post('ticket/priority', 'TicketController@createPriority');
            Route::put('ticket/priority', 'TicketController@updatePriority');
            Route::put('ticket/priority/delete', 'TicketController@deletePriority');
            Route::get('tickets/status', 'TicketController@status');
            Route::post('ticket/status', 'TicketController@createStatus');
            Route::put('ticket/status', 'TicketController@updateStatus');
            Route::put('ticket/status/delete', 'TicketController@deleteStatus');
            Route::get('tickets/type/{type}', 'TicketController@byType');
            Route::post('ticket/comment', 'TicketController@createComment');
            Route::put('ticket/comment', 'TicketController@updateComment');
            Route::delete('ticket/comment/{id}', 'TicketController@deleteComment');
            Route::get('ticket/comments/{id}', 'TicketController@getComments');
            Route::get('ticket/close/{id}', 'TicketController@close');
            Route::get('ticket/customer/{id}', 'TicketController@byCustomer');

            Route::get('messages/{id}', 'MessageController@get');
            Route::get('messages', 'MessageController@toSelect');
            Route::post('messages', 'MessageController@create');
            Route::put('messages', 'MessageController@update');
            Route::get('messages/formats', 'MessageController@getFormats');
            Route::post('messages/templates', 'MessageController@createTemplate');
            Route::put('messages/templates', 'MessageController@updateTemplate');
            Route::get('messages/templates/list', 'MessageController@getTemplates');
            Route::delete('messages/templates/delete/{id}', 'MessageController@deleteTemplates');
            Route::delete('messages/msg/{id}', 'MessageController@deleteMsg');
            Route::get('messages/bycustomer/{id}', 'MessageController@messageByCustomer');


            Route::get('people/{id}', 'PeopleController@get');
            Route::get('peoples/toselect', 'PeopleController@toSelect');
            Route::post('peoples', 'PeopleController@create');
            Route::put('peoples', 'PeopleController@update');
            Route::put('peoples/delete', 'PeopleController@delete');
            Route::post('people/csv', 'PeopleController@uploadCSV');

            Route::put('department', 'DepartmentController@update');
            Route::post('department', 'DepartmentController@create');
            Route::put('department/delete', 'DepartmentController@delete');
            Route::get('departments', 'DepartmentController@departments');

            Route::put('responsible', 'ResponsibleController@update');
            Route::post('responsible', 'ResponsibleController@create');
            Route::put('responsible/delete', 'ResponsibleController@delete');
            Route::get('responsibles', 'ResponsibleController@responsibles');

            Route::put('/contents/delete', 'ContentController@contentsDelete');
            Route::put('/customer/delete', 'CustomerController@customerDelete');

            Route::post('audiocategory', 'AudioCategoryController@create');
            Route::get('audiocategory/toselect', 'AudioCategoryController@toSelect');
            Route::get('audiocategory/list/{id}', 'AudioCategoryController@list');
            Route::get('audiocategory/{id}', 'AudioCategoryController@audioCategory');

            Route::delete('audiocategory/{id}', 'AudioCategoryController@delete');
            Route::put('audiocategory', 'AudioCategoryController@update');

            Route::post('audio', 'AudioController@create');
            Route::get('audio/toselect/{id}', 'AudioController@toSelect');
            Route::get('audio/list', 'AudioController@list');
            Route::get('audio/toselect', 'AudioController@toSelectAll');

            Route::delete('audio/{id}', 'AudioController@delete');
            Route::put('audio', 'AudioController@update');

            Route::post('logout', 'UserController@logout');
            Route::get('me', 'UserController@me');

            Route::post('dynamic-events', 'DynamicEventsController@create');
            Route::put('dynamic-events', 'DynamicEventsController@update');
            Route::get('dynamic-events', 'DynamicEventsController@list');
            Route::get('dynamic-events/{id}', 'DynamicEventsController@get');
            Route::delete('dynamic-events/{id}', 'DynamicEventsController@delete');
            Route::get('dynamic-events/toselect', 'DynamicEventsController@toselect');
            Route::get('dynamic-events/byplayer/{idcustomer}/{idsite}/{idplayer}', 'DynamicEventsController@byplayer');

            /**
             * Llamadas para integración de estadísticas
             */
            Route::post('impact/resumen', 'ImpactController@summary');
            Route::post('emission-sales/data', 'EmissionSaleController@data');
            Route::post('recomendations/update', 'RecomendationsController@update');
            Route::post('recomendations/upload/xlsx/{idcustomer}', 'RecomendationsController@uploadXlsx');
            Route::get('recomendations/associations/categories', 'RecomendationsController@categories');
            Route::get('recomendations/associations/conditional-probability/{idcategory}', 'RecomendationsController@probabilidadCondicionada');
            Route::get('recomendations/recomendations/categories/{month}', 'RecomendationsController@recomendationsCategories');
            Route::get('recomendations/recomendations/trends-categories', 'RecomendationsController@recomendationsTrendsCategories');
            Route::post('recomendations/recomendations/get-patron-ventas', 'RecomendationsController@getPatronVentas');
            Route::get('recomendations/recomendations/get-trends-provinces', 'RecomendationsController@getTrendsProvinces');
            Route::post('recomendations/recomendations/get-filas-dias-semana', 'RecomendationsController@getFilasDiasSemana');
            Route::post('recomendations/recomendations/get-temperature', 'RecomendationsController@getTemperature');
            Route::post('recomendations/recomendations/get-products-from-category', 'RecomendationsController@getProductsFromCategory');
            Route::post('recomendations/recomendations/get-social-demographic-data', 'RecomendationsController@getSocialDemographic');
            Route::get('recomendations/recomendations/get-provinces-from-customer', 'RecomendationsController@getProvinces');
            Route::post('recomendations/recomendations/get-sites-from-provinces', 'RecomendationsController@getSitesFromProvinces');
            Route::post('recomendations/recomendations/get-ratius-from-sites', 'RecomendationsController@getRatius');
            Route::post('recomendations/recomendations/get-social-economic-admin-data', 'RecomendationsController@getSocialEconomicAdminData');
            Route::post('recomendations/recomendations/get-segmentation-admin-data', 'RecomendationsController@getSegmentationAdminData');
            Route::get('recomendations/recomendations/get-age-vars', 'RecomendationsController@getAgeVars');
            Route::post('recomendations/recomendations/get-income-ranges', 'RecomendationsController@getIncomeRanges');
            Route::post('recomendations/recomendations/get-demographic-segmentation', 'RecomendationsController@getDemographicSegmentation');
            Route::post('recomendations/recomendations/get-economic-segmentation', 'RecomendationsController@getEconomicSegmentation');
            Route::post('algorithm/primetime', 'AlgorithmController@primetime');
            Route::post('algorithm/associations', 'AlgorithmController@associations');
            Route::post('algorithm/socioeconomico', 'AlgorithmController@socioeconomico');
            Route::post('translation/{lang}/save', 'TranslationController@save');
            /**
             * Llamada para coger los contenidos creado en el mes
             */
            Route::get('contents-by-month/{month}', 'ContentController@contentsByMonth');

            /**
             * Llamadas para productos
             */
            Route::get('categories/products/contents', 'ProductController@getCategoriesWithProductsWithContents');

        });

        Route::group(['middleware' => ['auth:api', 'authorized']], function () {
            /**
             * Llamadas para acciones con los usuarios
             */
            Route::post('user', 'UserController@register');
            Route::put('user/{iduser}', 'UserController@update');
            Route::delete('user/{iduser}', 'UserController@delete');
            Route::get('user/{iduser}', 'UserController@user');
            Route::get('users', 'UserController@users');
            Route::post('user/add-permission/', 'UserController@addPermission');
            Route::post('user/revoke-permission/', 'UserController@revokePermission');

            /**
             * Llamadas para acciones con los roles
             */
            Route::post('role', 'RoleController@create');
            Route::put('role/{idrole}', 'RoleController@update');
            Route::delete('role/{idrole}', 'RoleController@delete');
            Route::get('role/{idrole}', 'RoleController@role');
            Route::get('roles', 'RoleController@roles');
            /**
             * Llamadas para acciones con los permisos
             */
            Route::post('permission', 'PermissionController@create');
            Route::put('permission/{idpermission}', 'PermissionController@update');
            Route::delete('permission/{idpermission}', 'PermissionController@delete');
            Route::get('permission/{idpermission}', 'PermissionController@permission');
            Route::get('permissions', 'PermissionController@permissions');
            /**
             * Llamadas para acciones con los clientes
             */
            Route::post('customer', 'CustomerController@create');
            Route::put('customer/{idcustomer}', 'CustomerController@update');
            Route::delete('customer/{idcustomer}', 'CustomerController@delete');
            Route::get('customer/{idcustomer}', 'CustomerController@customer');
            Route::get('customers', 'CustomerController@customers');
            Route::get('customers/toselect', 'CustomerController@toSelect');
            Route::delete('customer/{idcustomer}/format/{idformat}', 'CustomerController@deleteCustomerFormat');
            Route::put('/customer/{idcustomer}/formats', 'CustomerController@updateFormats');
            Route::get('customer/status/{id}', 'CustomerController@customerStatus');
            /**
             * Llamadas para acciones con las tiendas
             */
            Route::post('site', 'SiteController@create');
            Route::put('site/{idsite}', 'SiteController@update');
            Route::delete('site/{idsite}', 'SiteController@delete');
            Route::get('sites', 'SiteController@sites');
            Route::get('site/{idsite}', 'SiteController@site');
            Route::get('sites/toselect', 'SiteController@toSelect');
            Route::put('/site/{idsite}/holidays', 'SiteController@updateHolidays');
            Route::delete('/site/{idsite}/holiday/{idholiday}', 'SiteController@deleteHoliday');
            Route::put('/site/{idsite}/tags', 'SiteController@updateTags');
            Route::delete('/site/{idsite}/tag/{idtag}', 'SiteController@deleteTag');
            Route::get('site/status/{id}', 'SiteController@siteStatus');

            /**
             * Llamadas para acciones con las aplicaciones
             */
            Route::post('app', 'AppController@create');
            Route::put('app/{idapp}', 'AppController@update');
            Route::delete('app/{idapp}', 'AppController@delete');
            Route::get('app/{idapp}', 'AppController@app');
            Route::get('apps', 'AppController@apps');
            Route::get('apps/toselect', 'AppController@appsToSelect');

            /**
             * Llamadas para acciones con las templates
             */
            Route::post('template', 'TemplateController@create');
            Route::put('template/{idtemplate}', 'TemplateController@update');
            Route::delete('template/{idtemplate}', 'TemplateController@delete');
            Route::get('template/{idtemplatep}', 'TemplateController@template');
            Route::get('templates', 'TemplateController@templates');

            /**
             * Llamadas para acciones con las Categorías de tag
             * TODO: Unir a Tag
             */
            Route::post('tagcategory', 'TagCategoryController@create');
            Route::put('tagcategory/{idcategory}', 'TagCategoryController@update');
            Route::delete('tagcategory/{idcategory}', 'TagCategoryController@delete');
            Route::get('tagcategory/{idcategory}', 'TagCategoryController@tagCategory');
            Route::get('tagcategories', 'TagCategoryController@tagCategories');
            Route::get('tagcategories/toselect', 'TagCategoryController@toSelect');

            /**
             * Llamadas para acciones con los tags
             */
            Route::post('tag', 'TagController@create');
            Route::put('tag/{idtag}', 'TagController@update');
            Route::delete('tag/{idtag}', 'TagController@delete');
            Route::get('tag/{idtag}', 'TagController@tag');
            Route::get('tags', 'TagController@tags');
            Route::get('tags/toselect', 'TagController@toSelect');

            /**
             * Llamada para acciones con players
             */
            Route::post('player', 'PlayerController@create');
            Route::put('player/{idplayer}', 'PlayerController@update');
            Route::delete('player/{idplayer}', 'PlayerController@delete');
            Route::get('player/{idplayer}', 'PlayerController@player');
            Route::get('players', 'PlayerController@players');
            Route::post('players/toanalytics', 'PlayerController@toAnalytics');
            Route::get('players/toselect', 'PlayerController@toSelect');
            Route::put('/player/{idplayer}/playlogics', 'PlayerController@updatePlaylogics');
            Route::delete('/players/{idplayer}/playlogic/{idlogic}', 'PlayerController@deletePlaylogic');
            Route::put('/player/{idplayer}/contents/{idcategory}', 'PlayerController@updateContentsOrder');
            Route::put('/player/{idplayer}/tags', 'PlayerController@updateTags');
            Route::delete('/player/{idplayer}/tag/{idtag}', 'PlayerController@deleteTag');
            Route::put('/player/{idplayer}/powers', 'PlayerController@updatePowers');
            Route::delete('/player/{idplayer}/power/{idpower}', 'PlayerController@deletePower');
            Route::get('player/status/{id}', 'PlayerController@playerStatus');

            /**
             * Llamada para acciones con cameras
             */
            Route::post('camera', 'CameraController@create');
            Route::put('camera/{idcamera}', 'CameraController@update');
            Route::delete('camera/{idcamera}', 'CameraController@delete');
            Route::get('camera/{idcamera}', 'CameraController@camera');
            Route::get('cameras', 'CameraController@cameras');
            Route::post('cameras/toanalytics', 'CameraController@toAnalytics');
            Route::get('cameras/toselect', 'CameraController@toSelect');
            Route::put('/camera/{idcamera}/powers', 'CameraController@updatePowers');
            Route::delete('/camera/{idcamera}/power/{idpower}', 'CameraController@deletePower');
            Route::get('camera/status/{id}', 'CameraController@cameraStatus');

            /**
             * Llamada para acciones con playareas
             */
            Route::post('playarea', 'PlayAreaController@create');
            Route::put('playarea/{idarea}', 'PlayAreaController@update');
            Route::delete('playarea/{idarea}', 'PlayAreaController@delete');
            Route::get('playarea/{idarea}', 'PlayAreaController@playarea');
            Route::get('playareas', 'PlayAreaController@playareas');
            Route::get('playareas/toselect', 'PlayAreaController@toSelect');
            Route::get('playareas/toshow', 'PlayAreaController@toShow');
            Route::post('playareas/tocreateplaylist', 'PlayAreaController@toCreatePlaylist');
            Route::put('/playarea/{idarea}/playlogics', 'PlayAreaController@updatePlaylogics');
            Route::delete('/playarea/{idarea}/playlogic/{idlogic}', 'PlayAreaController@deletePlaylogic');
            Route::get('playareas/touser', 'PlayAreaController@toUser');
            Route::put('/playarea/{idarea}/contents/{idcategory}', 'PlayAreaController@updateContentsOrder');
            Route::delete('/playarea/{idarea}/force', 'PlayAreaController@deletePlayAreaForce');

            /**
             * Llamada para acciones con formats
             */
            Route::post('format', 'FormatController@create');
            Route::post('format/{idformat}', 'FormatController@update');
            Route::delete('format/{idformat}', 'FormatController@delete');
            Route::get('formats/toselect', 'FormatController@toSelect');
            Route::get('format/{idformat}', 'FormatController@format');
            Route::get('formats', 'FormatController@formats');
            Route::get('formats/touser', 'FormatController@toUser');

            /**
             * Llamada para acciones con assets
             */
            Route::post('asset', 'AssetController@create');
            Route::post('asset/{idasset}', 'AssetController@update');
            Route::delete('asset/{idasset}', 'AssetController@delete');
            Route::get('asset/{idasset}', 'AssetController@asset');
            Route::get('assets', 'AssetController@assets');
            Route::get('assets/toselect', 'AssetController@toSelect');
            Route::delete('asset/{idasset}/force', 'AssetController@deleteAssetForce');

            /**
             * Llamada para acciones con assets types
             */
            Route::get('asset-types/toselect', 'AssetTypeController@toSelect');

            /**
             * Llamada para acciones con indoor locations
             */
            Route::get('indoorlocations/toselect', 'IndoorLocationController@toSelect');
            Route::post('indoorlocation', 'IndoorLocationController@create');
            Route::put('indoorlocation/{idlocation}', 'IndoorLocationController@update');
            Route::delete('indoorlocation/{idlocation}', 'IndoorLocationController@delete');
            Route::get('indoorlocation/{idlocation}', 'IndoorLocationController@indoorlocation');
            Route::get('indoorlocations', 'IndoorLocationController@indoorlocations');
            Route::delete('indoorlocation/{idlocation}/force', 'IndoorLocationController@deleteIndoorLocationForce');

            /**
             * Llamada para acciones con langs
             */
            Route::post('lang', 'LangController@create');
            Route::post('lang/{idlang}', 'LangController@update');
            Route::delete('lang/{idlang}', 'LangController@delete');
            Route::get('langs/toselect', 'LangController@toSelect');
            Route::get('lang/{idlang}', 'LangController@lang');
            Route::get('langs', 'LangController@langs');
            Route::get('langs/touser', 'LangController@toUser');

            /**
             * Llamada para acciones con playcircuits
             */
            Route::post('playcircuit', 'PlayCircuitController@create');
            Route::post('playcircuit/{idcircuit}', 'PlayCircuitController@update');
            Route::delete('playcircuit/{idcircuit}', 'PlayCircuitController@delete');
            Route::get('playcircuit/{idcircuit}', 'PlayCircuitController@playcircuit');
            Route::get('playcircuits', 'PlayCircuitController@playcircuits');
            Route::get('playcircuits/toselect', 'PlayCircuitController@toselect');
            Route::put('playcircuit/{idcircuit}/tags', 'PlayCircuitController@updateTags');
            Route::delete('playcircuit/{idcircuit}/tag/{idtag}', 'PlayCircuitController@deleteTag');
            Route::put('playcircuit/{idcircuit}/countries', 'PlayCircuitController@updateCountries');
            Route::delete('playcircuit/{idcircuit}/country/{idcountry}', 'PlayCircuitController@deleteCountry');
            Route::put('playcircuit/{idcircuit}/provinces', 'PlayCircuitController@updateProvinces');
            Route::delete('playcircuit/{idcircuit}/province/{idprovince}', 'PlayCircuitController@deleteProvince');
            Route::put('playcircuit/{idcircuit}/cities', 'PlayCircuitController@updateCities');
            Route::delete('playcircuit/{idcircuit}/city/{idcity}', 'PlayCircuitController@deleteCity');
            Route::put('playcircuit/{idcircuit}/sites', 'PlayCircuitController@updateSites');
            Route::delete('playcircuit/{idcircuit}/site/{idsite}', 'PlayCircuitController@deleteSite');
            Route::put('playcircuit/{idcircuit}/langs', 'PlayCircuitController@updateLangs');
            Route::delete('playcircuit/{idcircuit}/lang/{idlang}', 'PlayCircuitController@deleteLang');

            /**
             * Llamada para acciones con country
             */
            Route::post('country', 'CountryController@create');
            Route::put('country/{idcategory}', 'CountryController@update');
            Route::delete('country/{idcategory}', 'CountryController@delete');
            Route::get('countries/toselect', 'CountryController@toSelect');
            Route::get('country/{idcategory}', 'CountryController@country');
            Route::get('countries', 'CountryController@countries');

            /**
             * Llamada para acciones con province
             */
            Route::get('provinces/toselect/{idprovince}', 'ProvinceController@toSelectByCountry');
            Route::get('provinces/toselect', 'ProvinceController@toSelect');


            /**
             * Llamada para acciones con city
             */
            Route::get('cities/toselect', 'CityController@toSelect');

            /**
             * Llamada para acciones con contenttype
             */
            Route::get('content-types/toselect', 'ContentTypeController@toSelect');

            /**
             * Llamada para acciones con content category
             */
            Route::post('content-category', 'ContentCategoryController@create');
            Route::post('content-category/{idcategory}', 'ContentCategoryController@update');
            Route::delete('content-category/{idcategory}', 'ContentCategoryController@delete');
            Route::get('content-categories/toselect', 'ContentCategoryController@toSelect');
            Route::get('content-category/{idcategory}', 'ContentCategoryController@contentcategory');
            Route::get('content-categories', 'ContentCategoryController@contentCategories');

            /**
             * Llamada para acciones con contents
             */
            Route::post('content', 'ContentController@create');
            Route::put('content/{idcontent}', 'ContentController@update');
            Route::delete('content/{idcontent}', 'ContentController@delete');
            Route::get('content/{idcontent}', 'ContentController@content');

            Route::get('contents', 'ContentController@contents');
            Route::get('contents/toselect', 'ContentController@toSelect');
            Route::post('contents/toemission', 'ContentController@toEmission');
            Route::get('contents/{status}', 'ContentController@contentsStatus');
            Route::put('/content/{idcontent}/powers', 'ContentController@updatePowers');
            Route::delete('/content/{idcontent}/power/{idpower}', 'ContentController@deletePower');
            Route::put('/content/{idcontent}/products', 'ContentController@updateProducts');
            Route::get('/content/{idcontent}/products', 'ContentController@getProducts');
            Route::delete('/content/{idcontent}/product/{product_code}', 'ContentController@deleteProduct');
            Route::put('/content/{idcontent}/playareas', 'ContentController@updatePlayareas');
            Route::delete('/content/{idcontent}/playarea/{idarea}', 'ContentController@deletePlayarea');
            Route::put('/content/{idcontent}/tags', 'ContentController@updateTags');
            Route::delete('/content/{idcontent}/tag/{idtag}', 'ContentController@deleteTag');
            Route::post('/content/{idcontent}/asset/{idasset}', 'ContentController@createAsset');
            Route::put('/content/{idcontent}/assets', 'ContentController@updateAssets');
            Route::delete('/content/{idcontent}/asset/{idasset}', 'ContentController@deleteAsset');
            Route::put('/content/{idcontent}/playciruits', 'ContentController@updatePlaycircuits');
            Route::put('/content/{idcontent}/sites', 'ContentController@updateSites');
            Route::put('/content/{idcontent}/dates', 'ContentController@updateDates');
            Route::delete('/content/{idcontent}/playcircuit/{idcircuit}', 'ContentController@deletePlaycircuit');
            Route::post('/content/products/primetime', 'ContentController@productsPrimeTime');
            Route::post('/content/content/primetime', 'ContentController@contentsPrimeTime');
            Route::get('/content/{idcontent}/primetime', 'ContentController@primeTime');
            Route::put('/contents/orders', 'ContentController@updateContentOrder');
            Route::get('/contents/orders', 'ContentController@getOrder');
            Route::get('/content/status/{id}', 'ContentController@contentStatus');
            /**
             * Llamada para acciones con assets
             */
            Route::post('playlogic', 'PlayLogicController@create');
            Route::put('playlogic/{idplaylogic}', 'PlayLogicController@update');
            Route::delete('playlogic/{idplaylogic}', 'PlayLogicController@delete');
            Route::get('playlogic/{idplaylogic}', 'PlayLogicController@playlogic');
            Route::get('playlogics', 'PlayLogicController@playlogics');
            Route::get('playlogics/toselect', 'PlayLogicController@toSelect');

            /**
             * Llamadas para acciones con templates_sector
             */
            Route::post('templatessector', 'TemplatesSectorController@create');
            Route::put('templatessector/{idtemplatesector}', 'TemplatesSectorController@update');
            Route::delete('templatessector/{idtemplatesector}', 'TemplatesSectorController@delete');
            Route::get('templatessector/{idtemplatesector}', 'TemplatesSectorController@templatesSector');
            Route::get('templatessectors', 'TemplatesSectorController@templatesSectors');
            Route::get('templatessectors/toselect', 'TemplatesSectorController@templatesSectorsToSelect');

            /**
             * Llamadas para acciones con templates_customer
             */
            Route::post('templatescustomer', 'TemplatesCustomerController@create');
            Route::put('templatescustomer/{idtemplatecustomer}', 'TemplatesCustomerController@update');
            Route::delete('templatescustomer/{idtemplatecustomer}', 'TemplatesCustomerController@delete');
            Route::get('templatescustomer/{idtemplatecustomer}', 'TemplatesCustomerController@templatesCustomer');
            Route::get('templatescustomers', 'TemplatesCustomerController@templatesCustomers');
            Route::get('templatescustomers/toselect', 'TemplatesCustomerController@templatesCustomersToSelect');
            Route::post('templatescustomer/replace/{idtemplatecustomer}', 'TemplatesCustomerController@createOrUpdate');
            Route::get('templatescustomersbyidcustomermaster', 'TemplatesCustomerController@getTemplatesCustomerByIdcustomerMaster');

            /**
             * Llamadas para acciones con la landing page
             */
            Route::post('landingpage', 'LandingpageController@create');
            Route::put('landingpage/{id}', 'LandingpageController@update');
            Route::delete('landingpage/{id}', 'LandingpageController@delete');
            Route::get('landingpage/{id}', 'LandingpageController@landingpage');
            Route::get('landingpages', 'LandingpageController@landingpages');

            /**
             * Llamadas a productos
             */
            Route::get('products/toselect', 'ProductController@toSelect');
            Route::get('products/categories/toselect', 'ProductController@categoriesToSelect');
            Route::get('product/{idproduct}/tags', 'ProductController@getProductTags');
            Route::post('product/tags', 'ProductController@getAllProductTags');
            Route::put('product/{idproduct}/tags', 'ProductController@updateProductTags');
            Route::post('product', 'ProductController@create');
            Route::put('product/{idproduct}', 'ProductController@update');
            Route::delete('product/{idproduct}', 'ProductController@delete');
            Route::get('product', 'ProductController@detail');
            Route::post('product/category', 'ProductController@createCategory');
            Route::put('product/category/{idcategory}', 'ProductController@updateCategory');
            Route::delete('product/category/{idcategory}', 'ProductController@deleteCategory');
            Route::get('product/category', 'ProductController@detailCategory');
            Route::delete('product/{idproduct}/content/{idcontent}', 'ProductController@detachFromContent');
        });
    });
});

Route::group(['prefix' => 'v3', 'namespace' => 'Api\v3\Controllers'], function () {
    require base_path('routes/api.v3.php');
});
