<?php
Route::group(['prefix' => 'processcalculoauto', 'middleware' => 'ws'], function() {
    Route::post('/auto', 'AutomatizacionCalculoCtrl@saveDataProccess')->name('api.v3.process.autocalculo');
    Route::post('/daily', 'AutomatizacionCalculoCtrl@saveDataProccessDaily')->name('api.v3.process.daily');
});