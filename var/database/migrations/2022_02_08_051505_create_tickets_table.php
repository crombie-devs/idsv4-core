<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tickets', function (Blueprint $table) {
                
            $table->increments('id');
            
            $table->integer('phone')->length(8)->nullable();
            $table->string('title')->length(191);
            $table->string('email')->nullable();
            $table->string('point_address')->nullable();
            $table->text('description')->nullable();

            $table->unsignedInteger('id_priority');
            $table->unsignedInteger('id_category');
            $table->unsignedInteger('id_department');
            $table->unsignedInteger('id_responsible');
            $table->unsignedInteger('id_site');
            $table->unsignedInteger('id_status');
            $table->unsignedInteger('id_customer');
            $table->unsignedInteger('id_user');

            $table->boolean('finished')->default(false);
            $table->boolean('deleted')->default(false);
            $table->boolean('accept')->default(false);

            $table->boolean('expected_resolution_date')->default(false);
            $table->boolean('notify_user')->default(false);

            $table->date('date_alert');

            $table->timestamps();

        });

        // Schema::table('tickets', function (Blueprint $table) {
        //     $table->foreign('priority_id')->references('id')->on('tickets_priorities');
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
