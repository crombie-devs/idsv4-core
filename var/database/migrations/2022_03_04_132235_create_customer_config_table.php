<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_config', function (Blueprint $table) {
            $table->id();
            $table->integer('sms_apikey', 1)->nullable()->default(0);
            $table->integer('mailing_apikey', 1)->nullable()->default(0);
            $table->integer('whatsapp_apikey', 1)->nullable()->default(0);
            $table->foreignId('idcontent')->references('idcontent')->on('contents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_config');
    }
}
