<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets_comments', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->text('mensaje');
            $table->unsignedInteger('id_ticket');
            $table->unsignedInteger('id_admin');
            $table->unsignedInteger('id_created_by');
            $table->boolean('notificar_usuario')->default(false);
            $table->boolean('leido')->default(false);
            $table->boolean('deleted')->default(false);

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_comments');
    }
}
