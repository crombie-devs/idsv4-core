<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('idlog');
            $table->foreignId('iduser')->references('iduser')->on('user')->onDelete('cascade');
            $table->bigInteger('identity');
            $table->string('entity')->length(100);
            $table->string('action')->length(100);
            $table->string('ip')->length(50);
            $table->date('datetime')->useCurrent();
            $table->boolean('status');
            $table->string('path')->length(255);
            $table->text('params')->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
