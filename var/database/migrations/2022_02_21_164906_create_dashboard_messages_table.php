<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_messages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->default('0');
            $table->foreignId('customer_id')->references('idcustomer')->on('customers')->onDelete('cascade');
            $table->text('message');
            $table->date('date_on');
            $table->date('date_off');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_messages');
    }
}
