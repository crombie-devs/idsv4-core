<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ContentType;
use Faker\Generator as Faker;

$factory->define(ContentType::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
