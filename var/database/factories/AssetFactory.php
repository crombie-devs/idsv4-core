<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\Site;
use App\Models\Customer;
use App\Models\Lang;
use App\Models\Format;

use Faker\Generator as Faker;

$factory->define(Asset::class, function (Faker $faker) {

    $idcustomer=null;
    $idsite=null;

    $rndcustomer = rand(0, 15);
    if($rndcustomer>5){
        $idcustomer = Customer::whereIn('idcustomer',[1,2])->pluck('idcustomer')->random();

        $rndsite = rand(0, 15);
        if($rndcustomer>8){
            $idsite = Site::where('idcustomer',$idcustomer )->pluck('idsite')->random();
        }
    }



    $idlang = Lang::all()->pluck('idlang')->random();
    $idtype = AssetType::all()->pluck('idtype')->random();
    $idformat = Format::all()->pluck('idformat')->random();

   

    return [
        //'idcustomer' => $faker->randomElement($customers),
        'idcustomer' => $idcustomer,
        'idsite' => $idsite,
        'idtype' => $idtype,
        'name' => $faker->word,
        'asset_url' =>  $faker->url,
        'cover_url' => $faker->url,
        'duration' => $faker->numberBetween(1, 1000),
        'idformat' => $idformat,
        'size' => $faker->numberBetween(1, 1000),
        'idlang' => $idlang,
        'default' => $faker->randomElement([0, 1]),
        'has_audio' => $faker->randomElement([0, 1]),
        'status' => $faker->randomElement([0, 1]),
        'deleted' => $faker->randomElement([0, 1]),
    ];






});