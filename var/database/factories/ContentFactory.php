<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Content;
use App\Models\ContentType;
use App\Models\ContentCategory;
use App\Models\Site;
use App\Models\Customer;
use App\Models\Lang;
use App\Models\Format;

use Faker\Generator as Faker;
use Carbon\Carbon;


$factory->define(Content::class, function (Faker $faker) {
    $idcustomer=null;
    $idsite=null;
    $idcategory=null;

    $rndcustomer = rand(0, 15);
    
    if($rndcustomer>5){
        $idcustomer = Customer::whereIn('idcustomer',[7,26,31])->pluck('idcustomer')->random();

        $rndsite = rand(0, 15);
        if($rndcustomer>8){
            $idsite = Site::where('idcustomer',$idcustomer )->pluck('idsite')->random();
        }

        $rndcontentcategory = rand(0, 15);
        if($rndcontentcategory>8){
            $idcategory = ContentCategory::where('idcustomer',$idcustomer )->pluck('idcategory')->random();
        }
    }
    
    $idtype = ContentType::all()->pluck('idtype')->random();

    $year = 2020;
    $month = rand(1, 9);
    $day = rand(1, 28);
    $date = Carbon::create($year,$month ,$day , 0, 0, 0);
    $date_on = $date->format('Y-m-d');
    $date_off =$date->addDays(rand(0, 30))->format('Y-m-d');
      

    return [
        'idcustomer' => $idcustomer,
        'idsite' => $idsite,
        'idtype' => $idtype,
        'idcategory' => $idcategory,
        'code' => $faker->word,
        'name' => $faker->word,
        'thumbnail_url' => $faker->url,
        'tracker_url' => $faker->url,
        'date_on' => $date_on,
        'date_off' => $date_on,
        'max_emissions' => $faker->numberBetween(1, 1000),
        'frequency' => $faker->numberBetween(1, 1000),
        'frequency_method' => $faker->numberBetween(1, 1000),
        'is_exclusive' => $faker->randomElement([0, 1]),
        'status' => $faker->randomElement([0, 1]),
        'deleted' => $faker->randomElement([0, 1]),
    ];

});