<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Camera;
use App\Models\Site;
use Faker\Generator as Faker;

$factory->define(Camera::class, function (Faker $faker) {
    $sites = Site::all()->pluck('idsite')->toArray();
    return [
        'idsite' => $faker->randomElement($sites),
        'name' => $faker->word,
        'email' => $faker->unique()->safeEmail
    ];
});