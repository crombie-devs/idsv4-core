<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\IndoorLocation;
use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(IndoorLocation::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->name
    ];
});
