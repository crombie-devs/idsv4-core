<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Province;
use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(Province::class, function (Faker $faker) {
    $country = Country::all()->pluck('idcountry')->toArray();

    return [
        'name' => $faker->city(),
        'idcountry' =>$faker->randomElement($country),
    ];
});