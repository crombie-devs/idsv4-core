<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PlayArea;
use App\Models\IndoorLocation;
use App\Models\Format;
use App\Models\Customer;

use Faker\Generator as Faker;

$factory->define(PlayArea::class, function (Faker $faker) {
    $customers = Customer::where('deleted', 0)->get()->pluck('idcustomer')->toArray();
    $dataLocation['idcustomer'] = $faker->randomElement($customers);
    $dataLocation['name'] = $faker->word;
    $location = IndoorLocation::create($dataLocation);

    $dataFormat['name'] = $faker->unique()->realText($maxNbChars = 50, $indexSize = 1);
    //$dataFormat['image_url'] = $faker->image(storage_path('images\formats'),200,200, 'cats', true);
    $dataFormat['description'] = $faker->realText($maxNbChars = 255, $indexSize = 2);

    $format = Format::create($dataFormat);

    return [
        'idlocation' => $location->idlocation,
        'idformat' => $format->idformat,
        'name' => $faker->word,
    ];
});