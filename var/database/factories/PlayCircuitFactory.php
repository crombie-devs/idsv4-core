<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PlayCircuit;
use App\Models\Country;
use App\Models\Province;
use App\Models\City;
use App\Models\Tag;
use App\Models\Customer;

use Faker\Generator as Faker;

$factory->define(PlayCircuit::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
   
       
   /* $customers = Customer::where('deleted', 0)->get()->pluck('idcustomer')->toArray();
    $dataLocation['idcustomer'] = $faker->randomElement($customers);
    $dataLocation['name'] = $faker->word;
    $location = IndoorLocation::create($dataLocation);

    $dataFormat['name'] = $faker->unique()->realText($maxNbChars = 50, $indexSize = 1);
    //$dataFormat['image_url'] = $faker->image(storage_path('images\formats'),200,200, 'cats', true);
    $dataFormat['description'] = $faker->realText($maxNbChars = 255, $indexSize = 2);

    $format = Format::create($dataFormat);*/

    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->word,
        'image_url'=>'',
        'status' =>  $faker->numberBetween(1, 3),
    ];
});