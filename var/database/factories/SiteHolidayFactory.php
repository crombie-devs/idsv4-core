<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SiteHoliday;
use App\Models\Site;
use Faker\Generator as Faker;

$factory->define(SiteHoliday::class, function (Faker $faker) {
    $sites = site::all()->pluck('idsite')->toArray();
    return [
        'idsite' => $faker->randomElement($sites),
        'date' => $faker->unique()->dateTimeThisYear()->format('Y-m-d'),
    ];
});
