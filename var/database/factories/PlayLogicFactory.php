<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PlayLogic;
use App\Models\ContentType;
use App\Models\Site;
use App\Models\Customer;
use App\Models\PlayArea;
use App\Models\ContentCategory;

use Faker\Generator as Faker;

$factory->define(PlayLogic::class, function (Faker $faker) {
    $idcustomer=null;
    $idsite=null;

    $idcustomer = Customer::whereIn('idcustomer',[1,2])->pluck('idcustomer')->random();
    $idarea = PlayArea::all()->pluck('idarea')->random();
    $idtype = ContentType::all()->pluck('idtype')->random();
    $idcategory = ContentCategory::all()->pluck('idcategory')->random();

    return [
        'idcustomer' => $idcustomer,
        'idarea' => $idarea,
        'idtype' => $idtype,
        'idcategory' => $idcategory,
        'order' => $faker->randomElement([0, 5]),
    ];
});