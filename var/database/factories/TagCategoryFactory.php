<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TagCategory;
use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(TagCategory::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
    $customers[] = null;
    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->name,
    ];
});
