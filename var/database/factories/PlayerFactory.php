<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Player;

use App\Models\Site;
use App\Models\IndoorLocation;
use App\Models\PlayArea;
use Faker\Generator as Faker;

$factory->define(Player::class, function (Faker $faker) {
    $sites = Site::all()->pluck('idsite')->toArray();
    $site = $faker->randomElement($sites);
    $site = Site::find($site->idsite);
    $locations = IndoorLocation::where('idcustomer', $site->idcustomer)->get()->pluck('idlocation')->toArray();
    $location = $faker->randomElement($locations);
    $playareas = PlayArea::where('idlocation', $location)->get()->pluck('idarea')->toArray();
    return [
        'idsite' => $site,
        'idarea' => $faker->randomElement($playareas),
        'email' => $faker->unique()->safeEmail,
    ];
});