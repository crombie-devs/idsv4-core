<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Player;
use App\Models\PlayerPower;
use Faker\Generator as Faker;

$factory->define(PlayerPower::class, function (Faker $faker) {
    $players = Player::all()->pluck('idplayer')->toArray();
    return [
        'idplayer' => $faker->randomElement($players),
        'weekday' => $faker->numberBetween($min = 1, $max = 7),
        'time_on' => $faker->time(),
        'time_off' => $faker->time()
    ];
});