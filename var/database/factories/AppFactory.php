<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\App;
use Faker\Generator as Faker;
use App\Models\Customer;

$factory->define(App::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
    $customers[] = null;
    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->name,
        'apikey' => $faker->word
    ];
});
