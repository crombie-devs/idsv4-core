<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ContentCategory;
use Faker\Generator as Faker;
use App\Models\Customer;

$factory->define(ContentCategory::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
  
    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->name
    ];
});
