<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Site;
use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Site::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
    return [
        'idcustomer' => $faker->randomElement($customers),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
    ];
});
