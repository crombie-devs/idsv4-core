<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Camera;
use App\Models\CameraPower;
use Faker\Generator as Faker;

$factory->define(CameraPower::class, function (Faker $faker) {
    $cameras = Camera::all()->pluck('idcamera')->toArray();
    return [
        'idcamera' => $faker->randomElement($cameras),
        'weekday' => $faker->numberBetween($min = 1, $max = 7),
        'time_on' => $faker->time(),
        'time_off' => $faker->time()
    ];
});