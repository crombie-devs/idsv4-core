<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tag;
use App\Models\TagCategory;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    $tagCategories = TagCategory::all()->pluck('idcategory')->toArray();
    return [
        'idcategory' => $faker->randomElement($tagCategories),
        'name' => $faker->unique()->word,
    ];
});
