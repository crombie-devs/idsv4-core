<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\City;
use App\Models\Province;

use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {
    $province=Province::all()->random(1)->first();

    return [
        'name' => $faker->city(),
        'idprovince' => $province->idprovince
    ];
});