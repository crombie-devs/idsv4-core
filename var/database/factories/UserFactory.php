<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\Customer;
use App\Models\Site;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $customers = Customer::all()->pluck('idcustomer')->toArray();
    $sites = Site::all()->pluck('idsite')->toArray();
    for($i = 0; $i < 10; $i++){
        $sites[] = null;
    }
    $sites = Arr::shuffle($sites);
    return [
        'name' => $faker->name,
        'idcustomer' => $faker->randomElement($customers),
        'idsite' => $faker->randomElement($sites),
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->asciify('********************'), // password
    ];
});
