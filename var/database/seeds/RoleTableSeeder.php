<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getAppToSelectPermission = Permission::create(['name' => 'get.apps.toselect', 'guard_name' => 'api']);
        
        $superadminPermission = Permission::create(['name' => '*.*.*', 'guard_name' => 'api']);
        
        $getCustomerPermission = Permission::create(['name' => 'get.customer.*', 'guard_name' => 'api']);
        $putCustomerPermission = Permission::create(['name' => 'put.customer.*', 'guard_name' => 'api']);
        $getCustomerToSelectPermission = Permission::create(['name' => 'get.customers.toselect', 'guard_name' => 'api']);
        $deleteCustomerFormatPermission = Permission::create(['name' =>'delete.customer.*.format.*', 'guard_name' => 'api']);
        $updateCustomerFormatsPermission = Permission::Create(['name' => 'put.customer.*.formats', 'guard_name' => 'api']);

        $getSitesPermission = Permission::create(['name' => 'get.sites', 'guard_name' => 'api']);
        $getSitePermission = Permission::create(['name' => 'get.site.*', 'guard_name' => 'api']);
        $putSitePermission = Permission::create(['name' => 'put.site.*', 'guard_name' => 'api']);
        $deleteSitePermission = Permission::create(['name' => 'delete.site.*', 'guard_name' => 'api']);
        $putSiteHolidaysPermission = Permission::create(['name' => 'put.site.*.holidays', 'guard_name' => 'api']);
        $deleteSiteHolidaytPermission = Permission::create(['name' => 'delete.site.*.holiday.*', 'guard_name' => 'api']);
        $putSiteTagsPermission = Permission::create(['name' => 'put.site.*.tags', 'guard_name' => 'api']);
        $deleteSiteTagtPermission = Permission::create(['name' => 'delete.site.*.tag.*', 'guard_name' => 'api']);

        $getPlayersPermission = Permission::create(['name' => 'get.players', 'guard_name' => 'api']);
        $getPlayerPermission = Permission::create(['name' => 'get.player.*', 'guard_name' => 'api']);
        $putPlayerPermission = Permission::create(['name' => 'put.player.*', 'guard_name' => 'api']);
        $postPlayersToAnalyticsPermission = Permission::create(['name' => 'post.players.toanalytics', 'guard_name' => 'api']);
        $postPlayerPermission = Permission::create(['name' => 'post.player', 'guard_name' => 'api']);
        $deletePlayerPermission = Permission::create(['name' => 'delete.player.*', 'guard_name' => 'api']);
        $getPlayerToSelectPermission = Permission::create(['name' => 'get.player.toselect', 'guard_name' => 'api']);
        $updatePlayerPlayLogicsPermission = Permission::Create(['name' => 'put.player.*.playlogics', 'guard_name' => 'api']);
        $deletePlayerPlayLogicPermission = Permission::create(['name' =>'delete.player.*.playlogic.*', 'guard_name' => 'api']);
        $updatePlayerContentsPermission = Permission::Create(['name' => 'put.player.*.contents.*', 'guard_name' => 'api']);
        $updatePlayerPowersPermission = Permission::Create(['name' => 'put.player.*.powers', 'guard_name' => 'api']);
        $deletePlayerPowerPermission = Permission::create(['name' =>'delete.player.*.power.*', 'guard_name' => 'api']);
        $updatePlayerTagsPermission = Permission::Create(['name' => 'put.player.*.tags', 'guard_name' => 'api']);
        $deletePlayerTagPermission = Permission::create(['name' =>'delete.player.*.tag.*', 'guard_name' => 'api']);

        $getTagsToSelectPermission = Permission::create(['name' => 'get.tags.toselect', 'guard_name' => 'api']);
        $getTagsPermission = Permission::create(['name' => 'get.tags', 'guard_name' => 'api']);
        $getTagPermission = Permission::create(['name' => 'get.tag.*', 'guard_name' => 'api']);
        $putTagPermission = Permission::create(['name' => 'put.tag.*', 'guard_name' => 'api']);
        $postTagPermission = Permission::create(['name' => 'post.tag', 'guard_name' => 'api']);
        $deleteTagPermission = Permission::create(['name' => 'delete.tag.*', 'guard_name' => 'api']);
        
        $getTagCategoriesToSelectPermission = Permission::create(['name' => 'get.tagcategories.toselect', 'guard_name' => 'api']);
        $getTagCategoriesPermission = Permission::create(['name' => 'get.tagcategories', 'guard_name' => 'api']);
        $getTagCategoryPermission = Permission::create(['name' => 'get.tagcategory.*', 'guard_name' => 'api']);
        $putTagCategoryPermission = Permission::create(['name' => 'put.tagcategory.*', 'guard_name' => 'api']);
        $postTagCategoryPermission = Permission::create(['name' => 'post.tagcategory', 'guard_name' => 'api']);
        $deleteTagCategoryPermission = Permission::create(['name' => 'delete.tagcategory.*', 'guard_name' => 'api']);

        $getCamerasPermission = Permission::create(['name' => 'get.cameras', 'guard_name' => 'api']);
        $getCameraPermission = Permission::create(['name' => 'get.camera.*', 'guard_name' => 'api']);
        $putCameraPermission = Permission::create(['name' => 'put.camera.*', 'guard_name' => 'api']);
        $postCamerasToAnalyticsPermission = Permission::create(['name' => 'post.cameras.toanalytics', 'guard_name' => 'api']);
        $getCamerasToSelectPermission = Permission::create(['name' => 'get.cameras.toselect', 'guard_name' => 'api']);
        $updateCameraPowerPermission = Permission::Create(['name' => 'put.camera.*.powers', 'guard_name' => 'api']);
        $deleteCameraPowerPermission = Permission::create(['name' =>'delete.camera.*.power.*', 'guard_name' => 'api']);

        $getPlayAreasPermission = Permission::create(['name' => 'get.playareas', 'guard_name' => 'api']);
        $getPlayAreaPermission = Permission::create(['name' => 'get.playarea.*', 'guard_name' => 'api']);
        $getPlayAreaToSelectPermission = Permission::create(['name' => 'get.playareas.toselect', 'guard_name' => 'api']);
        $updatePlayAreaPlayLogicsPermission = Permission::Create(['name' => 'put.playarea.*.playlogics', 'guard_name' => 'api']);
        $deletePlayLogicPlayAreaPermission = Permission::create(['name' =>'delete.playarea.*.playlogic.*', 'guard_name' => 'api']);
        $getPlayAreaToUserPermission = Permission::create(['name' => 'get.playareas.touser', 'guard_name' => 'api']);
        $updatePlayAreaContentsPermission = Permission::Create(['name' => 'put.playarea.*.contents.*', 'guard_name' => 'api']);
        $deletePlayAreaForcePermission = Permission::create(['name' =>'delete.playarea.*.force', 'guard_name' => 'api']);
        
        $getFormatsPermission = Permission::create(['name' => 'get.formats', 'guard_name' => 'api']);
        $getFormatPermission = Permission::create(['name' => 'get.format.*', 'guard_name' => 'api']);
        $postFormatsPermission = Permission::create(['name' => 'post.formats', 'guard_name' => 'api']);
        $postFormatPermission = Permission::create(['name' => 'post.format.*', 'guard_name' => 'api']);
        $deleteFormatPermission = Permission::create(['name' => 'delete.format.*', 'guard_name' => 'api']);
        $getFormatToSelectPermission = Permission::create(['name' => 'get.formats.toselect', 'guard_name' => 'api']);
        $getFormatsToUserPermission = Permission::create(['name' => 'get.formats.touser', 'guard_name' => 'api']);

        $getLangPermission = Permission::create(['name' => 'get.lang.*', 'guard_name' => 'api']);
        $getLangsToSelectPermission = Permission::create(['name' => 'get.langs.toselect', 'guard_name' => 'api']);
        $getLangsToUserPermission = Permission::create(['name' => 'get.langs.touser', 'guard_name' => 'api']);

        $getIndoorLocationToSelectPermission = Permission::create(['name' => 'get.indoorlocations.toselect', 'guard_name' => 'api']);
        $getIndoorLocationsPermission = Permission::create(['name' => 'get.indoorlocations', 'guard_name' => 'api']);
        $getIndoorLocationPermission = Permission::create(['name' => 'get.indoorlocation.*', 'guard_name' => 'api']);
        $putIndoorLocationPermission = Permission::create(['name' => 'put.indoorlocation.*', 'guard_name' => 'api']);
        $postIndoorLocationPermission = Permission::create(['name' => 'post.indoorlocation', 'guard_name' => 'api']);
        $deleteIndoorLocationForcePermission = Permission::create(['name' =>'delete.indoorlocation.*.force', 'guard_name' => 'api']);
        $deleteIndoorLocationPermission = Permission::create(['name' => 'delete.indoorlocation.*', 'guard_name' => 'api']);
        
        $getPlayCircuitsPermission = Permission::create(['name' => 'get.playcircuits', 'guard_name' => 'api']);
        $getPlayCircuitsToSelectPermission = Permission::create(['name' => 'get.playcircuits.toselect', 'guard_name' => 'api']);
        $getPlayCircuitPermission = Permission::create(['name' => 'get.playcircuit.*', 'guard_name' => 'api']);
        $putPlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*', 'guard_name' => 'api']);
        $postPlayCircuitPermission = Permission::create(['name' => 'post.playcircuit', 'guard_name' => 'api']);
        $deletePlayCircuitPermission = Permission::create(['name' => 'delete.playcircuit.*', 'guard_name' => 'api']);
        $putPlayCircuitTagsPermission = Permission::create(['name' => 'put.playcircuit.*.tags', 'guard_name' => 'api']);
        $deleteTagPlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.tag.*', 'guard_name' => 'api']);
        $putPlayCircuitCountriesPermission = Permission::create(['name' => 'put.playcircuit.*.countries', 'guard_name' => 'api']);
        $deleteCountryPlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.country.*', 'guard_name' => 'api']);
        $putPlayCircuitProvincesPermission = Permission::create(['name' => 'put.playcircuit.*.provinces', 'guard_name' => 'api']);
        $deleteProvincePlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.province.*', 'guard_name' => 'api']);
        $putPlayCircuitCitiesPermission = Permission::create(['name' => 'put.playcircuit.*.cities', 'guard_name' => 'api']);
        $deleteCityPlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.city.*', 'guard_name' => 'api']);
        $putPlayCircuitSitesPermission = Permission::create(['name' => 'put.playcircuit.*.sites', 'guard_name' => 'api']);
        $deleteSitePlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.site.*', 'guard_name' => 'api']);
        $putPlayCircuitLangsPermission = Permission::create(['name' => 'put.playcircuit.*.langs', 'guard_name' => 'api']);
        $deleteLangPlayCircuitPermission = Permission::create(['name' => 'put.playcircuit.*.lang.*', 'guard_name' => 'api']);

        $getCountryToSelectPermission = Permission::create(['name' => 'get.countries.toselect', 'guard_name' => 'api']);
        $getProvinceToSelectPermission = Permission::create(['name' => 'get.provinces.toselect', 'guard_name' => 'api']);
        $getCityToSelectPermission = Permission::create(['name' => 'get.cities.toselect', 'guard_name' => 'api']);

        $getContentTypeToSelectPermission = Permission::create(['name' => 'get.content-types.toselect', 'guard_name' => 'api']);

        $getContentCategoryToSelectPermission = Permission::create(['name' => 'get.content-categories.toselect', 'guard_name' => 'api']);
        $getContentCategoryPermission = Permission::create(['name' => 'get.content-category.*', 'guard_name' => 'api']);
        $getContentCategoriesPermission = Permission::create(['name' => 'get.content-categories', 'guard_name' => 'api']);
        $putContentCategoryPermission = Permission::create(['name' => 'put.content-category.*', 'guard_name' => 'api']);
        $postContentCategoryPermission = Permission::create(['name' => 'post.content-category', 'guard_name' => 'api']);
        $deleteContentCategoryPermission = Permission::create(['name' => 'delete.content-category.*', 'guard_name' => 'api']);

        $getAssetTypeToSelectPermission = Permission::create(['name' => 'get.asset-types.toselect', 'guard_name' => 'api']);
        $getAssetsToSelectPermission = Permission::create(['name' => 'get.assets.toselect', 'guard_name' => 'api']);
        $getAssetsPermission = Permission::create(['name' => 'get.assets', 'guard_name' => 'api']);
        $getAssetPermission = Permission::create(['name' => 'get.asset.*', 'guard_name' => 'api']);
        $putAssetPermission = Permission::create(['name' => 'put.asset.*', 'guard_name' => 'api']);
        $postAssetPermission = Permission::create(['name' => 'post.asset', 'guard_name' => 'api']);
        $deleteAssetPermission = Permission::create(['name' => 'delete.asset.*', 'guard_name' => 'api']);
        $deleteAssetForcePermission = Permission::create(['name' =>'delete.asset.*.force', 'guard_name' => 'api']);

        $getContentToSelectPermission = Permission::create(['name' => 'get.contents.toselect', 'guard_name' => 'api']);
        $postContentToEmissionPermission = Permission::create(['name' => 'post.contents.toemission', 'guard_name' => 'api']);
        $getContentPermission = Permission::create(['name' => 'get.content.*', 'guard_name' => 'api']);
        $getContentesPermission = Permission::create(['name' => 'get.contents', 'guard_name' => 'api']);
        $putContentPermission = Permission::create(['name' => 'put.content.*', 'guard_name' => 'api']);
        $postContentPermission = Permission::create(['name' => 'post.content', 'guard_name' => 'api']);
        $deleteContentPermission = Permission::create(['name' => 'delete.content.*', 'guard_name' => 'api']);
        
        $updateContentPowersPermission = Permission::Create(['name' => 'put.content.*.powers', 'guard_name' => 'api']);
        $deletePowerContentPermission = Permission::create(['name' =>'delete.content.*.power.*', 'guard_name' => 'api']);
        $updateContentProductsPermission = Permission::Create(['name' => 'put.content.*.products', 'guard_name' => 'api']);
        $deleteProductContentPermission = Permission::create(['name' =>'delete.content.*.product.*', 'guard_name' => 'api']);
        $updateContentPlayareasPermission = Permission::Create(['name' => 'put.content.*.playareas', 'guard_name' => 'api']);
        $deletePlayareaContentPermission = Permission::create(['name' =>'delete.content.*.playarea.*', 'guard_name' => 'api']);
        $updateContentTagsPermission = Permission::Create(['name' => 'put.content.*.tags', 'guard_name' => 'api']);
        $deleteTagContentPermission = Permission::create(['name' =>'delete.content.*.tag.*', 'guard_name' => 'api']);
        $createContentAssetsPermission = Permission::Create(['name' => 'post.content.*.asset.*', 'guard_name' => 'api']);
        $updateContentAssetsPermission = Permission::Create(['name' => 'put.content.*.assets', 'guard_name' => 'api']);
        $deleteAssetContentPermission = Permission::create(['name' =>'delete.content.*.asset.*', 'guard_name' => 'api']);
        $updateContentPlaycircuitsPermission = Permission::Create(['name' => 'put.content.*.playcircuits', 'guard_name' => 'api']);
        $deletePlaycircuitContentPermission = Permission::create(['name' =>'delete.content.*.playcircuit.*', 'guard_name' => 'api']);
        $updateContentOrderPermission = Permission::Create(['name' => 'put.contents.order', 'guard_name' => 'api']);
        $getContentsOrderPermission = Permission::create(['name' => 'get.contents.order', 'guard_name' => 'api']);

        $getPlayLogicsToSelectPermission = Permission::create(['name' => 'get.playlogics.toselect', 'guard_name' => 'api']);
        $getPlayLogicsPermission = Permission::create(['name' => 'get.playlogics', 'guard_name' => 'api']);
        $getPlayLogicPermission = Permission::create(['name' => 'get.playlogic.*', 'guard_name' => 'api']);
        $putPlayLogicPermission = Permission::create(['name' => 'put.playlogic.*', 'guard_name' => 'api']);
        $postPlayLogicPermission = Permission::create(['name' => 'post.playlogic', 'guard_name' => 'api']);
        $deletePlayLogicPermission = Permission::create(['name' => 'delete.playlogic.*', 'guard_name' => 'api']);

        $getProductsToSelectPermission = Permission::create(['name' => 'get.products.toselect', 'guard_name' => 'api']);
        $getProductsPermission = Permission::create(['name' => 'get.products', 'guard_name' => 'api']);
        $getProductPermission = Permission::create(['name' => 'get.product.*', 'guard_name' => 'api']);
        $putProductPermission = Permission::create(['name' => 'put.product.*', 'guard_name' => 'api']);
        $postProductPermission = Permission::create(['name' => 'post.product', 'guard_name' => 'api']);
        $deleteProductPermission = Permission::create(['name' => 'delete.product.*', 'guard_name' => 'api']);        

        $superadmin = Role::create(['name' => 'superadmin', 'guard_name' => 'api']);
        $superadmin->givePermissionTo(
            $superadminPermission,
            $getAppToSelectPermission,
            $getCustomerPermission,
            $putCustomerPermission,
            $getSitesPermission,
            $getSitePermission,
            $putSitePermission,
            $getPlayersPermission, 
            $getPlayerPermission,
            $putPlayerPermission,
            $getTagsPermission, 
            $getTagPermission,
            $putTagPermission,
            $getCamerasPermission,
            $getCameraPermission,
            $getCamerasToSelectPermission,
            $putCameraPermission,
            $getPlayAreasPermission,
            $getPlayAreaPermission,
            $getFormatsPermission,
            $getFormatPermission,
            $postFormatsPermission,
            $postFormatPermission,
            $deleteFormatPermission,
            $getFormatToSelectPermission,
            $getLangPermission,
            $getLangsToSelectPermission,
            $getIndoorLocationPermission,
            $getIndoorLocationsPermission,
            $putIndoorLocationPermission,
            $postIndoorLocationPermission,
            $deleteIndoorLocationPermission,
            $getIndoorLocationToSelectPermission,
            $getPlayCircuitsPermission,
            $getPlayCircuitsToSelectPermission,
            $getPlayCircuitPermission,
            $putPlayCircuitPermission,
            $postPlayCircuitPermission,
            $deletePlayCircuitPermission,
            $getTagCategoriesPermission,
            $getTagCategoryPermission,
            $putTagCategoryPermission,
            $postTagCategoryPermission,
            $deleteTagCategoryPermission,
            $postTagPermission,
            $deleteTagPermission,
            $getCountryToSelectPermission,
            $getProvinceToSelectPermission,
            $getCityToSelectPermission,
            $getContentTypeToSelectPermission,
            $getContentCategoryToSelectPermission,
            $getContentCategoryPermission,
            $getContentCategoriesPermission,
            $putContentCategoryPermission,
            $postContentCategoryPermission,
            $deleteContentCategoryPermission,
            $getAssetTypeToSelectPermission,
            $getTagsToSelectPermission,
            $getTagCategoriesToSelectPermission,
            $getAssetsToSelectPermission,
            $getAssetsPermission,
            $getAssetPermission,
            $putAssetPermission,
            $postAssetPermission,
            $deleteAssetPermission,
            $getContentToSelectPermission,
            $getContentPermission,
            $getContentesPermission,
            $putContentPermission,
            $postContentPermission,
            $deleteContentPermission,
            $updateContentPowersPermission,
            $deletePowerContentPermission,
            $updateContentProductsPermission,
            $deleteProductContentPermission,
            $updateContentPlayareasPermission,
            $deletePlayareaContentPermission,
            $updateContentTagsPermission,
            $deleteTagContentPermission,
            $createContentAssetsPermission,
            $updateContentAssetsPermission,
            $deleteAssetContentPermission,
            $updateContentPlaycircuitsPermission,
            $deletePlaycircuitContentPermission,
            $putPlayCircuitTagsPermission,
            $deleteTagPlayCircuitPermission,
            $putPlayCircuitCountriesPermission,
            $deleteCountryPlayCircuitPermission,
            $putPlayCircuitProvincesPermission,
            $deleteProvincePlayCircuitPermission,
            $putPlayCircuitCitiesPermission,
            $deleteCityPlayCircuitPermission,
            $postPlayersToAnalyticsPermission,
            $postContentToEmissionPermission,
            $postCamerasToAnalyticsPermission,
            $postPlayerPermission,
            $deletePlayerPermission,
            $getPlayerToSelectPermission,
            $putPlayCircuitSitesPermission,
            $deleteSitePlayCircuitPermission,
            $updatePlayAreaPlayLogicsPermission,
            $deletePlayLogicPlayAreaPermission,
            $updateContentOrderPermission,
            $putPlayCircuitLangsPermission,
            $deleteLangPlayCircuitPermission,
            $getPlayAreaToUserPermission,
            $getLangsToUserPermission,
            $getFormatsToUserPermission,
            $updatePlayerPlayLogicsPermission,
            $deletePlayerPlayLogicPermission,
            $updatePlayerContentsPermission,
            $updatePlayAreaContentsPermission,
            $getContentsOrderPermission,
            $deleteCustomerFormatPermission,
            $putSiteHolidaysPermission,
            $deleteSiteHolidaytPermission,
            $updatePlayerPowersPermission,
            $deletePlayerPowerPermission,
            $updateCameraPowerPermission,
            $deleteCameraPowerPermission,
            $updateCustomerFormatsPermission,
            $deletePlayAreaForcePermission,
            $deleteIndoorLocationForcePermission,
            $deleteSitePermission,
            $deleteAssetForcePermission,
            $getProductsToSelectPermission,
            $getProductsPermission,
            $getProductPermission,
            $putProductPermission,
            $postProductPermission,
            $deleteProductPermission
        );
        
        $admin = Role::create(['name' => 'admin', 'guard_name' => 'api']);
        $admin->givePermissionTo(
            $getAppToSelectPermission,
            $getCustomerPermission,
            $getSitesPermission,
            $getSitePermission,
            $putSitePermission,
            $getPlayersPermission, 
            $getPlayerPermission,
            $putPlayerPermission,
            $getTagsPermission, 
            $getTagPermission,
            $putTagPermission,
            $getCamerasPermission,
            $getCameraPermission,
            $getCamerasToSelectPermission,
            $putCameraPermission,
            $getPlayAreasPermission,
            $getPlayAreaPermission,
            $getPlayAreaToSelectPermission,
            $getFormatsPermission,
            $getFormatPermission,
            $postFormatsPermission,
            $postFormatPermission,
            $deleteFormatPermission,
            $getFormatToSelectPermission,
            $getLangPermission,
            $getLangsToSelectPermission,
            $getIndoorLocationPermission,
            $getIndoorLocationsPermission,
            $putIndoorLocationPermission,
            $postIndoorLocationPermission,
            $deleteIndoorLocationPermission,
            $getIndoorLocationToSelectPermission,
            $getPlayCircuitsPermission,
            $getPlayCircuitsToSelectPermission,
            $getPlayCircuitPermission,
            $putPlayCircuitPermission,
            $postPlayCircuitPermission,
            $deletePlayCircuitPermission,
            $getTagCategoriesPermission,
            $getTagCategoryPermission,
            $putTagCategoryPermission,
            $postTagCategoryPermission,
            $deleteTagCategoryPermission,
            $postTagPermission,
            $deleteTagPermission,
            $getCountryToSelectPermission,
            $getProvinceToSelectPermission,
            $getCityToSelectPermission,
            $getContentTypeToSelectPermission,
            $getContentCategoryToSelectPermission,
            $getContentCategoryPermission,
            $getContentCategoriesPermission,
            $putContentCategoryPermission,
            $postContentCategoryPermission,
            $deleteContentCategoryPermission,
            $getAssetTypeToSelectPermission,
            $getTagsToSelectPermission,
            $getTagCategoriesToSelectPermission,
            $getAssetsToSelectPermission,
            $getAssetsPermission,
            $getAssetPermission,
            $putAssetPermission,
            $postAssetPermission,
            $deleteAssetPermission,
            $getContentToSelectPermission,
            $getContentPermission,
            $getContentesPermission,
            $putContentPermission,
            $postContentPermission,
            $deleteContentPermission,
            $updateContentPowersPermission,
            $deletePowerContentPermission,
            $updateContentProductsPermission,
            $deleteProductContentPermission,
            $updateContentPlayareasPermission,
            $deletePlayareaContentPermission,
            $updateContentTagsPermission,
            $deleteTagContentPermission,
            $createContentAssetsPermission,
            $updateContentAssetsPermission,
            $deleteAssetContentPermission,
            $getCustomerToSelectPermission,
            $getPlayLogicsToSelectPermission,
            $getPlayLogicsPermission,
            $getPlayLogicPermission,
            $putPlayLogicPermission,
            $postPlayLogicPermission,
            $deletePlayLogicPermission,
            $updateContentPlaycircuitsPermission,
            $deletePlaycircuitContentPermission,
            $putPlayCircuitTagsPermission,
            $deleteTagPlayCircuitPermission,
            $putPlayCircuitCountriesPermission,
            $deleteCountryPlayCircuitPermission,
            $putPlayCircuitProvincesPermission,
            $deleteProvincePlayCircuitPermission,
            $putPlayCircuitCitiesPermission,
            $deleteCityPlayCircuitPermission,
            $postPlayersToAnalyticsPermission,
            $postContentToEmissionPermission,
            $postCamerasToAnalyticsPermission,
            $postPlayerPermission,
            $deletePlayerPermission,
            $getPlayerToSelectPermission,
            $putPlayCircuitSitesPermission,
            $deleteSitePlayCircuitPermission,
            $updatePlayAreaPlayLogicsPermission,
            $deletePlayLogicPlayAreaPermission,
            $updateContentOrderPermission,
            $putPlayCircuitLangsPermission,
            $deleteLangPlayCircuitPermission,
            $getPlayAreaToUserPermission,
            $getLangsToUserPermission,
            $getFormatsToUserPermission,
            $updatePlayerPlayLogicsPermission,
            $deletePlayerPlayLogicPermission,
            $updatePlayerContentsPermission,
            $updatePlayAreaContentsPermission,
            $getContentsOrderPermission,
            $deleteCustomerFormatPermission,
            $putSiteHolidaysPermission,
            $deleteSiteHolidaytPermission,
            $updatePlayerPowersPermission,
            $deletePlayerPowerPermission,
            $updateCameraPowerPermission,
            $deleteCameraPowerPermission,
            $updateCustomerFormatsPermission,
            $deletePlayAreaForcePermission,
            $deleteIndoorLocationForcePermission,
            $deleteSitePermission,
            $deleteAssetForcePermission,
            $getProductsToSelectPermission,
            $getProductsPermission,
            $getProductPermission,
            $putProductPermission,
            $postProductPermission,
            $deleteProductPermission
        );
        
        $user = Role::create(['name' => 'user', 'guard_name' => 'api']);
        $user->givePermissionTo(
            $getCustomerPermission,
            $getSitesPermission,
            $getSitePermission,
            $putSitePermission,
            $getPlayersPermission, 
            $getPlayerPermission,
            $getTagPermission,
            $getCamerasPermission,
            $getCameraPermission,
            $getCamerasToSelectPermission,
            $putCameraPermission,
            $getPlayAreaPermission,
            $getPlayAreaToSelectPermission,
            $getFormatsPermission,
            $getFormatPermission,
            $getFormatToSelectPermission,
            $getLangPermission,
            $getLangsToSelectPermission,
            $getIndoorLocationToSelectPermission,
            $getIndoorLocationPermission,
            $getPlayCircuitsPermission,
            $getPlayCircuitsToSelectPermission,
            $getPlayCircuitPermission,
            $putPlayCircuitPermission,
            $postPlayCircuitPermission,
            $deletePlayCircuitPermission,
            $getTagCategoryPermission,
            $getCountryToSelectPermission,
            $getProvinceToSelectPermission,
            $getCityToSelectPermission,
            $getContentTypeToSelectPermission,
            $getContentCategoryToSelectPermission,
            $getContentCategoryPermission,
            $getAssetTypeToSelectPermission,
            $getTagsToSelectPermission,
            $getTagCategoriesToSelectPermission,
            $getAssetsToSelectPermission,
            $getAssetsPermission,
            $getAssetPermission,
            $putAssetPermission,
            $postAssetPermission,
            $deleteAssetPermission,
            $getContentToSelectPermission,
            $getContentPermission,
            $getContentesPermission,
            $putContentPermission,
            $postContentPermission,
            $deleteContentPermission,
            $updateContentPowersPermission,
            $deletePowerContentPermission,
            $updateContentProductsPermission,
            $deleteProductContentPermission,
            $updateContentPlayareasPermission,
            $deletePlayareaContentPermission,
            $updateContentTagsPermission,
            $deleteTagContentPermission,
            $createContentAssetsPermission,
            $updateContentAssetsPermission,
            $deleteAssetContentPermission,
            $getCustomerToSelectPermission,
            $getPlayLogicsToSelectPermission,
            $getPlayLogicPermission,
            $updateContentPlaycircuitsPermission,
            $deletePlaycircuitContentPermission,
            $putPlayCircuitTagsPermission,
            $deleteTagPlayCircuitPermission,
            $putPlayCircuitCountriesPermission,
            $deleteCountryPlayCircuitPermission,
            $putPlayCircuitProvincesPermission,
            $deleteProvincePlayCircuitPermission,
            $putPlayCircuitCitiesPermission,
            $deleteCityPlayCircuitPermission,
            $postPlayersToAnalyticsPermission,
            $postContentToEmissionPermission,
            $postCamerasToAnalyticsPermission,
            $getPlayerToSelectPermission,
            $putPlayCircuitSitesPermission,
            $deleteSitePlayCircuitPermission,
            $getPlayAreaToUserPermission,
            $getLangsToUserPermission,
            $getFormatsToUserPermission,
            $getContentsOrderPermission,
            $putSiteHolidaysPermission,
            $deleteSiteHolidaytPermission,
            $updatePlayerPowersPermission,
            $deletePlayerPowerPermission,
            $updateCameraPowerPermission,
            $deleteCameraPowerPermission,
            $updateCustomerFormatsPermission,
            $deletePlayAreaForcePermission,
            $deleteIndoorLocationForcePermission,
            $deleteAssetForcePermission,
        );
    }
}
