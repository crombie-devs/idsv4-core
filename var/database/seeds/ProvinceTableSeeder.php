<?php

use Illuminate\Database\Seeder;
use App\Models\Province;
class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Province::class, 100)->create();
    }
}
