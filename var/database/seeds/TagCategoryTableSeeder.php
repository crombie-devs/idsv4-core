<?php

use Illuminate\Database\Seeder;
use App\Models\TagCategory;

class TagCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TagCategory::class, 20)->create();
    }
}
