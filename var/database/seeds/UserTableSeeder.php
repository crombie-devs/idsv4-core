<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Permission;
use App\Models\Site;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();
        $customers = Customer::all()->pluck('idcustomer')->toArray();
        $superadmin = new User();
        $superadmin->name = 'superadmin';
        $superadmin->email = 'superadmin@ladorian.com';
        $superadmin->password = bcrypt('1234');
        $superadmin->save();
        $superadmin->assignRole('superadmin');

        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@ladorian.com';
        $admin->idcustomer = $customers[0];
        $admin->password = bcrypt('1234');
        $admin->save();
        $admin->assignRole('admin');

        $sites = Site::where('idcustomer', $customers[1])->pluck('idcustomer')->toArray();
        $user = new User();
        $user->name = 'user';
        $user->idcustomer = $customers[1];
        $user->idsite = $sites[1];
        $user->email = 'user@ladorian.com';
        $user->password = bcrypt('1234');
        $user->save();
        $user->assignRole('user');
    }   
}
