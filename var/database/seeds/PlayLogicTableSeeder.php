<?php

use Illuminate\Database\Seeder;
use App\Models\PlayLogic;


class PlayLogicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PlayLogic::class, 200)->create();
    }
}
