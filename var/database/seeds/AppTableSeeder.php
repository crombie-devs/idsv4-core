<?php

use Illuminate\Database\Seeder;
use App\Models\App;
use App\Models\Customer;

class AppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App::class, 20)->create();
    }
}
