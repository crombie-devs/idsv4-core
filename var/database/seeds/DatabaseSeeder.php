<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CustomerTableSeeder::class,
            RoleTableSeeder::class,
            AppTableSeeder::class,
            SiteTableSeeder::class,
            SiteHolidayTableSeeder::class,
            UserTableSeeder::class,
            TagCategoryTableSeeder::class,
            PlayAreaTableSeeder::class,
            PlayerTableSeeder::class,
            PlayerPowerTableSeeder::class,
            CameraTableSeeder::class,
            CameraPowerTableSeeder::class,
            IndoorLocationTableSeeder::class,
            LangTableSeeder::class,
            CountryTableSeeder::class,
            ProvinceTableSeeder::class,
            CityTableSeeder::class,
            PlayCircuitTableSeeder::class,
            TagTableSeeder::class,
            ContentTypeTableSeeder::class,
            ContentCategoryTableSeeder::class,
            AssetTypeTableSeeder::class,
            AssetTableSeeder::class,
            ContentTableSeeder::class,
            PlayLogicTableSeeder::class,
  
            ]);
    }
}
