<?php

use Illuminate\Database\Seeder;
use App\Models\PlayArea;
use Faker\Generator as Faker;


class PlayAreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PlayArea::class, 200)->create();
    }
}
