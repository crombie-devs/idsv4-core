<?php

use Illuminate\Database\Seeder;
use App\Models\CameraPower;
use Faker\Generator as Faker;


class CameraPowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CameraPower::class, 200)->create();
    }
}
