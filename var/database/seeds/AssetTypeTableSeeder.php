<?php

use Illuminate\Database\Seeder;
use App\Models\AssetType;
class AssetTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $assetType = new AssetType();
        $assetType->name = 'Imagen';
        $assetType->save();

        $assetType = new AssetType();
        $assetType->name = 'Video';
        $assetType->save();

        $assetType = new AssetType();
        $assetType->name = 'Url';
        $assetType->save();

    }
}
