<?php

use Illuminate\Database\Seeder;
use App\Models\Camera;
use App\Models\TagCategory;
use App\Models\Tag;
use App\Models\Site;
use Faker\Generator as Faker;


class CameraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Camera::class, 50)->create();
    }
}
