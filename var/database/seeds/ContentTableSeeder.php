<?php

use Illuminate\Database\Seeder;
use App\Models\Content;


class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Content::class, 200)->create();
    }
}
