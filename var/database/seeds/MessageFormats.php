<?php

use App\Models\MessageFormats;
use Illuminate\Database\Seeder;




class PlayAreaTableSeeder extends Seeder
{

    public function run()
    {

        $data = array(
            [
                'idformat' => 1,
                'name' => 'sms'
            ],
            [
                'idformat' => 2,
                'name' => 'whatsapp'
            ],
            [
                'idformat' => 3,
                'name' => 'email'
            ]

        );

        MessageFormats::create($data);
    }
}
