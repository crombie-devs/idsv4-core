<?php

use Illuminate\Database\Seeder;
use App\Models\ContentCategory;


class ContentCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ContentCategory::class, 50)->create();
    }
}
