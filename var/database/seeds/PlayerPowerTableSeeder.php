<?php

use Illuminate\Database\Seeder;
use App\Models\PlayerPower;
use Faker\Generator as Faker;


class PlayerPowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PlayerPower::class, 200)->create();
    }
}
