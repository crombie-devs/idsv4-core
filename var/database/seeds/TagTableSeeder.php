<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\User;
use Spatie\Permission\Models\Permission;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, 100)->create();
    }
}
