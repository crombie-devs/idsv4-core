<?php

use Illuminate\Database\Seeder;
use App\Models\SiteHoliday;
use Spatie\Permission\Models\Permission;

class SiteHolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SiteHoliday::class, 200)->create();
    }
}
