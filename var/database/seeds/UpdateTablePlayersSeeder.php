<?php

use Illuminate\Database\Seeder;
use App\Models\Player;
use App\Models\Channel;


class UpdateTablePlayersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = Channel::all();
        $players = Player::all();
        foreach ($players as $key => $pl) {
           $channel = Channel::where('idchannel',$pl->idchannel)->first();
           $play = Player::find($pl->idplayer);
           $play['idsite']= $channel->idsite;
           $play->save();
        }
    }
}
