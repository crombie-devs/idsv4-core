<?php

use Illuminate\Database\Seeder;
use App\Models\Player;

use App\Models\TagCategory;
use App\Models\Tag;
use App\Models\Site;
use Faker\Generator as Faker;


class PlayerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Player::class, 50)->create()->each(function($player){
            $faker = \Faker\Factory::create();
            $sites = Site::all()->pluck('idsite')->toArray();
            $idsite = $faker->randomElement($sites);
            $site = Site::find($idsite);
            $categories = TagCategory::where('idcustomer', $site->idcustomer)->orWhere('idcustomer', null)->get()->pluck('idcategory')->toArray();
            $tags = Tag::whereIn('idcategory', $categories)->get();
            foreach($tags as $tag){
             //   $player->tags()->attach($tag->idtag);
            }
        });
    }
}
