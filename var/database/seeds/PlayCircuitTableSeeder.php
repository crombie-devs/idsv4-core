<?php

use Illuminate\Database\Seeder;
use App\Models\PlayCircuit;

class PlayCircuitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PlayCircuit::class, 10)->create();
      
        for ($i=0; $i < 10; $i++) { 
            $c_cities = DB::table('cities')->inRandomOrder()->first();
            $c_provinces= DB::table('provinces')->where('idprovince',$c_cities->idprovince)->first();
            $c_playcircuit = DB::table('play_circuits')->inRandomOrder()->first();

            //$res_circuits_has_countries = DB::table('circuits_has_countries')->where('idcircuit',$c_playcircuit->idcircuit)->where('idcountry',$c_provinces->idcountry)->get();
           $res_circuits_has_countries = DB::table('circuits_has_countries')->where('idcircuit',$c_playcircuit->idcircuit)->where('idcountry',$c_provinces->idcountry)->get()->isEmpty();
           $res_circuits_has_provinces = DB::table('circuits_has_provinces')->where('idcircuit',$c_playcircuit->idcircuit)->where('idprovince',$c_cities->idprovince)->get()->isEmpty();
           $res_circuits_has_cities = DB::table('circuits_has_cities')->where('idcircuit',$c_playcircuit->idcircuit)->where('idcity',$c_cities->idcity)->get()->isEmpty();    
            
           if($res_circuits_has_countries && $res_circuits_has_provinces && $res_circuits_has_cities){
                DB::table('circuits_has_countries')->insert([
                    'idcircuit'=>$c_playcircuit->idcircuit,
                    'idcountry'=>$c_provinces->idcountry,
                ]);
          
                DB::table('circuits_has_provinces')->insert([
                    'idcircuit'=>$c_playcircuit->idcircuit,
                    'idprovince'=>$c_cities->idprovince,
                ]);

                DB::table('circuits_has_cities')->insert([
                    'idcircuit'=>$c_playcircuit->idcircuit,
                    'idcity'=>$c_cities->idcity,
                ]);
            }
        }
    }
}
