<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class LangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lang = new Lang();
        $lang->name = 'Español';
        $lang->image_url= public_path('images/assets/flags/24/Spain.png');
        $lang->save();

        $lang = new Lang();
        $lang->name = 'Ingles';
        $lang->image_url= public_path('images/assets/flags/24/United Kingdom(Great Britain).png');
        $lang->save();

        $lang = new Lang();
        $lang->name = 'Italiano';
        $lang->image_url= public_path('images/assets/flags/24/Italy.png');
        $lang->save();

        $lang = new Lang();
        $lang->name = 'Frances';
        $lang->image_url= public_path('images/assets/flags/24 France.png');
        $lang->save();

        $lang = new Lang();
        $lang->name = 'Catalan';
        $lang->image_url= public_path('images/assets/flags/24/Catalonia.png');
        $lang->save();
    }
}
