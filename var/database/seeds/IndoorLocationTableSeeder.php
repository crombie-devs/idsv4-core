<?php

use Illuminate\Database\Seeder;
use App\Models\IndoorLocation;
use App\Models\User;
use Spatie\Permission\Models\Permission;

class IndoorLocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(IndoorLocation::class, 100)->create();
    }
}
