<?php

use Illuminate\Database\Seeder;
use App\Models\ContentType;
class ContentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ContentType::class, 50)->create();
    }
}
