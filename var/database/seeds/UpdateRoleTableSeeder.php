<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UpdateRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $deleteAssetForcePermission = Permission::create(['name' =>'delete.asset.*.force', 'guard_name' => 'api']);

      $superadmin = Role::where('name','superadmin')->first();
      $superadmin->givePermissionTo(
        $deleteAssetForcePermission
      );
        
      $admin = Role::where('name','admin')->first();
      $admin->givePermissionTo(
        $deleteAssetForcePermission
      );

      $user = Role::where('name','user')->first();
      $user->givePermissionTo(
        $deleteAssetForcePermission
      );

    }
}
