<?php

use Illuminate\Database\Seeder;
use App\Models\Site;
use App\Models\Customer;
use App\Models\App;
use Spatie\Permission\Models\Permission;

class SiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Site::class, 50)->create()->each(function($site){
            $apps = App::where('idcustomer', null)->orWhere('idcustomer', $site->idcustomer)->get();
            foreach($apps as $app){
                $site->apps()->attach($app->idapp);
            }
        });
    }
}
