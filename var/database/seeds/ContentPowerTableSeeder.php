<?php

use App\Models\Content;
use App\Models\ContentPower;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ContentPowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = Content::all()->pluck('idcontent');
        foreach($content as $conte){
            $random = Carbon::now()->subHours(rand(0, 23));
            $timeon = $random->format('H');
            $tmttime= 23-$timeon;
            $rndtime = rand(0, $tmttime);
            $dt = Carbon::now();
            $dt->hour   = $timeon;
            $timeoff=  $dt->addHours($rndtime)->format('H');

            for ($weekday=1; $weekday < 8; $weekday++) { 
                $contentPower = new contentPower();
                $contentPower->idcontent = $conte;
                $contentPower->weekday = $weekday;
                $contentPower->time_on = $timeon . ':00:00';
                $contentPower->time_off = $timeoff. ':59:00';
                $contentPower->save();
            }
        }
    }
}
