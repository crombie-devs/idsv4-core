<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerEmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_emissions', function (Blueprint $table) {
            $table->foreignId('idplayer')->references('idplayer')->on('players')->onDelete('cascade');
            $table->integer('weekday');
            $table->integer('num_loop')->default(0);
            $table->integer('total_emissions')->default(0);
            $table->timestamps();
            $table->unique('idplayer', 'weekday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_emissions');
    }
}
