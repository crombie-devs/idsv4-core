<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleHasTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_has_tags', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idpeople')->references('id')->on('peoples');  
            $table->foreignId('idtag')->references('idtag')->on('message_tags'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_has_tags');
    }
}
