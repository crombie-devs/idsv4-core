<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idcustomer')->references('idcustomer')->on('customers');  
            $table->foreignId('idsite')->references('idsite')->on('sites');
            $table->foreignId('idformat')->references('idformat')->on('formats'); 
            $table->string('subject');
            $table->text('body');
            $table->integer('num_sends');
            $table->date('datetime');
            $table->integer('num_received');
            $table->integer('num_opened');
            $table->integer('status');
            $table->integer('is_sync');
            $table->date('created_at');
            $table->date('updated_at');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_message');
    }
}
