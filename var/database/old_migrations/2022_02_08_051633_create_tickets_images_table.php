<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets_images', function (Blueprint $table) {

            $table->id();

            $table->text('url');
            $table->foreignId('id_ticket')->references('id')->on('tickets');
            $table->foreignId('id_comment')->references('id')->on('ticket_comments');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_images');
    }
}
