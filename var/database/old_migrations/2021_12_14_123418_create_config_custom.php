<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigCustom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_customer', function (Blueprint $table) {
            $table->id();
            $table->string('sms_apikey')->default(null);
            $table->string('mailing_apikey')->default(null);;
            $table->string('whatsapp_apikey')->default(null);;
            $table->foreignId('idcustomer')->references('idcustomer')->on('customers');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_custom');
    }
}
