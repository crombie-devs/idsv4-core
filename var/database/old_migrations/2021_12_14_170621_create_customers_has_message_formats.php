<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersHasMessageFormats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_has_message_formats', function (Blueprint $table) {
            $table->foreignId('idcustomer')->references('idcustomer')->on('customers');  
            $table->foreignId('idformat')->references('idformat')->on('message_formats'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_has_message_formats');
    }
}
