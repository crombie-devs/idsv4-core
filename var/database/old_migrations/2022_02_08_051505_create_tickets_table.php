<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            
            $table->id();
            
            $table->integer('phone')->length(8)->nullable();
            $table->string('title')->length(191);
            $table->string('email')->nullable();
            $table->string('point_address')->nullable();
            $table->text('description')->nullable();

            $table->foreignId('id_priority')->references('id')->on('ticket_priorities');
            $table->foreignId('id_category')->references('id')->on('ticket_categories');
            $table->foreignId('id_department')->references('id')->on('ticket_departments')->nullable();
            $table->foreignId('id_responsible')->references('id')->on('ticket_responsibles')->nullable();
            $table->foreignId('id_site')->references('idsite')->on('sites');
            $table->foreignId('id_status')->references('id')->on('ticket_status');
            $table->foreignId('id_customer')->references('idcustomer')->on('users'); // id_customer.
            $table->foreignId('id_user')->references('iduser')->on('users')->nullable(); // id del usuario.

            $table->boolean('finished')->default(false);
            $table->boolean('deleted')->default(false);
            $table->boolean('accept')->default(false);

            $table->boolean('expected_resolution_date')->default(false);
            $table->boolean('notify_user')->default(false);

            $table->date('date_alert');
            $table->date('created_at');
            $table->date('updated_at');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
