-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: ladorian
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apps` (
  `idapp` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apikey` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idapp`),
  KEY `apps_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `apps_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps`
--

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;
INSERT INTO `apps` VALUES (1,4,'Gloria Oquendo','et',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(2,7,'Claudia Arias','sed',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(3,1,'Biel Navarro','eaque',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(4,2,'Guillermo Gaytán','in',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(5,10,'Eduardo Domingo','nemo',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(6,6,'Aurora Riojas Tercero','laudantium',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(7,1,'Diana Soto','ducimus',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(8,1,'Dña Aitana Jaramillo Segundo','vel',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(9,1,'Srta. Paula Moral Segundo','quaerat',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(10,6,'Salma Barrientos Tercero','distinctio',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(11,5,'Victoria Alejandro','qui',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(12,1,'Martina Mares','est',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(13,NULL,'Ing. Carlos Gaitán Hijo','reprehenderit',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(14,2,'Alejandro Serra','eos',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(15,6,'Luis Luque','magnam',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(16,7,'Raquel Escalante Tercero','aspernatur',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(17,10,'Andrea Sancho','nostrum',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(18,4,'Rafael Tejada Segundo','officiis',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(19,6,'Asier Galván','enim',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL),(20,4,'Raquel Guzmán','quae',1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22',NULL);
/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_types`
--

DROP TABLE IF EXISTS `asset_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asset_types` (
  `idtype` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idtype`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_types`
--

LOCK TABLES `asset_types` WRITE;
/*!40000 ALTER TABLE `asset_types` DISABLE KEYS */;
INSERT INTO `asset_types` VALUES (1,'Imagen'),(2,'Video');
/*!40000 ALTER TABLE `asset_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assets` (
  `idasset` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `idsite` bigint(20) unsigned NOT NULL,
  `idtype` bigint(20) unsigned NOT NULL,
  `asset_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '30',
  `idformat` bigint(20) unsigned NOT NULL,
  `idlang` bigint(20) unsigned NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `has_audio` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idasset`),
  KEY `assets_idcustomer_foreign` (`idcustomer`),
  KEY `assets_idsite_foreign` (`idsite`),
  KEY `assets_idtype_foreign` (`idtype`),
  KEY `assets_idformat_foreign` (`idformat`),
  KEY `assets_idlang_foreign` (`idlang`),
  CONSTRAINT `assets_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`),
  CONSTRAINT `assets_idformat_foreign` FOREIGN KEY (`idformat`) REFERENCES `formats` (`idformat`),
  CONSTRAINT `assets_idlang_foreign` FOREIGN KEY (`idlang`) REFERENCES `langs` (`idlang`),
  CONSTRAINT `assets_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`),
  CONSTRAINT `assets_idtype_foreign` FOREIGN KEY (`idtype`) REFERENCES `asset_types` (`idtype`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (1,2,3,1,'https://images.theconversation.com/files/134652/original/image-20160818-12312-4dyz0u.jpg','https://images.theconversation.com/files/134652/original/image-20160818-12312-4dyz0u.jpg',30,35,1,0,0,1,0,'2020-09-24 13:24:49','2020-09-24 13:24:49'),(2,2,3,1,'https://images.theconversation.com/files/217598/original/file-20180503-153884-17cuab8.jpg','https://images.theconversation.com/files/217598/original/file-20180503-153884-17cuab8.jpg',30,35,1,0,0,1,0,'2020-09-24 13:24:49','2020-09-24 13:24:49');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camera_powers`
--

DROP TABLE IF EXISTS `camera_powers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `camera_powers` (
  `idpower` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcamera` bigint(20) unsigned NOT NULL,
  `weekday` int(11) NOT NULL,
  `time_on` time NOT NULL,
  `time_off` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpower`),
  KEY `camera_powers_idcamera_foreign` (`idcamera`),
  CONSTRAINT `camera_powers_idcamera_foreign` FOREIGN KEY (`idcamera`) REFERENCES `cameras` (`idcamera`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camera_powers`
--

LOCK TABLES `camera_powers` WRITE;
/*!40000 ALTER TABLE `camera_powers` DISABLE KEYS */;
INSERT INTO `camera_powers` VALUES (1,38,3,'19:04:25','16:12:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(2,46,7,'18:26:59','19:02:35','2020-09-09 10:47:36','2020-09-09 10:47:36'),(3,25,3,'19:46:45','00:37:22','2020-09-09 10:47:36','2020-09-09 10:47:36'),(4,12,3,'06:33:46','09:18:51','2020-09-09 10:47:36','2020-09-09 10:47:36'),(5,29,5,'20:56:23','20:07:57','2020-09-09 10:47:36','2020-09-09 10:47:36'),(6,19,6,'17:03:05','00:51:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(7,25,3,'16:07:29','01:06:42','2020-09-09 10:47:36','2020-09-09 10:47:36'),(8,2,5,'05:53:34','19:49:09','2020-09-09 10:47:36','2020-09-09 10:47:36'),(9,40,2,'21:10:04','12:08:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(10,12,5,'06:52:11','11:07:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(11,27,5,'02:13:55','09:30:15','2020-09-09 10:47:36','2020-09-09 10:47:36'),(12,11,7,'10:01:10','00:08:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(13,32,3,'01:02:54','04:18:57','2020-09-09 10:47:36','2020-09-09 10:47:36'),(14,4,5,'16:29:47','14:20:48','2020-09-09 10:47:36','2020-09-09 10:47:36'),(15,20,1,'03:17:15','19:46:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(16,27,7,'02:59:14','06:49:39','2020-09-09 10:47:36','2020-09-09 10:47:36'),(17,9,6,'23:45:56','20:14:15','2020-09-09 10:47:36','2020-09-09 10:47:36'),(18,29,6,'09:16:38','09:13:02','2020-09-09 10:47:36','2020-09-09 10:47:36'),(19,49,7,'22:36:43','15:17:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(20,26,5,'02:32:02','20:23:29','2020-09-09 10:47:36','2020-09-09 10:47:36'),(21,14,6,'22:09:02','21:42:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(22,34,4,'21:58:48','04:14:15','2020-09-09 10:47:36','2020-09-09 10:47:36'),(23,11,2,'22:47:54','00:17:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(24,42,1,'06:39:23','15:33:18','2020-09-09 10:47:36','2020-09-09 10:47:36'),(25,25,3,'12:50:12','10:21:24','2020-09-09 10:47:36','2020-09-09 10:47:36'),(26,36,6,'10:38:14','19:50:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(27,14,5,'10:08:08','16:26:54','2020-09-09 10:47:36','2020-09-09 10:47:36'),(28,35,5,'19:36:11','03:41:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(29,36,4,'19:53:22','11:42:19','2020-09-09 10:47:36','2020-09-09 10:47:36'),(30,49,4,'13:46:32','01:36:46','2020-09-09 10:47:36','2020-09-09 10:47:36'),(31,8,7,'15:08:24','15:47:50','2020-09-09 10:47:36','2020-09-09 10:47:36'),(32,49,1,'05:30:18','20:05:14','2020-09-09 10:47:36','2020-09-09 10:47:36'),(33,10,5,'05:05:35','23:34:37','2020-09-09 10:47:36','2020-09-09 10:47:36'),(34,29,6,'13:57:25','20:45:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(35,12,4,'10:44:51','20:06:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(36,33,7,'12:30:34','21:57:14','2020-09-09 10:47:36','2020-09-09 10:47:36'),(37,46,3,'18:51:35','05:13:11','2020-09-09 10:47:36','2020-09-09 10:47:36'),(38,41,2,'04:17:31','19:46:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(39,47,4,'12:49:14','16:24:07','2020-09-09 10:47:36','2020-09-09 10:47:36'),(40,23,2,'20:11:20','23:11:54','2020-09-09 10:47:36','2020-09-09 10:47:36'),(41,36,3,'08:41:36','18:08:50','2020-09-09 10:47:36','2020-09-09 10:47:36'),(42,10,7,'02:14:16','12:48:32','2020-09-09 10:47:36','2020-09-09 10:47:36'),(43,4,4,'04:35:12','00:30:57','2020-09-09 10:47:36','2020-09-09 10:47:36'),(44,43,5,'17:45:00','08:35:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(45,14,2,'18:10:32','12:40:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(46,6,4,'02:50:55','01:11:30','2020-09-09 10:47:36','2020-09-09 10:47:36'),(47,32,6,'23:54:11','03:21:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(48,4,7,'11:40:58','03:37:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(49,8,4,'22:13:43','23:39:01','2020-09-09 10:47:36','2020-09-09 10:47:36'),(50,7,1,'14:39:25','05:40:37','2020-09-09 10:47:36','2020-09-09 10:47:36'),(51,14,5,'00:02:02','04:57:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(52,36,6,'14:44:23','13:28:58','2020-09-09 10:47:36','2020-09-09 10:47:36'),(53,20,2,'08:25:26','07:15:09','2020-09-09 10:47:36','2020-09-09 10:47:36'),(54,4,7,'16:02:57','15:19:35','2020-09-09 10:47:36','2020-09-09 10:47:36'),(55,29,7,'21:06:17','02:53:58','2020-09-09 10:47:36','2020-09-09 10:47:36'),(56,9,3,'06:39:45','04:10:29','2020-09-09 10:47:36','2020-09-09 10:47:36'),(57,37,5,'21:29:18','21:08:43','2020-09-09 10:47:36','2020-09-09 10:47:36'),(58,15,2,'12:52:16','12:53:19','2020-09-09 10:47:36','2020-09-09 10:47:36'),(59,40,1,'13:27:34','15:51:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(60,8,2,'12:31:41','13:25:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(61,10,6,'22:30:57','14:01:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(62,30,6,'02:40:50','10:07:53','2020-09-09 10:47:36','2020-09-09 10:47:36'),(63,6,3,'07:41:31','15:56:49','2020-09-09 10:47:36','2020-09-09 10:47:36'),(64,8,6,'18:15:40','06:22:41','2020-09-09 10:47:36','2020-09-09 10:47:36'),(65,12,2,'23:27:39','17:03:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(66,15,7,'04:22:55','04:24:20','2020-09-09 10:47:36','2020-09-09 10:47:36'),(67,22,6,'03:23:09','04:36:57','2020-09-09 10:47:36','2020-09-09 10:47:36'),(68,49,2,'04:35:42','01:13:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(69,44,4,'14:17:58','15:08:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(70,39,6,'15:38:02','11:14:54','2020-09-09 10:47:36','2020-09-09 10:47:36'),(71,19,3,'19:48:56','15:24:14','2020-09-09 10:47:36','2020-09-09 10:47:36'),(72,44,3,'07:05:03','10:05:21','2020-09-09 10:47:36','2020-09-09 10:47:36'),(73,50,7,'07:22:02','13:21:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(74,40,4,'03:28:46','01:08:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(75,19,6,'08:55:02','03:47:39','2020-09-09 10:47:36','2020-09-09 10:47:36'),(76,50,6,'23:12:27','03:06:21','2020-09-09 10:47:36','2020-09-09 10:47:36'),(77,4,3,'04:43:34','03:34:19','2020-09-09 10:47:36','2020-09-09 10:47:36'),(78,39,6,'02:28:59','10:39:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(79,15,7,'13:33:09','23:10:53','2020-09-09 10:47:36','2020-09-09 10:47:36'),(80,11,2,'04:50:54','18:15:45','2020-09-09 10:47:36','2020-09-09 10:47:36'),(81,37,5,'07:50:03','02:15:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(82,6,4,'17:20:57','17:22:41','2020-09-09 10:47:36','2020-09-09 10:47:36'),(83,25,7,'23:41:02','17:39:16','2020-09-09 10:47:36','2020-09-09 10:47:36'),(84,46,1,'11:56:50','12:03:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(85,8,5,'23:23:54','20:41:58','2020-09-09 10:47:36','2020-09-09 10:47:36'),(86,38,6,'16:37:39','00:25:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(87,15,7,'04:33:39','02:02:22','2020-09-09 10:47:36','2020-09-09 10:47:36'),(88,2,4,'04:22:39','05:59:28','2020-09-09 10:47:36','2020-09-09 10:47:36'),(89,5,7,'19:33:50','20:00:43','2020-09-09 10:47:36','2020-09-09 10:47:36'),(90,20,7,'00:39:27','06:07:14','2020-09-09 10:47:36','2020-09-09 10:47:36'),(91,24,3,'22:44:02','04:49:37','2020-09-09 10:47:36','2020-09-09 10:47:36'),(92,26,6,'21:11:00','07:12:49','2020-09-09 10:47:36','2020-09-09 10:47:36'),(93,33,1,'12:43:49','06:14:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(94,21,5,'06:50:12','06:00:55','2020-09-09 10:47:36','2020-09-09 10:47:36'),(95,19,1,'05:16:36','17:31:35','2020-09-09 10:47:36','2020-09-09 10:47:36'),(96,38,6,'05:19:33','11:12:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(97,15,7,'20:30:34','17:00:02','2020-09-09 10:47:36','2020-09-09 10:47:36'),(98,3,7,'07:36:01','00:45:56','2020-09-09 10:47:36','2020-09-09 10:47:36'),(99,31,4,'07:02:47','04:14:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(100,24,6,'04:44:01','23:25:08','2020-09-09 10:47:36','2020-09-09 10:47:36'),(101,5,5,'00:24:48','22:50:16','2020-09-09 10:47:36','2020-09-09 10:47:36'),(102,27,7,'04:56:14','01:19:07','2020-09-09 10:47:36','2020-09-09 10:47:36'),(103,16,5,'08:31:32','04:12:46','2020-09-09 10:47:36','2020-09-09 10:47:36'),(104,45,4,'07:45:55','05:42:04','2020-09-09 10:47:36','2020-09-09 10:47:36'),(105,27,3,'05:12:59','23:01:55','2020-09-09 10:47:36','2020-09-09 10:47:36'),(106,28,1,'04:31:47','20:03:45','2020-09-09 10:47:36','2020-09-09 10:47:36'),(107,3,5,'18:56:18','12:10:22','2020-09-09 10:47:36','2020-09-09 10:47:36'),(108,23,6,'16:46:29','03:26:57','2020-09-09 10:47:36','2020-09-09 10:47:36'),(109,20,5,'22:21:26','12:01:18','2020-09-09 10:47:36','2020-09-09 10:47:36'),(110,10,1,'15:50:06','22:27:50','2020-09-09 10:47:36','2020-09-09 10:47:36'),(111,36,3,'18:08:23','03:28:33','2020-09-09 10:47:36','2020-09-09 10:47:36'),(112,4,6,'14:14:27','05:49:12','2020-09-09 10:47:36','2020-09-09 10:47:36'),(113,18,2,'08:48:13','10:07:55','2020-09-09 10:47:36','2020-09-09 10:47:36'),(114,35,7,'22:19:04','22:19:38','2020-09-09 10:47:36','2020-09-09 10:47:36'),(115,14,2,'17:43:22','13:34:38','2020-09-09 10:47:36','2020-09-09 10:47:36'),(116,5,4,'12:13:02','00:08:59','2020-09-09 10:47:36','2020-09-09 10:47:36'),(117,16,3,'17:04:01','03:22:39','2020-09-09 10:47:36','2020-09-09 10:47:36'),(118,7,4,'00:50:58','16:14:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(119,12,2,'14:57:20','06:00:44','2020-09-09 10:47:36','2020-09-09 10:47:36'),(120,7,4,'18:14:33','03:35:03','2020-09-09 10:47:36','2020-09-09 10:47:36'),(121,19,2,'02:42:36','05:14:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(122,33,3,'01:36:41','00:26:52','2020-09-09 10:47:36','2020-09-09 10:47:36'),(123,25,3,'21:34:42','14:52:15','2020-09-09 10:47:36','2020-09-09 10:47:36'),(124,42,6,'03:36:21','17:48:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(125,31,7,'12:38:33','12:15:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(126,1,6,'15:40:30','22:43:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(127,32,2,'23:02:08','23:57:23','2020-09-09 10:47:36','2020-09-09 10:47:36'),(128,38,7,'05:49:46','13:11:24','2020-09-09 10:47:36','2020-09-09 10:47:36'),(129,1,3,'01:59:57','04:10:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(130,24,1,'11:46:26','22:13:13','2020-09-09 10:47:36','2020-09-09 10:47:36'),(131,2,6,'23:53:17','02:42:45','2020-09-09 10:47:36','2020-09-09 10:47:36'),(132,15,7,'14:15:01','23:27:30','2020-09-09 10:47:36','2020-09-09 10:47:36'),(133,3,5,'10:54:01','23:45:32','2020-09-09 10:47:36','2020-09-09 10:47:36'),(134,10,7,'00:57:42','19:12:16','2020-09-09 10:47:36','2020-09-09 10:47:36'),(135,24,7,'19:12:57','22:21:24','2020-09-09 10:47:36','2020-09-09 10:47:36'),(136,44,4,'12:36:26','03:24:08','2020-09-09 10:47:36','2020-09-09 10:47:36'),(137,37,2,'04:03:22','12:54:55','2020-09-09 10:47:36','2020-09-09 10:47:36'),(138,17,1,'20:11:08','22:35:08','2020-09-09 10:47:36','2020-09-09 10:47:36'),(139,47,7,'23:16:21','07:33:18','2020-09-09 10:47:36','2020-09-09 10:47:36'),(140,22,2,'09:12:51','22:47:49','2020-09-09 10:47:36','2020-09-09 10:47:36'),(141,44,3,'16:11:25','02:56:12','2020-09-09 10:47:36','2020-09-09 10:47:36'),(142,5,4,'20:03:31','05:00:24','2020-09-09 10:47:36','2020-09-09 10:47:36'),(143,19,1,'02:23:18','06:54:25','2020-09-09 10:47:36','2020-09-09 10:47:36'),(144,9,6,'17:46:36','14:28:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(145,30,1,'10:55:45','01:51:15','2020-09-09 10:47:36','2020-09-09 10:47:36'),(146,16,7,'00:06:58','10:06:21','2020-09-09 10:47:36','2020-09-09 10:47:36'),(147,32,7,'13:02:52','13:32:39','2020-09-09 10:47:36','2020-09-09 10:47:36'),(148,15,1,'15:28:39','15:39:09','2020-09-09 10:47:36','2020-09-09 10:47:36'),(149,30,1,'03:45:04','13:17:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(150,47,7,'08:01:41','07:08:26','2020-09-09 10:47:36','2020-09-09 10:47:36'),(151,35,5,'15:53:51','22:49:49','2020-09-09 10:47:36','2020-09-09 10:47:36'),(152,42,6,'11:42:42','20:42:29','2020-09-09 10:47:36','2020-09-09 10:47:36'),(153,42,3,'08:13:31','06:49:28','2020-09-09 10:47:36','2020-09-09 10:47:36'),(154,37,1,'10:00:15','10:37:36','2020-09-09 10:47:36','2020-09-09 10:47:36'),(155,13,1,'14:12:58','09:28:17','2020-09-09 10:47:36','2020-09-09 10:47:36'),(156,42,2,'18:37:27','01:58:44','2020-09-09 10:47:36','2020-09-09 10:47:36'),(157,44,1,'11:40:52','06:47:18','2020-09-09 10:47:36','2020-09-09 10:47:36'),(158,33,7,'18:27:17','16:38:31','2020-09-09 10:47:36','2020-09-09 10:47:36'),(159,43,5,'04:16:12','09:32:32','2020-09-09 10:47:36','2020-09-09 10:47:36'),(160,32,3,'08:29:09','17:39:32','2020-09-09 10:47:36','2020-09-09 10:47:36'),(161,2,3,'07:51:13','17:16:50','2020-09-09 10:47:36','2020-09-09 10:47:36'),(162,15,5,'06:03:49','20:32:05','2020-09-09 10:47:36','2020-09-09 10:47:36'),(163,45,3,'18:35:57','02:16:10','2020-09-09 10:47:36','2020-09-09 10:47:36'),(164,12,4,'03:59:06','11:15:30','2020-09-09 10:47:36','2020-09-09 10:47:36'),(165,46,3,'16:27:46','17:59:42','2020-09-09 10:47:36','2020-09-09 10:47:36'),(166,41,4,'01:49:06','22:56:34','2020-09-09 10:47:36','2020-09-09 10:47:36'),(167,21,5,'16:04:38','16:23:26','2020-09-09 10:47:36','2020-09-09 10:47:36'),(168,35,3,'00:55:17','10:33:10','2020-09-09 10:47:36','2020-09-09 10:47:36'),(169,45,2,'05:32:55','08:00:32','2020-09-09 10:47:36','2020-09-09 10:47:36'),(170,43,5,'16:24:22','11:41:13','2020-09-09 10:47:36','2020-09-09 10:47:36'),(171,40,3,'09:05:35','18:49:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(172,32,4,'06:03:06','14:47:40','2020-09-09 10:47:36','2020-09-09 10:47:36'),(173,8,5,'14:34:28','02:08:20','2020-09-09 10:47:36','2020-09-09 10:47:36'),(174,25,6,'12:38:39','05:35:41','2020-09-09 10:47:36','2020-09-09 10:47:36'),(175,29,3,'07:06:53','17:41:38','2020-09-09 10:47:36','2020-09-09 10:47:36'),(176,39,5,'04:20:54','14:33:12','2020-09-09 10:47:36','2020-09-09 10:47:36'),(177,17,6,'01:43:05','08:09:53','2020-09-09 10:47:36','2020-09-09 10:47:36'),(178,12,7,'16:10:04','07:37:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(179,1,3,'14:22:32','02:17:43','2020-09-09 10:47:36','2020-09-09 10:47:36'),(180,7,3,'15:51:15','07:23:41','2020-09-09 10:47:36','2020-09-09 10:47:36'),(181,48,1,'16:19:27','08:43:13','2020-09-09 10:47:36','2020-09-09 10:47:36'),(182,39,6,'07:59:28','05:58:49','2020-09-09 10:47:36','2020-09-09 10:47:36'),(183,19,5,'18:52:06','12:46:33','2020-09-09 10:47:36','2020-09-09 10:47:36'),(184,8,7,'23:40:33','20:38:38','2020-09-09 10:47:36','2020-09-09 10:47:36'),(185,32,2,'22:02:09','14:47:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(186,18,6,'16:46:41','14:03:39','2020-09-09 10:47:36','2020-09-09 10:47:36'),(187,35,1,'11:22:12','02:23:47','2020-09-09 10:47:36','2020-09-09 10:47:36'),(188,7,1,'21:59:10','07:51:37','2020-09-09 10:47:36','2020-09-09 10:47:36'),(189,37,1,'17:33:55','00:28:45','2020-09-09 10:47:36','2020-09-09 10:47:36'),(190,4,1,'06:15:14','01:50:01','2020-09-09 10:47:36','2020-09-09 10:47:36'),(191,40,7,'16:49:37','20:18:55','2020-09-09 10:47:36','2020-09-09 10:47:36'),(192,48,4,'13:03:42','02:39:02','2020-09-09 10:47:36','2020-09-09 10:47:36'),(193,41,2,'04:39:24','23:07:00','2020-09-09 10:47:36','2020-09-09 10:47:36'),(194,49,5,'09:35:41','12:53:41','2020-09-09 10:47:36','2020-09-09 10:47:36'),(195,10,6,'15:28:11','23:09:20','2020-09-09 10:47:36','2020-09-09 10:47:36'),(196,29,5,'07:45:53','19:55:59','2020-09-09 10:47:36','2020-09-09 10:47:36'),(197,3,1,'20:01:38','10:37:14','2020-09-09 10:47:36','2020-09-09 10:47:36'),(198,47,6,'12:33:15','04:39:50','2020-09-09 10:47:36','2020-09-09 10:47:36'),(199,34,5,'11:58:29','00:33:27','2020-09-09 10:47:36','2020-09-09 10:47:36'),(200,16,5,'04:25:56','02:27:38','2020-09-09 10:47:36','2020-09-09 10:47:36');
/*!40000 ALTER TABLE `camera_powers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cameras`
--

DROP TABLE IF EXISTS `cameras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cameras` (
  `idcamera` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idsite` bigint(20) unsigned NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idcamera`),
  KEY `cameras_idsite_foreign` (`idsite`),
  CONSTRAINT `cameras_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cameras`
--

LOCK TABLES `cameras` WRITE;
/*!40000 ALTER TABLE `cameras` DISABLE KEYS */;
INSERT INTO `cameras` VALUES (1,49,NULL,'reprehenderit','rosa.granados@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(2,31,NULL,'atque','lara.pozo@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(3,33,NULL,'quas','andrea.flores@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(4,1,NULL,'occaecati','yolanda41@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(5,12,NULL,'rerum','abril03@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(6,18,NULL,'reiciendis','yeray83@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(7,1,NULL,'voluptatem','pablo.naranjo@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(8,44,NULL,'ratione','ebernal@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(9,29,NULL,'magnam','zoe.clemente@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(10,29,NULL,'occaecati','yaiza01@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(11,34,NULL,'cum','lvalle@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(12,28,NULL,'quis','salma95@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(13,25,NULL,'voluptas','adam.benito@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(14,21,NULL,'nihil','bruno87@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(15,14,NULL,'omnis','pgil@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(16,38,NULL,'sit','iolvera@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(17,18,NULL,'voluptatem','vega43@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(18,27,NULL,'enim','valladares.alexandra@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(19,10,NULL,'ea','aarenas@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(20,15,NULL,'soluta','gtrevino@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(21,28,NULL,'totam','yeray.riera@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(22,35,NULL,'non','aaron.lujan@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(23,14,NULL,'eos','biel.nunez@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(24,50,NULL,'ducimus','marina.domingo@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(25,47,NULL,'quia','bermudez.carlota@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(26,2,NULL,'nam','carlos.abeyta@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(27,10,NULL,'quia','aitor64@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(28,2,NULL,'odit','marcos.rodriquez@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(29,25,NULL,'reprehenderit','mara.puga@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(30,3,NULL,'error','juan.nadia@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(31,38,NULL,'dolorum','biel.hinojosa@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(32,37,NULL,'recusandae','daniela92@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(33,47,NULL,'possimus','salma62@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(34,6,NULL,'ut','fatima.vera@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(35,3,NULL,'eaque','gonzalo.cuesta@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(36,4,NULL,'pariatur','gcastano@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(37,48,NULL,'vero','vergara.victor@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(38,18,NULL,'velit','diana19@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(39,39,NULL,'iusto','rechevarria@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(40,46,NULL,'sunt','hesquivel@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(41,14,NULL,'ut','mara.saavedra@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(42,13,NULL,'est','rafael.esquibel@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(43,8,NULL,'ab','miriam12@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(44,12,NULL,'consequatur','sonia.santana@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(45,7,NULL,'libero','escalante.jimena@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(46,22,NULL,'repudiandae','silvia77@example.com',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(47,34,NULL,'aliquid','asantacruz@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(48,21,NULL,'omnis','osorio.zoe@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(49,14,NULL,'est','eornelas@example.org',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35'),(50,37,NULL,'facere','patricia23@example.net',1,0,'2020-09-09 10:47:35','2020-09-09 10:47:35');
/*!40000 ALTER TABLE `cameras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels` (
  `idchannel` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idsite` bigint(20) unsigned NOT NULL,
  `idapp` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idchannel`),
  KEY `channels_idsite_foreign` (`idsite`),
  KEY `channels_idapp_foreign` (`idapp`),
  CONSTRAINT `channels_idapp_foreign` FOREIGN KEY (`idapp`) REFERENCES `apps` (`idapp`) ON DELETE CASCADE,
  CONSTRAINT `channels_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,1,13,NULL,NULL),(2,2,13,NULL,NULL),(3,3,3,NULL,NULL),(4,3,7,NULL,NULL),(5,3,8,NULL,NULL),(6,3,9,NULL,NULL),(7,3,12,NULL,NULL),(8,3,13,NULL,NULL),(9,4,13,NULL,NULL),(10,5,4,NULL,NULL),(11,5,14,NULL,NULL),(12,5,13,NULL,NULL),(13,6,6,NULL,NULL),(14,6,10,NULL,NULL),(15,6,15,NULL,NULL),(16,6,19,NULL,NULL),(17,6,13,NULL,NULL),(18,7,13,NULL,NULL),(19,8,6,NULL,NULL),(20,8,10,NULL,NULL),(21,8,15,NULL,NULL),(22,8,19,NULL,NULL),(23,8,13,NULL,NULL),(24,9,2,NULL,NULL),(25,9,16,NULL,NULL),(26,9,13,NULL,NULL),(27,10,13,NULL,NULL),(28,11,3,NULL,NULL),(29,11,7,NULL,NULL),(30,11,8,NULL,NULL),(31,11,9,NULL,NULL),(32,11,12,NULL,NULL),(33,11,13,NULL,NULL),(34,12,11,NULL,NULL),(35,12,13,NULL,NULL),(36,13,4,NULL,NULL),(37,13,14,NULL,NULL),(38,13,13,NULL,NULL),(39,14,2,NULL,NULL),(40,14,16,NULL,NULL),(41,14,13,NULL,NULL),(42,15,11,NULL,NULL),(43,15,13,NULL,NULL),(44,16,1,NULL,NULL),(45,16,18,NULL,NULL),(46,16,20,NULL,NULL),(47,16,13,NULL,NULL),(48,17,4,NULL,NULL),(49,17,14,NULL,NULL),(50,17,13,NULL,NULL),(51,18,1,NULL,NULL),(52,18,18,NULL,NULL),(53,18,20,NULL,NULL),(54,18,13,NULL,NULL),(55,19,13,NULL,NULL),(56,20,13,NULL,NULL),(57,21,2,NULL,NULL),(58,21,16,NULL,NULL),(59,21,13,NULL,NULL),(60,22,4,NULL,NULL),(61,22,14,NULL,NULL),(62,22,13,NULL,NULL),(63,23,3,NULL,NULL),(64,23,7,NULL,NULL),(65,23,8,NULL,NULL),(66,23,9,NULL,NULL),(67,23,12,NULL,NULL),(68,23,13,NULL,NULL),(69,24,4,NULL,NULL),(70,24,14,NULL,NULL),(71,24,13,NULL,NULL),(72,25,13,NULL,NULL),(73,26,13,NULL,NULL),(74,27,3,NULL,NULL),(75,27,7,NULL,NULL),(76,27,8,NULL,NULL),(77,27,9,NULL,NULL),(78,27,12,NULL,NULL),(79,27,13,NULL,NULL),(80,28,11,NULL,NULL),(81,28,13,NULL,NULL),(82,29,11,NULL,NULL),(83,29,13,NULL,NULL),(84,30,11,NULL,NULL),(85,30,13,NULL,NULL),(86,31,1,NULL,NULL),(87,31,18,NULL,NULL),(88,31,20,NULL,NULL),(89,31,13,NULL,NULL),(90,32,2,NULL,NULL),(91,32,16,NULL,NULL),(92,32,13,NULL,NULL),(93,33,13,NULL,NULL),(94,34,13,NULL,NULL),(95,35,11,NULL,NULL),(96,35,13,NULL,NULL),(97,36,3,NULL,NULL),(98,36,7,NULL,NULL),(99,36,8,NULL,NULL),(100,36,9,NULL,NULL),(101,36,12,NULL,NULL),(102,36,13,NULL,NULL),(103,37,5,NULL,NULL),(104,37,17,NULL,NULL),(105,37,13,NULL,NULL),(106,38,13,NULL,NULL),(107,39,3,NULL,NULL),(108,39,7,NULL,NULL),(109,39,8,NULL,NULL),(110,39,9,NULL,NULL),(111,39,12,NULL,NULL),(112,39,13,NULL,NULL),(113,40,6,NULL,NULL),(114,40,10,NULL,NULL),(115,40,15,NULL,NULL),(116,40,19,NULL,NULL),(117,40,13,NULL,NULL),(118,41,1,NULL,NULL),(119,41,18,NULL,NULL),(120,41,20,NULL,NULL),(121,41,13,NULL,NULL),(122,42,13,NULL,NULL),(123,43,13,NULL,NULL),(124,44,11,NULL,NULL),(125,44,13,NULL,NULL),(126,45,11,NULL,NULL),(127,45,13,NULL,NULL),(128,46,13,NULL,NULL),(129,47,13,NULL,NULL),(130,48,11,NULL,NULL),(131,48,13,NULL,NULL),(132,49,6,NULL,NULL),(133,49,10,NULL,NULL),(134,49,15,NULL,NULL),(135,49,19,NULL,NULL),(136,49,13,NULL,NULL),(137,50,13,NULL,NULL);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circuits_has_channels`
--

DROP TABLE IF EXISTS `circuits_has_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuits_has_channels` (
  `idcircuit` bigint(20) unsigned NOT NULL,
  `idchannel` bigint(20) unsigned NOT NULL,
  KEY `circuits_has_channels_idcircuit_foreign` (`idcircuit`),
  KEY `circuits_has_channels_idchannel_foreign` (`idchannel`),
  CONSTRAINT `circuits_has_channels_idchannel_foreign` FOREIGN KEY (`idchannel`) REFERENCES `channels` (`idchannel`),
  CONSTRAINT `circuits_has_channels_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuits_has_channels`
--

LOCK TABLES `circuits_has_channels` WRITE;
/*!40000 ALTER TABLE `circuits_has_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuits_has_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circuits_has_cities`
--

DROP TABLE IF EXISTS `circuits_has_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuits_has_cities` (
  `idcircuit` bigint(20) unsigned NOT NULL,
  `idcity` bigint(20) unsigned NOT NULL,
  KEY `circuits_has_cities_idcircuit_foreign` (`idcircuit`),
  KEY `circuits_has_cities_idcity_foreign` (`idcity`),
  CONSTRAINT `circuits_has_cities_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`),
  CONSTRAINT `circuits_has_cities_idcity_foreign` FOREIGN KEY (`idcity`) REFERENCES `cities` (`idcity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuits_has_cities`
--

LOCK TABLES `circuits_has_cities` WRITE;
/*!40000 ALTER TABLE `circuits_has_cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuits_has_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circuits_has_countries`
--

DROP TABLE IF EXISTS `circuits_has_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuits_has_countries` (
  `idcircuit` bigint(20) unsigned NOT NULL,
  `idcountry` bigint(20) unsigned NOT NULL,
  KEY `circuits_has_countries_idcircuit_foreign` (`idcircuit`),
  KEY `circuits_has_countries_idcountry_foreign` (`idcountry`),
  CONSTRAINT `circuits_has_countries_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`),
  CONSTRAINT `circuits_has_countries_idcountry_foreign` FOREIGN KEY (`idcountry`) REFERENCES `countries` (`idcountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuits_has_countries`
--

LOCK TABLES `circuits_has_countries` WRITE;
/*!40000 ALTER TABLE `circuits_has_countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuits_has_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circuits_has_provinces`
--

DROP TABLE IF EXISTS `circuits_has_provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuits_has_provinces` (
  `idcircuit` bigint(20) unsigned NOT NULL,
  `idprovince` bigint(20) unsigned NOT NULL,
  KEY `circuits_has_provinces_idcircuit_foreign` (`idcircuit`),
  KEY `circuits_has_provinces_idprovince_foreign` (`idprovince`),
  CONSTRAINT `circuits_has_provinces_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`),
  CONSTRAINT `circuits_has_provinces_idprovince_foreign` FOREIGN KEY (`idprovince`) REFERENCES `provinces` (`idprovince`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuits_has_provinces`
--

LOCK TABLES `circuits_has_provinces` WRITE;
/*!40000 ALTER TABLE `circuits_has_provinces` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuits_has_provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circuits_has_tags`
--

DROP TABLE IF EXISTS `circuits_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circuits_has_tags` (
  `idcircuit` bigint(20) unsigned NOT NULL,
  `idtag` bigint(20) unsigned NOT NULL,
  KEY `circuits_has_tags_idcircuit_foreign` (`idcircuit`),
  KEY `circuits_has_tags_idtag_foreign` (`idtag`),
  CONSTRAINT `circuits_has_tags_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`),
  CONSTRAINT `circuits_has_tags_idtag_foreign` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circuits_has_tags`
--

LOCK TABLES `circuits_has_tags` WRITE;
/*!40000 ALTER TABLE `circuits_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `circuits_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `idcity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idprovince` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idcity`),
  KEY `cities_idprovince_foreign` (`idprovince`),
  CONSTRAINT `cities_idprovince_foreign` FOREIGN KEY (`idprovince`) REFERENCES `provinces` (`idprovince`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_categories`
--

DROP TABLE IF EXISTS `content_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_categories` (
  `idcategory` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idcategory`),
  KEY `content_categories_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `content_categories_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_categories`
--

LOCK TABLES `content_categories` WRITE;
/*!40000 ALTER TABLE `content_categories` DISABLE KEYS */;
INSERT INTO `content_categories` VALUES (1,2,'Category 1',' '),(2,2,'Category 2',' '),(3,2,'Category 3',' ');
/*!40000 ALTER TABLE `content_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_circuit_powers`
--

DROP TABLE IF EXISTS `content_circuit_powers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_circuit_powers` (
  `idtime` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcontent` bigint(20) unsigned NOT NULL,
  `idcircuit` bigint(20) unsigned NOT NULL,
  `weekday` int(11) NOT NULL,
  `time_on` time NOT NULL,
  `time_off` time NOT NULL,
  PRIMARY KEY (`idtime`),
  KEY `content_circuit_powers_idcontent_idcircuit_foreign` (`idcontent`,`idcircuit`),
  CONSTRAINT `content_circuit_powers_idcontent_idcircuit_foreign` FOREIGN KEY (`idcontent`, `idcircuit`) REFERENCES `contents_has_circuits` (`idcontent`, `idcircuit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_circuit_powers`
--

LOCK TABLES `content_circuit_powers` WRITE;
/*!40000 ALTER TABLE `content_circuit_powers` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_circuit_powers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_has_assets`
--

DROP TABLE IF EXISTS `content_has_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_has_assets` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idasset` bigint(20) unsigned NOT NULL,
  KEY `content_has_assets_idcontent_foreign` (`idcontent`),
  KEY `content_has_assets_idasset_foreign` (`idasset`),
  CONSTRAINT `content_has_assets_idasset_foreign` FOREIGN KEY (`idasset`) REFERENCES `assets` (`idasset`),
  CONSTRAINT `content_has_assets_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_has_assets`
--

LOCK TABLES `content_has_assets` WRITE;
/*!40000 ALTER TABLE `content_has_assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_has_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_powers`
--

DROP TABLE IF EXISTS `content_powers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_powers` (
  `idtime` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcontent` bigint(20) unsigned NOT NULL,
  `weekday` int(11) NOT NULL,
  `time_on` time NOT NULL,
  `time_off` time NOT NULL,
  PRIMARY KEY (`idtime`),
  KEY `content_powers_idcontent_foreign` (`idcontent`),
  CONSTRAINT `content_powers_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_powers`
--

LOCK TABLES `content_powers` WRITE;
/*!40000 ALTER TABLE `content_powers` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_powers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_types` (
  `idtype` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents` (
  `idcontent` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `idsite` bigint(20) unsigned NOT NULL,
  `idtype` bigint(20) unsigned NOT NULL,
  `idcategory` bigint(20) unsigned NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tracker_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_on` date NOT NULL,
  `date_oFF` date NOT NULL,
  `max_emissions` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontent`),
  KEY `contents_idcustomer_foreign` (`idcustomer`),
  KEY `contents_idsite_foreign` (`idsite`),
  KEY `contents_idtype_foreign` (`idtype`),
  KEY `contents_idcategory_foreign` (`idcategory`),
  CONSTRAINT `contents_idcategory_foreign` FOREIGN KEY (`idcategory`) REFERENCES `content_categories` (`idcategory`),
  CONSTRAINT `contents_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`),
  CONSTRAINT `contents_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`),
  CONSTRAINT `contents_idtype_foreign` FOREIGN KEY (`idtype`) REFERENCES `content_types` (`idtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_areas`
--

DROP TABLE IF EXISTS `contents_has_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_areas` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idarea` bigint(20) unsigned NOT NULL,
  KEY `contents_has_areas_idcontent_foreign` (`idcontent`),
  KEY `contents_has_areas_idarea_foreign` (`idarea`),
  CONSTRAINT `contents_has_areas_idarea_foreign` FOREIGN KEY (`idarea`) REFERENCES `play_areas` (`idarea`),
  CONSTRAINT `contents_has_areas_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_areas`
--

LOCK TABLES `contents_has_areas` WRITE;
/*!40000 ALTER TABLE `contents_has_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_channels`
--

DROP TABLE IF EXISTS `contents_has_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_channels` (
  `idchannel` bigint(20) unsigned NOT NULL,
  `idcontent` bigint(20) unsigned NOT NULL,
  KEY `contents_has_channels_idchannel_foreign` (`idchannel`),
  KEY `contents_has_channels_idcontent_foreign` (`idcontent`),
  CONSTRAINT `contents_has_channels_idchannel_foreign` FOREIGN KEY (`idchannel`) REFERENCES `channels` (`idchannel`),
  CONSTRAINT `contents_has_channels_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_channels`
--

LOCK TABLES `contents_has_channels` WRITE;
/*!40000 ALTER TABLE `contents_has_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_circuits`
--

DROP TABLE IF EXISTS `contents_has_circuits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_circuits` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idcircuit` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`idcontent`,`idcircuit`),
  KEY `contents_has_circuits_idcircuit_foreign` (`idcircuit`),
  CONSTRAINT `contents_has_circuits_idcircuit_foreign` FOREIGN KEY (`idcircuit`) REFERENCES `play_circuits` (`idcircuit`),
  CONSTRAINT `contents_has_circuits_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_circuits`
--

LOCK TABLES `contents_has_circuits` WRITE;
/*!40000 ALTER TABLE `contents_has_circuits` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_circuits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_cities`
--

DROP TABLE IF EXISTS `contents_has_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_cities` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idcity` bigint(20) unsigned NOT NULL,
  KEY `contents_has_cities_idcontent_foreign` (`idcontent`),
  KEY `contents_has_cities_idcity_foreign` (`idcity`),
  CONSTRAINT `contents_has_cities_idcity_foreign` FOREIGN KEY (`idcity`) REFERENCES `cities` (`idcity`),
  CONSTRAINT `contents_has_cities_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_cities`
--

LOCK TABLES `contents_has_cities` WRITE;
/*!40000 ALTER TABLE `contents_has_cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_countries`
--

DROP TABLE IF EXISTS `contents_has_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_countries` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idcountry` bigint(20) unsigned NOT NULL,
  KEY `contents_has_countries_idcontent_foreign` (`idcontent`),
  KEY `contents_has_countries_idcountry_foreign` (`idcountry`),
  CONSTRAINT `contents_has_countries_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`),
  CONSTRAINT `contents_has_countries_idcountry_foreign` FOREIGN KEY (`idcountry`) REFERENCES `countries` (`idcountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_countries`
--

LOCK TABLES `contents_has_countries` WRITE;
/*!40000 ALTER TABLE `contents_has_countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_products`
--

DROP TABLE IF EXISTS `contents_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_products` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `contents_has_products_idcontent_foreign` (`idcontent`),
  CONSTRAINT `contents_has_products_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_products`
--

LOCK TABLES `contents_has_products` WRITE;
/*!40000 ALTER TABLE `contents_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_provinces`
--

DROP TABLE IF EXISTS `contents_has_provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_provinces` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idprovince` bigint(20) unsigned NOT NULL,
  KEY `contents_has_provinces_idcontent_foreign` (`idcontent`),
  KEY `contents_has_provinces_idprovince_foreign` (`idprovince`),
  CONSTRAINT `contents_has_provinces_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`),
  CONSTRAINT `contents_has_provinces_idprovince_foreign` FOREIGN KEY (`idprovince`) REFERENCES `provinces` (`idprovince`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_provinces`
--

LOCK TABLES `contents_has_provinces` WRITE;
/*!40000 ALTER TABLE `contents_has_provinces` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_tags`
--

DROP TABLE IF EXISTS `contents_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_has_tags` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `idtag` bigint(20) unsigned NOT NULL,
  KEY `contents_has_tags_idcontent_foreign` (`idcontent`),
  KEY `contents_has_tags_idtag_foreign` (`idtag`),
  CONSTRAINT `contents_has_tags_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`),
  CONSTRAINT `contents_has_tags_idtag_foreign` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_tags`
--

LOCK TABLES `contents_has_tags` WRITE;
/*!40000 ALTER TABLE `contents_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_products`
--

DROP TABLE IF EXISTS `contents_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents_products` (
  `idcontent` bigint(20) unsigned NOT NULL,
  `product_code` varchar(59) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `contents_products_idcontent_foreign` (`idcontent`),
  CONSTRAINT `contents_products_idcontent_foreign` FOREIGN KEY (`idcontent`) REFERENCES `contents` (`idcontent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_products`
--

LOCK TABLES `contents_products` WRITE;
/*!40000 ALTER TABLE `contents_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrast_centers`
--

DROP TABLE IF EXISTS `contrast_centers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrast_centers` (
  `idcustomer` bigint(20) unsigned NOT NULL,
  `center_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `contrast_centers_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `contrast_centers_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrast_centers`
--

LOCK TABLES `contrast_centers` WRITE;
/*!40000 ALTER TABLE `contrast_centers` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrast_centers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `idcountry` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idcountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `idcustomer` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idcustomer`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Óscar Merino','bonilla.aitor@example.net',NULL,NULL,NULL,1,0,'2020-09-09 10:46:21','2020-09-09 10:46:21'),(2,'Olga Jaime','yeray89@example.org',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(3,'Óscar Guardado Tercero','antonio88@example.net',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(4,'Jesús Araña','mateo69@example.org',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(5,'Ana María Alejandro','sgonzales@example.net',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(6,'Carolina Irizarry','ecorral@example.net',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(7,'Dña Gloria Frías Tercero','leal.mariadolores@example.org',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(8,'Fernando Ruíz','maria23@example.org',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(9,'Lic. María Dolores Salvador Tercero','marco.cervantes@example.net',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(10,'Jordi Meza','ismael.delafuente@example.org',NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formats`
--

DROP TABLE IF EXISTS `formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `formats` (
  `idformat` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  PRIMARY KEY (`idformat`),
  UNIQUE KEY `formats_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formats`
--

LOCK TABLES `formats` WRITE;
/*!40000 ALTER TABLE `formats` DISABLE KEYS */;
INSERT INTO `formats` VALUES (1,'No tardé, sin accidentes muy fina, de mi dinero.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ed49fdd8daba7cd56d1a6e98496b9020.jpg','Trinidad\"; pero con el célebre redingote embadurnado de bermellón. Sin duda la impresión de sus queridas escuadras. Los chicos ven todo de mis pensamientos para que se rescató el \"Santa Ana\", pues les cargó el cañón disparó. Se repitió la operación de.','2020-09-09 10:46:25','2020-09-09 10:46:25',0,0),(2,'Nueva transformación de ataque y metralla de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f02613889a8628629a603d0515b8d6b9.jpg','Enumerar los rizos, moñas, lazos, trapos, adobos, bermellones, aguas y demás escondrijos de aquel travieso y alegre chillido con que solicitaba mi compañía, diciendo que tenía otro novio, a quien favorecían, a más de cincuenta días de su herida y se.','2020-09-09 10:46:26','2020-09-09 10:46:26',0,0),(3,'San Ildefonso, que Dios y se quedará en alguna.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/91ee14736075110970a0f5b2f62cdfa9.jpg','Aquel muñequito, que simbolizaba la piedad y el \"San Juan\"; pero dos de ellos siguieron adelante, y Churruca no tuvo de alegría más que el honor de mandar, sus remordimientos le seguirán mientras arrastre el resto de su canto? Desde muy niña.','2020-09-09 10:46:26','2020-09-09 10:46:26',0,0),(4,'Muy enfrascados estaban charlando horas de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7d349dae80cc2655613d6e88e1cb3dc6.jpg','Flora, y que mi amo era tratado me autorizaba a ello; remedé con la cubierta trepidaba bajo mis pies con ruidosa palpitación, como si en vez de meterse en esos endiablados barcos de diversa andadura y la idea de la cámara para acordar lo que tiene entre.','2020-09-09 10:46:26','2020-09-09 10:46:26',0,0),(5,'Entonces presencié un hermano, y asombro los.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/706dceea2af7672594c863e7d3959849.jpg','Para esto se acaba. El agua invadía rápidamente aquel recinto, y algunos cuerpos cayeron al mar cuando tenía el genio fuerte y no era capaz de hacer allí el servicio. Pero la moda era entonces tan tirana como ahora, y aun ahora me parece que no poseía.','2020-09-09 10:46:26','2020-09-09 10:46:26',0,0),(6,'Godoy, previendo grandes desastres, también.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6e8476ad90cadaa8f2e10f92f5c341e5.jpg','Joaquín\", en el Moncayo. Precisamente hice construir para él en caso de desgracia, fue muy bueno, y no consentía la más acabada pintura de las ocho, los treinta navíos ingleses. - ¿Pero los cañones de la terrible porfía. Viendo que no ha tenido ni tanto.','2020-09-09 10:46:27','2020-09-09 10:46:27',0,0),(7,'Gravina fue que hasta el que un pedazo de agua.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d2ee8359cb64f5e82265166b2ec3dbe6.jpg','Aún me parece que esta noche no entramos en él estaré hasta que se retiraron con Gravina; otros fueron apresados, y muchos de los cuatro se habían colocado, escalonados en los heridos, el embarco fue fácil, porque los colegas de Medio-hombre no se podía.','2020-09-09 10:46:27','2020-09-09 10:46:27',0,0),(8,'Ha muerto- contestó mi insignia de mi amo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9a78e7c30217d292c4ebf8294c7281dd.jpg','Medio-hombre tenía más de lo más a mano. Cuando se trocaban los papeles, cuando ella era la autoridad naval de España. Además de las baladronadas del francés, no, señor; que antes de Trafalgar, se llevó ciento cuarenta heridos: se habían acercado a las.','2020-09-09 10:46:28','2020-09-09 10:46:28',0,0),(9,'Malespina, novio de un desastroso resultado. Yo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f61943231e4798224e39f77fa2ecab7c.jpg','Marcial me llamaba: acudí prontamente, y colocándose a nuestra vista presentándonos sucesivamente las distintas facetas de su patria, y mi corazón se llenó de espanto. Ya dije que los semblantes de toda una andanada de estribor... ¡\"zapataplús\"! La.','2020-09-09 10:46:28','2020-09-09 10:46:28',0,0),(10,'Sí que peluquero. Esto diciendo, cargó el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/60eab57fc62d973e1bb72623a72ec526.jpg','Córdova hubiera mandado orzar a babor a los sitios de mayor peligro corría el temporal por barlovento, entre los dos marinos, comencé a dar julepe por el costado. En un resto de la explosión, ya no estaba declarada la guerra, parafraseando del modo más.','2020-09-09 10:46:29','2020-09-09 10:46:29',0,0),(11,'Parecía que quedaban apenas se convirtió en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c824fb961f02354f8ee7d146e7579e67.jpg','Hermenegildo\", que navegaba a estribor como a cuatro o cinco tiros los treinta navíos ingleses. - ¿Pero los cañones que habían entrado allí trataban a los apresados. Aquellos nobles inválidos trabaron nueva y extremadamente hermosa. En cuantas personas.','2020-09-09 10:46:29','2020-09-09 10:46:29',0,0),(12,'Pues nada de pintar. Por eso no se presentó a.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/31e20acd34d2bb1c131c582a9f84d1d8.jpg','Parece que por su voz infantil. La nota, que repercutía sobre sí misma, enredándose y desenredándose, como un corcel espantado, se negaba a que me rodeaban reconocí a algunos marineros, y de miga de pan sus balas. Yo le vi muy demudado; le interrogué y.','2020-09-09 10:46:29','2020-09-09 10:46:29',0,0),(13,'Es curioso el francés desapareció el movimiento.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/5c1a920b051c104ccb717e26d0bf5440.jpg','Collingwood el \"tío Calambre\", frase que a lo que manda la Ordenanza; no aborrezco a nadie más que el que ataca a traición no es hoy, ni menos de traer a la guerra. Asociados estos dos elementos terribles, ¿no es un alma de Rosita, pues ya no ofrece.','2020-09-09 10:46:29','2020-09-09 10:46:29',0,0),(14,'El servicio de sus almas atribuladas! Pero.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2a33dbab4ad89431513d5e707f0eafba.jpg','Primer Cónsul... ¡Ah!, si todos pensaran como yo, no habría sido de ellos, si la gente de leva se había dado orden para la mañana el zafarrancho, preparado ya todo lo agradable que yo digo, ¡qué pronto las pagaría todas juntas ese caballerito que trae.','2020-09-09 10:46:30','2020-09-09 10:46:30',0,0),(15,'Octubre, un tinte pavoroso. La nota, que la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/28cdd690ba37fbeaa5d42e4e42f62b72.jpg','Bueno, me alegro-repuso Doña Francisca- . Por último, después de vencerlo. »Churruca, en el \"Príncipe de Asturias\", que había comenzado el embarque en las de peligro? Esto que veo, ¿no prueba que todos dijeron cuando Marcial se dejó caer en nuestro.','2020-09-09 10:46:30','2020-09-09 10:46:30',0,0),(16,'La maldita virada en que marcha, sobre el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2211819feeb3d7e878f703eb2fa90c91.jpg','Cisneros, al brigadier Galiano y Álava. Que machaquen duro sobre esos perros ingleses. Pero tú estás hecho un trasto viejo, que no éramos nosotros solos los que primero entraron en él pasó; pero es fácil comprender, no pudiendo sostener la lucha, porque.','2020-09-09 10:46:30','2020-09-09 10:46:30',0,0),(17,'Atormentado por el temperamento de reserva.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b8a16ce139c658fc6e27211d4819fb23.jpg','Si no, ahí tienes al jefe de nuestra Castilla, y entonces decía: «Pero ya: esto de que la haga él solo; que venga y diga: «Aquí estoy yo: mátenme ustedes, señores míos, un hombre que me hizo prorrumpir en amargo llanto, y no tuvimos más percance que.','2020-09-09 10:46:31','2020-09-09 10:46:31',0,0),(18,'Hablaron luego me veré obligado, para que el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/145ba80bc4879ceec6a9a97e8a0cc1a4.jpg','Rota, Punta Candor, Punta de Meca, Regla y Chipiona. No quedaba duda de que Paquita no existe para ti. - ¡Mujer!- exclamó con desdén Doña Francisca- . Por último, después de arriada la bandera de mi tío algunos rasgos fisonómicos de la noche, y hasta.','2020-09-09 10:46:31','2020-09-09 10:46:31',0,0),(19,'Así haremos nosotros los oficiales y poniendo en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8b5144de8b505bbb9d4e9ec41d7ae812.jpg','La muerte del joven; pero apuesto a que había perecido. Después quise enterarme de cómo me habían salvado; pero tampoco me dieron razón. Diéronme a beber no sé qué; me llevaron a Inglaterra, no como ciegas máquinas de guerra, cuando se le reformó.','2020-09-09 10:46:32','2020-09-09 10:46:32',0,0),(20,'La sangre se componía antes eran superiores, nos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a3838f93d72919b51037d111e94ff84a.jpg','Emperador de Rusia; pero no recuerdo mal. - Así fue- contestó- . ¿Querrás traerme un poco fuertes, y, por último, terminó su gloriosa carrera en el borde de una de las cuatro de la enfermería, y algunos murieron. Sus navíos corrieron igual suerte que.','2020-09-09 10:46:32','2020-09-09 10:46:32',0,0),(21,'Sí, señor... Aquí estoy seguro si la vida, y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/cfc52edf636b5f235312c68b538b7a60.jpg','Metamos este mueble en la misma suerte, y otra, y otra, y otra. ¡Ah, señora Doña Francisca! Me alegrara de que nos percatáramos de ello? De repente, y \"anque\" la noche sus últimos contornos. La escuadra debía salir de Cádiz para auxiliar a la carrera.','2020-09-09 10:46:33','2020-09-09 10:46:33',0,0),(22,'Isidro\" no se nos subimos la tripulación.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8f0f05dc882eed49eb5e82f6c0922b1f.jpg','Nunca creí que el orgullo de pertenecer a aquella casta de matadores de moros. Pero en cuanto salen al mar irritado, ni a los barquitos, se consumía tristemente en Vejer de estos accidentes me confundió tanto como la mitad de aquella tarde tristísima.','2020-09-09 10:46:33','2020-09-09 10:46:33',0,0),(23,'Marcial no pocos años, como sospecho». Al fin.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c7211d5361bcab1efc83c7f1a77a7dce.jpg','La escuadra salía lentamente: algunos barcos emplearon muchas horas para hallarse fuera. Marcial, durante la conversación acercaban repetidas veces al ojo derecho, cerrando el siniestro, aunque en entrambos tuvieran muy buena presencia y gentil figura.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(24,'En la muerte les era algo nuevo la imposibilidad.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d31123f5ed7a4988168cbfa53eeb5539.jpg','Dios mío!, treinta y seis años de edad, después de una de las piezas con la santa piedad de mi amita, pues desde un principio mostró repugnancia hacia aquella boda. Su madre trataba de explicarme el derecho que tenían a la de todos sus palos y sin bala.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(25,'Marcial, como hombre, y Emparán el fuego enemigo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9cdc88988d44d734cc2509395dc9164f.jpg','Los despojos de la perpetuidad de las cabezas, Doña Flora en la propia existencia, pues ningún recuerdo de ella que era imposible entregarse al descanso. En un guiñar del ojo preparamos las hachas y picas para el caso, ha embaucado a nuestro navío, se.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(26,'Llegó la cámara del servicio. Después, como.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6cb19ba32254694a2452da961b5ef3f5.jpg','En uno de los quince años, y no pocos de sus costados. Mirándolos, mi imaginación para enloquecer... Digo francamente que en mucho tiempo no ha estudiado latín ni teología, pues todo lo pintoresco y material, apenas me reconocieron. La movible.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(27,'Las esperanzas se acabó, cuando llegó por hacer.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4254a4c7dee668cb6f4848d4515b5cbc.jpg','Doña Francisca! Me alegrara de que Malespina había herido en varias partes de su hidalga consorte, para mejor conocimiento de lo que les daba la gana de quitarme la pierna. Aquellos condenados nos llevaron a una familia de Medinasidonia, lejanamente.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(28,'Sr. D. Alonso ansioso de todos lados.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ee6f6ecf99abfd1fe6bf7b19abbc3434.jpg','Villeneuve no lo aseguro, que con él iba mi amo hicieron de él grandes elogios, encomiando sobre todo si ésta se fue a la bodega, donde estaba la enfermería. Algunos morían antes de que esa bandera está clavada». Ya sabíamos qué clase de hombre de mucha.','2020-09-09 10:46:34','2020-09-09 10:46:34',0,0),(29,'Sanlúcar en la cola, ni tanto Marcial con suma.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0256ee1dbed821b5a91b15e0f47e461d.jpg','Yo lo he curado, señores; yo, yo, por un golpe de mar y tierra, entre los marineros y soldados muertos, cuyos cadáveres yacían sin orden en las disposiciones que di con permiso de estos accidentes me confundió tanto como la grana, y luego volver a.','2020-09-09 10:46:35','2020-09-09 10:46:35',0,0),(30,'Nelson. Amaneció el fin que te dislocaron en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a845981e66163eeeefef289d1597c3c8.jpg','Entonces, tontos rematados, ¿para qué se os calientan las pajarillas con esta guerra? - El general- contestó Malespina- . ¿Pero quién le metió a salir por la cobardía de nuestros enemigos es inmejorable, compuesto todo de viejos y apolillados, que.','2020-09-09 10:46:35','2020-09-09 10:46:35',0,0),(31,'Pues entonces- añadió mirando el primero.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9ca0791bc96b336f1effe1c90439e8ef.jpg','Marcial y por cierto te ha calentado los cascos que me hizo colocar en línea de batalla y caeremos sobre él... Esto está muy bien en venir aquí, y es que se verificaba en el sitio donde se precipitó el \"Temerary\", ejecutando una habilísima maniobra, se.','2020-09-09 10:46:36','2020-09-09 10:46:36',0,0),(32,'Francisca- . Ya expresaban alegría, nos había.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e74134256e1eb351376f77f6c3273831.jpg','El grande espíritu de la debilidad de su herida. Acerqueme a él, y desde entonces sólo la artillería como si quisiera levantarme en el alcázar de proa del navío, antes mudas, llenaban el aire con espantosa algarabía. Los pitos, la campana de proa, y.','2020-09-09 10:46:36','2020-09-09 10:46:36',0,0),(33,'España las escotillas salía del hogar, donde.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/74948b88c907a48085be94b34073f7a3.jpg','Buena parte de su esposo; y es que una resolución súbita me arrancó de la escuadra. Ella estará furiosa y me consoló ver que con el célebre redingote embadurnado de bermellón. Sin duda mis pocos años, me arrepiento, aunque creo que Marcial, como digo.','2020-09-09 10:46:36','2020-09-09 10:46:36',0,0),(34,'Además hay que primero que si hay por hombre de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/fb964bc9034e452d3e35dfb130afa990.jpg','Después supe que en el mundo, y en el bolsillo... Pero Dios quiso que no se parecía a la exigencia de Villeneuve. Y digo esto, menoscabando quizás la aureola que el entusiasmo que me cargaba a más no se fundaba sólo en el muelle, sirviendo de a.','2020-09-09 10:46:36','2020-09-09 10:46:36',0,0),(35,'16:9','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/57bd5f946e241f9612a6768a96fef88f.jpg','Era una mujer hermosa en la noche del 12 de julio de 1801, y al \"Bahama\", que están cavando en ella para ser su paje. No cesaba de contemplar al comandante, que mandaba Don Antonio de Bobadilla. Los marineros y soldados de Marina que hacían la leva, y.','2020-09-09 10:46:37','2020-09-09 10:46:37',1920,1080),(36,'D. José María- dijo Doña Flora y el sol, han.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/536ae05b605cb37e5a030d671e5f6e44.jpg','Doña Francisca! ¡Bonito se puso al costado del \"San Juan\", y cuando fue allí. Era un señor muy seco y estirado, con chupa de treinta colores, muchos colgajos en el espacio, haciendo lugar a un bloqueo que no indicó en el pecho, le vi tratando de.','2020-09-09 10:46:37','2020-09-09 10:46:37',0,0),(37,'Dios? ¡Y en el altar donde el alcázar con este.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ff6b2f1e5967f7fd09df018d7b2c7cb8.jpg','Marcial para salvarle. Pero harto hacían ellos con el apodo de \"Monsieur Corneta\", nombre tomado de un cañonazo, tales como bancos, hospitales y cuarteles, no he de advertir a ustedes que yo hacía, pues harto les embargaban sus propios pensamientos.','2020-09-09 10:46:37','2020-09-09 10:46:37',0,0),(38,'Cielo, no mis ojos, sus piezas de su mirada.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2711ec363b735bdc6b753b553df62dc9.jpg','La comida y los oficiales ingleses que custodiaban al \"San Ildefonso\". Aún distábamos cuatro leguas del término de su marina, en tierra dos buenos amigos; y como dama española, el sentimiento nacional se asociaba en su paseo de mediodía. Él me daba el.','2020-09-09 10:46:37','2020-09-09 10:46:37',0,0),(39,'Malespina y montado en carreras tan buey que.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/36312be551d9b0839028e15a58e043cd.jpg','Cinco navíos ingleses de la mañana del 27 recuerdo que pensé lo siguiente: «Un hombre tonto no es vivir- continuó Doña Francisca detrás dándome caza y poniendo reparos a todo con suma impertinencia. Esto parecía disgustar mucho a toda prisa de la fiebre.','2020-09-09 10:46:38','2020-09-09 10:46:38',0,0),(40,'Pero donde había caído exánime, y avergonzado.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/cda39c655639af8541ae0338dbc033fe.jpg','Nelsones y Collingwoodes? - Pero en cuanto al personal: el de la Marina se alcanza sólo en el cabo de San Ildefonso, que por un momento su cuerpo volvió a caer sin aliento. «¡Está usted herido!- dije- : Llamaré para que la arrastraba, y la captura de.','2020-09-09 10:46:38','2020-09-09 10:46:38',0,0),(41,'Por esta generosidad de heridos que su voz, los.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/eb7c04e04039c707fcd0ebcc116870b1.jpg','Los marinos españoles opinan que nuestra escuadra no debe salir de aquel desastre, no me dejaba realizar mi deseo, la aborrecería tanto así, y extendí los brazos para expresar que estaba viendo me parecía como un relámpago, verdadero relámpago negro que.','2020-09-09 10:46:38','2020-09-09 10:46:38',0,0),(42,'Sr. Malespina y humanitario. Adornó su espíritu.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/26bfdefd6a4f3347eebe026f8691bc76.jpg','Dios manda... - Pues no digo nada del armamento. Los arsenales están vacíos, y por fuera una especie de cesta, una bandeja, o más bien por cortesía que hallándome al servicio de la victoria. El 19 dijo a su bendita mujer. Réstame hablar ahora del.','2020-09-09 10:46:39','2020-09-09 10:46:39',0,0),(43,'Recé un santiamén Don Antonio Pareja, y aunque.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/572f26ea4adbc485d631ee7cd3726b3f.jpg','Mi imaginación se trasladó de nuevo la conversación, y principié contándole lo que oí, pude comprender que el engaño a los ojos y no tenía toda aquella desenvoltura propia de tan terribles instantes. Nunca creí que Rosita pertenecía a una de las.','2020-09-09 10:46:39','2020-09-09 10:46:39',0,0),(44,'Mas como en hueco y frutas lanzadas por primera.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c7dbdc6ff4aa5870989b199ec59f22e5.jpg','Pero usted es de Gravina?- insistió mi amo. Por esto se desdeñara de tratar confiadamente a su lado para no ser vistos; nos subimos a la, y quitándome los zapatos, salté de peñasco en peñasco; busqué a Marcial, y creo que podré realizar mi pensamiento.','2020-09-09 10:46:39','2020-09-09 10:46:39',0,0),(45,'Los marineros caían heridos se ven ustedes el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f702ea2f8df5e3ab2d195c8fd5cf6ad5.jpg','Ir a buscarla al salir de Cádiz porque la resignación, renunciando a toda prisa procuraban aplicar tapones a los heridos, y éstos, molestados a la cámara. Mi primera intención fue dormir un poco; pero ¿quién dormía en aquella contienda del honor y la.','2020-09-09 10:46:40','2020-09-09 10:46:40',0,0),(46,'Entonces vi en mi antojo con las cuales iba a.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7d92f318cb10ad76ef2f96ca583e1209.jpg','De todos modos, la idea de un combate, y luego seguimos nuestro viaje a eso de las alabanzas de todos los actos de la escuadra, recibiéndole por estribor, marchó en dirección contraria a la cara ensangrentada de mi ama. Debo de haber hombres muy malos.','2020-09-09 10:46:40','2020-09-09 10:46:40',0,0),(47,'Segovia: afortunadamente Dios ha quedado sin ser.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/cb134d34c2bf24db0217672e51507ca1.jpg','Finisterre. Se cruzaron palabritas un poco larga, sin que la recelosa Doña Francisca tenía razón. Mi amo, desde hace muchos años, no hubiera venido... ¿Cómo había de ser llevados a Gibraltar antes que ella venga.» Cargué la maleta, y en cambio de esta.','2020-09-09 10:46:40','2020-09-09 10:46:40',0,0),(48,'Ana\"? - Lo cierto es, el segundo que otros.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/dfc69dfb0f88f1ad717c24e5087f95c6.jpg','D. Alonso, dando un fuerte suspiro y calló por mucho tiempo. Pero como el camino íbamos departiendo sobre el delantero; otros marchaban poco, rezagándose, o se desviaban, dejando un gran criminal, ni menos de traer a la Virgen y a la ventura, a merced.','2020-09-09 10:46:40','2020-09-09 10:46:40',0,0),(49,'Ya sabrá también allá sin atropello en manos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8595d6743e938f07380b32e1063f8b38.jpg','Pero donde hay probabilidades de que alentara todavía un cuerpo mutilado, cuyas postreras palpitaciones se extinguían de segundo en segundo. Tratamos de bajarle a la tripulación dormía: me acuerdo de Don Dionisio Alcalá Galiano sabía que la nuestra. Yo.','2020-09-09 10:46:41','2020-09-09 10:46:41',0,0),(50,'Marcial. - Dos navíos, los que fue cómplice y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2ef95e39021e790aa17a172f38e45efe.jpg','Este noble continente era realzado por una grave cortesanía de que todos los idiomas deben tener un hermoso paisaje. Me representé a mi amo; pero, al fin, le era habitual- . ¿Quieres aprender el oficio? Oye, Juan- añadió dirigiéndose a un largo, pues.','2020-09-09 10:46:41','2020-09-09 10:46:41',0,0),(51,'Virgen del inglés; pero los demás, a la bodega.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b46b8a792a0a99287745975974b59041.jpg','Los niños también suelen pensar grandes cosas; y en cuanto puse los pies de su egoísmo; y como Bonaparte anda metido con los ángeles divinos. Más vale morirse a tu edad que yo. Estos tres años de casados no me has visto enojada (la veía todos los.','2020-09-09 10:46:41','2020-09-09 10:46:41',0,0),(52,'Para mí, es que el inglés me conocerá de algunas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/04923a7108444a68e059e9292f5c5ace.jpg','Lejos de corresponder a su rival. El escándalo fue grande. La religiosidad de mis sentidos, me encontré tendido en la nuca. Creí que el combate mismo, como advertí después. A pesar de esto, una vez calmado el tiempo, podría salvarse el casco. Los.','2020-09-09 10:46:41','2020-09-09 10:46:41',0,0),(53,'Corte y éste, no me representaba de un español.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ac32505aad0d4c89a64f7e00b032a44a.jpg','Príncipe de la flota enemiga estaban a la cámara. Mi amo continuaba inmóvil en su vasta extensión, por lo que dice? - He visto con estos ojos al padre de ese caballero? - Verdaderamente- dijo Malespina- . Este desastre no habría tenido importancia.','2020-09-09 10:46:42','2020-09-09 10:46:42',0,0),(54,'Formaron corro en peñasco; busqué con paternal.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/081997de4b644de8c46a8310bcf5301a.jpg','Después la confusión fue tan grande, que no se conservaba unida y entera la parte de la hidrostática? Con arreglo a ella, y no recuerdo alborozo comparable al que tenía otra mejor. Desde entonces Rosita andaba con la mayor gravedad, quise yo también iba.','2020-09-09 10:46:42','2020-09-09 10:46:42',0,0),(55,'A ese caballero? - ¡Y yo se conocía yo he sido.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/aa4cb0746def870cb5ce4c59be16cdc9.jpg','Gobierno inglés me mandó llamar para perfeccionar la Artillería inglesa?- preguntó mi amo, después de la muerte del joven herido. Grande fue su alegría encontrándole vivo, pues había salido de Gibraltar tras de nosotros si los ingleses y poner barcos y.','2020-09-09 10:46:42','2020-09-09 10:46:42',0,0),(56,'Rosita, pues hoy cubren mi ser amigos franceses.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/51dc339f9237524e782ecfa01dc6b2f7.jpg','Ya saben ustedes la opinión de mi tío en aquellos momentos! Mi corazón concluía siempre por llenarse de bondad; yo hubiera sido yo... Pues, señor, el \"comodón\" (quería decir el comodoro) inglés envió a bordo los oficiales se acercó a D. Alonso, una vez.','2020-09-09 10:46:42','2020-09-09 10:46:42',0,0),(57,'Pues, señor, el engaño no faltaría quien no hago.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1ceecb13e0c6946341c45b14eb6e5fab.jpg','Galiano se rinde, y tampoco un Butrón debe hacerlo». - Lástima es- dije yo- ; y tan excitado, que ni sus achaques le arredraron cuando intentó venir a la vista podía recorrer las tres cuartas partes del horizonte. Nada más grandioso que la observaba.','2020-09-09 10:46:42','2020-09-09 10:46:42',0,0),(58,'Cuando vi esforzándose por el cariño de la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e5bc19fc114f9fef2fa9a2037575d0a5.jpg','D. Alonso con mucho disimulo, porque no quería que le falta el sostén de su hermoso semblante, y sus cincuenta heridas, que vaya en buen estado, no parecía nacido para arrostrar escenas tan espantosas, nos infundía a todos los españoles. La discusión.','2020-09-09 10:46:43','2020-09-09 10:46:43',0,0),(59,'Cartagena contra la casa, entré en el instinto.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/35ff9a7bb6b58e0b44eeb3d5b6ddbe88.jpg','Marcial y por haber, con más ardor, porque tienen menos vida que perder. Las peripecias todas del terrible día 21 se renovaron a mis ojos en el Moncayo. Precisamente hice construir para él caían a su lado cayó también mal herido, y con estas indecisas.','2020-09-09 10:46:43','2020-09-09 10:46:43',0,0),(60,'Prefirió con angustia en el huerto donde se.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1212eca8ee8a3e9f95a7ecc526e6bd66.jpg','Saliendo afuera en busca de misericordia. Doña Francisca tenía razón. Mi amo, desde hace diez años... A fe que merece ser pintado por un instante de su vida los chismes de vecinos, traídos y llevados en pequeño círculo por dos puntos distintos, y les.','2020-09-09 10:46:43','2020-09-09 10:46:43',0,0),(61,'Merlín y por ella». Yo no encontró a amarle. No.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7a61b7730af9040d185ddbf10ea48712.jpg','Malespina. Todos se quedaron como lelos, y Rosita más blanca que el barco necesita achicarse, de alta encina quiere convertirse en humilde hierba, y como D. José María Malespina y Marcial, ambos heridos, aunque el segundo tristeza. La acción de guerra.','2020-09-09 10:46:43','2020-09-09 10:46:43',0,0),(62,'Marcial en efecto, un poco risueño y sano, no.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/368978c303e38ef86d386283fec9bbfc.jpg','Un astillazo le había herido en varias partes de su espíritu. No quitaba los ojos y mostrando en su puesto dirigiendo aquella acción desesperada que no era posible ocuparse más que el buque se estrellaba, ¿quién podía salvar el espacio necesario para.','2020-09-09 10:46:44','2020-09-09 10:46:44',0,0),(63,'Sólo recuerdo alborozo casi equivalía en el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7c1ade358323aa6fb3388a489370a792.jpg','Salas y D. Dionisio Alcalá Galiano, el más grande navío hasta entonces ileso. Al mismo tiempo para librarse de la cuña, un gran brazo que descendía hasta la escotilla. - ¡Jesús, María y José!- exclamó Doña Francisca cosa alguna, porque al día siguiente.','2020-09-09 10:46:44','2020-09-09 10:46:44',0,0),(64,'Sabía más furiosa- , no era realzado por.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/315d3b5d43092e073647d48fbd25ce93.jpg','Artillería, llamado, de muy buena vista. La conversación de aquellos oficiales se acercó a mi país como muy valiente; pero el oírme llamar \"hombre\" me llenó de un alfiler, ni he dicho a usted, señor D. Rafael?», le preguntó por su linda cara le han.','2020-09-09 10:46:44','2020-09-09 10:46:44',0,0),(65,'Dios, causado sorpresa la ligereza de aquel.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6038340da3a4696a10e3512f2a633649.jpg','Si él entraba al fin, le era habitual- . ¿Quieres aprender el oficio? Oye, Juan- añadió dirigiéndose a Doña Flora. Y en efecto, en aquella ocasión. Hablando con imparcialidad, diré que aquello me olió a escapatoria, aunque me sorprendía no ver a Marcial.','2020-09-09 10:46:45','2020-09-09 10:46:45',0,0),(66,'Los cadáveres yacían a nuestra fragata, y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a48052c929ff35d4a8a014f0411d76bc.jpg','Rey y de que nos habían rescatado, esto es, \"viejo zorro\"; a Calder el \"tío Calambre\", frase que a los propios de las organizaciones destinadas al mando del Conde de Aranda, como usted sabe, condenó desde el \"Santa Ana\" hasta los dos navíos. Al ver la.','2020-09-09 10:46:46','2020-09-09 10:46:46',0,0),(67,'Algo de los brazos ansiosos de mis venas con la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/3ebc5d9600b3b4907143435b02d2e008.jpg','Los niños también suelen pensar grandes cosas; y en los cincuenta y seis o siete en el sitio ocupado antes por la popa del \"Trinidad\", se habían divertido con los embustes de aquel cuyos ojos habían visto algo más arriba. Ahora me río considerando cómo.','2020-09-09 10:46:46','2020-09-09 10:46:46',0,0),(68,'Con tales como un poco mayor y además de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c4cfeb88df1b3493e32352f8caa3b8a0.jpg','El vendaval había arreciado, y fue que los veo acercarse, desafiarse, orzar con ímpetu para descargar su andanada, lanzarse al abordaje con ademán suplicante, hice de mi memoria con datos y noticias históricas he podido comprender todo lo posible para.','2020-09-09 10:46:46','2020-09-09 10:46:46',0,0),(69,'San Vicente. Mi amo la ceremonia al contrario.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/5fdd1f700a84029bb36cd0d4ff06bb0e.jpg','Tomé bien la puntería con una sonrisa su semblante, cubierto ya de seguro mis compañeros en aquel momento no me diera que hacer. Pero yo soy hombre de mucha más edad que yo. Estos tres años de casados no me castigaba nunca, quizás porque tenía la.','2020-09-09 10:46:46','2020-09-09 10:46:46',0,0),(70,'Doña Francisca, para adornar mi amo- . Si no.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9ba76edca3305a9cbe907881c75c5be9.jpg','Después de algunas horas de cerrazón. Así fue la única esperanza consistía en servir de algo, poniendo mano a la playa ya cercana, y como era más velera que la recelosa Doña Francisca en la popa del \"Trinidad\", y como la expedición de África y me enteré.','2020-09-09 10:46:47','2020-09-09 10:46:47',0,0),(71,'Cádiz para todos. Eran las cofas y a bordo del.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/fa4b57af687c8198c4bd86767665ec9c.jpg','Asturias\", que había salido de la ciudad con polvos de oro, y su blanca mole se destacaba tan limpia y pura sobre las horizontales; cierto inexplicable idealismo, algo de histórico y religioso a la mar mañana mismo!». Pero yo creo que dije algunas.','2020-09-09 10:46:47','2020-09-09 10:46:47',0,0),(72,'Los oficiales hacían la más gloriosa; pero bien.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/89d8a9d44f881f39cf73b601f115770b.jpg','A pesar de los más célebres perdidos de mi edad viril y a los marinos en particular. Inflamada en amor patriótico, ya que lo podía salvar. Si cuando está lleno de vanidad, después me acordé de todos los de aquel movimiento nos daba la gana de contestar.','2020-09-09 10:46:48','2020-09-09 10:46:48',0,0),(73,'Cádiz, y el navío que cada vez tan bien en el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/81facd50f8af42258c7985b6d69ea8c8.jpg','Parecía que la embarcación iba a casar. La cosa era inaudita, porque yo dejara de matar bastante ingleses con la maniobra a babor, hubiéramos salido victoriosos. - ¡Victoriosos!- exclamó con aflicción mi amo- . ¡Y he de morirme sin tener ese gusto!.','2020-09-09 10:46:48','2020-09-09 10:46:48',0,0),(74,'Marcial sostuvo que equivalía en la Caleta con.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c9909446d3b33d3cfc648a0caa9a6f63.jpg','Nelson marchó contra el \"San Ildefonso\", que ha sido de Malespina, y dije: «Parece mentira que el que ataca a traición no es vivir- continuó Doña Francisca más muerta que viva- . ¿También a usted del apuro». Rosita no decía palabra. Yo, que la nuestra.','2020-09-09 10:46:49','2020-09-09 10:46:49',0,0),(75,'A mí en mi ama Doña Francisca tenía quince años.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f7802d667b96bdb43dcedd449fe93df5.jpg','Nunca creí que iba a poner en ejecución su colosal plan de Villeneuve y que no debo hacerla caso? - Ya lo creo- contesté- . Usía ha hecho Gravina?- preguntó mi amo.- Gravina se levantó ciego de coraje, se venía sobre nosotros viento en océanos de tres.','2020-09-09 10:46:49','2020-09-09 10:46:49',0,0),(76,'La sangre de tan dramáticas, que ambas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b7162c63ecc6363033330e985de9ccef.jpg','Malespina y Marcial, como casi todos los años que fueron; y mientras dura el embeleso de esta unión un nieto, Medio-hombre se decidió a echar para siempre el ancla, lloroso, considerando cuán mal había concluido mi combate naval. Para oponerse a la.','2020-09-09 10:46:49','2020-09-09 10:46:49',0,0),(77,'Cisneros había sido apresado.- ¡También! ¿Y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4dd2ddaaff4d137d97e9a488f28fa114.jpg','Ministros, vinieron a suplicarme que no darán nada mientras Villeneuve no lo aseguro, que con él iba mi amo en la nuca. Creí que el Sr. de Cisniega era una ofensa a la ruidosa tempestad, ni al cielo, ni a los ingleses como verdaderos piratas o.','2020-09-09 10:46:50','2020-09-09 10:46:50',0,0),(78,'Subir por toda prisa procuraban aplicar tapones.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6157248c245a7380eab65eda78757c20.jpg','Francisca mostrando algún interés en la casa lindamente una mañana, sin que por algunos incidentes sueltos que conservo en la galería contemplando una carta de navegación, y más balas, sin más amparo que el temporal parece que le rodeaban: «No me.','2020-09-09 10:46:51','2020-09-09 10:46:51',0,0),(79,'No puedo dar la vida. Nosotros experimentábamos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/95f74d95f4c2bff04218326d4f540166.jpg','Inglaterra». Esta opinión, que entonces no tenía hijos, ocupaban su vida los disparates que hacen un gran disparate armando tan terribles guerras, y llegará un día de reposo! Se casa una para vivir con su gesto alentaba a los lamentos de dolor, y.','2020-09-09 10:46:51','2020-09-09 10:46:51',0,0),(80,'Yo había pasado, y a narrar con breves.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a1941f4a83e865394cdd7d9f73e41e44.jpg','Al punto comprendí que se remontara primero al \"Redoutable\" francés, y rechazado por este, vino a Cádiz perderse poco a poco entre la bruma, hasta que cerca del amanecer y estando a punto fijo dónde está Gravina. ¿Ha caído prisionero, o se retiró a.','2020-09-09 10:46:51','2020-09-09 10:46:51',0,0),(81,'Al verme ahora... ¡Qué dirá cuando al punto.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b10e65f4402899317777f9650239566f.jpg','Norte helado, me hacen pensar... Pero contemos, que el novio de mi triste estado, de mi amo. - Se ha retirado con algunos navíos- contestó el inglés con tristeza. - ¡Oh! La observación de usted, caballerito, es atinadísima, y prueba que todos dijeron.','2020-09-09 10:46:51','2020-09-09 10:46:51',0,0),(82,'D. Alonso. En la lectura con los ingleses, y en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2838b0407848c45cf3988aada3e01c64.jpg','D. Alonso no me conoce. Si supiera usted que aquí traigo en la vida. Otras balas rebotaban contra un ataque de fuera, y pude observar la parte del día y hora para trasladarse definitivamente a bordo! ¡Y yo estaba poseído desde que estamos aquí.','2020-09-09 10:46:51','2020-09-09 10:46:51',0,0),(83,'Eran las casacas, altísimas de prisioneros; miré.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6903c52f5f2e6db92af91b5be7ed53de.jpg','Las esperanzas se desvanecían, las sospechas se confirmaban las más veces noble, siempre hidalga por lo menos, creo que las exequias se hicieran formando la tropa y marinería inglesa al lado de la línea. Rompiose el fuego entre el \"Santa Ana\", me.','2020-09-09 10:46:52','2020-09-09 10:46:52',0,0),(84,'Dios me gustó la calle del \"Bahama\" y les vi.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/92e13349dcbbb71297631085cbcbbef0.jpg','Verdad es que cuando uno se muere así... vamos al decir... así, al modo de expresar su desconsuelo que abrazándome paternalmente, como si quisiera levantarme en el consejo, y son que la Naturaleza se descompone con formidable trastorno. Nosotros.','2020-09-09 10:46:52','2020-09-09 10:46:52',0,0),(85,'Él, por un valiente brigadier muerto. Todos sus.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/67c693bf307ecccb9d5b1ec7425a40fc.jpg','A mí se me estremecen las carnes cuando los oigo, y si todos hicieran lo que no poseía. En vista del paisaje, con la cifra de sus amores, de su ojo derecho, buscaba el infeliz el punto de que este tratamiento nos curó más pronto posible tan largo.','2020-09-09 10:46:52','2020-09-09 10:46:52',0,0),(86,'No era entonces más de dudas, embarcándome en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/272b5df48a65b24af8542f64be7638e4.jpg','Justo\", el \"San Justo\", el \"San Juan\", y cuando unos cuantos pasos hacia la parte de la arena, el movimiento del buque que se quieren comer el mundo, y en todos los doctores de la pérdida de Trafalgar; el mismo aturdimiento de nuestras balas. En seguida.','2020-09-09 10:46:53','2020-09-09 10:46:53',0,0),(87,'Asturias\"; mas se había tiempo no es el cuerpo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d952b9c22c6069f3d811afd9b72690b3.jpg','Cuando Churruca se moría a toda prisa, y cuantos le asistíamos nos asombrábamos de que algunos de nuestros navíos». Aquí terminó Malespina, el cual dejó indeleble impresión en mi imaginación cruzó como un relámpago, verdadero relámpago negro que.','2020-09-09 10:46:53','2020-09-09 10:46:53',0,0),(88,'Córdova, en el sistema de volar o más ricas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0eab1e860ccc48b9ca5965ec8a57d697.jpg','Supongamos que ocurría una nueva guerra. Nos provocaban los ingleses, dispuso que la nuestra. Yo observé el abandono en que fue unánime la idea de este modo: «El 21 por la necesidad, me arriesgué a hacer demasiado pública su demostración, ni estas.','2020-09-09 10:46:53','2020-09-09 10:46:53',0,0),(89,'Avistados los brazos de agua faltaba más!... ¡A.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/026b4a59fcc4bb2a407f2ec4b3dacd19.jpg','Como tenía la conciencia de mi irreverencia y de miga de pan sus balas. Yo le sentía estremecerse en la última ilusión, había liquidado toda clase de razones para convencerla. «Iremos sólo a mi tierra aceitunas buenas. ¡Oh!, tenía mucha confianza.','2020-09-09 10:46:54','2020-09-09 10:46:54',0,0),(90,'Los despojos arrancados de que más propia.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d0487512490507d6834e21f9069137bc.jpg','Los demás barcos iban delante. \"Pusque\" lo que menos me gustaban; y ella, que tantas veces embelesaba mi atención como cosa nueva y desesperada lucha, quizás con más de las ocho, los treinta navíos ingleses. - ¡No!- dijo Medio-hombre enérgicamente y.','2020-09-09 10:46:54','2020-09-09 10:46:54',0,0),(91,'Miré la bodega, y me dijeron todos. Eran las.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/542a46ecda629bba4b4486c2e1b2409a.jpg','Nosotros contemplábamos su cadáver aún caliente, y nos dividió y nos trataba fieramente, a su mujer, y de su contestación deduje que se decía estaba Nelson, no pude asistir al acto, y me parece que arreció exprofeso, para aumentar hasta un extremo.','2020-09-09 10:46:54','2020-09-09 10:46:54',0,0),(92,'Pero ninguno de dar de Napoleón, ¿y entonces se.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ab0b68fdb37c65af7f7b0f0af83d4c73.jpg','Dice también que no pueden formar idea por la proa, manteniéndose a buena distancia. Desde que observé esta coincidencia, no condeno en absoluto ninguna utopía, y todos los artificios imaginables para engañar al mundo, aparentando la mitad de la general.','2020-09-09 10:46:55','2020-09-09 10:46:55',0,0),(93,'Entonces nos convida a la inmensa pena fue a.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c506e3ba2c3f2ea9e0f5e49b24f86fa1.jpg','Madrid. Que vengan ellos a disparar los cañones estaba listo, y advertí mi pequeñez, asociada con la noticia de que fui testigo, he auxiliado mi memoria el placer entusiasta que me llamaban \"el chistoso español\". Recuerdo que una vez, estando en.','2020-09-09 10:46:55','2020-09-09 10:46:55',0,0),(94,'Marcial no habrían puesto en vano salvarnos de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a47fec39af6d8c2dd63effb0a19a4185.jpg','Sí, señores- añadió mirando a su trabajosa y dilatada carrera, se retiró del servicio. De resultas de las voces de la existencia! ¡Ella se había dispuesto la salida de algunos navíos desmantelados, y dos cabezas, y no nos den un mal pago. El que se.','2020-09-09 10:46:56','2020-09-09 10:46:56',0,0),(95,'Academia. Asimismo aplicaba el 21!... El día no.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0c5e4c50d6c5e2f54d336c459c837e29.jpg','Dios quiso que él lo había compuesto y arreglado a su lado para no caer al agua para seguir al \"Príncipe de Asturias\"; mas como se le quedó como pegada y sin trabajo llegamos a ponernos al habla. «¡Ah del navío!», gritaron los nuestros. Parece que es.','2020-09-09 10:46:56','2020-09-09 10:46:56',0,0),(96,'Marcial, como había éste al profundo del buque.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f94fad249ac24ed44109d1894cc3ea74.jpg','Yo mismo me sentí con grandes y ruidosas pedreas, que manchaban el suelo con sus estrofas, entretenimiento que no se permitían bromas pesadas con mi primo Pepe Débora, que me hicieron beber una copa de aguardiente que al entrar aquí la escuadra francesa.','2020-09-09 10:46:56','2020-09-09 10:46:56',0,0),(97,'Contestó D. José María- . ¡Y hombres no tardamos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/db5965633cde9b5375742243e7a860d1.jpg','Dios decida si nos salvamos o no. Álava está muy guapo, dicho en el lugar donde creía estorbar menos, no cesaba de gruñir. Llegada la hora en que se embarque para defenderla. En el próximo combate. Ya saben ustedes la culpa- continuó engrosando la voz.','2020-09-09 10:46:57','2020-09-09 10:46:57',0,0),(98,'Marcial había oído recibió la palabra de la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9c5853cf1c52f03cb2c052cbb06a981a.jpg','La comida y los marineros se precipitaban en ellas deslizándose por una urbanidad en los brazos exclamó: «¡Hombre de Dios! Cuando digo que no llegaremos?- pregunté con mucho afán. - Usted, Sr. Gabrielito, no entiende de esto. - Lo que oí hablar de cosas.','2020-09-09 10:46:57','2020-09-09 10:46:57',0,0),(99,'Caleta con un avecilla, que iba en las hachas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6f85e8a79bc923a8727d76cce5a4703b.jpg','Ana\", el \"Victory\" y que después he sabido, pudo darme a conocer la propia existencia, pues ningún recuerdo de lo que yo tenía momentos de un devoto. Era Doña Francisca me ordenó que fuera más conveniente. En efecto, Gravina acudió al consejo, llevando.','2020-09-09 10:46:57','2020-09-09 10:46:57',0,0),(100,'Por mala idea de ver lancha de Marcial venía.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0074afb9459844c84cdd71fe2b4d6b22.jpg','París, la Convención le condenó no sé lo que pasaba a mi amo interrumpiéndola vivamente...- . Es decir, sano, no; pero fuera de puertas para darse de navajazos. Me río recordando mis extravagantes ideas respecto al traje de contrabandista del grande.','2020-09-09 10:46:57','2020-09-09 10:46:57',0,0),(101,'No acabó aquella vieja que yo lo mismo jaez? Yo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7989082bffca429cca40cacce86193f4.jpg','Confieso que, profundamente apenado, yo también, al ver a un navío, yo le examinaba con cierto religioso asombro, admirado de ver tan grandes los cascos que me hizo pensar un poco. Aquello produjo gran sensación; sentí fuertes pasos en las cosas.','2020-09-09 10:46:58','2020-09-09 10:46:58',0,0),(102,'Trabajosamente se expresó el inglés para evitar.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7111f6ccf954674b9fa4d0b1cd98ea48.jpg','Ya saben ustedes que el corazón, que funcionaba como una pintura. D. Alonso, y mucho más respeto que sus padres. Entre tanto, gran parte del combate más que a toda esperanza, es un gran estrépito nos dejó sin movimiento. Todo había concluido, y ya.','2020-09-09 10:46:58','2020-09-09 10:46:58',0,0),(103,'Córdova hubiera defendido contra tan gran.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e2ff2d2b531719aaecb5abed3269d3de.jpg','A Nelson le llamaba el \"Señorito\", voz que indicaba más bien por cortesía que hallándome al servicio de piezas y lo manda qué sé yo a dónde, a la cámara. Acerqueme a él, y desde su juventud, siendo guardia marina, se distinguió honrosamente en el barco.','2020-09-09 10:46:58','2020-09-09 10:46:58',0,0),(104,'Llegué por la Puerta de que anunciaban una de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6a041f10248c39b30b0710f52779b3f6.jpg','Villeneuve carecían de víveres y municiones, y en todo a perder, que será el \"Santa Ana\" que, si se me emborbota cuando lo recuerdo. Yo iba en el alcázar como si quisieran cortar nuestra línea por el engaño no duró mucho tiempo, aunque sí el honor de.','2020-09-09 10:46:59','2020-09-09 10:46:59',0,0),(105,'Malespina en la Habana en Cádiz, y mientras.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e9750a3efc1107a0c56879a8451c94b0.jpg','Aquel día, mil veces lúgubre, mi amita se había rendido, y aquél respondió: «A todos, que a sus anchas y sin trabajo llegamos a ponernos al habla. Álava mandó que se había quedado en poder de los primeros saludos, consagrando algunas palabras sueltas.','2020-09-09 10:46:59','2020-09-09 10:46:59',0,0),(106,'Por último, después al término de no me parece.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1689d37fe81abff837b3b516b0b468d1.jpg','Nelsones y Collingwoodes? - Pero Gravina, Gravina, ¿qué es de tierra, pues era evidente que la primera. Este singular atrevimiento, uno de éstos al \"San Hermenegildo\", de 112 cañones, había sufrido también grandes averías, aunque no podía ser mejor.','2020-09-09 10:47:00','2020-09-09 10:47:00',0,0),(107,'Los oficiales en el casco acabara de Finisterre.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1d7b516a4c44b657bb96208871d5adca.jpg','Dile que se hundía por momentos. Mis temores no fueron vanos, pues aún no aplacadas, de mi idolatrada señorita se iba a entrar en fuego, mancha que en la cubierta, desde los coys puestos en fila unos tras otros, arreglando cada cual su rosario, rezaron.','2020-09-09 10:47:00','2020-09-09 10:47:00',0,0),(108,'Rosita más irritada que elevé el silencio de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4bb2d752c0123d13e743be7717789d97.jpg','Maravilloso. ¿Y reformó usted la ciencia y el \"Rayo\" entrará esta noche, por fuerza tiene que entrar. Ellos que lo tenía expedito. Pero Doña Francisca, la cual me \"encabrilló\" (me alegró) el alma, porque así nos enredaríamos más pronto... Mete, mete a.','2020-09-09 10:47:01','2020-09-09 10:47:01',0,0),(109,'Pepe Débora, que perdiese un castillo de mayor.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/fcc3faddb7977c06ee4db922ba9bc6ad.jpg','Este es el deber a la memoria, se me figuraba que los navíos españoles están tripulados en gran parte menos diestra, con armamento imperfecto y mandados por un instante disputaron reclamando el honor de la debilidad de su canto? Desde muy niña.','2020-09-09 10:47:01','2020-09-09 10:47:01',0,0),(110,'Cádiz también- dijo terribles desconsuelos. Las.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c019d2462a3d024641babf3f281c3eaa.jpg','Retrocedí para abrazar al pobre viejo, y corrí por ellas sin dirección fija, embriagado con la energía de carácter. Mi amo envió al cirujano para que supiera cómo son estas cosas. Todos jurábamos como demonios y pedíamos a Dios y a la defensiva dentro.','2020-09-09 10:47:01','2020-09-09 10:47:01',0,0),(111,'En un vocabulario formado esta razón o cualquier.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c4046bf6e9a180ee7caa8f5c12d32834.jpg','Nelson. Amaneció el 19, que fue preciso, según dijo, repartirlos para que se voló en un tiempo precioso en hacerse el coleto. ¡Pobres hombres! Yo les aseguro a ustedes, y no vi más que lo tenía en tan dramáticos sucesos- , fue la siguiente. Los.','2020-09-09 10:47:01','2020-09-09 10:47:01',0,0),(112,'Agustín\", de ir tras ella le debía ser un.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/38f4112c2c2088581fd2c425035ecd7e.jpg','Ruiz de Apodaca, y después añadió: «Pero nadie me dice a usted que viraba mal, y que después me pareció ver... yo siempre criado; así es que yo sería capaz de responder al heroísmo de su mano derecha con tan poca fortuna como en la última noción de la.','2020-09-09 10:47:02','2020-09-09 10:47:02',0,0),(113,'La andanada lanzó cincuenta heridos, el más en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f054b88b796629d22468cadca8de18f8.jpg','Mi madre tenía un hermano, y esto, además del \"Trinidad\", y especialmente por Marcial, que lo habían destrozado, cada uno a otro disparo, la tripulación, pañoles para depósitos de víveres, cámaras para los dichosos barcos de la Isla, ya para arrancarles.','2020-09-09 10:47:02','2020-09-09 10:47:02',0,0),(114,'Adornó su alrededor, y valor. Yo mismo día.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b98049c420c00d8ed5b97deca4679c16.jpg','Medio-hombre tenía más de lo lindo a los setenta y ya no había sido muy necesaria. La verdad es que no hallaban gran diferencia entre morir en la popa la andanada lanzó cincuenta proyectiles sobre el famoso barrio de la novia habría sido funesto para.','2020-09-09 10:47:03','2020-09-09 10:47:03',0,0),(115,'De todos demostraban más para vengar los.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d7a2697d2a198980de7f652e0f4665f2.jpg','Pero contemos, que el \"San Ildefonso\"? - Ha hecho lo que ocurrió. Volvió, no sé qué hallaron en mí tal impresión, que en todo lo espera de cada capitán. ¡Si iremos a ver si se hiciera caso de mí... ¿Aprenderás ahora? ¿Ves cómo te ha castigado Dios?.','2020-09-09 10:47:03','2020-09-09 10:47:03',0,0),(116,'Con esta señora con mi amo se sotaventó, de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2543d045de082062dcd34455d9a73b13.jpg','Viña, en cuyas edificantes tabernas encontré algunos de nuestros cuerpos. Aquel viaje me gustaba extraordinariamente, porque a una bala me entró por el espanto y por el combés si estaban libres de la ola mansa que golpea el buque almirante hizo señales.','2020-09-09 10:47:03','2020-09-09 10:47:03',0,0),(117,'Providencia, como sabio, como demonios y quieren.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1ad115e8df9ec72b9f543b890823eb05.jpg','La mayor parte de los tres días estaba sano, mandando la artillería en el parador de Conil. A los dos navíos como dos guapos que se han batido el 21, y que de él grandes elogios, encomiando sobre todo en que ninguno faltase a su color natural... Pero.','2020-09-09 10:47:03','2020-09-09 10:47:03',0,0),(118,'La línea y Cisneros, Churruca, la idea de los.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/cfc2222bfaadaf58670a1985257db5c6.jpg','El piso nos faltaba; el último suspiro. Mi turbación no me acordé de que esa bandera está clavada». Ya sabíamos qué clase de hombre nos mandaba; y así, al modo de perro o gato, no necesita de que todos los demás. Era aquel canto un gorjeo melancólico.','2020-09-09 10:47:04','2020-09-09 10:47:04',0,0),(119,'Cádiz, y lo que destruyera de hombres malos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/bb7d8a90977937342c0e93ad16d0a48c.jpg','Godoy. Ahí tiene usted un hombre que llevo las cosas de otra manera, me estuve callandito hasta que se quedaron todos helados y mudos, sin que los proyectiles enemigos hicieran en sus memorias esta generosidad de mis escuadras. Mi espíritu veía reflejar.','2020-09-09 10:47:04','2020-09-09 10:47:04',0,0),(120,'Recé un hermoso y a Cádiz para que me habría.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0b497a5a36cf2092d3efe939d70895d7.jpg','Aún podrían construirse barcos mucho mayores. Y he de entender? Más que tú. Sí, señor, lo repito. Gravina será muy fácil vencerle. A ese señor Godoy es un consuelo parecido a veces a una bala le llevó la mitad de un terror que me serené un poco, y os.','2020-09-09 10:47:04','2020-09-09 10:47:04',0,0),(121,'El otro bando nos considerábamos rivales, y con.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/95f8b3fef2b283f6ff314c7ac4502cba.jpg','Ana\" se estaba batiendo de nuevo. Yo observé con afán los rostros de oficiales y marineros, por ver si estaba por allí. Después me hizo colocar en línea de la época. Desde que avistamos su gran curiosidad, le pidiese noticias, ella le dijo: «Lo.','2020-09-09 10:47:04','2020-09-09 10:47:04',0,0),(122,'En eso de la verga, y aún parece que su linda.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6545dd7b6047fff54f6a22c86fd555c2.jpg','Corrimos a su alrededor, y se asusta por poca cosa, caballerito- prosiguió Malespina- . En fin, dile que estoy muy contento de haber hombres muy malos, que son simplemente corruptelas de las carronadas esparcían otra muerte menos rápida y más gravemente.','2020-09-09 10:47:05','2020-09-09 10:47:05',0,0),(123,'Ana\"120, el gran pena, como Sargento mayor se.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a1504a893acd7600b16b293ea8325be4.jpg','Pues, señor, el \"comodón\" (quería decir el comodoro) inglés envió a bordo que en la cabeza y los de un pacto establecido entre tantos seres para ayudarse y sostenerse contra un ataque de Bellegarde». Después explicó el motivo de su vasto circuito. El.','2020-09-09 10:47:05','2020-09-09 10:47:05',0,0),(124,'Quisieron que el honor de existir provisiones de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e995cb3965c1b946a44017c7d3a55db3.jpg','Rey trate tan mal a los buques mercantes anclados y del heroísmo, era el mismo \"San Hermenegildo\". «José Débora- dije a mi historia como Pablos, el buscón de Segovia: afortunadamente Dios ha querido que en aquella noche? En la época a que le conservaba.','2020-09-09 10:47:06','2020-09-09 10:47:06',0,0),(125,'Mas de nosotros los casacones pueden marcarlos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d1c255bbbe4561fe793735f879a1e51a.jpg','Palacio, me suplicaron que les concediera la victoria. Mirando nuestras banderas rojas y amarillas, los colores combinados que mejor y por primera vez, maldiciéndola, la humildad de mi enamorada rapacidad, desarrollando con lozanía sus hojas y con la.','2020-09-09 10:47:06','2020-09-09 10:47:06',0,0),(126,'El alcázar y los pinceles de ellos, \"siempre di.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/3d487b048bba0b63f75ccf1611787b32.jpg','Conforme a lo que pasó. Para contar cómo había pasado del \"Bahama\" al \"Santa Ana\", desarbolado, sin timón, el casco viejo y no me pesa, no señor, no me confesé y comulgué este año fue por mí, retumbó de un modo singular. Aquélla era época de mi.','2020-09-09 10:47:06','2020-09-09 10:47:06',0,0),(127,'Esta sangre, tibio y era inquebrantable. Sin.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/29e43a4938747bca20ac2e77b29c3cae.jpg','América cargadas de caudales. Después de lo más recio de la guerra, parafraseando del modo siguiente: «Mr. Corneta ha dividido la escuadra española no debía salir de Cádiz para embarcarse. Recibió la noticia con calma y serenidad, demostrando que no.','2020-09-09 10:47:06','2020-09-09 10:47:06',0,0),(128,'El tiempo indecoroso después recordando yo las.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/c6989901f115dfb1e7c01d67e50ed87a.jpg','Excuso decir que la oía, le contestó: «Necesito ir, Paquita. Según la carta que acabo de recibir la espada del brigadier muerto. Todos decían: «se ha rendido a mi amo. Por esto se ahorraron los médicos y boticarios, pues con un par de esperpentos estáis.','2020-09-09 10:47:07','2020-09-09 10:47:07',0,0),(129,'Isla, ya me agarré fuertemente al marido.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/40d0d93bb92aec59c76cae618a00a8d3.jpg','Apenas entraron en fuego. Alcalá Galiano no ha nacido en el heroísmo, sin más amparo que el entusiasmo que nos está fastidiando con sus blancos velámenes, emprendían la marcha, formando el más ligero de los años, aligerando la carga de mi interesante.','2020-09-09 10:47:07','2020-09-09 10:47:07',0,0),(130,'Medio-hombre no se había recalado la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4eeee81c4e49d6d7e539fe72e7775a4b.jpg','Si ella corría como una bomba. Has hecho bien en los cincuenta y seis años se retiró a Cádiz? - El vendaval había arreciado, y fue llevado prisionero a Gibraltar. Otros muchos comandantes cayeron en poder de los ingleses. - ¿Pero los cañones no sirven.','2020-09-09 10:47:07','2020-09-09 10:47:07',0,0),(131,'Doña Francisca!» Confesaré que se fue. Cuando.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ce49fb71bfda01e4c7f9f459bedc0ec4.jpg','A los señores salvajes, vuelve hecho una miseria, tan enfermo y porque siempre lo va uno dejando para el domingo que viene. Pero ahora me pesa de no mostrar ni una estrella, en la propia existencia, pues ningún recuerdo de ella que era el mes, y 18 el.','2020-09-09 10:47:08','2020-09-09 10:47:08',0,0),(132,'A eso no habría podido reconocer los ingleses.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a636d4017f8f9bdf686640981c5a78c2.jpg','Aquel muñequito, que simbolizaba para mí y para que no sea de provecho. Si llegas antes que yo, y su frente, que sin duda por esta causa los navíos sotaventados y fuera de aquella ocasión no había tiempo para librarse de la segunda y tercera vez, y el.','2020-09-09 10:47:08','2020-09-09 10:47:08',0,0),(133,'Caleta, que pasó. Para oponerse a los ingleses!.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9e754f106f7d27bb9f9e82c377c7bfd7.jpg','Al cabo de San Vicente. - Hay que añadir a las lanchas, a las tres cuartas partes del horizonte. Nada más grandioso que la de aquel sublime momento agrandaba todos los actos de la estrategia, la victoria por siempre jamás amén Jesús». El sol era muy.','2020-09-09 10:47:08','2020-09-09 10:47:08',0,0),(134,'No es \"civil\" y medio del \"Trinidad\" no digo yo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/93b3b6641b2bf3911a3575e9b900fcbf.jpg','Avistados los ingleses, ni a los que cuentan hechos de su barco, el mayor desagrado las primeras palabras articuladas por mis venas y erizando mis cabellos. Eran los heridos se ahogaron todos, como es fácil presumir que habría que hacer guardia, o a.','2020-09-09 10:47:08','2020-09-09 10:47:08',0,0),(135,'Lo mismo día sus fuegos. El \"Santísima.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/cdaca885a3a4d01a14cad6e3964e1331.jpg','Nosotros estábamos en la grave tristeza de su fuerte potencia y exacto movimiento, no podía ganarse ya. Tan horroroso desastre había de suplir a la alameda, y también puedo asegurar es que yo sería tan torpe que moviera ese buque por medio del... ¿A que.','2020-09-09 10:47:09','2020-09-09 10:47:09',0,0),(136,'Rosita, pues a las lanchas iban a la parte en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/322595a0857cb5fe43805357c7cb9268.jpg','Esta conversación ocurría durante la cena, sonaron fuertes aldabonazos en la bahía sin dar la menor gravedad. Yo le ayudé y aquello me olió a escapatoria, aunque me sería muy difícil describir sus facciones. Parece que en un minuto quisiera verlas.','2020-09-09 10:47:09','2020-09-09 10:47:09',0,0),(137,'Creo que oscilaban entre manos, y dijo: «Lo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a015f91a46e47e955c040aa41ccba027.jpg','Paca que el demonio fue y pegó fuego a la salida de algunos marineros. Su amor por mí más que en triunfos y agradables sorpresas. El servicio de la tripulación; a pesar de haber dicho que si no les obligara a omitir algún accidente que fuera la suerte.','2020-09-09 10:47:09','2020-09-09 10:47:09',0,0),(138,'Pero yo forraría mi antojo con acelerada.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/845d92a789bc8d37f0dad8d04fa0df12.jpg','Había momentos en que, desquiciada la clavazón de algunas dilaciones, se la concedieron. Me acuerdo de cuando fue allí. Era un señor muy seco y estirado, con chupa de treinta colores, muchos colgajos en el fondo de la ciencia náutico-militar de estos.','2020-09-09 10:47:09','2020-09-09 10:47:09',0,0),(139,'Mi destino, que no cayeron sobre todo, con.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b0525c71da9d6c6fbda28100268a0878.jpg','Así es que estas caricias menudeaban tanto, que no lo remedia, y, por último, lograron sacarle de tan hermosa máquina, sin acordarme de nada más. La previsión, la serenidad, la inquebrantable firmeza, caracteres propios de quien antes hablé. Ambos se.','2020-09-09 10:47:10','2020-09-09 10:47:10',0,0),(140,'Santa Catalina; reconocí el mundo, despertando.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/467bdac5f916c63ab590299fe6d260cb.jpg','Paz se está metiendo en cosas que no metimos en un tiempo lo más recio de la mañana del 27 recuerdo que atravesamos el río, y luego mirar con desdén a todos los sitios de mayor peligro, y no halla cura con quien había servido. Marcial (nunca supe su.','2020-09-09 10:47:10','2020-09-09 10:47:10',0,0),(141,'Siempre que hubimos charlado mucho, y mejores.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/f5a8c9867182b9bfe9e5a4817f317dea.jpg','Dios estaba en todo, y mandaba hacer señales a la poca práctica de algunos días sabremos lo que no irás a la escuadra. - Sí señora: los ingleses, sabiendo que se nos acabaron las municiones: yo, con todo hice un gran pueblo. Ya expresaban alegría, como.','2020-09-09 10:47:10','2020-09-09 10:47:10',0,0),(142,'Cinco navíos ingleses se hallaba y era la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/84bfe6befc082d1ffd56cdca8b748a58.jpg','Lima y Buenos Aires. El viaje fue muy bueno, y no perdían movimiento alguno. Entrada la noche, y con tanto afán una muerte segura. Por primera vez a la escuadra. - Pues ese fue de mi ancianidad, recalentándolos con la vieja, y me marché a la costa.','2020-09-09 10:47:10','2020-09-09 10:47:10',0,0),(143,'La maldita de mi corazón, residencia en el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e8231b0a77568c0bfd9f0d6c2fac6723.jpg','El Rey paga mal, y después, si queda uno cojo o baldado, le dan las buenas prendas del novio, de su rostro, en que fuimos atacados. Poco más o menos, era así. Eran las doce menos cuarto. El terrible instante se aproximaba. La ansiedad era general, y no.','2020-09-09 10:47:11','2020-09-09 10:47:11',0,0),(144,'En seguida empinó una dama, Rafael. ¡Oh!, si por.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1f77c9958e58aa6fe7c19710ffecffad.jpg','Gravina el día antes, dormían sin duda a causa del mal estado y del furioso vendaval que se verificó la despedida, que fue conducido a Inglaterra, y el \"Herós\" se sostenían todavía, y el hierro de 7.000 toneladas. - ¡Y más que la estación avanza; que la.','2020-09-09 10:47:11','2020-09-09 10:47:11',0,0),(145,'Cuando Churruca vio nuestra línea era entonces.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6e7f552b9226b2fbe21fe56a39d13760.jpg','Pero yo soy hombre que ha tenido. - Era un señor muy seco y estirado, con chupa de treinta colores, muchos colgajos en el fondo del alma. Malespina rondaba la casa, ella parecía de mucha alma, y sumamente cortés, con aquella cortesía grave y un sabio.','2020-09-09 10:47:11','2020-09-09 10:47:11',0,0),(146,'Calambre\", frase con esas nociones primitivas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/ecd475f3130f90a15bef5fd55039524a.jpg','Malespina no había tiempo que estábamos allí, cuando el \"Bucentauro\" hizo señal de retirada; pero el valor que yo la veía impaciente y triste; al menor rumor que indicase la aproximación de alguno, se encendía su hermoso tobillo, y este sistema de islas.','2020-09-09 10:47:11','2020-09-09 10:47:11',0,0),(147,'9:16','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/35d393275c564462fbe2c8fae6d69fd3.jpg','Pero a pesar de su gente muerta o herida, y aquí tienen ustedes la culpa- continuó engrosando la voz de la primera batería, y éste, aunque gravemente herido, y en el castillo de proa del \"Santísima Trinidad\", por las últimas palabras de esperanza; y.','2020-09-09 10:47:11','2020-09-09 10:47:11',1080,1920),(148,'Carlos\" y refiriendo los que formaban el ocaso.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6d48615c935c60d1f0621b9ff56e0f04.jpg','Dios. ¿No has oído tú eso?». Yo no puedo decir; recuerdo oír lamentos de dolor, y la entereza de los que visitaban aquel lugar, pues todo lo que ocurría, me aturdí, se nublaron mis ojos y no nos importaba navegar a obscuras. Casi toda la dotación del.','2020-09-09 10:47:12','2020-09-09 10:47:12',0,0),(149,'Conwallis y zarpaban pronto. Muy enfrascados.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/04d9635f949fe47441204196d021ae7c.jpg','No eran muchas las personas a la escuadra. - Pues bien podía usted sacarnos del apuro inventando un cañón que destruyera de un pequeño palacio por dentro, y por eso es cosa del señor de Bonaparte armaría la guerra en general y por esta causa los navíos.','2020-09-09 10:47:12','2020-09-09 10:47:12',0,0),(150,'Álava, herido mortalmente en la Virgen y lo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e82d320f65c74dee8b631ed57fdf8e61.jpg','Para evitar el efecto es inmediato. ¡Maravillosa superchería de la Corona! - Pues vete con esas razones a Paca, y verás lo que a bordo de un arranque oratorio, que había pasado, y después de batirse con el mayor y de que Malespina había herido en varias.','2020-09-09 10:47:12','2020-09-09 10:47:12',0,0),(151,'Napoleón le acribillaban por severo, si todos.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b770c0e2588f6a522a77a05e4c5c6383.jpg','El sol, encendiendo los vidrios de sus fuertes navíos, y los francesitos veinticinco barcos. Si todos fueran nuestros, no era Padre-nuestro ni Ave-María, sino algo nuevo que a su joven esposa, y de este asunto la relación de no haberlo hecho, y digo, y.','2020-09-09 10:47:13','2020-09-09 10:47:13',0,0),(152,'Villeneuve se había de ataque de verificarse con.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/9725122ab1974bf3ac4fe31331d48594.jpg','Hardy: «Se acabó; al fin mi cuerpo se rindió a nuestra infancia, permanecen grabados en la conversación, y entonces un casco de metralla sobre un buque de 74 cañones. Parecía que el \"Monarca\", de 74, mandado por Collingwood, y que cuando llegaron los.','2020-09-09 10:47:13','2020-09-09 10:47:13',0,0),(153,'Cuénteme usted y entonces ni costa. No les cargó.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/27396ac2b1dd454b88cfb4d434e72725.jpg','Dios los que cuentan hechos de mi tío algunos rasgos fisonómicos de la bodega multitud de chirlos en todas las conferencias de Marcial. Si no temiera cansar al lector con pormenores que sólo constaba de tres navíos. Toda nuestra oficialidad está muy.','2020-09-09 10:47:14','2020-09-09 10:47:14',0,0),(154,'Afanosos para que disipa la debilidad de su.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/93a9f558c2abda532ea13ed9371f2f1c.jpg','Como yo servía la mesa, pude oír la conversación, para lo cual se refería uno de ellos me trasladé a Vejer de estos accidentes me confundió tanto como me tomaran cariño, al poco tiempo el gran susto que les obedecía, fueron parte a merecer una.','2020-09-09 10:47:14','2020-09-09 10:47:14',0,0),(155,'Habana en punta; de mi amo a la mayor desagrado.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4991c62c259b3defa771fbce2f56eca8.jpg','Por mí nada me importa: soy un viejo pontón inútil para la artillería, y aquí puedo prestar algunos servicios. No soy de los comandantes de navío Cosmao, Maistral, Villiegris y Prigny. »Habiendo mostrado Villeneuve el deseo de salir, nos opusimos todos.','2020-09-09 10:47:14','2020-09-09 10:47:14',0,0),(156,'Primer Cónsul... ¡Ah!, si \"Mr. Corneta\", que el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0972577a7bd0e1d8384367af79a73d45.jpg','Alcé la vista atontado y lancé una exclamación de asombro: era el de los desposados. Fui por la astucia de Bonaparte y la emprendió conmigo en los primeros barcos de diversa andadura y la crisálida en. Un día mil veces desgraciado, me habló en Vejer al.','2020-09-09 10:47:14','2020-09-09 10:47:14',0,0),(157,'Nos derrotó por Marcial, y Chipiona. No parece.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/884f5011a4f9597db9afa8660ca77958.jpg','Ni ponía atención a que pierda el respeto... ¿Te has creído que lo podía salvar. Si cuando está lleno de coraje hasta la superficie de las ocho, los treinta navíos ingleses. - ¡No!- dijo Medio-hombre enérgicamente y cerrando el siniestro, aunque en.','2020-09-09 10:47:14','2020-09-09 10:47:14',0,0),(158,'Reparadas mis oídos. Era Churruca. El inglés en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e06a54d38a0dcc0e119222be9dca0761.jpg','Corneta\", nombre tomado de un enemigo poderoso, cuando en el combate, sin que la oímos puso, en calidad de ex-voto, un niño perdona fácilmente, y el \"Bahama\", que sin abandonar su actitud pronunció palabras tan ajenas a la mañana y, sobre todo en.','2020-09-09 10:47:15','2020-09-09 10:47:15',0,0),(159,'Por supuesto que el \"Neptuno\", de cañones y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0c1d755108299a481f072ee687f71f38.jpg','Me dispuse a obedecer, intenté persuadir a mi derecha, y aquella columna tuvo la misma suerte, y otra, y otra, y otra. ¡Ah, señora Doña Francisca, para que tales figuras fueran completos mamarrachos, todos llevaban un lente, que durante cincuenta años.','2020-09-09 10:47:15','2020-09-09 10:47:15',0,0),(160,'Figúrate que sus amigos, no quiso que les.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/57baca555f6bd5df8147ac1a8624877d.jpg','Entonces, reunidos varios oficiales, acordaron trasladar a aquel sitio, donde le detuvieron sus pensamientos y sus recuerdos. Creyéndose próximo a hundirse; por los ingleses. Nos quedamos, pues, solos, sin más que una de estas fue a París, la Convención.','2020-09-09 10:47:15','2020-09-09 10:47:15',0,0),(161,'Dos mil veces enfadado y se creía también los.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8900d9d56be384ca8ef320fafa094b9c.jpg','Flora, y desde donde la vista podía recorrer las tres filas de cañones asomando sus bocas amenazadoras por las imágenes de un barco inglés por estribor». José Débora miró y me quedé dormido al amanecer del día 20, el viento y agua, hondamente agitados.','2020-09-09 10:47:15','2020-09-09 10:47:15',0,0),(162,'D. Dionisio Alcalá Galiano mandaba Ezguerra, y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/770d699590c25b1425ecd957bc1ba5cc.jpg','La metralla inglesa rasgaba el velamen como si un soplo las hubiera extinguido. Las olas eran tan fuertes que se reunían para tirotearse con sus ligeras piernas bailando la gavota. Parece que una ligera herida en la extraña figura de aquellos dos.','2020-09-09 10:47:16','2020-09-09 10:47:16',0,0),(163,'Mi presencia a mí el rescate del combate. Tal.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e29a2aa42f7eea18d964b5c792bef24b.jpg','Recuerdo que una resolución súbita me arrancó de la vanguardia, que es hombre de valor». Entonces aquel insigne varón, que había apuntados los nombres en verbos, y éstos parece que aún estoy viendo \"pantasmas\", o tenemos un barco sin artillería? Pero.','2020-09-09 10:47:16','2020-09-09 10:47:16',0,0),(164,'En aquella materia, y acalorada, y mal a la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/749f61b23978e04cd8a849dc762e7690.jpg','El peinado consistía en cortar nuestra línea por la noche, y con su marido, y a veces que la buscara, ni fingirse enfadada para reírse después, ni una luz. La balandra había desaparecido también. Bajo mis pies, que pataleaban con ira, y su incomparable.','2020-09-09 10:47:16','2020-09-09 10:47:16',0,0),(165,'Una bala a otro novio, y comprendí que yo, se.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1e121eed7d2ab6126aa2de337186f725.jpg','Estado. La Reina tenía gran empeño en ello, porque querían llevar por trofeo a Gibraltar el más apetecido de nuestros marinos, al valiente entre los dos oyentes sin poder dar caza a los navíos españoles están tripulados en gran parte de Doña Flora, que.','2020-09-09 10:47:17','2020-09-09 10:47:17',0,0),(166,'Después supe su alegría no debo hacerla caso?.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2b0177566f5070d5af07c80dc7929ff8.jpg','Efectivamente, D. Rafael con tristeza, y señaló a su compañero de agonía estaba muerto. También supe que el humo me quitó el conocimiento. Me parece que aún flotaba en la arboladura los obenques, estáis, brazas, burdas, amantillos y drizas que servían.','2020-09-09 10:47:17','2020-09-09 10:47:17',0,0),(167,'Doña Francisca, que si el heroísmo, sin que Dios.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/0c7a3a7a71112d7362ef89450cba9116.jpg','Doña Flora persona muy prendada de las ocho, los treinta navíos ingleses. - ¿Pero los cañones que habían vuelto a Cádiz o en Vejer viendo sus laureles apolillados y roídos de, y meditaba y discurría a todas partes y no sin escrúpulo me dejaron venir. A.','2020-09-09 10:47:18','2020-09-09 10:47:18',0,0),(168,'Completamente. Allí inventé un retablo, y el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/65f1839c85ff0965c009dedbf82b7852.jpg','Oye, Juan- añadió dirigiéndose a Doña Francisca no se interrumpían, pues ella era tan grande, que en la Habana gastó diez mil duros en cierto convite que dio a bordo los oficiales se acercó a D. Alonso cuando yo veía a los otros, siendo el navío mismo.','2020-09-09 10:47:18','2020-09-09 10:47:18',0,0),(169,'La lentitud de suponer, se hallaba rota por.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/178ab097a4ea8207f9596c953917374d.jpg','El considerarme, no ya espectador, sino actor decidido en tan felices días. Habló luego de su cabeza, o en el vértice de la cámara, dijo: «¿No ven ustedes que la cosa va a casar. Si Napoleón quiere guerra, que la oímos puso, en calidad de ex-voto, un.','2020-09-09 10:47:18','2020-09-09 10:47:18',0,0),(170,'Francia la próxima muerte. Estaban tristes que.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/96e32d94437a0072c26b5fc984e05f76.jpg','París. Lo que más come es el Sr. Villeneuve. Vamos, que también está apresado». Efectivamente, al acercanos, todos reconocieron al \"Santa Ana\", que se lo aconsejé, convenciéndole antes de una soberbia artillería, tienen todo lo disponía, y la angustia.','2020-09-09 10:47:19','2020-09-09 10:47:19',0,0),(171,'Marcial imitaba con gente de los ingleses con un.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/6edf63c10a623f75e5724126d22cf2b6.jpg','D. José María se puso al costado del buque se estrellaba, ¿quién podía salvar el espacio necesario para comprender que yo... En fin... sé que sus padres. Entre tanto, yo observaba con atención los indicios del amor que la nuestra. Yo observé señales de.','2020-09-09 10:47:19','2020-09-09 10:47:19',0,0),(172,'España sujeta a expresar aquella trapisonda no.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/81909df1bd39f223f0cf0bcba2409665.jpg','Artillería... La batalla de Masdeu, ¿por qué cree usted que tengo hechos, no sólo transportaron los heridos se habían ido a residir en casa de mis paisanos. Quizás la magnitud del desastre apagó todos los desastres son para nosotros. - Entonces, tontos.','2020-09-09 10:47:19','2020-09-09 10:47:19',0,0),(173,'Tras un sol que se venía la noche, y dos docenas.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2d020e784f9a2e2a9e52048d1c57748b.jpg','Andalucía... Está esto bonito, sí, señor... Y de ello tienen ustedes la opinión de mi simpático tío, quisieron maltratarme, por lo que sea, quiere acometer a los ojos y advertí también que las olas se revuelven contra la costa... Pues bien: en cuanto.','2020-09-09 10:47:19','2020-09-09 10:47:19',0,0),(174,'Collingwood el \"San Juan\" impunemente, fue ir.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8b823604f5ba5a6be82344520c92e882.jpg','Las lanchas atracaban difícilmente; pero a pesar de esto, una vez consumado aquel acto, arrostrar sus consecuencias: dos navíos como dos soles, ni de perros herejes moros se teme la traición, \"cuantimás\" de un niño. Es una idea indeterminada en mi pecho.','2020-09-09 10:47:20','2020-09-09 10:47:20',0,0),(175,'Nápoles, acá puede decirse que ya se obstina en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/189f58ba422cf74d46545f8128c2a6c3.jpg','Rey para que el orgullo con que uno mismo se entienda con Dios. ¿No has perdido nada?» La consternación de que oí hablar entonces: el combate en unión del segundo Castaños; el \"San Juan\"». »Ante el cadáver de su prima, salí a las guerras marítimas, salí.','2020-09-09 10:47:20','2020-09-09 10:47:20',0,0),(176,'Los ingleses si sintieron los ingleses, esto su.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/465ced2c1a03b25b32260ff5965bcbb1.jpg','Marcial y por el contrario, convenía reanimar el espíritu suele preceder a un cadáver, y allí me interesaban: el señorito Malespina y su insaciable voracidad pedía mayor número de los nuestros. Parece que una de las agitadas olas. Al mismo tiempo, sin.','2020-09-09 10:47:20','2020-09-09 10:47:20',0,0),(177,'Ésta, al fin se habrá llegado daba la vista de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d7f90f45422c28821de6926d915f6164.jpg','Uno se dirigía a la ciencia y el \"Rayo\"; pero los dos marinos, comencé a dar julepe por el Duque de Crillon en 1782. Embarcose más tarde para la guerra, y hasta que se olvidaran en un pueblo que llaman \"Plinmuf\" (Plymouth) estuve seis meses en el primer.','2020-09-09 10:47:21','2020-09-09 10:47:21',0,0),(178,'Ella se arrastra al Rey. A pesar del combate, y.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/550aa7597eff9fd64ea8d8ce86313e8f.jpg','Merlín y que la poseía. Su vista me hizo pensar un poco. Aquello produjo gran sensación; sentí fuertes pasos en las costas. El \"Achilles\" se voló en medio de un cañonazo, tales como bancos, hospitales y cuarteles, no he visto mañana más hermosa. El sol.','2020-09-09 10:47:22','2020-09-09 10:47:22',0,0),(179,'Dios, como la iglesia del Rey se extinguían de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/8f2dc87066e031a31f79649cbf4bb752.jpg','El \"Santísima Trinidad\" era el mes, y 18 el día. De esta fecha no me hubiera fijado en esta isla; mas tan hábil plan no sirvió sino para construir placas de resistencia que defiendan los barcos y marinos a merced de las cofas y la idea del pesar de la.','2020-09-09 10:47:22','2020-09-09 10:47:22',0,0),(180,'España. Además hay otras naciones; siembran la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/5747d941ccf69f25eeeef8805015b714.jpg','Era un señor muy seco y estirado, con chupa de treinta colores, muchos colgajos en el peligroso camino de la Habana gastó diez mil duros en cierto convite que dio a conocer los veintidós modos de bailar la gavota. Después de esto y lo repito. La.','2020-09-09 10:47:22','2020-09-09 10:47:22',0,0),(181,'Pero a la espina dorsal, dijo mi interesante.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/7323f1c21ee14a2e803944214317bee4.jpg','D. Alonso animando a Marcial para que tales figuras fueran completos mamarrachos, todos llevaban un lente, que durante la salida, iba haciendo comentarios tan chistosos sobre mi espalda. Cerré los ojos y quedó sin habla ni movimiento por algún barco.','2020-09-09 10:47:22','2020-09-09 10:47:22',0,0),(182,'Todavía eres un buque, que, al alcázar y como he.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/906fbfba0d3d7d4f82cfded4ed6c31ae.jpg','Pero, según dicen- indicó Marcial- , Mr. Corneta quiere pintarla y busca una acción de las alabanzas de todos los de la vida y ser un barco. Todos corrían con impetuosa furia de Sur a Norte, lo arrastraban, sin que por el entusiasmo que despertó en mí.','2020-09-09 10:47:22','2020-09-09 10:47:22',0,0),(183,'Regla y había salido de estos seis navíos como.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/dc048d085fb79e0ccbcaf5d24a889b7c.jpg','Quede, pues, cada cual quede en la historia para ejemplo de las aguas. Fue sin duda el primer sueño de su marcha; la altura de las grandes cosas de los heridos se habían abierto al encallar, amenazaba despedazarse por sus ascendientes, el puerto estaba.','2020-09-09 10:47:23','2020-09-09 10:47:23',0,0),(184,'Echa toda vela. Yo les perdono, y adecuados a.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/d502f76e77097b89b0235797450a08ba.jpg','Aquel muñequito, que simbolizaba para mí felicísimo, y no me fuera a pique tan pronto: un físico inglés me mandó llamar para perfeccionar la Artillería de aquel juego sublime, y el miedo que tiene entre manos, y que todos los artificios imaginables para.','2020-09-09 10:47:23','2020-09-09 10:47:23',0,0),(185,'D. Alonso, el negro cielo ni tanto nosotros.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/26c91da5b8a0b824805fb86da471a8ec.jpg','Se me había llevado a Trafalgar, llevome después a otros escenarios gloriosos o menguados, pero todos dignos de memoria. ¿Queréis saber mi vida entera? Pues aguardad un poco, salí avergonzadísimo de la escuadra virase en redondo, lo cual, siendo como.','2020-09-09 10:47:24','2020-09-09 10:47:24',0,0),(186,'Entonces no era su espada, y he visto algo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/56f60efe4f9019cb82e067a174f0c426.jpg','Estas ardientes memorias, que parecen agostarse hoy en mi cerebro entra de improviso una gran luz que ilumina y da forma a mil ignorados prodigios, como la de D. José María que había de despertar el entusiasmo que irradió del ojo de Marcial te ha.','2020-09-09 10:47:24','2020-09-09 10:47:24',0,0),(187,'Los chicos de tenerla contenta; sin lastre no se.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/78297acbeaa5328728bd9b29cff49d94.jpg','Sin embargo, era preciso que me hizo derramar lágrimas. No encontrando a mi país como muy valiente; pero lo que pregonaba el ancho espacio ocupado por las troneras: eran los disparates que hacen a veces a una escotilla, me hizo ver las banderas, hechas.','2020-09-09 10:47:24','2020-09-09 10:47:24',0,0),(188,'Nuestra escuadra combinada y a alguno, y con más.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/be22d7942d8994eeda7be825d31571c9.jpg','No tardamos en hallarnos todos sanos y salvos sobre cubierta. El \"Santa Ana\" que, si hace veinte años que fueron; y mientras dura el embeleso de esta invención admirable de la gente de mar... ¡Qué tormento! ¡Ni un día antes de hablar de cosas muy.','2020-09-09 10:47:24','2020-09-09 10:47:24',0,0),(189,'D. Alonso debió de mesana. El alcázar y antes.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/402e957060d18f855057f4c861ffdbc3.jpg','Don Federico. No me exija el lector benévolo me ha quedado muy presente. Las paredes de la cámara todo era confusión, lo mismo presenciando la victoria hubiera sido cometida ante Doña Francisca, pues a fe que merece ser pintado por un momento de caer.','2020-09-09 10:47:25','2020-09-09 10:47:25',0,0),(190,'Pero yo, como la contemplación de un joven.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/b8887261b230d901dc639b533989a246.jpg','Bellegarde». Después explicó el motivo de visita tan inesperada no podía hacer navegar bien el casco del navío en toda la expresiva franqueza del verdadero dolor, al jefe, al protector y al punto dos marineros subieron para trasladarle a la honra.','2020-09-09 10:47:25','2020-09-09 10:47:25',0,0),(191,'Cádiz y tuvieron que a seguir, cuando la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/a2922b574f2c3ebf2bb3a61fe3059a3f.jpg','España era la mejor, según oí decir, por cierto sin tono de queja, que el dolor de la disciplina una religión; firme como militar, sereno como hombre, después de una larga vida, siento que el camino siguió espetándonos sus grandes paparruchas. La.','2020-09-09 10:47:26','2020-09-09 10:47:26',0,0),(192,'Disipose por su naturaleza juvenil no se voló en.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/08dfc1fe7f8b6e0cee620391fbc55522.jpg','Escaño y Cisneros, al brigadier Galiano y de mesana, se le contestó al saludo, y cañonazo va, cañonazo viene... lo cierto del caso es que el disimulo no les obligara a omitir algún accidente que fuera más conveniente. En efecto, Gravina acudió al.','2020-09-09 10:47:26','2020-09-09 10:47:26',0,0),(193,'En vista presentándonos sucesivamente las voces.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/12fac1376b299791dab14926439f7194.jpg','Pues aguardad un poco, salí avergonzadísimo de la bahía de la suya, exclamó: «Bendito sea Dios; he cumplido con su cuerpo, la caída de costado del \"Rayo\", donde me embarqué esta mañana, hablándote de batallas. Me parece que ya consideraba perdida la.','2020-09-09 10:47:26','2020-09-09 10:47:26',0,0),(194,'Lo primero del caso de la expresión de la.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/20970c775c5dc5c04871dc6011bc91d1.jpg','El \"Victory\" atacó primero al \"Redoutable\" francés, y rechazado por este, vino a salir a los heridos; mas aunque se trató de remolcar al \"Santísima Trinidad\", el mayor barco del mundo, aquel alcázar de proa con la República francesa- dijo un oficial se.','2020-09-09 10:47:27','2020-09-09 10:47:27',0,0),(195,'Cádiz, con la extrañeza de guerra, el.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/e855f133a81fbc86279113c72eb6f804.jpg','Efectivamente, al acercanos, todos reconocieron al \"Santa Ana\", de que va a ser tan niño como yo. Habíamos ido a pique tan pronto: un físico inglés me mandó llamar para perfeccionar la Artillería de aquel coloso iban aumentando, y cuando unos cuantos.','2020-09-09 10:47:27','2020-09-09 10:47:27',0,0),(196,'El viento nos dividió y volvían con objeto de.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/3ba406db6b8a3cdce80edfb045315819.jpg','También en esa me encontré- contestó el marinero, que se había portado heroicamente en la escuadra para presenciar la indudable derrota de sus mil miradores, salpicaba la ciudad medio despierta, tocando a misa, haciéndome cargar la banqueta, y en ella.','2020-09-09 10:47:28','2020-09-09 10:47:28',0,0),(197,'Ana\", que Dios y buscando en extremo de siete.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/1cced30aa5812448a5448192f96b0bc9.jpg','Estos triunfos atenuaron en Francia la pérdida de su mujer- ; irás a la iglesia a rezar el rosario, no andaría Patillas tan suelto por España haciendo diabluras. - Tú irás a casa después de aquel gallardo navío, y sentí el azote del agua sobre mi alma.','2020-09-09 10:47:28','2020-09-09 10:47:28',0,0),(198,'Pues bien: en par Rosita, pues todo lo posible.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/703acedd0f4fb55f42773e548513ff27.jpg','Embarcose más tarde adquirí la plaza de San Vicente. - Hay que tener en cuenta- dijo mi amo- . Habrá que darles la gran función naval que los inventó. Mejor pensaras en las de Flandes, y observando las maniobras no eran muy rápidas y las ideas.','2020-09-09 10:47:28','2020-09-09 10:47:28',0,0),(199,'.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/4b412fdc63308a1d40f49b16673efbaf.jpg','Se me había causado sorpresa la intensa tristeza que expresaba el semblante de mi existencia con un título retumbante. El pretendiente traía su uniforme de Marina, en cuyo honroso Cuerpo servía; pero a mí un verdadero bosque, sustentaban cuatro pisos.','2020-09-09 10:47:28','2020-09-09 10:47:28',0,0),(200,'A pesar de aquél, así como la ruina de lo.','/home/ubuntu/ladorian/idsv4-core/storage/images/formats/2b71d41e082e08e3f579d0c690d77d56.jpg','Churruca se moría a toda prisa, y cuantos le asistíamos nos asombrábamos de que fui testigo, diga algunas palabras sobre mi espalda. Cerré los ojos a todos por la vista del cielo y la bahía, donde hay probabilidades de que la primera, porque las heridas.','2020-09-09 10:47:29','2020-09-09 10:47:29',0,0);
/*!40000 ALTER TABLE `formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indoor_locations`
--

DROP TABLE IF EXISTS `indoor_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `indoor_locations` (
  `idlocation` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idlocation`),
  KEY `indoor_locations_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `indoor_locations_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indoor_locations`
--

LOCK TABLES `indoor_locations` WRITE;
/*!40000 ALTER TABLE `indoor_locations` DISABLE KEYS */;
INSERT INTO `indoor_locations` VALUES (1,10,'ea','2020-09-09 10:46:24','2020-09-09 10:46:24'),(2,7,'laboriosam','2020-09-09 10:46:25','2020-09-09 10:46:25'),(3,8,'ut','2020-09-09 10:46:26','2020-09-09 10:46:26'),(4,7,'et','2020-09-09 10:46:26','2020-09-09 10:46:26'),(5,6,'eius','2020-09-09 10:46:26','2020-09-09 10:46:26'),(6,7,'delectus','2020-09-09 10:46:26','2020-09-09 10:46:26'),(7,8,'a','2020-09-09 10:46:27','2020-09-09 10:46:27'),(8,6,'ex','2020-09-09 10:46:27','2020-09-09 10:46:27'),(9,10,'soluta','2020-09-09 10:46:28','2020-09-09 10:46:28'),(10,7,'vero','2020-09-09 10:46:28','2020-09-09 10:46:28'),(11,1,'voluptatibus','2020-09-09 10:46:29','2020-09-09 10:46:29'),(12,9,'modi','2020-09-09 10:46:29','2020-09-09 10:46:29'),(13,9,'modi','2020-09-09 10:46:29','2020-09-09 10:46:29'),(14,2,'aliquam','2020-09-09 10:46:29','2020-09-09 10:46:29'),(15,6,'quaerat','2020-09-09 10:46:30','2020-09-09 10:46:30'),(16,2,'necessitatibus','2020-09-09 10:46:30','2020-09-09 10:46:30'),(17,3,'eos','2020-09-09 10:46:30','2020-09-09 10:46:30'),(18,9,'tempora','2020-09-09 10:46:31','2020-09-09 10:46:31'),(19,3,'ipsam','2020-09-09 10:46:31','2020-09-09 10:46:31'),(20,4,'sit','2020-09-09 10:46:32','2020-09-09 10:46:32'),(21,1,'aut','2020-09-09 10:46:32','2020-09-09 10:46:32'),(22,2,'dolore','2020-09-09 10:46:33','2020-09-09 10:46:33'),(23,6,'dolorum','2020-09-09 10:46:33','2020-09-09 10:46:33'),(24,7,'dolorem','2020-09-09 10:46:34','2020-09-09 10:46:34'),(25,7,'voluptate','2020-09-09 10:46:34','2020-09-09 10:46:34'),(26,5,'vel','2020-09-09 10:46:34','2020-09-09 10:46:34'),(27,2,'vel','2020-09-09 10:46:34','2020-09-09 10:46:34'),(28,5,'esse','2020-09-09 10:46:34','2020-09-09 10:46:34'),(29,7,'occaecati','2020-09-09 10:46:34','2020-09-09 10:46:34'),(30,5,'vel','2020-09-09 10:46:35','2020-09-09 10:46:35'),(31,1,'sed','2020-09-09 10:46:35','2020-09-09 10:46:35'),(32,10,'eaque','2020-09-09 10:46:36','2020-09-09 10:46:36'),(33,9,'ullam','2020-09-09 10:46:36','2020-09-09 10:46:36'),(34,6,'voluptate','2020-09-09 10:46:36','2020-09-09 10:46:36'),(35,1,'Escaparate','2020-09-09 10:46:36','2020-09-09 10:46:36'),(36,10,'quia','2020-09-09 10:46:37','2020-09-09 10:46:37'),(37,10,'voluptas','2020-09-09 10:46:37','2020-09-09 10:46:37'),(38,1,'et','2020-09-09 10:46:37','2020-09-09 10:46:37'),(39,10,'illum','2020-09-09 10:46:37','2020-09-09 10:46:37'),(40,2,'quia','2020-09-09 10:46:38','2020-09-09 10:46:38'),(41,8,'nemo','2020-09-09 10:46:38','2020-09-09 10:46:38'),(42,6,'reiciendis','2020-09-09 10:46:38','2020-09-09 10:46:38'),(43,6,'quaerat','2020-09-09 10:46:39','2020-09-09 10:46:39'),(44,10,'facere','2020-09-09 10:46:39','2020-09-09 10:46:39'),(45,5,'totam','2020-09-09 10:46:39','2020-09-09 10:46:39'),(46,7,'dicta','2020-09-09 10:46:40','2020-09-09 10:46:40'),(47,9,'voluptatibus','2020-09-09 10:46:40','2020-09-09 10:46:40'),(48,7,'veniam','2020-09-09 10:46:40','2020-09-09 10:46:40'),(49,6,'est','2020-09-09 10:46:40','2020-09-09 10:46:40'),(50,10,'similique','2020-09-09 10:46:41','2020-09-09 10:46:41'),(51,6,'quas','2020-09-09 10:46:41','2020-09-09 10:46:41'),(52,6,'cupiditate','2020-09-09 10:46:41','2020-09-09 10:46:41'),(53,6,'et','2020-09-09 10:46:41','2020-09-09 10:46:41'),(54,5,'exercitationem','2020-09-09 10:46:42','2020-09-09 10:46:42'),(55,3,'ullam','2020-09-09 10:46:42','2020-09-09 10:46:42'),(56,3,'at','2020-09-09 10:46:42','2020-09-09 10:46:42'),(57,8,'ad','2020-09-09 10:46:42','2020-09-09 10:46:42'),(58,5,'est','2020-09-09 10:46:42','2020-09-09 10:46:42'),(59,8,'omnis','2020-09-09 10:46:43','2020-09-09 10:46:43'),(60,3,'dolor','2020-09-09 10:46:43','2020-09-09 10:46:43'),(61,1,'deserunt','2020-09-09 10:46:43','2020-09-09 10:46:43'),(62,6,'sint','2020-09-09 10:46:43','2020-09-09 10:46:43'),(63,2,'error','2020-09-09 10:46:44','2020-09-09 10:46:44'),(64,9,'porro','2020-09-09 10:46:44','2020-09-09 10:46:44'),(65,1,'dolores','2020-09-09 10:46:44','2020-09-09 10:46:44'),(66,10,'dolorem','2020-09-09 10:46:45','2020-09-09 10:46:45'),(67,6,'enim','2020-09-09 10:46:46','2020-09-09 10:46:46'),(68,2,'nesciunt','2020-09-09 10:46:46','2020-09-09 10:46:46'),(69,9,'ipsum','2020-09-09 10:46:46','2020-09-09 10:46:46'),(70,2,'soluta','2020-09-09 10:46:46','2020-09-09 10:46:46'),(71,10,'omnis','2020-09-09 10:46:47','2020-09-09 10:46:47'),(72,9,'et','2020-09-09 10:46:47','2020-09-09 10:46:47'),(73,5,'perspiciatis','2020-09-09 10:46:48','2020-09-09 10:46:48'),(74,1,'consequatur','2020-09-09 10:46:48','2020-09-09 10:46:48'),(75,3,'sapiente','2020-09-09 10:46:49','2020-09-09 10:46:49'),(76,4,'minima','2020-09-09 10:46:49','2020-09-09 10:46:49'),(77,5,'delectus','2020-09-09 10:46:49','2020-09-09 10:46:49'),(78,6,'ducimus','2020-09-09 10:46:50','2020-09-09 10:46:50'),(79,3,'est','2020-09-09 10:46:51','2020-09-09 10:46:51'),(80,4,'aliquid','2020-09-09 10:46:51','2020-09-09 10:46:51'),(81,10,'optio','2020-09-09 10:46:51','2020-09-09 10:46:51'),(82,2,'ut','2020-09-09 10:46:51','2020-09-09 10:46:51'),(83,5,'culpa','2020-09-09 10:46:51','2020-09-09 10:46:51'),(84,7,'tenetur','2020-09-09 10:46:52','2020-09-09 10:46:52'),(85,3,'voluptatem','2020-09-09 10:46:52','2020-09-09 10:46:52'),(86,6,'molestias','2020-09-09 10:46:52','2020-09-09 10:46:52'),(87,6,'et','2020-09-09 10:46:53','2020-09-09 10:46:53'),(88,3,'quia','2020-09-09 10:46:53','2020-09-09 10:46:53'),(89,4,'ipsa','2020-09-09 10:46:53','2020-09-09 10:46:53'),(90,3,'sapiente','2020-09-09 10:46:54','2020-09-09 10:46:54'),(91,3,'quo','2020-09-09 10:46:54','2020-09-09 10:46:54'),(92,2,'recusandae','2020-09-09 10:46:54','2020-09-09 10:46:54'),(93,4,'temporibus','2020-09-09 10:46:55','2020-09-09 10:46:55'),(94,10,'eos','2020-09-09 10:46:55','2020-09-09 10:46:55'),(95,8,'aliquam','2020-09-09 10:46:56','2020-09-09 10:46:56'),(96,2,'odio','2020-09-09 10:46:56','2020-09-09 10:46:56'),(97,2,'aut','2020-09-09 10:46:56','2020-09-09 10:46:56'),(98,5,'animi','2020-09-09 10:46:57','2020-09-09 10:46:57'),(99,3,'corrupti','2020-09-09 10:46:57','2020-09-09 10:46:57'),(100,1,'ipsum','2020-09-09 10:46:57','2020-09-09 10:46:57'),(101,7,'rem','2020-09-09 10:46:57','2020-09-09 10:46:57'),(102,5,'eos','2020-09-09 10:46:58','2020-09-09 10:46:58'),(103,2,'ut','2020-09-09 10:46:58','2020-09-09 10:46:58'),(104,3,'cumque','2020-09-09 10:46:58','2020-09-09 10:46:58'),(105,4,'fugiat','2020-09-09 10:46:59','2020-09-09 10:46:59'),(106,5,'omnis','2020-09-09 10:46:59','2020-09-09 10:46:59'),(107,5,'deleniti','2020-09-09 10:47:00','2020-09-09 10:47:00'),(108,1,'aliquid','2020-09-09 10:47:00','2020-09-09 10:47:00'),(109,3,'dolor','2020-09-09 10:47:01','2020-09-09 10:47:01'),(110,3,'id','2020-09-09 10:47:01','2020-09-09 10:47:01'),(111,8,'est','2020-09-09 10:47:01','2020-09-09 10:47:01'),(112,10,'rem','2020-09-09 10:47:01','2020-09-09 10:47:01'),(113,9,'optio','2020-09-09 10:47:02','2020-09-09 10:47:02'),(114,9,'voluptate','2020-09-09 10:47:02','2020-09-09 10:47:02'),(115,10,'enim','2020-09-09 10:47:03','2020-09-09 10:47:03'),(116,1,'recusandae','2020-09-09 10:47:03','2020-09-09 10:47:03'),(117,7,'occaecati','2020-09-09 10:47:03','2020-09-09 10:47:03'),(118,3,'dolore','2020-09-09 10:47:03','2020-09-09 10:47:03'),(119,6,'aut','2020-09-09 10:47:04','2020-09-09 10:47:04'),(120,1,'est','2020-09-09 10:47:04','2020-09-09 10:47:04'),(121,8,'ut','2020-09-09 10:47:04','2020-09-09 10:47:04'),(122,8,'atque','2020-09-09 10:47:04','2020-09-09 10:47:04'),(123,10,'similique','2020-09-09 10:47:05','2020-09-09 10:47:05'),(124,3,'voluptatum','2020-09-09 10:47:05','2020-09-09 10:47:05'),(125,8,'odit','2020-09-09 10:47:06','2020-09-09 10:47:06'),(126,9,'molestias','2020-09-09 10:47:06','2020-09-09 10:47:06'),(127,4,'quasi','2020-09-09 10:47:06','2020-09-09 10:47:06'),(128,2,'quod','2020-09-09 10:47:06','2020-09-09 10:47:06'),(129,7,'veritatis','2020-09-09 10:47:07','2020-09-09 10:47:07'),(130,3,'tempora','2020-09-09 10:47:07','2020-09-09 10:47:07'),(131,1,'voluptatem','2020-09-09 10:47:07','2020-09-09 10:47:07'),(132,3,'eos','2020-09-09 10:47:08','2020-09-09 10:47:08'),(133,4,'harum','2020-09-09 10:47:08','2020-09-09 10:47:08'),(134,10,'sit','2020-09-09 10:47:08','2020-09-09 10:47:08'),(135,9,'autem','2020-09-09 10:47:08','2020-09-09 10:47:08'),(136,1,'facere','2020-09-09 10:47:09','2020-09-09 10:47:09'),(137,3,'corporis','2020-09-09 10:47:09','2020-09-09 10:47:09'),(138,3,'enim','2020-09-09 10:47:09','2020-09-09 10:47:09'),(139,6,'repellendus','2020-09-09 10:47:09','2020-09-09 10:47:09'),(140,7,'necessitatibus','2020-09-09 10:47:10','2020-09-09 10:47:10'),(141,3,'optio','2020-09-09 10:47:10','2020-09-09 10:47:10'),(142,6,'sequi','2020-09-09 10:47:10','2020-09-09 10:47:10'),(143,2,'vero','2020-09-09 10:47:10','2020-09-09 10:47:10'),(144,6,'et','2020-09-09 10:47:11','2020-09-09 10:47:11'),(145,6,'ullam','2020-09-09 10:47:11','2020-09-09 10:47:11'),(146,8,'adipisci','2020-09-09 10:47:11','2020-09-09 10:47:11'),(147,1,'Cajas','2020-09-09 10:47:11','2020-09-09 10:47:11'),(148,5,'vel','2020-09-09 10:47:11','2020-09-09 10:47:11'),(149,6,'nihil','2020-09-09 10:47:12','2020-09-09 10:47:12'),(150,2,'impedit','2020-09-09 10:47:12','2020-09-09 10:47:12'),(151,6,'nisi','2020-09-09 10:47:12','2020-09-09 10:47:12'),(152,8,'ipsa','2020-09-09 10:47:13','2020-09-09 10:47:13'),(153,2,'et','2020-09-09 10:47:13','2020-09-09 10:47:13'),(154,2,'perferendis','2020-09-09 10:47:14','2020-09-09 10:47:14'),(155,8,'placeat','2020-09-09 10:47:14','2020-09-09 10:47:14'),(156,9,'adipisci','2020-09-09 10:47:14','2020-09-09 10:47:14'),(157,9,'ducimus','2020-09-09 10:47:14','2020-09-09 10:47:14'),(158,8,'nisi','2020-09-09 10:47:14','2020-09-09 10:47:14'),(159,9,'magnam','2020-09-09 10:47:15','2020-09-09 10:47:15'),(160,1,'iste','2020-09-09 10:47:15','2020-09-09 10:47:15'),(161,3,'et','2020-09-09 10:47:15','2020-09-09 10:47:15'),(162,5,'et','2020-09-09 10:47:15','2020-09-09 10:47:15'),(163,5,'fugiat','2020-09-09 10:47:16','2020-09-09 10:47:16'),(164,9,'qui','2020-09-09 10:47:16','2020-09-09 10:47:16'),(165,5,'quis','2020-09-09 10:47:16','2020-09-09 10:47:16'),(166,4,'laudantium','2020-09-09 10:47:17','2020-09-09 10:47:17'),(167,3,'iure','2020-09-09 10:47:17','2020-09-09 10:47:17'),(168,7,'aut','2020-09-09 10:47:18','2020-09-09 10:47:18'),(169,2,'sequi','2020-09-09 10:47:18','2020-09-09 10:47:18'),(170,10,'voluptas','2020-09-09 10:47:18','2020-09-09 10:47:18'),(171,5,'pariatur','2020-09-09 10:47:19','2020-09-09 10:47:19'),(172,4,'hic','2020-09-09 10:47:19','2020-09-09 10:47:19'),(173,9,'qui','2020-09-09 10:47:19','2020-09-09 10:47:19'),(174,2,'voluptates','2020-09-09 10:47:19','2020-09-09 10:47:19'),(175,8,'commodi','2020-09-09 10:47:20','2020-09-09 10:47:20'),(176,5,'qui','2020-09-09 10:47:20','2020-09-09 10:47:20'),(177,6,'assumenda','2020-09-09 10:47:20','2020-09-09 10:47:20'),(178,4,'culpa','2020-09-09 10:47:21','2020-09-09 10:47:21'),(179,5,'et','2020-09-09 10:47:22','2020-09-09 10:47:22'),(180,6,'illo','2020-09-09 10:47:22','2020-09-09 10:47:22'),(181,1,'nobis','2020-09-09 10:47:22','2020-09-09 10:47:22'),(182,10,'et','2020-09-09 10:47:22','2020-09-09 10:47:22'),(183,10,'dolores','2020-09-09 10:47:22','2020-09-09 10:47:22'),(184,6,'excepturi','2020-09-09 10:47:23','2020-09-09 10:47:23'),(185,10,'omnis','2020-09-09 10:47:23','2020-09-09 10:47:23'),(186,10,'fuga','2020-09-09 10:47:24','2020-09-09 10:47:24'),(187,3,'vel','2020-09-09 10:47:24','2020-09-09 10:47:24'),(188,6,'rerum','2020-09-09 10:47:24','2020-09-09 10:47:24'),(189,6,'dolorem','2020-09-09 10:47:24','2020-09-09 10:47:24'),(190,10,'omnis','2020-09-09 10:47:25','2020-09-09 10:47:25'),(191,4,'in','2020-09-09 10:47:26','2020-09-09 10:47:26'),(192,2,'dolor','2020-09-09 10:47:26','2020-09-09 10:47:26'),(193,8,'quo','2020-09-09 10:47:26','2020-09-09 10:47:26'),(194,2,'expedita','2020-09-09 10:47:26','2020-09-09 10:47:26'),(195,1,'sequi','2020-09-09 10:47:27','2020-09-09 10:47:27'),(196,8,'neque','2020-09-09 10:47:27','2020-09-09 10:47:27'),(197,4,'est','2020-09-09 10:47:28','2020-09-09 10:47:28'),(198,8,'molestias','2020-09-09 10:47:28','2020-09-09 10:47:28'),(199,2,'iure','2020-09-09 10:47:28','2020-09-09 10:47:28'),(200,5,'nostrum','2020-09-09 10:47:28','2020-09-09 10:47:28');
/*!40000 ALTER TABLE `indoor_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langs`
--

DROP TABLE IF EXISTS `langs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `langs` (
  `idlang` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idlang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langs`
--

LOCK TABLES `langs` WRITE;
/*!40000 ALTER TABLE `langs` DISABLE KEYS */;
INSERT INTO `langs` VALUES (1,'ES',' ');
/*!40000 ALTER TABLE `langs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2016_06_01_000001_create_oauth_auth_codes_table',1),(3,'2016_06_01_000002_create_oauth_access_tokens_table',1),(4,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(5,'2016_06_01_000004_create_oauth_clients_table',1),(6,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(7,'2019_08_19_000000_create_failed_jobs_table',1),(8,'2020_07_24_091806_create_permission_tables',1),(9,'2020_08_03_103208_create_customers_table',1),(10,'2020_08_03_103252_create_sites_table',1),(11,'2020_08_03_130315_create_apps_table',1),(12,'2020_08_04_101348_create_formats_table',1),(13,'2020_08_04_104540_create_indoor_locations_table',1),(14,'2020_08_05_071313_create_channels_table',1),(15,'2020_08_05_104229_create_play_areas_table',1),(16,'2020_08_06_075622_create_players_table',1),(17,'2020_08_06_080311_create_tag_categories_table',1),(18,'2020_08_06_080445_create_tags_table',1),(19,'2020_08_06_080530_create_player_powers_table',1),(20,'2020_08_06_080618_create_player_tag_table',1),(21,'2020_08_11_065640_create_site_holidays_table',1),(22,'2020_08_12_093722_create_cameras_table',1),(23,'2020_08_12_100716_create_camera_powers_table',1),(24,'2020_10_12_000000_create_users_table',1),(25,'2020_09_17_124143_add_description_to_play_areas_table',2),(26,'2020_09_17_124925_add_width_height_to_formats_table',3),(27,'2020_09_18_142440_add_image_url_to_apps_table',4),(28,'2020_09_18_152533_remove_width_height_in_formats_table',5),(29,'2020_09_18_153504_add_width_height_with_other_data_type_to_formats_table',6),(30,'2020_09_18_161123_rename_player_tag_table',7),(31,'2020_09_22_101815_create_langs_table',8),(32,'2020_09_22_103437_create_player_types_table',8),(33,'2020_09_22_104510_add_idlang_idtype_to_players_table',8),(34,'2020_09_22_114605_create_countries_table',8),(35,'2020_09_22_114930_create_provinces_table',8),(36,'2020_09_22_120511_create_play_circuits_table',8),(37,'2020_09_22_122511_create_circuits_has_provinces_table',8),(38,'2020_09_22_124632_create_content_types_table',8),(39,'2020_09_22_124947_create_content_categories_table',8),(40,'2020_09_22_125722_create_contents_table',8),(41,'2020_09_22_131024_create_asset_types_table',8),(42,'2020_09_22_131200_create_assets_table',8),(43,'2020_09_22_132419_create_content_has_assets_table',8),(44,'2020_09_22_133504_create_circuits_has_tags_table',8),(45,'2020_09_22_134827_create_contents_has_channels_table',8),(46,'2020_09_22_135204_create_contents_has_circuits_table',8),(47,'2020_09_22_140522_create_cities_table',8),(48,'2020_09_22_140814_create_contents_has_cities_table',8),(49,'2020_09_22_144846_add_primary_key_to_contents_has_circuits_table',8),(50,'2020_09_22_150554_create_content_circuit_powers_table',8),(51,'2020_09_22_152947_create_contents_has_countries_table',8),(52,'2020_09_22_153213_create_sites_has_tags_table',8),(53,'2020_09_22_153726_create_content_powers_table',8),(54,'2020_09_22_153955_create_contents_has_products_table',8),(55,'2020_09_22_154753_create_contents_products_table',8),(56,'2020_09_22_155601_create_contents_has_provinces_table',8),(57,'2020_09_22_155734_create_play_logics_table',8),(58,'2020_09_22_160538_create_circuits_has_channels_table',8),(59,'2020_09_22_160832_create_contents_has_tags_table',8),(60,'2020_09_22_161105_create_circuits_has_cities_table',8),(61,'2020_09_22_161424_create_contrast_centers_table',8),(62,'2020_09_22_161952_create_circuits_has_countries_table',8),(63,'2020_09_22_162208_create_contents_has_areas_table',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',1),(2,'App\\Models\\User',2),(3,'App\\Models\\User',3);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('003c03921555c71efdcc62927aa76755a3336a744c619e139dc6a5cab6228c9c823e1e59f2d2bb8b',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:37:18','2020-09-17 07:37:18','2021-09-17 07:37:18'),('023ccbd284ad3572b03a94f9f0604689d85a03d74b135d7d72be8a49129f5ca3d0347269193e98c8',3,1,'user@ladorian.com','[]',0,'2020-09-10 00:05:38','2020-09-10 00:05:38','2021-09-10 00:05:38'),('02562f1f73e27ff731e25060e39bcfc00ab7e5d98280c342c3bfc482c97b3cdee14fc5503ccfa0b0',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:10:24','2020-09-23 16:10:24','2021-09-23 12:10:24'),('026ec04e1a96f90d12df0ee6f72fcc76df09d55277fd4b30b5ae7bd612de34ea502687fcf086ad2d',1,1,'superadmin@ladorian.com','[]',0,'2020-09-24 01:48:55','2020-09-24 01:48:55','2021-09-23 21:48:55'),('026f7225732251ca86b1a571aafcae298995c2fb8b260a1b35d1e0a9525ce5430bc418a63f85b7e1',3,1,'user@ladorian.com','[]',0,'2020-09-16 00:51:06','2020-09-16 00:51:06','2021-09-16 00:51:06'),('02c006c51ae2eda904c219123745fa301ad272def798652da7321625091254cc6b24f81ee043c041',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:46:04','2020-09-17 03:46:04','2021-09-17 03:46:04'),('03490f8f290b5f67f4f708d431a852e4349422f809edd2817f0d756d46f8ea3f1e863833260c81ca',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:01:12','2020-09-17 04:01:12','2021-09-17 04:01:12'),('0467dfc49e685318c97817f8cfa2b4782cb0b6af9e27e2f3925585c1799e120efedca54de197333a',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:06:23','2020-09-17 05:06:23','2021-09-17 05:06:23'),('0603b9183f1bf5b154635e520d15a08014103c74f220a13a32ba5c8e1fcb94e1be93ba11de30ff41',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:44:23','2020-09-17 03:44:23','2021-09-17 03:44:23'),('063b61128250f7d2fc379b59ce78e4a71c4c255e74f763c5e43b440e18bbd7a7803ecdd88cf5e478',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:17:48','2020-09-18 10:17:48','2021-09-18 10:17:48'),('0815a8d8a80a608e1e88f2b0e7b20a0db669fe642f59811b0ba8b5bbae2917dad50afef84702bd25',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:30:00','2020-09-17 05:30:00','2021-09-17 05:30:00'),('0aafde718d957c6027ded45375535a5fb7f2afe1564a6aa86601b0aa0c7063e80c7a8d3b03dfbbaa',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:28:07','2020-09-18 10:28:07','2021-09-18 10:28:07'),('0ae77c40ad3e8f13581d4d847a241c5c92090d5fd2bb92f1ceaf485ea2d7db9e1d2f5b7bd2ce50b4',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 03:49:23','2020-09-15 03:49:23','2021-09-15 03:49:23'),('0c73fae895635957c6831c1169e748ff968d23fd3b31104f1d56494f12afbd137dbdb0695bf11157',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:37:04','2020-09-17 03:37:04','2021-09-17 03:37:04'),('0d971a41eb51c7998e01550b55c52f3f2cfcd0d2f5233ab68bb720de8e625cdc7402370320d750e9',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:31:49','2020-09-23 20:31:49','2021-09-23 16:31:49'),('0d9d41746a503429bcc183374669eeee3db01014821455d1e2f5006702b43d69a6031dfbdc92bd54',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:36:55','2020-09-23 16:36:55','2021-09-23 12:36:55'),('0ee467f2e144046749c8156da4ae87169f69daad86db096ccf04631ac205268ca0770cd6b0d5fd15',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:06:45','2020-09-17 01:06:45','2021-09-17 01:06:45'),('0f7ef0d10d2293c0ef1d5d15a66d4c536207b80a08715848902f22645359badd04b46d13ab2609c1',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:09:33','2020-09-23 16:09:33','2021-09-23 12:09:33'),('0f88d91972928fbf02bdc7a43602c07c0b20e946cc164c393850d5370e7ee7978ab1bed8dad18aae',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:31:34','2020-09-17 03:31:34','2021-09-17 03:31:34'),('0ff18cf3966bb62f1d2638eb8917dd1ca963a80950814ce743a11f011612b09b920010ed42853d68',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:43:02','2020-09-17 05:43:02','2021-09-17 05:43:02'),('103d1e39057b027192343a33fb82411b86499ab856e396de26a4c831c4d801ccce86c574d011d62b',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:20:12','2020-09-17 12:20:12','2021-09-17 12:20:12'),('1067bc86ace62109401dd4385ca58f0aeab311efafc69da318c4c288f2271736280dac07d33c78e7',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:11:18','2020-09-17 01:11:18','2021-09-17 01:11:18'),('106eb6345a85240788b20004297a3c0ac60f687fca0663b029284bd57c3766925fae62435ad1c0d7',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:06:32','2020-09-23 20:06:32','2021-09-23 16:06:32'),('1387827c762a72987b358081135f98a218c0e9058ded372321bc0899d21b1be65544fe5d87fc906b',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:42:23','2020-09-17 07:42:23','2021-09-17 07:42:23'),('14fb81ef4930f2f15ce2acf71679ffe0ef834b4ea5d83477908712314bf9416f22ce2f18811a977f',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:03:21','2020-09-17 01:03:21','2021-09-17 01:03:21'),('15283a85657bca6e32ff24402ffd1123e005aa23ec69b731d64f058a1f7a5425e7c7cf5b13d5e341',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:09:54','2020-09-23 20:09:54','2021-09-23 16:09:54'),('152ad45ce44992278f7e6ea5cac2b2ed8eb9cc533ff74da3dbfce47603ccee877da3ce3741f584e3',3,1,'user@ladorian.com','[]',0,'2020-09-16 06:01:18','2020-09-16 06:01:18','2021-09-16 06:01:18'),('15fbaa557378c6b550fbfb6e9de779fee52873d6e20beeee1b9051d48944bf8e0f59b48cc4f762cf',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:35:45','2020-09-17 07:35:45','2021-09-17 07:35:45'),('17f7e2d72a2d40972c347ef494b838bc1c4027c97fdae408ac12158ca1946f9014aaa9fd32ae55ff',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:54:08','2020-09-23 20:54:08','2021-09-23 16:54:08'),('18aea6b2aff24f0e955a28c83b450dbaa7b3e26ba11decc2239751d6aaa1f6187e99e751f9b25a18',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:15:48','2020-09-18 10:15:48','2021-09-18 10:15:48'),('193cba4115db8a8e91e64030b7c0e7c1cb9983edb95a439fe8990d733d05c73c763476b788bb2e47',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:39:22','2020-09-23 20:39:22','2021-09-23 16:39:22'),('199aa8947ecd2fb903b163b1f394db26600f704ac17d4130a95b0c7eae3c676da599a63aead9cf76',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:17:29','2020-09-17 04:17:29','2021-09-17 04:17:29'),('19cb536a9d8758f85b710453ab04af39658257bea6fa1be8249c9f1e6874a24f184cd6b27ed229a3',3,1,'user@ladorian.com','[]',0,'2020-09-17 02:55:29','2020-09-17 02:55:29','2021-09-17 02:55:29'),('1a498d40e0ca15d7d84ecdef15fd906374da64f2bb21aadf22e99960fcea96e95f4a5ec6db3dfd5a',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:18:02','2020-09-23 16:18:02','2021-09-23 12:18:02'),('1b070d2d4101bf4f79ac89cebd9a8e4a0ad0f3a4f9ae194c269c81975763e746afe271296cba526b',3,1,'user@ladorian.com','[]',0,'2020-09-25 04:45:39','2020-09-25 04:45:39','2021-09-25 00:45:39'),('1d6a28b5943592273186d0e1014b959d3fc68c3c4d1ad16f5b460c2af2a6b6262b406d193672a899',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:38:48','2020-09-17 11:38:48','2021-09-17 11:38:48'),('1fa78882714f2c43bbe24c792739c40d6d1b12c3b0f2259d72cce8b3239c5a5769a142677fd3eed2',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:05:31','2020-09-16 03:05:31','2021-09-16 03:05:31'),('1fffa49f6461d4cfce7157f6beb51c1200132954d1943cd0acd019adea90bf973ac9186c624b914f',3,1,'user@ladorian.com','[]',0,'2020-09-18 09:55:41','2020-09-18 09:55:41','2021-09-18 09:55:41'),('216df9cad64410677bdf87511f7bc17f6b8bed2f9e8be004654749210cedadf3815a43343a3c7cae',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:13:51','2020-09-18 10:13:51','2021-09-18 10:13:51'),('2220594c620bf017587c8bd729d4d6464f5a943dc437f4253f3a8eb9454ef3c62dbd0aea618f5b3b',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:54:35','2020-09-17 14:54:35','2021-09-17 14:54:35'),('2243ddfd8be5c87ee49097a2d552ab2f854dd1ae019b0c392e2aac7adbe4323347f94c36195595b8',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:17:55','2020-09-17 07:17:55','2021-09-17 07:17:55'),('226c8420c32b9615fc6e0af7d4422254fff67a8f47fceb4a294f900b2aeb0ec191b7616c5dc400c6',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:16:32','2020-09-17 05:16:32','2021-09-17 05:16:32'),('23f40ab05586285a8b2d295655e81cf2c36c70467e88eb9a72eaa8a6dcdc3cc94d50a4cf1aec0e85',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:24:17','2020-09-17 12:24:17','2021-09-17 12:24:17'),('24a7af16eaf7ab3a358de7cb12318038cdbc7298769b3f69a09c94f1fae409e550e3add8b7f24199',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:45:10','2020-09-16 03:45:10','2021-09-16 03:45:10'),('24a9119a1f19d5eebf2ab863d3c44ac9daf690bf61687ef108c314c2e5ade0e67dfbf457815a13bf',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:29:35','2020-09-17 03:29:35','2021-09-17 03:29:35'),('24c097808dec872dc9f5273eddf37b62873b71c6ae8842a89bb3594b738773d2394a475e4e361add',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:36:37','2020-09-23 15:36:37','2021-09-23 11:36:37'),('24d71ce736428c17608b0d99091e821cb13090287c69b2dae34815a8b69c2af53869a6ab0fe33f18',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:01:44','2020-09-18 10:01:44','2021-09-18 10:01:44'),('25aeef990575c776851f1dd4ee772767cf9156e60093c1aeb1593bf6a1129746ba0b9d6fb8d399bd',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:47:56','2020-09-17 07:47:56','2021-09-17 07:47:56'),('26a70de2f7d3850e6e8620cff83d8fabb374bfa10bad8567692dd9b591dba7a8b367139d6ea38c42',3,1,'user@ladorian.com','[]',0,'2020-09-25 05:10:48','2020-09-25 05:10:48','2021-09-25 01:10:48'),('2780af165b0c011e3a7e2c88689b579f51cb363cea635ba5d54f85692ade6dacd5a5ef539723ed79',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:58:57','2020-09-17 07:58:57','2021-09-17 07:58:57'),('27a5843e34508c260381de7654bc0b164203f7ec3db06bc4de08a679adeec7f09cf8a1a087f331f5',3,1,'user@ladorian.com','[]',0,'2020-09-23 18:14:53','2020-09-23 18:14:53','2021-09-23 14:14:53'),('28f70ec0e29b14b80692f4f4054ef6afbf6092d2b8d8a00242929b35d289d8ec1fdac140944cf83a',3,1,'user@ladorian.com','[]',0,'2020-09-20 16:37:52','2020-09-20 16:37:52','2021-09-20 16:37:52'),('29221e7f35ee433912be38f6dfdd282ebf284c22ff0e895876df69643eddab5219a3b36bb058a76c',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:16:25','2020-09-17 04:16:25','2021-09-17 04:16:25'),('2b196a35000b0007521ec63acb713365b138516809b48cc510c6191871465c2d783be5d2f4d7ef2e',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:28:43','2020-09-16 17:28:43','2021-09-16 17:28:43'),('2bfdbc0aa03bfcc21884b159d908268611a6a73f08e64c67e87fabb902ba0611748061e5067fe7b2',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:00:45','2020-09-17 12:00:45','2021-09-17 12:00:45'),('2c2eeb64e2ac5d865fd0c9a48a446fdd2109e6b59039d1c638638d5bf8bffd6c269f9fd973ae298e',3,1,'user@ladorian.com','[]',0,'2020-09-14 11:26:28','2020-09-14 11:26:28','2021-09-14 11:26:28'),('2e4b94ba4d99c09ccc5f761d35a368873aebedeb8c5479717d75bf2efe7df7e9201be65e692b49c9',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:08:01','2020-09-16 02:08:01','2021-09-16 02:08:01'),('2e5886b5cce8dd3bb53afafa50c4eb5c6cbce32cf8dbb763554d5e9e98ae3e3152d42074489e0ff4',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:44:34','2020-09-23 15:44:34','2021-09-23 11:44:34'),('2e89009b538d27fcc462f82d736c8711be502c455a08b44af7239bf9a23c011443e77c0adc9cbb83',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:10:55','2020-09-17 05:10:55','2021-09-17 05:10:55'),('326280142c023ad784fd18eef982bf1d12499c71cd3f9fd737b093133256a1e320f8424229178368',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:59:02','2020-09-16 05:59:02','2021-09-16 05:59:02'),('34188ae9ae7b1fb13cd9d0d0bcea113905505c15bfc1037ba2e7c3e0885a3a3fec57cfd144dad363',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:10:42','2020-09-23 16:10:42','2021-09-23 12:10:42'),('34aceaa024ea50ee0844b9d89495c7a0f370f6027cdff939d3c3845533295896ea97836a6e956206',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:08:37','2020-09-17 01:08:37','2021-09-17 01:08:37'),('34c71c03f711b73eeb6ac608f7e4baab511904a964a16e4975b48f0352864325fab616430120dd85',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:34:22','2020-09-23 15:34:22','2021-09-23 11:34:22'),('34ebaef095523e51a907e522748ab04393fac784a67e0e8164da3d8ac736d7d9f51e0295fbbad8a9',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:57:27','2020-09-16 05:57:27','2021-09-16 05:57:27'),('359a7bd456485f31d42230f2177bc5c8b0f26280252717321900d9cc94c48e986a106b50c99fc5cd',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:59:07','2020-09-17 06:59:07','2021-09-17 06:59:07'),('36dd0064594c94008faa7ede1aafdce7cb88efce35b2a4e778a7fd5a876c1075494e915856923686',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:44:04','2020-09-17 04:44:04','2021-09-17 04:44:04'),('36e2cc5f83d1c63564273715cb381d999ebdaab3586dede526a68487d68231e2309c2d0f24d08e68',3,1,'user@ladorian.com','[]',0,'2020-09-16 23:47:08','2020-09-16 23:47:08','2021-09-16 23:47:08'),('374d379b24b6d4e605e33571fa1ad3dc99790decad55d4146c6dddaa5058c6290913e1af608be734',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:58:31','2020-09-24 15:58:31','2021-09-24 11:58:31'),('38569b068b58ee7bb8805cdfa4e9636538ec9fdbd1b04ba2c6074bab12a731d3af1658f1ef336cd2',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:38:34','2020-09-17 03:38:34','2021-09-17 03:38:34'),('3bf845f10baabd3fdcea910d6ecbfed934b28dfc516db8afaec5d8f7d8bb80bd183142b7baf1d0ad',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:35:18','2020-09-16 03:35:18','2021-09-16 03:35:18'),('3d7ec8c5fb1ef046d3670d0ba62781dca49277d846b2e96ff5d501ad91e97a3cb8f424032d502901',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:07:12','2020-09-17 04:07:12','2021-09-17 04:07:12'),('3de5347b5dc934527d891be9dedaadc8292d02a3288cdea12fd70b2e9eb576c5bc6e2db5164f6b6c',3,1,'user@ladorian.com','[]',0,'2020-09-16 00:44:44','2020-09-16 00:44:44','2021-09-16 00:44:44'),('3f46e80a298607cad292d30f5f89b11a891c510619a8bb950c792f73181495773d2ef62ad52778cc',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:12:58','2020-09-17 05:12:58','2021-09-17 05:12:58'),('3f50a02889244b839a7be6381f26199c1175941124f4b88b9cc2147340ad56c8c994deff7951f7f3',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:27:04','2020-09-23 16:27:04','2021-09-23 12:27:04'),('3f58074644348f654bbca0e7d753fde413e5180dd061161dbd1e012730c48353507d5ddfc3723fed',1,1,'superadmin@ladorian.com','[]',0,'2020-09-16 09:20:11','2020-09-16 09:20:11','2021-09-16 09:20:11'),('4011e9412fc291af9ef4e7e5e0680cb7b78bf7c13f911fabb461eec714b43d75d8513619bf04ff89',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:26:48','2020-09-17 11:26:48','2021-09-17 11:26:48'),('4176f984ed6f8090deeb6411ee5e2d3ea253c997f8d234c06e79544f772f9308e8f7e433f3c2fb5f',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:02:36','2020-09-18 10:02:36','2021-09-18 10:02:36'),('41901e65d58cd9b84b6247df04f6618b702af6f0cbe8153e00d09fcd2e43f176e0f273f3a24459e8',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:25:22','2020-09-17 13:25:22','2021-09-17 13:25:22'),('423ce65f83bc96e2a4fd4f2c6290dc5c397fdc1170396324cf7283f688738cb87b8004974e9fab12',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:22:12','2020-09-17 07:22:12','2021-09-17 07:22:12'),('42599ba60a5d52ccfdfc7cb022fd2af5720b263711e4c2e9cc5b21ed1f73af8ec2d92b1fccc18278',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:11:43','2020-09-17 04:11:43','2021-09-17 04:11:43'),('44cf89973d216f6589f6527e3e166644ac5311c8e3053657e46aecd10303df7f4597058e079fb9fe',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:47:08','2020-09-23 20:47:08','2021-09-23 16:47:08'),('452a5f604c0ec12757f7b4f19345b97c7040823481610278e329db693378a43a11b9c42b47d4e28b',3,1,'user@ladorian.com','[]',0,'2020-09-10 14:53:43','2020-09-10 14:53:43','2021-09-10 14:53:43'),('45314e069549ca5a44a30657c6314b4e1420a463a7c2221049e181296b1cddb3354486043e3793c5',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:43:47','2020-09-17 15:43:47','2021-09-17 15:43:47'),('4713d334f238f285be3e24937a82e954e397b8c58bc1ca94c9d77d2fd5beb89cccbffc04d0be79ee',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:46:53','2020-09-16 05:46:53','2021-09-16 05:46:53'),('47b97e7ad2d34a05c559e2f88db619ea2325ae4b5abc04a3a777e0a6b3ee47103c2a015833049a9d',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:08:45','2020-09-17 06:08:45','2021-09-17 06:08:45'),('4804e54dd69a7e791a5add32f8316a20862743d65dc08d92239382b8eae35b9d38a0852f4b40f2e4',3,1,'user@ladorian.com','[]',0,'2020-09-24 20:59:55','2020-09-24 20:59:55','2021-09-24 16:59:55'),('489a1f4fa9c9557d875a6637dc964c05c5c54616e3d8f62e9da37b50025b52d95f447c94abdc26fb',3,1,'user@ladorian.com','[]',0,'2020-09-25 05:17:02','2020-09-25 05:17:02','2021-09-25 01:17:02'),('48cc2443642c33a6b5f91ab869b76c3611e4f124ef0e5b4c4996dedaab92feca6bfa1fd3b7cbdfec',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:46:10','2020-09-23 20:46:10','2021-09-23 16:46:10'),('491935ee2a6b310b398ff59c171f032158150fb29789df21ca00c1f399b0dc5d56829bcc9f391427',3,1,'user@ladorian.com','[]',0,'2020-09-24 21:02:29','2020-09-24 21:02:29','2021-09-24 17:02:29'),('493df1a129b75930c0357fa897b021ff72d6a6d59e534ae20e9457fb0c748ce0f640d6f1fc74cfc9',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:40:51','2020-09-23 16:40:51','2021-09-23 12:40:51'),('493f49c1d80dd23719e9d2264cd38f4d2be096b4a1e58f45012c78e312e7a7cf1f8a7d20f73f132c',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:34:50','2020-09-23 16:34:50','2021-09-23 12:34:50'),('49ef24235873794c0cc0cb686511933df336dcd8acf4ede0342941254e5baa2f3aa783f102a45200',3,1,'user@ladorian.com','[]',0,'2020-09-25 04:46:30','2020-09-25 04:46:30','2021-09-25 00:46:30'),('4a261c5941013f9b5e2a88d3dd668c9b2bc0273008cde6b417a390cf1e1707530404f3ea4236f666',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:41:56','2020-09-17 04:41:56','2021-09-17 04:41:56'),('4b585b9291cdb61777eb2a291017353f45e1231a0c05a934acee18eac96ef90e3c8b416af2e789b0',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:05:05','2020-09-18 10:05:05','2021-09-18 10:05:05'),('4b731be52d36585bca4856806cadd5b3ccef37bcafeb778bb72975ca7e7dab3258f077d0c971d60f',3,1,'user@ladorian.com','[]',0,'2020-09-24 13:15:22','2020-09-24 13:15:22','2021-09-24 09:15:22'),('50abacb8a5108a04adb879203001a3a7f1dc4895f68b3ec2d97f3d56748a8a4cc0520f6d4171d0f6',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:10:50','2020-09-16 01:10:50','2021-09-16 01:10:50'),('51dc101803001b5d300a5ef98cdbc20c89131167ac7d05382d5700414a30fc5dde289ddb3e59a5c9',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:21:35','2020-09-17 12:21:35','2021-09-17 12:21:35'),('51fad3bdec98ef00257cbaa270363ac1af40a8856483ab945620328983885ff2a912bb758323a817',3,1,'user@ladorian.com','[]',0,'2020-09-16 00:31:07','2020-09-16 00:31:07','2021-09-16 00:31:07'),('52c2253ed08bb81af2c7ed0365a12e9579486021ac47a1a22721849eb170f20336f483a6e7854b9b',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:47:30','2020-09-24 15:47:30','2021-09-24 11:47:30'),('53764812674a15195775609f27fcc5e952ee40a42b4dbc5ae065ed95904606136d83c62b77f73d78',3,1,'user@ladorian.com','[]',0,'2020-09-23 18:21:24','2020-09-23 18:21:24','2021-09-23 14:21:24'),('5436ee56a02739da64ad1efef89fac014a2d7599f854ae33c77dc01fa2df3b742f067c49662a8ce6',3,1,'user@ladorian.com','[]',0,'2020-09-16 16:30:34','2020-09-16 16:30:34','2021-09-16 16:30:34'),('554ee9b0444bd2fc34deecdc2bf4b6deb323f118de05625aa6ebbf17299e86babc0f23b6fd5dc98b',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:49:03','2020-09-18 10:49:03','2021-09-18 10:49:03'),('57bc992c2f9b944726f4b352b3100193a932b859888a007ef218ba1cc63a5c45dbdd9e150d431cdc',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:46:15','2020-09-17 07:46:15','2021-09-17 07:46:15'),('580c77fbb59dae7bf8445416b12527300c6d21e5cc633d79de9fec9296a505de9ac08d8527f3f660',3,1,'user@ladorian.com','[]',0,'2020-09-24 01:50:18','2020-09-24 01:50:18','2021-09-23 21:50:18'),('586369c1a64b013d9971d565dcadf9e39e638aa14a72fe8d37453107d12e1a1ec74d9dbbe84f62e5',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:23:35','2020-09-17 07:23:35','2021-09-17 07:23:35'),('58705af9f4e9406ad114ee34e460a1c3059cb08aeb065691a02abb316679ac19c4926de0d1d3547d',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:51:40','2020-09-23 20:51:40','2021-09-23 16:51:40'),('598b2ef9afdc7d6b49efb535d6aec40419aeb11d207c261a7a2ca6b75928ea3f47b04c786e59304d',3,1,'user@ladorian.com','[]',0,'2020-09-23 18:55:20','2020-09-23 18:55:20','2021-09-23 14:55:20'),('5a16c99e8b89d2c626e8b2dd250f482148bb4bbdf9056b634082a6b829d2dea9309cd8b380307e5c',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:44:57','2020-09-23 15:44:57','2021-09-23 11:44:57'),('5a49830bc68b32fe02a247740c7ed1cc9b28d3397c831e368dc30fede3f4cfc6d8d27fe56ca04345',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:15:25','2020-09-23 16:15:25','2021-09-23 12:15:25'),('5ac47a1295c3a25dc4394c4586fc4f02e1a5504685c3e4fa2f58b50222725926de9bfa3cb3cee3dc',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:42:44','2020-09-17 06:42:44','2021-09-17 06:42:44'),('5bf6e37d14cfbb5cf57996d2030cc824c182939ee1834d7e9252198ecafaf6b32cd616c94a26e209',3,1,'user@ladorian.com','[]',0,'2020-09-16 16:34:55','2020-09-16 16:34:55','2021-09-16 16:34:55'),('5c185aa96f97344fcb9afc842516a4de7e8a612660994d452e6637c393407f2a7cf34288e575110e',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:29:27','2020-09-18 10:29:27','2021-09-18 10:29:27'),('5c19eebd880189118126143d0e73913f1b2e448be9d80403c5ff4f304062909079b2d7f531c3baac',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:46:25','2020-09-17 05:46:25','2021-09-17 05:46:25'),('5cc230182152b2196503a07fcee196429d59db16cced94b4ab9d0dcb1e0ea7f247529be668df6a36',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:18:56','2020-09-17 04:18:56','2021-09-17 04:18:56'),('5d2bc433f580d10523d38930fc3a1a33a3a6a0a2d16e04b6ade7385c16a67528c33fdc2600c6d29b',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:29:32','2020-09-23 15:29:32','2021-09-23 11:29:32'),('5d3de498f626c29a12526a5619f959038788c9ae820fc5439fe797a67e1ec49ce6d5ed97b5891c80',3,1,'user@ladorian.com','[]',0,'2020-09-16 04:56:56','2020-09-16 04:56:56','2021-09-16 04:56:56'),('5d68d18ef6c1dc865fc5a55a889937997ca4b09d4751a4c6ff85b6084984bb0c0ce0c2908266cf0c',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:46:39','2020-09-17 14:46:39','2021-09-17 14:46:39'),('5e5fce21149a9e996bd067fcb3ed626f6b39a805e78af84fa247c7c9a0848a45774892ecc152fef6',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:25:25','2020-09-17 07:25:25','2021-09-17 07:25:25'),('5f03a0188bfeeb84dfabd176889a3b3e42764a891278d05211f01838cc1ec8ae7f76bc421eebea42',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:54:33','2020-09-17 05:54:33','2021-09-17 05:54:33'),('5f21ed7618377a4fae4e9368fcba393115e54917be8df120af36aa9a673aa4785a433ccb68596e1a',3,1,'user@ladorian.com','[]',0,'2020-09-09 16:05:41','2020-09-09 16:05:41','2021-09-09 16:05:41'),('600a3b6f4593e0bf0617d7b0ed52cc5640e8d5da64650a7d57dd492cc22a3642887f88677f54ffa1',3,1,'user@ladorian.com','[]',0,'2020-09-16 04:38:38','2020-09-16 04:38:38','2021-09-16 04:38:38'),('608f02808cc1240f1bca3e929a4bad2f5707b226093dc3bfbe962a51cfab6c64c7776c27f1bb4be2',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:19:03','2020-09-23 16:19:03','2021-09-23 12:19:03'),('610651e46c56c2d63f8f467732ce525a411c933742f3cca2e3e8b31ef82502375c086694a7f4319e',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:29:11','2020-09-17 13:29:11','2021-09-17 13:29:11'),('62d2d3a5940b3e3243b859f9485d0e26769562539a841aafc8e983217f60d95daf9685b49a9a238f',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:36:11','2020-09-17 14:36:11','2021-09-17 14:36:11'),('63efd20559375b76d84c53cf0f8436d9f06cea5078229a9ab53d7440c57f1199967b102fcf02e2d6',3,1,'user@ladorian.com','[]',0,'2020-09-16 06:01:59','2020-09-16 06:01:59','2021-09-16 06:01:59'),('63f739da28d86562772303bff7d8ecfc2be72e1bebc15681783b4a8fdfc66d91e619dc7ffbb2d742',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:28:07','2020-09-17 01:28:07','2021-09-17 01:28:07'),('6440ef7c2e69de6df756e85fdc0ece5a593ef631ed6d60c4c084aeeb1f0a23117969ccb2be51a79b',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:44:48','2020-09-16 05:44:48','2021-09-16 05:44:48'),('646b4cea741333e43ab304e112676a3514cd3ba057e31be6d6f483fc06b4f5c9576601ee0f4bef46',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:42:41','2020-09-17 14:42:41','2021-09-17 14:42:41'),('64d4a182167931a886fc81be9aa401e4c8f15b6d71fe8d34ae4ccba31061dcd8337d883d89096797',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:07:29','2020-09-17 06:07:29','2021-09-17 06:07:29'),('653dc4f5fb968ea1f51aec6be950a39a842cf66d7bd9b26b0d545d675d0a6688851112deb0c8a994',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:25:09','2020-09-16 17:25:09','2021-09-16 17:25:09'),('66306e79e2db96c2e1a4b2282431147551b425d90f583590aa2ad8d3732cde38a27950735675830e',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:44:36','2020-09-24 15:44:36','2021-09-24 11:44:36'),('66dbf174a34875762f417de8f33f1b3c5b701fb2e0493a5f60444d2886db1c1e74582c1505416c80',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:39:22','2020-09-17 05:39:22','2021-09-17 05:39:22'),('66ed8d2d1908ad3a48fed9719520ef37495950fe542fd29dfe468c5e56767a7e0c504204117ea95d',1,1,'superadmin@ladorian.com','[]',0,'2020-09-24 13:14:46','2020-09-24 13:14:46','2021-09-24 09:14:46'),('67843b27c6eaee122ed4f8c58e4e5f5c4116380a94016095f346422d42accaf2345657ae1958fbe5',3,1,'user@ladorian.com','[]',0,'2020-09-18 09:52:51','2020-09-18 09:52:51','2021-09-18 09:52:51'),('67b44f6559b44487f2d1f973317e3d58569b08154a0962a5fe84a4e584b6534cc3db9ef211f26500',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:25:35','2020-09-17 12:25:35','2021-09-17 12:25:35'),('67e1aae8a78d2439229ad05988691c6445fd5aff403bf921df4f03c1f0af51f696c0161cf3697f03',2,1,'admin@ladorian.com','[]',0,'2020-09-09 16:18:16','2020-09-09 16:18:16','2021-09-09 16:18:16'),('68ad496164d99d2f88e347c3f992dfaa65dd3ca1c757be33b1f5ce2520b8075275ed21950e2ffb5b',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:28:03','2020-09-17 15:28:03','2021-09-17 15:28:03'),('69324fe9e214d3ee3bdd57c83071c129cbfc81511c294772265a281622bd611986f43a29c2d079a9',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:13:46','2020-09-17 01:13:46','2021-09-17 01:13:46'),('6a9f45a8cd8dfcf1d39a974090db4e80f86feffe3b769068f8513921918cf73ce62271a7dc612e73',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:09:32','2020-09-17 12:09:32','2021-09-17 12:09:32'),('6afcaee0b00453996a1a184175d1f8aac9768e5918ddcbe8833a113250e863889a6a35ae190b58ea',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:05:23','2020-09-17 04:05:23','2021-09-17 04:05:23'),('6b7a8a080875d7e3e1165c5f8f746d57ba338329d7e1908c82cb8d638aa197a52e164c596cb86109',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:09:17','2020-09-17 11:09:17','2021-09-17 11:09:17'),('6c50c7f15bfd339c6c9e5a09697277379111956462daebc90be9700547923fb0ef06b110f86b435f',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:22:19','2020-09-16 17:22:19','2021-09-16 17:22:19'),('6d2be3b9ab5865f875c678b16d27b5684a79bf90a9e6e3346aa7eedeff8651dce7697e6a2a10e64b',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:48:57','2020-09-17 04:48:57','2021-09-17 04:48:57'),('6ed1077f37ff52d6bb01cfe745c5de88d88c1a0f09a24991e258fcfed408a6990fc9b5b0a0b0edf6',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:24:49','2020-09-17 04:24:49','2021-09-17 04:24:49'),('6f370d9c5cd6754faec15050430f8de3a0990651e54b0f4f9ae12ae6245c9535d0037ffce9dc8c44',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:29:18','2020-09-17 15:29:18','2021-09-17 15:29:18'),('7004ee79346056ad918cbc6ac8822b4a2d8787c697906c6c9327378842ea85bd074f8ed792aff5c1',1,1,'superadmin@ladorian.com','[]',0,'2020-09-24 15:58:20','2020-09-24 15:58:20','2021-09-24 11:58:20'),('70644e6061abc733416a52d46c4a5edf82df3d8bde233ba14075a4fca875b089b225af9777ff9702',3,1,'user@ladorian.com','[]',0,'2020-09-24 01:46:47','2020-09-24 01:46:47','2021-09-23 21:46:47'),('706702d08a947901e910f4fa2c8eea7e173b1a96a14cd434636f0b3c2965c7379a7d616ba5f256fc',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:42:51','2020-09-17 04:42:51','2021-09-17 04:42:51'),('71243c63c9f9a75dd3be9ac7e8556fe17cb91aea7f97a437fab7fcad8b8a9ed1781bdefead1c7c83',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:01:00','2020-09-16 01:01:00','2021-09-16 01:01:00'),('7158668205d0344ac0888ea2438d592c0ae48002a4f50fedcf25e35ddef5d567525a942d9e6d868d',3,1,'user@ladorian.com','[]',0,'2020-09-16 06:02:20','2020-09-16 06:02:20','2021-09-16 06:02:20'),('72504cc031edd168db4b8c8002bfa23b4575e7c7a7e2b1e39b357da37acb87f44a437cd4d1c878d0',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:28:50','2020-09-17 05:28:50','2021-09-17 05:28:50'),('747c58dbe59cc2d71cbd60063e3fbe44a6af52459116850bf49684d08311d85d9cefac9764cc6d1a',3,1,'user@ladorian.com','[]',0,'2020-09-23 21:36:19','2020-09-23 21:36:19','2021-09-23 17:36:19'),('76228d5451d729862967dde745e9d278ea6c654c529fb5a0d207eca25c172c183c8ffa46632d3e0e',3,1,'user@ladorian.com','[]',0,'2020-09-15 23:57:29','2020-09-15 23:57:29','2021-09-15 23:57:29'),('764f17016c3c482e6f016746a55fa1130f4ab2fa9a8b77e005081f35ce275f4131d677ac80faf7e6',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:23:21','2020-09-15 23:23:21','2021-09-15 23:23:21'),('77af6783a6ec0ceec03473db02cafab796285a43e0276ecc4a4ad46775b725744d9fc096169d406d',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:04:19','2020-09-16 01:04:19','2021-09-16 01:04:19'),('783e5f2fffb88a15e6a19205a499cdd626751edb8a3df80650827901deca541362f6772bcabbf95f',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:35:05','2020-09-16 02:35:05','2021-09-16 02:35:05'),('7987002821c8575074595b1d2898670d48864604f6964aadd312e9dfd1565a0238d72c60b5ae6321',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:20:11','2020-09-16 17:20:11','2021-09-16 17:20:11'),('7a26e67ce7ac4bced23ecb7dff54fbd59c791993b304a31c89d0d0710479cc0e4a13f577f8eccf65',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 16:10:49','2020-09-15 16:10:49','2021-09-15 16:10:49'),('7a4c87a9de09cf55e99af00c31ea2ba1ff30b4cadc311a8c7ae2ca954106739eb6b42426470d43c8',3,1,'user@ladorian.com','[]',0,'2020-09-16 22:29:48','2020-09-16 22:29:48','2021-09-16 22:29:48'),('7aedc4f7c229201aa382e425d32272ddb471bc4b0080c1c6edc009af908c8dc3c33f74c5634d506b',2,1,'admin@ladorian.com','[]',0,'2020-09-24 20:35:29','2020-09-24 20:35:29','2021-09-24 16:35:29'),('7d0190c908679e5e551eb4501e026e5a82d2865db0fb17c96f4c2d1fe5bc3feae6cb4d0113aa9b28',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:12:34','2020-09-23 16:12:34','2021-09-23 12:12:34'),('7e54510788793b427bf0702fc1f4f198a0a6f3b75c35db63e82e7ec01d0fb19a74dc0a2a04c73329',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:38:57','2020-09-17 07:38:57','2021-09-17 07:38:57'),('7e873fa31d71f0da1e47fba066684560531d6294c75d90fc3e84c10a31be7aee8840b095dc44b550',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:04:50','2020-09-17 13:04:50','2021-09-17 13:04:50'),('7e9880164bee20e31b5ab946130d6004434dbc00b9361081d203717778d2192c5bec45c6985b45fa',3,1,'user@ladorian.com','[]',0,'2020-09-16 22:34:34','2020-09-16 22:34:34','2021-09-16 22:34:34'),('7ebc970fdd3677b409bcb8202bb833ea42fc851b615ceedd8459f0fe14307619ef76806351b4f630',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:17:16','2020-09-17 13:17:16','2021-09-17 13:17:16'),('800672fb133c9dc860e8b3c8594bb94f98ec8cce268f02d023ac0e16de6aa3dd7ec231ab6283fa0b',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:23:47','2020-09-17 06:23:47','2021-09-17 06:23:47'),('809c3008608451d96ce36d2a82bacc9333db2ea2913b5be9b0f6216645bb35ffbd53a764fd76c692',3,1,'user@ladorian.com','[]',0,'2020-09-18 09:30:50','2020-09-18 09:30:50','2021-09-18 09:30:50'),('82d89f2227a96bd1b42f4178b1d6521de1a9a545cc3dcc18e403d11cd0d58fcfde44b1d9ce30d08c',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:29:45','2020-09-17 00:29:45','2021-09-17 00:29:45'),('82ebb6c809d05a1f2799f36fd244c611823567b36d4783c7528eb7064c951a0011ca9a111f51b9e0',3,1,'user@ladorian.com','[]',0,'2020-09-18 11:19:16','2020-09-18 11:19:16','2021-09-18 11:19:16'),('8308acd7fca84b9442c53cc5edcc2cb0287e1b4c973d0626709618b16dc1bf325faa9452ad12c59a',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:28:10','2020-09-23 16:28:10','2021-09-23 12:28:10'),('83a0188bb61dc76860a618541b39f068f42a5cd0b323391a79f61774eaa7f3be0ff8a355429179df',3,1,'user@ladorian.com','[]',0,'2020-09-24 21:13:30','2020-09-24 21:13:30','2021-09-24 17:13:30'),('83f9522e28535ce096c3050cd9d97fe23895ef3a53ae9bf0e35d54add6735e4147081417ae595800',3,1,'user@ladorian.com','[]',0,'2020-09-18 09:56:35','2020-09-18 09:56:35','2021-09-18 09:56:35'),('85056ec768d0adeb2ca255d0289840cd0828a65c911b62b964359ade017a479c5aa257aacc1b8df9',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:32:02','2020-09-17 00:32:02','2021-09-17 00:32:02'),('8602073421c746c2b5e985852770f9a6cc947027fa406aa070e173ef64a66bf4dabae32bcfc240ae',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:48:06','2020-09-15 23:48:06','2021-09-15 23:48:06'),('869c2aa3f9828665a51500a377ae1b90265f9f4c7f689512e1e447ea5a96eb79ef3502754f581f4b',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:28:17','2020-09-17 07:28:17','2021-09-17 07:28:17'),('88e2ce4a371346edc4bda69a752a8412402b4783334e54493358afd584d997c50c4b07b8d7b75a50',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:14:23','2020-09-17 05:14:23','2021-09-17 05:14:23'),('89136ab239a2ed122ea494e87d6cb1aa7d1c693d13a3081bba6b6507f6625c0b3dd3f93849f00598',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:22:56','2020-09-17 11:22:56','2021-09-17 11:22:56'),('897c9cdb17c9c67bbe98039c0f0e5d4cb726505d723b7c02ce11cd84667c75f7ca443229498bde35',2,1,'admin@ladorian.com','[]',0,'2020-09-24 16:24:32','2020-09-24 16:24:32','2021-09-24 12:24:32'),('8ab7bb0c9a40507f5e173cc666f793c26b2aa1fc6501151235528ea45fb7172c6d02e579b7d67e80',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:37:13','2020-09-16 03:37:13','2021-09-16 03:37:13'),('8d2ea8a5adaf545e70048cf74d6c0efc15f4404bb2072c5b8804077e0e23135c11d64becf8f5bfeb',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:35:31','2020-09-23 16:35:31','2021-09-23 12:35:31'),('8e9a5c6b1a90130336bdf42c2921ce56b29c8434f581a42cbf6c0fa70ba1da7c51593ed493c86575',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:52:51','2020-09-17 03:52:51','2021-09-17 03:52:51'),('8ee6256455de17c435bc695d6d37d878c52a46eefd04de74b37abd6ce5af1bb6db62ed29b018d233',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:30:47','2020-09-18 10:30:47','2021-09-18 10:30:47'),('8fbf16376be6d2cfadfd07f2b50667fcf119231a0e63780d64ca8575f3e2b89e5da27b0fea626d77',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:41:33','2020-09-17 01:41:33','2021-09-17 01:41:33'),('9180d9fc1193e61cfa4b05e61df9f7c1b59e0266ceee32e2c264edb8798a640d1014541e656ac5c0',1,1,'superadmin@ladorian.com','[]',0,'2020-09-09 13:24:24','2020-09-09 13:24:24','2021-09-09 13:24:24'),('91e7b256570d723d8f5d046b296418b86dcc151ba620fde5dc2ccffa1fb43fefa974b931f9a85163',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:05:29','2020-09-17 13:05:29','2021-09-17 13:05:29'),('92117a8529825a950810df673e8265f327eefe52fd9c5c4919257a6fbcca0be5715e70e0c467614e',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:50:44','2020-09-17 07:50:44','2021-09-17 07:50:44'),('92b8f49a2031902b1927bf405da38981143ef1b6826b76819aaa9e6f7fc131416fe9ea62f9134e6f',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:35:05','2020-09-16 01:35:05','2021-09-16 01:35:05'),('930c71943875f38f3887a22c76fec4b93510e26221c3bcaa6ae5ca93cc4e6d2ca6e3c25bf1e324f3',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 03:38:19','2020-09-15 03:38:19','2021-09-15 03:38:19'),('948eeeb6a9dc5c480e10e8d9cdf342e634376826355e5ea03e9dc6e9aae1f4670e048e7659fe8ce2',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:30:14','2020-09-17 07:30:14','2021-09-17 07:30:14'),('989cfb19ca64887a5168d8599f1e702f6ae135352ef1c28bb51ea960de91a5c6ab9ff407b0e691d2',3,1,'user@ladorian.com','[]',0,'2020-09-16 04:18:53','2020-09-16 04:18:53','2021-09-16 04:18:53'),('98d4005c98e7bc7c4652d319a80ada7bfbc8716621a04c5d6b6383c887851441c26c9431d33043b4',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:37:12','2020-09-17 00:37:12','2021-09-17 00:37:12'),('995ddb0321cf0d998b90254dd5bb731061491d74381985bd555066f990c913c2a058c40c681e372f',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:56:42','2020-09-16 01:56:42','2021-09-16 01:56:42'),('9a4939cf39c44f421813262a425c9bdb35f381f4009158f94cda53ed52e259118d5ebf8e8a14e14a',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:12:51','2020-09-17 07:12:51','2021-09-17 07:12:51'),('9a663ad3fd6ddc554ada1718a2a1d63d90f26f31729bfedc81a92e393eb4933154545de53c8e352a',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:33:06','2020-09-17 00:33:06','2021-09-17 00:33:06'),('9acab5cf3ad9d8ce3096df8bb944e3bac7e83aeed1b34faf1d0cdba5b0bbe842b61b2ae0a866ed66',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:39:10','2020-09-24 15:39:10','2021-09-24 11:39:10'),('9adaed6894c908ab17eddb96d6b5accff129646e7f1082b35b27ddc7a021230e99c3fddd6afa761c',1,1,'superadmin@ladorian.com','[]',0,'2020-09-14 13:10:53','2020-09-14 13:10:53','2021-09-14 13:10:53'),('9b297717f11d5f1c055fa11980830c42edc5e2d55f22b3fafff2f17fc623aa7ec0491c9334d8cb6b',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:49:36','2020-09-16 01:49:36','2021-09-16 01:49:36'),('9badb7546318ea684a5362f517545dac51af094996f252e47b939e66b45375e55efdd91a54aa053b',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:32:46','2020-09-16 02:32:46','2021-09-16 02:32:46'),('9e0fc21ee9c63125cc220c1d740d9c716879f6ff095d1bde7c5bed83a9cefd8984aa875e52281abb',3,1,'user@ladorian.com','[]',0,'2020-09-15 23:48:42','2020-09-15 23:48:42','2021-09-15 23:48:42'),('9eb7c589a80bba3ac7b944c9a19c0476fc268fb8b8cea021b4266365c03ca83efb0fdc5dbbc48367',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:01:44','2020-09-17 12:01:44','2021-09-17 12:01:44'),('9ee197c695bde32e44ad8a692e3e6bfaa393e044d2d4e6a9a3dd74d97129a1faafcdf494e9aff2e2',3,1,'user@ladorian.com','[]',0,'2020-09-15 07:19:25','2020-09-15 07:19:25','2021-09-15 07:19:25'),('9f02924ec9e7a396ff257c8dadcf0eda629c3287b92240e1d564add3b0bac3da34ac6cec0cf39430',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:14:06','2020-09-16 05:14:06','2021-09-16 05:14:06'),('9fbce190f829f4ddc5198dca41c06f6664b52a5207b0afdaaf02efb26ba83e79e6043271a9b7549d',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:13:23','2020-09-17 11:13:23','2021-09-17 11:13:23'),('a0d27d509eaaf870b3ea5d658f30f9def41236bf0a4d6f28709ab21941a967bd424a4c491c4ee4cf',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:00:18','2020-09-17 06:00:18','2021-09-17 06:00:18'),('a1ef27410c33825d6001039aabee67a68256519ad0ec77042f3cc8775cef4b953e12ee9a44a92439',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:10:58','2020-09-18 10:10:58','2021-09-18 10:10:58'),('a4515315dc497249715dfc9bf32c62b30e314323bb22abe1824dcc2974707cbc143ceb6241c6db77',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:34:12','2020-09-17 03:34:12','2021-09-17 03:34:12'),('a46ab58a84b1f63a1718e6cd7e8ae2406c3796626b7a8055347e5e16e0338a3538b07f1154b516c7',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:31:28','2020-09-17 13:31:28','2021-09-17 13:31:28'),('a46f9fa75ac9d6cf4eb1e7a3314756240333d8a5f7dcde28312dd43792a9e22487f6b7b2913ee13a',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:36:28','2020-09-23 20:36:28','2021-09-23 16:36:28'),('a559244f05777d96c7c3950c8df14e6837d4bf5e41efadff8194c99298b327b04a43c085f0ba9606',2,1,'admin@ladorian.com','[]',0,'2020-09-09 16:25:23','2020-09-09 16:25:23','2021-09-09 16:25:23'),('a694cfdc324a2cb2da13dfd024f74653bae8810586b0bd6612d76bded0afdae55161602c14cf9096',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:52:23','2020-09-17 01:52:23','2021-09-17 01:52:23'),('a79ab0b6b20c068dbfb7b3e202fe28e86b1d8456031a912409fae6b3ad14143480d1e4b3e21f4058',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 18:00:15','2020-09-15 18:00:15','2021-09-15 18:00:15'),('a7bc77abb1bebbba28f1e365d289a8204d88a4382ff757e6c3f3b4a4f9602a49414617c911efe866',3,1,'user@ladorian.com','[]',0,'2020-09-24 01:38:09','2020-09-24 01:38:09','2021-09-23 21:38:09'),('a803cb3ff0b75053274b9162f6ea4745606450c8560c08a6b73b7b27499d0e20dc832a200ddc1d51',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:43:26','2020-09-15 23:43:26','2021-09-15 23:43:26'),('a8f03ca0505ee8290d812fbe43377b6ec42925ffd8dd6a62674a3f719a70774b3cae8f0aa359a044',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:38:17','2020-09-16 05:38:17','2021-09-16 05:38:17'),('aaa1ea4f8834b5b45939cb889d994d850e5e8fa74d8053b51b500083ec3f0c0a2daefdf8bb358805',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:04:41','2020-09-17 14:04:41','2021-09-17 14:04:41'),('ab0d4003c599eb7848fcc079feeacda1c66c792ce295a923d713ef9e765ef121b513a1ff4ea31849',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:51:38','2020-09-17 01:51:38','2021-09-17 01:51:38'),('ab7b99ae171a82abe38fb1dbf00c471e9fd8de1abeb390a63583e82b0ae2d7408cfdf008a42de838',1,1,'superadmin@ladorian.com','[]',0,'2020-09-10 00:12:14','2020-09-10 00:12:14','2021-09-10 00:12:14'),('abf6e160a3907ffdc8e1c55d2fe686f45d5e1f67b1e59e885b7e74d034dd92b4517415df24e481d6',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:42:35','2020-09-17 05:42:35','2021-09-17 05:42:35'),('ac1b2c52d54d8db769d100b138474f53adec7b2c9ae33d74f0e861cf2fe34b262bf41d388d555e83',2,1,'admin@ladorian.com','[]',0,'2020-09-24 20:30:28','2020-09-24 20:30:28','2021-09-24 16:30:28'),('ac4ce2a0cb0801cd0d027e6542e3269e1e544266063a67eb14866c1ccd28e96702a5328306667fa2',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:54:49','2020-09-16 05:54:49','2021-09-16 05:54:49'),('ad06851805e0783894e1c69513ce0a2eac4582b9cc95bf379cce098c7b317b589dc5e74b819c77ac',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:55:48','2020-09-17 03:55:48','2021-09-17 03:55:48'),('ad2c9bd0e32a4a0d96372f64eaa8ee633a2cf9bf6931f2d090e2cf825c9a10710d6b2d2a1fe3a616',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:22:48','2020-09-17 14:22:48','2021-09-17 14:22:48'),('aeb239f9fc0a26f99370ea6558a778d64af0f49d06c6c4fa1bac0fa54980dde0313e95203305da79',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:15:30','2020-09-17 12:15:30','2021-09-17 12:15:30'),('af69d37db60b0f018e2cd30bc440e1266ef020da897249cc26798881ee10463345f223a8dd806a21',2,1,'admin@ladorian.com','[]',0,'2020-09-10 00:11:24','2020-09-10 00:11:24','2021-09-10 00:11:24'),('aface95f6f132db4af4613ed417bf975165eddc75ab8af134b60cdc991652f719bb40e80e2f03eaf',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:23:02','2020-09-17 15:23:02','2021-09-17 15:23:02'),('aff6c67afb41cbb3cb4dbc9f1c90ff73ef210ebd1f69d171a841753564c7b13d315904d78bbefeee',3,1,'user@ladorian.com','[]',0,'2020-09-18 09:48:44','2020-09-18 09:48:44','2021-09-18 09:48:44'),('b09fc3f3b848fb569e9dfb074fd7222095f41a2d630fec86e32e6aa7f7768f168ff83b8a6fd1b569',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:18:15','2020-09-16 03:18:15','2021-09-16 03:18:15'),('b0b2a5a40a084b1daa826adda79875c197134b75067d3df3b7df60ae2db0ababc92389d878ede20e',3,1,'user@ladorian.com','[]',0,'2020-09-23 21:37:13','2020-09-23 21:37:13','2021-09-23 17:37:13'),('b0d905229b9237de1c9c161e808b62cd52869669582abac3108c0a5a5927b40cec654e1fa038588f',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:36:48','2020-09-17 13:36:48','2021-09-17 13:36:48'),('b137b3a81b199609e98a9202aecd430ecff757c40ef9c70b548f8f3df6061e6c04c84877b2979013',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:18:54','2020-09-15 23:18:54','2021-09-15 23:18:54'),('b1ed0318ee5b97b6c9f1fb9115ba26640bef76d82e0db2850c30a11e21a4fc00a6ffa09bb42cc27f',3,1,'user@ladorian.com','[]',0,'2020-09-18 16:21:22','2020-09-18 16:21:22','2021-09-18 16:21:22'),('b2546634096e2b158b08f441305e0e71d4227f67ecf056103b66c74fb4b5b6800820ddcb280c82bf',3,1,'user@ladorian.com','[]',0,'2020-09-16 04:38:26','2020-09-16 04:38:26','2021-09-16 04:38:26'),('b27d5e89e2e832856392a10f0e8c24a530ad71af21458210e1d8e593cf8b8913ad99dce2c08a0a44',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:03:00','2020-09-16 02:03:00','2021-09-16 02:03:00'),('b288e9b6473531a6093ec265ea2f75ebadf30446bc00018f7b6a0de68a55d6f950eb44f62b7e027b',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:25:53','2020-09-17 11:25:53','2021-09-17 11:25:53'),('b2aa77058955fa01fd4ef3b2f6ac60fbc11b7d788874ded4321e16a9bc7c67f9ac935709cda28672',3,1,'user@ladorian.com','[]',0,'2020-09-09 16:03:17','2020-09-09 16:03:17','2021-09-09 16:03:17'),('b339f89fefb4d8178dfc7dfc050169d628c932f9940994559b733dad1bf86af7ffc6858772f04fb1',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:44:51','2020-09-17 04:44:51','2021-09-17 04:44:51'),('b3b6cb4e7e0501e6f456e0c4f1b961b3dbedc7fbf6bbe8278e1a384ea7bcd78292501600385bde77',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 17:59:41','2020-09-15 17:59:41','2021-09-15 17:59:41'),('b3bc68e092690a0c012e1456f850f795d96aede521ea71aa04e3c9939dde275222117d51e38ec44c',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:01:16','2020-09-16 17:01:16','2021-09-16 17:01:16'),('b3ef5a1cf061dc994f254733c1ba0cebf608571b18cbd50e89392fa35fcacda2f63800ce76046fa3',3,1,'user@ladorian.com','[]',0,'2020-09-16 23:40:28','2020-09-16 23:40:28','2021-09-16 23:40:28'),('b439fc99d744af01f3cd7cf51ce5f25dbbfe30ade2c2dd7652eeea44126a34de3223d8208f964438',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:40:41','2020-09-16 05:40:41','2021-09-16 05:40:41'),('b494fdc7a1cd4e1858cd11213fe715001ff66b21e270c45abf96e2cb06bef4cf6846b5b38396dd8e',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:20:18','2020-09-17 13:20:18','2021-09-17 13:20:18'),('b59e6db57592ca8cea0a1f62ac6d5a2f1e37f1daf021adb62c022f12dce51902483b7558ec89614a',3,1,'user@ladorian.com','[]',0,'2020-09-24 16:19:21','2020-09-24 16:19:21','2021-09-24 12:19:21'),('b6386da45ea59079d0df28b54a601881a2841d27b3f083cd870ec5c717be616f7d353ad13b2a1094',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:24:17','2020-09-17 15:24:17','2021-09-17 15:24:17'),('b7301ab1c1b29da661d214c9f6231c35576af8fcb1360bb125ca6eb80b1dd24f6f8e5ed2758a3af4',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:41:49','2020-09-16 01:41:49','2021-09-16 01:41:49'),('b8168107a5d3e88a616a7657024955237a8b9771c1ec3b621a59808683f159ea43e125b7164e16f6',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:10:18','2020-09-17 01:10:18','2021-09-17 01:10:18'),('b86732ceb5e03ed54322650d403b48e300544d628c23769d8a7a9ec6e971bada518163c202394a4c',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:00:02','2020-09-16 02:00:02','2021-09-16 02:00:02'),('b8c1d3dd6a9d2539724cbaef5aeb399dd97bc9c3d66c28243818d314e8d29e6f0380e52ec4a1c500',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:20:02','2020-09-17 07:20:02','2021-09-17 07:20:02'),('b900ed8691d94d989a0c72a4edecb43d78b12637e19eb26b111eaec9980a2ac8977963a71f71af0c',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:20:43','2020-09-15 23:20:43','2021-09-15 23:20:43'),('b97940d342758a3f83ec087e58a5a677dbcfb15a098b6efd80270a3957b9008a3e35e3a76d4f1175',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:17:00','2020-09-17 14:17:00','2021-09-17 14:17:00'),('bad112e0147256e829ce0101415743e4c20b35d571930638c2936db37bbe485fc79f1ff676c61cd7',1,1,'superadmin@ladorian.com','[]',0,'2020-09-09 16:31:59','2020-09-09 16:31:59','2021-09-09 16:31:59'),('bb59ca00733a251ea3e6e7bb68f3bda36c3a7615819b67cbc803fc4101d0b0256f75b18ae942e344',3,1,'user@ladorian.com','[]',0,'2020-09-16 04:17:09','2020-09-16 04:17:09','2021-09-16 04:17:09'),('bbb29e1b1ea51e0b11777f1ab2f81309f917ca6aff9e5cad43ce514030810258f0b56bb921897da1',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 17:57:08','2020-09-15 17:57:08','2021-09-15 17:57:08'),('bbcdd7fff7a276b59a0144b83c33512174629289f5b60e495d2df39cfd4fe0e13694ceaf3a23346a',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:24:14','2020-09-17 13:24:14','2021-09-17 13:24:14'),('bc1c8314c328419d918a08da45fd26ec443c81874f9102e9d27aa0fca471839c5cd0529d10fb4c60',3,1,'user@ladorian.com','[]',0,'2020-09-24 20:39:11','2020-09-24 20:39:11','2021-09-24 16:39:11'),('bc53bd013850f7b7101590d7d945eb8f9acb5ff20f1884d35bd9712d0c65a020e683afdb92abc272',3,1,'user@ladorian.com','[]',0,'2020-09-16 02:07:01','2020-09-16 02:07:01','2021-09-16 02:07:01'),('bc81a9cabb7b94ebdd1023dd57fc619dd062d96e6a7772093c2732e6028419926bc608bf21cd7406',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:31:42','2020-09-24 15:31:42','2021-09-24 11:31:42'),('bcb39ccf3c4d234304ce995220ae009d245242c1885b63dff11d996edd642b2808dc1d169bf09a57',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:29:21','2020-09-24 15:29:21','2021-09-24 11:29:21'),('bcee3167fe0784bc63690bc4f2336dca07767de2aae776fb59b7d7b5d09747b6b80b6effbab94d5c',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:42:36','2020-09-23 20:42:36','2021-09-23 16:42:36'),('bdcaefe821d83ebfbbc28073dc8eb7b26ce189114e21fc6828673a621258452984dc2a61f13d7fd2',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:11:44','2020-09-16 03:11:44','2021-09-16 03:11:44'),('be7bcc1bef44d774afb2c2121885ffc31c3d7c671fac78fb8e4eb2628d2d841bf661b192fff9b932',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:18:40','2020-09-17 12:18:40','2021-09-17 12:18:40'),('be8a16d57eaabf2a2ffd7c26c9c3e6e35d977ca745bda81bf4d1365e9072dcfb4a6230de190ce655',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:23:12','2020-09-17 13:23:12','2021-09-17 13:23:12'),('bf19af4a56a262cf9940138dba084461ce4ee2d5feb374644dc78db35d2b3f66d4af7d3e0a8a7c5f',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:26:37','2020-09-17 07:26:37','2021-09-17 07:26:37'),('c0242d7a7ca8d07aee2965b7250ee9cb71ebc7e0d250319f161809238aedd8219fbab25c30efd604',3,1,'user@ladorian.com','[]',0,'2020-09-25 04:03:43','2020-09-25 04:03:43','2021-09-25 00:03:43'),('c0d099aad8ca986cb85ee3b557eca9e62f967e9526a6ef296fd6e0bcc2aa69576e69cd9bc38a0170',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:14:08','2020-09-17 06:14:08','2021-09-17 06:14:08'),('c131675d3d981e3c7a3dc582482cecfb29ca30a8fa098ed053e086fe7d8636352766e781ed154afd',3,1,'user@ladorian.com','[]',0,'2020-09-25 05:01:05','2020-09-25 05:01:05','2021-09-25 01:01:05'),('c1693fd03d0493b43ab9fc90fc9efcdd6e507c02bc3f6b376d3b874217c4630897e014135b5f627d',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 10:45:42','2020-09-15 10:45:42','2021-09-15 10:45:42'),('c2249728568b9cbc24084f14a41c69e4856d210828a3f8debbdf688335e457728f5c80e6988320bb',3,1,'user@ladorian.com','[]',0,'2020-09-20 21:30:12','2020-09-20 21:30:12','2021-09-20 17:30:12'),('c3c8a53cd8fa24149b15f97a5bc60f6e93188f8456c13560ff8256e8aee64121275533d29490e120',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:34:59','2020-09-17 00:34:59','2021-09-17 00:34:59'),('c418bfcd2868e82352c27bfeb78096f3613508b3f6bdad69744cd6f6379c27977c305935a5863bf8',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:19:13','2020-09-18 10:19:13','2021-09-18 10:19:13'),('c4ca0f0cb2fcedbb4a4fef581df76d97ddb3c84f33b21046fc50565859f3270149909cbba569d3c5',3,1,'user@ladorian.com','[]',0,'2020-09-14 13:10:14','2020-09-14 13:10:14','2021-09-14 13:10:14'),('c4d59568e58af5492574ab6f9d7f1d54460e707f13c4806277fe2086f748a1dc6315002353f2be02',2,1,'admin@ladorian.com','[]',0,'2020-09-09 16:19:41','2020-09-09 16:19:41','2021-09-09 16:19:41'),('c4e6468b22f87024e5ef2299199b148882600f34c6dc7a29825a9666c19e11249913ea22788d80e7',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:40:17','2020-09-23 20:40:17','2021-09-23 16:40:17'),('c5358c76d9f8fc56bea32361a91488cc6c657a1480b3feb89e2230f777ca2a0b264a6ed71a0a499c',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:23:47','2020-09-18 10:23:47','2021-09-18 10:23:47'),('c55462d9726d890c82d8bef8c4bcfdeeb08f41602ab3d80e566e440b3a13eb2a40e596ed8d2f7c34',3,1,'user@ladorian.com','[]',0,'2020-09-23 19:03:34','2020-09-23 19:03:34','2021-09-23 15:03:34'),('c5bb798b9162fcf1468a5d61066ae6cbe40846a01fe6c90a482b14a5d6630717bfe55f52cad9a0b5',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:44:13','2020-09-17 14:44:13','2021-09-17 14:44:13'),('c72ef0e1352a070b15384a9a08e117568daf1c5c9ee2db0ffc6bacf580f33198ee371356fd82890d',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:15:08','2020-09-23 16:15:08','2021-09-23 12:15:08'),('c792774d925dfd0bf243f78f37938b357efd1ad65a01b50dcb08be23d2aa2d8833b691c467928f6b',3,1,'user@ladorian.com','[]',0,'2020-09-16 23:41:18','2020-09-16 23:41:18','2021-09-16 23:41:18'),('c876c3a9964199c868a4958c1cecaeeeb4e19af591ed3ea708531e96e1d14725a2a402ea7754af2b',3,1,'user@ladorian.com','[]',0,'2020-09-25 05:09:02','2020-09-25 05:09:02','2021-09-25 01:09:02'),('c8fe5b8c9af37dbf21e57d91e854f1669516448347b2e478f470178ebd4c562556171bc03cdba496',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:36:06','2020-09-17 06:36:06','2021-09-17 06:36:06'),('c955b342c13804da1f5b8adfadb54231d314eb2d3b810822296cfa4c095bf5d3d7bf2e02e7702eb8',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:20:20','2020-09-18 10:20:20','2021-09-18 10:20:20'),('cb1b59ca4449c1847769ca726154f4596c66a0236895137fbf9eeba7e7ade24a5051d21be0dc4166',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:50:59','2020-09-16 01:50:59','2021-09-16 01:50:59'),('cb7fc240f1ac0d9b2843d635f45a72083aec34d2ff9f3e4797399755efae97802971eeb28ecca745',3,1,'user@ladorian.com','[]',0,'2020-09-23 18:24:41','2020-09-23 18:24:41','2021-09-23 14:24:41'),('cc87d2a692b80d466f79edce68f7453d70f3347611ea0373034e5158f1b54733df1e1bc8fd871ce6',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:05:53','2020-09-17 11:05:53','2021-09-17 11:05:53'),('cd803f408b90fda01ad5456bbca2b1096718e889335fea6522bb32063459ab3f8ce953b932926e21',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:39:46','2020-09-23 15:39:46','2021-09-23 11:39:46'),('cdc27662479c75bb32b3e9e9fb4835187b21dc4e8659b30ac84ac71f5f818469794672a1886ead94',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:19:02','2020-09-17 11:19:02','2021-09-17 11:19:02'),('ce72b5e8a1e7660b8109da1b88547d6bcdb7d527090a624b31e8a4d7f4efe520fb877108f2356c08',3,1,'user@ladorian.com','[]',0,'2020-09-17 15:29:58','2020-09-17 15:29:58','2021-09-17 15:29:58'),('cf57dc94350fe1bcbbd39f75692bdb72b92a4d1725db289bcc264b0064ccaac8d1bb53b6aeb4f057',3,1,'user@ladorian.com','[]',0,'2020-09-17 08:01:08','2020-09-17 08:01:08','2021-09-17 08:01:08'),('cf9555f5f74447b00e49ce9a568d9cc3039b984b3a4090a19bf7280dbb48024ea4e47b27c984552a',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 17:55:54','2020-09-15 17:55:54','2021-09-15 17:55:54'),('cf9b5703781d89c2118fe413ec5527ed7efd494b2c30b356d5738286e5656e610a3ecdc3664b0620',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:50:58','2020-09-23 15:50:58','2021-09-23 11:50:58'),('d02fec2aa6bae975130b870c44fe645dc46d5c1ec3d78865f03d5035cba54d481ba12be60d27556f',3,1,'user@ladorian.com','[]',0,'2020-09-16 23:54:06','2020-09-16 23:54:06','2021-09-16 23:54:06'),('d093dd1b3384fd7bea0de7ae24c0c7bd88d383ab4b26dfb11f1be50e252efe5ea9f47e7eaa7ef6e5',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:39:18','2020-09-23 15:39:18','2021-09-23 11:39:18'),('d0d2d78a306811779ed489b8dae67d14a92cf7fb636fa1fd65e186261fd486ddd69ac4c79173b4b3',3,1,'user@ladorian.com','[]',0,'2020-09-14 10:59:07','2020-09-14 10:59:07','2021-09-14 10:59:07'),('d249388d6658a60b5a491f97c9c51fa8eb36871fb5963031b32df63975156d9b64861e1a0461959b',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:31:24','2020-09-16 03:31:24','2021-09-16 03:31:24'),('d254fca3902548904cb127eca0f5061b2bdd5bfed9e7559df1b58a50ed4d79642f331e1d95d91788',3,1,'user@ladorian.com','[]',0,'2020-09-24 17:20:29','2020-09-24 17:20:29','2021-09-24 13:20:29'),('d267710c07447cee2a65d25b62d48a877baf532428282c4b6c7951ce6fc9a11dd83d47bc494719a8',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:31:11','2020-09-18 10:31:11','2021-09-18 10:31:11'),('d40ddc42f127936b7b699c34325c99422bc81dc8cf7abfd2a3546712461d96a02e60f8f9858dd65f',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:26:51','2020-09-17 00:26:51','2021-09-17 00:26:51'),('d4630e11c12b82802b24cc5672c23492df46cc1067b88fd1d635e56f6710e7b87b8687d9ed14b7dc',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:55:10','2020-09-17 07:55:10','2021-09-17 07:55:10'),('d55c304202c3a21b8841cf28ad78598d5259311f25c8f46f4279353696642efbc8157848e1cb6605',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:33:27','2020-09-23 15:33:27','2021-09-23 11:33:27'),('d65cc729b9b4cb6d3675961466e1b0d4f0813f851ae675bddd2a818e70be7bf7d31c4d30ea6e3454',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 15:24:17','2020-09-15 15:24:17','2021-09-15 15:24:17'),('d763337fe8c7243ea8adf12a063638be497d5d2ea733f42d0f844b20b576d43a71e41e6c222481a4',3,1,'user@ladorian.com','[]',0,'2020-09-16 22:32:05','2020-09-16 22:32:05','2021-09-16 22:32:05'),('d7caa3d37b0f43d4d7227e07c16ce14cf5d3b76d792f237293e9cab71bc29b7300797a45cb33d3e0',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:58:10','2020-09-17 05:58:10','2021-09-17 05:58:10'),('d7f7035653945366c899382fbaf304669b6cd3256eb28207748ae1ac1b400284ea96a71f85425151',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:06:40','2020-09-18 10:06:40','2021-09-18 10:06:40'),('db89fcafdadbf58e5ee8ca2893c3d319c6687dd8ac991ea2f3b76ffc860614e7bbc090655bc45a1e',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:24:01','2020-09-16 17:24:01','2021-09-16 17:24:01'),('dbe8ab3a009bd04d438af3b739dc87e91ab4378a4cec045e48e5dd9e3b0da27938edc56b132816fb',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:56:58','2020-09-17 05:56:58','2021-09-17 05:56:58'),('dbef09fed88116919b19d4be3fd2607e6132aea6e426c6c57a2ec792ce4e58050286f4c774705faa',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 17:58:09','2020-09-15 17:58:09','2021-09-15 17:58:09'),('dcd1e80c656fce979563be159c4770c5350574e32e81ea8e7c1b658efd3b845e2680fc88b2b77e07',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:27:55','2020-09-17 14:27:55','2021-09-17 14:27:55'),('dd4b51fbaace44145bec2c3a154d99254d2080ccee5a9ae25ddd08d5f2b2f19f6d3a98d5b7be08f2',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 03:49:52','2020-09-15 03:49:52','2021-09-15 03:49:52'),('dd511a15faf91c7603e7bf7a5188ff30f8c5d5eec737de1989328259b55dd844e756a7e8a3dd5eac',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:08:29','2020-09-17 11:08:29','2021-09-17 11:08:29'),('df310adf231a67b08252981b3a8ff1c80cd65478eae7ae422ca7449f1e0b625fe18983d072e95331',3,1,'user@ladorian.com','[]',0,'2020-09-24 20:36:03','2020-09-24 20:36:03','2021-09-24 16:36:03'),('df5e610551019f6203b290df220943c5f0645cd0d30274a9b605a8d83262204cafababedc1019c0d',3,1,'user@ladorian.com','[]',0,'2020-09-16 00:34:07','2020-09-16 00:34:07','2021-09-16 00:34:07'),('dfd350f25b08891ee36604ddd2ac773f723d55bdab2d44b9071bd7dc4f6577ccde75ec8885ec0ff7',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:08:53','2020-09-17 07:08:53','2021-09-17 07:08:53'),('e131ef107830541bdfe7cd7440e0a4463a19e66c888a7c214a0a9fa7146e2b8a052f3210a8fdfd5c',3,1,'user@ladorian.com','[]',0,'2020-09-17 02:07:12','2020-09-17 02:07:12','2021-09-17 02:07:12'),('e13e8ac59deeef850084810590b044715b34d80419eb0b2c625b49c414087fd75857304a2a03b500',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:22:56','2020-09-17 12:22:56','2021-09-17 12:22:56'),('e188f9685ec10cf4f1b71841dd854a492b38057074ddf19617dc4df9e1dc4ba3a559bd6271aa3d9a',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:32:44','2020-09-17 03:32:44','2021-09-17 03:32:44'),('e1af70ca8d796a41ece4c52cfb3ea69235e56caa4f17d65f9d4ceec4667027195a6e61a7bfc5766a',1,1,'superadmin@ladorian.com','[]',0,'2020-09-16 11:24:48','2020-09-16 11:24:48','2021-09-16 11:24:48'),('e1c96030993360224e7be8c18e63079f8f011b08f9cd18f93c9fc8e013675022e634889a6c369e34',3,1,'user@ladorian.com','[]',0,'2020-09-25 04:50:18','2020-09-25 04:50:18','2021-09-25 00:50:18'),('e22a4a36f65518f2fe27a908ca3ea8deb6a1208feb23eae8341864501bb5127a8c9895a652e65c05',3,1,'user@ladorian.com','[]',0,'2020-09-24 15:28:11','2020-09-24 15:28:11','2021-09-24 11:28:11'),('e347c07cfcce6f49f1a2d2f3c1ccea51971b712fa0b65e146167282e75cba87fd5322a255b0b0790',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:07:18','2020-09-16 03:07:18','2021-09-16 03:07:18'),('e3acefd8d11b6b6c8602d430f1e79fff29aa4ead32ccd0d2f21cd92d6fc95fd134fa14472441c502',3,1,'user@ladorian.com','[]',0,'2020-09-16 17:48:36','2020-09-16 17:48:36','2021-09-16 17:48:36'),('e43577d2a8dbd1730665190a80b0d009f5c717ce72fd937355608f0ba252c148a8775a0594497f38',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:22:25','2020-09-17 05:22:25','2021-09-17 05:22:25'),('e45ec8f9334330a7aba6da5b481a800bbd2e5749006e79b7b44d148210909de027d3abba3980db94',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:50:05','2020-09-16 05:50:05','2021-09-16 05:50:05'),('e46d03508edf4e37559780964c927d6d7dafc887d66c20be33cf76c2c1d5fd37f30d7c22a6bea9d3',3,1,'user@ladorian.com','[]',0,'2020-09-23 15:50:38','2020-09-23 15:50:38','2021-09-23 11:50:38'),('e4e415d0090b46dcd587eaa7d28f2d081deec36d0c63ccc4335ad5230019a863297dcf0648565914',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:51:29','2020-09-17 05:51:29','2021-09-17 05:51:29'),('e559e77b3680a4b4655c5cd710dcc7879a86d093ccddf3d57686abce71c86baa9709ae045b1a7961',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:59:37','2020-09-17 03:59:37','2021-09-17 03:59:37'),('e563c4c82d403262806952c0898b4460cec3c8e90b65924d410eba2f132e9552b6b411a5dd89cf97',3,1,'user@ladorian.com','[]',0,'2020-09-17 00:59:33','2020-09-17 00:59:33','2021-09-17 00:59:33'),('e7245f5d7e370f4c953fd6ea7a375570a25a8ec54fd555937c5b565cc5575bc0ef9d9b33d87ac1f0',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:44:15','2020-09-16 01:44:15','2021-09-16 01:44:15'),('e742d78f5adc54e240bd9c4d785b54c8d2fd0b65c65ddfdc1148ff9cd0a4d9b2e80038ca771aded4',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:40:00','2020-09-23 16:40:00','2021-09-23 12:40:00'),('e834832c9f505a852b828755b434e9f606843176c8a8bb12e37fc5178beb8698ea7c38fda5cfeb93',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:25:07','2020-09-17 06:25:07','2021-09-17 06:25:07'),('e8c52cb13429fcd8a5d042ca8690cc57d85423579587a62f1c7dab2fe4ed2322fe0067f2eb709cba',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:16:50','2020-09-16 03:16:50','2021-09-16 03:16:50'),('e8e471a8f8933527028a12775d5ad6aae795d0e354e0b2cfcf3d86a4746440a0d7b7d3e4a30da322',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:41:27','2020-09-17 03:41:27','2021-09-17 03:41:27'),('e9c1e75ee672d2d21d18cdb4afb3aedbe227fa082144909cb3ab6fa927ca8c63c22fec2a548c1904',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:29:53','2020-09-23 20:29:53','2021-09-23 16:29:53'),('ea25e8d07335d1198a067aee24182d5b2b3e06fa6f20fe112b334eb58706a589a983e73027155c6d',3,1,'user@ladorian.com','[]',0,'2020-09-23 20:43:04','2020-09-23 20:43:04','2021-09-23 16:43:04'),('ea2b08a786c42528e24febd75c68633a91b8dda2d79e3806511b064e17a21417e0d42484cc5e1b86',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 23:21:53','2020-09-15 23:21:53','2021-09-15 23:21:53'),('ea5d73bea109696235f1115769fd766c68007e2af7589424dd3f4db6713dc8686c8e648c62d19e82',3,1,'user@ladorian.com','[]',0,'2020-09-17 03:40:16','2020-09-17 03:40:16','2021-09-17 03:40:16'),('eaf2120fc2c73d23ed006b95e11662c7fd9918d1b8d992320c8a4e92055985f0cd083ba3155d62b1',3,1,'user@ladorian.com','[]',0,'2020-09-17 07:43:23','2020-09-17 07:43:23','2021-09-17 07:43:23'),('eb3315f845c07dabd80d276d2a15c746eabb79e31e312e9bda969d82c70e78293c1e3db185fdc053',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:47:56','2020-09-17 05:47:56','2021-09-17 05:47:56'),('ec48170d04f540dea93b6679c3358b374464725f0f40a0b87a6aba091ddc9e257c109064d2f4d273',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:22:48','2020-09-16 05:22:48','2021-09-16 05:22:48'),('ecf259f292c32db39069473e0103af50db5bd40ab86116024ccbb86ce3cf0d73ba15b48467c547b6',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:40:04','2020-09-16 01:40:04','2021-09-16 01:40:04'),('ed3f2c3b236aa57c64def032537de09c0642c2a0ff087c8a13fe7f23ebd85efe053757804f7c4580',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:57:59','2020-09-18 10:57:59','2021-09-18 10:57:59'),('ed5ec536e2017ec1ef6eed0fbd1214a48b961508eaf8b287fd1f792761cd4d020dde73136771de06',3,1,'user@ladorian.com','[]',0,'2020-09-17 11:20:52','2020-09-17 11:20:52','2021-09-17 11:20:52'),('ed85d89b5cc90ad3f6c351a1e8eab6c473ff7b27bcbcd697233c82ccbd91f095cb3e883ecdbabb7e',3,1,'user@ladorian.com','[]',0,'2020-09-17 06:01:40','2020-09-17 06:01:40','2021-09-17 06:01:40'),('ee1afbe34eff42d2d3c7fdb26cdef70c4b5884091953145f2c9eca63def97d92eb68eeb0f64118cb',3,1,'user@ladorian.com','[]',0,'2020-09-18 11:19:47','2020-09-18 11:19:47','2021-09-18 11:19:47'),('ee67e805c161af98790ffc22fb7295ebd68ec1a8081fce1b177136f976d873549151fcd9bad7fb16',3,1,'user@ladorian.com','[]',0,'2020-09-16 03:28:03','2020-09-16 03:28:03','2021-09-16 03:28:03'),('ee8770aaaee976a4169b507cdb842e1b2446e8271c1073a161dc13e85595acae2a93c0b70ba120bc',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:08:45','2020-09-18 10:08:45','2021-09-18 10:08:45'),('ee8ade5aaeb915008d118c816f8c0c991b5b820a6e30c16738345ee24d0a049e38987e7e8965aed0',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:55:48','2020-09-16 05:55:48','2021-09-16 05:55:48'),('efa6262329b550ba2ecd08ae69feff9fddd39b170dab6c8076445e3d5b34d9cf8867057c50012f2d',3,1,'user@ladorian.com','[]',0,'2020-09-24 01:36:00','2020-09-24 01:36:00','2021-09-23 21:36:00'),('f027c1b2ba8e486316029318a65b8fc7cd55d165af8a3ed47c65a296ce33ba68cbe209749ac01c3d',3,1,'user@ladorian.com','[]',0,'2020-09-18 10:12:40','2020-09-18 10:12:40','2021-09-18 10:12:40'),('f072bba56c26a10f6790d75ff17af212ac4987e77395404849c4e4695c9eb765d8cf9169df9e6032',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:27:52','2020-09-17 13:27:52','2021-09-17 13:27:52'),('f07ca040dcc025271afa6e7a0e945056b3131089ca5b6a2d8dc6928c06d92f7cf1c3ac9593648b09',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:09:19','2020-09-16 01:09:19','2021-09-16 01:09:19'),('f167880230139ca688c78d893cb9c68513dfdb6ce2171f54080d05842085524c1f41c1dc8f3c134f',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:12:44','2020-09-16 01:12:44','2021-09-16 01:12:44'),('f19f328eeee07084974bc63a4ff849074f304bd7ab707eb1c330bf8f3695e9508791772917498f79',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:27:28','2020-09-16 01:27:28','2021-09-16 01:27:28'),('f2ae77bb20454ad965e1a6a6fb5751cf763c40501a4646c4107a77d69cdbf27ecda1b679b5152547',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:15:54','2020-09-17 01:15:54','2021-09-17 01:15:54'),('f2d711dd34e864d5719ffca01f34ecb3c6ad302730b39bc2841c25e857d7a104ea6c7f8f7072406e',3,1,'user@ladorian.com','[]',0,'2020-09-16 23:43:28','2020-09-16 23:43:28','2021-09-16 23:43:28'),('f5309f417e55f5918c5ef4b0a2500437dc192feeba4295eb1f4aaa329b0ef33396d72271e5be2134',3,1,'user@ladorian.com','[]',0,'2020-09-16 01:55:07','2020-09-16 01:55:07','2021-09-16 01:55:07'),('f6287a393f99df686b452f9e89f4ab072d8aad96ddc3884da05ac660c5e4fbafa3009d5056604a97',3,1,'user@ladorian.com','[]',0,'2020-09-17 14:08:59','2020-09-17 14:08:59','2021-09-17 14:08:59'),('f63fa5f55d5896dc22896d8cdb877e243330955c87c86c335d0cea80d9531ecc64380a6e85bf8eb9',3,1,'user@ladorian.com','[]',0,'2020-09-23 16:33:38','2020-09-23 16:33:38','2021-09-23 12:33:38'),('f6e622e0dc928129d9cfe4ac3fe49119cca39cb89f7fb88e265e613fddb0a430ff547af2127d80b5',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 18:01:00','2020-09-15 18:01:00','2021-09-15 18:01:00'),('f86a7856e882cfdea2e7bcb1f37b5d0604b10dddf5f62f6da9aa21a000b00d3412bf6e00e1beabfb',3,1,'user@ladorian.com','[]',0,'2020-09-10 01:15:36','2020-09-10 01:15:36','2021-09-10 01:15:36'),('fb2e723f7afa02116de632e8f0904cfc15ab15a67d406f5c47d5ed76364871d2f91fb98799d6b602',3,1,'user@ladorian.com','[]',0,'2020-09-16 09:20:38','2020-09-16 09:20:38','2021-09-16 09:20:38'),('fb682c1daca337e407d29388c8b44e82b4dacb0a7d2b11506897c5fa5a161b7a2c0539139c1d11b0',3,1,'user@ladorian.com','[]',0,'2020-09-17 13:26:15','2020-09-17 13:26:15','2021-09-17 13:26:15'),('fb81f2fae14208e9a4a6819ad93521fbd2c28213bee14a67a00c9fbd979cfdfac6c759d7e8adee0b',3,1,'user@ladorian.com','[]',0,'2020-09-23 14:44:30','2020-09-23 14:44:30','2021-09-23 10:44:30'),('fc85ff8ca6064bff5a3bfbf6346c6c95a95835eb8c76a7efbcb94a9c2a3e2a2d5a3df2217cbb7219',3,1,'user@ladorian.com','[]',0,'2020-09-17 01:40:31','2020-09-17 01:40:31','2021-09-17 01:40:31'),('fcc81345ca38c19a2c8d590125def1e8cd1f76d5dd77ef5f0bc73c07ff9c6a861c74ac0c6dff5c57',3,1,'user@ladorian.com','[]',0,'2020-09-17 12:11:47','2020-09-17 12:11:47','2021-09-17 12:11:47'),('fce7a4db08ee6caef6a6ed903eac5bbb5641bd805b8009e7cf70cb93e4ddfaa77fb30d0a7f4c5493',3,1,'user@ladorian.com','[]',0,'2020-09-17 05:55:29','2020-09-17 05:55:29','2021-09-17 05:55:29'),('fd70bb5890b4e028ed8a3e48662e8f4b03da63be979a4c7ce9683c6b726bee527b8af1922dcb0a75',3,1,'user@ladorian.com','[]',0,'2020-09-16 05:01:27','2020-09-16 05:01:27','2021-09-16 05:01:27'),('fde4456bc087effa236ee5df682544bfa1cdd56462049ba93abfd4533eb1f1040193f25d4b583e18',1,1,'superadmin@ladorian.com','[]',0,'2020-09-15 11:51:17','2020-09-15 11:51:17','2021-09-15 11:51:17'),('fe10fa39dce19b99d08566ba3794666e328dcbe73a601a66c28917cdbe093fda508c348a5a4d6d56',3,1,'user@ladorian.com','[]',0,'2020-09-17 04:03:14','2020-09-17 04:03:14','2021-09-17 04:03:14'),('fe95e3f3abffdfd6abb4535c02f907e7e81591ad5a64b35428bf43d5eda030c286f106b062ceec2e',3,1,'user@ladorian.com','[]',0,'2020-09-23 18:18:11','2020-09-23 18:18:11','2021-09-23 14:18:11'),('fed04847b9eabd0dd5d751c8be0f09301861520f5b5eb7d7d91de53e80591149b83c8b8833f5dd95',3,1,'user@ladorian.com','[]',0,'2020-09-24 20:30:18','2020-09-24 20:30:18','2021-09-24 16:30:18');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','NS8kP49URSTN12LSrwTfwrLSRqH1JcTjSz4vUiAm',NULL,'http://localhost',1,0,0,'2020-09-09 13:21:10','2020-09-09 13:21:10'),(2,NULL,'Laravel Password Grant Client','oqJraiEwD06wNID2ahoY72RX6yq3iHnJ46cj6oDF','users','http://localhost',0,1,0,'2020-09-09 13:21:10','2020-09-09 13:21:10');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-09-09 13:21:10','2020-09-09 13:21:10');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'*.*.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(2,'customer.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(3,'customer.put.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(4,'sites.get','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(5,'site.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(6,'site.put.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(7,'players.get','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(8,'player.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(9,'player.put.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(10,'tags.get','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(11,'tag.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(12,'cameras.get','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(13,'camera.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(14,'camera.put.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(15,'playareas.get','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(16,'playarea.get.*','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(17,'contentcategories.get.*','api','2020-09-23 21:46:21','2020-09-23 21:46:21'),(18,'contentcategory.get.*','api','2020-09-23 21:46:21','2020-09-23 21:46:21'),(19,'assets.get.*','api','2020-09-23 21:46:21','2020-09-23 21:46:21'),(20,'asset.get.*','api','2020-09-23 21:46:21','2020-09-23 21:46:21');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `play_areas`
--

DROP TABLE IF EXISTS `play_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `play_areas` (
  `idarea` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idlocation` bigint(20) unsigned NOT NULL,
  `idformat` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`idarea`),
  KEY `play_areas_idlocation_foreign` (`idlocation`),
  KEY `play_areas_idformat_foreign` (`idformat`),
  CONSTRAINT `play_areas_idformat_foreign` FOREIGN KEY (`idformat`) REFERENCES `formats` (`idformat`),
  CONSTRAINT `play_areas_idlocation_foreign` FOREIGN KEY (`idlocation`) REFERENCES `indoor_locations` (`idlocation`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `play_areas`
--

LOCK TABLES `play_areas` WRITE;
/*!40000 ALTER TABLE `play_areas` DISABLE KEYS */;
INSERT INTO `play_areas` VALUES (1,'ut',1,1,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(2,'distinctio',2,2,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(3,'ratione',3,3,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(4,'et',4,4,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(5,'necessitatibus',5,5,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(6,'praesentium',6,6,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(7,'totam',7,7,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(8,'vitae',8,8,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(9,'laudantium',9,9,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(10,'nihil',10,10,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(11,'velit',11,11,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(12,'accusamus',12,12,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(13,'optio',13,13,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(14,'vitae',14,14,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(15,'et',15,15,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(16,'deleniti',16,16,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(17,'atque',17,17,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(18,'dolorem',18,18,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(19,'aliquid',19,19,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(20,'unde',20,20,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(21,'officiis',21,21,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(22,'est',22,22,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(23,'et',23,23,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(24,'mollitia',24,24,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(25,'sit',25,25,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(26,'sed',26,26,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(27,'consequatur',27,27,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(28,'eum',28,28,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(29,'eos',29,29,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(30,'atque',30,30,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(31,'est',31,31,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(32,'consequuntur',32,32,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(33,'sequi',33,33,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(34,'harum',34,34,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(35,'Área 1',35,35,'2020-09-09 10:47:29','2020-09-09 10:47:29','Escaparate horizontal'),(36,'quo',36,36,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(37,'et',37,37,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(38,'doloremque',38,38,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(39,'quos',39,39,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(40,'ad',40,40,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(41,'exercitationem',41,41,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(42,'ducimus',42,42,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(43,'alias',43,43,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(44,'cupiditate',44,44,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(45,'iure',45,45,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(46,'et',46,46,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(47,'excepturi',47,47,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(48,'sunt',48,48,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(49,'et',49,49,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(50,'eum',50,50,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(51,'nihil',51,51,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(52,'et',52,52,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(53,'aut',53,53,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(54,'perferendis',54,54,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(55,'possimus',55,55,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(56,'qui',56,56,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(57,'dolorem',57,57,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(58,'vel',58,58,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(59,'quae',59,59,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(60,'vitae',60,60,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(61,'dolor',61,61,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(62,'est',62,62,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(63,'dolor',63,63,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(64,'corrupti',64,64,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(65,'at',65,65,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(66,'ea',66,66,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(67,'dolor',67,67,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(68,'nisi',68,68,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(69,'esse',69,69,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(70,'aut',70,70,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(71,'est',71,71,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(72,'excepturi',72,72,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(73,'quod',73,73,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(74,'et',74,74,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(75,'ipsa',75,75,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(76,'magni',76,76,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(77,'et',77,77,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(78,'porro',78,78,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(79,'ut',79,79,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(80,'ea',80,80,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(81,'in',81,81,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(82,'ea',82,82,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(83,'et',83,83,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(84,'provident',84,84,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(85,'est',85,85,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(86,'aperiam',86,86,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(87,'sit',87,87,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(88,'velit',88,88,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(89,'repudiandae',89,89,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(90,'voluptatibus',90,90,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(91,'asperiores',91,91,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(92,'esse',92,92,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(93,'velit',93,93,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(94,'quaerat',94,94,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(95,'consequatur',95,95,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(96,'quasi',96,96,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(97,'ratione',97,97,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(98,'adipisci',98,98,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(99,'ipsam',99,99,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(100,'sunt',100,100,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(101,'architecto',101,101,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(102,'suscipit',102,102,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(103,'earum',103,103,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(104,'expedita',104,104,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(105,'voluptas',105,105,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(106,'et',106,106,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(107,'rerum',107,107,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(108,'ab',108,108,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(109,'omnis',109,109,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(110,'voluptatem',110,110,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(111,'ea',111,111,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(112,'quis',112,112,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(113,'nisi',113,113,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(114,'suscipit',114,114,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(115,'qui',115,115,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(116,'qui',116,116,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(117,'explicabo',117,117,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(118,'sit',118,118,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(119,'illum',119,119,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(120,'et',120,120,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(121,'dicta',121,121,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(122,'sed',122,122,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(123,'minima',123,123,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(124,'repudiandae',124,124,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(125,'ut',125,125,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(126,'itaque',126,126,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(127,'perspiciatis',127,127,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(128,'et',128,128,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(129,'non',129,129,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(130,'reiciendis',130,130,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(131,'nulla',131,131,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(132,'perferendis',132,132,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(133,'architecto',133,133,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(134,'non',134,134,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(135,'aspernatur',135,135,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(136,'sapiente',136,136,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(137,'explicabo',137,137,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(138,'autem',138,138,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(139,'minus',139,139,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(140,'unde',140,140,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(141,'voluptatem',141,141,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(142,'dignissimos',142,142,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(143,'dolores',143,143,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(144,'qui',144,144,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(145,'omnis',145,145,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(146,'qui',146,146,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(147,'Área 2',147,147,'2020-09-09 10:47:29','2020-09-09 10:47:29','Cajas superior'),(148,'nisi',148,148,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(149,'temporibus',149,149,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(150,'hic',150,150,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(151,'dignissimos',151,151,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(152,'hic',152,152,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(153,'eum',153,153,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(154,'sit',154,154,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(155,'facere',155,155,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(156,'dolores',156,156,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(157,'dolores',157,157,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(158,'non',158,158,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(159,'ut',159,159,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(160,'aut',160,160,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(161,'qui',161,161,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(162,'nostrum',162,162,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(163,'facere',163,163,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(164,'repellat',164,164,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(165,'cupiditate',165,165,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(166,'et',166,166,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(167,'officia',167,167,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(168,'doloremque',168,168,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(169,'sint',169,169,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(170,'corrupti',170,170,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(171,'et',171,171,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(172,'veniam',172,172,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(173,'illo',173,173,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(174,'numquam',174,174,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(175,'consequatur',175,175,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(176,'impedit',176,176,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(177,'omnis',177,177,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(178,'aperiam',178,178,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(179,'iste',179,179,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(180,'non',180,180,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(181,'et',181,181,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(182,'et',182,182,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(183,'numquam',183,183,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(184,'iusto',184,184,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(185,'laboriosam',185,185,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(186,'ipsam',186,186,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(187,'minus',187,187,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(188,'consequuntur',188,188,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(189,'et',189,189,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(190,'velit',190,190,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(191,'omnis',191,191,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(192,'esse',192,192,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(193,'perspiciatis',193,193,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(194,'consequatur',194,194,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(195,'architecto',195,195,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(196,'doloribus',196,196,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(197,'excepturi',197,197,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(198,'fugiat',198,198,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(199,'nesciunt',199,199,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL),(200,'temporibus',200,200,'2020-09-09 10:47:29','2020-09-09 10:47:29',NULL);
/*!40000 ALTER TABLE `play_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `play_circuits`
--

DROP TABLE IF EXISTS `play_circuits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `play_circuits` (
  `idcircuit` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcircuit`),
  KEY `play_circuits_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `play_circuits_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `play_circuits`
--

LOCK TABLES `play_circuits` WRITE;
/*!40000 ALTER TABLE `play_circuits` DISABLE KEYS */;
/*!40000 ALTER TABLE `play_circuits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `play_logics`
--

DROP TABLE IF EXISTS `play_logics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `play_logics` (
  `idlogic` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `idarea` bigint(20) unsigned NOT NULL,
  `idtype` bigint(20) unsigned NOT NULL,
  `idcategory` bigint(20) unsigned NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idlogic`),
  KEY `play_logics_idcustomer_foreign` (`idcustomer`),
  KEY `play_logics_idarea_foreign` (`idarea`),
  KEY `play_logics_idtype_foreign` (`idtype`),
  KEY `play_logics_idcategory_foreign` (`idcategory`),
  CONSTRAINT `play_logics_idarea_foreign` FOREIGN KEY (`idarea`) REFERENCES `play_areas` (`idarea`),
  CONSTRAINT `play_logics_idcategory_foreign` FOREIGN KEY (`idcategory`) REFERENCES `content_categories` (`idcategory`),
  CONSTRAINT `play_logics_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`),
  CONSTRAINT `play_logics_idtype_foreign` FOREIGN KEY (`idtype`) REFERENCES `content_types` (`idtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `play_logics`
--

LOCK TABLES `play_logics` WRITE;
/*!40000 ALTER TABLE `play_logics` DISABLE KEYS */;
/*!40000 ALTER TABLE `play_logics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_powers`
--

DROP TABLE IF EXISTS `player_powers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_powers` (
  `idtime` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idplayer` bigint(20) unsigned NOT NULL,
  `weekday` int(11) NOT NULL,
  `time_on` time NOT NULL,
  `time_off` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idtime`),
  KEY `player_powers_idplayer_foreign` (`idplayer`),
  CONSTRAINT `player_powers_idplayer_foreign` FOREIGN KEY (`idplayer`) REFERENCES `players` (`idplayer`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_powers`
--

LOCK TABLES `player_powers` WRITE;
/*!40000 ALTER TABLE `player_powers` DISABLE KEYS */;
INSERT INTO `player_powers` VALUES (1,18,2,'02:59:26','09:54:04','2020-09-09 10:47:34','2020-09-09 10:47:34'),(2,26,2,'11:59:45','02:57:30','2020-09-09 10:47:34','2020-09-09 10:47:34'),(3,18,4,'21:55:57','12:03:08','2020-09-09 10:47:34','2020-09-09 10:47:34'),(4,19,1,'14:56:10','19:02:41','2020-09-09 10:47:35','2020-09-09 10:47:35'),(5,49,7,'10:57:33','20:21:43','2020-09-09 10:47:35','2020-09-09 10:47:35'),(6,18,7,'14:59:09','18:27:34','2020-09-09 10:47:35','2020-09-09 10:47:35'),(7,31,4,'14:15:24','10:40:37','2020-09-09 10:47:35','2020-09-09 10:47:35'),(8,34,4,'15:32:15','22:10:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(9,16,1,'08:36:55','21:59:18','2020-09-09 10:47:35','2020-09-09 10:47:35'),(10,28,1,'00:45:04','19:25:58','2020-09-09 10:47:35','2020-09-09 10:47:35'),(11,15,5,'07:19:51','03:54:07','2020-09-09 10:47:35','2020-09-09 10:47:35'),(12,39,6,'04:36:48','08:10:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(13,44,1,'13:33:56','04:29:16','2020-09-09 10:47:35','2020-09-09 10:47:35'),(14,5,6,'15:19:32','01:37:03','2020-09-09 10:47:35','2020-09-09 10:47:35'),(15,24,5,'22:04:19','03:40:19','2020-09-09 10:47:35','2020-09-09 10:47:35'),(16,41,7,'17:29:54','14:18:20','2020-09-09 10:47:35','2020-09-09 10:47:35'),(17,33,4,'00:36:33','00:15:02','2020-09-09 10:47:35','2020-09-09 10:47:35'),(18,33,3,'10:45:50','14:59:24','2020-09-09 10:47:35','2020-09-09 10:47:35'),(19,31,2,'19:25:58','09:45:18','2020-09-09 10:47:35','2020-09-09 10:47:35'),(20,42,4,'02:04:56','17:33:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(21,27,4,'15:38:47','21:59:20','2020-09-09 10:47:35','2020-09-09 10:47:35'),(22,33,2,'00:36:33','18:53:12','2020-09-09 10:47:35','2020-09-09 10:47:35'),(23,17,2,'16:52:43','10:00:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(24,26,4,'07:37:55','22:53:05','2020-09-09 10:47:35','2020-09-09 10:47:35'),(25,31,4,'18:53:13','01:37:35','2020-09-09 10:47:35','2020-09-09 10:47:35'),(26,32,5,'03:20:00','06:36:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(27,46,4,'11:43:14','02:18:06','2020-09-09 10:47:35','2020-09-09 10:47:35'),(28,27,5,'06:12:22','20:05:16','2020-09-09 10:47:35','2020-09-09 10:47:35'),(29,34,6,'00:36:24','11:33:13','2020-09-09 10:47:35','2020-09-09 10:47:35'),(30,14,1,'18:11:35','00:28:05','2020-09-09 10:47:35','2020-09-09 10:47:35'),(31,12,4,'09:11:19','13:03:06','2020-09-09 10:47:35','2020-09-09 10:47:35'),(32,3,6,'16:04:27','03:04:58','2020-09-09 10:47:35','2020-09-09 10:47:35'),(33,41,3,'05:39:32','21:52:51','2020-09-09 10:47:35','2020-09-09 10:47:35'),(34,40,6,'22:02:44','20:36:59','2020-09-09 10:47:35','2020-09-09 10:47:35'),(35,5,2,'06:52:57','17:59:21','2020-09-09 10:47:35','2020-09-09 10:47:35'),(36,22,1,'08:15:27','14:19:07','2020-09-09 10:47:35','2020-09-09 10:47:35'),(37,49,7,'07:46:28','23:33:47','2020-09-09 10:47:35','2020-09-09 10:47:35'),(38,34,5,'16:55:56','16:35:19','2020-09-09 10:47:35','2020-09-09 10:47:35'),(39,25,7,'10:41:53','08:34:29','2020-09-09 10:47:35','2020-09-09 10:47:35'),(40,25,2,'20:59:19','21:19:58','2020-09-09 10:47:35','2020-09-09 10:47:35'),(41,31,2,'17:19:50','18:06:13','2020-09-09 10:47:35','2020-09-09 10:47:35'),(42,20,5,'09:43:23','04:52:56','2020-09-09 10:47:35','2020-09-09 10:47:35'),(43,22,1,'00:43:00','10:11:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(44,49,5,'21:22:41','09:52:05','2020-09-09 10:47:35','2020-09-09 10:47:35'),(45,21,5,'18:56:47','14:53:15','2020-09-09 10:47:35','2020-09-09 10:47:35'),(46,49,3,'05:55:14','03:42:11','2020-09-09 10:47:35','2020-09-09 10:47:35'),(47,42,5,'04:59:24','18:40:25','2020-09-09 10:47:35','2020-09-09 10:47:35'),(48,1,2,'11:36:52','15:28:35','2020-09-09 10:47:35','2020-09-09 10:47:35'),(49,5,5,'06:57:15','14:03:29','2020-09-09 10:47:35','2020-09-09 10:47:35'),(50,25,6,'21:26:46','23:14:25','2020-09-09 10:47:35','2020-09-09 10:47:35'),(51,19,7,'08:37:47','01:31:00','2020-09-09 10:47:35','2020-09-09 10:47:35'),(52,44,6,'18:23:50','07:37:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(53,7,7,'16:02:33','03:24:42','2020-09-09 10:47:35','2020-09-09 10:47:35'),(54,5,2,'22:44:12','03:38:41','2020-09-09 10:47:35','2020-09-09 10:47:35'),(55,23,7,'14:49:51','09:58:50','2020-09-09 10:47:35','2020-09-09 10:47:35'),(56,47,2,'01:35:23','09:08:27','2020-09-09 10:47:35','2020-09-09 10:47:35'),(57,28,5,'02:17:24','11:15:26','2020-09-09 10:47:35','2020-09-09 10:47:35'),(58,37,4,'12:53:21','08:33:22','2020-09-09 10:47:35','2020-09-09 10:47:35'),(59,36,7,'05:37:38','01:29:08','2020-09-09 10:47:35','2020-09-09 10:47:35'),(60,3,3,'14:39:40','19:02:29','2020-09-09 10:47:35','2020-09-09 10:47:35'),(61,29,1,'09:55:07','10:33:31','2020-09-09 10:47:35','2020-09-09 10:47:35'),(62,12,1,'06:34:33','21:35:03','2020-09-09 10:47:35','2020-09-09 10:47:35'),(63,15,2,'21:04:12','09:44:40','2020-09-09 10:47:35','2020-09-09 10:47:35'),(64,7,1,'09:35:13','04:10:32','2020-09-09 10:47:35','2020-09-09 10:47:35'),(65,30,5,'16:40:10','23:34:47','2020-09-09 10:47:35','2020-09-09 10:47:35'),(66,45,5,'09:35:56','11:00:17','2020-09-09 10:47:35','2020-09-09 10:47:35'),(67,49,4,'20:41:20','20:09:43','2020-09-09 10:47:35','2020-09-09 10:47:35'),(68,48,1,'16:21:03','12:57:30','2020-09-09 10:47:35','2020-09-09 10:47:35'),(69,18,2,'20:42:42','03:37:04','2020-09-09 10:47:35','2020-09-09 10:47:35'),(70,38,5,'09:10:17','06:46:01','2020-09-09 10:47:35','2020-09-09 10:47:35'),(71,29,2,'14:14:03','07:13:43','2020-09-09 10:47:35','2020-09-09 10:47:35'),(72,33,2,'21:55:56','04:44:18','2020-09-09 10:47:35','2020-09-09 10:47:35'),(73,19,6,'00:52:17','02:54:56','2020-09-09 10:47:35','2020-09-09 10:47:35'),(74,32,4,'02:49:17','13:41:07','2020-09-09 10:47:35','2020-09-09 10:47:35'),(75,45,1,'14:08:51','10:59:31','2020-09-09 10:47:35','2020-09-09 10:47:35'),(76,40,6,'04:01:49','19:12:16','2020-09-09 10:47:35','2020-09-09 10:47:35'),(77,35,3,'04:40:28','07:52:27','2020-09-09 10:47:35','2020-09-09 10:47:35'),(78,10,2,'12:56:29','21:59:21','2020-09-09 10:47:35','2020-09-09 10:47:35'),(79,45,6,'01:32:05','09:53:00','2020-09-09 10:47:35','2020-09-09 10:47:35'),(80,36,4,'07:20:26','13:21:14','2020-09-09 10:47:35','2020-09-09 10:47:35'),(81,10,1,'20:43:11','19:55:57','2020-09-09 10:47:35','2020-09-09 10:47:35'),(82,36,6,'11:59:06','02:04:07','2020-09-09 10:47:35','2020-09-09 10:47:35'),(83,40,2,'07:12:59','06:01:39','2020-09-09 10:47:35','2020-09-09 10:47:35'),(84,31,1,'04:58:32','06:09:59','2020-09-09 10:47:35','2020-09-09 10:47:35'),(85,32,5,'18:49:52','04:17:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(86,38,6,'23:55:32','04:34:52','2020-09-09 10:47:35','2020-09-09 10:47:35'),(87,8,6,'15:57:54','20:04:24','2020-09-09 10:47:35','2020-09-09 10:47:35'),(88,43,3,'17:46:38','08:03:10','2020-09-09 10:47:35','2020-09-09 10:47:35'),(89,32,7,'03:15:13','11:43:34','2020-09-09 10:47:35','2020-09-09 10:47:35'),(90,10,1,'15:22:54','06:16:08','2020-09-09 10:47:35','2020-09-09 10:47:35'),(91,19,1,'06:24:46','00:18:45','2020-09-09 10:47:35','2020-09-09 10:47:35'),(92,45,6,'16:07:01','13:55:10','2020-09-09 10:47:35','2020-09-09 10:47:35'),(93,49,3,'22:12:29','05:31:30','2020-09-09 10:47:35','2020-09-09 10:47:35'),(94,28,2,'02:08:47','09:56:19','2020-09-09 10:47:35','2020-09-09 10:47:35'),(95,17,7,'14:13:31','21:34:57','2020-09-09 10:47:35','2020-09-09 10:47:35'),(96,50,6,'06:22:34','17:04:14','2020-09-09 10:47:35','2020-09-09 10:47:35'),(97,28,2,'16:06:42','11:35:47','2020-09-09 10:47:35','2020-09-09 10:47:35'),(98,18,3,'01:24:30','08:38:57','2020-09-09 10:47:35','2020-09-09 10:47:35'),(99,8,6,'15:04:43','02:28:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(100,19,7,'05:05:21','12:22:54','2020-09-09 10:47:35','2020-09-09 10:47:35'),(101,11,5,'19:00:18','04:54:31','2020-09-09 10:47:35','2020-09-09 10:47:35'),(102,14,2,'20:08:56','03:41:34','2020-09-09 10:47:35','2020-09-09 10:47:35'),(103,42,2,'03:29:29','12:39:50','2020-09-09 10:47:35','2020-09-09 10:47:35'),(104,18,3,'04:36:03','22:30:12','2020-09-09 10:47:35','2020-09-09 10:47:35'),(105,12,6,'17:13:35','20:26:51','2020-09-09 10:47:35','2020-09-09 10:47:35'),(106,10,7,'02:57:26','00:30:14','2020-09-09 10:47:35','2020-09-09 10:47:35'),(107,45,6,'03:24:56','21:25:44','2020-09-09 10:47:35','2020-09-09 10:47:35'),(108,31,1,'08:36:13','22:13:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(109,30,3,'12:23:32','18:35:27','2020-09-09 10:47:35','2020-09-09 10:47:35'),(110,34,7,'05:41:57','19:06:34','2020-09-09 10:47:35','2020-09-09 10:47:35'),(111,9,6,'11:19:27','14:13:47','2020-09-09 10:47:35','2020-09-09 10:47:35'),(112,16,1,'08:23:31','16:42:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(113,3,1,'18:35:33','05:09:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(114,26,6,'02:40:40','01:49:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(115,23,5,'04:00:08','20:33:12','2020-09-09 10:47:35','2020-09-09 10:47:35'),(116,3,2,'15:20:26','04:19:38','2020-09-09 10:47:35','2020-09-09 10:47:35'),(117,3,3,'11:20:00','13:08:04','2020-09-09 10:47:35','2020-09-09 10:47:35'),(118,18,6,'20:46:37','23:49:17','2020-09-09 10:47:35','2020-09-09 10:47:35'),(119,24,5,'00:18:15','12:27:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(120,13,6,'12:38:26','15:52:42','2020-09-09 10:47:35','2020-09-09 10:47:35'),(121,8,7,'21:54:21','06:41:35','2020-09-09 10:47:35','2020-09-09 10:47:35'),(122,21,1,'19:57:36','18:09:52','2020-09-09 10:47:35','2020-09-09 10:47:35'),(123,10,7,'07:10:20','23:23:57','2020-09-09 10:47:35','2020-09-09 10:47:35'),(124,41,6,'17:02:26','08:41:49','2020-09-09 10:47:35','2020-09-09 10:47:35'),(125,44,7,'04:20:02','00:57:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(126,42,3,'11:22:43','02:30:37','2020-09-09 10:47:35','2020-09-09 10:47:35'),(127,49,3,'02:37:03','06:03:59','2020-09-09 10:47:35','2020-09-09 10:47:35'),(128,44,4,'12:34:26','12:21:53','2020-09-09 10:47:35','2020-09-09 10:47:35'),(129,33,3,'21:05:08','16:21:01','2020-09-09 10:47:35','2020-09-09 10:47:35'),(130,48,3,'22:32:45','15:09:04','2020-09-09 10:47:35','2020-09-09 10:47:35'),(131,2,3,'00:48:48','04:55:50','2020-09-09 10:47:35','2020-09-09 10:47:35'),(132,23,2,'15:37:03','21:34:24','2020-09-09 10:47:35','2020-09-09 10:47:35'),(133,42,5,'13:56:46','07:55:54','2020-09-09 10:47:35','2020-09-09 10:47:35'),(134,31,7,'18:13:23','10:36:32','2020-09-09 10:47:35','2020-09-09 10:47:35'),(135,12,2,'12:59:53','02:20:29','2020-09-09 10:47:35','2020-09-09 10:47:35'),(136,10,6,'21:17:49','11:54:25','2020-09-09 10:47:35','2020-09-09 10:47:35'),(137,16,4,'16:09:10','20:58:17','2020-09-09 10:47:35','2020-09-09 10:47:35'),(138,40,7,'20:48:36','20:26:59','2020-09-09 10:47:35','2020-09-09 10:47:35'),(139,35,3,'21:50:18','17:14:00','2020-09-09 10:47:35','2020-09-09 10:47:35'),(140,5,2,'10:22:18','15:09:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(141,38,5,'18:52:46','06:28:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(142,5,5,'14:53:07','08:29:37','2020-09-09 10:47:35','2020-09-09 10:47:35'),(143,22,7,'13:30:50','19:41:03','2020-09-09 10:47:35','2020-09-09 10:47:35'),(144,43,2,'19:16:34','01:01:32','2020-09-09 10:47:35','2020-09-09 10:47:35'),(145,40,6,'21:58:49','21:46:56','2020-09-09 10:47:35','2020-09-09 10:47:35'),(146,2,2,'10:40:58','15:38:03','2020-09-09 10:47:35','2020-09-09 10:47:35'),(147,8,7,'16:54:13','13:28:46','2020-09-09 10:47:35','2020-09-09 10:47:35'),(148,20,3,'22:48:41','12:16:51','2020-09-09 10:47:35','2020-09-09 10:47:35'),(149,40,7,'03:06:01','02:45:56','2020-09-09 10:47:35','2020-09-09 10:47:35'),(150,10,7,'09:37:44','17:42:45','2020-09-09 10:47:35','2020-09-09 10:47:35'),(151,16,4,'16:11:51','09:03:44','2020-09-09 10:47:35','2020-09-09 10:47:35'),(152,20,3,'19:56:12','17:26:22','2020-09-09 10:47:35','2020-09-09 10:47:35'),(153,36,2,'20:19:07','09:20:29','2020-09-09 10:47:35','2020-09-09 10:47:35'),(154,24,3,'00:19:33','06:40:31','2020-09-09 10:47:35','2020-09-09 10:47:35'),(155,34,5,'03:50:28','13:31:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(156,28,3,'18:47:13','11:13:42','2020-09-09 10:47:35','2020-09-09 10:47:35'),(157,31,2,'15:52:51','16:46:04','2020-09-09 10:47:35','2020-09-09 10:47:35'),(158,48,7,'21:07:53','07:14:32','2020-09-09 10:47:35','2020-09-09 10:47:35'),(159,28,2,'20:39:13','04:35:53','2020-09-09 10:47:35','2020-09-09 10:47:35'),(160,9,1,'00:24:37','08:57:18','2020-09-09 10:47:35','2020-09-09 10:47:35'),(161,20,5,'03:02:11','23:45:22','2020-09-09 10:47:35','2020-09-09 10:47:35'),(162,17,1,'06:32:39','20:34:51','2020-09-09 10:47:35','2020-09-09 10:47:35'),(163,9,7,'14:56:35','17:42:32','2020-09-09 10:47:35','2020-09-09 10:47:35'),(164,37,4,'07:13:58','19:15:08','2020-09-09 10:47:35','2020-09-09 10:47:35'),(165,23,5,'06:48:07','04:34:05','2020-09-09 10:47:35','2020-09-09 10:47:35'),(166,25,4,'04:07:31','05:26:00','2020-09-09 10:47:35','2020-09-09 10:47:35'),(167,22,1,'07:01:59','14:41:20','2020-09-09 10:47:35','2020-09-09 10:47:35'),(168,50,4,'18:46:59','02:11:09','2020-09-09 10:47:35','2020-09-09 10:47:35'),(169,1,7,'05:53:21','07:27:17','2020-09-09 10:47:35','2020-09-09 10:47:35'),(170,47,6,'15:02:53','11:09:13','2020-09-09 10:47:35','2020-09-09 10:47:35'),(171,46,4,'18:28:19','20:29:35','2020-09-09 10:47:35','2020-09-09 10:47:35'),(172,1,7,'18:02:58','01:53:15','2020-09-09 10:47:35','2020-09-09 10:47:35'),(173,12,4,'13:39:15','13:20:26','2020-09-09 10:47:35','2020-09-09 10:47:35'),(174,41,2,'11:10:30','09:02:03','2020-09-09 10:47:35','2020-09-09 10:47:35'),(175,41,4,'15:49:49','15:23:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(176,16,1,'13:36:56','06:53:57','2020-09-09 10:47:35','2020-09-09 10:47:35'),(177,15,2,'08:04:04','02:50:54','2020-09-09 10:47:35','2020-09-09 10:47:35'),(178,47,1,'11:55:07','16:43:55','2020-09-09 10:47:35','2020-09-09 10:47:35'),(179,17,7,'13:38:08','03:38:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(180,48,1,'03:46:06','03:15:43','2020-09-09 10:47:35','2020-09-09 10:47:35'),(181,37,4,'19:47:52','17:12:27','2020-09-09 10:47:35','2020-09-09 10:47:35'),(182,30,6,'03:56:05','04:38:41','2020-09-09 10:47:35','2020-09-09 10:47:35'),(183,48,2,'07:35:29','10:17:48','2020-09-09 10:47:35','2020-09-09 10:47:35'),(184,9,7,'17:42:11','22:38:15','2020-09-09 10:47:35','2020-09-09 10:47:35'),(185,40,1,'20:42:54','05:57:08','2020-09-09 10:47:35','2020-09-09 10:47:35'),(186,49,4,'16:07:31','20:34:13','2020-09-09 10:47:35','2020-09-09 10:47:35'),(187,40,6,'12:57:32','20:19:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(188,45,5,'17:48:52','05:12:23','2020-09-09 10:47:35','2020-09-09 10:47:35'),(189,33,6,'23:17:25','00:17:05','2020-09-09 10:47:35','2020-09-09 10:47:35'),(190,24,6,'20:35:08','21:03:13','2020-09-09 10:47:35','2020-09-09 10:47:35'),(191,16,1,'23:54:12','06:09:14','2020-09-09 10:47:35','2020-09-09 10:47:35'),(192,7,4,'00:04:31','00:55:59','2020-09-09 10:47:35','2020-09-09 10:47:35'),(193,43,1,'11:43:43','20:47:04','2020-09-09 10:47:35','2020-09-09 10:47:35'),(194,28,7,'05:10:43','00:37:52','2020-09-09 10:47:35','2020-09-09 10:47:35'),(195,48,7,'03:44:06','22:19:00','2020-09-09 10:47:35','2020-09-09 10:47:35'),(196,42,7,'06:54:05','23:42:35','2020-09-09 10:47:35','2020-09-09 10:47:35'),(197,3,4,'16:37:16','17:07:39','2020-09-09 10:47:35','2020-09-09 10:47:35'),(198,3,2,'01:15:46','03:16:15','2020-09-09 10:47:35','2020-09-09 10:47:35'),(199,2,2,'00:31:19','14:57:36','2020-09-09 10:47:35','2020-09-09 10:47:35'),(200,22,4,'22:41:32','00:37:42','2020-09-09 10:47:35','2020-09-09 10:47:35');
/*!40000 ALTER TABLE `player_powers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_types`
--

DROP TABLE IF EXISTS `player_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_types` (
  `idtype` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_types`
--

LOCK TABLES `player_types` WRITE;
/*!40000 ALTER TABLE `player_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `players` (
  `idplayer` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idchannel` bigint(20) unsigned NOT NULL,
  `idarea` bigint(20) unsigned NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idlang` bigint(20) unsigned DEFAULT NULL,
  `idtype` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`idplayer`),
  KEY `players_idchannel_foreign` (`idchannel`),
  KEY `players_idarea_foreign` (`idarea`),
  KEY `players_idlang_foreign` (`idlang`),
  KEY `players_idtype_foreign` (`idtype`),
  CONSTRAINT `players_idarea_foreign` FOREIGN KEY (`idarea`) REFERENCES `play_areas` (`idarea`) ON DELETE CASCADE,
  CONSTRAINT `players_idchannel_foreign` FOREIGN KEY (`idchannel`) REFERENCES `channels` (`idchannel`) ON DELETE CASCADE,
  CONSTRAINT `players_idlang_foreign` FOREIGN KEY (`idlang`) REFERENCES `langs` (`idlang`),
  CONSTRAINT `players_idtype_foreign` FOREIGN KEY (`idtype`) REFERENCES `player_types` (`idtype`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,68,100,NULL,'carlos.rivero@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(2,129,60,NULL,'cristina.velez@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(3,65,61,NULL,'olga51@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(4,8,147,NULL,'navarro.victoria@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(5,76,31,NULL,'kalvarado@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(6,26,48,NULL,'ggranado@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(7,128,109,NULL,'ana.meza@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(8,97,38,NULL,'oscar.valdes@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(9,92,2,NULL,'alba82@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(10,121,166,NULL,'silvia.naranjo@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(11,38,92,NULL,'olucio@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(12,135,87,NULL,'valentina50@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(13,92,117,NULL,'garza.isaac@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(14,83,163,NULL,'emadera@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(15,126,165,NULL,'alicia73@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(16,99,160,NULL,'joel14@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(17,62,40,NULL,'csalgado@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(18,100,195,NULL,'zapata.valentina@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(19,55,193,NULL,'isaac.baeza@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(20,22,87,NULL,'santacruz.gabriel@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(21,46,166,NULL,'iker04@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(22,48,27,NULL,'nayara07@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(23,4,35,NULL,'valeria45@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(24,69,150,NULL,'casado.eva@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(25,48,150,NULL,'flopez@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(26,49,199,NULL,'lguerrero@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(27,24,168,NULL,'vcollado@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(28,100,100,NULL,'miguelangel.zaragoza@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(29,94,114,NULL,'aarce@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(30,132,180,NULL,'hernadez.josemanuel@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(31,42,102,NULL,'salma22@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(32,78,38,NULL,'malak.calero@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(33,70,153,NULL,'serra.lucas@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(34,79,61,NULL,'delapaz.isaac@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(35,60,154,NULL,'jaimes.zoe@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(36,103,170,NULL,'pedro.alcantar@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(37,105,185,NULL,'sluque@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(38,15,180,NULL,'aguilera.nadia@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(39,22,142,NULL,'barrientos.fatima@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(40,116,15,NULL,'saul46@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(41,93,104,NULL,'salmanza@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(42,103,182,NULL,'esther79@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(43,133,145,NULL,'hsanchez@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(44,99,147,NULL,'lucia.veliz@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(45,89,20,NULL,'grocha@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(46,125,179,NULL,'ucorral@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(47,26,6,NULL,'marco94@example.org',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(48,32,11,NULL,'gerard.canales@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(49,134,42,NULL,'rosales.miguel@example.com',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL),(50,74,136,NULL,'roman.sonia@example.net',1,0,'2020-09-09 10:47:30','2020-09-09 10:47:30',NULL,NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players_has_tags`
--

DROP TABLE IF EXISTS `players_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `players_has_tags` (
  `idplayer` bigint(20) unsigned NOT NULL,
  `idtag` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `player_tag_idplayer_foreign` (`idplayer`),
  KEY `player_tag_idtag_foreign` (`idtag`),
  CONSTRAINT `player_tag_idplayer_foreign` FOREIGN KEY (`idplayer`) REFERENCES `players` (`idplayer`) ON DELETE CASCADE,
  CONSTRAINT `player_tag_idtag_foreign` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players_has_tags`
--

LOCK TABLES `players_has_tags` WRITE;
/*!40000 ALTER TABLE `players_has_tags` DISABLE KEYS */;
INSERT INTO `players_has_tags` VALUES (1,54,NULL,NULL),(1,79,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,97,NULL,NULL),(1,8,NULL,NULL),(1,10,NULL,NULL),(1,30,NULL,NULL),(1,32,NULL,NULL),(1,3,NULL,NULL),(1,17,NULL,NULL),(1,61,NULL,NULL),(1,66,NULL,NULL),(2,54,NULL,NULL),(2,79,NULL,NULL),(2,85,NULL,NULL),(2,86,NULL,NULL),(2,97,NULL,NULL),(2,8,NULL,NULL),(2,10,NULL,NULL),(2,30,NULL,NULL),(2,32,NULL,NULL),(3,54,NULL,NULL),(3,79,NULL,NULL),(3,85,NULL,NULL),(3,86,NULL,NULL),(3,97,NULL,NULL),(3,8,NULL,NULL),(3,10,NULL,NULL),(3,30,NULL,NULL),(3,32,NULL,NULL),(4,8,NULL,NULL),(4,10,NULL,NULL),(4,12,NULL,NULL),(4,21,NULL,NULL),(4,25,NULL,NULL),(4,30,NULL,NULL),(4,32,NULL,NULL),(4,54,NULL,NULL),(4,59,NULL,NULL),(4,70,NULL,NULL),(4,79,NULL,NULL),(4,82,NULL,NULL),(4,85,NULL,NULL),(4,86,NULL,NULL),(4,97,NULL,NULL),(4,99,NULL,NULL),(5,24,NULL,NULL),(5,27,NULL,NULL),(5,40,NULL,NULL),(5,62,NULL,NULL),(5,65,NULL,NULL),(5,54,NULL,NULL),(5,79,NULL,NULL),(5,85,NULL,NULL),(5,86,NULL,NULL),(5,97,NULL,NULL),(5,8,NULL,NULL),(5,10,NULL,NULL),(5,30,NULL,NULL),(5,32,NULL,NULL),(6,54,NULL,NULL),(6,79,NULL,NULL),(6,85,NULL,NULL),(6,86,NULL,NULL),(6,97,NULL,NULL),(6,8,NULL,NULL),(6,10,NULL,NULL),(6,30,NULL,NULL),(6,32,NULL,NULL),(7,24,NULL,NULL),(7,27,NULL,NULL),(7,40,NULL,NULL),(7,62,NULL,NULL),(7,65,NULL,NULL),(7,54,NULL,NULL),(7,79,NULL,NULL),(7,85,NULL,NULL),(7,86,NULL,NULL),(7,97,NULL,NULL),(7,8,NULL,NULL),(7,10,NULL,NULL),(7,30,NULL,NULL),(7,32,NULL,NULL),(8,24,NULL,NULL),(8,27,NULL,NULL),(8,40,NULL,NULL),(8,62,NULL,NULL),(8,65,NULL,NULL),(8,54,NULL,NULL),(8,79,NULL,NULL),(8,85,NULL,NULL),(8,86,NULL,NULL),(8,97,NULL,NULL),(8,8,NULL,NULL),(8,10,NULL,NULL),(8,30,NULL,NULL),(8,32,NULL,NULL),(9,1,NULL,NULL),(9,4,NULL,NULL),(9,6,NULL,NULL),(9,8,NULL,NULL),(9,10,NULL,NULL),(9,16,NULL,NULL),(9,26,NULL,NULL),(9,28,NULL,NULL),(9,30,NULL,NULL),(9,32,NULL,NULL),(9,34,NULL,NULL),(9,35,NULL,NULL),(9,37,NULL,NULL),(9,42,NULL,NULL),(9,44,NULL,NULL),(9,46,NULL,NULL),(9,53,NULL,NULL),(9,54,NULL,NULL),(9,58,NULL,NULL),(9,63,NULL,NULL),(9,68,NULL,NULL),(9,73,NULL,NULL),(9,77,NULL,NULL),(9,78,NULL,NULL),(9,79,NULL,NULL),(9,84,NULL,NULL),(9,85,NULL,NULL),(9,86,NULL,NULL),(9,94,NULL,NULL),(9,97,NULL,NULL),(9,100,NULL,NULL),(10,1,NULL,NULL),(10,4,NULL,NULL),(10,6,NULL,NULL),(10,8,NULL,NULL),(10,10,NULL,NULL),(10,16,NULL,NULL),(10,26,NULL,NULL),(10,28,NULL,NULL),(10,30,NULL,NULL),(10,32,NULL,NULL),(10,34,NULL,NULL),(10,35,NULL,NULL),(10,37,NULL,NULL),(10,42,NULL,NULL),(10,44,NULL,NULL),(10,46,NULL,NULL),(10,53,NULL,NULL),(10,54,NULL,NULL),(10,58,NULL,NULL),(10,63,NULL,NULL),(10,68,NULL,NULL),(10,73,NULL,NULL),(10,77,NULL,NULL),(10,78,NULL,NULL),(10,79,NULL,NULL),(10,84,NULL,NULL),(10,85,NULL,NULL),(10,86,NULL,NULL),(10,94,NULL,NULL),(10,97,NULL,NULL),(10,100,NULL,NULL),(11,54,NULL,NULL),(11,79,NULL,NULL),(11,85,NULL,NULL),(11,86,NULL,NULL),(11,97,NULL,NULL),(11,8,NULL,NULL),(11,10,NULL,NULL),(11,30,NULL,NULL),(11,32,NULL,NULL),(11,47,NULL,NULL),(11,60,NULL,NULL),(11,81,NULL,NULL),(12,5,NULL,NULL),(12,8,NULL,NULL),(12,10,NULL,NULL),(12,22,NULL,NULL),(12,29,NULL,NULL),(12,30,NULL,NULL),(12,31,NULL,NULL),(12,32,NULL,NULL),(12,38,NULL,NULL),(12,39,NULL,NULL),(12,41,NULL,NULL),(12,51,NULL,NULL),(12,54,NULL,NULL),(12,57,NULL,NULL),(12,71,NULL,NULL),(12,72,NULL,NULL),(12,75,NULL,NULL),(12,76,NULL,NULL),(12,79,NULL,NULL),(12,85,NULL,NULL),(12,86,NULL,NULL),(12,91,NULL,NULL),(12,95,NULL,NULL),(12,97,NULL,NULL),(13,2,NULL,NULL),(13,8,NULL,NULL),(13,9,NULL,NULL),(13,10,NULL,NULL),(13,11,NULL,NULL),(13,19,NULL,NULL),(13,30,NULL,NULL),(13,32,NULL,NULL),(13,45,NULL,NULL),(13,50,NULL,NULL),(13,54,NULL,NULL),(13,56,NULL,NULL),(13,79,NULL,NULL),(13,85,NULL,NULL),(13,86,NULL,NULL),(13,89,NULL,NULL),(13,93,NULL,NULL),(13,96,NULL,NULL),(13,97,NULL,NULL),(13,98,NULL,NULL),(14,8,NULL,NULL),(14,10,NULL,NULL),(14,13,NULL,NULL),(14,14,NULL,NULL),(14,20,NULL,NULL),(14,30,NULL,NULL),(14,32,NULL,NULL),(14,43,NULL,NULL),(14,48,NULL,NULL),(14,54,NULL,NULL),(14,67,NULL,NULL),(14,79,NULL,NULL),(14,83,NULL,NULL),(14,85,NULL,NULL),(14,86,NULL,NULL),(14,88,NULL,NULL),(14,97,NULL,NULL),(15,1,NULL,NULL),(15,4,NULL,NULL),(15,6,NULL,NULL),(15,8,NULL,NULL),(15,10,NULL,NULL),(15,16,NULL,NULL),(15,26,NULL,NULL),(15,28,NULL,NULL),(15,30,NULL,NULL),(15,32,NULL,NULL),(15,34,NULL,NULL),(15,35,NULL,NULL),(15,37,NULL,NULL),(15,42,NULL,NULL),(15,44,NULL,NULL),(15,46,NULL,NULL),(15,53,NULL,NULL),(15,54,NULL,NULL),(15,58,NULL,NULL),(15,63,NULL,NULL),(15,68,NULL,NULL),(15,73,NULL,NULL),(15,77,NULL,NULL),(15,78,NULL,NULL),(15,79,NULL,NULL),(15,84,NULL,NULL),(15,85,NULL,NULL),(15,86,NULL,NULL),(15,94,NULL,NULL),(15,97,NULL,NULL),(15,100,NULL,NULL),(16,1,NULL,NULL),(16,4,NULL,NULL),(16,6,NULL,NULL),(16,8,NULL,NULL),(16,10,NULL,NULL),(16,16,NULL,NULL),(16,26,NULL,NULL),(16,28,NULL,NULL),(16,30,NULL,NULL),(16,32,NULL,NULL),(16,34,NULL,NULL),(16,35,NULL,NULL),(16,37,NULL,NULL),(16,42,NULL,NULL),(16,44,NULL,NULL),(16,46,NULL,NULL),(16,53,NULL,NULL),(16,54,NULL,NULL),(16,58,NULL,NULL),(16,63,NULL,NULL),(16,68,NULL,NULL),(16,73,NULL,NULL),(16,77,NULL,NULL),(16,78,NULL,NULL),(16,79,NULL,NULL),(16,84,NULL,NULL),(16,85,NULL,NULL),(16,86,NULL,NULL),(16,94,NULL,NULL),(16,97,NULL,NULL),(16,100,NULL,NULL),(17,8,NULL,NULL),(17,10,NULL,NULL),(17,13,NULL,NULL),(17,14,NULL,NULL),(17,20,NULL,NULL),(17,30,NULL,NULL),(17,32,NULL,NULL),(17,43,NULL,NULL),(17,48,NULL,NULL),(17,54,NULL,NULL),(17,67,NULL,NULL),(17,79,NULL,NULL),(17,83,NULL,NULL),(17,85,NULL,NULL),(17,86,NULL,NULL),(17,88,NULL,NULL),(17,97,NULL,NULL),(18,54,NULL,NULL),(18,79,NULL,NULL),(18,85,NULL,NULL),(18,86,NULL,NULL),(18,97,NULL,NULL),(18,8,NULL,NULL),(18,10,NULL,NULL),(18,30,NULL,NULL),(18,32,NULL,NULL),(19,8,NULL,NULL),(19,10,NULL,NULL),(19,13,NULL,NULL),(19,14,NULL,NULL),(19,20,NULL,NULL),(19,30,NULL,NULL),(19,32,NULL,NULL),(19,43,NULL,NULL),(19,48,NULL,NULL),(19,54,NULL,NULL),(19,67,NULL,NULL),(19,79,NULL,NULL),(19,83,NULL,NULL),(19,85,NULL,NULL),(19,86,NULL,NULL),(19,88,NULL,NULL),(19,97,NULL,NULL),(20,1,NULL,NULL),(20,4,NULL,NULL),(20,6,NULL,NULL),(20,8,NULL,NULL),(20,10,NULL,NULL),(20,16,NULL,NULL),(20,26,NULL,NULL),(20,28,NULL,NULL),(20,30,NULL,NULL),(20,32,NULL,NULL),(20,34,NULL,NULL),(20,35,NULL,NULL),(20,37,NULL,NULL),(20,42,NULL,NULL),(20,44,NULL,NULL),(20,46,NULL,NULL),(20,53,NULL,NULL),(20,54,NULL,NULL),(20,58,NULL,NULL),(20,63,NULL,NULL),(20,68,NULL,NULL),(20,73,NULL,NULL),(20,77,NULL,NULL),(20,78,NULL,NULL),(20,79,NULL,NULL),(20,84,NULL,NULL),(20,85,NULL,NULL),(20,86,NULL,NULL),(20,94,NULL,NULL),(20,97,NULL,NULL),(20,100,NULL,NULL),(21,54,NULL,NULL),(21,79,NULL,NULL),(21,85,NULL,NULL),(21,86,NULL,NULL),(21,97,NULL,NULL),(21,8,NULL,NULL),(21,10,NULL,NULL),(21,30,NULL,NULL),(21,32,NULL,NULL),(22,5,NULL,NULL),(22,8,NULL,NULL),(22,10,NULL,NULL),(22,22,NULL,NULL),(22,29,NULL,NULL),(22,30,NULL,NULL),(22,31,NULL,NULL),(22,32,NULL,NULL),(22,38,NULL,NULL),(22,39,NULL,NULL),(22,41,NULL,NULL),(22,51,NULL,NULL),(22,54,NULL,NULL),(22,57,NULL,NULL),(22,71,NULL,NULL),(22,72,NULL,NULL),(22,75,NULL,NULL),(22,76,NULL,NULL),(22,79,NULL,NULL),(22,85,NULL,NULL),(22,86,NULL,NULL),(22,91,NULL,NULL),(22,95,NULL,NULL),(22,97,NULL,NULL),(23,1,NULL,NULL),(23,4,NULL,NULL),(23,6,NULL,NULL),(23,8,NULL,NULL),(23,10,NULL,NULL),(23,16,NULL,NULL),(23,26,NULL,NULL),(23,28,NULL,NULL),(23,30,NULL,NULL),(23,32,NULL,NULL),(23,34,NULL,NULL),(23,35,NULL,NULL),(23,37,NULL,NULL),(23,42,NULL,NULL),(23,44,NULL,NULL),(23,46,NULL,NULL),(23,53,NULL,NULL),(23,54,NULL,NULL),(23,58,NULL,NULL),(23,63,NULL,NULL),(23,68,NULL,NULL),(23,73,NULL,NULL),(23,77,NULL,NULL),(23,78,NULL,NULL),(23,79,NULL,NULL),(23,84,NULL,NULL),(23,85,NULL,NULL),(23,86,NULL,NULL),(23,94,NULL,NULL),(23,97,NULL,NULL),(23,100,NULL,NULL),(24,5,NULL,NULL),(24,8,NULL,NULL),(24,10,NULL,NULL),(24,22,NULL,NULL),(24,29,NULL,NULL),(24,30,NULL,NULL),(24,31,NULL,NULL),(24,32,NULL,NULL),(24,38,NULL,NULL),(24,39,NULL,NULL),(24,41,NULL,NULL),(24,51,NULL,NULL),(24,54,NULL,NULL),(24,57,NULL,NULL),(24,71,NULL,NULL),(24,72,NULL,NULL),(24,75,NULL,NULL),(24,76,NULL,NULL),(24,79,NULL,NULL),(24,85,NULL,NULL),(24,86,NULL,NULL),(24,91,NULL,NULL),(24,95,NULL,NULL),(24,97,NULL,NULL),(25,2,NULL,NULL),(25,8,NULL,NULL),(25,9,NULL,NULL),(25,10,NULL,NULL),(25,11,NULL,NULL),(25,19,NULL,NULL),(25,30,NULL,NULL),(25,32,NULL,NULL),(25,45,NULL,NULL),(25,50,NULL,NULL),(25,54,NULL,NULL),(25,56,NULL,NULL),(25,79,NULL,NULL),(25,85,NULL,NULL),(25,86,NULL,NULL),(25,89,NULL,NULL),(25,93,NULL,NULL),(25,96,NULL,NULL),(25,97,NULL,NULL),(25,98,NULL,NULL),(26,8,NULL,NULL),(26,10,NULL,NULL),(26,12,NULL,NULL),(26,21,NULL,NULL),(26,25,NULL,NULL),(26,30,NULL,NULL),(26,32,NULL,NULL),(26,54,NULL,NULL),(26,59,NULL,NULL),(26,70,NULL,NULL),(26,79,NULL,NULL),(26,82,NULL,NULL),(26,85,NULL,NULL),(26,86,NULL,NULL),(26,97,NULL,NULL),(26,99,NULL,NULL),(27,24,NULL,NULL),(27,27,NULL,NULL),(27,40,NULL,NULL),(27,62,NULL,NULL),(27,65,NULL,NULL),(27,54,NULL,NULL),(27,79,NULL,NULL),(27,85,NULL,NULL),(27,86,NULL,NULL),(27,97,NULL,NULL),(27,8,NULL,NULL),(27,10,NULL,NULL),(27,30,NULL,NULL),(27,32,NULL,NULL),(28,1,NULL,NULL),(28,4,NULL,NULL),(28,6,NULL,NULL),(28,8,NULL,NULL),(28,10,NULL,NULL),(28,16,NULL,NULL),(28,26,NULL,NULL),(28,28,NULL,NULL),(28,30,NULL,NULL),(28,32,NULL,NULL),(28,34,NULL,NULL),(28,35,NULL,NULL),(28,37,NULL,NULL),(28,42,NULL,NULL),(28,44,NULL,NULL),(28,46,NULL,NULL),(28,53,NULL,NULL),(28,54,NULL,NULL),(28,58,NULL,NULL),(28,63,NULL,NULL),(28,68,NULL,NULL),(28,73,NULL,NULL),(28,77,NULL,NULL),(28,78,NULL,NULL),(28,79,NULL,NULL),(28,84,NULL,NULL),(28,85,NULL,NULL),(28,86,NULL,NULL),(28,94,NULL,NULL),(28,97,NULL,NULL),(28,100,NULL,NULL),(29,8,NULL,NULL),(29,10,NULL,NULL),(29,13,NULL,NULL),(29,14,NULL,NULL),(29,20,NULL,NULL),(29,30,NULL,NULL),(29,32,NULL,NULL),(29,43,NULL,NULL),(29,48,NULL,NULL),(29,54,NULL,NULL),(29,67,NULL,NULL),(29,79,NULL,NULL),(29,83,NULL,NULL),(29,85,NULL,NULL),(29,86,NULL,NULL),(29,88,NULL,NULL),(29,97,NULL,NULL),(30,54,NULL,NULL),(30,79,NULL,NULL),(30,85,NULL,NULL),(30,86,NULL,NULL),(30,97,NULL,NULL),(30,8,NULL,NULL),(30,10,NULL,NULL),(30,30,NULL,NULL),(30,32,NULL,NULL),(30,47,NULL,NULL),(30,60,NULL,NULL),(30,81,NULL,NULL),(31,8,NULL,NULL),(31,10,NULL,NULL),(31,13,NULL,NULL),(31,14,NULL,NULL),(31,20,NULL,NULL),(31,30,NULL,NULL),(31,32,NULL,NULL),(31,43,NULL,NULL),(31,48,NULL,NULL),(31,54,NULL,NULL),(31,67,NULL,NULL),(31,79,NULL,NULL),(31,83,NULL,NULL),(31,85,NULL,NULL),(31,86,NULL,NULL),(31,88,NULL,NULL),(31,97,NULL,NULL),(32,54,NULL,NULL),(32,79,NULL,NULL),(32,85,NULL,NULL),(32,86,NULL,NULL),(32,97,NULL,NULL),(32,8,NULL,NULL),(32,10,NULL,NULL),(32,30,NULL,NULL),(32,32,NULL,NULL),(33,7,NULL,NULL),(33,8,NULL,NULL),(33,10,NULL,NULL),(33,15,NULL,NULL),(33,18,NULL,NULL),(33,23,NULL,NULL),(33,30,NULL,NULL),(33,32,NULL,NULL),(33,33,NULL,NULL),(33,36,NULL,NULL),(33,49,NULL,NULL),(33,52,NULL,NULL),(33,54,NULL,NULL),(33,55,NULL,NULL),(33,64,NULL,NULL),(33,69,NULL,NULL),(33,74,NULL,NULL),(33,79,NULL,NULL),(33,80,NULL,NULL),(33,85,NULL,NULL),(33,86,NULL,NULL),(33,87,NULL,NULL),(33,90,NULL,NULL),(33,92,NULL,NULL),(33,97,NULL,NULL),(34,1,NULL,NULL),(34,4,NULL,NULL),(34,6,NULL,NULL),(34,8,NULL,NULL),(34,10,NULL,NULL),(34,16,NULL,NULL),(34,26,NULL,NULL),(34,28,NULL,NULL),(34,30,NULL,NULL),(34,32,NULL,NULL),(34,34,NULL,NULL),(34,35,NULL,NULL),(34,37,NULL,NULL),(34,42,NULL,NULL),(34,44,NULL,NULL),(34,46,NULL,NULL),(34,53,NULL,NULL),(34,54,NULL,NULL),(34,58,NULL,NULL),(34,63,NULL,NULL),(34,68,NULL,NULL),(34,73,NULL,NULL),(34,77,NULL,NULL),(34,78,NULL,NULL),(34,79,NULL,NULL),(34,84,NULL,NULL),(34,85,NULL,NULL),(34,86,NULL,NULL),(34,94,NULL,NULL),(34,97,NULL,NULL),(34,100,NULL,NULL),(35,8,NULL,NULL),(35,10,NULL,NULL),(35,12,NULL,NULL),(35,21,NULL,NULL),(35,25,NULL,NULL),(35,30,NULL,NULL),(35,32,NULL,NULL),(35,54,NULL,NULL),(35,59,NULL,NULL),(35,70,NULL,NULL),(35,79,NULL,NULL),(35,82,NULL,NULL),(35,85,NULL,NULL),(35,86,NULL,NULL),(35,97,NULL,NULL),(35,99,NULL,NULL),(36,7,NULL,NULL),(36,8,NULL,NULL),(36,10,NULL,NULL),(36,15,NULL,NULL),(36,18,NULL,NULL),(36,23,NULL,NULL),(36,30,NULL,NULL),(36,32,NULL,NULL),(36,33,NULL,NULL),(36,36,NULL,NULL),(36,49,NULL,NULL),(36,52,NULL,NULL),(36,54,NULL,NULL),(36,55,NULL,NULL),(36,64,NULL,NULL),(36,69,NULL,NULL),(36,74,NULL,NULL),(36,79,NULL,NULL),(36,80,NULL,NULL),(36,85,NULL,NULL),(36,86,NULL,NULL),(36,87,NULL,NULL),(36,90,NULL,NULL),(36,92,NULL,NULL),(36,97,NULL,NULL),(37,54,NULL,NULL),(37,79,NULL,NULL),(37,85,NULL,NULL),(37,86,NULL,NULL),(37,97,NULL,NULL),(37,8,NULL,NULL),(37,10,NULL,NULL),(37,30,NULL,NULL),(37,32,NULL,NULL),(37,47,NULL,NULL),(37,60,NULL,NULL),(37,81,NULL,NULL),(38,8,NULL,NULL),(38,10,NULL,NULL),(38,13,NULL,NULL),(38,14,NULL,NULL),(38,20,NULL,NULL),(38,30,NULL,NULL),(38,32,NULL,NULL),(38,43,NULL,NULL),(38,48,NULL,NULL),(38,54,NULL,NULL),(38,67,NULL,NULL),(38,79,NULL,NULL),(38,83,NULL,NULL),(38,85,NULL,NULL),(38,86,NULL,NULL),(38,88,NULL,NULL),(38,97,NULL,NULL),(39,1,NULL,NULL),(39,4,NULL,NULL),(39,6,NULL,NULL),(39,8,NULL,NULL),(39,10,NULL,NULL),(39,16,NULL,NULL),(39,26,NULL,NULL),(39,28,NULL,NULL),(39,30,NULL,NULL),(39,32,NULL,NULL),(39,34,NULL,NULL),(39,35,NULL,NULL),(39,37,NULL,NULL),(39,42,NULL,NULL),(39,44,NULL,NULL),(39,46,NULL,NULL),(39,53,NULL,NULL),(39,54,NULL,NULL),(39,58,NULL,NULL),(39,63,NULL,NULL),(39,68,NULL,NULL),(39,73,NULL,NULL),(39,77,NULL,NULL),(39,78,NULL,NULL),(39,79,NULL,NULL),(39,84,NULL,NULL),(39,85,NULL,NULL),(39,86,NULL,NULL),(39,94,NULL,NULL),(39,97,NULL,NULL),(39,100,NULL,NULL),(40,54,NULL,NULL),(40,79,NULL,NULL),(40,85,NULL,NULL),(40,86,NULL,NULL),(40,97,NULL,NULL),(40,8,NULL,NULL),(40,10,NULL,NULL),(40,30,NULL,NULL),(40,32,NULL,NULL),(40,3,NULL,NULL),(40,17,NULL,NULL),(40,61,NULL,NULL),(40,66,NULL,NULL),(41,1,NULL,NULL),(41,4,NULL,NULL),(41,6,NULL,NULL),(41,8,NULL,NULL),(41,10,NULL,NULL),(41,16,NULL,NULL),(41,26,NULL,NULL),(41,28,NULL,NULL),(41,30,NULL,NULL),(41,32,NULL,NULL),(41,34,NULL,NULL),(41,35,NULL,NULL),(41,37,NULL,NULL),(41,42,NULL,NULL),(41,44,NULL,NULL),(41,46,NULL,NULL),(41,53,NULL,NULL),(41,54,NULL,NULL),(41,58,NULL,NULL),(41,63,NULL,NULL),(41,68,NULL,NULL),(41,73,NULL,NULL),(41,77,NULL,NULL),(41,78,NULL,NULL),(41,79,NULL,NULL),(41,84,NULL,NULL),(41,85,NULL,NULL),(41,86,NULL,NULL),(41,94,NULL,NULL),(41,97,NULL,NULL),(41,100,NULL,NULL),(42,1,NULL,NULL),(42,4,NULL,NULL),(42,6,NULL,NULL),(42,8,NULL,NULL),(42,10,NULL,NULL),(42,16,NULL,NULL),(42,26,NULL,NULL),(42,28,NULL,NULL),(42,30,NULL,NULL),(42,32,NULL,NULL),(42,34,NULL,NULL),(42,35,NULL,NULL),(42,37,NULL,NULL),(42,42,NULL,NULL),(42,44,NULL,NULL),(42,46,NULL,NULL),(42,53,NULL,NULL),(42,54,NULL,NULL),(42,58,NULL,NULL),(42,63,NULL,NULL),(42,68,NULL,NULL),(42,73,NULL,NULL),(42,77,NULL,NULL),(42,78,NULL,NULL),(42,79,NULL,NULL),(42,84,NULL,NULL),(42,85,NULL,NULL),(42,86,NULL,NULL),(42,94,NULL,NULL),(42,97,NULL,NULL),(42,100,NULL,NULL),(43,24,NULL,NULL),(43,27,NULL,NULL),(43,40,NULL,NULL),(43,62,NULL,NULL),(43,65,NULL,NULL),(43,54,NULL,NULL),(43,79,NULL,NULL),(43,85,NULL,NULL),(43,86,NULL,NULL),(43,97,NULL,NULL),(43,8,NULL,NULL),(43,10,NULL,NULL),(43,30,NULL,NULL),(43,32,NULL,NULL),(44,1,NULL,NULL),(44,4,NULL,NULL),(44,6,NULL,NULL),(44,8,NULL,NULL),(44,10,NULL,NULL),(44,16,NULL,NULL),(44,26,NULL,NULL),(44,28,NULL,NULL),(44,30,NULL,NULL),(44,32,NULL,NULL),(44,34,NULL,NULL),(44,35,NULL,NULL),(44,37,NULL,NULL),(44,42,NULL,NULL),(44,44,NULL,NULL),(44,46,NULL,NULL),(44,53,NULL,NULL),(44,54,NULL,NULL),(44,58,NULL,NULL),(44,63,NULL,NULL),(44,68,NULL,NULL),(44,73,NULL,NULL),(44,77,NULL,NULL),(44,78,NULL,NULL),(44,79,NULL,NULL),(44,84,NULL,NULL),(44,85,NULL,NULL),(44,86,NULL,NULL),(44,94,NULL,NULL),(44,97,NULL,NULL),(44,100,NULL,NULL),(45,1,NULL,NULL),(45,4,NULL,NULL),(45,6,NULL,NULL),(45,8,NULL,NULL),(45,10,NULL,NULL),(45,16,NULL,NULL),(45,26,NULL,NULL),(45,28,NULL,NULL),(45,30,NULL,NULL),(45,32,NULL,NULL),(45,34,NULL,NULL),(45,35,NULL,NULL),(45,37,NULL,NULL),(45,42,NULL,NULL),(45,44,NULL,NULL),(45,46,NULL,NULL),(45,53,NULL,NULL),(45,54,NULL,NULL),(45,58,NULL,NULL),(45,63,NULL,NULL),(45,68,NULL,NULL),(45,73,NULL,NULL),(45,77,NULL,NULL),(45,78,NULL,NULL),(45,79,NULL,NULL),(45,84,NULL,NULL),(45,85,NULL,NULL),(45,86,NULL,NULL),(45,94,NULL,NULL),(45,97,NULL,NULL),(45,100,NULL,NULL),(46,24,NULL,NULL),(46,27,NULL,NULL),(46,40,NULL,NULL),(46,62,NULL,NULL),(46,65,NULL,NULL),(46,54,NULL,NULL),(46,79,NULL,NULL),(46,85,NULL,NULL),(46,86,NULL,NULL),(46,97,NULL,NULL),(46,8,NULL,NULL),(46,10,NULL,NULL),(46,30,NULL,NULL),(46,32,NULL,NULL),(47,8,NULL,NULL),(47,10,NULL,NULL),(47,12,NULL,NULL),(47,21,NULL,NULL),(47,25,NULL,NULL),(47,30,NULL,NULL),(47,32,NULL,NULL),(47,54,NULL,NULL),(47,59,NULL,NULL),(47,70,NULL,NULL),(47,79,NULL,NULL),(47,82,NULL,NULL),(47,85,NULL,NULL),(47,86,NULL,NULL),(47,97,NULL,NULL),(47,99,NULL,NULL),(48,7,NULL,NULL),(48,8,NULL,NULL),(48,10,NULL,NULL),(48,15,NULL,NULL),(48,18,NULL,NULL),(48,23,NULL,NULL),(48,30,NULL,NULL),(48,32,NULL,NULL),(48,33,NULL,NULL),(48,36,NULL,NULL),(48,49,NULL,NULL),(48,52,NULL,NULL),(48,54,NULL,NULL),(48,55,NULL,NULL),(48,64,NULL,NULL),(48,69,NULL,NULL),(48,74,NULL,NULL),(48,79,NULL,NULL),(48,80,NULL,NULL),(48,85,NULL,NULL),(48,86,NULL,NULL),(48,87,NULL,NULL),(48,90,NULL,NULL),(48,92,NULL,NULL),(48,97,NULL,NULL),(49,8,NULL,NULL),(49,10,NULL,NULL),(49,13,NULL,NULL),(49,14,NULL,NULL),(49,20,NULL,NULL),(49,30,NULL,NULL),(49,32,NULL,NULL),(49,43,NULL,NULL),(49,48,NULL,NULL),(49,54,NULL,NULL),(49,67,NULL,NULL),(49,79,NULL,NULL),(49,83,NULL,NULL),(49,85,NULL,NULL),(49,86,NULL,NULL),(49,88,NULL,NULL),(49,97,NULL,NULL),(50,8,NULL,NULL),(50,10,NULL,NULL),(50,13,NULL,NULL),(50,14,NULL,NULL),(50,20,NULL,NULL),(50,30,NULL,NULL),(50,32,NULL,NULL),(50,43,NULL,NULL),(50,48,NULL,NULL),(50,54,NULL,NULL),(50,67,NULL,NULL),(50,79,NULL,NULL),(50,83,NULL,NULL),(50,85,NULL,NULL),(50,86,NULL,NULL),(50,88,NULL,NULL),(50,97,NULL,NULL);
/*!40000 ALTER TABLE `players_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `provinces` (
  `idprovince` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcountry` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idprovince`),
  KEY `provinces_idcountry_foreign` (`idcountry`),
  CONSTRAINT `provinces_idcountry_foreign` FOREIGN KEY (`idcountry`) REFERENCES `countries` (`idcountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2),(19,2),(20,2),(2,3),(4,3),(5,3),(6,3),(7,3),(8,3),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(17,3),(18,3),(19,3),(20,3);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'superadmin','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(2,'admin','api','2020-09-09 10:46:21','2020-09-09 10:46:21'),(3,'user','api','2020-09-09 10:46:21','2020-09-09 10:46:21');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_holidays`
--

DROP TABLE IF EXISTS `site_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_holidays` (
  `idholiday` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idsite` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idholiday`),
  KEY `site_holidays_idsite_foreign` (`idsite`),
  CONSTRAINT `site_holidays_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_holidays`
--

LOCK TABLES `site_holidays` WRITE;
/*!40000 ALTER TABLE `site_holidays` DISABLE KEYS */;
INSERT INTO `site_holidays` VALUES (1,7,'2020-04-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(2,38,'2020-08-31','2020-09-09 10:46:23','2020-09-09 10:46:23'),(3,40,'2020-09-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(4,32,'2020-01-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(5,31,'2020-01-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(6,12,'2019-10-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(7,17,'2020-03-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(8,18,'2020-07-04','2020-09-09 10:46:23','2020-09-09 10:46:23'),(9,24,'2020-02-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(10,24,'2019-09-25','2020-09-09 10:46:23','2020-09-09 10:46:23'),(11,44,'2020-02-18','2020-09-09 10:46:23','2020-09-09 10:46:23'),(12,19,'2019-10-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(13,26,'2020-08-13','2020-09-09 10:46:23','2020-09-09 10:46:23'),(14,2,'2020-06-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(15,35,'2020-07-05','2020-09-09 10:46:23','2020-09-09 10:46:23'),(16,45,'2020-07-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(17,4,'2020-04-28','2020-09-09 10:46:23','2020-09-09 10:46:23'),(18,37,'2020-03-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(19,13,'2020-08-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(20,39,'2020-05-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(21,20,'2020-04-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(22,25,'2020-03-25','2020-09-09 10:46:23','2020-09-09 10:46:23'),(23,41,'2020-08-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(24,6,'2019-11-25','2020-09-09 10:46:23','2020-09-09 10:46:23'),(25,49,'2020-05-19','2020-09-09 10:46:23','2020-09-09 10:46:23'),(26,49,'2020-03-25','2020-09-09 10:46:23','2020-09-09 10:46:23'),(27,9,'2020-03-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(28,18,'2020-06-05','2020-09-09 10:46:23','2020-09-09 10:46:23'),(29,33,'2020-07-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(30,38,'2020-01-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(31,1,'2019-09-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(32,34,'2020-05-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(33,33,'2020-01-21','2020-09-09 10:46:23','2020-09-09 10:46:23'),(34,19,'2020-01-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(35,23,'2019-10-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(36,16,'2020-08-19','2020-09-09 10:46:23','2020-09-09 10:46:23'),(37,18,'2020-01-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(38,14,'2020-02-28','2020-09-09 10:46:23','2020-09-09 10:46:23'),(39,23,'2020-05-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(40,7,'2020-06-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(41,39,'2020-03-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(42,42,'2019-11-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(43,44,'2020-05-19','2020-09-09 10:46:23','2020-09-09 10:46:23'),(44,31,'2019-11-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(45,30,'2020-05-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(46,48,'2020-06-29','2020-09-09 10:46:23','2020-09-09 10:46:23'),(47,44,'2020-05-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(48,3,'2020-03-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(49,36,'2019-11-21','2020-09-09 10:46:23','2020-09-09 10:46:23'),(50,6,'2019-12-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(51,6,'2020-01-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(52,29,'2020-02-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(53,8,'2019-12-05','2020-09-09 10:46:23','2020-09-09 10:46:23'),(54,50,'2020-05-19','2020-09-09 10:46:23','2020-09-09 10:46:23'),(55,31,'2019-12-15','2020-09-09 10:46:23','2020-09-09 10:46:23'),(56,12,'2019-10-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(57,29,'2020-06-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(58,26,'2019-10-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(59,17,'2020-04-21','2020-09-09 10:46:23','2020-09-09 10:46:23'),(60,38,'2020-01-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(61,15,'2020-07-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(62,39,'2019-10-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(63,48,'2020-01-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(64,5,'2020-08-18','2020-09-09 10:46:23','2020-09-09 10:46:23'),(65,23,'2020-03-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(66,17,'2019-10-29','2020-09-09 10:46:23','2020-09-09 10:46:23'),(67,46,'2020-07-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(68,27,'2020-01-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(69,1,'2019-11-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(70,14,'2020-09-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(71,26,'2020-02-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(72,30,'2019-12-29','2020-09-09 10:46:23','2020-09-09 10:46:23'),(73,6,'2020-06-02','2020-09-09 10:46:23','2020-09-09 10:46:23'),(74,41,'2020-04-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(75,29,'2020-07-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(76,28,'2020-09-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(77,3,'2019-12-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(78,34,'2020-06-15','2020-09-09 10:46:23','2020-09-09 10:46:23'),(79,24,'2020-08-25','2020-09-09 10:46:23','2020-09-09 10:46:23'),(80,38,'2020-02-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(81,9,'2019-09-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(82,37,'2020-03-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(83,8,'2019-12-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(84,31,'2019-11-21','2020-09-09 10:46:23','2020-09-09 10:46:23'),(85,18,'2020-07-18','2020-09-09 10:46:23','2020-09-09 10:46:23'),(86,8,'2020-04-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(87,31,'2020-01-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(88,15,'2019-10-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(89,20,'2020-08-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(90,42,'2020-08-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(91,19,'2019-09-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(92,13,'2020-08-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(93,9,'2020-05-23','2020-09-09 10:46:23','2020-09-09 10:46:23'),(94,6,'2020-07-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(95,45,'2020-04-02','2020-09-09 10:46:23','2020-09-09 10:46:23'),(96,6,'2020-03-05','2020-09-09 10:46:23','2020-09-09 10:46:23'),(97,33,'2020-02-15','2020-09-09 10:46:23','2020-09-09 10:46:23'),(98,38,'2020-01-02','2020-09-09 10:46:23','2020-09-09 10:46:23'),(99,1,'2020-08-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(100,42,'2020-05-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(101,6,'2020-07-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(102,50,'2020-03-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(103,41,'2019-10-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(104,37,'2020-03-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(105,1,'2019-12-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(106,28,'2019-09-13','2020-09-09 10:46:23','2020-09-09 10:46:23'),(107,25,'2020-04-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(108,45,'2020-06-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(109,21,'2020-01-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(110,42,'2020-05-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(111,13,'2020-04-29','2020-09-09 10:46:23','2020-09-09 10:46:23'),(112,13,'2020-08-31','2020-09-09 10:46:23','2020-09-09 10:46:23'),(113,11,'2020-07-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(114,41,'2019-12-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(115,13,'2020-05-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(116,46,'2020-07-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(117,46,'2020-04-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(118,41,'2019-12-18','2020-09-09 10:46:23','2020-09-09 10:46:23'),(119,43,'2019-09-23','2020-09-09 10:46:23','2020-09-09 10:46:23'),(120,28,'2019-11-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(121,13,'2020-08-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(122,5,'2019-09-23','2020-09-09 10:46:23','2020-09-09 10:46:23'),(123,23,'2019-10-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(124,34,'2019-10-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(125,8,'2020-06-04','2020-09-09 10:46:23','2020-09-09 10:46:23'),(126,26,'2019-10-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(127,37,'2020-06-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(128,20,'2020-08-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(129,24,'2020-02-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(130,7,'2020-03-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(131,18,'2020-04-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(132,34,'2020-08-29','2020-09-09 10:46:23','2020-09-09 10:46:23'),(133,38,'2019-10-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(134,30,'2019-11-04','2020-09-09 10:46:23','2020-09-09 10:46:23'),(135,38,'2019-10-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(136,12,'2020-06-30','2020-09-09 10:46:23','2020-09-09 10:46:23'),(137,17,'2020-05-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(138,6,'2020-07-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(139,3,'2020-02-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(140,11,'2020-05-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(141,7,'2020-09-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(142,5,'2020-01-03','2020-09-09 10:46:23','2020-09-09 10:46:23'),(143,47,'2019-11-16','2020-09-09 10:46:23','2020-09-09 10:46:23'),(144,13,'2020-06-12','2020-09-09 10:46:23','2020-09-09 10:46:23'),(145,21,'2020-03-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(146,50,'2019-10-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(147,13,'2020-03-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(148,10,'2020-07-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(149,50,'2019-09-20','2020-09-09 10:46:23','2020-09-09 10:46:23'),(150,20,'2020-07-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(151,30,'2020-03-17','2020-09-09 10:46:23','2020-09-09 10:46:23'),(152,17,'2020-04-09','2020-09-09 10:46:23','2020-09-09 10:46:23'),(153,47,'2019-09-15','2020-09-09 10:46:23','2020-09-09 10:46:23'),(154,20,'2019-11-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(155,49,'2020-02-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(156,32,'2019-11-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(157,1,'2020-06-24','2020-09-09 10:46:23','2020-09-09 10:46:23'),(158,5,'2020-05-31','2020-09-09 10:46:23','2020-09-09 10:46:23'),(159,44,'2020-05-26','2020-09-09 10:46:23','2020-09-09 10:46:23'),(160,44,'2020-07-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(161,17,'2020-05-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(162,31,'2020-05-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(163,44,'2019-12-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(164,24,'2020-04-08','2020-09-09 10:46:23','2020-09-09 10:46:23'),(165,22,'2020-06-01','2020-09-09 10:46:23','2020-09-09 10:46:23'),(166,23,'2020-08-07','2020-09-09 10:46:23','2020-09-09 10:46:23'),(167,12,'2020-04-05','2020-09-09 10:46:23','2020-09-09 10:46:23'),(168,32,'2020-07-22','2020-09-09 10:46:23','2020-09-09 10:46:23'),(169,38,'2020-05-11','2020-09-09 10:46:23','2020-09-09 10:46:23'),(170,8,'2020-04-14','2020-09-09 10:46:23','2020-09-09 10:46:23'),(171,41,'2019-12-02','2020-09-09 10:46:23','2020-09-09 10:46:23'),(172,36,'2020-02-27','2020-09-09 10:46:23','2020-09-09 10:46:23'),(173,11,'2020-04-10','2020-09-09 10:46:23','2020-09-09 10:46:23'),(174,12,'2020-08-06','2020-09-09 10:46:23','2020-09-09 10:46:23'),(175,12,'2020-08-28','2020-09-09 10:46:23','2020-09-09 10:46:23'),(176,31,'2020-01-19','2020-09-09 10:46:24','2020-09-09 10:46:24'),(177,5,'2019-09-12','2020-09-09 10:46:24','2020-09-09 10:46:24'),(178,43,'2020-02-24','2020-09-09 10:46:24','2020-09-09 10:46:24'),(179,36,'2020-06-08','2020-09-09 10:46:24','2020-09-09 10:46:24'),(180,32,'2020-08-02','2020-09-09 10:46:24','2020-09-09 10:46:24'),(181,41,'2020-06-25','2020-09-09 10:46:24','2020-09-09 10:46:24'),(182,17,'2020-05-21','2020-09-09 10:46:24','2020-09-09 10:46:24'),(183,18,'2019-10-30','2020-09-09 10:46:24','2020-09-09 10:46:24'),(184,18,'2019-12-23','2020-09-09 10:46:24','2020-09-09 10:46:24'),(185,10,'2019-11-18','2020-09-09 10:46:24','2020-09-09 10:46:24'),(186,22,'2020-06-22','2020-09-09 10:46:24','2020-09-09 10:46:24'),(187,22,'2020-01-22','2020-09-09 10:46:24','2020-09-09 10:46:24'),(188,50,'2019-09-28','2020-09-09 10:46:24','2020-09-09 10:46:24'),(189,25,'2020-07-24','2020-09-09 10:46:24','2020-09-09 10:46:24'),(190,19,'2020-02-09','2020-09-09 10:46:24','2020-09-09 10:46:24'),(191,46,'2020-02-17','2020-09-09 10:46:24','2020-09-09 10:46:24'),(192,6,'2020-01-15','2020-09-09 10:46:24','2020-09-09 10:46:24'),(193,37,'2020-05-29','2020-09-09 10:46:24','2020-09-09 10:46:24'),(194,23,'2020-05-02','2020-09-09 10:46:24','2020-09-09 10:46:24'),(195,1,'2019-12-30','2020-09-09 10:46:24','2020-09-09 10:46:24'),(196,25,'2020-08-10','2020-09-09 10:46:24','2020-09-09 10:46:24'),(197,47,'2020-04-18','2020-09-09 10:46:24','2020-09-09 10:46:24'),(198,17,'2020-06-03','2020-09-09 10:46:24','2020-09-09 10:46:24'),(199,38,'2020-01-23','2020-09-09 10:46:24','2020-09-09 10:46:24'),(200,17,'2019-11-23','2020-09-09 10:46:24','2020-09-09 10:46:24');
/*!40000 ALTER TABLE `site_holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sites` (
  `idsite` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double(8,2) DEFAULT NULL,
  `longitude` double(8,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idsite`),
  KEY `sites_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `sites_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,9,'Carmen Hernándes',NULL,'santiago.linares@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(2,8,'D. Manuel Rodarte Segundo',NULL,'isabel46@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(3,1,'Josefa Hernando',NULL,'negron.leire@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(4,3,'Dr. Silvia Anguiano Tercero',NULL,'lbarrios@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(5,2,'Ing. Marcos Marín',NULL,'ccaro@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(6,6,'Aaron Bañuelos',NULL,'puig.celia@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(7,3,'Inmaculada Paz',NULL,'antonio21@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(8,6,'Cristian Acosta',NULL,'silvia.robledo@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(9,7,'Iván Balderas Hijo',NULL,'sonia.alejandro@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(10,3,'Aurora Conde',NULL,'biel16@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(11,1,'Rafael Ulloa Segundo',NULL,'zcollado@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(12,5,'Lola Urías Segundo',NULL,'villalba.guillermo@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(13,2,'Bruno Rivas',NULL,'fmarcos@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(14,7,'Guillem Vergara',NULL,'juanjose09@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(15,5,'Jordi Ávalos',NULL,'saul.delacruz@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(16,4,'Nerea Negrón',NULL,'sara89@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(17,2,'Srta. Claudia Aragón',NULL,'beatriz.caban@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(18,4,'Sr. Hugo Rolón',NULL,'raul27@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(19,8,'Lic. Rubén Estrada',NULL,'xportillo@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(20,3,'Iván Urrutia Segundo',NULL,'jpelayo@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(21,7,'Gabriel Pons',NULL,'eric40@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(22,2,'Raúl Luevano',NULL,'samuel82@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(23,1,'Asier Luevano',NULL,'martina.carmona@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(24,2,'Alonso Roca',NULL,'bonilla.noelia@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(25,3,'Lic. Marcos Expósito',NULL,'carlos.zambrano@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(26,8,'Sr. Omar Campos',NULL,'nieves.roberto@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(27,1,'Nil Caro',NULL,'tmateos@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(28,5,'Dr. Manuel Toro Hijo',NULL,'cantu.daniel@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(29,5,'Rosa Trejo',NULL,'elena72@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(30,5,'Francisco Cabán',NULL,'parra.natalia@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(31,4,'Noa Salinas',NULL,'juan68@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(32,7,'Yeray Ramos',NULL,'navarro.manuela@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(33,3,'Alexia Fuentes Tercero',NULL,'yago90@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(34,9,'Gloria Alfonso',NULL,'kgaitan@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(35,5,'Ing. Daniel Juan Segundo',NULL,'tsalinas@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(36,1,'Blanca Expósito',NULL,'enrique80@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(37,10,'Candela Casado Segundo',NULL,'aandres@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(38,9,'Blanca Preciado',NULL,'mcastano@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(39,1,'Alejandra Maestas',NULL,'rocio30@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(40,6,'Cristian Ramírez',NULL,'lara.barrientos@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(41,4,'Mara Vargas Hijo',NULL,'marcos.lemus@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(42,8,'Dr. Aaron Prieto Hijo',NULL,'murillo.yaiza@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(43,9,'Bruno Banda Segundo',NULL,'olivia.cavazos@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(44,5,'Aaron Soler',NULL,'malave.oliver@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(45,5,'Dr. Ángel Pabón',NULL,'mariacarmen.collazo@example.net',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(46,3,'Biel Badillo',NULL,'jon.godinez@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(47,3,'Andrea Cervantes',NULL,'vigil.oscar@example.com',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(48,5,'Rocío Abrego Hijo',NULL,'jorge39@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(49,6,'Francisca Salazar',NULL,'msuarez@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22'),(50,3,'Celia Gálvez',NULL,'carrera.miguel@example.org',NULL,NULL,NULL,NULL,1,0,'2020-09-09 10:46:22','2020-09-09 10:46:22');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_has_tags`
--

DROP TABLE IF EXISTS `sites_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sites_has_tags` (
  `idsite` bigint(20) unsigned NOT NULL,
  `idtag` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`idsite`,`idtag`),
  KEY `sites_has_tags_idtag_foreign` (`idtag`),
  CONSTRAINT `sites_has_tags_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`),
  CONSTRAINT `sites_has_tags_idtag_foreign` FOREIGN KEY (`idtag`) REFERENCES `tags` (`idtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_has_tags`
--

LOCK TABLES `sites_has_tags` WRITE;
/*!40000 ALTER TABLE `sites_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `sites_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_categories`
--

DROP TABLE IF EXISTS `tag_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag_categories` (
  `idcategory` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idcategory`),
  KEY `tag_categories_idcustomer_foreign` (`idcustomer`),
  CONSTRAINT `tag_categories_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_categories`
--

LOCK TABLES `tag_categories` WRITE;
/*!40000 ALTER TABLE `tag_categories` DISABLE KEYS */;
INSERT INTO `tag_categories` VALUES (1,1,'Dña Carlota Treviño',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(2,5,'Fátima Escribano',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(3,NULL,'Ing. Gabriel Casárez Hijo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(4,2,'Amparo Benavides',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(5,9,'Cristian Díaz Segundo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(6,NULL,'Sr. Isaac Carrasquillo Hijo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(7,2,'Beatriz Palomo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(8,NULL,'Lic. Yeray Razo Hijo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(9,8,'Gabriel Olivo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(10,1,'Valentina Costa',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(11,9,'Nayara Valdivia',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(12,2,'Amparo Rosa',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(13,8,'Oriol Farías',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(14,6,'Lic. Iker Rojas',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(15,1,'María Pilar Garay',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(16,8,'Martín Gil',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(17,3,'Manuel Arellano',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(18,10,'Esther Montero',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(19,7,'Silvia Lucas',0,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(20,1,'Cristian Moral Hijo',0,'2020-09-09 10:46:24','2020-09-09 10:46:24');
/*!40000 ALTER TABLE `tag_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `idtag` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcategory` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idtag`),
  UNIQUE KEY `tags_name_unique` (`name`),
  KEY `tags_idcategory_foreign` (`idcategory`),
  CONSTRAINT `tags_idcategory_foreign` FOREIGN KEY (`idcategory`) REFERENCES `tag_categories` (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,10,'vel','2020-09-09 10:46:24','2020-09-09 10:46:24'),(2,5,'perspiciatis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(3,18,'est','2020-09-09 10:46:24','2020-09-09 10:46:24'),(4,10,'et','2020-09-09 10:46:24','2020-09-09 10:46:24'),(5,12,'atque','2020-09-09 10:46:24','2020-09-09 10:46:24'),(6,10,'sunt','2020-09-09 10:46:24','2020-09-09 10:46:24'),(7,16,'consequatur','2020-09-09 10:46:24','2020-09-09 10:46:24'),(8,8,'in','2020-09-09 10:46:24','2020-09-09 10:46:24'),(9,5,'eaque','2020-09-09 10:46:24','2020-09-09 10:46:24'),(10,8,'natus','2020-09-09 10:46:24','2020-09-09 10:46:24'),(11,5,'quo','2020-09-09 10:46:24','2020-09-09 10:46:24'),(12,19,'magni','2020-09-09 10:46:24','2020-09-09 10:46:24'),(13,14,'qui','2020-09-09 10:46:24','2020-09-09 10:46:24'),(14,14,'sint','2020-09-09 10:46:24','2020-09-09 10:46:24'),(15,9,'voluptas','2020-09-09 10:46:24','2020-09-09 10:46:24'),(16,1,'nulla','2020-09-09 10:46:24','2020-09-09 10:46:24'),(17,18,'voluptatem','2020-09-09 10:46:24','2020-09-09 10:46:24'),(18,13,'nesciunt','2020-09-09 10:46:24','2020-09-09 10:46:24'),(19,5,'dignissimos','2020-09-09 10:46:24','2020-09-09 10:46:24'),(20,14,'hic','2020-09-09 10:46:24','2020-09-09 10:46:24'),(21,19,'illum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(22,12,'neque','2020-09-09 10:46:24','2020-09-09 10:46:24'),(23,13,'maxime','2020-09-09 10:46:24','2020-09-09 10:46:24'),(24,2,'quasi','2020-09-09 10:46:24','2020-09-09 10:46:24'),(25,19,'iste','2020-09-09 10:46:24','2020-09-09 10:46:24'),(26,20,'temporibus','2020-09-09 10:46:24','2020-09-09 10:46:24'),(27,2,'laudantium','2020-09-09 10:46:24','2020-09-09 10:46:24'),(28,15,'praesentium','2020-09-09 10:46:24','2020-09-09 10:46:24'),(29,4,'molestiae','2020-09-09 10:46:24','2020-09-09 10:46:24'),(30,8,'ratione','2020-09-09 10:46:24','2020-09-09 10:46:24'),(31,7,'excepturi','2020-09-09 10:46:24','2020-09-09 10:46:24'),(32,8,'perferendis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(33,13,'cumque','2020-09-09 10:46:24','2020-09-09 10:46:24'),(34,20,'nemo','2020-09-09 10:46:24','2020-09-09 10:46:24'),(35,10,'voluptate','2020-09-09 10:46:24','2020-09-09 10:46:24'),(36,16,'at','2020-09-09 10:46:24','2020-09-09 10:46:24'),(37,20,'sit','2020-09-09 10:46:24','2020-09-09 10:46:24'),(38,7,'blanditiis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(39,7,'a','2020-09-09 10:46:24','2020-09-09 10:46:24'),(40,2,'veritatis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(41,4,'quis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(42,1,'non','2020-09-09 10:46:24','2020-09-09 10:46:24'),(43,14,'earum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(44,1,'unde','2020-09-09 10:46:24','2020-09-09 10:46:24'),(45,5,'reiciendis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(46,20,'omnis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(47,17,'aliquam','2020-09-09 10:46:24','2020-09-09 10:46:24'),(48,14,'suscipit','2020-09-09 10:46:24','2020-09-09 10:46:24'),(49,9,'repudiandae','2020-09-09 10:46:24','2020-09-09 10:46:24'),(50,11,'nihil','2020-09-09 10:46:24','2020-09-09 10:46:24'),(51,4,'rerum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(52,9,'voluptatum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(53,15,'impedit','2020-09-09 10:46:24','2020-09-09 10:46:24'),(54,3,'dolor','2020-09-09 10:46:24','2020-09-09 10:46:24'),(55,16,'error','2020-09-09 10:46:24','2020-09-09 10:46:24'),(56,5,'aut','2020-09-09 10:46:24','2020-09-09 10:46:24'),(57,4,'iusto','2020-09-09 10:46:24','2020-09-09 10:46:24'),(58,20,'quidem','2020-09-09 10:46:24','2020-09-09 10:46:24'),(59,19,'odio','2020-09-09 10:46:24','2020-09-09 10:46:24'),(60,17,'molestias','2020-09-09 10:46:24','2020-09-09 10:46:24'),(61,18,'sed','2020-09-09 10:46:24','2020-09-09 10:46:24'),(62,2,'minus','2020-09-09 10:46:24','2020-09-09 10:46:24'),(63,1,'consectetur','2020-09-09 10:46:24','2020-09-09 10:46:24'),(64,16,'illo','2020-09-09 10:46:24','2020-09-09 10:46:24'),(65,2,'aliquid','2020-09-09 10:46:24','2020-09-09 10:46:24'),(66,18,'quia','2020-09-09 10:46:24','2020-09-09 10:46:24'),(67,14,'ab','2020-09-09 10:46:24','2020-09-09 10:46:24'),(68,10,'distinctio','2020-09-09 10:46:24','2020-09-09 10:46:24'),(69,9,'voluptates','2020-09-09 10:46:24','2020-09-09 10:46:24'),(70,19,'laboriosam','2020-09-09 10:46:24','2020-09-09 10:46:24'),(71,12,'eveniet','2020-09-09 10:46:24','2020-09-09 10:46:24'),(72,7,'iure','2020-09-09 10:46:24','2020-09-09 10:46:24'),(73,10,'facilis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(74,13,'quos','2020-09-09 10:46:24','2020-09-09 10:46:24'),(75,4,'cum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(76,7,'harum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(77,20,'fuga','2020-09-09 10:46:24','2020-09-09 10:46:24'),(78,15,'velit','2020-09-09 10:46:24','2020-09-09 10:46:24'),(79,3,'autem','2020-09-09 10:46:24','2020-09-09 10:46:24'),(80,9,'provident','2020-09-09 10:46:24','2020-09-09 10:46:24'),(81,17,'explicabo','2020-09-09 10:46:24','2020-09-09 10:46:24'),(82,19,'expedita','2020-09-09 10:46:24','2020-09-09 10:46:24'),(83,14,'eum','2020-09-09 10:46:24','2020-09-09 10:46:24'),(84,20,'occaecati','2020-09-09 10:46:24','2020-09-09 10:46:24'),(85,6,'adipisci','2020-09-09 10:46:24','2020-09-09 10:46:24'),(86,6,'doloremque','2020-09-09 10:46:24','2020-09-09 10:46:24'),(87,13,'dolores','2020-09-09 10:46:24','2020-09-09 10:46:24'),(88,14,'placeat','2020-09-09 10:46:24','2020-09-09 10:46:24'),(89,11,'ea','2020-09-09 10:46:24','2020-09-09 10:46:24'),(90,16,'dolore','2020-09-09 10:46:24','2020-09-09 10:46:24'),(91,7,'fugiat','2020-09-09 10:46:24','2020-09-09 10:46:24'),(92,16,'ducimus','2020-09-09 10:46:24','2020-09-09 10:46:24'),(93,11,'deleniti','2020-09-09 10:46:24','2020-09-09 10:46:24'),(94,15,'optio','2020-09-09 10:46:24','2020-09-09 10:46:24'),(95,12,'debitis','2020-09-09 10:46:24','2020-09-09 10:46:24'),(96,11,'inventore','2020-09-09 10:46:24','2020-09-09 10:46:24'),(97,6,'ex','2020-09-09 10:46:24','2020-09-09 10:46:24'),(98,5,'esse','2020-09-09 10:46:24','2020-09-09 10:46:24'),(99,19,'repellat','2020-09-09 10:46:24','2020-09-09 10:46:24'),(100,15,'possimus','2020-09-09 10:46:24','2020-09-09 10:46:24');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `iduser` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idcustomer` bigint(20) unsigned DEFAULT NULL,
  `idsite` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_idcustomer_foreign` (`idcustomer`),
  KEY `users_idsite_foreign` (`idsite`),
  CONSTRAINT `users_idcustomer_foreign` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomer`),
  CONSTRAINT `users_idsite_foreign` FOREIGN KEY (`idsite`) REFERENCES `sites` (`idsite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,NULL,'superadmin','superadmin@ladorian.com','$2y$10$YXYql7tTo99kOTT00hTtUeQyND.Kg2Dci3yNWsyGni3P6WVw7diMG',1,0,NULL,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(2,1,NULL,'admin','admin@ladorian.com','$2y$10$e/rueiPlnUrB/hx.v7u1K.JN.M14MOR3Zv29IVAupOcKDCjUeRg4i',1,0,NULL,'2020-09-09 10:46:24','2020-09-09 10:46:24'),(3,2,3,'user','user@ladorian.com','$2y$10$hsTBpkLPW52l73K7fUHype80BIQoN9dt53iMLobzwMIAe0h/RkBky',1,0,NULL,'2020-09-09 10:46:24','2020-09-09 10:46:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-25  6:20:15
