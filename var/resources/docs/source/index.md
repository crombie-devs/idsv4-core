---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://127.0.0.1:8000/docs/collection.json)

<!-- END_INFO -->

#App management


<!-- START_aec6ff8a4a59a675ef3bd102218b0ca9 -->
## Creación de una nueva app
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/app" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"sit","idcustomer":"ut","apikey":"praesentium"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/app"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "sit",
    "idcustomer": "ut",
    "apikey": "praesentium"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/app`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `idcustomer` | string |  required  | 
        `apikey` | string |  required  | 
    
<!-- END_aec6ff8a4a59a675ef3bd102218b0ca9 -->

<!-- START_2fbbd588d93571c3920c21a18453c2e9 -->
## Actualización de una app
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/app/eligendi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"sed","idcustomer":"eos","apikey":"accusamus","status":true}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/app/eligendi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "sed",
    "idcustomer": "eos",
    "apikey": "accusamus",
    "status": true
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/app/{idapp}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idapp` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
        `idcustomer` | string |  optional  | 
        `apikey` | string |  optional  | 
        `status` | boolean |  optional  | 
    
<!-- END_2fbbd588d93571c3920c21a18453c2e9 -->

<!-- START_55ec7f3fc1e4109d61e074807229dc29 -->
## Borrado de una app de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/app/quas" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/app/quas"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/app/{idapp}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idapp` |  required  | 

<!-- END_55ec7f3fc1e4109d61e074807229dc29 -->

<!-- START_7c3dad513ec3099ca4178870f4e4b644 -->
## Aceso a los datos de una app
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/app/delectus" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/app/delectus"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/app/{idapp}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idapp` |  optional  | int required

<!-- END_7c3dad513ec3099ca4178870f4e4b644 -->

<!-- START_4e749e7aca7a13024817be37f0107845 -->
## Acceso a todas las apps de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/apps" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/apps"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/apps`


<!-- END_4e749e7aca7a13024817be37f0107845 -->

<!-- START_8685d46514c343feb0f1ff46584e636f -->
## Acceso a las apps de customer de la base de datos
Esta acción la pueden realizar el admin y el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/apps/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/apps/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/apps/toselect`


<!-- END_8685d46514c343feb0f1ff46584e636f -->

#AssetType management


<!-- START_d8e62e7060d3e9312bef355cae0b3307 -->
## Acceso a todos los asset type de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/asset-types/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/asset-types/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/asset-types/toselect`


<!-- END_d8e62e7060d3e9312bef355cae0b3307 -->

#Asset management


<!-- START_c70df79cf1e430cab36f1878720d36f7 -->
## Creación de un nuevo asset
Esta acción la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden crear &quot;assets&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden crear "assets" con su mismo idcustomer.

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/asset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":6,"idsite":15,"idtype":8,"name":"dolorum","asset":"ducimus","duration":10,"idformat":9,"size":1,"idlang":19,"default":4,"has_audio":8,"status":7,"deleted":7,"asset_url":"totam"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/asset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 6,
    "idsite": 15,
    "idtype": 8,
    "name": "dolorum",
    "asset": "ducimus",
    "duration": 10,
    "idformat": 9,
    "size": 1,
    "idlang": 19,
    "default": 4,
    "has_audio": 8,
    "status": 7,
    "deleted": 7,
    "asset_url": "totam"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/asset`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
        `idtype` | integer |  required  | 
        `name` | string |  required  | 
        `asset` | File |  optional  | 
        `duration` | integer |  optional  | 
        `idformat` | integer |  required  | 
        `size` | integer |  optional  | 
        `idlang` | integer |  required  | 
        `default` | integer |  optional  | 
        `has_audio` | integer |  optional  | 
        `status` | integer |  optional  | 
        `deleted` | integer |  optional  | 
        `asset_url` | string |  optional  | 
    
<!-- END_c70df79cf1e430cab36f1878720d36f7 -->

<!-- START_bd6b9219929113c5de1a8b167342c90d -->
## Actualización de un asset
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden editar &quot;assets&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden editar "assets" con su mismo idcustomer.

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/asset/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":4,"idsite":2,"idtype":17,"name":"perferendis","asset":"et","duration":15,"idformat":8,"size":15,"idlang":3,"default":12,"has_audio":6,"status":7,"deleted":7,"asset_url":"et"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/asset/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 4,
    "idsite": 2,
    "idtype": 17,
    "name": "perferendis",
    "asset": "et",
    "duration": 15,
    "idformat": 8,
    "size": 15,
    "idlang": 3,
    "default": 12,
    "has_audio": 6,
    "status": 7,
    "deleted": 7,
    "asset_url": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/asset/{idasset}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
        `idtype` | integer |  required  | 
        `name` | string |  required  | 
        `asset` | File |  optional  | 
        `duration` | integer |  optional  | 
        `idformat` | integer |  required  | 
        `size` | integer |  optional  | 
        `idlang` | integer |  required  | 
        `default` | integer |  optional  | 
        `has_audio` | integer |  optional  | 
        `status` | integer |  optional  | 
        `deleted` | integer |  optional  | 
        `asset_url` | string |  optional  | 
    
<!-- END_bd6b9219929113c5de1a8b167342c90d -->

<!-- START_92eb08e04480c91d6d227ef2f058677a -->
## Borrado de un asset de la base de datos
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden eliminar &quot;assets&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden eliminar "assets" con su mismo idcustomer.

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/asset/vel" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/asset/vel"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/asset/{idasset}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idasset` |  required  | 

<!-- END_92eb08e04480c91d6d227ef2f058677a -->

<!-- START_0543f5cef4678797a6e17632adef699a -->
## Acceso a los datos de un asset
Esta acción sólo la puede realizar el superadmin, admin  y user
Los usuarios con idsite, solo pueden visualizar &quot;assets&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden visualizar "assets" con su mismo idcustomer.

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/asset/sed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/asset/sed"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/asset/{idasset}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idasset` |  optional  | int required

<!-- END_0543f5cef4678797a6e17632adef699a -->

<!-- START_00c005aaff779b23a0b12ad43a010961 -->
## Acceso a todos los assets de la base de datos
Esta acción sólo la puede realizar el superadmin, admin, user
delete = 0
idsite de usuario (solo si usuario tiene idsite)
idcustomer de usuario (solo si usuario tiene idcustomer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/assets" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/assets"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/assets`


<!-- END_00c005aaff779b23a0b12ad43a010961 -->

<!-- START_95d16e8a9e0d1d9034235e6bf0e62ab8 -->
## Acceso a todos los assets de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Debe devolver el listado de todos los assets filtrados por: status = 1 delete = 0
idsite de usuario (solo si usuario tiene idsite)
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/assets/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/assets/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/assets/toselect`


<!-- END_95d16e8a9e0d1d9034235e6bf0e62ab8 -->

#Camera management


<!-- START_3d942ec39cada0ea3dfb6025ad3b3a11 -->
## Creación de un nuevo camera
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/camera" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"reiciendis","idsite":1,"code":"harum","name":"porro"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "reiciendis",
    "idsite": 1,
    "code": "harum",
    "name": "porro"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/camera`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | 
        `idsite` | integer |  required  | 
        `code` | string |  optional  | 
        `name` | string |  optional  | 
    
<!-- END_3d942ec39cada0ea3dfb6025ad3b3a11 -->

<!-- START_77393836df02d43cfe2b088737488b6f -->
## Actualización de un camera
Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el usuario de la misma tienda

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/camera/mollitia" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"sint","status":"autem"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera/mollitia"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "sint",
    "status": "autem"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/camera/{idcamera}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcamera` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  optional  | 
        `status` | string |  optional  | 
    
<!-- END_77393836df02d43cfe2b088737488b6f -->

<!-- START_5265cb40c1f591e3a5ab9e59d98e6836 -->
## Borrado de un camera de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/camera/neque" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera/neque"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/camera/{idcamera}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcamera` |  required  | 

<!-- END_5265cb40c1f591e3a5ab9e59d98e6836 -->

<!-- START_2829a7aede105d1bc409f2bda4918b36 -->
## Acceso a los datos de un camera
Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user de la misma tienda

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/camera/nisi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera/nisi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/camera/{idcamera}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcamera` |  optional  | int required

<!-- END_2829a7aede105d1bc409f2bda4918b36 -->

<!-- START_ed2527b454a56b93725d0b38240522c6 -->
## Acceso a todos los cameras de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/cameras" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/cameras"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/cameras`


<!-- END_ed2527b454a56b93725d0b38240522c6 -->

<!-- START_a71bd1e49a5aa7541795d9f47ebcd9a6 -->
## Acceso a las cámaras para estadísticas
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/cameras/toanalytics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/cameras/toanalytics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/cameras/toanalytics`


<!-- END_a71bd1e49a5aa7541795d9f47ebcd9a6 -->

<!-- START_b3ec3e4e9f7d93c77582710602e23aa0 -->
## Acceso a todos las camaras de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Debe devolver el listado de todos las camaras filtrados por: status = 1 delete = 0

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/cameras/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/cameras/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/cameras/toselect`


<!-- END_b3ec3e4e9f7d93c77582710602e23aa0 -->

<!-- START_8360fdbafc347362d4dcc42b8323220f -->
## Actualización de los power de un camera
Esta acción la puede realizar cualquier usuario
Debe devolver el camera con todos los nuevos power.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/camera/odio/powers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"powers":"in"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera/odio/powers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "powers": "in"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/camera/{idcamera}/powers`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcamera` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `powers` | required |  optional  | array idpower integer required
    
<!-- END_8360fdbafc347362d4dcc42b8323220f -->

<!-- START_39cf8909d809d074d80f705762d563cb -->
## Borra una relación power/camera
Esta acción la puede realizar cualquier usuario
Devuelve camera con nuevos power

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/camera/sapiente/power/quibusdam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/camera/sapiente/power/quibusdam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/camera/{idcamera}/power/{idpower}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcamera` |  required  | int
    `idpower` |  required  | int

<!-- END_39cf8909d809d074d80f705762d563cb -->

#City management


<!-- START_4b316fddd9a9cf836c83fdf372f27302 -->
## Acceso a todos las cities de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/cities/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/cities/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/cities/toselect`


<!-- END_4b316fddd9a9cf836c83fdf372f27302 -->

#ContentCategory management


<!-- START_af2e656edbe5a8eb3666ffd09b984a7a -->
## Creación de un nuevo contentcategory
Esta acción sólo la puede realizar el superadmin y admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/content-category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":6,"name":"quasi","image_url":"enim"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 6,
    "name": "quasi",
    "image_url": "enim"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/content-category`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `name` | string |  optional  | 
        `image_url` | File |  optional  | 
    
<!-- END_af2e656edbe5a8eb3666ffd09b984a7a -->

<!-- START_0f5dcdf260fa262abd98df932f8601d8 -->
## Actualización de un contentcategory
Esta acción sólo la puede realizar el superadmin y admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/content-category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":9,"name":"sapiente","image_url":"occaecati"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 9,
    "name": "sapiente",
    "image_url": "occaecati"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/content-category/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontentcategory` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `name` | string |  optional  | 
        `image_url` | File |  optional  | 
    
<!-- END_0f5dcdf260fa262abd98df932f8601d8 -->

<!-- START_83600e7193615171f1251e76810292cb -->
## Borrado de un contentcategory de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content-category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content-category/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontentcategory` |  required  | 

<!-- END_83600e7193615171f1251e76810292cb -->

<!-- START_9ab59063a380e6dd3c243cbf18afe4e1 -->
## Acceso a todos los contentcategories de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content-categories/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-categories/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content-categories/toselect`


<!-- END_9ab59063a380e6dd3c243cbf18afe4e1 -->

<!-- START_0f21329f94412d7155008a5754666812 -->
## Aceso a los datos de un contentcategory
Esta acción sólo superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content-category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content-category/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontentcategory` |  optional  | int required

<!-- END_0f21329f94412d7155008a5754666812 -->

<!-- START_9a36c6d2aa75e80c964f172e0381c1fe -->
## Acceso a todos las categorias de contenidos de la base de datos
Esta acción sólo la puede realizar el user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content-categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content-categories`


<!-- END_9a36c6d2aa75e80c964f172e0381c1fe -->

#ContentType management


<!-- START_f398468593312f25490727e7f6119fcb -->
## Acceso a todos las content type de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content-types/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content-types/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content-types/toselect`


<!-- END_f398468593312f25490727e7f6119fcb -->

#Content management


<!-- START_c61d7a44de05006c625196530ce1aabd -->
## Creación de un nuevo content
Esta acción la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden crear &quot;contents&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden crear "contents" con su mismo idcustomer.

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/content" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":10,"idsite":5,"idtype":3,"idcategory":11,"code":"quis","name":"sit","thumbnail_url":"tempore","tracker_url":"ut","date_on":"repudiandae","date_off":"voluptatibus","max_emissions":13,"frequency":20,"frequency_method":5,"is_exclusive":15,"status":20,"deleted":2}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 10,
    "idsite": 5,
    "idtype": 3,
    "idcategory": 11,
    "code": "quis",
    "name": "sit",
    "thumbnail_url": "tempore",
    "tracker_url": "ut",
    "date_on": "repudiandae",
    "date_off": "voluptatibus",
    "max_emissions": 13,
    "frequency": 20,
    "frequency_method": 5,
    "is_exclusive": 15,
    "status": 20,
    "deleted": 2
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/content`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
        `idtype` | integer |  required  | 
        `idcategory` | integer |  optional  | 
        `code` | string |  optional  | 
        `name` | string |  required  | 
        `thumbnail_url` | string |  optional  | 
        `tracker_url` | string |  optional  | 
        `date_on` | date |  required  | 
        `date_off` | date |  required  | 
        `max_emissions` | integer |  optional  | 
        `frequency` | integer |  optional  | 
        `frequency_method` | integer |  optional  | 
        `is_exclusive` | integer |  optional  | 
        `status` | integer |  optional  | 
        `deleted` | integer |  optional  | 
    
<!-- END_c61d7a44de05006c625196530ce1aabd -->

<!-- START_b2f956a22e3f52d8310f734697b31372 -->
## Actualización de un content
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden editar &quot;contents&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden editar "contents" con su mismo idcustomer.

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":20,"idsite":3,"idtype":2,"idcategory":13,"code":"aliquid","name":"voluptas","thumbnail_url":"qui","tracker_url":"aut","date_on":"aperiam","date_off":"odit","max_emissions":13,"frequency":6,"frequency_method":17,"is_exclusive":8,"status":6,"deleted":7}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 20,
    "idsite": 3,
    "idtype": 2,
    "idcategory": 13,
    "code": "aliquid",
    "name": "voluptas",
    "thumbnail_url": "qui",
    "tracker_url": "aut",
    "date_on": "aperiam",
    "date_off": "odit",
    "max_emissions": 13,
    "frequency": 6,
    "frequency_method": 17,
    "is_exclusive": 8,
    "status": 6,
    "deleted": 7
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
        `idtype` | integer |  required  | 
        `idcategory` | integer |  optional  | 
        `code` | string |  optional  | 
        `name` | string |  required  | 
        `thumbnail_url` | string |  optional  | 
        `tracker_url` | string |  optional  | 
        `date_on` | date |  required  | 
        `date_off` | date |  required  | 
        `max_emissions` | integer |  optional  | 
        `frequency` | integer |  optional  | 
        `frequency_method` | integer |  optional  | 
        `is_exclusive` | integer |  optional  | 
        `status` | integer |  optional  | 
        `deleted` | integer |  optional  | 
    
<!-- END_b2f956a22e3f52d8310f734697b31372 -->

<!-- START_d43d3c20cdfda4a2520590ff7e4aab20 -->
## Borrado de un content de la base de datos
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idsite, solo pueden eliminar &quot;contents&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden eliminar "contents" con su mismo idcustomer.

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/voluptatem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/voluptatem"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | 

<!-- END_d43d3c20cdfda4a2520590ff7e4aab20 -->

<!-- START_f32dff93cc1934531dc2967029fd55ac -->
## Acceso a los datos de un content
Esta acción sólo la puede realizar el superadmin, admin  y user
Los usuarios con idsite, solo pueden visualizar &quot;contents&quot; con su mismo idsite.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Los usuarios con idcustomer, solo pueden visualizar "contents" con su mismo idcustomer.

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content/eum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/eum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content/{idcontent}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  optional  | int required

<!-- END_f32dff93cc1934531dc2967029fd55ac -->

<!-- START_4cfc103d97eaba183c72b295bbaa087c -->
## Acceso a todos los contents de la base de datos
Esta acción sólo la puede realizar el superadmin, admin, user
delete = 0
idsite de usuario (solo si usuario tiene idsite)
idcustomer de usuario (solo si usuario tiene idcustomer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/contents" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/contents"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/contents`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `date` |  optional  | array

<!-- END_4cfc103d97eaba183c72b295bbaa087c -->

<!-- START_e1adb3bf1ce8361d5ac3baf0d7e9cb53 -->
## Acceso a todos los contents de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Debe devolver el listado de todos los contents y demas asociados filtrados por: status = 1 - delete = 0
content.date_on &lt;= $TODAY // ó parámetro date
content.date_off &gt;= $TODAY // ó parámetro date

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
idsite de usuario (solo si usuario tiene idsite)
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/contents/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/contents/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/contents/toselect`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `date` |  optional  | array

<!-- END_e1adb3bf1ce8361d5ac3baf0d7e9cb53 -->

<!-- START_0eff1b3c44f7eb3ad423414b32dfa0c8 -->
## Acceso a todos los contents de la base de datos desde el select para estadísticas/emisiones
Esta acción la puede realizar cualquier usuario
Debe devolver el listado de todos los contents del customer en el mes y año seleccionado

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/contents/toemission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":19,"idsite":6,"idplayer":6,"year":5,"month":10}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/contents/toemission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 19,
    "idsite": 6,
    "idplayer": 6,
    "year": 5,
    "month": 10
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/contents/toemission`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  required  | 
        `idsite` | integer |  optional  | 
        `idplayer` | integer |  optional  | 
        `year` | integer |  optional  | 
        `month` | integer |  optional  | 
    
<!-- END_0eff1b3c44f7eb3ad423414b32dfa0c8 -->

<!-- START_16918c5274f4c6073ea72ea5e58768dc -->
## Actualización de los powers de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos powers.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/natus/powers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"powers":[]}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/natus/powers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "powers": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/powers`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `powers` | array |  required  | weekday integer, time_on date con formato H:i,time_off date con formato H:i
    
<!-- END_16918c5274f4c6073ea72ea5e58768dc -->

<!-- START_ad2390b8bdd4ad9f1554ce142bfb42b4 -->
## Borra un power
Esta acción la puede realizar cualquier usuario
Devuelve null en data

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/et/power/sed" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/et/power/sed"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/power/{idpower}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idpower` |  required  | int

<!-- END_ad2390b8bdd4ad9f1554ce142bfb42b4 -->

<!-- START_7b6ff7816eb51b975472c598eba036f1 -->
## Actualización de los productos de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos productos.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/qui/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"products":[]}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/qui/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "products": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/products`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `products` | array |  required  | product_code string required, name string required
    
<!-- END_7b6ff7816eb51b975472c598eba036f1 -->

<!-- START_426a4a051b2fcdf3946c42b20eb1a9b5 -->
## Selecciona los productos de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos productos.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content/consequatur/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/consequatur/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content/{idcontent}/products`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int

<!-- END_426a4a051b2fcdf3946c42b20eb1a9b5 -->

<!-- START_ef3625f3a02807fbf04acbc376b9af90 -->
## Borra la relación de un producto con su contenido asociado
Esta acción la puede realizar cualquier usuario
Devuelve null en data

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/doloremque/product/quisquam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/doloremque/product/quisquam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/product/{product_code}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `product_code` |  required  | int

<!-- END_ef3625f3a02807fbf04acbc376b9af90 -->

<!-- START_7e03148d75c899a0b6690dd0d10ba5a0 -->
## Actualización de los playareas de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos playareas.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/est/playareas" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"areas":[]}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/est/playareas"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "areas": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/playareas`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `areas` | array |  required  | idarea int required
    
<!-- END_7e03148d75c899a0b6690dd0d10ba5a0 -->

<!-- START_2bd52b6a65189d70a5da5a420a6f7615 -->
## Borra una relación playarea - contenido
Esta acción la puede realizar cualquier usuario
Devuelve contenido con playareas

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/et/playarea/iure" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/et/playarea/iure"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/playarea/{idarea}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idarea` |  required  | int

<!-- END_2bd52b6a65189d70a5da5a420a6f7615 -->

<!-- START_2ab1d7ee75fe868ec2a95d93ac4c4b4a -->
## Actualización de los tags de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos tags.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/perferendis/tags" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"tags":"reprehenderit"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/perferendis/tags"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "tags": "reprehenderit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/tags`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `tags` | required |  optional  | array idtag integer required
    
<!-- END_2ab1d7ee75fe868ec2a95d93ac4c4b4a -->

<!-- START_dbe581a5df9fac16380ff585cea055b2 -->
## Borra un una relación tag/contenido
Esta acción la puede realizar cualquier usuario
Devuelve null en data

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/libero/tag/et" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/libero/tag/et"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idtag` |  required  | int

<!-- END_dbe581a5df9fac16380ff585cea055b2 -->

<!-- START_6300071248f320623c890cb292c8897c -->
## Añadir una nueva relación asset/contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos assets.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/content/iste/asset/aperiam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/iste/asset/aperiam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/content/{idcontent}/asset/{idasset}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idasset` |  required  | int

<!-- END_6300071248f320623c890cb292c8897c -->

<!-- START_3707f01a35f3fa0918296b488127bcb0 -->
## Actualización de los assets de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos assets.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/rem/assets" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"assets":"cupiditate"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/rem/assets"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "assets": "cupiditate"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/assets`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `assets` | required |  optional  | array idasset integer required, idcircuit integer optional
    
<!-- END_3707f01a35f3fa0918296b488127bcb0 -->

<!-- START_f9413d11caba84ad71bc728ca9a36474 -->
## Borra una relación asset/contenido
Esta acción la puede realizar cualquier usuario
Devuelve content con nuevos assets

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/enim/asset/autem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/enim/asset/autem"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/asset/{idasset}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idasset` |  required  | int

<!-- END_f9413d11caba84ad71bc728ca9a36474 -->

<!-- START_9b8ecf65d4c936d6667ba0cb014c90ab -->
## Actualización de los playcircuits de un contenido
Esta acción la puede realizar cualquier usuario
Debe devolver el content con todos los nuevos assets.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/content/id/playciruits" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"playcircuits":"sed"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/id/playciruits"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "playcircuits": "sed"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/content/{idcontent}/playciruits`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `playcircuits` | required |  optional  | array idcircuit integer required
    
<!-- END_9b8ecf65d4c936d6667ba0cb014c90ab -->

<!-- START_dfa7b417d40dfe085cedafe6b07100a6 -->
## Borra una relación playcircuit/contenido
Esta acción la puede realizar cualquier usuario
Devuelve content con nuevos playcircuits

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/content/repudiandae/playcircuit/dicta" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/repudiandae/playcircuit/dicta"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/content/{idcontent}/playcircuit/{idcircuit}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int
    `idcircuit` |  required  | int

<!-- END_dfa7b417d40dfe085cedafe6b07100a6 -->

<!-- START_74182067705886f814a80d5c5fbdca3a -->
## Acceso a todos los horas de mayor venta de un producto
Esta acción la puede realizar cualquier usuario
Devolverá un listado de weekdays y horas, que serán los powers.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/content/products/primetime" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"code_site":"nemo","idcategoryticket":"dicta"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/products/primetime"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "code_site": "nemo",
    "idcategoryticket": "dicta"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/content/products/primetime`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `code_site` | required |  optional  | number
        `idcategoryticket` | required |  optional  | number
    
<!-- END_74182067705886f814a80d5c5fbdca3a -->

<!-- START_074abb552cf8ed842cd869a44d95bdcb -->
## Acceso a todos los contents de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Devolverá un listado de weekdays y horas, que serán los powers.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/content/vel/primetime" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/content/vel/primetime"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/content/{idcontent}/primetime`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcontent` |  required  | int

<!-- END_074abb552cf8ed842cd869a44d95bdcb -->

<!-- START_176005c3dfdd7570bb540cf567e3274b -->
## Actualización de los order de un contenido
Esta acción la puede realizar superadmin y amdin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/contents/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"contents":"sed"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/contents/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "contents": "sed"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/contents/orders`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `contents` | required |  optional  | array idcontent integer required
    
<!-- END_176005c3dfdd7570bb540cf567e3274b -->

<!-- START_89d2f9aaad9dbb854646769a946d5d81 -->
## Debe devolver el listado de &quot;orders&quot; que cumpla:
Esta acción la puede realizar cualquier usuario

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
content.idcustomer = $user.idcustomer | NULL
content.idsite = $user.idsite | NULL
content.date_on <= $TODAY   // ó parámetro date
content.date_off >= $TODAY   // ó parámetro date
content.status = 1
content.deleted = 0

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/contents/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/contents/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/contents/orders`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `date` |  optional  | array

<!-- END_89d2f9aaad9dbb854646769a946d5d81 -->

#Country management


<!-- START_d29e58b86f0be3bca379f56f5148e693 -->
## Creación de un nuevo country
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/country" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"tempore"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/country"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "tempore"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/country`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
    
<!-- END_d29e58b86f0be3bca379f56f5148e693 -->

<!-- START_a522ae656251e9662bec61fe849d69f2 -->
## Actualización de un country
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/country/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"rem"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/country/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "rem"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/country/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcountry` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
    
<!-- END_a522ae656251e9662bec61fe849d69f2 -->

<!-- START_25fb8b7266d70efa89d2403009be3a8f -->
## Borrado de un country de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/country/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/country/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/country/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcountry` |  required  | 

<!-- END_25fb8b7266d70efa89d2403009be3a8f -->

<!-- START_8bc439ca454d807702c53cf6bf3279b5 -->
## Acceso a todos los countries de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/countries/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/countries/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/countries/toselect`


<!-- END_8bc439ca454d807702c53cf6bf3279b5 -->

<!-- START_3e786ac177dd575265fe4cd50524d7d4 -->
## Aceso a los datos de un country
Esta acción sólo superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/country/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/country/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/country/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcountry` |  optional  | int required

<!-- END_3e786ac177dd575265fe4cd50524d7d4 -->

<!-- START_92ff514be156eae68b06801a0bf6a843 -->
## Acceso a todos los countries de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/countries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/countries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/countries`


<!-- END_92ff514be156eae68b06801a0bf6a843 -->

#Customer management


<!-- START_93d8b49a2286546c00698cc1dd90dd8c -->
## Creación de un nuevo cliente
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/customer" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"quo","email":"dolor","phone":"aliquam","gaid":"quas","image_url":"nulla","has_sales":false,"has_influxes":false,"status":true,"deleted":true}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "quo",
    "email": "dolor",
    "phone": "aliquam",
    "gaid": "quas",
    "image_url": "nulla",
    "has_sales": false,
    "has_influxes": false,
    "status": true,
    "deleted": true
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/customer`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `email` | string |  optional  | 
        `phone` | string |  optional  | 
        `gaid` | string |  optional  | 
        `image_url` | File |  optional  | 
        `has_sales` | boolean |  optional  | 
        `has_influxes` | boolean |  optional  | 
        `status` | boolean |  optional  | 
        `deleted` | boolean |  optional  | 
    
<!-- END_93d8b49a2286546c00698cc1dd90dd8c -->

<!-- START_ee66bef176e3b9106b721dbc76167205 -->
## Actualización de un cliente
Esta acción sólo la puede realizar el superadmin y el admin del mismo cliente

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/customer/eum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"quia","email":"quia","phone":"et","gaid":"sed","image_url":"sunt","has_sales":false,"has_influxes":false,"status":false,"deleted":false}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer/eum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "quia",
    "email": "quia",
    "phone": "et",
    "gaid": "sed",
    "image_url": "sunt",
    "has_sales": false,
    "has_influxes": false,
    "status": false,
    "deleted": false
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/customer/{idcustomer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcustomer` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
        `email` | string |  optional  | 
        `phone` | string |  optional  | 
        `gaid` | string |  optional  | 
        `image_url` | File |  optional  | 
        `has_sales` | boolean |  optional  | 
        `has_influxes` | boolean |  optional  | 
        `status` | boolean |  optional  | 
        `deleted` | boolean |  optional  | 
    
<!-- END_ee66bef176e3b9106b721dbc76167205 -->

<!-- START_f0b723096fd8fc35c25d59e2e2079ec5 -->
## Borrado de un cliente de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/customer/tempore" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer/tempore"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/customer/{idcustomer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcustomer` |  required  | 

<!-- END_f0b723096fd8fc35c25d59e2e2079ec5 -->

<!-- START_ccca5bea82e0951d904b162d4beb4fb3 -->
## Aceso a los datos de un cliente
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idcustomer, solo pueden visualizar &quot;customers&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/customer/laboriosam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer/laboriosam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/customer/{idcustomer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcustomer` |  optional  | int required

<!-- END_ccca5bea82e0951d904b162d4beb4fb3 -->

<!-- START_2fe1c389d01f9fa9f08d473c64194075 -->
## Acceso a todos los clientes de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/customers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/customers`


<!-- END_2fe1c389d01f9fa9f08d473c64194075 -->

<!-- START_a512bec9d5dd2c5545e1170469e33428 -->
## Acceso a todos los customers de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Debe devolver el listado de todos los customers filtrados por: status = 1 delete = 0 idcustomer de usuario (solo si usuario != superadmin)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/customers/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customers/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/customers/toselect`


<!-- END_a512bec9d5dd2c5545e1170469e33428 -->

<!-- START_173ec5d307c2f6ba88b6574c8caa0f1b -->
## Borra la  relacion en customer_format
Esta acción la puede realizar cualquier usuario
Devuelve customer con sus formats

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/customer/ut/format/ipsum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer/ut/format/ipsum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/customer/{idcustomer}/format/{idformat}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcustomer` |  required  | int
    `idformat` |  required  | int

<!-- END_173ec5d307c2f6ba88b6574c8caa0f1b -->

<!-- START_1b0a19e6087b50cae6b65338c610c33b -->
## Actualización de los format de un customer
Esta acción la puede realizar cualquier usuario
Debe devolver el customer con todos los nuevos format.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/customer/dolorem/formats" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"formats":"nisi"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/customer/dolorem/formats"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "formats": "nisi"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/customer/{idcustomer}/formats`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcustomer` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `formats` | required |  optional  | array idformat integer required
    
<!-- END_1b0a19e6087b50cae6b65338c610c33b -->

#EmissionSale Consult


<!-- START_ddca907e5d2d98293a63782bdaa205f2 -->
## Consulta datos para las gráficas de emisión y ventas
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/emission-sales/data" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"et","idcustomer":"sint","apikey":"delectus"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/emission-sales/data"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "et",
    "idcustomer": "sint",
    "apikey": "delectus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/emission-sales/data`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `idcustomer` | string |  required  | 
        `apikey` | string |  required  | 
    
<!-- END_ddca907e5d2d98293a63782bdaa205f2 -->

#Format Player management


<!-- START_72ead95567e1d4aa89ed8b75371043d8 -->
## Creación de un nuevo formato de player
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/format" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"aut","image_url":"quisquam","description":"a"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/format"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "aut",
    "image_url": "quisquam",
    "description": "a"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/format`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `image_url` | File |  required  | 
        `description` | text |  optional  | 
    
<!-- END_72ead95567e1d4aa89ed8b75371043d8 -->

<!-- START_989d4f223508cfb083b11b19ecc825af -->
## Actualización de un formato de player
Esta acción sólo la puede realizar el superadmin y el admin del mismo cliente

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/format/quaerat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"et","image":"sapiente","description":"omnis"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/format/quaerat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "et",
    "image": "sapiente",
    "description": "omnis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/format/{idformat}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idformat` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
        `image` | File |  optional  | 
        `description` | text |  optional  | 
    
<!-- END_989d4f223508cfb083b11b19ecc825af -->

<!-- START_07642d92d9c700fe11f34a612acc7db6 -->
## Borrado de un formato de player de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/format/non" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/format/non"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/format/{idformat}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idformat` |  required  | 

<!-- END_07642d92d9c700fe11f34a612acc7db6 -->

<!-- START_206279f23a3778489289466fdd0d20b8 -->
## Acceso a todos los formats de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/formats/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/formats/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/formats/toselect`


<!-- END_206279f23a3778489289466fdd0d20b8 -->

<!-- START_c4cbc11649f6da080ac04fb08e2fbc69 -->
## Aceso a los datos de un formato de player
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/format/molestiae" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/format/molestiae"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/format/{idformat}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idformat` |  optional  | int required

<!-- END_c4cbc11649f6da080ac04fb08e2fbc69 -->

<!-- START_92c67ebf68ee3aa01e2d9222f37dcb0f -->
## Acceso a todos los formatos de player de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/formats" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/formats"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/formats`


<!-- END_92c67ebf68ee3aa01e2d9222f37dcb0f -->

#Impact Consult


<!-- START_556e2881809544a0d9670eb7c138dd80 -->
## Llamada para recoger datos resumen de Emisiones y ventas
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/impact/resumen" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"nihil","year":"quidem","month":"ratione"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/impact/resumen"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "nihil",
    "year": "quidem",
    "month": "ratione"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/impact/resumen`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  required  | 
        `year` | string |  required  | 
        `month` | string |  required  | 
    
<!-- END_556e2881809544a0d9670eb7c138dd80 -->

#IndoorLocation management


<!-- START_13a0446460ae1432d00a250995f7514a -->
## Acceso a todos los contentcategories de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/indoorlocations/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocations/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/indoorlocations/toselect`


<!-- END_13a0446460ae1432d00a250995f7514a -->

<!-- START_5e9d8b6df13272af5bcdaf37b0322a8c -->
## Creación de un nuevo indoor location
Esta acción sólo la puede realizar el superadmin, admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/indoorlocation" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"veritatis","name":"iure"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocation"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "veritatis",
    "name": "iure"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/indoorlocation`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  optional  | 
        `name` | string |  required  | 
    
<!-- END_5e9d8b6df13272af5bcdaf37b0322a8c -->

<!-- START_e49741f00a856fa323ad011a3619eef6 -->
## Actualización de un indoor location
Esta acción sólo la puede realizar el superadmin,admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/indoorlocation/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"non","name":"et"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocation/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "non",
    "name": "et"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/indoorlocation/{idlocation}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  optional  | 
        `name` | string |  optional  | 
    
<!-- END_e49741f00a856fa323ad011a3619eef6 -->

<!-- START_fab00fcfd1d6c1cf5d9167a2c372a7cf -->
## Borrado de un indoor location de la base de datos
Esta acción sólo la puede realizar el superadmin, admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/indoorlocation/voluptas" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocation/voluptas"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/indoorlocation/{idlocation}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idlocation` |  required  | 

<!-- END_fab00fcfd1d6c1cf5d9167a2c372a7cf -->

<!-- START_eb0c7b0337b044d179dd14f86a26e58c -->
## Acceso a los datos de un indoor location
Esta acción sólo la puede realizar el superadmin, el admin de los indoor location que pertenecen a su customer o no pertenecen a ninún customer y el user a los que pertenecen a su site o no pertenecen a ningún customer

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/indoorlocation/omnis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocation/omnis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/indoorlocation/{idlocation}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idlocation` |  optional  | int required

<!-- END_eb0c7b0337b044d179dd14f86a26e58c -->

<!-- START_8056177b530d73ac05857c078e9796fa -->
## Acceso a todos los indoor location de la base de datos
Esta acción sólo la puede realizar el superadmin, el admin verá el listado de indoor location pertenecientes a su customer

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/indoorlocations" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocations"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/indoorlocations`


<!-- END_8056177b530d73ac05857c078e9796fa -->

<!-- START_813c60b2bf47f2bcf146f39a2440026e -->
## Debe eliminar:registro en indoor_locations (idlocation) play_areas (idlocation) contents_has_areas (idarea) contents_order (idarea) play_logics (idarea) players (idarea)
Esta acción la puede realizar cualquier usuario
Devuelve indoorlocation

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/indoorlocation/ut/force" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/indoorlocation/ut/force"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/indoorlocation/{idlocation}/force`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idlocation` |  required  | int

<!-- END_813c60b2bf47f2bcf146f39a2440026e -->

#Lang management


<!-- START_30a1d5b37ad6546e9c44b0ed606f3759 -->
## Creación de un nuevo lang
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/lang" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"porro","image_url":"molestias"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/lang"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "porro",
    "image_url": "molestias"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/lang`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `image_url` | File |  required  | 
    
<!-- END_30a1d5b37ad6546e9c44b0ed606f3759 -->

<!-- START_8c043bcc4422d435fbe1594df6bda8ff -->
## Actualización de un lang
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/lang/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idlang":"quisquam","name":"commodi","image_url":"iusto"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/lang/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idlang": "quisquam",
    "name": "commodi",
    "image_url": "iusto"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/lang/{idlang}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idlang` | string |  optional  | 
        `name` | string |  optional  | 
        `image_url` | File |  optional  | 
    
<!-- END_8c043bcc4422d435fbe1594df6bda8ff -->

<!-- START_7d239ad08b7db995e5a515d01735bfb8 -->
## Borrado de un lang de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/lang/fugit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/lang/fugit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/lang/{idlang}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idlang` |  required  | 

<!-- END_7d239ad08b7db995e5a515d01735bfb8 -->

<!-- START_c9b61a902fc67dd7d38db9baeec79c7b -->
## Acceso a todos los langs de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/langs/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/langs/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/langs/toselect`


<!-- END_c9b61a902fc67dd7d38db9baeec79c7b -->

<!-- START_ecfe2d5f156c15b6266c4a57117e956b -->
## Acceso a los datos de un lang
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/lang/et" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/lang/et"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/lang/{idlang}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idlang` |  optional  | int required

<!-- END_ecfe2d5f156c15b6266c4a57117e956b -->

<!-- START_f305fd31b1a9fac6f38c8bbd1489a28b -->
## Acceso a todos los langs de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/langs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/langs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/langs`


<!-- END_f305fd31b1a9fac6f38c8bbd1489a28b -->

#Password reset


<!-- START_dbde307dba12aec940f4e130f90b2fe5 -->
## Llamada para enviar el correo al usuario con la ruta para cambiar la contraseña.

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/public/remember" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"temporibus"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/public/remember"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "temporibus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST public/remember`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | email |  required  | 
    
<!-- END_dbde307dba12aec940f4e130f90b2fe5 -->

<!-- START_7bdc5795a08b5993e8e291ab28cc2982 -->
## Llamada para actualizar la contraseña del usuario.

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/public/update" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"token":"aut","passwword":"assumenda","c_password":"dolorum"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/public/update"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "token": "aut",
    "passwword": "assumenda",
    "c_password": "dolorum"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT public/update`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | Token de petición de reseteo de contraseña
        `passwword` | string |  required  | 
        `c_password` | string |  required  | 
    
<!-- END_7bdc5795a08b5993e8e291ab28cc2982 -->

#Permission management


<!-- START_5e1e7bac0e6fbefd50a949540ea52b78 -->
## Creación de un nuevo permiso
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"est"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "est"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/permission`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
    
<!-- END_5e1e7bac0e6fbefd50a949540ea52b78 -->

<!-- START_e40ff0b17fc65358d440a867e5212b0e -->
## Actualización de un permiso
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/permission/ea" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"omnis"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/permission/ea"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "omnis"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/permission/{idpermission}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idpermission` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
    
<!-- END_e40ff0b17fc65358d440a867e5212b0e -->

<!-- START_180328b9c59864d37cfb7bf419d258df -->
## Borrado de un permiso de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/permission/distinctio" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/permission/distinctio"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/permission/{idpermission}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idpermission` |  required  | 

<!-- END_180328b9c59864d37cfb7bf419d258df -->

<!-- START_5d792b726a7af1be1f49d6ff59b5deaf -->
## Aceso a los datos de un permiso
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/permission/consequuntur" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/permission/consequuntur"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/permission/{idpermission}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idpermission` |  optional  | int required

<!-- END_5d792b726a7af1be1f49d6ff59b5deaf -->

<!-- START_33be27bb1c5a6d6a8544a9f6893ff83c -->
## Acceso a todos los permisos de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/permissions" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/permissions"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/permissions`


<!-- END_33be27bb1c5a6d6a8544a9f6893ff83c -->

#PlayArea management


<!-- START_b94ef6610150c8bff9fbf9377695356d -->
## Creación de un nuevo playArea
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/playarea" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idlocation":"voluptas","location":{},"idformat":"possimus","format":{}}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idlocation": "voluptas",
    "location": {},
    "idformat": "possimus",
    "format": {}
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/playarea`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idlocation` | string |  optional  | 
        `location` | object |  optional  | 
        `idformat` | string |  optional  | 
        `format` | object |  optional  | 
    
<!-- END_b94ef6610150c8bff9fbf9377695356d -->

<!-- START_35cadf2c791ad8549868fd2eb77f153f -->
## Actualización de un playArea
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playarea/vero" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idlocation":"quis","location":{},"idformat":"aspernatur","format":{}}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/vero"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idlocation": "quis",
    "location": {},
    "idformat": "aspernatur",
    "format": {}
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playarea/{idarea}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idlocation` | string |  optional  | 
        `location` | object |  optional  | 
        `idformat` | string |  optional  | 
        `format` | object |  optional  | 
    
<!-- END_35cadf2c791ad8549868fd2eb77f153f -->

<!-- START_e77c8c94952bcd74921a70e075e194fd -->
## Borrado de un playArea de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playarea/enim" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/enim"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playarea/{idarea}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | 

<!-- END_e77c8c94952bcd74921a70e075e194fd -->

<!-- START_24c2bfa5f2f748bb7348880015a961d9 -->
## Acceso a los datos de un playArea
Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user de la misma tienda

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playarea/commodi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/commodi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playarea/{idarea}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  optional  | int required

<!-- END_24c2bfa5f2f748bb7348880015a961d9 -->

<!-- START_cebb9171711287672ffc61a6ac376ed5 -->
## Acceso a todos los playAreas de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playareas" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playareas"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playareas`


<!-- END_cebb9171711287672ffc61a6ac376ed5 -->

<!-- START_b43b84fae16d96152d9a117b42d34569 -->
## Acceso a todos los playAreas de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playareas/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playareas/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playareas/toselect`


<!-- END_b43b84fae16d96152d9a117b42d34569 -->

<!-- START_edeb700dae57a3fb5170938c31d51202 -->
## Actualización de los playlogic de un playarea
Esta acción la puede realizar cualquier usuario
Debe devolver el playarea con todos los nuevos playlogic.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playarea/maiores/playlogics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"playlogics":"itaque"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/maiores/playlogics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "playlogics": "itaque"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playarea/{idarea}/playlogics`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `playlogics` | required |  optional  | array idlogic integer required
    
<!-- END_edeb700dae57a3fb5170938c31d51202 -->

<!-- START_a9e4d398939d9e5b7c171db71f302061 -->
## Borra una relación playlogic/playarea
Esta acción la puede realizar cualquier usuario
Devuelve playarea con nuevos playlogic

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playarea/esse/playlogic/suscipit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/esse/playlogic/suscipit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playarea/{idarea}/playlogic/{idlogic}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | int
    `idlogic` |  required  | int

<!-- END_a9e4d398939d9e5b7c171db71f302061 -->

<!-- START_a54719ee0434c5ed343401dabb4174d3 -->
## Acceso a todos los playAreas de un usuario
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playareas/touser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playareas/touser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playareas/touser`


<!-- END_a54719ee0434c5ed343401dabb4174d3 -->

<!-- START_0bf8adf74a269f7146b9c16b623ecdb9 -->
## Actualización de los area order de contents
Esta acción la puede realizar superadmin y admin
Debe devolver el nuevo content order del area

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playarea/laboriosam/contents/rerum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"contents":"occaecati"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/laboriosam/contents/rerum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "contents": "occaecati"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playarea/{idarea}/contents/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | int
    `idcategory` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `contents` | required |  optional  | array idcontent integer required
    
<!-- END_0bf8adf74a269f7146b9c16b623ecdb9 -->

<!-- START_654f230a7741cab3605dbab8178b0faa -->
## Debe eliminar: play_areas (idarea) / contents_has_areas (idarea) / contents_order (idarea) / play_logics (idarea) / players (idarea)
Esta acción la puede realizar cualquier usuario
Devuelve playarea

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playarea/et/force" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playarea/et/force"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playarea/{idarea}/force`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idarea` |  required  | int

<!-- END_654f230a7741cab3605dbab8178b0faa -->

<!-- START_4b4c77943ba9b83195f53487968af7ba -->
## Acceso a todos los formats de los players asociados al usuario
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/formats/touser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/formats/touser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/formats/touser`


<!-- END_4b4c77943ba9b83195f53487968af7ba -->

<!-- START_250ef0a67c14cc9fca1222e724a5d701 -->
## Acceso a todos los langs de los players asociados al usuario
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/langs/touser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/langs/touser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/langs/touser`


<!-- END_250ef0a67c14cc9fca1222e724a5d701 -->

#PlayCircuit management


<!-- START_6b6edcc041e68144f4cb9ad53a195105 -->
## Creación de un nuevo playcircuit
Esta acción la puede realizar superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/playcircuit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"exercitationem","image":"cupiditate","idcustomer":"et"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "exercitationem",
    "image": "cupiditate",
    "idcustomer": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/playcircuit`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | required |  optional  | string
        `image` | File |  optional  | 
        `idcustomer` | required |  optional  | int
    
<!-- END_6b6edcc041e68144f4cb9ad53a195105 -->

<!-- START_e0350d012fa3afdae5209b30dd85173b -->
## Actualización de un playcircuit
Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/playcircuit/nihil" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"image":"eum","status":"sunt"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/nihil"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "image": "eum",
    "status": "sunt"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/playcircuit/{idcircuit}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `image` | File |  optional  | 
        `status` | string |  optional  | 
    
<!-- END_e0350d012fa3afdae5209b30dd85173b -->

<!-- START_11f5822417dc6b52572dcf9058c4ce1f -->
## Borrado de un playcircuit de la base de datos
Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/quod" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/quod"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | 

<!-- END_11f5822417dc6b52572dcf9058c4ce1f -->

<!-- START_0332c7d03f1081b40abf7ef790cf6dcf -->
## Acceso a los datos de un playcircuit
Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playcircuit/sapiente" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/sapiente"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playcircuit/{idcircuit}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  optional  | int required

<!-- END_0332c7d03f1081b40abf7ef790cf6dcf -->

<!-- START_cdf55644ea5a506ab53789f32570cb6e -->
## Acceso a todos los playcircuits de la base de datos
Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playcircuits" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuits"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playcircuits`


<!-- END_cdf55644ea5a506ab53789f32570cb6e -->

<!-- START_aa9479fdc8bdc2fb92991aa8ffa3d96f -->
## Acceso a los playcircuits con status 1 y deleted 0 de la base de datos
Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playcircuits/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuits/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playcircuits/toselect`


<!-- END_aa9479fdc8bdc2fb92991aa8ffa3d96f -->

<!-- START_9ddad426e278ae5ee7095151c89b3798 -->
## Actualización de los tags de un circuito
Esta acción la puede realizar cualquier usuario
Debe devolver el playcircuit con los nuevos tags.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/voluptatem/tags" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"tags":"et"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/voluptatem/tags"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "tags": "et"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/tags`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `tags` | required |  optional  | array idtag integer required
    
<!-- END_9ddad426e278ae5ee7095151c89b3798 -->

<!-- START_523671cd8cfe78be6472dfccd14f8dd3 -->
## Borra una relación playcircuit/tag
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos tags

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/animi/tag/officiis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/animi/tag/officiis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idtag` |  required  | int

<!-- END_523671cd8cfe78be6472dfccd14f8dd3 -->

<!-- START_5115db07987396fb909fce12028ba3d5 -->
## Actualización de los countries de un circuito
Esta acción la puede realizar cualquier usuario
Debe devolver el playcircuit con los nuevos countries.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/sunt/countries" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"countries":"consectetur"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/sunt/countries"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "countries": "consectetur"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/countries`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `countries` | required |  optional  | array idcountry integer required
    
<!-- END_5115db07987396fb909fce12028ba3d5 -->

<!-- START_ae810d00c14c670ff185015dea36e13f -->
## Borra una relación playcircuit/country
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos countries

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/ipsa/country/consequatur" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/ipsa/country/consequatur"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/country/{idcountry}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idcountry` |  required  | int

<!-- END_ae810d00c14c670ff185015dea36e13f -->

<!-- START_fdba69603a6e7805c53eaccad6761fb2 -->
## Actualización de las provinces de un circuito
Esta acción la puede realizar cualquier usuario
Debe devolver el playcircuit con las nuevas provinces.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/ut/provinces" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"provinces":"qui"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/ut/provinces"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "provinces": "qui"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/provinces`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `provinces` | required |  optional  | array idprovince integer required
    
<!-- END_fdba69603a6e7805c53eaccad6761fb2 -->

<!-- START_6ca4cc25ad8c0dad157f02e5a2ce5159 -->
## Borra una relación playcircuit/province
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos provinces

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/nulla/province/qui" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/nulla/province/qui"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/province/{idprovince}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idprovince` |  required  | int

<!-- END_6ca4cc25ad8c0dad157f02e5a2ce5159 -->

<!-- START_2b4c2a92bbacbeed487e97d4b15e6b2a -->
## Actualización de los cities de un circuito
Esta acción la puede realizar cualquier usuario
Debe devolver el playcircuit con los nuevos cities.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/possimus/cities" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"cities":"asperiores"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/possimus/cities"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "cities": "asperiores"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/cities`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `cities` | required |  optional  | array idcity integer required
    
<!-- END_2b4c2a92bbacbeed487e97d4b15e6b2a -->

<!-- START_17f54b40df16b901cd2d59884a28bfd0 -->
## Borra una relación playcircuit/city
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos cities

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/corporis/city/blanditiis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/corporis/city/blanditiis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/city/{idcity}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idcity` |  required  | int

<!-- END_17f54b40df16b901cd2d59884a28bfd0 -->

<!-- START_245e041ad0f7a250e9b57b3e60782c07 -->
## Actualización de los sites de un circuito
Esta acción la puede realizar cualquier usuario
Debe devolver el playcircuit con los nuevos sites.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/eaque/sites" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"sites":"ipsum"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/eaque/sites"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "sites": "ipsum"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/sites`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `sites` | required |  optional  | array idcircuit integer required
    
<!-- END_245e041ad0f7a250e9b57b3e60782c07 -->

<!-- START_5433f1abe0ec7dabdab982473c05ddea -->
## Borra una relación playcircuit/site
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos sites

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/consequatur/site/explicabo" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/consequatur/site/explicabo"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/site/{idsite}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idsite` |  required  | int

<!-- END_5433f1abe0ec7dabdab982473c05ddea -->

<!-- START_49d90812632b845f49569127c7f19fe0 -->
## Actualización de los langs de un circuito
Esta acción la puede realizar superadmin y admin
Debe devolver el playcircuit con los nuevos langs.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playcircuit/vel/langs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"langs":"inventore"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/vel/langs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "langs": "inventore"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playcircuit/{idcircuit}/langs`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `langs` | required |  optional  | array idcircuit integer required
    
<!-- END_49d90812632b845f49569127c7f19fe0 -->

<!-- START_fef3abfcf2a05a1e79675062b568d131 -->
## Borra una relación playcircuit/lang
Esta acción la puede realizar cualquier usuario
Devuelve circuitos con nuevos superadmin y admin

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playcircuit/est/lang/corrupti" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playcircuit/est/lang/corrupti"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playcircuit/{idcircuit}/lang/{idlang}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idcircuit` |  required  | int
    `idlang` |  required  | int

<!-- END_fef3abfcf2a05a1e79675062b568d131 -->

#PlayLogic management


<!-- START_1ce444e211e2e60429c282e03b84e76c -->
## Creación de un nuevo playlogic
Esta acción la puede realizar el superadmin, admin
Los usuarios con idcustomer, solo pueden crear &quot;playlogics&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/playlogic" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":1,"idarea":17,"idtype":9,"idcategory":11,"order":9}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogic"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 1,
    "idarea": 17,
    "idtype": 9,
    "idcategory": 11,
    "order": 9
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/playlogic`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  required  | 
        `idarea` | integer |  required  | 
        `idtype` | integer |  required  | 
        `idcategory` | integer |  optional  | 
        `order` | integer |  optional  | 
    
<!-- END_1ce444e211e2e60429c282e03b84e76c -->

<!-- START_13d9ee5c9353ea290573e87260a1f471 -->
## Actualización de un playlogic
Esta acción sólo la puede realizar el superadmin, admin
Los usuarios con idcustomer, solo pueden editar &quot;playlogics&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/playlogic/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":10,"idarea":8,"idtype":6,"idcategory":12,"order":16}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogic/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": 10,
    "idarea": 8,
    "idtype": 6,
    "idcategory": 12,
    "order": 16
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/playlogic/{idplaylogic}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | integer |  required  | 
        `idarea` | integer |  required  | 
        `idtype` | integer |  required  | 
        `idcategory` | integer |  optional  | 
        `order` | integer |  optional  | 
    
<!-- END_13d9ee5c9353ea290573e87260a1f471 -->

<!-- START_9305ddf62619a5e7da529f595e4e9200 -->
## Borrado de un playlogic de la base de datos
Esta acción sólo la puede realizar el superadmin, admin
Los usuarios con idcustomer, solo pueden eliminar &quot;playlogics&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/playlogic/repudiandae" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogic/repudiandae"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/playlogic/{idplaylogic}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplaylogic` |  required  | 

<!-- END_9305ddf62619a5e7da529f595e4e9200 -->

<!-- START_1e1b4ee159ca3652556d2fc32c66c33a -->
## Acceso a los datos de un playlogic
Esta acción sólo la puede realizar el superadmin, admin  y user
Los usuarios con idcustomer, solo pueden visualizar &quot;playlogics&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playlogic/eius" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogic/eius"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playlogic/{idplaylogic}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplaylogic` |  optional  | int required

<!-- END_1e1b4ee159ca3652556d2fc32c66c33a -->

<!-- START_8a1bebaa5ff6a61a2dce84f3f2f443ba -->
## Acceso a todos los playlogics de la base de datos
Esta acción sólo la puede realizar el superadmin, admin
idcustomer de usuario (solo si usuario tiene idcustomer)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playlogics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playlogics`


<!-- END_8a1bebaa5ff6a61a2dce84f3f2f443ba -->

<!-- START_1f782713424a5536dae6115e0ff6f1f3 -->
## Acceso a todos los playlogics de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/playlogics/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/playlogics/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/playlogics/toselect`


<!-- END_1f782713424a5536dae6115e0ff6f1f3 -->

#Player management


<!-- START_161635f5651d09e06835c6b23174dbf9 -->
## Creación de un nuevo player
Esta acción sólo la puede realizar el superadmin, admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
admin -> Solo los Players con site.idcustomer = admin.idcustomer

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/player" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"numquam","idsite":10,"idarea":17,"idlang":13,"idtemplate":11}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "numquam",
    "idsite": 10,
    "idarea": 17,
    "idlang": 13,
    "idtemplate": 11
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/player`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | 
        `idsite` | integer |  required  | 
        `idarea` | integer |  required  | 
        `idlang` | integer |  required  | 
        `idtemplate` | integer |  required  | 
    
<!-- END_161635f5651d09e06835c6b23174dbf9 -->

<!-- START_2c274150a1429368632d54f7fa5d827c -->
## Actualización de un player
Esta acción sólo la puede realizar el superadmin, el admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
admin -> Solo los Players con site.idcustomer = admin.idcustomer

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/player/saepe" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"et","status":"dolores","idsite":2,"idarea":15,"idlang":15,"idtemplate":15}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/saepe"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "et",
    "status": "dolores",
    "idsite": 2,
    "idarea": 15,
    "idlang": 15,
    "idtemplate": 15
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/player/{idplayer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  optional  | 
        `status` | string |  optional  | 
        `idsite` | integer |  optional  | 
        `idarea` | integer |  optional  | 
        `idlang` | integer |  optional  | 
        `idtemplate` | integer |  optional  | 
    
<!-- END_2c274150a1429368632d54f7fa5d827c -->

<!-- START_adb73094c131ea360152d9d4836733f4 -->
## Borrado de un player de la base de datos
Esta acción sólo la puede realizar el superadmin, admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
admin -> Solo los Players con site.idcustomer = admin.idcustomer

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/player/accusantium" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/accusantium"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/player/{idplayer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | 

<!-- END_adb73094c131ea360152d9d4836733f4 -->

<!-- START_989ca8b51177bea22f1d34ebd5d626d1 -->
## Acceso a los datos de un player
Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
admin -> Solo los Players con site.idcustomer = admin.idcustomer
user -> Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/player/alias" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/alias"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/player/{idplayer}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  optional  | int required

<!-- END_989ca8b51177bea22f1d34ebd5d626d1 -->

<!-- START_1fddaa5a3615e39924ce0b76d98a69a8 -->
## Acceso a todos los players de la base de datos
Esta acción sólo la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
admin -> Solo los Players con site.idcustomer = admin.idcustomer
user -> Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/players" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/players"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/players`


<!-- END_1fddaa5a3615e39924ce0b76d98a69a8 -->

<!-- START_f736e627403bf9f61c06a476ecfc02f7 -->
## Acceso a los players para estadísticas
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Filtrar por deleted = 0;
superadmin -> TODOS
admin -> Solo los Players con site.idcustomer = admin.idcustomer
user ->  Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/players/toanalytics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/players/toanalytics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/players/toanalytics`


<!-- END_f736e627403bf9f61c06a476ecfc02f7 -->

<!-- START_4bcb87e9c49b00181050a635323aedbc -->
## Acceso a todos los player de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/players/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/players/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/players/toselect`


<!-- END_4bcb87e9c49b00181050a635323aedbc -->

<!-- START_6bf0df511fbae69932219bee7a5e8647 -->
## Actualización de los playlogic de un player
Esta acción la puede realizar cualquier usuario
Debe devolver el player con todos los nuevos playlogic.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/player/quo/playlogics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"playlogics":"dolorum"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/quo/playlogics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "playlogics": "dolorum"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/player/{idplayer}/playlogics`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `playlogics` | required |  optional  | array idlogic integer required
    
<!-- END_6bf0df511fbae69932219bee7a5e8647 -->

<!-- START_9046bc448b15dc1640d86cc4b693e562 -->
## Borra una relación playlogic/player
Esta acción la puede realizar cualquier usuario
Devuelve player con nuevos playlogic

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/players/et/playlogic/deleniti" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/players/et/playlogic/deleniti"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/players/{idplayer}/playlogic/{idlogic}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
    `idlogic` |  required  | int

<!-- END_9046bc448b15dc1640d86cc4b693e562 -->

<!-- START_3e90779574a9b37287d02f54334b1148 -->
## Actualización de los player order de contents
Esta acción la puede realizar superadmin y admin
Debe devolver el nuevo content order del player

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/player/iure/contents/est" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"contents":"qui"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/iure/contents/est"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "contents": "qui"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/player/{idplayer}/contents/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
    `idcategory` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `contents` | required |  optional  | array idcontent integer required
    
<!-- END_3e90779574a9b37287d02f54334b1148 -->

<!-- START_d49102eb8a2a8b535628f22c6fcb035d -->
## Actualización de los tag de un player
Esta acción la puede realizar cualquier usuario
Debe devolver el player con todos los nuevos tag.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/player/voluptatibus/tags" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"tags":"fugit"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/voluptatibus/tags"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "tags": "fugit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/player/{idplayer}/tags`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `tags` | required |  optional  | array idtag integer required
    
<!-- END_d49102eb8a2a8b535628f22c6fcb035d -->

<!-- START_e60cc875e2a14cf1ba953f801aafcea5 -->
## Borra una relación tag/player
Esta acción la puede realizar cualquier usuario
Devuelve player con nuevos tag

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/player/nihil/tag/quibusdam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/nihil/tag/quibusdam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/player/{idplayer}/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
    `idtag` |  required  | int

<!-- END_e60cc875e2a14cf1ba953f801aafcea5 -->

<!-- START_945029777f214621b731e9e09c1339fc -->
## Actualización de los power de un player
Esta acción la puede realizar cualquier usuario
Debe devolver el player con todos los nuevos power.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/player/dolor/powers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"powers":"qui"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/dolor/powers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "powers": "qui"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/player/{idplayer}/powers`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `powers` | required |  optional  | array idpower integer required
    
<!-- END_945029777f214621b731e9e09c1339fc -->

<!-- START_d98e91edb801512e7f38e1adba91539b -->
## Borra una relación power/player
Esta acción la puede realizar cualquier usuario
Devuelve player con nuevos power

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/player/nam/power/occaecati" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/player/nam/power/occaecati"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/player/{idplayer}/power/{idpower}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idplayer` |  required  | int
    `idpower` |  required  | int

<!-- END_d98e91edb801512e7f38e1adba91539b -->

#Province management


<!-- START_fee62b48ef6f9949bc0b1c6e16aaebd5 -->
## Acceso a todos las provinces de la base de datos desde un select By Country
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/provinces/toselect/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/provinces/toselect/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/provinces/toselect/{idprovince}`


<!-- END_fee62b48ef6f9949bc0b1c6e16aaebd5 -->

<!-- START_32b667c472274d20b5e45cfbb2d57a83 -->
## Acceso a todos las provinces de la base de datos desde un select
Esta acción la puede realizar cualquier usuario

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/provinces/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/provinces/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/provinces/toselect`


<!-- END_32b667c472274d20b5e45cfbb2d57a83 -->

#Recomendations Consult


<!-- START_90b884b004a8d590a47bf217552053a8 -->
## Consulta para recoger las categorías de la sección recomendaciones/asociaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/recomendations/associations/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/associations/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/recomendations/associations/categories`


<!-- END_90b884b004a8d590a47bf217552053a8 -->

<!-- START_2a66db148306726a6f43c4e6d29279fa -->
## Consulta para recoger los datos de la sección recomendaciones/asociaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/recomendations/associations/conditional-probability/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/associations/conditional-probability/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/recomendations/associations/conditional-probability/{idcategory}`


<!-- END_2a66db148306726a6f43c4e6d29279fa -->

<!-- START_4c7a66f4c76ec47962fa65af24952de7 -->
## Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/recomendations/recomendations/categories/ea" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/recomendations/categories/ea"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/recomendations/recomendations/categories/{month}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `month` |  optional  | integer required

<!-- END_4c7a66f4c76ec47962fa65af24952de7 -->

<!-- START_612e78dfda9212756b512e5d83262a7c -->
## Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-patron-ventas" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-patron-ventas"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/recomendations/recomendations/get-patron-ventas`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `month` |  optional  | integer required

<!-- END_612e78dfda9212756b512e5d83262a7c -->

<!-- START_740074b2e0de9e10a017f558e0468ebf -->
## Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-filas-dias-semana" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-filas-dias-semana"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/recomendations/recomendations/get-filas-dias-semana`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `month` |  optional  | integer required

<!-- END_740074b2e0de9e10a017f558e0468ebf -->

<!-- START_6941260e025f2f58d08e36c18eb866b1 -->
## Consulta para recoger la temperatura de la la categoría seleccionada en recomendaciones/recomendaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-temperature" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-temperature"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/recomendations/recomendations/get-temperature`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `month` |  optional  | integer required

<!-- END_6941260e025f2f58d08e36c18eb866b1 -->

<!-- START_d08b69f2c31c162974535736f70bd49b -->
## Consulta para recoger los productos de la categoría seleccionada en recomendaciones/recomendaciones
Esta acción la puede realizar el superadmin, admin y user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-products-from-category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/recomendations/recomendations/get-products-from-category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/recomendations/recomendations/get-products-from-category`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `month` |  optional  | integer required

<!-- END_d08b69f2c31c162974535736f70bd49b -->

#Role management


<!-- START_3861ff673e4efc2c1749ce2be04ddf42 -->
## Creación de un nuevo rol
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"illum"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "illum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/role`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
    
<!-- END_3861ff673e4efc2c1749ce2be04ddf42 -->

<!-- START_1ba539ea376d2430d6f09a52fdb50212 -->
## Actualización de un rol
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/role/commodi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"ut"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/role/commodi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ut"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/role/{idrole}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idrole` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
    
<!-- END_1ba539ea376d2430d6f09a52fdb50212 -->

<!-- START_812eb616597a7aadda8827bc331972b5 -->
## Borrado de un rol de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/role/quis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/role/quis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/role/{idrole}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idrole` |  required  | 

<!-- END_812eb616597a7aadda8827bc331972b5 -->

<!-- START_dee2fa8ab1e346d3b7ef75afb1bdccdc -->
## Aceso a los datos de un rol
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/role/aut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/role/aut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/role/{idrole}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idrole` |  optional  | int required

<!-- END_dee2fa8ab1e346d3b7ef75afb1bdccdc -->

<!-- START_a7e9f684460b2014cca5635beb888b82 -->
## Acceso a todos los roles de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/roles" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/roles"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/roles`


<!-- END_a7e9f684460b2014cca5635beb888b82 -->

#Site management


<!-- START_64c18255ef7cca38022116025bbc039f -->
## Creación de una nueva tienda
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/site" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"dolorem","name":"iusto","idcity":"vero","phone":"sunt","email":"impedit","address":"et","zipcode":"qui","latitud":"cupiditate","longitude":"fugiat"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "dolorem",
    "name": "iusto",
    "idcity": "vero",
    "phone": "sunt",
    "email": "impedit",
    "address": "et",
    "zipcode": "qui",
    "latitud": "cupiditate",
    "longitude": "fugiat"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/site`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  required  | 
        `name` | string |  required  | 
        `idcity` | string |  required  | 
        `phone` | string |  optional  | 
        `email` | string |  optional  | 
        `address` | string |  optional  | 
        `zipcode` | string |  optional  | 
        `latitud` | string |  optional  | 
        `longitude` | string |  optional  | 
    
<!-- END_64c18255ef7cca38022116025bbc039f -->

<!-- START_d8959e94716d03fff1d6b6e985a7e0aa -->
## Actualización de una tienda
Esta acción sólo la puede realizar el superadmin o un admin del mismo idcustomer o un usuario con el mismo idsite

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/site/alias" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"rerum","name":"aut","idcity":"consequuntur","phone":"aut","email":"necessitatibus","address":"repudiandae","zipcode":"rem","latitud":"aut","longitude":"ea","status":false}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/alias"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "rerum",
    "name": "aut",
    "idcity": "consequuntur",
    "phone": "aut",
    "email": "necessitatibus",
    "address": "repudiandae",
    "zipcode": "rem",
    "latitud": "aut",
    "longitude": "ea",
    "status": false
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/site/{idsite}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idsite` |  optional  | string required
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  optional  | 
        `name` | string |  optional  | 
        `idcity` | string |  optional  | require
        `phone` | string |  optional  | 
        `email` | string |  optional  | 
        `address` | string |  optional  | 
        `zipcode` | string |  optional  | 
        `latitud` | string |  optional  | 
        `longitude` | string |  optional  | 
        `status` | boolean |  optional  | 
    
<!-- END_d8959e94716d03fff1d6b6e985a7e0aa -->

<!-- START_75041cb398c20589d272033a1050b211 -->
## Borrado de una tienda de la base de datos
Esta acción sólo la puede realizar el superadmin.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/site/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/site/{idsite}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | 

<!-- END_75041cb398c20589d272033a1050b211 -->

<!-- START_ac031285f72d89ad895e6e48f0bb351e -->
## Acceso a todas las tiendas de la base de datos
Esta acción sólo la puede realizar el superadmin, el admin accede a las tiendas de is cliente y el user a las tiendas de si tienda

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/sites" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/sites"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/sites`


<!-- END_ac031285f72d89ad895e6e48f0bb351e -->

<!-- START_0d75b1ff092088cd2a7e7e6b37962bb1 -->
## Acceso a los datos de una tienda
Esta acción sólo la puede realizar el superadmin, el admin si es de una tienda con su mismo idcustomer o un usuario con su mismo idsite

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/site/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/site/{idsite}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required

<!-- END_0d75b1ff092088cd2a7e7e6b37962bb1 -->

<!-- START_652c586ab861fd4649b6cffdbc98b149 -->
## Acceso a todos los site de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
idcustomer de usuario (solo si usuario tiene idcustomer)

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/sites/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/sites/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/sites/toselect`


<!-- END_652c586ab861fd4649b6cffdbc98b149 -->

<!-- START_be59c8312613fd39112e72aff82f9220 -->
## Actualización de los holidays de un site
Esta acción la puede realizar cualquier usuario
Debe devolver el site con todos los nuevos holidays.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/site/molestiae/holidays" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"site_holidays":"quam"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/molestiae/holidays"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "site_holidays": "quam"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/site/{idsite}/holidays`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idsite` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `site_holidays` | required |  optional  | array date integer required
    
<!-- END_be59c8312613fd39112e72aff82f9220 -->

<!-- START_db40bc1b511eef35c923656f34cc529e -->
## Borra una relación holiday/site
Esta acción la puede realizar cualquier usuario
Devuelve site con nuevos holidays

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/site/non/holiday/nisi" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/non/holiday/nisi"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/site/{idsite}/holiday/{idholiday}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idsite` |  required  | int
    `idholiday` |  required  | int

<!-- END_db40bc1b511eef35c923656f34cc529e -->

<!-- START_6a2cc6def1a19dd7f5a65d1e68121945 -->
## Actualización de los tags de un site
Esta acción la puede realizar cualquier usuario
Debe devolver el site con todos los nuevos tags.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/site/sunt/tags" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"tags":"sit"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/sunt/tags"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "tags": "sit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/site/{idsite}/tags`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idsite` |  required  | int
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `tags` | required |  optional  | array tag integer required
    
<!-- END_6a2cc6def1a19dd7f5a65d1e68121945 -->

<!-- START_f19bcb6f1ec36a977ce869b98b3531a0 -->
## Borra una relación tag/site
Esta acción la puede realizar cualquier usuario
Devuelve site con nuevos tags

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/site/vel/tag/est" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/site/vel/tag/est"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/site/{idsite}/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idsite` |  required  | int
    `idtag` |  required  | int

<!-- END_f19bcb6f1ec36a977ce869b98b3531a0 -->

#TagCategory management


<!-- START_8463fa19aff5a6a5bffa4a4baec9e712 -->
## Creación de una nueva categoría de tag
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden crear &quot;tagcategory&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/tagcategory" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"in","name":"ut"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategory"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "in",
    "name": "ut"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/tagcategory`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  optional  | 
        `name` | string |  required  | 
    
<!-- END_8463fa19aff5a6a5bffa4a4baec9e712 -->

<!-- START_8f40ab98800e7d00ac2bbb4046fdf291 -->
## Actualización de una categoría de tag
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden updatear &quot;tagcategory&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/tagcategory/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcustomer":"non","name":"ipsum"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategory/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcustomer": "non",
    "name": "ipsum"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/tagcategory/{idcategory}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcustomer` | string |  optional  | 
        `name` | string |  optional  | 
    
<!-- END_8f40ab98800e7d00ac2bbb4046fdf291 -->

<!-- START_68e4b9b6497be779b2c1e4921e3c5034 -->
## Borrado de una categoría de tag de la base de datos
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden borrar &quot;tagcategory&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/tagcategory/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategory/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/tagcategory/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | 

<!-- END_68e4b9b6497be779b2c1e4921e3c5034 -->

<!-- START_768f532c00fcb9124e9a59e04c87b45f -->
## Acceso a los datos de una categoría de tag
Esta acción sólo la puede realizar el superadmin, admin y user
Los usuarios con idcustomer, solo pueden visualizar &quot;tagcategory&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tagcategory/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategory/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tagcategory/{idcategory}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  optional  | int required

<!-- END_768f532c00fcb9124e9a59e04c87b45f -->

<!-- START_a8b4d2f7294e721fd35b5abe3f2a4a2d -->
## Acceso a todas las categorías de tag de la base de datos
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden visualizar &quot;tagcategory&quot; con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tagcategories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tagcategories`


<!-- END_a8b4d2f7294e721fd35b5abe3f2a4a2d -->

<!-- START_0249ce7d2c2f8d5455108027df761098 -->
## Acceso a todos los categorías de tag de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Los usuarios con idcustomer, solo pueden visualizar &quot;tagcategory&quot; con su mismo idcustomer.

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tagcategories/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tagcategories/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tagcategories/toselect`


<!-- END_0249ce7d2c2f8d5455108027df761098 -->

#Tag management


<!-- START_1ce1f679b5aaf2c01f5a08a9094f9f0a -->
## Creación de un nuevo tag
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden crear tag con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/tag" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idcategory":"qui","name":"eveniet"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tag"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idcategory": "qui",
    "name": "eveniet"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/tag`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idcategory` | string |  optional  | 
        `name` | string |  required  | 
    
<!-- END_1ce1f679b5aaf2c01f5a08a9094f9f0a -->

<!-- START_0b82ccf6640266d5c5136a8333aa6997 -->
## Actualización de un tag
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden updatear tag con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/tag/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"idtag":"autem","name":"sit"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tag/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "idtag": "autem",
    "name": "sit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/tag/{idtag}`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `idtag` | string |  optional  | 
        `name` | string |  optional  | 
    
<!-- END_0b82ccf6640266d5c5136a8333aa6997 -->

<!-- START_09a6a1460e6446fd99d64765e8227027 -->
## Borrado de un tag de la base de datos
Esta acción sólo la puede realizar el superadmin y admin
Los usuarios con idcustomer, solo pueden borrar tag con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/tag/aut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tag/aut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idtag` |  required  | 

<!-- END_09a6a1460e6446fd99d64765e8227027 -->

<!-- START_a3df1738d98706354a176fb0f4e4d5d7 -->
## Acceso a los datos de un tag
Esta acción sólo la puede realizar el superadmin, el admin y user
Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tag/explicabo" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tag/explicabo"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tag/{idtag}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `idtag` |  optional  | int required

<!-- END_a3df1738d98706354a176fb0f4e4d5d7 -->

<!-- START_eb51195b37059d7a10566d5b7d818d6a -->
## Acceso a todos los tags de la base de datos
Esta acción sólo la puede realizar el superadmin, el admin
Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tags" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tags"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tags`


<!-- END_eb51195b37059d7a10566d5b7d818d6a -->

<!-- START_7be6b75456b9446350e44c9d02a0a356 -->
## Acceso a todos los tags de la base de datos desde un select
Esta acción la puede realizar cualquier usuario
Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/tags/toselect" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/tags/toselect"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/tags/toselect`


<!-- END_7be6b75456b9446350e44c9d02a0a356 -->

#User access


<!-- START_fe77c98fed98e3931e2863f0d90e77bc -->
## Acceso de usuario a la web

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/public/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"vel","password":"amet"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/public/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "vel",
    "password": "amet"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST public/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `password` | string |  required  | 
    
<!-- END_fe77c98fed98e3931e2863f0d90e77bc -->

<!-- START_d3666a28258a5dda2f73e70014ea3011 -->
## logout de la plataforma

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/logout`


<!-- END_d3666a28258a5dda2f73e70014ea3011 -->

<!-- START_e3fad3de6a768969090b1cee58d66304 -->
## Acceso a los datos del usuario me

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/me`


<!-- END_e3fad3de6a768969090b1cee58d66304 -->

#User management


<!-- START_dfb11fbffa82b011847751cc897aefdd -->
## Registro de un nuevo usuario
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"voluptas","email":"adipisci","password":"adipisci","role":"suscipit","idcustomer":5,"idsite":18}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "voluptas",
    "email": "adipisci",
    "password": "adipisci",
    "role": "suscipit",
    "idcustomer": 5,
    "idsite": 18
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/user`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | 
        `email` | email |  required  | 
        `password` | string |  required  | 
        `role` | string |  required  | 
        `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
    
<!-- END_dfb11fbffa82b011847751cc897aefdd -->

<!-- START_d9203b730038ebed5ab0d661e5b8b8b1 -->
## Actualización de un usuario ya registrado
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/private/user/earum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"numquam","email":"sunt","role":"qui","idcustomer":1,"idsite":2,"status":false}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user/earum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "numquam",
    "email": "sunt",
    "role": "qui",
    "idcustomer": 1,
    "idsite": 2,
    "status": false
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT private/user/{iduser}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `iduser` |  required  | 
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | 
        `email` | email |  optional  | 
        `role` | string |  optional  | 
        `idcustomer` | integer |  optional  | 
        `idsite` | integer |  optional  | 
        `status` | boolean |  optional  | 
    
<!-- END_d9203b730038ebed5ab0d661e5b8b8b1 -->

<!-- START_afa224881b843d054a2e4f40854ad751 -->
## Borrado de un usuario de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/private/user/molestiae" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user/molestiae"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE private/user/{iduser}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `iduser` |  required  | 

<!-- END_afa224881b843d054a2e4f40854ad751 -->

<!-- START_6966beebd1f17b23f4183f7165c24989 -->
## Aceso a los datos de un usuario que no es me
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/user/eveniet" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user/eveniet"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/user/{iduser}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `iduser` |  optional  | int required

<!-- END_6966beebd1f17b23f4183f7165c24989 -->

<!-- START_33b51d6373e6f78fcea981e1d9b1478e -->
## Acceso a todos los usuarios de la base de datos
Esta acción sólo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/private/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "message": "Usuario no autenticado."
}
```

### HTTP Request
`GET private/users`


<!-- END_33b51d6373e6f78fcea981e1d9b1478e -->

<!-- START_2f193da7521642ed2e36cab0f654d18e -->
## Consulta que agrega un permiso a un usuario
Esta acción solo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/user/add-permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user/add-permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/user/add-permission`


<!-- END_2f193da7521642ed2e36cab0f654d18e -->

<!-- START_d959f6921cc9e4ffbe85508429e458a5 -->
## Consulta que quita un permiso a un usuario
Esta acción solo la puede realizar el superadmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/private/user/revoke-permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/private/user/revoke-permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST private/user/revoke-permission`


<!-- END_d959f6921cc9e4ffbe85508429e458a5 -->


