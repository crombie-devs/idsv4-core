<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class PasswordsManagementTest extends TestCase
{
    //use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group password
     */
    public function user_can_ask_remember_password()
    {    
        $data['email'] = 'superadmin@ladorian.com';
        $response = $this->json('post', 'public/remember', $data);
        $response->assertStatus(200);       
    }
    /**
     * @test
     * @group password
     */
    public function wrong_email_remember_password()
    {    
        $data['email'] = 'oscar.fernandezamo@ladorian.com';
        $response = $this->json('post', 'public/remember', $data);
        $response->assertStatus(422);       
    }
}