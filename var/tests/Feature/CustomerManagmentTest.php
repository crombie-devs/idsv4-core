<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Customer;
use Illuminate\Support\Str;

class CustomerManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group customer
     */
    public function superadmin_can_get_all_customers()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/customers');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_cant_get_all_customers(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/customers');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group customer
     */
    public function user_cant_get_all_customers(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/customers');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group customer
     */    
    public function superadmin_can_get_customer()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $customers = Customer::all();
        $customer = Customer::where('idcustomer', $customers[0]->idcustomer)->first();
        $response = $this->json('get', 'private/customer/'.$customer->idcustomer);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_cant_get_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/customer/2');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_can_get_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $response = $this->json('get', 'private/customer/'. $user->idcustomer);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group customer
     */
    public function user_cant_get_customer(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/customer/1');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group customer
     */
    public function user_can_get_his_customer(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $user = User::with('customer')->where('email', self::USER_EMAIL)->first();
        $response = $this->json('get', 'private/customer/'.$user->idcustomer);
        $response->assertStatus(200);
    }
    /**
     * @test
     * @group customer
     */
    public function superadmin_can_create_customer(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Customer::class, 1)->make()->toArray();
        $response = $this->json('post','private/customer', $data[0]);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_cant_create_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Customer::class, 1)->make()->toArray();
        $response = $this->json('post','private/customer', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function user_cant_create_customer(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Customer::class, 1)->make()->toArray();
        $response = $this->json('post','private/customer', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function superadmin_can_update_customer(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $customers = Customer::all();
        do{
            $id = rand(2, count($customers) - 1);
            $customer = Customer::where('idcustomer', $customers[$id]->idcustomer)->first();
        }while(!$customer);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/customer/'.$customer->idcustomer, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_cant_update_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/customer/2', $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_can_update_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $customer = Customer::find($user->idcustomer);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/customer/'. $customer->idcustomer, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group customer
     */
    public function user_cant_update_customer(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/customer/1', $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function superadmin_can_delete_customer(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $customers = Customer::all();
        $response = $this->json('delete', 'private/customer/4');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group customer
     */
    public function admin_cant_delete_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/customer/3');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group customer
     */
    public function user_cant_delete_customer(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('delete', 'private/customer/3');
        $response->assertStatus(403);
    }
}