<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Camera;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CameraManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();
    }
    
    /**
     * @test
     * @group camera
     */
    public function superadmin_can_create_camera(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Camera::class, 1)->make()->toArray();
        $response = $this->json('post','private/camera', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_cant_create_camera(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Camera::class, 1)->make()->toArray();
        $response = $this->json('post','private/camera', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function user_cant_create_camera(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Camera::class, 1)->make()->toArray();
        $response = $this->json('post','private/camera', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function superadmin_can_delete_camera(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $cameras = Camera::all();
        $id = count($cameras) - 1;
        $response = $this->json('delete', 'private/camera/'.$cameras[$id]->idcamera);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_cant_delete_camera(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $cameras = Camera::all();
        $id = count($cameras) - 1;
        $response = $this->json('delete', 'private/camera/'.$cameras[$id]->idcamera);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function user_cant_delete_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $cameras = Camera::all();
        $id = count($cameras) - 1;
        $response = $this->json('delete', 'private/camera/'.$cameras[$id]->idcamera);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function superadmin_can_read_all_cameras()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/cameras');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_can_get_cameras_of_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/cameras');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function user_can_get_cameras_of_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/cameras');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function superadmin_can_read_any_camera(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $cameras = Camera::where('deleted', 0)->get();
        $response = $this->json('get', 'private/camera/' . $cameras[0]->idcamera);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_cant_get_camera_of_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $cameras = Camera::where('deleted', 0)->get();
        $response = $this->json('get', 'private/camera/' . $cameras[3]->idcamera);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

        /**
     * @test
     * @group camera
     */
    public function admin_can_get_camera_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $cameras = $this->json('get', 'private/cameras');
        $response = $this->json('get', 'private/camera/' . $cameras['data'][0]['idcamera']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function user_cant_get_camera_of_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $cameras = Camera::where('deleted', 0)->get();
        $response = $this->json('get', 'private/camera/' . $cameras[4]->idcamera);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function user_can_get_camera_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $cameras = $this->json('get', 'private/cameras');
        $response = $this->json('get', 'private/camera/' . $cameras['data'][0]['idcamera']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function superadmin_can_update_camera(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $camera = Camera::where('idcamera', 1)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/camera/'.$camera->idcamera, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_cant_update_camera_of_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $camera = Camera::where('idcamera', 4)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/camera/'.$camera->idcamera, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function admin_can_update_camera_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $cameras = $this->json('get', 'private/cameras');
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/camera/'.$cameras['data'][0]['idcamera'], $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group camera
     */
    public function user_cant_update_camera_of_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $camera = Camera::where('idcamera', 2)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/camera/'.$camera->idcamera, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group camera
     */
    public function user_can_update_camera_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $cameras = $this->json('get', 'private/cameras');
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/camera/'.$cameras['data'][0]['idcamera'], $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
}