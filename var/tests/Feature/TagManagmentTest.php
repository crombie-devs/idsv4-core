<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Tag;
use App\Models\TagCategory;
use Illuminate\Support\Str;

class TagManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group tag
     */
    public function superadmin_can_get_all_tags()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tags');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group tag
     */
    public function admin_can_get_all_tags_from_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tags');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group tag
     */
    public function user_can_get_all_tags_from_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/tags');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group tag
     */    
    public function superadmin_can_get_tag()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $tags = Tag::all();
        $response = $this->json('get', 'private/tag/'.$tags[1]->idtag);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group tag
     */
    public function admin_cant_get_tag_from_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tag/1');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tag
     */
    public function user_cant_get_tag_from_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/tag/1');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group tag
     */
    public function superadmin_can_create_tag(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Tag::class, 1)->make()->toArray();
        $response = $this->json('post','private/tag', $data[0]);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tag
     */
    public function admin_cant_create_tag(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(TagCategory::class, 1)->make()->toArray();
        $response = $this->json('post','private/tag', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tag
     */
    public function user_cant_create_tag_category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(TagCategory::class, 1)->make()->toArray();
        $response = $this->json('post','private/tag', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tag
     */
    public function superadmin_can_update_tag_category(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $tag = Tag::all();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag/'.$tag[0]->idtag, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tag
     */
    public function admin_cant_update_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $tag = Tag::all();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag/'.$tag[0]->idtag, $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tag
     */
    public function user_cant_update_tag_category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());       
        $tag = Tag::all();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag/'.$tag[0]->idtag, $data);
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group tag
     */
    public function superadmin_can_delete_tag_category(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $tag = Tag::all();
        $response = $this->json('delete', 'private/tag/'.$tag[0]->idtag);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tag
     */
    public function admin_cant_delete_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $tag = Tag::all();
        $response = $this->json('delete', 'private/tag/'.$tag[0]->idtag);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tag
     */
    public function user_cant_delete_tag_Category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $tag = Tag::all();
        $response = $this->json('delete', 'private/tag/'.$tag[0]->idtag);
        $response->assertStatus(403);
    }
}