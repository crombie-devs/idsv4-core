<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\App;
use App\Models\Site;
use Illuminate\Support\Str;

class SiteManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group site
     */
    public function superadmin_can_get_all_sites()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/sites');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group site
     */
    public function admin_can_get_all_sites_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/sites');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group site
     */
    public function user_can_get_all_sites_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/sites');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group site
     */    
    public function superadmin_can_get_site()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/site/1');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group site
     */
    public function admin_cant_get_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $sites = Site::all();
        do{
            $id = rand(0, count($sites) - 1);
            $site = Site::find($sites[$id]->idsite);
        }while($site->idcustomer == $user->idcustomer);
        $response = $this->json('get', 'private/site/'.$site->idsite);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group site
     */
    public function admin_can_get_his_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $site = Site::where('idcustomer', $user->idcustomer)->first();
        $response = $this->json('get', 'private/site/'.$site->idsite);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group site
     */
    public function user_cant_get_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/site/1');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group site
     */
    public function user_can_get_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $user = User::where('email', self::USER_EMAIL)->first();
        $response = $this->json('get', 'private/site/'.$user->idsite);
        $response->assertStatus(200);
    }
    /**
     * @test
     * @group site
     */
    public function superadmin_can_create_site(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Site::class, 1)->make()->toArray();
        $customers = Customer::all();
        $id = rand(0, count($customers) - 1);
        $customer = Customer::where('idcustomer', $customers[$id]->idcustomer)->first();
        $data[0]['idcustomer'] = $customer->idcustomer;
        $response = $this->json('post','private/site', $data[0]);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group site
     */
    public function admin_cant_create_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Site::class, 1)->make()->toArray();
        $response = $this->json('post','private/site', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group site
     */
    public function user_cant_create_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Site::class, 1)->make()->toArray();
        $response = $this->json('post','private/site', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group site
     */
    public function superadmin_can_update_site(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $site = Site::find(1);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/site/'.$site->idsite, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group site
     */
    public function admin_cant_update_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $sites = Site::all();
        do{
            $id = rand(0, count($sites) - 1);
            $site = Site::find($sites[$id]->idsite);
        }while($site->idcustomer == $user->idcustomer);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/site/'.$site->idsite, $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group site
     */
    public function admin_can_update_his_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $site = Site::where('idcustomer', $user->idcustomer)->first();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/site/'.$site->idsite, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group site
     */
    public function user_cant_update_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $user = User::where('email', self::USER_EMAIL)->first();
        $sites = Site::all();
        do{
            $id = rand(0, count($sites) - 1);
            $site = Site::find($sites[$id]->idsite);
        }while($site->idsite == $user->idiste);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/site/'.$site->idsite, $data);
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group site
     */
    public function user_can_update_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $user = User::where('email', self::USER_EMAIL)->first();
        $site = Site::where('idsite', $user->idsite)->first();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/site/'.$site->idsite, $data);
        $response->assertStatus(200);
    }
    /**
     * @test
     * @group site
     */
    public function superadmin_can_delete_customer(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/site/1');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group site
     */
    public function admin_cant_delete_site(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/site/2');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group site
     */
    public function user_cant_delete_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('delete', 'private/site/2');
        $response->assertStatus(403);
    }
}