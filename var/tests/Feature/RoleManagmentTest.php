<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Role;
use Illuminate\Support\Str;

class RoleManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group role
     */
    public function superadmin_can_get_all_roles()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/roles');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function admin_cant_get_all_roles(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/roles');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function user_cant_get_all_roles(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/roles');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group role
     */    
    public function superadmin_can_get_role()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $roles = Role::all();
        $id = rand(0, count($roles) - 1);
        $role = Role::where('id', $roles[$id]->id)->first();
        $response = $this->json('get', 'private/role/'.$role->id);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function admin_cant_get_role(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $roles = Role::all();
        $id = rand(0, count($roles) - 1);
        $role = Role::where('id', $roles[$id]->id)->first();
        $response = $this->json('get', 'private/role/'.$role->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function user_cant_get_role(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $roles = Role::all();
        $id = rand(0, count($roles) - 1);
        $role = Role::where('id', $roles[$id]->id)->first();
        $response = $this->json('get', 'private/role/'.$role->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function superadmin_can_create_role(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Role::class, 1)->make()->toArray();
        $response = $this->json('post','private/role', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function admin_cant_create_role(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Role::class, 1)->make()->toArray();
        $response = $this->json('post','private/role', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function user_cant_create_role(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Role::class, 1)->make()->toArray();
        $response = $this->json('post','private/role', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function superadmin_can_update_role(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(3, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/role/'.$role->id, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);

    }

    /**
     * @test
     * @group role
     */
    public function admin_cant_update_role(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(3, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/role/'.$role->id, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function user_cant_update_role(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(3, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/role/'.$role->id, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function superadmin_can_delete_role(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(3, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $response = $this->json('delete', 'private/role/'.$role->id);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function admin_cant_delete_role(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(0, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $response = $this->json('delete', 'private/role/'.$role->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group role
     */
    public function user_cant_delete_role(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $roles = Role::all();
        do{
            $id = rand(0, count($roles) - 1);
            $role = Role::where('id', $roles[$id]->id)->first();
        }while(!$role);
        $response = $this->json('delete', 'private/role/'.$role->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
}