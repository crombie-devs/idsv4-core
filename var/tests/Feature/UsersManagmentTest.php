<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UsersManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * @group users
     */
    public function superadmin_can_get_all_users()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/users');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_cant_get_all_users(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/users');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_cant_get_all_users(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/users');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group users
     */
    public function superadmin_can_get_me()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/me');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group users
     */
    public function admin_can_get_me()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/me');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group users
     */
    public function user_can_get_me()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/me');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function superadmin_can_get_user()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $users = User::all();
        $id = rand(3, count($users));
        $user = User::where('iduser', $users[$id]->iduser)->first();
        $response = $this->json('get', 'private/user/'.$user->iduser);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_cant_get_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
         $response = $this->json('get', 'private/user/2');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_cant_get_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/user/2');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group users
     */
    public function superadmin_can_login(){
        $data = [
            "name" => 'superadmin',
            "password" => '1234'
        ];

        $response = $this->json('post', 'public/login', $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_can_login(){
        $data = [
            "name" => 'admin',
            "password" => '1234'
        ];

        $response = $this->json('post', 'public/login', $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_can_login(){
        $data = [
            "name" => 'user',
            "password" => '1234'
        ];

        $response = $this->json('post', 'public/login', $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function wrong_login(){
        $data = [
            "name" => 'user',
            "password" => '345'
        ];

        $response = $this->json('post', 'public/login', $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function superadmin_can_logout(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('post', 'private/logout');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_can_logout(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('post', 'private/logout');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_can_logout(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('post', 'private/logout');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group users
     */
    public function superadmin_can_register_user(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(User::class, 1)->make()->toArray();
        $data[0]['password'] = '1234';
        if(isset($data[0]['idcustomer']) && isset($data[0]['idsite'])){
            $data[0]['role'] = 'user';
        }else{
            $data[0]['role'] = 'admin';
        }
        $response = $this->json('post','private/user', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_cant_register_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(User::class, 1)->make()->toArray();
        $data[0]['password'] = '1234';
        $response = $this->json('post','private/user', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_cant_register_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(User::class, 1)->make()->toArray();
        $data[0]['password'] = '1234';
        $response = $this->json('post','private/user', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function superadmin_can_update_user(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $user = User::where('iduser', 4)->first();
        $data['name'] = 'Periquín';
        $response = $this->json('put', 'private/user/'.$user->iduser, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_cant_update_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data['name'] = 'Periquín';
        $response = $this->json('put', 'private/user/1', $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_cant_update_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data['name'] = "Periquín";
        $response = $this->json('put', 'private/user/1', $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function superadmin_can_delete_user(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $users = User::all();
        $id = rand(3, count($users));
        $user = User::where('iduser', $users[$id]->iduser)->first();
        $response = $this->json('delete', 'private/user/'.$user->iduser);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function admin_cant_delete_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/user/1');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group users
     */
    public function user_cant_delete_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $users = User::all();
        $response = $this->json('delete', 'private/user/1');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
}