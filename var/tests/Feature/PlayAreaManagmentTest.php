<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\PlayArea;
use App\Models\IndoorLocation;
use App\Models\Format;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class PlayAreaManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();
    }
    
    /**
     * @test
     * @group playarea
     */
    public function superadmin_can_create_playarea(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(PlayArea::class, 1)->make()->toArray();
        $response = $this->json('post','private/playarea', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_cant_create_playarea(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(PlayArea::class, 1)->make()->toArray();
        $response = $this->json('post','private/playarea', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group playarea
     */
    public function user_cant_create_playarea(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(PlayArea::class, 1)->make()->toArray();
        $response = $this->json('post','private/playarea', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function superadmin_can_delete_playarea(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $playareas = PlayArea::all();
        $id = count($playareas) - 1;
        $response = $this->json('delete', 'private/playarea/'.$playareas[$id]->idarea);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_cant_delete_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $playareas = PlayArea::all();
        $id = count($playareas) - 1;
        $response = $this->json('delete', 'private/playarea/'.$playareas[$id]->idarea);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function user_cant_delete_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $playareas = PlayArea::all();
        $id = count($playareas) - 1;
        $response = $this->json('delete', 'private/playarea/'.$playareas[$id]->idarea);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group playarea
     */
    public function superadmin_can_read_all_playareas()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/playareas');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_can_get_playareas_of_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/playareas');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function user_can_get_playareas_of_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/playareas');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group playarea
     */
    public function superadmin_can_read_any_playarea(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $playareas = PlayArea::all();
        $response = $this->json('get', 'private/playarea/' . $playareas[0]->idarea);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_cant_get_playarea_of_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $playareas = PlayArea::all();
        $response = $this->json('get', 'private/playarea/' . $playareas[3]->idarea);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_can_get_playarea_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $playareas = $this->json('get', 'private/playareas');
        $response = $this->json('get', 'private/playarea/' . $playareas['data'][0]['idarea']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group playarea
     */
    public function user_cant_get_playarea_of_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $playareas = PlayArea::all();
        $response = $this->json('get', 'private/playarea/' . $playareas[4]->idarea);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group playarea
     */
    public function user_can_get_playarea_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $playareas = $this->json('get', 'private/playareas');
        $response = $this->json('get', 'private/playarea/' . $playareas['data'][0]['idarea']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function superadmin_can_update_playarea(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $playarea = PlayArea::where('idarea', 1)->first();
        $data['name'] = 'Periquín';
        $response = $this->json('put', 'private/playarea/'.$playarea->idarea, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function admin_cant_update_playarea_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $playareas = $this->json('get', 'private/playareas');
        $data['name'] = 'Periquín';
        $response = $this->json('put', 'private/playarea/'.$playareas['data'][0]['idarea'], $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group playarea
     */
    public function user_cant_update_playarea_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $playareas = $this->json('get', 'private/playareas');
        $data['name'] = 'Periquín';
        $response = $this->json('put', 'private/playarea/'.$playareas['data'][0]['idarea'], $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
}