<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\TagCategory;
use Illuminate\Support\Str;

class TagCategoryManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group tagcategory
     */
    public function superadmin_can_get_all_tag_categories()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tag-categories');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function admin_cant_get_all_tag_categories(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tag-categories');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function user_cant_get_all_tag_categories(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/tag-categories');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group tagcategory
     */    
    public function superadmin_can_get_tag_category()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tag-category/1');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function admin_cant_get_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/tag-category/1');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function user_cant_get_tag_category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/tag-category/1');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group tagcategory
     */
    public function superadmin_can_create_tag_category(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(TagCategory::class, 1)->make()->toArray();
        $customers = Customer::all();
        $id = rand(0, count($customers) - 1);
        $customer = Customer::where('idcustomer', $customers[$id]->idcustomer)->first();
        $data[0]['idcustomer'] = $customer->idcustomer;
        $response = $this->json('post','private/tag-category', $data[0]);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function admin_cant_create_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(TagCategory::class, 1)->make()->toArray();
        $response = $this->json('post','private/tag-category', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function user_cant_create_tag_category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(TagCategory::class, 1)->make()->toArray();
        $response = $this->json('post','private/tag-category', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function superadmin_can_update_tag_category(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $tagCategory = TagCategory::find(1);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag-category/'.$tagCategory->idcategory, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function admin_cant_update_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $user = User::where('email', self::ADMIN_EMAIL)->first();
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag-category/1', $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function user_cant_update_tag_category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());       
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/tag-category/1', $data);
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group tagcategory
     */
    public function superadmin_can_delete_tag_category(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/tag-category/1');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function admin_cant_delete_tag_category(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/tag-category/2');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group tagcategory
     */
    public function user_cant_delete_tag_Category(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('delete', 'private/tag-category/2');
        $response->assertStatus(403);
    }
}