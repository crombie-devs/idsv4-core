<?php


namespace Tests\Feature;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;
use Tests\TestCase;

class PermissionManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group permission
     */
    public function superadmin_can_get_all_permissions()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/permissions');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function admin_cant_get_all_permissions(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/permissions');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function user_cant_get_all_permissions(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/permissions');
        $response->assertStatus(403);
    }
    
    /**
     * @test
     * @group permission
     */
    public function superadmin_can_get_permission()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('get', 'private/permission/'.$permission->id);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function admin_cant_get_permission(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(0, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('get', 'private/permission/'.$permission->id);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group permission
     */
    public function user_cant_get_permission(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(0, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('get', 'private/permission/'.$permission->id);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group permission
     */
    public function superadmin_can_create_permission(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Permission::class, 1)->make()->toArray();
        $response = $this->json('post','private/permission', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function admin_cant_create_permission(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Permission::class, 1)->make()->toArray();
        $response = $this->json('post','private/permission', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function user_cant_create_permission(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Permission::class, 1)->make()->toArray();
        $response = $this->json('post','private/permission', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function superadmin_can_update_permission(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $data['name'] = Str::random(6) . ":cambio";
        $response = $this->json('put', 'private/permission/'.$permission->id, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function admin_cant_update_permission(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $data['name'] = "periquin:cambio";
        $response = $this->json('put', 'private/permission/'.$permission->id, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function user_cant_update_permission(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $data['name'] = "periquin:cambio";
        $response = $this->json('put', 'private/permission/'.$permission->id, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function superadmin_can_delete_permission(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('delete', 'private/permission/'.$permission->id);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function admin_cant_delete_permission(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('delete', 'private/permission/'.$permission->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group permission
     */
    public function user_cant_delete_permission(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $permissions = Permission::all();
        do{
            $id = rand(3, count($permissions) - 1);
            $permission = Permission::where('id', $permissions[$id]->id)->first();
        }while(!$permission);
        $response = $this->json('delete', 'private/permission/'.$permission->id);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
}