<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Customer;
use App\Models\App;
use Illuminate\Support\Str;

class AppManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();

    }

    /**
     * @test
     * @group app
     */
    public function superadmin_can_get_all_apps()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/apps');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group app
     */
    public function admin_cant_get_all_apps(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/apps');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group app
     */
    public function user_cant_get_all_apps(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/apps');
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group app
     */    
    public function superadmin_can_get_app()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $apps = App::all();
        $id = rand(0, count($apps) - 1);
        $app = App::find($apps[$id]->idapp);
        $response = $this->json('get', 'private/app/'.$app->idapp);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group app
     */
    public function admin_cant_get_app(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/app/2');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group app
     */
    public function user_cant_get_app(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/app/2');
        $response->assertStatus(403);
    }
    /**
     * @test
     * @group app
     */
    public function superadmin_can_create_app(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(App::class, 1)->make()->toArray();
        $response = $this->json('post','private/app', $data[0]);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group app
     */
    public function admin_cant_create_app(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(App::class, 1)->make()->toArray();
        $response = $this->json('post','private/app', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group app
     */
    public function user_cant_create_app(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(App::class, 1)->make()->toArray();
        $response = $this->json('post','private/app', $data[0]);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group app
     */
    public function superadmin_can_update_app(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $apps = App::all();
        do{
            $id = rand(2, count($apps) - 1);
            $app = App::find($apps[$id]->idapp);
        }while(!$app);
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/app/'.$app->idapp, $data);
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group app
     */
    public function admin_cant_update_app(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/app/2', $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group app
     */
    public function user_cant_update_app(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data['name'] = Str::random(10);
        $response = $this->json('put', 'private/app/1', $data);
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group app
     */
    public function superadmin_can_delete_app(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/app/3');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @group app
     */
    public function admin_cant_delete_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('delete', 'private/app/3');
        $response->assertStatus(403);
    }

    /**
     * @test
     * @group app
     */
    public function user_cant_delete_app(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('delete', 'private/app/3');
        $response->assertStatus(403);
    }
}