<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Player;
use App\Models\Channel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class PlayerManagementTest extends TestCase
{
    //use RefreshDatabase;

    const SUPER_ADMIN_EMAIL = 'superadmin@ladorian.com';
    const ADMIN_EMAIL = 'admin@ladorian.com';
    const USER_EMAIL = 'user@ladorian.com';

    protected function setUp(): void
    {
        parent::setUp();
    }
    
    /**
     * @test
     * @group player
     */
    public function superadmin_can_create_player(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $data = factory(Player::class, 1)->make()->toArray();
        $channels = Channel::all();
        $id = rand(0, count($channels) - 1);
        $data[0]['idchannel'] = $channels[$id]->idchannel;
        $response = $this->json('post','private/player', $data[0]);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_cant_create_player(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $data = factory(Player::class, 1)->make()->toArray();
        $channels = Channel::all();
        $id = rand(0, count($channels) - 1);
        $data[0]['idchannel'] = $channels[$id]->idchannel;
        $response = $this->json('post','private/player', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function user_cant_create_player(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $data = factory(Player::class, 1)->make()->toArray();
        $channels = Channel::all();
        $id = rand(0, count($channels) - 1);
        $data[0]['idchannel'] = $channels[$id]->idchannel;
        $response = $this->json('post','private/player', $data[0]);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function superadmin_can_delete_player(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $players = Player::all();
        $id = count($players) - 1;
        $response = $this->json('delete', 'private/player/'.$players[$id]->idplayer);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_cant_delete_user(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $players = Player::all();
        $id = count($players) - 1;
        $response = $this->json('delete', 'private/player/'.$players[$id]->idplayer);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function user_cant_delete_user(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $players = Player::all();
        $id = count($players) - 1;
        $response = $this->json('delete', 'private/player/'.$players[$id]->idplayer);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function superadmin_can_read_all_players()
    {
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        
        $response = $this->json('get', 'private/players');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_can_get_players_of_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $response = $this->json('get', 'private/players');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function user_can_get_players_of_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $response = $this->json('get', 'private/players');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function superadmin_can_read_any_player(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $players = Player::where('deleted', 0)->get();
        $response = $this->json('get', 'private/player/' . $players[0]->idplayer);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_cant_get_player_of_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $players = Player::where('status', 1)->where('deleted', 0)->get();
        $response = $this->json('get', 'private/player/' . $players[3]->idplayer);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

        /**
     * @test
     * @group player
     */
    public function admin_can_get_player_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $players = $this->json('get', 'private/players');
        $response = $this->json('get', 'private/player/' . $players['data'][0]['idplayer']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function user_cant_get_player_of_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $players = Player::where('deleted', 0)->get();
        $response = $this->json('get', 'private/player/' . $players[4]->idplayer);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function user_can_get_player_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $players = $this->json('get', 'private/players');
        $response = $this->json('get', 'private/player/' . $players['data'][0]['idplayer']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function superadmin_can_update_player(){
        Passport::actingAs(User::where('email', self::SUPER_ADMIN_EMAIL)->first());
        $player = Player::where('idplayer', 1)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/player/'.$player->idplayer, $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_cant_update_player_of_other_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $player = Player::where('idplayer', 4)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/player/'.$player->idplayer, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function admin_can_update_player_of_his_customer(){
        Passport::actingAs(User::where('email', self::ADMIN_EMAIL)->first());
        $players = $this->json('get', 'private/players');
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/player/'.$players['data'][0]['idplayer'], $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    /**
     * @test
     * @group player
     */
    public function user_cant_update_player_of_other_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $player = Player::where('idplayer', 2)->first();
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/player/'.$player->idplayer, $data);
        $response->assertStatus(403);
        $response->assertJson([
            'success' => false
        ]);
    }
    /**
     * @test
     * @group player
     */
    public function user_can_update_player_of_his_site(){
        Passport::actingAs(User::where('email', self::USER_EMAIL)->first());
        $players = $this->json('get', 'private/players');
        $data['email'] = 'svasquez1@example.com';
        $response = $this->json('put', 'private/player/'.$players['data'][0]['idplayer'], $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }
}