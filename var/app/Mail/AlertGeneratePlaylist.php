<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertGeneratePlaylist extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $errors;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($errors, $subject, $env){
        $this->errors = $errors;
        $this->subject = "[".$env."] ".$subject." generados correctamente";
        if(count($errors)>0)
            $this->subject = "[".$env."] "."Error en la Generación de ".$subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(['developer@ladorian.com'])
                    ->with(['response' => $this->errors])
                    ->subject($this->subject)
                    ->view("email.generateplaylist");
    }
}
