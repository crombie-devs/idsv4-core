<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertDownPlayer extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $text;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $text)
    {

        $this->subject = $subject;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $to = ['soporte@ladorian.com', 'juancarlos@ladorian.com', 'cristiano@ladorian.com', "julian.cardona@ladorian.com"];
        $to = ['link4your@gmail.com', 'isabelo@ladorian.com'];
        return $this->to($to)
            ->with(['response' => 'error'])
            ->subject($this->subject)
            ->view("email.downPlayer")->with('response', $this->text);
    }
}
