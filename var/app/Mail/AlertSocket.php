<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertSocket extends Mailable
{
    use Queueable, SerializesModels;
    protected $idplayer;
    protected $filename;
    public $subject;
    protected $idcustomer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($idcustomer, $idplayer, $filename, $subject)
    {
        $this->idplayer = $idplayer;
        $this->filename = $filename;
        $this->subject = $subject;
        $this->idcustomer = $idcustomer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $to = [ 'soporte@ladorian.com', 'juancarlos@ladorian.com'];
        if($this->idcustomer == 65)
            $to[] = "julian.cardona@ladorian.com";
        return $this->to($to)
                    ->with(['idplayer' => $this->idplayer])
                    ->subject($this->subject)
                    ->attachFromStorageDisk('alert', $this->filename)
                    ->view("email.alert");
    }
}
