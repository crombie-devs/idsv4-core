<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertOpenTicket extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $data;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $env, $data)
    {

        $this->subject = "[" . $env . "] Enviado por  soporte Ladorian";
        $this->email = $data['email'];
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->to([$this->email])
            ->with(['response' => 'error'])
            ->subject($this->subject)
            ->view("email.openTicket")->with('response', $this->data);
    }
}
