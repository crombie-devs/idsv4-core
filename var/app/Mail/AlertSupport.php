<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertSupport extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $data;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $env, $data, $email){
       
        $this->subject = $subject;
        $this->email = $email;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to([$this->email])
            ->with(['response' => 'error'])
            ->subject($this->subject)
            ->view("email.support")->with('response' , $this->data);
                  
    }
}
