<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertSupportClose extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $data;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $env, $data)
    {

        $this->subject = "[" . $env . "] Enviado por  " . $subject;
        $this->email = $subject;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to([$this->email])
            ->with(['response' => 'error'])
            ->subject($this->subject)
            ->view("email.supportClose")->with('response', $this->data);
    }
}
