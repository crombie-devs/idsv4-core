<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertBuildPlaylist extends Mailable
{
    use Queueable, SerializesModels;
    protected $idcontent;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($idcontent, $subject)
    {
        $this->idcontent = $idcontent;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['name' => 'Playlist'])
                    ->to([ 'mjesus.diaz@ladorian.com', 'juancarlos@ladorian.com'])
                    ->with(['idcontent' => $this->idcontent])
                    ->subject($this->subject)
                    ->view("email.playlist");
    }
}
