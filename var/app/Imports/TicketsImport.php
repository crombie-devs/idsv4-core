<?php

namespace App\Imports;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Repositories\Product\ProductRepositoryInterface;

class TicketsImport implements OnEachRow, WithHeadingRow
{
    private $idcustomer;
    private $productrepository;
    public function __construct(ProductRepositoryInterface $productrepository, int $idcustomer)
    {   
        $this->productrepository = $productrepository;
        $this->idcustomer = $idcustomer;
    }
    public function onRow(Row $row)
    {
        $category = [
            "idcustomer" => $this->idcustomer,
            "code" => $row['category1_id'],
            "name" => $row['category1_name']
        ];
        $cat = $this->productrepository->categoryFindOneAndUpdate($category);
        if(isset($row['category2_id'])){
            $category = [
                "idcustomer" => $this->idcustomer,
                "code" => $row['category2_id'],
                "name" => $row['category2_name']
            ];
            $cat = $this->productrepository->categoryFindOneAndUpdate($category);
        }
        if(isset($row['category3_id'])){
            $category = [
                "idcustomer" => $this->idcustomer,
                "code" => $row['category3_id'],
                "name" => $row['category3_name']
            ];
            $cat = $this->productrepository->categoryFindOneAndUpdate($category);
        }
        if(isset($row['category4_id'])){
            $category = [
                "idcustomer" => $this->idcustomer,
                "code" => $row['category4_id'],
                "name" => $row['category4_name']
            ];
            $cat = $this->productrepository->categoryFindOneAndUpdate($category);
        }

        $product = [
            "idcustomer" => $this->idcustomer,
            "idcategory" => $cat->idcategory,
            "code" => $row['product_id'],
            "name" => $row['product_name'],
            "brand" => $row['brand']
        ];
        if(isset($row['supplier']))
            $product['supplier'] = $row['supplier'];
            
        $prod = $this->productrepository->findOneAndUpdate($product);
        return [$cat,$prod];
    }
}
