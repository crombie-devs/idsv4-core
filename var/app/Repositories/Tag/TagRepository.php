<?php

namespace App\Repositories\Tag;

use Illuminate\Http\Request;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Models\Tag;
use App\Models\TagCategory;
use App\Models\User;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class TagRepository implements TagRepositoryInterface {

    public static function create(array $request){
        $tag = new Tag();
        $tag->fill($request);

        //check desde request
        if(!$tag->isValid()){
            throw new NotValidUserException;
        }

        if(!$tag->save())
            throw new NotFoundException('Error al insertar el tag.');

        return $tag;
    }

    public static function update(int $id, array $request){
        $tag = Tag::find($id);
        if(!$tag)
            throw new NotFoundException('No se ha encontrado el Tag.');

        //check desde la BD
        if(!$tag->isValid()){
            throw new NotValidUserException;
        }

        $tag->fill($request);

        //check desde request
        if(!$tag->isValid()){
            throw new NotValidUserException;
        }

        $tag->save();
        return $tag;


    }

    public static function delete(int $id){
        $tag = Tag::find($id);
        if(!$tag)
            throw new NotFoundException('No se ha encontrado el Tag.');

        if(!$tag->isValid()){
            throw new NotValidUserException;
        }

        $tag->delete();
        return $tag;
    }

    public static function tag(int $id){
        $tag = Tag::getDetail($id);
        if(!$tag){
            throw new NotFoundException('No se ha encontrado el tag.');
        }
        if(!$tag->isValid()){
            throw new NotValidUserException;
        }
        return $tag;
    }

    public static function tags(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $tags = Tag::getSuperAdmin();
        }
        else {
            $tags = Tag::getAdmin($user);
        }

        if(!$tags)
            throw new NotFoundException('No se ha encontrado tags');

        return $tags;
    }

    public static function tagsProtect(){
        $tags = Tag::tagsProtect();
        if(!$tags)
            throw new NotFoundException('No se ha encontrado tags');

        return $tags;
    }





    public static function toSelect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $tags = Tag::getSuperAdmin(1);
        }
        else {
            $tags = Tag::getAdmin($user,1);
        }

        if(!$tags)
            throw new NotFoundException('No se ha encontrado content tags');

        return $tags;
    }


    public static function byType($type) {

        $user = Auth::user();
        return Tag::byType($type, $user->idcustomer);

    }

}