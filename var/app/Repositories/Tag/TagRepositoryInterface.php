<?php

namespace App\Repositories\Tag;

use Illuminate\Http\Request;

interface TagRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function tags();
    public static function tag(int $id);
}