<?php

namespace App\Repositories\Landingpage;

use Illuminate\Http\Request;
use App\Repositories\Landingpage\LandingpageRepositoryInterface;
use App\Models\Landingpage;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class LandingpageRepository implements LandingpageRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $landingpage = Landingpage::create($input);
        if(!$landingpage)
            throw new NotFoundException('Error al insertar la landing page.');

        return $landingpage;
    }

    public static function update(int $id, Request $request){
        $landingpage = Landingpage::find($id);
        if(!$landingpage){
            throw new NotFoundException('No se ha encontrado la landing page.');
        }
        if(isset($request['name'])) $landingpage->name = $request['name'];
        if(isset($request['thumbnail_url'])) $landingpage->thumbnailUrl = $request['thumbnail_url']; 
        if(isset($request['base_url'])) $landingpage->baseUrl = $request['base_url'];
        if(isset($request['config'])) $landingpage->config = $request['config'];
        $landingpage->save();
        return $landingpage;
    }

    public static function delete(int $id){
        $landingpage = Landingpage::find($id);
        if(!$landingpage){
            throw new NotFoundException('No se ha encontrado la landing page.');
        }
        $landingpage->deleted = true;
        $landingpage->save();
        return $landingpage;
    }

    public static function landingpage(int $id){
        $landingpage = Landingpage::where('id', $id)->first();
        if(!$landingpage){
            throw new NotFoundException('No se ha encontrado la landing page.');
        }
        return $landingpage;
    }

    public static function landingpages(){
        $landingpages = Landingpage::get();
        if(!$landingpages){
            throw new NotFoundException('No se han encontrado landing page.');
        }
        return $landingpages;
    }    

}