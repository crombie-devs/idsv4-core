<?php

namespace App\Repositories\Landingpage;

use Illuminate\Http\Request;

interface LandingpageRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);

    public static function landingpages();
    public static function landingpage(int $id);
}