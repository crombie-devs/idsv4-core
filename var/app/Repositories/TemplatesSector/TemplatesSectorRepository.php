<?php

namespace App\Repositories\TemplatesSector;

use Illuminate\Http\Request;
use App\Repositories\TemplatesSector\TemplatesSectorRepositoryInterface;
use App\Models\Customer;
use App\Models\TemplatesSector;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class TemplatesSectorRepository implements TemplatesSectorRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $templatesSector = TemplatesSector::create($input);
        if(!$templatesSector)
            throw new NotFoundException('Error al insertar el template sector.');

        return $templatesSector;
    }

    public static function update(int $id, Request $request){
        $templatesSector = TemplatesSector::find($id);
        if(!$templatesSector){
            throw new NotFoundException('No se ha encontrado el Templates Sector.');
        }
        if(isset($request['name'])) $templatesSector->name = $request['name'];
        if(isset($request['icon'])) $templatesSector->icon = $request['icon']; 
        if(isset($request['idcustomer'])){
            $customer = Customer::find($request['idcustomer']);
            $templatesSector->idcustomer = $customer->idcustomer;
        }
       
        $templatesSector->save();
        return $templatesSector;
    }

    public static function delete(int $id){
        $templatesSector = TemplatesSector::find($id);
        if(!$templatesSector){
            throw new NotFoundException('No se ha encontrado el Templates Sector.');
        }
        $templatesSector->deleted = true;
        $templatesSector->save();
        return $templatesSector;
    }

    public static function templatesSector(int $id){
        $templatesSector = TemplatesSector::with('customer')->where('idtemplatesector', $id)->first();
        if(!$templatesSector){
            throw new NotFoundException('No se ha encontrado el Templates Sector.');
        }
        return $templatesSector;
    }

    public static function templatesSectors(){
        $templatesSectors = TemplatesSector::with('customer')->get();
        if(!$templatesSectors){
            throw new NotFoundException('No se han encontrado Templates Sector.');
        }
        return $templatesSectors;
    }

    public static function templatesSectorsToSelect(){
        $user = Auth::user();        
        if($user->roles[0]->name == 'superadmin'){
            $templatesSectors = TemplatesSector::get();
        }else{
            $templatesSectors = TemplatesSector::where('idcustomer',$user->customer->idcustomer)->orWhere('idcustomer',null)->get();
        }
        if(!$templatesSectors){
            throw new NotFoundException('No se han encontrado Templates Sector.');
        }
        return $templatesSectors;
    }

}