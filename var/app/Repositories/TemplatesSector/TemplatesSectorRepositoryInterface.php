<?php

namespace App\Repositories\TemplatesSector;

use Illuminate\Http\Request;

interface TemplatesSectorRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);

    public static function templatessectors();
    public static function templatessector(int $id);

    public static function templatessectorsToSelect();
}