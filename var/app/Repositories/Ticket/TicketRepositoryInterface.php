<?php

namespace App\Repositories\Ticket;

interface TicketRepositoryInterface
{

    public static function list();
    public static function get($id);
    public function create(array $request, $files);
    public function update(array $request, $files, $comments);
    public static function toSelect();
    public static function category();
    public static function priority();
}
