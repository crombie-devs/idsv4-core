<?php

namespace App\Repositories\Ticket;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use App\Models\Ticket;
use App\Models\TicketCategory;
use App\Models\TicketPriority;
use App\Models\TicketStatus;
use App\Models\TicketComment;
use App\Models\TicketImage;
use App\Repositories\Ticket\TicketRepositoryInterface;
use App\Exceptions\NotFoundException;
use App\Traits\ImageUpload;
use DateTime;

class TicketRepository  implements TicketRepositoryInterface
{

    use ImageUpload;

    public static function list()
    {
    }
    public static function get($id)
    {
    }

    public function create(array $request, $files) {
        $ticket = new Ticket();
        $ticket->fill($request);
        $ticket->save();
        if ($request['comments']) {
            foreach ($request['comments'] as $comment) {
                $ticketComment = new TicketComment();
                $ticketComment->fill($comment);
                $ticketComment->save();
            }
        }
        if ($files) {
            foreach ($files as $key => $value) {
                foreach ($value as $f) {
                    $img = $this->subeImage($f, '/comments/');
                    $image = new TicketImage();
                    $setImage['id_ticket'] = $ticket->id;
                    $setImage['url'] = $img;
                    $image->fill($setImage);
                    $image->save();
                }
            }
        }
        return $ticket;
    }

    public function update(array $request, $files, $comments)
    {

        $ticket = Ticket::find($request['id']);

        $pass = false;
        if ($ticket->id_responsible != $request['id_responsible']) {
            $pass = true;
        }

        if (!$ticket) {
            throw new NotFoundException('No se ha encontrado el ticket');
        }

        $ticket->fill($request);
        $ticket->save();

        if ($comments) {

            TicketComment::where('id_ticket', $request['id'])->delete();

            foreach ($comments as $comment) {

                $commentDecode = json_decode($comment);

                $ticketComment = new TicketComment();
                $ticketComment->fill((array)$commentDecode);
                $ticketComment->save();
            }
        } else {
            TicketComment::where('id_ticket', $request['id'])->delete();
        }

        if ($files) {

            TicketImage::where('id_ticket', $request['id'])->delete();

            foreach ($files as $key => $value) {

                foreach ($value as $f) {

                    $img = $this->subeImage($f, '/comments/');
                    // $img = $this->subeImage($f, '/tickets/');
                    $image = new TicketImage();
                    $setImage['id_ticket'] = $ticket->id;
                    $setImage['url'] = $img;
                    $image->fill($setImage);
                    $image->save();
                }
            }
        }

        $ticket = Ticket::find($request['id']);

        return [$ticket, $pass];
    }

    public static function getBy($orderBy, $column = false, $condition = false, $sortMethod = 'asc')
    {

        $error = false;

        if (Schema::hasColumn('tickets', $orderBy)) {

            if (!$column && !$condition) {

                $entities = Ticket::query()
                    ->with('priority')
                    ->with('category')
                    ->with('department')
                    ->with('responsible')
                    ->with('site')
                    ->with('status')
                    ->with('customer')
                    ->with('user')
                    ->with('comments')
                    ->with('images')
                    ->with('site.customer')
                    ->with('site.city')
                    ->with('site.city.provinces')
                    ->with('site.city.provinces.countries')
                    ->orderBy($orderBy, $sortMethod)
                    ->get();

                if (!$entities)
                    throw new NotFoundException('No se han encontrado tickets para mostrar');
            } else {

                if (Schema::hasColumn('tickets', $column)) {

                    $entities = Ticket::query()
                        ->with('priority')
                        ->with('category')
                        ->with('department')
                        ->with('responsible')
                        ->with('site')
                        ->with('status')
                        ->with('customer')
                        ->with('user')
                        ->with('comments')
                        ->with('images')
                        ->with('site.customer')
                        ->with('site.city')
                        ->with('site.city.provinces')
                        ->with('site.city.provinces.countries')
                        ->where($column, $condition)
                        ->orderBy($orderBy, $sortMethod)
                        ->get();

                    if (!$entities)
                        throw new NotFoundException('No se han encontrado tickets para mostrar');
                } else {

                    $error = true;
                }
            }
        } else {

            $error = true;
        }

        if ($error) {

            $response = [
                'error' => [
                    'type' => 'Not Acceptable',
                    'code' => 406,
                ],
                'message' => 'La columna ' . $orderBy . ' o ' . $column . ' no existe sobre la entidad tickets.',
            ];
        } else {

            $response = [
                'success' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count()
                ],
                'data' => $entities
            ];
        }

        return $response;
    }

    public static function groupBy($groupBy)
    {

        if (Schema::hasColumn('tickets', $groupBy)) {

            $entities = Ticket::query()
                ->selectRaw($groupBy . ', count(*) as count')
                ->with(str_replace('id_', '', $groupBy))
                ->groupBy($groupBy)
                ->get();

            if (!$entities)
                throw new NotFoundException('No se han encontrado tickets para mostrar');

            $response = [
                'success' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count()
                ],
                'data' => $entities
            ];
        } else {

            $response = [
                'error' => [
                    'type' => 'Not Acceptable',
                    'code' => 406,
                ],
                'message' => 'La columna ' . $groupBy . ' no existe sobre la entidad tickets.',
            ];
        }

        return $response;
    }

    public function support($data, $files)
    {

        $ticket = new Ticket();
        $ticket->fill($data);
        $ticket->save();
        if ($files) {
            foreach ($files as $key => $value) {
                foreach ($value as $f) {

                    $img = $this->subeImage($f, '/comments/');
                    $image = new TicketImage();
                    $setImage['id_ticket'] = $ticket->id;
                    $setImage['ruta'] = $img;
                    $image->fill($setImage);
                    $image->save();
                }
            }
        }
        $data['id_ticket'] = $ticket->id;
        $fecha = new DateTime();
        $data['created_at'] = $fecha->format('d-m-Y');

        return $data;
    }

    public function close($id)
    {

        $status = Ticket::close([$id]);

        $ticket = Ticket::with('category')
            ->with('priority')
            ->with('status')
            ->with('user')
            ->with('comments')
            ->with('department')
            ->with('responsible')
            ->with('client')
            ->find($id);

        if (!$ticket)
            throw new NotFoundException('No se han encontrado ticket para mostrar');

        return $ticket;
    }

    public static function byCustomer($type)
    {

        $tickets = Ticket::byCustomer($type);

        if (!$tickets)
            throw new NotFoundException('No se han encontrado los tickets');

        return $tickets;
    }

    public function deleteComment($id)
    {
        return TicketComment::commentClose($id);
    }

    public function getComments($id)
    {

        $ticket = TicketComment::where('id', $id)
            ->andWhere('deleted', 1)
            ->with('images')
            ->with('status')
            ->with('responsible')
            ->with('created_by');

        if (!$ticket)
            throw new NotFoundException('No se han encontrado ticket para mostrar');

        return $ticket;
    }

    public function createComment($data, $files)
    {

        $ticket = new TicketComment();
        $ticket->fill($data);
        $ticket->save();

        foreach ($files as $key => $value) {
            foreach ($value as $f) {

                $img = $this->subeImage($f, '/comments/');
                $imageComment = new TicketImage();
                $setImage['id_comment'] = $ticket->id;
                $setImage['ruta'] = $img;
                $imageComment->fill($setImage);
                $imageComment->save();
            }
        }

        $response = TicketComment::where('id', $ticket->id)
            ->with('images')
            ->with('created_by')
            ->get();

        return $response;
    }

    public static function byType($type)
    {

        $tickets = Ticket::byType($type);

        if (!$tickets)
            throw new NotFoundException('No se han encontrado los tickets');

        return $tickets;
    }

    public static function createStatus(array $request)
    {

        $ticket = new TicketStatus();
        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }

    public static function updateStatus(array $request)
    {

        $ticket = TicketStatus::find($request['id']);

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado la categoria');

        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }

    public static function deleteStatus($ids)
    {

        foreach ($ids->ids as $key => $value) {
            $ticket = TicketStatus::where('id', $value)->delete();
        }

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado el ticket');

        return $ticket;
    }

    public static function status()
    {

        $ticket_priority = TicketStatus::get();

        if (!$ticket_priority)
            throw new NotFoundException('No se han encontrado prioridades para mostrar');

        return $ticket_priority;
    }

    public static function createPriority(array $request)
    {

        $ticket = new TicketPriority();
        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }

    public static function updatePriority(array $request)
    {

        $ticket = TicketPriority::find($request['id']);

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado la categoria');

        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }



    public static function deletePriority($ids)
    {

        foreach ($ids->ids as $key => $value) {
            $ticket = TicketPriority::where('id', $value)->delete();
        }

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado el ticket');

        return $ticket;
    }


    public static function priority()
    {

        $ticket_priority = TicketPriority::get();

        if (!$ticket_priority)
            throw new NotFoundException('No se han encontrado prioridades para mostrar');

        return $ticket_priority;
    }

    public static function deleteCategory($ids)
    {

        foreach ($ids->ids as $key => $value) {
            $ticket = TicketCategory::where('id', $value)->delete();
        }

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado el ticket');

        return $ticket;
    }

    public static function category()
    {

        $ticket_categories = TicketCategory::get();

        if (!$ticket_categories)
            throw new NotFoundException('No se han encontrado categorias para mostrar');

        return $ticket_categories;
    }

    public static function createCategory(array $request)
    {

        $ticket = new TicketCategory();
        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }

    public static function updateCategory(array $request)
    {

        $ticket = TicketCategory::find($request['id']);

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado la categoria');

        $ticket->fill($request);
        $ticket->save();

        return $ticket;
    }

    public static function toSelect()
    {

        $tickets = Ticket::where('status', 1)->get();

        if (!$tickets)
            throw new NotFoundException('No se han encontrado tickets para mostrar');

        return $tickets;
    }

    public static function delete($ids)
    {

        $ticket = Ticket::deleteTicket($ids->ids);

        if (!$ticket)
            throw new NotFoundException('No se ha encontrado el ticket');

        return $ticket;
    }
}
