<?php

namespace App\Repositories\Support;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Repositories\Support\SupportRepositoryInterface;
use App\Exceptions\NotFoundException;
use App\Exceptions\UserLoginException;

use function Ramsey\Uuid\v1;

class SupportRepository implements SupportRepositoryInterface {

  

    public static function login(Request $request) {
        $name = $request->name;
        $password = $request->password;
       
        if (Auth::attempt(['name' => $name, 'password' => $password])) {
            $user = $request->user();

            foreach($user->roles as $role) {
         
                if($role->name == 'superadmin') {
                     $response = self::getTokenAndRefreshToken($user);
                     return $response;
                }

            }

            throw new UserLoginException;
            
        } else {
           throw new UserLoginException;
        }
    
    }

    public static function logout(Request $request) {
        $request->user()->token()->revoke();
        return true;
    }

  

    public static function getTokenAndRefreshToken(User $user) {
        $tokenResult = $user->createToken($user->email);
        $token = $tokenResult->token;
        $token->save();
        $response = ['data' => [
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
            ]
        ];

        return $response;
    }
}