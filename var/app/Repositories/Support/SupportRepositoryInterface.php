<?php

namespace App\Repositories\Support;

use Illuminate\Http\Request;
use App\Models\User;

interface SupportRepositoryInterface {
    
 

    public static function login(Request $request);
    public static function logout(Request $request);
    public static function getTokenAndRefreshToken(User $user);
}