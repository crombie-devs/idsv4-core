<?php

namespace App\Repositories\Country;

use Illuminate\Http\Request;

interface CountryRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function countries();
    public static function country(int $id);

}