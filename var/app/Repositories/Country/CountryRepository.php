<?php

namespace App\Repositories\Country;

use Illuminate\Http\Request;
use App\Repositories\Country\CountryRepositoryInterface;
use App\Models\Country;
use App\Exceptions\NotFoundException;

class CountryRepository implements CountryRepositoryInterface {

    public static function create(array $input){
        $country = Country::create($input);
        if(!$country) 
            throw new NotFoundException('Error al insertar country');

        return $country;
    }

    public static function update(int $id, array $request){
        $country = Country::where('idcustomer', $id)->first();
        if(!$country)
            throw new NotFoundException('Country no encontrado');

        if(isset($request['name'])) $country->name = $request['name'];

        $country->save();
        return $country;
    }

    public static function delete(int $id){
        $country = Country::where('idcountry', $id)->first();
        if(!$country)
            throw new NotFoundException('Country no encontrado');

        $country->delete();
        return $country;
    }

    public static function country(int $id){
        $country = Country::where('idcountry', $id)->first();
        if(!$country){
            throw new NotFoundException('Country no encontrado');
        }
        return $country;
    }

    public static function countries(){
        $countries = Country::all()->sortBy('name')->values();
        if(!$countries)
            throw new NotFoundException('No se han encontrado countries');

        return $countries;
    }

    public static function toSelect(){
        $countries = Country::get()->sortBy('name')->values();
        return $countries;
    }

}