<?php

namespace App\Repositories\Format;

use Illuminate\Http\Request;

interface FormatRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function formats();
    public static function format(int $id);

}