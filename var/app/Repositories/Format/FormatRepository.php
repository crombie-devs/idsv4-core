<?php

namespace App\Repositories\Format;

use Illuminate\Http\Request;
use App\Repositories\Format\FormatRepositoryInterface;
use App\Models\Format;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class FormatRepository implements FormatRepositoryInterface {

    public static function create(array $input){
        $format = Format::create($input);
        if(!$format) 
            throw new NotFoundException('Error al insertar formato');
        $user = Auth::user();
        if($user->customer)
            $format->customers()->attach($user);
        return $format;
    }

    public static function update(int $id, array $request){
        $format = Format::where('idformat', $id)->first();
        if(!$format)
            throw new NotFoundException('Formato no encontrado');

        if(isset($request['name'])) $format->name = $request['name'];
        if(isset($request['image_url'])) $format->image_url = $request['image_url'];
        if(isset($request['frame_image_url'])) $format->frame_image_url = $request['frame_image_url']; 
        if(isset($request['description'])) $format->description = $request['description']; 
        if(isset($request['with'])) $format->with = $request['with']; 
        if(isset($request['height'])) $format->height = $request['height']; 

        $format->save();
        return $format;
    }

    public static function delete(int $id){
        $format = Format::where('idformat', $id)->first();
        if(!$format)
            throw new NotFoundException('Formato no encontrado');

        $format->delete();
        return $format;
    }

    public static function format(int $id){
        $format = Format::where('idformat', $id)->first();
        if(!$format){
            throw new NotFoundException('Formato no encontrado');
        }
        return $format;
    }

    public static function formats(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $formats = Format::getAllFormats();
        }
        else if($user->is_admin()){
            $formats = Format::getAdminFormats($user);
        }
        else if($user->is_user()){
            $formats = Format::getAdminFormats($user);
        }

        if(!$formats)
            throw new NotFoundException('No se han encontrado formats');

        return $formats;
    }

    public static function toSelect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $formats = Format::getAllFormats();
        }
        else if($user->is_admin()){
            $formats = Format::getAdminFormats($user);
        }
        else if($user->is_user()){
            $formats = Format::getAdminFormats($user);
        }

        if(!$formats)
        throw new NotFoundException('No se han encontrado formats');
        
        return $formats;
    }

    public static function toUser(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $formats = Format::getFormatsPlayerSuperAdmin();
        }
        else if($user->is_admin()){
            $formats = Format::getFormatsPlayerAdmin($user);
        }
        else if($user->is_user()){
            $formats = Format::getFormatsPlayerUser($user);
        }
       
        if(!$formats)
            throw new NotFoundException('No se ha encontrado el format');

        return $formats;
    }

}