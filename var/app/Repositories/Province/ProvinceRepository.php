<?php

namespace App\Repositories\Province;

use Illuminate\Http\Request;
use App\Repositories\Province\ProvinceRepositoryInterface;
use App\Models\Province;
use App\Exceptions\NotFoundException;

class ProvinceRepository implements ProvinceRepositoryInterface {

    public static function toSelect($data = []){
        if(empty($data)){
            $countries = Province::all()->sortBy('name')->values();
        }
        else{
            $countries = Province::where('idcountry',$data)->orderBy('name')->get();
        }
        
        return $countries;
    }
}