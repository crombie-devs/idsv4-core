<?php

namespace App\Repositories\PlayArea;

use Illuminate\Http\Request;
use App\Repositories\PlayArea\PlayAreaRepositoryInterface;
use App\Models\PlayArea;
use App\Models\PlayLogic;
use App\Models\Player;
use App\Models\Content;
use App\Models\ContentPower;
use App\Models\ContentOrder;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class PlayAreaRepository implements PlayAreaRepositoryInterface {

    public static function create(array $request){
        $playarea = new PlayArea();
        $playarea->fill($request);

        //check desde request
        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }

        $playarea->save();

        $playarea = PlayArea::with('location')->where('idarea',$playarea->idarea)->first();
        return $playarea;
    }

    public static function update(int $id, array $request){
        $playarea = PlayArea::find($id);
      
        if(!$playarea){
            throw new NotFoundException('No se ha encontrado el playarea');
        }
        //check desde la BD
        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }

        $playarea->fill($request);

        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }

        $playarea->save();
        $playarea = PlayArea::with('location')->where('idarea',$id)->first();
        return $playarea;            
    }
    
    public static function delete(int $id){
        $playarea = PlayArea::find($id);
        if(!$playarea)
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }
        $playlogics = new PlayLogic();

        $playlogics = $playlogics->where('idarea',$id);
        
        if(!$playlogics)
        throw new NotFoundException('No se ha encontrado el playlogics');


        $playlogics->delete();
        $playarea->delete();

        return true;
    }

    public static function playArea(int $id){
        $playarea = PlayArea::with('location')->with('format')->where('idarea',$id)->first();

        if(!$playarea)            
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid())
            throw new NotValidUserException;

        return $playarea;
    }

    public static function playAreas(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playareas = PlayArea::getSuperAdmin();
        }
        else if($user->is_admin()){
            $playareas = PlayArea::getAdmin($user);
        }
        if(!$playareas)
            throw new NotFoundException('No se ha encontrado el playarea');

        return $playareas;
    }

    public static function toSelect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playareas = PlayArea::getSuperAdmin();
        }else {
            $playareas = PlayArea::getAdmin($user);
        }
        if(!$playareas)
            throw new NotFoundException('No se ha encontrado el playarea');

        return $playareas;
    }

    public static function toShow(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playareas = PlayArea::getSuperAdmin();
        }else if($user->is_admin()){
            $playareas = PlayArea::getAdmin($user);
        } else {
            $playareas = PlayArea::getUser($user);
        }
        if(!$playareas)
            throw new NotFoundException('No se ha encontrado el playarea');

        return $playareas;
    }

    public static function toCreatePlaylist($request){
        $user = Auth::user();
        $playareas = PlayArea::toCreatePlaylist($request, $user);        
        if(!$playareas)
            throw new NotFoundException('No se ha encontrado el playarea');

        return $playareas;
    }

    public static function toUser(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playareas = PlayArea::getPlayAreasPlayerSuperAdmin();
        }
        else if($user->is_admin()){
            $playareas = PlayArea::getPlayAreasPlayerAdmin($user);
        }
        else if($user->is_user()){
            $playareas = PlayArea::getPlayAreasPlayerUser($user);
        }
       
        if(!$playareas)
            throw new NotFoundException('No se ha encontrado el playarea');

        return $playareas;
    }


    public static function updatePlaylogics($request, $idarea){
        $playarea = PlayArea::find($idarea);

        if(!$playarea)            
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }
       
        foreach($playarea->playlogics as $playlogic){
            $playarea->playlogics()->delete($playlogic->idlogic);
        }

        $playlogics = $request->all();
        
        foreach($playlogics['playlogics'] as $playlogic){
            $playlogic['idarea'] = $idarea;
            PlayLogic::create($playlogic);
        }

        $playarea = PlayArea::with('playlogics')->find($idarea);
        return $playarea;
    }

    public static function deletePlaylogic($idarea,$idlogic){
        $playarea = PlayArea::find($idarea);

        if(!$playarea)            
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid())
            throw new NotValidUserException;
            
        $playarea->playlogics()->where('idarea',$idarea)->where('idlogic',$idlogic)->delete();

        return PlayArea::with('playlogics')->find($idarea);
    }

    public static function updateContentsOrder($request, $idarea, $idcategory){
        $user = Auth::user();
        $playarea = PlayArea::find($idarea);
        if($user->is_superadmin()){
            $contents = Content::where('idcategory',$idcategory)->get()->pluck('idcontent');
        }
        else if($user->is_admin()){
            $contents = Content::where('idcategory',$idcategory)->where('idcustomer',$user->idcustomer)->get()->pluck('idcontent');
        }
        else if($user->is_user()){
            $contents = Content::where('idcategory',$idcategory)->where('idcustomer',$user->idcustomer)->where('idsite',$user->idsite)->get()->pluck('idcontent');
        }

        if(!$playarea)            
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid()){
            throw new NotValidUserException;
        }

        if($contents->isEmpty())            
            throw new NotFoundException('No se ha encontrado el content');

        $playarea->contentsorder()->whereIn('idcontent',$contents)->delete($idarea);

        $contentsorder = $request->all();

        foreach($contentsorder['contents'] as $content){
            $content['idarea'] = $idarea;
            ContentOrder::create($content);  
        }

        $playarea = PlayArea::with('contentsorder')->find($idarea);
        return $playarea;
    }

    public static function deletePlayAreaForce($idarea){
        $user = Auth::user();
        $playarea = PlayArea::find($idarea);

        if(!$playarea)            
            throw new NotFoundException('No se ha encontrado el playarea');

        if(!$playarea->isValid())
            throw new NotValidUserException;

       if($user->is_superadmin()){
            $playarea->playlogics()->where('idarea',$idarea)->delete();
        }
        else if($user->is_admin()){
            $playarea->playlogics()->where('idarea',$idarea)->where('idcustomer',$user->idcustomer)->delete();
        }
        else if($user->is_user()){
            $playarea->playlogics()->where('idarea',$idarea)->where('idcustomer',$user->idcustomer)->delete();
        }
        
        $playarea->contentsorder()->where('idarea',$idarea)->delete();

        foreach($playarea->contents as $content){
            $playarea->contents()->detach($content->idcontent);
        }
       
        $player = Player::where('idarea',$idarea);
        $player->update(["deleted"=>true]);

        $playarea->delete();
        return true;
    }
}