<?php

namespace App\Repositories\PlayArea;

use Illuminate\Http\Request;

interface PlayAreaRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function playAreas();
    public static function playArea(int $id);
    public static function toSelect();
    public static function toShow();
    public static function toUser();
    
    public static function updatePlaylogics(Request $request, int $idarea);
    public static function deletePlaylogic(int $idarea, int $idlogic);

}