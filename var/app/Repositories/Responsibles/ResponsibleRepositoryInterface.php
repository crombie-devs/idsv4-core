<?php

namespace App\Repositories\Responsibles;

use Illuminate\Http\Request;

interface ResponsibleRepositoryInterface {
    public static function create(Request $request);
    public static function update(Request $request);
    public static function delete($id);
    public static function responsibles();
}