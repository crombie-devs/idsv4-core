<?php

namespace App\Repositories\Responsibles;

use Illuminate\Http\Request;
use App\Repositories\Responsibles\ResponsibleRepositoryInterface;
use App\Models\TicketResponsible;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class ResponsibleRepository implements  ResponsibleRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $app = TicketResponsible::create($input);
        if(!$app)
            throw new NotFoundException('Error al insertar la app.');

        $responsible = TicketResponsible::where('id', $app->id)->with('department')->get();
        return  $responsible;
    }

    public static function update(Request $request){
        $app = TicketResponsible::find($request['id']);
        if(!$app){
            throw new NotFoundException('No se ha encontrado la app.');
        }

        if(isset($request['name'])) $app->name = $request['name'];
        if(isset($request['email'])) $app->email = $request['email'];
        if(isset($request['id_department'])) $app->id_department = $request['id_department'];
       
        $app->save();
        $responsible = TicketResponsible::where('id',$request['id'])->with('department')->get();
        return  $responsible;
    }

    public static function delete($ids){
       
        foreach($ids->ids as $key => $value) {
            $Responsibles = TicketResponsible::where('id', $value)->delete();
   }
       if (!$Responsibles)
       throw new NotFoundException('No se ha encontrado el departamento');
       return $Responsibles;       
    }

   

    public static function responsibles(){
        $apps = TicketResponsible::where('deleted', false)->with('department')->get();
        if(!$apps){
            throw new NotFoundException('No se han encontrado apps.');
        }
        return $apps;
    }

    

}