<?php

namespace App\Repositories\AudioCategory;


use App\Repositories\AudioCategory\AudioCategoryRepositoryInterface;
use App\Models\AudioCategory;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;



class AudioCategoryRepository  implements AudioCategoryRepositoryInterface
{

    public static function  create(array $request)
    {
        $audio = new AudioCategory();
        $user = Auth::user();

        $audio->idcustomer = $user->idcustomer;
        $audio->fill($request);
        $audio->save();
        return $audio;
    }


    public static function listAudioCategories($idcustomer)
    {


        $audios = AudioCategory::where('idcustomer', $idcustomer)->get();

        if (!$audios)


            throw new NotFoundException('No se ha encontrado la categoria de audio');
        return $audios;
    }



    public static function audioCategory($id)
    {
        $audio = AudioCategory::getCategory($id);
        if (!$audio)
            throw new NotFoundException('No se ha encontrado la categoria de audio');
        return $audio;
    }


    public static function audiosCustomer($idcustomer)
    {

        $audio = AudioCategory::wsFiltertByCustomer($idcustomer);
        if (!$audio)
            throw new NotFoundException('No se ha encontrado la categoria de audio');
        return $audio;
    }




    public static function audioCategoriesToSelect()
    {
        $user = Auth::user();

        $audios = AudioCategory::filtertByCustomer($user->idcustomer);
        if (!$audios)
            throw new NotFoundException('No se han encontrado audios para mostrar');

        return $audios;
    }


    public static function audioCategories()
    {
        $user = Auth::user();
        $audios = AudioCategory::getAdminAudioCategoriesToSelect($user);
        if (!$audios)
            throw new NotFoundException('No se han encontrado audios para mostrar');

        return $audios;
    }


    public static function updateAudioCategory(array $request, $id)
    {
        $audio = AudioCategory::find($id);
        if (!$audio) {
            throw new NotFoundException('No se ha encontrado el audio');
        }
        $audio->fill($request);
        $audio->save();
        return $audio;
    }

    public static function deleteAudioCategory($id)
    {
        $audio = AudioCategory::where('idcategory', $id)->delete();
        if (!$audio)
            throw new NotFoundException('No se ha encontrado la categoria de audio');

        return $audio;
    }
}
