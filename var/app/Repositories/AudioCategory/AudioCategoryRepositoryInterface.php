<?php

namespace App\Repositories\AudioCategory;



interface AudioCategoryRepositoryInterface {
    public static function create(array $request);
    public static function audioCategoriesToSelect();
    public static function audioCategory($id);
    public static function updateAudioCategory(array $request, $id);
    public static function deleteAudioCategory($id);

    
}