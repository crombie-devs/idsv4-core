<?php

namespace App\Repositories\PlayLogic;

use Illuminate\Http\Request;

interface PlayLogicRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function playlogics();
    public static function playlogic(int $id);
}
