<?php

namespace App\Repositories\PlayLogic;

use Illuminate\Http\Request;
use App\Repositories\PlayLogic\PlayLogicRepositoryInterface;
use App\Models\PlayLogic;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class PlayLogicRepository implements PlayLogicRepositoryInterface {

    public static function create(array $request){
        $playlogic = new PlayLogic();
        $playlogic->fill($request);

        //check desde request
        if(!$playlogic->isValid()){
            throw new NotValidUserException;
        }

        $playlogic->save();
        return $playlogic;
    }

    public static function update(int $id, array $request){
        $playlogic = PlayLogic::find($id);
        if(!$playlogic){
            throw new NotFoundException('No se ha encontrado el playlogic.');
        }
        //check desde la BD
        if(!$playlogic->isValid()){
            throw new NotValidUserException;
        }

        $playlogic->fill($request);

        //check desde request
        if(!$playlogic->isValid()){
            throw new NotValidUserException;
        }

        $playlogic->save();
        return $playlogic;
    }

    public static function delete(int $id){
        $playlogic = PlayLogic::find($id);
        if(!$playlogic){
            throw new NotFoundException('No se ha encontrado el playlogic.');
        }

        if(!$playlogic->isValid()){
            throw new NotValidUserException;
        }

        $playlogic->delete();
        return $playlogic;
    }

    public static function playlogic(int $id){
        $playlogic = PlayLogic::getDetail($id);
        if(!$playlogic){
            throw new NotFoundException('No se ha encontrado el playlogic.');
        }
        if(!$playlogic->isValid()){
            throw new NotValidUserException;
        }
        return $playlogic;
    }

    public static function playlogics(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playlogics = PlayLogic::getSuperAdmin();
        }
        else if($user->is_admin()){
            $playlogics = PlayLogic::getAdmin($user);
        }

        if(!$playlogics)
            throw new NotFoundException('No se ha encontrado el content playlogics');

        return $playlogics;
    }

    public static function toselect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $playlogics = PlayLogic::getSuperAdmin();
        }
        else {
            $playlogics = PlayLogic::getAdmin($user);
        }

        if(!$playlogics)
            throw new NotFoundException('No se ha encontrado el content playlogics');

        return $playlogics;
    }

}