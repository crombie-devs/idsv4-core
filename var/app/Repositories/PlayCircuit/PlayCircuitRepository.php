<?php

namespace App\Repositories\PlayCircuit;

use Illuminate\Http\Request;
use App\Repositories\PlayCircuit\PlayCircuitRepositoryInterface;
use App\Models\PlayCircuit;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class PlayCircuitRepository implements PlayCircuitRepositoryInterface {

    public static function create(array $request){
        $user = Auth::user();

        $playcircuit = PlayCircuit::create($request);

        if(!$playcircuit->isValid())
            throw new NotValidUserException;
       
        if(!$playcircuit) 
            throw new NotFoundException('Error al insertar play circuit');

        return $playcircuit;
    }

    public static function update(int $id, array $request){
        $playcircuit = PlayCircuit::where('idcircuit', $id)->first();
        if(!$playcircuit)
            throw new NotFoundException('PlayCircuit no encontrado');

        if(!$playcircuit->isValid())
            throw new NotValidUserException;

        if(isset($request['idcustomer'])) $playcircuit->idcustomer = $request['idcustomer'];
        if(isset($request['name'])) $playcircuit->name = $request['name'];
        if(isset($request['image_url'])) $playcircuit->image_url = $request['image_url'];
        if(isset($request['status'])) $playcircuit->status = $request['status'];
       
        $playcircuit->save();
        return $playcircuit;
    }

    public static function delete(int $id){
        $playcircuit = PlayCircuit::where('idcircuit', $id)->first();
        if(!$playcircuit)
            throw new NotFoundException('No se ha encontrado el PlayCircuit');

        if(!$playcircuit->isValid())
            throw new NotValidUserException;

            foreach($playcircuit->countries as $countries){
                $playcircuit->countries()->detach($countries->idcountry);
            }

            foreach($playcircuit->provinces as $provinces){
                $playcircuit->provinces()->detach($provinces->idprovince);
            }

            foreach($playcircuit->cities as $cities){
                $playcircuit->cities()->detach($cities->idcity);
            }

            foreach($playcircuit->tags as $tags){
                $playcircuit->tags()->detach($tags->idtag);
            }

            foreach($playcircuit->sites as $sites){
                $playcircuit->sites()->detach($sites->idsite);
            }

            foreach($playcircuit->langs as $langs){
                $playcircuit->langs()->detach($langs->idlang);
            }

            foreach($playcircuit->contents as $contents){
                $playcircuit->contents()->detach($contents->idcontent);
            }

        $playcircuit->delete();

        return true;
    }

    public static function playCircuit(int $id){
        $playcircuit = PlayCircuit::with('customer')
            ->with('tags')
            ->with('countries')
            ->with('provinces')
            ->with('cities')
            ->with('sites')
            ->with('langs')
            ->where('idcircuit', $id)->first();

        if(!$playcircuit)
            throw new NotFoundException('No se ha encontrado el PlayCircuit');

        if(!$playcircuit->isValid())
            throw new NotValidUserException;

        return $playcircuit;
    }

    public static function playCircuits(){
        $user = Auth::user();
        
        if(!$user->idcustomer){
            $playcircuit = PlayCircuit::with('langs')->where('deleted', 0)->get();
        }
        else {
            $playcircuit = PlayCircuit::with('langs')->where('deleted', 0)->where('idcustomer',$user->idcustomer )->get();
        }
        if(!$playcircuit)
            throw new NotFoundException('No se ha encontrado el playcircuit');

        return $playcircuit;
    }

    public static function toSelect(){
        $user = Auth::user();
        if(!$user->idcustomer){
            $playcircuit = PlayCircuit::with('langs')->with('sites')->with('countries')->with('provinces')->with('cities')->where('status', 1)->where('deleted', 0)->orderBy('name')->get();
        } else if(!is_null($user->idsite)){
            $playcircuit = PlayCircuit::with('langs')->
                                        with('sites')->
                                        with('countries')->
                                        with('provinces')->
                                        with('cities')->
                                        where('deleted', 0)->
                                        where('status', 1)->
                                        where('idcustomer',$user->idcustomer)->
                                        orderBy('name')->
                                        get();

            $circuits = [];
            foreach($playcircuit as $circuit){
                if(count($circuit->sites) === 1 && $circuit->sites[0]->idsite === $user->idsite){
                    $circuits[] = $circuit;
                }
            }
            $playcircuit = $circuits;
        } else
            $playcircuit = PlayCircuit::with('langs')->
                                        with('sites')->
                                        with('countries')->
                                        with('provinces')->
                                        with('cities')->
                                        where('deleted', 0)->
                                        where('status', 1)->
                                        where('idcustomer',$user->idcustomer)->
                                        orderBy('name')->
                                        get();
        if(!$playcircuit)
            throw new NotFoundException('No se ha encontrado el playcircuit');

        return $playcircuit;
    }

    public static function updateTags($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->tags as $tag){
            $circuit->tags()->detach($tag->idtag);
        }
        $tags = $request->all();
        foreach($tags['tags'] as $tag){
            $circuit->tags()->attach($tag['idtag']);
        }
        $circuit = PlayCircuit::with('tags')->find($idcircuit);

        return $circuit;

    }

    public static function deleteTag($idcircuit,$idtag){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->tags()->detach($idtag);

        return PLayCircuit::with('tags')->find($idcircuit);
    }

    public static function updateCountries($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->countries as $country){
            $circuit->countries()->detach($country->idcountry);
        }
        $countries = $request->all();
        foreach($countries['countries'] as $country){
            $circuit->countries()->attach($country['idcountry']);
        }
        $circuit = PlayCircuit::with('countries')->find($idcircuit);

        return $circuit;

    }

    public static function deleteCountry($idcircuit,$idcountry){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->countries()->detach($idcountry);

        return PLayCircuit::with('countries')->find($idcircuit);
    }

    public static function updateProvinces($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->provinces as $province){
            $circuit->provinces()->detach($province->idprovince);
        }
        $provinces = $request->all();
        foreach($provinces['provinces'] as $province){
            $circuit->provinces()->attach($province['idprovince']);
        }
        $circuit = PlayCircuit::with('provinces')->find($idcircuit);

        return $circuit;

    }

    public static function deleteProvince($idcircuit,$idprovince){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->provinces()->detach($idprovince);

        return PLayCircuit::with('provinces')->find($idcircuit);
    }

    public static function updateCities($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->cities as $city){
            $circuit->cities()->detach($city->idcity);
        }
        $cities = $request->all();
        foreach($cities['cities'] as $city){
            $circuit->cities()->attach($city['idcity']);
        }
        $circuit = PlayCircuit::with('cities')->find($idcircuit);

        return $circuit;

    }

    public static function deleteCity($idcircuit,$idcity){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->cities()->detach($idcity);

        return PLayCircuit::with('cities')->find($idcircuit);
    }

    public static function updateSites($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->sites as $site){
            $circuit->sites()->detach($site->idsite);
        }
        $sites = $request->all();
        foreach($sites['sites'] as $site){
            $circuit->sites()->attach($site['idsite']);
        }
        $circuit = PlayCircuit::with('sites')->find($idcircuit);

        return $circuit;

    }

    public static function deleteSite($idcircuit,$idsite){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->sites()->detach($idsite);

        return PLayCircuit::with('sites')->find($idcircuit);
    }


    public static function updateLangs($request, $idcircuit){
        $circuit = PlayCircuit::find($idcircuit);
        if(!$circuit->isValid()){
            throw new NotValidUserException;
        }
        foreach($circuit->langs as $lang){
            $circuit->langs()->detach($lang->idlang);
        }
        $langs = $request->all();
        foreach($langs['langs'] as $lang){
            $circuit->langs()->attach($lang['idlang']);
        }
        $circuit = PlayCircuit::with('langs')->find($idcircuit);

        return $circuit;

    }

    public static function deleteLang($idcircuit,$idlang){
        $circuit = PLayCircuit::find($idcircuit);
        if(!$circuit->isValid())
            throw new NotValidUserException;

        $circuit->langs()->detach($idlang);

        return PLayCircuit::with('langs')->find($idcircuit);
    }
}