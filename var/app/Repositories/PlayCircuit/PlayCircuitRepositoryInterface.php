<?php

namespace App\Repositories\PlayCircuit;

use Illuminate\Http\Request;

interface PlayCircuitRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function playCircuits();
    public static function playCircuit(int $id);
    public static function toSelect();

    public static function updateTags(Request $request, int $idcircuit);
    public static function deleteTag(int $idcircuit, int $idtag);
    public static function updateCountries(Request $request, int $idcircuit);
    public static function deleteCountry(int $idcircuit, int $idcountry);
    public static function updateProvinces(Request $request, int $idcircuit);
    public static function deleteProvince(int $idcircuit, int $idprovince);
    public static function updateCities(Request $request, int $idcircuit);
    public static function deleteCity(int $idcircuit, int $idcity);
    public static function updateSites(Request $request, int $idcircuit);
    public static function deleteSite(int $idcircuit, int $idsite);

}