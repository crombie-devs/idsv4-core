<?php

namespace App\Repositories\TagCategory;

use Illuminate\Http\Request;
use App\Repositories\TagCategory\TagCategoryRepositoryInterface;
use App\Models\TagCategory;
use App\Models\Tag;
use App\models\Customer;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class TagCategoryRepository implements TagCategoryRepositoryInterface {

    public static function create(array $request){
        $tagCategory = new TagCategory();
        $tagCategory->fill($request);

        //check desde request
        if(!$tagCategory->isValid()){
            throw new NotValidUserException;
        }

        if(!$tagCategory->save())
            throw new NotFoundException('Error al insertar la categoría de tag.');

        return $tagCategory;
    }

    public static function update(int $idcategory, array $request){
        $tagCategory = TagCategory::find($idcategory);
        if(!$tagCategory)
            throw new NotFoundException('No se ha encontrado la categoría de tag.');

        //check desde la BD
        if(!$tagCategory->isValid()){
            throw new NotValidUserException;
        }
        $tagCategory->fill($request);

        //check desde request
        if(!$tagCategory->isValid()){
            throw new NotValidUserException;
        }

        $tagCategory->save();
        return $tagCategory;
    }

    public static function delete(int $idcategory){
        $tagCategory = TagCategory::find($idcategory);
        if(!$tagCategory){
            throw new NotFoundException('No se ha encontrado el tagCategory.');
        }

        if(!$tagCategory->isValid()){
            throw new NotValidUserException;
        }

        $tagCategory->delete();
        return $tagCategory;
    }

    public static function tagCategory(int $idcategory){
        $tagcategory = TagCategory::getDetail($idcategory);
        if(!$tagcategory){
            throw new NotFoundException('No se ha encontrado el tagcategory.');
        }
        if(!$tagcategory->isValid()){
            throw new NotValidUserException;
        }
        return $tagcategory;
    }

    public static function tagCategories(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $tagCategories = TagCategory::getSuperAdmin();
        }
        else {
            $tagCategories = TagCategory::getAdmin($user);
        }

        if(!$tagCategories)
            throw new NotFoundException('No se ha encontrado el content tagCategories');

        return $tagCategories;
    }

    public static function toSelect(){
        $user = Auth::user();

        if($user->is_superadmin()){
            $tagCategories = TagCategory::getSuperAdmin(1);
        }
        else{
            $tagCategories = TagCategory::getAdmin($user,1);
        }

        if(!$tagCategories)
            throw new NotFoundException('No se ha encontrado el content tagCategories');

        return $tagCategories;
    }

}