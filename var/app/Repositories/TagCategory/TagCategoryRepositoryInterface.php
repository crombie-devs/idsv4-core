<?php

namespace App\Repositories\TagCategory;

use Illuminate\Http\Request;

interface TagCategoryRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function tagCategories();
    public static function tagCategory(int $id);
}