<?php

namespace App\Repositories\Otis;

use Illuminate\Http\Request;
use App\Repositories\Otis\OtisRepositoryInterface;
use App\Models\Player;
use App\Exceptions\NotFoundException;

class OtisRepository implements OtisRepositoryInterface {

    public static function auth(string $email){
        $player = Player::getDetailOtisPlayer($email);
        if(!$player)            
            throw new NotFoundException('No se encontro info del player');

        return $player;
    }

}