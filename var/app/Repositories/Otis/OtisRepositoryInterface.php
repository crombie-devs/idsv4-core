<?php

namespace App\Repositories\Otis;

use Illuminate\Http\Request;

interface OtisRepositoryInterface {
    public static function auth(string $email);
}