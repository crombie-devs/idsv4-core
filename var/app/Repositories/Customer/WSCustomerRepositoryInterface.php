<?php

namespace App\Repositories\Customer;

use Illuminate\Http\Request;

interface WSCustomerRepositoryInterface {
    public static function getCircuit(int $idcustomer);
}