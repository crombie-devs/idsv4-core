<?php

namespace App\Repositories\Customer;

use Illuminate\Http\Request;

interface CustomerRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function customers();
    public static function customer(int $id);

    public static function updateFormats(array $request, int $id);
    public static function deleteCustomerFormat(int $idcustomer, int $idformat);
}