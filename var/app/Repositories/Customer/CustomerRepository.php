<?php

namespace App\Repositories\Customer;

use Illuminate\Http\Request;
use App\Repositories\Customer\CustomerRepositoryInterface;
use App\Models\Customer;
use App\Models\ConfigCustomer;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;

use App\Traits\ImageUpload;
use Illuminate\Support\Facades\Auth;

class CustomerRepository implements CustomerRepositoryInterface
{

    public static function customerStatus($id)
    {
        $customer = Customer::find($id);
        $arr = [];
        $arr['status'] = !$customer->status;
        $customer->fill($arr);
        $customer->save();
        return $customer;
    }


    public static function create(array $input)
    {
        $customer = new Customer();
        $customer->fill($input);
        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }
        $customer->save();
        foreach ($input['langs'] as $lang) {
            $customer->attachLang($lang);
        }


        if (!empty($input['config_customer'])) {
            $customer->attachConfigCustomer($input['config_customer']);
        }



        return $customer;
    }

    public static function customerDelete(array $idcustomers)
    {
        $customers = Customer::customerDelete($idcustomers);
        return $customers;
    }

    public static function update(int $id, array $request)
    {
        $customer = Customer::where('idcustomer', $id)->first();
        if (!$customer) {
            throw new NotFoundException('No se ha encontrado el customer.');
        }
        //check desde la BD
        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }
        $customer->fill($request);
        //check desde request
        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }
        $customer->save();
        foreach ($customer->langs as $lang) {
            $customer->langs()->detach($lang->idlang);
        }
        foreach ($request['langs'] as $lang) {
            $customer->attachLang($lang);
        }

        if (!empty($request['config_customer'])) {
            $customer->updateConfigCustomer($request['config_customer']);
        }

        return $customer;
    }

    public static function delete(int $id)
    {
        $customer = Customer::find($id);
        if (!$customer) {
            throw new NotFoundException('No se ha encontrado el customer.');
        }

        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }

        $customer->deleted = true;
        $customer->save();
        return $customer;
    }

    public static function customer(int $id)
    {
        $customer = Customer::with('sites')->with('formats')->with('langs')->with('config_customer')->find($id);
        if (!$customer) {
            throw new NotFoundException('Customer no encontrado.');
        }
        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }
        return $customer;
    }

    public static function customers()
    {
        $customers = Customer::where('deleted', 0)->get();
        if (!$customers)
            throw new NotFoundException('No se han encontrado customers');

        return $customers;
    }

    public static function toselect()
    {
        $user = Auth::user();
        if ($user->is_superadmin()) {
            $customers = Customer::where('status', 1)->where('deleted', 0)->orderBy('name', 'asc')->get();
        } else {
            $customers = Customer::where('status', 1)->where('deleted', 0)->where('idcustomer', $user->idcustomer)->orderBy('name', 'asc')->get();
        }

        if (!$customers)
            throw new NotFoundException('No se ha encontrado customers');

        return $customers;
    }

    public static function deleteCustomerFormat($idcustomer, $idformat)
    {
        $customer = Customer::find($idcustomer);

        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }

        $customer->formats()
            ->where('idcustomer', $idcustomer)
            ->wherePivot('idformat', $idformat)
            ->detach();

        $customer = $customer->with('formats')->get();

        return  $customer;
    }

    public static function deleteCustomerLang($idcustomer, $idlang)
    {
        $customer = Customer::find($idcustomer);

        if (!$customer->isValid()) {
            throw new NotValidUserException;
        }

        $customer->langs()
            ->where('idcustomer', $idcustomer)
            ->wherePivot('idlang', $idlang)
            ->detach();

        $customer = $customer->with('langs')->get();

        return  $customer;
    }

    public static function updateFormats($request, $idcustomer)
    {

        $customers = Customer::find($idcustomer);

        if (!$customers)
            throw new NotFoundException('No se ha encontrado el customer');

        if (!$customers->isValid()) {
            throw new NotValidUserException;
        }

        if ($customers->formats->isNotEmpty()) {
            foreach ($customers->formats as $format) {
                $customers->formats()->detach($format->idformat);
            }
        }

        $formats = $request->all();

        foreach ($formats['formats'] as $format) {
            $customers->formats()->attach($format['idformat']);
        }

        $customers = Customer::with('formats')->find($idcustomer);
        return $customers;
    }

    public static function updateLangs($request, $idcustomer)
    {
        $customers = Customer::find($idcustomer);

        if (!$customers)
            throw new NotFoundException('No se ha encontrado el customer');

        if (!$customers->isValid()) {
            throw new NotValidUserException;
        }

        if ($customers->langs->isNotEmpty()) {
            foreach ($customers->langs as $lang) {
                $customers->langs()->detach($lang->idlang);
            }
        }

        $langs = $request->all();

        foreach ($langs['langs'] as $lang) {
            $customers->langs()->attach($lang['idlang']);
        }

        $customers = Customer::with('langs')->find($idcustomer);
        return $customers;
    }
}
