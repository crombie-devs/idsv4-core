<?php

namespace App\Repositories\Customer;

use Illuminate\Http\Request;
use App\Repositories\Customer\WSCustomerRepositoryInterface;
use App\Models\PlayCircuit;
use App\Exceptions\NotFoundException;

class WSCustomerRepository implements WSCustomerRepositoryInterface {
  
    public static function getCircuit(int $idcustomer){
        $playcircuit = PlayCircuit::getCircuitsByCustomer($idcustomer);
        if(!$playcircuit)            
            throw new NotFoundException('No se encontro info del playcircuit');

        return $playcircuit;
    }

}