<?php

namespace App\Repositories\AssetType;

use Illuminate\Http\Request;
use App\Repositories\AssetType\AssetTypeRepositoryInterface;
use App\Models\AssetType;
use App\Exceptions\NotFoundException;

class AssetTypeRepository implements AssetTypeRepositoryInterface {

    public static function toSelect(){
        
        return AssetType::all();
    }
}