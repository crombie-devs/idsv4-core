<?php

namespace App\Repositories\Product;

use Illuminate\Http\Request;

interface ProductRepositoryInterface {
    public static function updateProductTags(string $idproduct, array $request);
    public static function getProductTags(string $idproduct);
    public static function getAllProductTags(array $request);
    public static function toSelect();
    public static function categoriesToSelect();
    public static function findOneAndUpdate(array $request);
    public static function categoryFindOneAndUpdate(array $request);
    
    public static function getCategoriesWithProductsWithContents();

    public static function create(array $request);
    public static function update(int $idproduct, array $request);
    public static function delete(int $idproduct);

    public static function createCategory(array $request);
    public static function updateCategory(int $idcategory, array $request);
    public static function deleteCategory(int $idcategory);

    public static function detachFromContent(int $idproduct, int $idcontent);
}