<?php

namespace App\Repositories\Product;

use Illuminate\Http\Request;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Customer;
use App\Models\Content;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class ProductRepository implements ProductRepositoryInterface
{

    public static function updateProductTags(string $idproduct, array $tags)
    {
        $product = Product::find($idproduct);
        if (!$product)
            throw new NotFoundException('No se ha encontrado el asset');
        if (!$product->isValid())
            throw new NotValidUserException;
        if (count($product->tags) > 0)
            foreach ($product->tags as $tag) {
                $product->tags()->detach($tag->idtag);
            }
        foreach ($tags as $tag) {
            $product->tags()->attach($tag);
        }
        return Product::with('category')->with('tags')->find($idproduct);
    }

    public static function getAllProductTags(array $request)
    {
        $products = Product::with('tags')->whereIn('idproduct', $request)->get();
        if (!$products)
            throw new NotFoundException('No se ha encontrado el asset');

        return $products;
    }

    public static function getProductTags(string $idproduct)
    {
        $product = Product::find($idproduct);
        if (!$product)
            throw new NotFoundException('No se ha encontrado el asset');
        if (!$product->isValid())
            throw new NotValidUserException;

        return $product->tags();
    }

    public static function toSelect()
    {
        $user = Auth::user();
        $products = Product::with('category')->with('tags')->where('idcustomer', $user->customer->idcustomer)->get();
        if (!$products)
            throw new NotFoundException('No se ha encontrado el asset');
        return $products;
    }

    public static function categoriesToSelect()
    {
        $user = Auth::user();
        $categories = ProductCategory::where('idcustomer', $user->customer->idcustomer)->get();
        if (!$categories)
            throw new NotFoundException('No se ha encontrado el asset');
        return $categories;
    }

    public static function deleteTag($idproduct, $tag)
    {
        $product = Product::find($idproduct);
        if (!$product)
            throw new NotFoundException('No se ha encontrado el asset');
        if (!$product->isValid())
            throw new NotValidUserException;

        $product->tags()->detach($tag);

        return $product;
    }

    public static function findOneAndUpdate(array $data)
    {
        $product = Product::where('code', $data['code'])->where('idcustomer', $data['idcustomer'])->first();
        if (!$product) {
            $product = new Product;
            $product->idcustomer = $data['idcustomer'];
            $product->idcategory = $data['idcategory'];
            $product->code = $data['code'];
            $product->name = $data['name'];
            $product->brand = $data['brand'];
            if (isset($data['supplier']))
                $product->supplier = $data['supplier'];
            $product->save();
            return $product;
        } else {
            return $product;
        }
    }

    public static function categoryFindOneAndUpdate(array $data)
    {
        $category = ProductCategory::where('code', $data['code'])->where('idcustomer', $data['idcustomer'])->first();
        if (!$category) {
            $category = new ProductCategory;

            $category->idcustomer = $data['idcustomer'];
            $category->code = $data['code'];
            $category->name = $data['name'];
            $category->save();
            return $category;
        } else {
            return $category;
        }
    }

    public static function getCategoriesWithProductsWithContents()
    {
        $user = Auth::user();
        $request = ProductCategory::with('products')->where('idcustomer', $user->idcustomer)->get()->toArray();
        return $request;
    }

    public static function create($request)
    {
        $user = Auth::user();
        $request['idcustomer'] = $user->idcustomer;
        $product = new Product();
        $product->fill($request);
        $product->save();

        return $product;
    }
    public static function update($idproduct, $request)
    {
        $product = Product::find($idproduct);
        if (!$product) {
            throw new NotFoundException('No se ha encontrado el player.');
        }
        //check desde la BD
        if (!$product->isValid()) {
            throw new NotValidUserException;
        }
        $product->fill($request);
        //check desde request
        if (!$product->isValid()) {
            throw new NotValidUserException;
        }

        $product->save();
        return $product;
    }
    public static function delete($idproduct)
    {
        $product = Product::find($idproduct);
        if (!$product) {
            throw new NotFoundException('No se ha encontrado el producto.');
        }
        //check desde la BD
        if (!$product->isValid()) {
            throw new NotValidUserException;
        }
        $contents = Content::with('products')->whereHas('products', function ($query) use ($idproduct) {
            $query->where('products.idproduct', $idproduct);
        })->get();
        foreach ($contents as $content) {
            $content->products()->detach($idproduct);
        }
        $product->delete();
        return true;
    }

    public static function createCategory($request)
    {
        $user = Auth::user();
        $request['idcustomer'] = $user->idcustomer;
        $category = new ProductCategory();
        $category->fill($request);
        //check desde request
        if (!$category->isValid()) {
            throw new NotValidUserException;
        }
        $category->save();
        $category::with('products')->where('idcategory', $category->idcategory)->first();
        return $category;
    }
    public static function updateCategory($idcategory, $request)
    {
        $category = ProductCategory::find($idcategory);
        if (!$category) {
            throw new NotFoundException('No se ha encontrado el player.');
        }
        //check desde la BD
        if (!$category->isValid()) {
            throw new NotValidUserException;
        }
        $category->fill($request);
        //check desde request
        if (!$category->isValid()) {
            throw new NotValidUserException;
        }
        $category->save();
        $category::with('products')->where('idcategory', $category->idcategory)->first();
        return $category;
    }
    public static function deleteCategory($idcategory)
    {
        $category = ProductCategory::find($idcategory);
        if (!$category) {
            throw new NotFoundException('No se ha encontrado el player.');
        }
        //check desde la BD
        if (!$category->isValid()) {
            throw new NotValidUserException;
        }
        $products = Product::where('idcategory', $idcategory)->get();
        foreach ($products as $product) {
            $contents = Content::with('products')->whereHas('products', function ($query) use ($product) {
                $query->where('products.idproduct', $product->idproduct);
            })->get();
            foreach ($contents as $content) {
                $content->products()->detach($product->idproduct);
            }
            $product->delete();
        }
        $category->delete();
        return true;
    }
    public static function detachFromContent($idproduct, $idcontent)
    {
        $content = Content::where('idcontent', $idcontent)->first();
        if (!$content) {
            throw new NotFoundException('No se ha encontrado el player.');
        }
        //check desde la BD
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        $content->products()->detach($idproduct);

        return true;
    }
}
