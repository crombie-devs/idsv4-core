<?php

namespace App\Repositories\Messages;

use App\Repositories\Messages\MessageRepositoryInterface;
use App\Models\Messages;
use App\Models\MessageTags;
use App\Models\MessageFormats;
use App\Models\MessageTemplate;
use App\Exceptions\NotFoundException;
use App\Models\Content;
use Illuminate\Support\Facades\Auth;
use DateTime;



class MessageRepository  implements MessageRepositoryInterface
{


    public static function messageByCustomer($id)
    {
        $messages = Content::getContentMessagesByCustomer($id)->get();
        return  $messages;
    }


    public static function deleteMsg($id)
    {

        $message = Messages::where('idcontent', $id)->where('status', 0)->delete();

        return  $message;
    }


    public static function deleteTemplates($id)
    {
        $template = MessageTemplate::where('idtemplate', $id)->delete();
        return $template;
    }




    public  function  createTemplate(array $request)
    {

        $template = new MessageTemplate();
        $t['name_template'] = $request['nameTemplate'];
        $template->fill($t);
        $template->save();
        return $template;
    }

    public  function  updateTemplate(array $request)
    {
        $template = MessageTemplate::find($request['idtemplate']);
        $template->fill($request);
        $template->save();
        return $template;
    }


    public static function  getTemplates()
    {
        $templates = MessageTemplate::get();
        if (!$templates)
            throw new NotFoundException('No se han encontrado Message para mostrar');

        return $templates;
    }





    public static function  get($id)
    {
        $message = Messages::with('tags')->where('id', $id)->get();
        if (!$message)
            throw new NotFoundException('No se han encontrado Message para mostrar');

        return $message;
    }

    public  function  create(array $m)
    {
        $message = new Messages();
        $message->fill($m);
        $message->save();
        return  $message;
    }

    public static function toSelect()
    {
        $messages = Messages::with('formats')->get();
        if (!$messages)
            throw new NotFoundException('No se han encontrado tickts para mostrar');

        return $messages;
    }


    public static function update(array $request)
    {
        $message = Messages::find($request['id']);
        $message->fill($request);
        $message->save();
        $rtags = MessageTags::where('idmessage', $request['id'])->delete();
        foreach ($request['tags'] as $k => $v) {
            if (array_key_exists('tags', $v)) {
                foreach ($v['tags'] as $key => $value) {
                    $tag = new MessageTags();
                    $t['idtag']  = $value['idtag'];
                    $t['idmessage']  = $request['id'];
                    $tag->fill($t);
                    $tag->save();
                }
            }
        }
        return  $message;
    }


    public static function delete($ids)
    {
    }



    public static function getFormats()
    {
        $formats = MessageFormats::all();
        if (!$formats)
            throw new NotFoundException('No se han encontrado tickts para mostrar');
        return $formats;
    }
}
