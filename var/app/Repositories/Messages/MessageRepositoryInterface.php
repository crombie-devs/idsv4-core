<?php

namespace App\Repositories\Messages;
use Illuminate\Http\Request;


interface MessageRepositoryInterface {
    public  function create(array $request);
    public  static function get($id);
    public  static function update(array $request);
  

    
}