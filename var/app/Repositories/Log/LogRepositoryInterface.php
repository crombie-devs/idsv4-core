<?php

namespace App\Repositories\Log;

interface LogRepositoryInterface
{
    public function list();
    public function get($id);
    public function create(array $request);
    // public function delete($id);
}