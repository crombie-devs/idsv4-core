<?php

namespace App\Repositories\Log;

use App\Models\Log;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Repositories\Log\LogRepositoryInterface;

class LogRepository implements LogRepositoryInterface
{

    public function list()
    {
        $entities = Log::with('user')->get();

        if (!$entities) {

            $response = [
                'success' => false,
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => true,
                'data' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count(),
                    'entities' => $entities
                ],
            ];
        }

        return $response;
    }

    public function get($id)
    {
        $entity = Log::find($id)
            ->with('user')
            ->get();

        if (!$entity) {

            $response = [
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json'
                ],
                'data' => $entity
            ];
        }

        return $response;
    }

    public function create(array $request)
    {
        $entity = new Log();

        $entity->fill($request);
        $entity->save();

        return $entity;
    }

    public static function logger($entityName, $action, $status, $params = null, $response = null)
    {
        
        $user = Auth::user();
        
        $entity = new Log();
        $entity->iduser = $user->iduser;
        $entity->identity = 1;
        $entity->entity = $entityName;
        $entity->action = $action;
        $entity->ip = \Request::ip();
        $entity->datetime = Carbon::now()->format('Y-m-d H:i:s');
        $entity->path = \Request::url();
        $entity->status = $status;
        $entity->params = '-';
        $entity->response = '-';

        if ($params && $response) {
            $entity->params = json_encode($params);
            $entity->response = json_encode($response);
        }

        $entity->save();

        return $entity;
    }

    // public function delete($id)
    // {
    // }
}