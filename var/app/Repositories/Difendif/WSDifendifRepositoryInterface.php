<?php

namespace App\Repositories\Difendif;

interface WSDifendifRepositoryInterface
{
    public function list(int $idcustomer, int $ejercicio, int $mes, int $idcampaign);
    public function add(array $request);
    public function delete(int $idcustomer, int $ejercicio, int $mes, int $idcampaign);
}
