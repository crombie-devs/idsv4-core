<?php

namespace App\Repositories\Difendif;

use Carbon\Carbon;

use App\Models\Difendif;
use App\Repositories\Difendif\WSDifendifRepositoryInterface;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;

class WSDifendifRepository implements WSDifendifRepositoryInterface
{

    public function list(int $idcustomer, int $ejercicio, int $mes, int $idcampaign = null)
    {
        if ($idcampaign) {
            $entities = Difendif::where('idcustomer', $idcustomer)->where('ejercicio', $ejercicio)->where('mes', $mes)->where('idcampaign', $idcampaign)->get();
        } else {
            $entities = Difendif::where('idcustomer', $idcustomer)->where('idcustomer', $idcustomer)->where('idcustomer', $idcustomer)->get();
        }

        if (!$entities) {

            $response = [
                'success' => false,
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => true,
                'data' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count(),
                    'entities' => $entities
                ],
            ];
        }

        return $response;
    }

    public function add(array $request)
    {
        $entity = new Difendif();
        $entity->fill($request);
        $entity->save();

        return $entity;
    }

    public function delete(int $idcustomer, int $ejercicio, int $mes, int $idcampaign = null)
    {
        if ($idcampaign) {
            $entity = Difendif::where('idcustomer', $idcustomer)->where('ejercicio', $ejercicio)->where('mes', $mes)->where('idcampaign', $idcampaign)->get();
        } else {
            $entity = Difendif::where('idcustomer', $idcustomer)->where('idcustomer', $idcustomer)->where('idcustomer', $idcustomer)->get();
        }
        
        if(!$entity)
            throw new NotFoundException('No se ha encontrado Difendif.');

        if(!$entity->isValid()){
            throw new NotValidUserException;
        }

        $entity->delete();

        return $entity;
    }
}
