<?php

namespace App\Repositories\Lang;

use Illuminate\Http\Request;
use App\Repositories\Lang\LangRepositoryInterface;
use App\Models\Lang;
use App\Models\User;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use App\Models\IndoorLocation;
use App\Models\Format;
use Illuminate\Support\Facades\Auth;

class LangRepository implements LangRepositoryInterface {

    public static function create(array $request){
        $lang = Lang::create($input);
        if(!$lang) 
            throw new NotFoundException('Error al insertar lang');

        return $lang;
    }

    public static function update(int $id, array $request){
        $lang = Lang::find($id);
        if(!$lang){
            throw new NotFoundException('No se ha encontrado lang.');
        }
        if(isset($request['name'])) $lang->name = $request['name'];
        if(isset($request['image_url'])) $lang->image_url = $request['image_url']; 
       
        $lang->save();
        return $lang;   
    }

    public static function delete(int $id){
        $lang = Lang::find($id);
        if(!$lang)
            throw new NotFoundException('No se ha encontrado Lang.');

        $lang->delete();
        return $lang;
    }

    public static function lang(int $id){
        $lang = Lang::where('idlang', $id)->get();
        if(!$lang)
            throw new NotFoundException('No se han encontrado lang');

        return $lang;
    }

    public static function langs(){
        $user = Auth::user();
        $user = User::find($user->iduser);
        $langs = null;
        
        if($user->is_superadmin()){
            $langs = Lang::all();
        }

        if(!$langs)
            throw new NotFoundException('No se han encontrado langs');

        return $langs;
    }

    public static function toSelect(){
        $user = Auth::user();
        $langs = Lang::getLangsByCustomer($user);
        //$lang = Lang::all()->sortBy('name')->values();
        return $langs;
    }

    public static function toUser(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $langs = Lang::getLangsPlayerSuperAdmin();
        }
        else if($user->is_admin()){
            $langs = Lang::getLangsPlayerAdmin($user);
        }
        else if($user->is_user()){
            $langs = Lang::getLangsPlayerUser($user);
        }
       
        if(!$langs)
            throw new NotFoundException('No se ha encontrado el langs');

        return $langs;
    }
}