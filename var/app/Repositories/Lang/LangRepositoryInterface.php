<?php

namespace App\Repositories\Lang;

use Illuminate\Http\Request;

interface LangRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function langs();
    public static function lang(int $id);

}