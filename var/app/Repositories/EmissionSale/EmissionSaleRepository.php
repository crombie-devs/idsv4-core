<?php

namespace App\Repositories\EmissionSale;

use Illuminate\Http\Request;
use App\Repositories\EmissionSale\EmissionSaleRepositoryInterface;
use GuzzleHttp\Client;
use App\Models\Content;
use App\Models\Site;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use File;
use DB;
use stdClass;

class EmissionSaleRepository implements EmissionSaleRepositoryInterface {

    public static function data(Request $request){
        $data = $request->all();
        $a_dates = $data['dates'];
        $idcampaign = $data['idcontent'];
        $idcustomer = $data['idcustomer'];
        if(isset($data['idsite'])){
            $site = Site::find($data['idsite']);
            $idsite = [$site->code];
        }
            
        $a_sites = [];
        $check = 0;
        $desde_for = $a_dates['minDate_'];
        $hasta_for = $a_dates['maxDate_'];
        $desde_peant = $a_dates['minDate_peant'];
        $hasta_peant = $a_dates['maxDate_peant'];
        $desde_campaign = $a_dates['minDate_campaign'];
        $hasta_campaign = $a_dates['maxDate_campaign'];

        $desde = $desde_for;
        if($desde_for <= $desde_campaign){
            $desde = $desde_campaign;
        }
        $hasta = $hasta_for;
        if($hasta_for >= $hasta_campaign){
            $hasta = $hasta_campaign;
        }
        $ddesde = Carbon::createFromFormat('Y-m-d', $desde);
        $dhasta = Carbon::createFromFormat('Y-m-d', $hasta);
        $ddesde_for = Carbon::createFromFormat('Y-m-d', $desde_for);
        $diferencia_en_dias_total = $ddesde->diffInDays($dhasta);
        $diferencia_dias_inicial = $ddesde_for->diffInDays($ddesde);
        $desde__pa = date("Y-m-d", strtotime($desde_peant .' +'.$diferencia_dias_inicial.' day'));
        $hasta__pa = date("Y-m-d", strtotime($desde__pa .' +'.$diferencia_en_dias_total.' day'));
        if(File::exists(public_path('filesautocalculo/difendif-'.$idcampaign  . '-'. $idcustomer .'.json'))){
            $datafil = File::get(public_path('filesautocalculo/difendif-'.$idcampaign  . '-'. $idcustomer .'.json'));
            $datafil=json_decode($datafil);
        } else {
            return false;
        }
        /*$a_sites_contraste = DB::table('contrast_centers')
            ->where('idcustomer',$idcustomer)
            ->select('center_code')
            ->get()->pluck("center_code")->toArray();*/
        $a_sites_contraste = DB::table('emision_contraste')
            ->where('idcustomer',$idcustomer)
            ->where('idsite', 0)
            ->select('codesite as center_code')
            ->get()->pluck("center_code")->toArray();
        //Todos los sites
        if(!isset($idsite)){
            $aa_sites = self::Fetch_Sites($idcustomer);
            $c_sites = collect(json_decode($aa_sites))->pluck('code');
            $idsite = $c_sites->toArray();
        }
        foreach($datafil as $daf){ 
            if(!empty($daf)){
                if(($daf[0]->fecha >= $desde) && ($daf[0]->fecha <= $hasta)){
                    if(count($daf) == 1){
                        if(isset($idsite) && in_array($daf[0]->codesite, $idsite)){
                            $daf[0]->enperiodoemision = 1;
                            $daf[0]->tiendaemision=1;
                            array_push($a_sites, $daf[0]);
                        }
                        elseif(in_array($daf[0]->codesite, $a_sites_contraste)){ 
                            $daf[0]->enperiodoemision=0;
                            $daf[0]->tiendaemision=1;
                            array_push($a_sites, $daf[0]);
                        }
                    }else{
                        foreach ($daf as  $dt) {
                            if(isset($idsite) && in_array($dt->codesite, $idsite)){
                                $dt->enperiodoemision=1;
                                $dt->tiendaemision=1;
                                array_push($a_sites, $dt);
                            }
                            elseif(in_array($dt->codesite, $a_sites_contraste)){
                                $dt->enperiodoemision=0;
                                $dt->tiendaemision=1;
                                array_push($a_sites, $dt);
                            }
                        }
                    }
                } elseif(($daf[0]->fecha >= $desde__pa) && ($daf[0]->fecha <= $hasta__pa)){
                    if(count($daf)==1){
                        if(isset($idsite) && in_array($daf[0]->codesite, $idsite)){    
                            $daf[0]->enperiodoemision=1;
                            $daf[0]->tiendaemision=0;
                            array_push($a_sites, $daf[0]);
                        }elseif(in_array($daf[0]->codesite, $a_sites_contraste)){
                            $daf[0]->enperiodoemision=0;
                            $daf[0]->tiendaemision=0;
                            array_push($a_sites, $daf[0]);
                        }
                    }else{
                        foreach ($daf as  $dt) {       
                            if(isset($idsite) && in_array($dt->codesite, $idsite)){                         
                                $dt->enperiodoemision=1;
                                $dt->tiendaemision=0;
                                array_push($a_sites, $dt);
                            }elseif(in_array($dt->codesite, $a_sites_contraste)){
                                $dt->enperiodoemision=0;
                                $dt->tiendaemision=0;
                                array_push($a_sites, $dt);
                            }
                        }
                    }
                }
            }
        }
        $a_sites = self::normalizeJsonPython($a_sites);
        $wspython = self::getWsPythonFix('apibp','PUT',$a_sites);
        $getCampaignDtls = Content::where('idcontent',$idcampaign)
                                  ->where('deleted',0)
                                  ->first()
                                  ->toArray();
        $namecampaign = $getCampaignDtls['name'];
        $variacionvmdte = self::convertJsonPython($wspython,$namecampaign);
        $sitesLadorian = self::Fetch_Sites($idcustomer);
        $sitesLadorian = collect(json_decode($sitesLadorian))->pluck("code");
        $c_datadifendif = collect($a_sites);
        $c_datadifendif = $c_datadifendif->where('fecha','>=',$a_dates['minDate_'])->where('fecha','<=',$a_dates['maxDate_'])->whereIn('codestacion',$sitesLadorian);
        $c_dat_group_hour = $c_datadifendif->sortBy('hour')->groupBy('hour');
        $a_dif_usold=[];
        $total_unid_vendidas = 0;
        if($c_dat_group_hour->count() == 24){
            foreach($c_dat_group_hour as $key => $grhour){
                $tmpuniimp = self::getSumatorioUnidades($grhour);
                $total_unid_vendidas = $total_unid_vendidas + $tmpuniimp;
                array_push($a_dif_usold, [$key,$tmpuniimp]);
            }
        }else{
            for ($i = 0; $i < 24; $i++) { 
                if(isset($c_dat_group_hour[$i])){
                    $tmpuniimp = self::getSumatorioUnidades($c_dat_group_hour[$i]);
                    $total_unid_vendidas = $total_unid_vendidas + $tmpuniimp;
                    array_push($a_dif_usold, [$i,$tmpuniimp]);
                }else{
                    array_push($a_dif_usold, [$i,0]);
                }
            } 
        }
        $a_usold_grpe = $a_dif_usold;
        $dayweek = $c_datadifendif->groupBy('weekday');
        $aweekticket = self::santinizeGraphVentasHoraDia($dayweek,'unidades');
        $porcmaxvga=0;
        $total_unid_x = 0;
        $prodname="";

        $ts =  Carbon::now()->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);

        $jsonp = array(
            "customer_id" => intval($idcustomer),
            "date_from"=> $desde_for,
            "date_to"=> $hasta_for
        );

        if(isset($getSite) && !is_null($getSite)){
            $jsonp['site_id']=$getSite->idsite;
        }
        $client = new \GuzzleHttp\Client();
        $responseAfluencias = $client->request('POST','https://people.ladorianids.es/ws/people/count/byHour?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $jsonp]);
        $afluencias = json_decode($responseAfluencias->getBody(), true);
       
        $afluenciasData = $afluencias["data"];

        $a_faceshour=[];
        for ($i=0; $i <24 ; $i++) { 
            $takey = array_search($i, array_column($afluenciasData,'hour'));
            $object = new StdClass;
            if(is_numeric($takey)){
                $object->hour = $i;
                $object->count = $afluenciasData[$takey]['count'];
                array_push($a_faceshour,$object);
            }else{
                $object->hour = $i;
                $object->count = 0;
                array_push($a_faceshour,$object);
            }
        }
        $cont_faceshour = array_sum(array_column($a_faceshour,'count'));
        
        $a_maxv=[];
        foreach ($a_faceshour as $key => $value) {
           array_push($a_maxv,$value->count);
        }

        $hora_punta = array_search(max($a_maxv), $a_maxv);

        if(isset($a_faceshour[$hora_punta])){
            if($cont_faceshour !=0){
                $xdeesaface = ($a_faceshour[$hora_punta]->count / $cont_faceshour) * 100;
                $xdeesaface = round($xdeesaface, 2);
            }else{
                $xdeesaface=0;
            }
        }else{
            $xdeesaface=0;
        }

        $a_maxpunta=[];

        foreach ($a_usold_grpe as $key => $value) {
            array_push($a_maxpunta,$value[1]);
        }
        if(!empty($a_maxpunta)){
            $hora_max_venta = array_search(max($a_maxpunta), $a_maxpunta);
        }
        else{
            $hora_max_venta=0;
        }
        if(isset($a_usold_grpe[$hora_max_venta][1])){
            if($total_unid_vendidas != 0){
                $xdeesaventa = ($a_usold_grpe[$hora_max_venta][1] / $total_unid_vendidas) * 100;
                $xdeesaventa = round($xdeesaventa, 2);
            }else{
                $xdeesaventa=0;
            }
        }else{
            $xdeesaventa=0;
        }

        $totdh = self::getArrayTotHoraDays($aweekticket);
        $whorapunta = array_search(max(array_column($totdh, 'count')),array_column($totdh, 'count'));
        $weekdaymayorafluencia = self::getweekdaymayorafluencia($aweekticket);
        return compact('variacionvmdte','cont_faceshour','xdeesaface','xdeesaventa','a_faceshour','porcmaxvga','hora_punta','hora_max_venta','total_unid_vendidas','total_unid_x','a_usold_grpe','aweekticket','totdh','weekdaymayorafluencia','whorapunta');
    }

    public static function getSumatorioUnidades($data){
        $sumatorio = 0;
        foreach($data as $line){
            $unidades = str_replace(',','.', $line['unidades']);
            $sumatorio += floatVal($unidades);
        }
        return $sumatorio;
    }
    public static function getArrayTotHoraDays($a_week_faces){
        $a_data=[];
        for ($i=0; $i < 24; $i++) { 
            $wfl=0;$wfm=0;$wfx=0;$wfj=0;$wfv=0;$wfs=0;$wfd=0;

            if(isset($a_week_faces[0][$i]->count)){
                $wfl=$a_week_faces[0][$i]->count;
            }
            if(isset($a_week_faces[1][$i]->count)){
                $wfm=$a_week_faces[1][$i]->count;
            }
            if(isset($a_week_faces[2][$i]->count)){
                $wfx=$a_week_faces[2][$i]->count;
            }
            if(isset($a_week_faces[3][$i]->count)){
                $wfj=$a_week_faces[3][$i]->count;
            }
            if(isset($a_week_faces[4][$i]->count)){
                $wfv=$a_week_faces[4][$i]->count;
            }
            if(isset($a_week_faces[5][$i]->count)){
                $wfs=$a_week_faces[5][$i]->count;
            }
            if(isset($a_week_faces[6][$i]->count)){
                $wfd=$a_week_faces[6][$i]->count;
            }
            $tmpsum = $wfl+$wfm+$wfx+$wfj+$wfv+$wfs+$wfd;
            $object = new StdClass;    
            $object->hour =$i;
            $object->count = $tmpsum;
            array_push($a_data,$object);
        }
        return $a_data;
    }

    public static function getweekdaymayorafluencia($a_week_faces){
		$afluenciaweekday = array();
		for ($i=0; $i < 7; $i++) {
			$contador = 0;
			for ($j=0; $j < 24; $j++) 
                if(isset($a_week_faces[$i][$j]->count)){
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
				    $contador += $a_week_faces[$i][$j]->count;			
                    $contador += $a_week_faces[$i][$j]->count;
                }		
			$afluenciaweekday[$i] = $contador;	
		}
		
		$indicemayorvalor = 0;
		$mayor = 0;
		for ($i = 0; $i < 7; $i++) {
			if($afluenciaweekday[$i] > $mayor){
				$indicemayorvalor = $i;
				$mayor=$afluenciaweekday[$i];
			}
		}
		
		switch($indicemayorvalor){
			case 0: $day = __('lunes');break;
			case 1: $day = __('martes');break;
			case 2: $day = __('miercoles');break;
			case 3: $day = __('jueves');break;
			case 4: $day = __('viernes');break;
			case 5: $day = __('sabado');break;
			case 6: $day = __('domingo');break;
			default: $day = __('error');break;			
		}
		return $day;
    }	

    public static function santinizeGraphVentasHoraDia($dayweek,$data_type){
        $a_dayweek=[];
        for ($i = 1; $i <= 7; $i++) { 
            if(!isset($dayweek[$i])){
                $tmp_d = [];
                for ($x=0; $x <24 ; $x++) {
                    $object = new StdClass;
                    $object->hour = $x;
                    $object->count = 0;
                    array_push($tmp_d,$object);
                }       
                array_push($a_dayweek,$tmp_d);
            }else{
                $grpWeekday =$dayweek[$i]->groupBy('hour');
                $tmp_d=[];  
                for ($z=0; $z < 24; $z++) { 
                    if(!isset($grpWeekday[$z])){
                        $object = new StdClass;
                        $object->hour = $z;
                        $object->count = 0;
                        array_push($tmp_d,$object);
                    }else{
                        $object = new StdClass;
                        $object->hour = $z;
                        $object->count = self::sumatorioGrpWeekday($grpWeekday[$z], $data_type);
                        array_push($tmp_d,$object);
                    }
                }
                array_push($a_dayweek,$tmp_d);
            }
        }
        return $a_dayweek;
    }

    public static function sumatorioGrpWeekday($data, $type){
        $sumatorio = 0;
        foreach($data as $line){
            $unidades = str_replace(',','.', $line[$type]);
            $sumatorio += floatVal($unidades);
        }
        return $sumatorio;
    }

    public static function convertJsonPython($a_data,$namecampaign=""){
        $object = new StdClass;
        if($namecampaign !=""){
            $object->name = $namecampaign;
        }

        isset($a_data->idcampaña) ? $object->idcampaign = intval($a_data->idcampaña) : '';
        isset($a_data->IncVentasLd_prom) ? $object->diff_var_uni_em = $a_data->IncVentasLd_prom : '';
        isset($a_data->IncVentasCo_prom) ? $object->diff_var_uni_co = $a_data->IncVentasCo_prom : '';
        isset($a_data->ImpCamp_prom) ? $object->diff_inc_uni = str_replace(',','', number_format(floatval($a_data->ImpCamp_prom),2))  : '';
        isset($a_data->TotalVentaLdEm) ? $object->sum_uni_em_pEm= $a_data->TotalVentaLdEm : '';
        isset($a_data->TotalVentaLdnEm) ? $object->sum_uni_em_preEm= $a_data->TotalVentaLdnEm : '';
        isset($a_data->TotalVentaCoEm) ? $object->sum_var_uni_co_pEm=  ($a_data->TotalVentaCoEm == "NaN") ? 0 : $a_data->TotalVentaCoEm : '';
        isset($a_data->TotalVentaConEm) ? $object->sum_var_uni_co_preEm= $a_data->TotalVentaConEm : '';
        isset($a_data->TotalImporteLdEm) ? $object->sum_var_imp_em_pEm= $a_data->TotalImporteLdEm : '';
        isset($a_data->TotalImporteLdnEm) ? $object->sum_var_imp_em_preEm= $a_data->TotalImporteLdnEm : '';
        isset($a_data->TotalImporteCoEm) ? $object->sum_var_imp_co_pEm= ($a_data->TotalImporteCoEm == "NaN") ? 0 : $a_data->TotalImporteCoEm : '';
        isset($a_data->TotalImporteConEm) ? $object->sum_var_imp_co_preEm= $a_data->TotalImporteConEm : '';
        isset($a_data->IncVentaLdBP) ? $object->sum_inc_uni_em= $a_data->IncVentaLdBP : '';
        isset($a_data->IncVentaCoBP) ? $object->sum_inc_uni_co= ($a_data->IncVentaCoBP == "NaN") ? 0 : $a_data->IncVentaCoBP : '';
        isset($a_data->IncVentadBP) ? $object->sum_inc_uni= ($a_data->IncVentadBP == "NaN") ? 0 : $a_data->IncVentadBP : '';
        isset($a_data->IncImporteLdBP) ? $object->sum_inc_imp_em= $a_data->IncImporteLdBP : '';
        isset($a_data->IncImporteCoBP) ? $object->sum_inc_imp_co=  ($a_data->IncImporteCoBP == "NaN") ? 0 : $a_data->IncImporteCoBP: '';
        isset($a_data->IncImporteBP) ? $object->sum_inc_imp=  ($a_data->IncImporteBP == "NaN") ? 0 : $a_data->IncImporteBP : '';
        isset($a_data->Diff_var_uni_Em) ? $object->diff_var_uni_pEm = str_replace(',','', number_format($a_data->Diff_var_uni_Em,2)) : '';
        isset($a_data->Diff_var_uni_preEm) ? $object->diff_var_uni_preEm = str_replace(',','', number_format($a_data->Diff_var_uni_preEm,2)) : '';
        isset($a_data->Diff_var_uni) ? $object->diif_var_uni = str_replace(',','', number_format($a_data->Diff_var_uni,2)) : '';
        isset($a_data->ImpactoEuros) ? $object->diff_eur = str_replace(',','', number_format($a_data->ImpactoEuros,2)) : '';
        isset($a_data->MLe_prom) ? $object->diff_em_pEm	= str_replace(',','', number_format($a_data->MLe_prom,2)) : '';
        isset($a_data->MLne_prom) ? $object->diff_em_preEm	= str_replace(',','', number_format($a_data->MLne_prom,2)) : '';
        isset($a_data->MCne_prom) ? $object->diff_co_preEm = str_replace(',','', number_format($a_data->MCne_prom,2)) : ''; 
        isset($a_data->MCe_prom) ? $object->diff_co_pEm = str_replace(',','', number_format($a_data->MCe_prom,2)) : '';

        isset($a_data->MLe) ? $object->diff_em_pEm_r	= str_replace(',','', number_format($a_data->MLe,2)) : '';
        isset($a_data->MLne) ? $object->diff_em_preEm_r	= str_replace(',','', number_format($a_data->MLne,2)) : '';
        isset($a_data->MCne) ? $object->diff_co_preEm_r = str_replace(',','', number_format($a_data->MCne,2)) : '';
        isset($a_data->MCe) ? $object->diff_co_pEm_r = str_replace(',','', number_format($a_data->MCe,2)) : '';

        isset($a_data->ImpactoEuros) ? $object->impactoeuros = str_replace(',','', number_format($a_data->ImpactoEuros,2)) : ''; 
        isset($a_data->Peso_Camp) ? $object->pesocamp =  str_replace(',','', number_format($a_data->Peso_Camp,2)) : ''; 
    
        return $object;
    }

    public static function getWsPythonFix($service,$methods='GET',$dat=""){
        try {
            $client = new \GuzzleHttp\Client();
            //apibp
            //$url = "http://localhost:5000/".$service;
            $url = "http://82.223.69.121:5000/".$service;
            $responsePython = $client->request($methods,$url, ['json'=>$dat, 'timeout' => 60]);
            $data = $responsePython->getBody(true)->getContents();
            $res = json_decode(self::jsonFixer($data));
            return reset($res);
        } catch (\GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $responseBody = $exception->getResponse()->getBody(true);
        }
    
        return $res;
    }

    public static function jsonFixer($json){
        $patterns     = [];
        /** garbage removal */
        $patterns[0]  = "/([\s:,\{}\[\]])\s*'([^:,\{}\[\]]*)'\s*([\s:,\{}\[\]])/"; //Find any character except colons, commas, curly and square brackets surrounded or not by spaces preceded and followed by spaces, colons, commas, curly or square brackets...
        $patterns[1]  = '/([^\s:,\{}\[\]]*)\{([^\s:,\{}\[\]]*)/'; //Find any left curly brackets surrounded or not by one or more of any character except spaces, colons, commas, curly and square brackets...
        $patterns[2]  =  "/([^\s:,\{}\[\]]+)}/"; //Find any right curly brackets preceded by one or more of any character except spaces, colons, commas, curly and square brackets...
        $patterns[3]  = "/(}),\s*/"; //JSON.parse() doesn't allow trailing commas
        /** reformatting */
        $patterns[4]  = '/([^\s:,\{}\[\]]+\s*)*[^\s:,\{}\[\]]+/'; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets followed by one or more of any character except spaces, colons, commas, curly and square brackets...
        $patterns[5]  = '/["\']+([^"\':,\{}\[\]]*)["\']+/'; //Find one or more of quotation marks or/and apostrophes surrounding any character except colons, commas, curly and square brackets...
        $patterns[6]  = '/(")([^\s:,\{}\[\]]+)(")(\s+([^\s:,\{}\[\]]+))/'; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets surrounded by quotation marks followed by one or more spaces and  one or more of any character except spaces, colons, commas, curly and square brackets...
        $patterns[7]  = "/(')([^\s:,\{}\[\]]+)(')(\s+([^\s:,\{}\[\]]+))/"; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets surrounded by apostrophes followed by one or more spaces and  one or more of any character except spaces, colons, commas, curly and square brackets...
        $patterns[8]  = '/(})(")/'; //Find any right curly brackets followed by quotation marks...
        $patterns[9]  = '/,\s+(})/'; //Find any comma followed by one or more spaces and a right curly bracket...
        $patterns[10] = '/\s+/'; //Find one or more spaces...
        $patterns[11] = '/^\s+/'; //Find one or more spaces at start of string...
    
        $replacements     = [];
        /** garbage removal */
        $replacements[0]  = '$1 "$2" $3'; //...and put quotation marks surrounded by spaces between them;
        $replacements[1]  = '$1 { $2'; //...and put spaces between them;
        $replacements[2]  = '$1 }'; //...and put a space between them;
        $replacements[3]  = '$1'; //...so, remove trailing commas of any right curly brackets;
        /** reformatting */
        $replacements[4]  = '"$0"'; //...and put quotation marks surrounding them;
        $replacements[5]  = '"$1"'; //...and replace by single quotation marks;
        $replacements[6]  = '\\$1$2\\$3$4'; //...and add back slashes to its quotation marks;
        $replacements[7]  = '\\$1$2\\$3$4'; //...and add back slashes to its apostrophes;
        $replacements[8]  = '$1, $2'; //...and put a comma followed by a space character between them;
        $replacements[9]  = ' $1'; //...and replace by a space followed by a right curly bracket;
        $replacements[10] = ' '; //...and replace by one space;
        $replacements[11] = ''; //...and remove it.
    
        $result = preg_replace($patterns, $replacements, $json);
    
        return $result;
    }

    public static function normalizeJsonPython($a_sites){
        $a_normalize=[];
      
        foreach ($a_sites as $key => $value) { 

            if(!isset($value->idcontent)) {
                $value->idcontent = 0;
            }
          
            str_replace('.',',',$value->total_amount);
            $object['idcampaña'] =  $value->idcontent;
            $object['codestacion'] = $value->codesite;
            $object['idproducto'] = (int)$value->codeprod;
            $object['fecha'] = date('Y-m-d',strtotime($value->fecha));
            $object['unidades'] = (string)$value->total_units;
            $object['importe'] = (string)$value->total_amount;
            $object['EnperiodoCampaña'] = $value->tiendaemision;
            $object['TiendaLadorian'] = $value->enperiodoemision;
            $object['HahabidoEmisiones'] = $value->tieneemision;
            $object['hour']= $value->hour;
            $object['weekday']= $value->weekday;

            array_push($a_normalize,$object);
        }
        return $a_normalize;
    }

    public static function Fetch_Sites($customerid){
        $modSite = new Site();
        $user =  auth()->user();

        if($user->hasRole('admin') && $customerid == $user->idcustomer){
            $sitesOfCustomer = $modSite->sitesOfCustomerConsiderInSales($customerid);
        }
        elseif($user->hasRole('user') && $customerid == $user->idcustomer) {
            if(isset($user->idsite)){
                $sitesOfCustomer = $modSite->siteOfSite($user->idsite);
            }
        }
        else{ 
            $sitesOfCustomer = $modSite->sitesOfCustomerConsiderInSales($customerid);
        }
        
        return json_encode($sitesOfCustomer);
    }

}