<?php

namespace App\Repositories\EmissionSale;

use Illuminate\Http\Request;

interface EmissionSaleRepositoryInterface {
    public static function data(Request $request);

}
