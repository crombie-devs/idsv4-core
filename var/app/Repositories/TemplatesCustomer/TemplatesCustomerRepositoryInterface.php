<?php

namespace App\Repositories\TemplatesCustomer;

use Illuminate\Http\Request;

interface TemplatesCustomerRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);
    public static function createOrUpdate(int $id, Request $request);

    public static function templatesCustomers();
    public static function templatesCustomer(int $id);

    public static function templatesCustomersToSelect();

    public static function getTemplatesCustomerByIdcustomerMaster();
}