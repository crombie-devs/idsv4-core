<?php

namespace App\Repositories\TemplatesCustomer;

use Illuminate\Http\Request;
use App\Repositories\TemplatesCustomer\TemplatesCustomerRepositoryInterface;
use App\Models\Customer;
use App\Models\TemplatesCustomer;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class TemplatesCustomerRepository implements TemplatesCustomerRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $templatesCustomer = TemplatesCustomer::create($input);
        if(!$templatesCustomer)
            throw new NotFoundException('Error al insertar el template Customer.');

        return $templatesCustomer;
    }

    public static function update(int $id, Request $request){
        $templatesCustomer = TemplatesCustomer::find($id);
        if(!$templatesCustomer){
            throw new NotFoundException('No se ha encontrado el Templates Sector.');
        }
        if(isset($request['idcustomer_master'])) $templatesCustomer->idcustomer_master = $request['idcustomer_master'];
        if(isset($request['idcustomer'])){
            $customer = Customer::find($request['idcustomer']);
            $templatesCustomer->idcustomer = $customer->idcustomer;
        }
       
        $templatesCustomer->save();
        return $templatesCustomer;
    }

    public static function delete(int $id){
        $templatesCustomer = TemplatesCustomer::find($id);
        if(!$templatesCustomer){
            throw new NotFoundException('No se ha encontrado el Templates Customer.');
        }
        $templatesCustomer->delete();
        return true;
    }

    public static function templatesCustomer(int $id){
        $templatesCustomer = TemplatesCustomer::with('customer')->where('idtemplatecustomerr', $id)->first();
        if(!$templatesCustomer){
            throw new NotFoundException('No se ha encontrado el Templates Customer.');
        }
        return $templatesCustomer;
    }

    public static function templatesCustomers(){
        $templatesCustomers = TemplatesCustomer::with('customer')->get();
        if(!$templatesCustomers){
            throw new NotFoundException('No se han encontrado Templates Sector.');
        }
        return $templatesCustomers;
    }

    public static function templatesCustomersToSelect(){
        $user = Auth::user();        
        if($user->roles[0]->name == 'superadmin'){
            $templatesCustomers = TemplatesCustomer::get();
        }else{
            $templatesCustomers = TemplatesCustomer::where('idcustomer',$user->customer->idcustomer)->orWhere('idcustomer',null)->get();
        }
        if(!$templatesCustomers){
            throw new NotFoundException('No se han encontrado Templates Sector.');
        }
        return $templatesCustomers;
    }

    public static function createOrUpdate(int $id, Request $request){
        $templatesCustomer = TemplatesCustomer::where('idcustomer', $id)->first();
        if(!$templatesCustomer){
            //Crear
            $input = $request->all();
            $templatesCustomer = TemplatesCustomer::create($input);
            return $templatesCustomer;

        }
        if(isset($request['idcustomer_master'])) $templatesCustomer->idcustomer_master = $request['idcustomer_master'];
        if(isset($request['idcustomer'])){
            $customer = Customer::find($request['idcustomer']);
            $templatesCustomer->idcustomer = $customer->idcustomer;
        }
       
        $templatesCustomer->save();
        return $templatesCustomer;
    }

    public static function getTemplatesCustomerByIdcustomerMaster(){
        $templatesCustomers = TemplatesCustomer::getTemplatesCustomerByIdcustomerMaster();
        if(!$templatesCustomers){
            throw new NotFoundException('No se han encontrado Templates Sector.');
        }
        return $templatesCustomers;
    }

}