<?php

namespace App\Repositories\Audio;



interface AudioRepositoryInterface
{
    public static function create(array $request);
    public static function audioList();
    public static function audioToSelect($id);
    public static function audioToSelectAll();
    public static function updateAudio(array $request, $id);
    public static function deleteAudio($id);
}
