<?php

namespace App\Repositories\Audio;


use App\Repositories\Audio\AudioRepositoryInterface;
use App\Models\Audio;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;



class AudioRepository  implements AudioRepositoryInterface
{

    public static function  create(array $request)
    {

        $audio = new Audio();
        $audio->fill($request);
        $audio->save();
        return $audio;
    }

    public static function audioList()
    {
        $audios = Audio::get();
        if (!$audios)
            throw new NotFoundException('No se han encontrado audios para mostrar');

        return $audios;
    }


    public static function audiosCustomer($idcustomer)
    {

        $audio = Audio::filtertByCustomer($idcustomer);
        if (!$audio)
            throw new NotFoundException('No se ha encontrado la categoria de audio');
        return $audio;
    }





    public static function audioToSelect($id)
    {
        $audios = Audio::where('idcategory', $id)->where('status', 1)->get();
        if (!$audios)
            throw new NotFoundException('No se han encontrado audios para mostrar');

        return $audios;
    }


    public static function audioToSelectAll()
    {
        $audios = Audio::where('status', 1)->get();
        if (!$audios)
            throw new NotFoundException('No se han encontrado audios para mostrar');

        return $audios;
    }


    public static function updateAudio(array $request, $id)
    {
        $audio = Audio::find($id);

        if (!$audio) {
            throw new NotFoundException('No se ha encontrado el audio');
        }

        $audio->fill($request);
        $audio->save();
        return $audio;
    }

    public static function deleteAudio($id)
    {

        $audio_path = Audio::find($id)->getAttributes();


        $exp = explode('/', $audio_path['audio_url']);
        $imagePath = public_path("audio/audiofiles/") . $exp[count($exp) - 1];
        if (file_exists($imagePath))
            unlink($imagePath);

        $audio = Audio::where('idaudio', $id)->delete();


        if (!$audio)
            throw new NotFoundException('No se ha encontrado la categoria de audio');

        return $audio;
    }
}
