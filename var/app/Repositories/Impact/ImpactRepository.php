<?php

namespace App\Repositories\Impact;

use Illuminate\Http\Request;
use App\Repositories\Impact\ImpactRepositoryInterface;
use App\Models\Impact;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;

class ImpactRepository implements ImpactRepositoryInterface {

    public static function summary($request){
        $data = $request->all();
        $user = Auth::user();
        if(!env('DEMO'))
            if(!$user->hasRole('superadmin') && $user->idcustomer != $data['idcustomer'])
                throw new NotValidUserException;

        $summary = Impact::dataSumary($data);
        $graph = Impact::dataGraph($data);
        if(!$summary || !$graph)
            throw new NotFoundException('No se han encontrado datos.');
        
        $result = array();
        $result['summary'] = $summary;
        $result['graph'] = $graph;
        
        return $result;
    }
    public static function graph($request){
        $data = $request->all();
        $user = Auth::user();
        if(!$user->hasRole('superadmin') && $user->idcustomer != $data['idcustomer'])
            throw new NotValidUserException;

        $graph = Impact::dataGraph($data);
        if(!$graph)
            throw new NotFoundException('No se han encontrado datos.');
        return $graph;
    }

}