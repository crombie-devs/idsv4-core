<?php

namespace App\Repositories\Impact;

use Illuminate\Http\Request;

interface ImpactRepositoryInterface {
    public static function summary(Request $request);
    public static function graph(Request $request);
}
