<?php

namespace App\Repositories\Template;

use Illuminate\Http\Request;

interface TemplateRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);

    public static function templates();
    public static function template(int $id);
}