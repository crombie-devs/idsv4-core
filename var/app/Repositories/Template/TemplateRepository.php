<?php

namespace App\Repositories\Template;

use Illuminate\Http\Request;
use App\Repositories\Template\TemplateRepositoryInterface;
use App\Models\Player;
use App\Models\Template;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class TemplateRepository implements TemplateRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $template = Template::create($input);
        if(!$template)
            throw new NotFoundException('Error al insertar el template.');

        return $template;
    }

    public static function update(int $id, Request $request){
        $template = Template::find($id);
        if(!$template){
            throw new NotFoundException('No se ha encontrado el template.');
        }
        if(isset($request['name'])) $template->name = $request['name'];
        if(isset($request['html'])) $template->html = $request['html'];        
        $template->save();
        return $template;
    }

    public static function delete(int $id){
        $template = Template::find($id);
        if(!$template){
            throw new NotFoundException('No se ha encontrado el template.');
        }
        $template->deleted = true;
        $template->save();
        return $template;
    }

    public static function template(int $id){
        $template = Template::find($id);
        if(!$template){
            throw new NotFoundException('No se ha encontrado el template.');
        }
        return $template;
    }

    public static function templates(){
        $templates = null;

     
        $templates = Template::getTemplates();
       

        if(!$templates)
            throw new NotFoundException('No se han encontrado templates');

        return $templates;
    }

    

}