<?php

namespace App\Repositories\Password;

use Illuminate\Http\Request;

interface PasswordRepositoryInterface {
    public function remember(Request $request);
    public function update(Request $request);
    public function response(bool $success, $data, int $statusCode);
}