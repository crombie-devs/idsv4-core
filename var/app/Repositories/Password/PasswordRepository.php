<?php

namespace App\Repositories\Password;

use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Http\Request;
use App\Repositories\Password\PasswordRepositoryInterface;
use Illuminate\Support\Facades\Password;
use App\Notifications\PasswordResetRequestNotification;
use App\Notifications\PasswordResetSuccessNotification;
use Notification;

class PasswordRepository implements PasswordRepositoryInterface {
    const SUCCESS_STATUS_CODE = 200;
    const NOT_FOUND_STATUS_CODE = 404;
    const INVALID_TOKEN_CODE = 400;

    public function remember(Request $request){
        Password::sendResetLink($request->toArray());
        return $this->response(true, ["message" => "Email con el enlace enviado."], self::SUCCESS_STATUS_CODE);
    }

    public function update(Request $request){
     /*  $details = ['greeting' => 'Hi Artisan', 'body' => 'This is my first notification from ItSolutionStuff.com', 
        'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
        'actionText' => 'View My Site','actionURL' => url('/'), 'order_id' => 101
    ];*/
    
        $reset_password_status = Password::reset($request->toArray(), function ($user, $password) {
           $user->password = bcrypt($password);
            $user->save();
            Notification::send($user ,new PasswordResetSuccessNotification());
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return $this->response(false, ["msg" => "Invalid token provided"], self::INVALID_TOKEN_CODE);
        }
        return $this->response(true, $reset_password_status, self::SUCCESS_STATUS_CODE);
    }

    public function response(bool $success, $data, int $statusCode) {
        $response = ["success" => $success, "data"=>$data, "statusCode"=>$statusCode];
        return $response;
    }
}