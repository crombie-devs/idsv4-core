<?php

namespace App\Repositories\ContentType;

use App\Models\ContentType;

use Illuminate\Http\Request;
use App\Exceptions\NotFoundException;
use App\Repositories\ContentType\ContentTypeRepositoryInterface;

class ContentTypeRepository implements ContentTypeRepositoryInterface {

    public static function toSelect(){
        $contenttype = ContentType::all();
        
        if(empty($contenttype)){
            throw new NotFoundException('ContentType no encontrado');
        }
        return $contenttype;
    }
}