<?php

namespace App\Repositories\IndoorLocation;

use Illuminate\Http\Request;

interface IndoorLocationRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function indoorlocations();
    public static function indoorlocation(int $id);

}