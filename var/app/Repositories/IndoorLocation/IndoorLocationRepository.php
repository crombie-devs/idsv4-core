<?php

namespace App\Repositories\IndoorLocation;

use Illuminate\Http\Request;
use App\Repositories\IndoorLocation\IndoorLocationRepositoryInterface;
use App\Models\IndoorLocation;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class IndoorLocationRepository implements IndoorLocationRepositoryInterface {

    public static function create(array $input){
        $indoorlocation = new IndoorLocation();
        $indoorlocation->fill($input);
    
        //check desde request
        if(!$indoorlocation->isValid()){
            throw new NotValidUserException;
        }
    
        $indoorlocation->save();
        return $indoorlocation;
    }

    public static function update(int $id, array $request){
        $indoorlocation = IndoorLocation::find($id);
        if(!$indoorlocation){
            throw new NotFoundException('No se ha encontrado el IndoorLocation.');
        }
        //check desde la BD
        if(!$indoorlocation->isValid()){
            throw new NotValidUserException;
        }
    
        $indoorlocation->fill($request);
    
        //check desde request
        if(!$indoorlocation->isValid()){
            throw new NotValidUserException;
        }
    
        $indoorlocation->save();
        return $indoorlocation;
    }

    public static function delete(int $id){
        $indoorlocation = IndoorLocation::find($id);
        if(!$indoorlocation){
            throw new NotFoundException('No se ha encontrado el IndoorLocation.');
        }

        if(!$indoorlocation->isValid()){
            throw new NotValidUserException;
        }

        $indoorlocation->delete();
        return $indoorlocation;
    }

    public static function indoorlocation(int $id){
        $indoorlocation = IndoorLocation::getDetail($id);
        if(!$indoorlocation){
            throw new NotFoundException('No se ha encontrado el IndoorLocation.');
        }
        if(!$indoorlocation->isValid()){
            throw new NotValidUserException;
        }
        return $indoorlocation;
    }
    
    public static function indoorlocations(){
        $user = Auth::user();

        if($user->is_superadmin()){
            $indoorlocations = IndoorLocation::all();
        }
        else{
            $indoorlocations = IndoorLocation::where('idcustomer',$user->idcustomer)->get();
        }

        if(!$indoorlocations)
            throw new NotFoundException('No se han encontrado IndoorLocation');

        return $indoorlocations;
    }

    public static function toSelect(){
        $user = Auth::user();
        
        if($user->is_superadmin()){
            $indoorlocations = IndoorLocation::all()->sortBy('name')->values();
        }else{
            $indoorlocations = IndoorLocation::where('idcustomer',$user->idcustomer)->orderBy('name')->get();
        }

        if(!$indoorlocations)
            throw new NotFoundException('No se ha encontrado IndoorLocation');
        

        return $indoorlocations;
    }

   

    public static function deleteIndoorLocationForce($idlocation){
        $indoorlocation = IndoorLocation::find($idlocation);

        if(!$indoorlocation)            
            throw new NotFoundException('No se ha encontrado el indoorlocation');

        if(!$indoorlocation->isValid())
            throw new NotValidUserException;

        $indoor = IndoorLocation::deleteIndoorLocationForce($idlocation);    

        return $indoor;
         
    }

}