<?php

namespace App\Repositories\Site;

use Illuminate\Http\Request;

interface SiteRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);

    public static function sites();
    public static function site(int $id);
}