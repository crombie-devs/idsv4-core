<?php

namespace App\Repositories\Site;

use Illuminate\Http\Request;
use App\Repositories\Site\SiteRepositoryInterface;
use App\Models\Site;
use App\Models\App;
use App\Models\SiteHoliday;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;

use Illuminate\Support\Facades\Auth;

class SiteRepository implements SiteRepositoryInterface {

    public static function siteStatus($id) {
        $site = Site::find($id);
        $arr = [];
        $arr['status'] = !$site->status;
        $site->fill($arr);
        $site->save();
        return $site;
          
    }

    public static function create(Request $request){
        $input = $request->all();
        $siteHolidays = [];
        if(isset($input['siteHolidays'])){
            $siteHolidays = $input['siteHolidays'];
        }
            
        $site = Site::create($input);
        if(!$site)
            throw new NotFoundException('Error al insertar la tienda.');

        if(count($siteHolidays) > 1){
            self::attachSiteHolidays($site, $siteHolidays);
        }
        return $site;
    }

    public static function attachSiteHolidays($site, $siteHolidays){die;
        self::deleteObsoleteHolidays($site->idsite,$siteHolidays);
        self::addNewHolidays($site->idsite, $siteHolidays);
    }

    public static function deleteObsoleteHolidays($idsite, $siteHolidays){
        $holidays = SiteHoliday::where('idsite', $idsite)->get()->pluck('idholiday')->array();
        $newHolidays = [];
        foreach($siteHolidays as $holiday)
            if($holiday->idholiday)
                $newHolidays[] = $holiday->idholiday;
            
        foreach($holidays as $holiday)
            if(!in_array($holiday, $newHolidays))
                $holiday->delete();
        
    }

    public static function addNewHolidays($idsite, $siteHolidays){
        foreach($siteHolidays as $siteHoliday){           
            $holiday = SiteHoliday::where('idsite', $idsite)->where('date', $siteHoliday->date)->first();
            if(!$holiday){
                $data['idsite'] = $idsite;
                $data['date'] = $siteHoliday->date;
                $holiday = SiteHoliday::create($data);
                if(!$holiday)
                    throw new NotFoundException('Error al insertar un dia de vacaciones de la tienda.');
            }
        }
    }

    public static function update(int $idsite, Request $request){
        $site = Site::find($idsite);
        $a_request = $request->toArray();

        if(!$site)
            throw new NotFoundException('No se ha encontrado la tienda.');

        //check desde la BD
        if(!$site->isValid()){
            throw new NotValidUserException;
        }

        $site->fill($a_request);

        if(!$site->isValid()){
            throw new NotValidUserException;
        }

        $site->save();
        
        
        if(isset($a_request['site_holidays'])){
            self::updateHolidays($request, $idsite);
        }
        $site = Site::find($idsite);

        return $site;
     }

    public static function delete(int $idsite){
        $site = Site::find($idsite);
        if(!$site)
            throw new NotFoundException('No se ha encontrado la tienda.');

        $site->deleted = true;
        $site->save();
        return $site;
    }

    public static function site(int $idsite){
        $user = Auth::user();
        if(!self::isValid($user, $idsite))
            throw new NotValidUserException;

        $site = Site::with('customer')->with('siteHolidays')->with('tags')->with('playcircuits')->where('idsite', $idsite)->where('deleted', 0)->first();
        if(!$site)
            throw new NotFoundException('No se ha encontrado la tienda.');
        
        $province_country =  Site::getProvinceCountry($site['idcity']);
        $site->setAttribute('idprovince',$province_country['idprovince']);
        $site->setAttribute('idcountry',$province_country['idcountry']);

        return $site;
    }

    public static function sites(){
        $user = Auth::user();
        if(!$user->idcustomer && !$user->idsite){
            $sites = Site::with('customer')->where('deleted', 0)->where('status', 1)->get();
        }else if($user->idcustomer && !$user->idsite){
            $sites = Site::with('customer')->where('idcustomer', $user->idcustomer)->where('deleted', 0)->get();
        }else if($user->idcustomer && $user->idsite){
            $sites = Site::with('customer')->where('idcustomer', $user->idcustomer)->where('idsite', $user->idsite)->where('deleted',0)->get();
        }

        if(!$sites)
            throw new NotFoundException('No se han encontrado tiendas.');
        return $sites;
    }

    public static function isValid($user, $idsite){
        $result = false;
        if($user->idsite && $user->idsite == $idsite){
            $result = true;
        } else if(!$user->idsite && !$user->idcustomer){
            $result = true;
        }else if(!$user->idsite && $user->idcustomer){
            $sites = Site::where('idcustomer', $user->idcustomer)->where('deleted', 0)->get()->pluck('idsite')->toArray();
            if(in_array($idsite, $sites)){
                $result = true;
            }
        }
        return $result;
    }

    public static function toselect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $sites = Site::getSuperAdminSites();
        }
        else if($user->is_admin()){
            $sites = Site::getAdminSites($user);
        } 
        else {
            $sites = Site::getUserSites($user);
        }

        if(!$sites)
            throw new NotFoundException('No se ha encontrado el content sites');

        return $sites;
    }

    public static function updateHolidays($request, $idsite){
        $site = Site::find($idsite);
        if(!$site){
            throw new NotFoundException('No se ha encontrado el site.');
        }
       
        if(!$site->isValid()){
            throw new NotValidUserException;
        }

        $site->siteHolidays()->delete($idsite);
        $holidays = $request->all();
    
        foreach($holidays['site_holidays'] as $holiday){
            $data['idsite'] = $idsite;
            $data['date'] = $holiday['date'];
            $tmpholiday = SiteHoliday::create($data);
            if(!$tmpholiday)
                throw new NotFoundException('Error al insertar holiday.');
        }
        
        $site = Site::with('siteHolidays')->find($idsite);

        return $site;
    }

    public static function deleteHoliday($idsite,$idholiday){
        $site = Site::find($idsite);
        if(!$site->isValid())
            throw new NotValidUserException;

        $site->siteHolidays()->where('idholiday',$idholiday)->delete();
        return Site::with('siteHolidays')->find($idsite);
    }

    public static function updateTags($request, $idsite){
        $site = Site::find($idsite);
        if(!$site){
            throw new NotFoundException('No se ha encontrado el site.');
        }
        if(!$site->isValid()){
            throw new NotValidUserException;
        }
        foreach($site->tags as $tag){
            $site->tags()->detach($tag->idtag);
        }
        $tags = $request->all();
        foreach($tags['tags'] as $tag){
          
            $site->tags()->attach($tag['idtag']);
        }
        $site = Site::with('tags')->find($idsite);

        return $site;
    }

    public static function deleteTag($idsite,$idtag){
        $site = Site::find($idsite);
        if(!$site->isValid())
            throw new NotValidUserException;

        $site->tags()->detach($idtag);

        return Site::with('tags')->find($idsite);
    }
}