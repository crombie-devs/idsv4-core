<?php

namespace App\Repositories\Departments;

use Illuminate\Http\Request;
use App\Repositories\Departments\DepartmentRepositoryInterface;
use App\Models\TicketDepartment;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class DepartmentRepository implements  DepartmentRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $app = TicketDepartment::create($input);
        if(!$app)
            throw new NotFoundException('Error al insertar la app.');

        return $app;
    }

    public static function update(Request $request){
        $app = TicketDepartment::find($request['id']);
        if(!$app){
            throw new NotFoundException('No se ha encontrado la app.');
        }
        if(isset($request['name'])) $app->name = $request['name'];
       
        $app->save();
        return $app;
    }

    public static function delete($ids){
       
        foreach($ids->ids as $key => $value) {
            $departments = TicketDepartment::where('id', $value)->delete();
   }
       if (!$departments)
       throw new NotFoundException('No se ha encontrado el departamento');
       return $departments;       
    }

   

    public static function departments(){
        $apps = TicketDepartment::where('deleted', false)->get();
        if(!$apps){
            throw new NotFoundException('No se han encontrado apps.');
        }
        return $apps;
    }

    

}