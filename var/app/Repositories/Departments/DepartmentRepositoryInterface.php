<?php

namespace App\Repositories\Departments;

use Illuminate\Http\Request;

interface DepartmentRepositoryInterface {
    public static function create(Request $request);
    public static function update(Request $request);
    public static function delete(int $id);
    public static function departments();
}