<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;

interface WSPlayerRepositoryInterface {
    public static function getPlaylist(string $idcustomer, string $idarea);
    public static function auth(string $email);
    public static function saveEmissions(int $idplayer, array $request);
}