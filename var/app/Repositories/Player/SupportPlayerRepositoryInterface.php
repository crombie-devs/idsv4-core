<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;

interface SupportPlayerRepositoryInterface {
    public static function totalPlayerGroupByCustomer();
    public static function totalPlayerGroupBySite(string $idcustomer);
    public static function getPlayerBySite(string $idsite);
    public static function getPlayerByCustomer(string $idcustomer);
}