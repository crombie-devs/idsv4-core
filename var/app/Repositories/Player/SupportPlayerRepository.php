<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;
use App\Repositories\Player\SupportPlayerRepositoryInterface;
use App\Models\Player;
use App\Models\PlayLogic;
use App\Models\Content;
use App\Exceptions\NotFoundException;

class SupportPlayerRepository implements SupportPlayerRepositoryInterface { 

    public static function getPlayerBySite($idsite){
        
        $players = Player::getPlayerBySite($idsite);
        
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');

        return $players;
    }

    public static function getPlayerByCustomer($idcustomer){
        
        $players = Player::getPlayerByCustomer($idcustomer);
        
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');

        return $players;
    }
    
    
    
    public static function totalPlayerGroupByCustomer(){
        
        $players = Player::totalPlayerGroupByCustomer();
        /* foreach($customers as $c){
            $players = Player::getPlayerByCustomer($c->idcustomer);
            foreach($players as $p){
			    $array[] = $p->idplayer;

			}
            $customer['players']=$array; 
          
        }
        var_dump($customer); */
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');

        return $players;
    }

    public static function totalPlayerGroupBySite($idcustomer){
        
        $players = Player::totalPlayerGroupBySite($idcustomer);
        
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');

        return $players;
    }

    public static function getPlayer(){
        
        $players = Player::getPlayer();
        
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');

        return $players;
    }

}