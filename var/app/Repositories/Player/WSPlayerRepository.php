<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;
use App\Repositories\Player\WSPlayerRepositoryInterface;
use App\Models\Player;
use App\Models\PlayLogic;
use App\Models\Content;
use App\Models\PlayerEmission;
use App\Exceptions\NotFoundException;

class WSPlayerRepository implements WSPlayerRepositoryInterface {

    public static function getPlaylist(string $idcustomer, string $idarea){
        $playlist = PlayLogic::getplaylist($idcustomer, $idarea);
        if(!$playlist)            
            throw new NotFoundException('No se ha encontrado playlist');

            return $playlist;
    }

    public static function auth(string $email){
        $player = Player::getDetailPlayer($email);
        if(!$player)            
            throw new NotFoundException('No se encontro info del player');

        return $player;
    }

    public static function findByCode(string $code){
        $player = Player::select('idplayer', 'email')->where('code', $code)->first();
        if(!$player)            
            throw new NotFoundException('No se encontro info del player');

        return $player;
    }

    public static function playlogics(int $idplayer){
        $playlogics = Player::getListPlayLogics($idplayer);
        if(!$playlogics)            
            throw new NotFoundException('No se encontraron playlogics');

        return $playlogics;
    }

    public static function contents(int $idplayer, string $date){
        $contents = Content::getContentsForPlayer($idplayer, $date);
        if(!$contents)            
            throw new NotFoundException('No se encontraron contents');

        return $contents;
    }

    public static function contentsBySite(int $idsite){
        $contents = Content::getContentsForSite($idsite);
        if(!$contents)            
            throw new NotFoundException('No se encontraron contents');

        return $contents;
    }

    public static function saveEmissions($idplayer, $data){
        /*$emission = PlayerEmission::updateOrCreate(
            ['idplayer' => $idplayer, 'weekday' => $data['weekday']],
            ['num_loop' => $data['num_loop'], 'total_emissions' => $data['total_emissions']] 
        );*/
        $emission = PlayerEmission::where('idplayer', $idplayer)->where('weekday', $data['weekday'])->first();
        if($emission){
            $emission = PlayerEmission::where('idplayer', $idplayer)->where('weekday', $data['weekday'])->update([
                'num_loop' => $data['num_loop'], 
                'total_emissions' => $data['total_emissions']
            ]);
        }else{
            $emission = PlayerEmission::create([
                "idplayer" => $idplayer,
                "weekday" => $data['weekday'],
                "num_loop" => $data['num_loop'],
                "total_emissions" => $data['total_emissions']
            ]);
        }
        return $emission;
    }

}