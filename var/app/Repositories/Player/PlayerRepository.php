<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;
use App\Repositories\Player\PlayerRepositoryInterface;
use App\Models\Player;
use App\Models\PlayerPower;
use App\Models\PlayLogic;
use App\Models\Tag;
use App\Models\Content;
use App\Models\ContentOrder;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class PlayerRepository implements PlayerRepositoryInterface {


    public static function playerStatus($id) {
        $player = Player::find($id);
        $arr = [];
        $arr['status'] = !$player->status;
        $player->fill($arr);
        $player->save();
        return $player;
          
    }
    



    public static function create(array $request){
        $player = new Player();
        $player->fill($request);
        //check desde request
        if(!$player->isValid()){
            throw new NotValidUserException;
        }
        $player->save();
        if(isset($request['tags'])){
            if($request['tags']) self::attachTags($player, $request['tags']);
        }        
        if(isset($request['powers'])){
            if($request['powers']) self::createPowers($player, $request['powers']);
        }
        return $player;
    }

    public static function attachTags($player, $tags){
        foreach($tags as $tag){
            $player->tags()->attach($tag->idtag);
        }
    }

    public static function createPowers($player, $powers){
        foreach($powers as $power){
            $data['idplayer'] = $player->idplayer;
            $data['weekday'] = $power['weekday'];
            $data['time_on'] = $power['time_on'];
            $data['time_off'] = $power['time_off'];
            $playerpower = PlayerPower::create($data);
            if(!$playerpower)
                throw new NotFoundException('Error al insertar player power');
        }
    }

    public static function update(int $id, array $request){
        $player = Player::find($id);
        if(!$player){
            throw new NotFoundException('No se ha encontrado el player.');
        }
        //check desde la BD
        if(!$player->isValid()){
            throw new NotValidUserException;
        }
        $player->fill($request);
        //check desde request
        if(!$player->isValid()){
            throw new NotValidUserException;
        }
        if(isset($request['tags'])){
            if($request['tags']) self::attachTags($player, $request['tags']);
        }
        if(isset($request['powers'])){
            if($request['powers']) self::createPowers($player, $request['powers']);
        }

        $player->save();
        return $player;        
    }

    public static function delete(int $id){
        $player = Player::find($id);
        if(!$player){
            throw new NotFoundException('No se ha encontrado el player.');
        }
        if(!$player->isValid()){
            throw new NotValidUserException;
        }
        $player->deleted = true;
        $player->save();
        return $player;
    }

    public static function player(int $id){
        $player = Player::getPlayerData($id);
        if(!$player)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$player->isValid()){
            throw new NotValidUserException;
        }
        return $player;
    }

    public static function players(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $players = Player::getSuperAdminPlayers();
        }
        else if($user->is_admin()){
            $players = Player::getAdminPlayers($user);
        }
        else if($user->is_user()){
            $players = Player::getUserPlayers($user);
        }
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');
        return $players;
    }


    public static function toAnalytics($request){
        $user = Auth::user();
        $params = $request->all();
        if($params['idcustomer'] != $user->idcustomer){
            throw new NotValidUserException;
        }else if($user->idsite && $params['idsite'] != $user->idsite){
            throw new NotValidUserException;
        }
        $players = Player::getToAnalytics($params);
        if(!$players)
            throw new NotFoundException('No se ha encontrado el player');
        return $players;
    }

    public static function toselect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $players = Player::getSuperAdminPlayersToselect();
        } else if($user->is_admin()){
            $players = Player::getAdminPlayersToselect($user);
        } else {
            $players = Player::getUserPlayersToselect($user);
        }
        if(!$players)
            throw new NotFoundException('No se ha encontrado el content players');
        return $players;
    }

    public static function updatePlaylogics($request, $idplayer){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid()){
            throw new NotValidUserException;
        }
        foreach($players->playlogics as $playlogic){
            $players->playlogics()->delete($playlogic->idlogic);
        }
        $playlogics = $request->all();
        foreach($playlogics['playlogics'] as $playlogic){
            $playlogic['idplayer'] = $idplayer;
            PlayLogic::create($playlogic);
        }
        //$players = Player::with('playlogics')->find($idplayer);
        $playlogic = Playlogic::with('type')->with('contentCategory:idcategory,name')->where('idplayer',$idplayer)->orderBy('order', 'ASC')->get();
        return $playlogic;
    }

    public static function deletePlaylogic($idplayer,$idlogic){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid())
            throw new NotValidUserException;
        $players->playlogics()->where('idplayer',$idplayer)->where('idlogic',$idlogic)->delete();
        return Player::with('playlogics')->find($idplayer);
    }

    public static function updateContentsOrder($request, $idplayer, $idcategory){
        $user = Auth::user();
        $players = Player::find($idplayer);
        if($user->is_superadmin()){
            $contents = Content::where('idcategory',$idcategory)->get()->pluck('idcontent');
        } else if($user->is_admin()){
            $contents = Content::where('idcategory',$idcategory)->where('idcustomer',$user->idcustomer)->get()->pluck('idcontent');
        } else if($user->is_user()){
            $contents = Content::where('idcategory',$idcategory)->where('idcustomer',$user->idcustomer)->where('idsite',$user->idsite)->get()->pluck('idcontent');
        }
        if(!$players)            
           throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid()){
            throw new NotValidUserException;
        }
        if($contents->isEmpty())            
            throw new NotFoundException('No se ha encontrado el content');
        $players->contentsorder()->whereIn('idcontent',$contents)->delete($idplayer);
        $contentsorder = $request->all();
        foreach($contentsorder['contents'] as $content){
            $content['idplayer'] = $idplayer;
            ContentOrder::create($content);  
        }
        $players = Player::with('contentsorder')->find($idplayer);
        return $players;
    }

    public static function updateTags($request, $idplayer){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid()){
            throw new NotValidUserException;
        }
        foreach($players->tags as $tag){
            $players->tags()->detach($tag->idtag);
        }
        $tags = $request->all();
        foreach($tags['tags'] as $tag){
            $players->tags()->attach($tag['idtag']);
        }
        $players = Player::with('tags')->find($idplayer);
        return $players;
    }

    public static function deleteTag($idplayer,$idtag){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid())
            throw new NotValidUserException;
        $players->tags()->detach($idtag);
        return Player::with('tags')->find($idplayer);
    }

    public static function updatePowers($request, $idplayer){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid()){
            throw new NotValidUserException;
        }
        foreach($players->powers as $power){
            $players->powers()->delete($power->idtime);
        }
        $powers = $request->all();
        foreach($powers['powers'] as $power){
            $power['idplayer'] = $idplayer;
            PlayerPower::create($power);
        }
        $players = Player::with('powers')->find($idplayer);
        return $players;
    }

    public static function deletePower($idplayer,$idtime){
        $players = Player::find($idplayer);
        if(!$players)            
            throw new NotFoundException('No se ha encontrado el player');
        if(!$players->isValid())
            throw new NotValidUserException;
        $players->powers()->where('idplayer',$idplayer)->where('idtime',$idtime)->delete();
        return Player::with('powers')->find($idplayer);
    }
}