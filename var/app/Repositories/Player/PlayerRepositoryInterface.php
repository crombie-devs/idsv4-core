<?php

namespace App\Repositories\Player;

use Illuminate\Http\Request;

interface PlayerRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function players();
    public static function player(int $id);
    public static function toAnalytics(Request $request);
}