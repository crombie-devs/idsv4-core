<?php

namespace App\Repositories\ContentCategory;

use Illuminate\Http\Request;
use App\Repositories\ContentCategory\ContentCategoryRepositoryInterface;
use App\Models\ContentCategory;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class ContentCategoryRepository implements ContentCategoryRepositoryInterface {
    
    public static function create(array $request){
        $contentcategory = new ContentCategory();
        $contentcategory->fill($request);

        //check desde request
        if(!$contentcategory->isValid()){
            throw new NotValidUserException;
        }

        if(!$contentcategory->save())
            throw new NotFoundException('Error al insertar el contencategory.');

        return $contentcategory;
    }

    public static function update(int $id, array $request){
        $contentcategory = ContentCategory::find($id);
        if(!$contentcategory)
            throw new NotFoundException('No se ha encontrado el Content Category.');

        //check desde la BD
        if(!$contentcategory->isValid()){
            throw new NotValidUserException;
        }

        $contentcategory->fill($request);

        //check desde request
        if(!$contentcategory->isValid()){
            throw new NotValidUserException;
        }

        $contentcategory->save();
        return $contentcategory;
    }

    public static function delete(int $id){
        $contentcategory = ContentCategory::find($id);
        if(!$contentcategory){
            throw new NotFoundException('No se ha encontrado el ContentCategory.');
        }

        if(!$contentcategory->isValid()){
            throw new NotValidUserException;
        }

        $contentcategory->delete();
        return $contentcategory;
    }

    public static function contentcategory(int $id){
        $contentcategory = ContentCategory::where('idcategory', $id)->first();
        if(!$contentcategory){
            throw new NotFoundException('No se ha encontrado el ContentCategory.');
        }
        if(!$contentcategory->isValid()){
            throw new NotValidUserException;
        }
        return $contentcategory;
    }
    
    public static function contentCategories(){
        $user = Auth::user();

        if($user->is_superadmin()){
            $contentCategories = ContentCategory::all();
        }else if($user->is_admin()){
            $contentCategories = ContentCategory::getUserContentCategories($user);
        }else if($user->is_user()){
            $contentCategories = ContentCategory::getUserContentCategories($user);
        }

        if(!$contentCategories)
            throw new NotFoundException('No se ha encontrado content categories');

        return $contentCategories;
    }

    public static function toSelect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $contentCategories =  ContentCategory::where('name','!=','null')->orderBy('name')->get();
        }else if($user->is_admin()){
            $contentCategories = ContentCategory::getUserContentCategories($user);
        }else if($user->is_user()){
            $contentCategories = ContentCategory::getUserContentCategories($user);
        }

        if(!$contentCategories)
            throw new NotFoundException('No se ha encontrado content categories');
        
        return $contentCategories;
    }
}

