<?php

namespace App\Repositories\ContentCategory;

use Illuminate\Http\Request;

interface ContentCategoryRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function contentCategories();
    public static function contentCategory(int $id);
    public static function toSelect();

}