<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;

interface WSCameraRepositoryInterface {
    public static function auth(string $email);
}