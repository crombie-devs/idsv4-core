<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;

interface CameraRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function cameras();
    public static function camera(int $id);
    public static function toAnalytics(Request $request);
}