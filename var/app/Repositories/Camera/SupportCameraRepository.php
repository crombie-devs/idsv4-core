<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;
use App\Repositories\Camera\SupportCameraRepositoryInterface;
use App\Models\Camera;
use App\Exceptions\NotFoundException;

class SupportCameraRepository implements SupportCameraRepositoryInterface { 

    public static function getCameraBySite($idsite){
        
        $cameras = Camera::getCameraBySite($idsite);
        
        if(!$cameras)
            throw new NotFoundException('No se ha encontrado la cámara');

        return $cameras;
    }

    public static function getCameraByCustomer($idcustomer){
        
        $cameras = Camera::getCameraByCustomer($idcustomer);
        
        if(!$cameras)
            throw new NotFoundException('No se ha encontrado la cámara');

        return $cameras;
    }
    
    
    
    public static function totalCameraGroupByCustomer(){
        
        $cameras = Camera::totalCameraGroupByCustomer();
        
        if(!$cameras)
            throw new NotFoundException('No se ha encontrado el player');

        return $cameras;
    }

    public static function totalCameraGroupBySite($idcustomer){
        
        $cameras = Camera::totalCameraGroupBySite($idcustomer);
        
        if(!$cameras)
            throw new NotFoundException('No se ha encontrado la cámara');

        return $cameras;
    }

    public static function getCamera(){
        
        $cameras = Camera::getCamera();
        
        if(!$cameras)
            throw new NotFoundException('No se ha encontrado la camara');

        return $cameras;
    }

}