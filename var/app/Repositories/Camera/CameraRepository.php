<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;
use App\Repositories\Camera\CameraRepositoryInterface;
use App\Models\Camera;
use App\Models\CameraPower;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;

use Illuminate\Support\Facades\Auth;

class CameraRepository implements CameraRepositoryInterface {

    
    public static function cameraStatus($id) {
        $camera = Camera::find($id);
        $arr = [];
        $arr['status'] = !$camera->status;
        $camera->fill($arr);
        $camera->save();
        return $camera;
          
    }
    

    public static function create(array $request){
        $camera = new Camera();
        $camera->fill($request);

        //check desde request
        if(!$camera->isValid()){
            throw new NotValidUserException;
        }

        $camera->save();

        if($request['powers']) self::createPowers($camera, $request['powers']);
        return $camera;
    }

    public static function update(int $id, array $request){
        $camera = Camera::find($id);

        if(!$camera){
            throw new NotFoundException('No se ha encontrado la cámara');
        }

        //check desde la BD
        if(!$camera->isValid()){
            throw new NotValidUserException;
        }

        $camera->fill($request);

        //check desde request
        if(!$camera->isValid()){
            throw new NotValidUserException;
        }

        $camera->save();
        if(isset($request['powers'])) self::updatePowersCamera($camera, $request['powers']);
        return $camera;            
    }

    public static function delete(int $id){
        $camera = Camera::where('idcamera', $id)->first();
        if(!$camera)
            throw new NotFoundException('No se ha encontrado el player');
        
        if(!$camera->isValid()){
            throw new NotValidUserException;
        }

        $camera->deleted = true;
        $camera->save();
        return $camera;
    }

    public static function camera(int $id){
        $user = Auth::user();

        $camera = Camera::getCameraData($id);
        if(!$camera)            
            throw new NotFoundException('No se ha encontrado la cámara');

        if(!$camera->isValid()){
            throw new NotValidUserException;
        }

        return $camera;
    }

    public static function cameras(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $cameras = Camera::getSuperAdminCameras();
        }
        else if($user->is_admin()){
            $cameras = Camera::getAdminCameras($user);
        }
        else if($user->is_user()){
            $cameras = Camera::getUserCameras($user);
        }
        if(!$cameras)
            throw new NotFoundException('No se han encontrado cámaras para mostrar');

        return $cameras;
    }

    public static function toAnalytics($request){
        $user = Auth::user();
        $params = $request->all();

        if($params['idcustomer'] != $user->idcustomer){
            throw new NotValidUserException;
        }else if($user->idsite && $params['idsite'] != $user->idsite){
            throw new NotValidUserException;
        }
        $cameras = Camera::getToAnalytics($params);

        if(!$cameras)
            throw new NotFoundException('No se has encontrado cámaras');

        return $cameras;
    }

    public static function toSelect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $cameras = Camera::getSuperAdminCameras(1);
        }
        else if($user->is_admin()){
            $cameras = Camera::getAdminCameras($user,1);
        }
        else if($user->is_user()){
            $cameras = Camera::getUserCameras($user,1);
        }
        if(!$cameras)
            throw new NotFoundException('No se han encontrado cámaras para mostrar');

        return $cameras;
    }

    public static function createPowers($camera, $powers){
        if (count($powers) != count($powers, COUNT_RECURSIVE)) { 
            foreach($powers as $power){
                $power['idcamera'] = $camera->idcamera;
                $oPower = new CameraPower();
                $oPower->fill($power);
                $oPower->save();
            }
        }
        else{
            $powers['idcamera'] = $camera->idcamera;
            $oPower = new CameraPower();
            $oPower->fill($powers);
            $oPower->save();
        }
    }

    public static function updatePowersCamera($camera, $powers){
        self::deleteCameraPowers($camera->idcamera, $powers);
        self::createPowers($camera, $powers);
    }
    
    public static function deleteCameraPowers($camera, $powers){
        $camerapowers = CameraPower::where('idcamera', $camera);
        if($camerapowers->get()->isNotEmpty()){
            $camerapowers->delete();
        }
        return true;
    }

    public static function updatePowers($request, $idcamera){
        $cameras = Camera::find($idcamera);

        if(!$cameras)            
            throw new NotFoundException('No se ha encontrado el camera');

        if(!$cameras->isValid()){
            throw new NotValidUserException;
        }
       
        foreach($cameras->powers as $power){
            $cameras->powers()->delete($power->idpower);
        }

        $powers = $request->all();

        foreach($powers['powers'] as $power){
            $power['idcamera'] = $idcamera;
            CameraPower::create($power);
        }

        $cameras = Camera::with('powers')->find($idcamera);
        return $cameras;
    }

    public static function deletePower($idcamera,$idpower){
        $cameras = Camera::find($idcamera);

        if(!$cameras)            
            throw new NotFoundException('No se ha encontrado el camera');

        if(!$cameras->isValid())
            throw new NotValidUserException;
            
        $cameras->powers()->where('idcamera',$idcamera)->where('idpower',$idpower)->delete();

        return Camera::with('powers')->find($idcamera);
    }
}