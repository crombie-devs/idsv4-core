<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;
use App\Repositories\Camera\WSCameraRepositoryInterface;
use App\Models\Camera;
use App\Exceptions\NotFoundException;

class WSCameraRepository implements WSCameraRepositoryInterface {
  
    public static function auth(string $email){
        $camera = Camera::getDetailCamera($email);
        if(!$camera)            
            throw new NotFoundException('No se encontro info del camera');

        return $camera;
    }

}