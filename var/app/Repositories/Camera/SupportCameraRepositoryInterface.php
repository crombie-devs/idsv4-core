<?php

namespace App\Repositories\Camera;

use Illuminate\Http\Request;

interface SupportCameraRepositoryInterface {
    public static function getCameraBySite(string $idsite);
    public static function getCameraByCustomer(string $idcustomer);
    public static function totalCameraGroupByCustomer();
    public static function totalCameraGroupBySite(string $idcustomer);
  
}