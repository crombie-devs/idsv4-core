<?php

namespace App\Repositories\Content;

use Illuminate\Http\Request;
use App\Repositories\Content\ContentRepositoryInterface;
use App\Models\Content;
use App\Models\PrimeTime;
use App\Models\Product;
use App\Models\ContentPower;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Traits\TicketWs;
use App\Models\Asset;
use DB;

class ContentRepository implements ContentRepositoryInterface
{
    use TicketWs;



    public static function contentStatus($id)
    {
        $content = Content::find($id);
        $arr = [];
        $arr['status'] = !$content->status;
        $content->fill($arr);
        $content->save();
        $response = Content::with('dates')->find($id);
        return $response;
    }


    public static function create(array $request)
    {
        $content = new Content();
        $content->fill($request);

        //check desde request
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $content->save();
        return $content;
    }





    public static function contentsDelete(array $idcontents)
    {
        $contents = Content::contentsDelete($idcontents);
        return $contents;
    }

    public static function update(int $id, array $request)
    {
        $content = Content::find($id);
        if (!$content) {
            throw new NotFoundException('No se ha encontrado el content.');
        }
        //check desde la BD
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $content->fill($request);

        //check desde request
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $content->save();
        return $content;
    }

    public static function delete(int $id)
    {
        $content = Content::find($id);
        if (!$content) {
            throw new NotFoundException('No se ha encontrado el content.');
        }

        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $content->deleted = true;
        $content->save();
        return $content;
    }

    public static function content(int $id)
    {
        $content = Content::getDetail($id);
        if (!$content) {
            throw new NotFoundException('No se ha encontrado el content.');
        }
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        return $content;
    }

    public static function contents($request)
    {
        $user = Auth::user();
        if (empty($request->toArray())) {
            $date = null;
        } else {
            $date = $request["date"];
        }
        if ($user->is_superadmin()) {
            $contents = Content::getSuperAdmin(null, $date);
        } else if ($user->is_admin()) {
            $contents = Content::getAdmin($user, null, $date);
        } else if ($user->is_user()) {
            $contents = Content::getUser($user, null, $date);
        }

        if (!$contents)
            throw new NotFoundException('No se ha encontrado el content contents');

        return $contents;
    }

    public static function contentsByMonth($month)
    {
        $user = Auth::user();
        $contents = Content::getContentsByMonth($user, $month);
        if (!$contents)
            throw new NotFoundException('No se ha encontrado el content contents');

        return $contents;
    }

    public static function toselect($request)
    {
        $user = Auth::user();
        if (empty($request->toArray())) {
            $date = date("Y-m-d");;
        } else {
            $date = $request["date"];
        }
        if ($user->is_superadmin()) {
            $contents = Content::getSuperAdmin(1, $date);
        } else if ($user->is_admin()) {
            $contents = Content::getAdmin($user, 1, $date);
        } else if ($user->is_user()) {
            $contents = Content::getUser($user, 1, $date);
        }
        if (!$contents)
            throw new NotFoundException('No se ha encontrado el content contents');

        return $contents;
    }

    public static function toEmission($request)
    {
        $data = $request->all();
        if (!isset($data['status']) || !$data['status']) {
            $query = Content::with("dates")->with('category')->with('type')->with('tags')->where('contents.idcustomer', $data['idcustomer'])->where('contents.deleted', 0)->where('contents.status', 1);
            if (isset($data['month']) && isset($data['year'])) {
                $start = new Carbon($data['year'] . '-' . $data['month'] . '-01');
                $start = $start->startOfMonth()->toDateString();
                $end = new Carbon($data['year'] . '-' . $data['month'] . '-01');
                $end = $end->endOfMonth()->toDateString();
                $query->whereExists(function ($query) use ($start, $end) {
                    $query->select(DB::raw('1'))
                        ->from('contents_dates')
                        ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                        ->where('contents_dates.date_on', '<=', $end)
                        ->where('contents_dates.date_off', '>=', $start);
                });
            }
            $contents = $query->get()->toArray();
        } else if (isset($data['status']) && $data['status']) {
            $query = Content::with("dates")->with('category')->with('type')->with('tags')->where('contents.idcustomer', $data['idcustomer']);
            $query->join('difendif_python_impacto', 'contents.idcontent', '=', 'difendif_python_impacto.idcampaign')
                ->where('difendif_python_impacto.status', 0)
                ->where('difendif_python_impacto.ejercicio', $data['year'])
                ->where('difendif_python_impacto.mes', $data['month']);
            $contents = $query->get()->toArray();
        }
        if (!$contents || count($contents) == 0)
            throw new NotFoundException('No se ha encontrado contenidos');
        return array_unique($contents, SORT_REGULAR);
    }

    public static function updatePowers($request, $idcontent)
    {
        $content = Content::find($idcontent);

        if (!$content)
            throw new NotFoundException('No se ha encontrado el content');

        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        foreach ($content->powers as $power) {
            $content->deleteContentPower($power->idtime);
        }

        foreach ($request['powers'] as $power) {
            $power["idcontent"] = $idcontent;
            $content_power = new ContentPower();
            $content_power->fill($power);
            $content_power->save();
        }
        $content = Content::with('powers')->find($idcontent);

        return $content;
    }

    public static function deletePower($idcontent, $idtime)
    {
        $power = ContentPower::where('idtime', $idtime)->where('idcontent', $idcontent)->first();
        if (!$power)
            throw new NotFoundException('No se ha encontrado power');
        if (!$power->content()->isValid())
            throw new NotValidUserException;

        $power->delete();

        return null;
    }

    public static function getProducts($idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content)
            throw new NotFoundException('No se han encontrado productos');
        return $content->products;
    }

    public static function updateProducts($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        foreach ($content->products as $product) {
            $content->products()->detach($product->idproduct);
        }
        $products = $request;
        foreach ($products['products'] as $product) {
            $content->products()->attach($product);
        }
        $content = Content::with('products')->find($idcontent);

        return $content;
    }

    public static function deleteProduct($idcontent, $idproduct)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid())
            throw new NotValidUserException;

        $content->deleteProduct($idproduct);

        return null;
    }

    public static function createAsset($idcontent, $idasset)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        $content->assets()->attach($idasset);

        return $content;
    }


    public static function updateAssets($request, $idcontent)
    {
        $content = Content::find($idcontent);

        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        foreach ($content->assets as $asset) {
            $content->assets()->detach($asset->idasset);
        }
        $assets = $request;

        foreach ($assets['assets'] as $key => $asset) {
            if ($key == 0) {
                $obj = Asset::find($asset['idasset']);
                $content['thumbnail_url'] = $obj['cover_url'];
                $content->save();
            }
            $content->assets()->attach($asset);
        }
        $content = Content::with('assets')->find($idcontent);
        return $content;
    }

    public static function deleteAsset($idcontent, $idasset)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid())
            throw new NotValidUserException;

        $content->assets()->detach($idasset);

        return Content::with('assets')->find($idcontent);
    }

    public static function updateTags($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        foreach ($content->tags as $tag) {
            $content->tags()->detach($tag->idtag);
        }
        $tags = $request;
        foreach ($tags['tags'] as $tag) {
            $content->tags()->attach($tag);
        }
        $content = Content::with('tags')->find($idcontent);

        return $content;
    }

    public static function deleteTag($idcontent, $idtag)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid())
            throw new NotValidUserException;

        $content->tags()->detach($idtag);

        return Content::with('tags')->find($idcontent);
    }

    public static function updatePlayareas($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        foreach ($content->playareas as $playarea) {
            $content->playareas()->detach($playarea->idarea);
        }
        $playareas = $request;
        foreach ($playareas['areas'] as $playarea) {
            $content->playareas()->attach($playarea);
        }
        $content = Content::with('playareas')->find($idcontent);

        return $content;
    }

    public static function deletePlayarea($idcontent, $idarea)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid())
            throw new NotValidUserException;

        $content->playareas()->detach($idarea);

        return Content::with('playareas')->find($idcontent);
    }

    public static function updatePlaycircuits($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }
        foreach ($content->playcircuits as $playcircuit) {
            $content->playcircuits()->detach($playcircuit->idcircuit);
        }
        $playcircuits = $request;
        foreach ($playcircuits['playcircuits'] as $playcircuit) {
            $content->playcircuits()->attach($playcircuit);
        }
        $content = Content::with('playareas')->find($idcontent);

        return $content;
    }

    public static function updateSites($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $sites = $request;
        \DB::table('contents_has_sites')->where('idcontent', $idcontent)->delete();
        foreach ($sites['sites'] as $site) {
            \DB::table('contents_has_sites')
                ->insert(
                    [
                        'idsite' => $site['idsite'],
                        'idcircuit' => $site['idcircuit'],
                        'idcontent' => $idcontent
                    ]
                );
        }

        $content = Content::with('playareas')->find($idcontent);
        return $content;
    }

    public static function updateDates($request, $idcontent)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid()) {
            throw new NotValidUserException;
        }

        $dates = $request;
        \DB::table('contents_dates')->where('idcontent', $idcontent)->delete();
        foreach ($dates['dates'] as $date) {
            \DB::table('contents_dates')
                ->insert(
                    [
                        'date_on' => $date['date_on'],
                        'date_off' => $date['date_off'],
                        'idcontent' => $idcontent
                    ]
                );
        }

        $content = Content::with('playareas')->find($idcontent);
        return $content;
    }

    public static function deletePlaycircuit($idcontent, $idcircuit)
    {
        $content = Content::find($idcontent);
        if (!$content->isValid())
            throw new NotValidUserException;

        $content->playcircuits()->detach($idcircuit);

        return Content::with('playcircuits')->find($idcontent);
    }

    public function primeTime($idcontent)
    {
        $content = Content::with('powers')->with('tags')->where('idcontent', $idcontent)->first();

        if (isset($content->idcustomer)) {
            $idcustomer = $content->idcustomer;
            $ticketData = Product::where('idcustomer', $idcustomer);

            if ($ticketData) {
                $ret = $ticketData;
                if (isset($ticketData["data"][0]['idcategory'])) {
                    $c_content = collect($content);
                    $new_content = $this->create($c_content->except('idcontent', 'powers')->toArray());
                    $idcontent = $new_content->idcontent;
                    $idcategory = $ticketData["data"][0]['idcategory'];
                    $listPatternData = Content::getDataPatternData($idcustomer, $idcategory);
                    $a_timeszones = $this->createTimesZones($listPatternData, $new_content->idcontent);

                    foreach ($a_timeszones as $key => $tmz) {
                        ContentPower::create($tmz);
                    }

                    $result = Content::with('powers')->find($idcontent);

                    return  $result;
                } else {
                    throw new NotFoundException('Error no existe idcategory de producto');
                }
            } else {
                throw new NotFoundException('Error en ws');
            }
        } else {
            throw new NotFoundException('Error no existe idcustomer en el contenido');
        }
    }

    public function createTimesZones($listPatternData, $idcontent)
    {
        //check listPatterdata
        $tmpLPat = [];
        foreach ($listPatternData as $k => $lpat) {
            if ($lpat->weekday < 8) {
                if (!isset($tmpLPat[$lpat->weekday])) {
                    $tmpLPat[$lpat->weekday] = [];
                }
                array_push($tmpLPat[$lpat->weekday], $lpat->hour);
            }
        }

        $a_franjas = [];
        foreach ($tmpLPat as $wday => $frj) {
            $tmpfr = 0;
            ksort($frj);

            $tmpFranja = [];
            $ind = 0;
            //if(count($frj)>1){
            foreach ($frj as  $k => $fr) {
                if ($tmpfr == 0) {
                    $tmpfr = $fr;
                    $tmpFranja[$ind][0] = $fr;
                } else {
                    if ($fr > $tmpfr) {
                        if (($tmpfr + 1) == $fr) {
                            $tmpFranja[$ind][1] = $fr;
                        } else {
                            $ind++;
                            $tmpFranja[$ind][0] = $fr;
                        }
                        $tmpfr = $fr;
                    }
                }
            }
            // }

            //checkeo que tenga inicio y fin
            $tmpMedFranja = [];
            foreach ($tmpFranja as $kf => $chkfranj) {
                if (count($chkfranj) < 2) {
                    if (!isset($chkfranj[1])) {
                        $chkfranj[1] = $chkfranj[0];
                    } else {
                        throw new NotFoundException('Error en franjas');
                    }
                }
                array_push($tmpMedFranja, $chkfranj);
            }

            $tmpFinFranja = [];
            foreach ($tmpMedFranja as $kf => $chkFinfranj) {
                if ($chkFinfranj[0] < 10) $chkFinfranj[0] = '0' . $chkFinfranj[0];
                if ($chkFinfranj[1] < 10) $chkFinfranj[1] = '0' . $chkFinfranj[1];
                array_push($tmpFinFranja, [$chkFinfranj[0] . ':00:00', $chkFinfranj[1] . ':59:00']);
            }

            $a_franjas[$wday] = $tmpFinFranja;
        }

        $a_timeszones = [];
        foreach ($a_franjas as $akf => $afranja) {
            foreach ($afranja as $afr) {
                array_push($a_timeszones, ["idcontent" => $idcontent, "weekday" => $akf, "time_on" => $afr[0], "time_off" => $afr[1]]);
            }
        }
        return $a_timeszones;
    }

    public static function updateContentOrder($request)
    {
        $a_contents = [];
        $contents = $request->all();
        if (isset($contents['contents'])) {
            foreach ($contents['contents'] as $key => $content) {

                $cont = Content::find($content['idcontent']);

                if (!$cont) {
                    throw new NotFoundException('No se ha encontrado el content.');
                }

                if (!$cont->isValid())
                    throw new NotValidUserException;

                $cont['order'] = $content['order'];

                //check desde request
                if (!$cont->isValid()) {
                    throw new NotValidUserException;
                }

                $cont->save();
                array_push($a_contents, $cont);
            }
            return $a_contents;
        } else {
            throw new NotFoundException('No hay datos en contents.');
        }
    }

    public static function getOrder($request)
    {
        $user = Auth::user();

        if (empty($request->toArray())) {
            $date = date("Y-m-d");;
        } else {
            $date = $request["date"];
        }

        if ($user->is_superadmin()) {
            $contents = Content::getOrderSuperAdmin($date);
        } else if ($user->is_admin()) {
            $contents = Content::getOrderAdmin($user, $date);
        } else if ($user->is_user()) {
            $contents = Content::getOrderUser($user, $date);
        }

        if (!$contents)
            throw new NotFoundException('No se ha encontrado el content contents');

        return $contents;
    }

    public static function productPrimeTime($request)
    {
        $data = $request->all();
        $user = Auth::user();
        $primetime = PrimeTime::where('idcustomer', $user->idcustomer)
            ->where('code_site', $data['code_site'])
            ->where('idcategory', $data['idcategory'])
            ->select('weekday', 'hour')
            ->orderBy('weekday', 'ASC')
            ->orderBy('hour', 'ASC')
            ->get();
        if (!$primetime)
            throw new NotFoundException('No se han encontrado datos');

        return $primetime;
    }

    public static function contentsPrimeTime($request)
    {
        $data = $request->all();
        $user = Auth::user();
        $primetime = PrimeTime::join('sites as s', 's.code', '=', 'primetime.code_site')
            ->join('product_categories as pc', 'pc.code', '=', 'primetime.idcategory')
            ->where('primetime.idcustomer', $user->idcustomer)
            ->whereIn('primetime.code_site', $data['sites'])
            ->whereIn('primetime.idcategory', $data['categories'])
            ->select('primetime.weekday', 'primetime.hour', 's.name as site', 'pc.name as category')
            ->orderBy('primetime.weekday', 'ASC')
            ->orderBy('primetime.hour', 'ASC')
            ->get();
        if (!$primetime)
            throw new NotFoundException('No se han encontrado datos');

        return $primetime;
    }

    public static function contentsStatus($status)
    {
        $user = Auth::user();
        $today = date("Y-m-d");
        $contents = Content::with('playareas')
            ->with('playcircuits')
            ->with('category')
            ->with('dates')
            ->where('idcustomer', $user->idcustomer);
        if ($user->idsite)
            $contents->where('idsite', $user->idsite);
        switch ($status) {
            case 'all':
                //$contents->where('deleted', 0);
                break;
            case 'active':
                $contents->where('status', 1)
                    ->where('deleted', 0)
                    ->whereHas('dates', function ($query) use ($today) {
                        $query->where('date_on', '<=', $today)
                            ->where('date_off', '>=', $today);
                    });
                break;
            case 'inactive':
                $contents->where('deleted', 0)
                    ->where(function ($query) use ($today) {
                        $query->where('status', 0)
                            ->orWhereHas('dates', function ($query2) use ($today) {
                                $query2->where('date_on', '>', $today)
                                    ->orWhere('date_off', '<', $today);
                            });
                    });
                break;
            case 'archived':
                $contents->where('deleted', 1);
                break;
        }
        return $contents->orderBy('contents.idcontent', 'desc')->get();
    }
}
