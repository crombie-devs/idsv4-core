<?php

namespace App\Repositories\Content;

use Illuminate\Http\Request;

interface ContentRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function contents(Request $request);
    public static function contentsByMonth(string $month);
    public static function toSelect(Request $request);
    public static function toEmission(Request $request);
    public static function content(int $id);

    public static function updatePowers(array $request, int $id);
    public static function deletePower(int $idcontent,int $idpower);

    public static function getProducts(int $id);
    public static function updateProducts(array $request, int $id);
    public static function deleteProduct(int $idcontent,int $productcode);
    
    public static function createAsset(int $idcontent,int $idasset);
    public static function updateAssets(array $request, int $id);
    public static function deleteAsset(int $idcontent,int $idasset);

    public static function updateTags(array $request, int $id);
    public static function deleteTag(int $idcontent,int $idtag);

    public static function updatePlayareas(array $request, int $id);
    public static function deletePlayarea(int $idcontent,int $idplayarea);

    public static function updatePlaycircuits(array $request, int $id);
    public static function deletePlaycircuit(int $idcontent,int $idplayarea);
    public static function updateDates(array $dates, int $id);
    public static function updateSites(array $sites, int $id);

    public static function updateContentOrder(Request $request);
    public static function productPrimeTime(Request $request);
    public static function contentsPrimeTime(Request $request);
    public static function getOrder(Request $request);

    public static function contentsStatus(string $status);
}
