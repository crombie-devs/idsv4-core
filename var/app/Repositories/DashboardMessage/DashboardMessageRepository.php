<?php

namespace App\Repositories\DashboardMessage;

use Carbon\Carbon;

use App\Models\DashboardMessage;
use App\Repositories\DashboardMessage\DashboardMessageRepositoryInterface;
class DashboardMessageRepository implements DashboardMessageRepositoryInterface
{

    public function list()
    {
        $entities = DashboardMessage::with('customer')->get();

        if (!$entities) {

            $response = [
                'success' => false,
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => true,
                'data' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count(),
                    'entities' => $entities
                ],
            ];
        }

        return $response;
    }

    public function get($id)
    {
        $entity = DashboardMessage::find($id)
            ->with('customer')
            ->get();

        if (!$entity) {

            $response = [
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json'
                ],
                'data' => $entity
            ];
        }

        return $response;
    }

    public function latest()
    {
        $now = Carbon::now();

        $entities = DashboardMessage::with('customer')->where('date_on', '<=', $now)->where('date_off', '>=', $now)->get();

        if (!$entities) {

            $response = [
                'success' => false,
                'error' => [
                    'type' => 'GET',
                    'code' => 404,
                ],
                'message' => 'No existen mensajes para mostrar.',
            ];

        } else {

            $response = [
                'success' => true,
                'data' => [
                    'type' => 'GET',
                    'code' => 200,
                    'dataType' => 'json',
                    'count' => $entities->count(),
                    'entities' => $entities
                ],
            ];
        }

        return $response;
    }

    // public function create(array $request)
    // {
    // }

    // public function update(array $request)
    // {
    // }

    // public function delete($id)
    // {
    // }
}