<?php

namespace App\Repositories\App;

use Illuminate\Http\Request;

interface AppRepositoryInterface {
    public static function create(Request $request);
    public static function update(int $id, Request $request);
    public static function delete(int $id);

    public static function apps();
    public static function app(int $id);

    public static function appsToSelect();
}