<?php

namespace App\Repositories\App;

use Illuminate\Http\Request;
use App\Repositories\App\AppRepositoryInterface;
use App\Models\Customer;
use App\Models\App;
use App\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Auth;

class AppRepository implements AppRepositoryInterface {

    public static function create(Request $request){
        $input = $request->all();
        $app = App::create($input);
        if(!$app)
            throw new NotFoundException('Error al insertar la app.');

        return $app;
    }

    public static function update(int $id, Request $request){
        $app = App::find($id);
        if(!$app){
            throw new NotFoundException('No se ha encontrado la app.');
        }
        if(isset($request['name'])) $app->name = $request['name'];
        if(isset($request['apikey'])) $app->apikey = $request['apikey']; 
        if(isset($request['idcustomer'])){
            $customer = Customer::find($request['idcustomer']);
            $app->idcustomer = $customer->idcustomer;
        }
        if(isset($request['status'])) $app->status = $request['status'];
        $app->save();
        return $app;
    }

    public static function delete(int $id){
        $app = App::find($id);
        if(!$app){
            throw new NotFoundException('No se ha encontrado la app.');
        }
        $app->deleted = true;
        $app->save();
        return $app;
    }

    public static function app(int $id){
        $app = App::with('customer')->where('idapp', $id)->where('deleted', false)->first();
        if(!$app){
            throw new NotFoundException('No se ha encontrado la app.');
        }
        return $app;
    }

    public static function apps(){
        $apps = App::with('customer')->where('deleted', false)->get();
        if(!$apps){
            throw new NotFoundException('No se han encontrado apps.');
        }
        return $apps;
    }

    public static function appsToSelect(){
        $user = Auth::user();
        $query = App::where('status', true)->where('deleted', false);
        if($user->roles[0]->name == 'superadmin'){
            $apps = $query->get();
        }else{
            $apps = $query->where('idcustomer',$user->customer->idcustomer)->orWhere('idcustomer',null)->get();
        }
        if(!$apps){
            throw new NotFoundException('No se han encontrado apps.');
        }
        return $apps;
    }

}