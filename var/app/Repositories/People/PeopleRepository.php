<?php

namespace App\Repositories\People;

use App\Repositories\People\PeopleRepositoryInterface;
use App\Models\People;
use App\Models\PeopleTags;
use App\Exceptions\NotFoundException;
use Illuminate\Http\Request;





class PeopleRepository  implements PeopleRepositoryInterface
{






    public static function  get($id)
    {
        $message = People::with('tags')->where('id', $id)->get();
        if (!$message)
            throw new NotFoundException('No se han encontrado Message para mostrar');

        return $message;
    }

    public  function  create(array $request)
    {
        $people = new People();
        $people->fill($request);
        $people->save($request);
        $tags = $request['tags'];
        foreach ($tags as $k => $v) {
            $tag = new PeopleTags();
            $t['idtag']  = $v->idtag;
            $t['idpeople']  = $people->id;
            $tag->fill($t);
            $tag->save($t);
        }

        return  $people;
    }

    public static function toSelect()
    {
        $peoples = People::with('tags')->get();
        if (!$peoples)
            throw new NotFoundException('No se han encontrado tickts para mostrar');

        return $peoples;
    }


    public static function update(array $request)
    {
        $people = People::find($request['id']);
        $people->fill($request);
        $people->save();
        $rtags = PeopleTags::where('idpeople', $request['id'])->delete();



        foreach ($request['tags'] as $k => $v) {

            $tag = new PeopleTags();
            $t['idtag']  = $v['idtag'];
            $t['idpeople']  = $request['id'];
            $tag->fill($t);
            $tag->save();
        }
        return  $people;
    }


    public static function delete($ids)
    {

        foreach ($ids->ids as $key => $id) {

            $people = People::find($id);

            $people->tags()->delete();
            $people->delete();
        }


        return $people;
    }
}
