<?php

namespace App\Repositories\People;
use Illuminate\Http\Request;


interface PeopleRepositoryInterface {
    public  function create(array $request);
    public  static function get($id);
    public  static function update(array $request);
  

    
}