<?php

namespace App\Repositories\City;

use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Exceptions\NotFoundException;
use App\Repositories\City\CityRepositoryInterface;

class CityRepository implements CityRepositoryInterface {

    public static function toSelect($data){
        if(empty($data)){
           $city = City::all()->sortBy('name')->values();
        }
        else{
            if(isset($data['idcountry']) && isset($data['idprovince'])){
                $provinces= Province::where('idcountry',$data['idcountry'])->pluck('idprovince');
                $city = City::where('idprovince',$data['idprovince'])->whereIn('idprovince',$provinces)->orderBy('name')->get();
            }
            elseif(isset($data['idprovince'])){
                $city = City::where('idprovince',$data['idprovince'])->orderBy('name')->get();
            }
            elseif(isset($data['idcountry'])){
                $provinces= Province::where('idcountry',$data['idcountry'])->pluck('idprovince');
                if($provinces->isNotEmpty()){
                    $city = City::whereIn('idprovince',$provinces)->orderBy('name')->get();
                }
            }   
        }
        
        if(empty($city)){
            throw new NotFoundException('City no encontrado');
        }
        return $city;
    }
}