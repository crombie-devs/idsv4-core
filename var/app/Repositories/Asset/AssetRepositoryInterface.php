<?php

namespace App\Repositories\Asset;

use Illuminate\Http\Request;

interface AssetRepositoryInterface {
    public static function create(array $request);
    public static function update(int $id, array $request);
    public static function delete(int $id);

    public static function assets();
    public static function asset(int $id);
}
