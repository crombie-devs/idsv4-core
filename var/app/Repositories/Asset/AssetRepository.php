<?php

namespace App\Repositories\Asset;

use Illuminate\Http\Request;
use App\Repositories\Asset\AssetRepositoryInterface;
use App\Models\Asset;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;

class AssetRepository implements AssetRepositoryInterface {

    public static function create(array $request){
        $asset = new Asset();
        unset($request['asset']);

       /*  if($request['is_widget'] == "true"){
            $request['is_widget'] = 1;
        }else{
            $request['is_widget'] = 0;
        } */
        $asset->fill($request);

        //check desde request
        if(!$asset->isValid()){
            throw new NotValidUserException;
        }

        $asset->save();
        return $asset->load('lang')->load('format');
    }

    public static function update(int $id, array $request){
        $asset = Asset::find($id);
        if(!$asset){
            throw new NotFoundException('No se ha encontrado el asset.');
        }
        //check desde la BD
        if(!$asset->isValid()){
            throw new NotValidUserException;
        }
        
        $asset->fill($request);
      
        //check desde request
        if(!$asset->isValid()){
            throw new NotValidUserException;
        }
        
         $asset->save();
        return $asset;
    }

    public static function delete(int $id){
        $asset = Asset::find($id);
        if(!$asset){
            throw new NotFoundException('No se ha encontrado el asset.');
        }

        if(!$asset->isValid()){
            throw new NotValidUserException;
        }

        $asset->deleted = true;
        $asset->save();
        return $asset;
    }

    public static function asset(int $id){
        $asset = Asset::getDetail($id);

        if(!$asset){
            throw new NotFoundException('No se ha encontrado el asset.');
        }
        if(!$asset->isValid()){
            throw new NotValidUserException;
        }
        return $asset;
    }

    public static function assets(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $assets = Asset::getSuperAdmin();
        }
        else if($user->is_admin()){
            $assets = Asset::getAdmin($user);
        }
        else if($user->is_user()){
            $assets = Asset::getUser($user);
        }

        if(!$assets)
            throw new NotFoundException('No se ha encontrado el content assets');

        return $assets;
    }

    public static function toselect(){
        $user = Auth::user();
        if($user->is_superadmin()){
            $assets = Asset::getSuperAdmin(1);
        }
        else if($user->is_admin()){
            $assets = Asset::getAdmin($user,1);
        }
        else if($user->is_user()){
            $assets = Asset::getUser($user,1);
        }

        if(!$assets)
            throw new NotFoundException('No se ha encontrado el content assets');

        return $assets;
    }

    public static function deleteAssetForce($idasset){
        $user = Auth::user();
        $asset = Asset::find($idasset);

        if(!$asset)            
            throw new NotFoundException('No se ha encontrado el asset');

        if(!$asset->isValid())
            throw new NotValidUserException;

        if(count($asset->contents)>0)
            foreach($asset->contents as $content){
                $asset->contents()->detach($content->idcontent);
            }   
        
        $asset->delete();

        return true;
    }

}