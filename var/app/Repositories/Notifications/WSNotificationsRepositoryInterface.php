<?php

namespace App\Repositories\Notifications;

use Illuminate\Http\Request;

interface WSNotificationsRepositoryInterface {
    public static function notificate(Request $request);
}