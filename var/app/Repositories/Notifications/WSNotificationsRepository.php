<?php

namespace App\Repositories\Notifications;

use Illuminate\Http\Request;
use App\Repositories\Notifications\WSNotificationsRepositoryInterface;
use App\Models\User;
use App\Exceptions\NotFoundException;
use App\Notifications\GenericNotification as Notification;

class WSNotificationsRepository implements WSNotificationsRepositoryInterface {
  
    public static function notificate($request){
        $data = $request->all();
        $user = User::where('email', $data['email'])->first();
        if(!$user)            
            throw new NotFoundException('No se encontro info del usuario');

            $user->notify(new Notification($data['message'], $data['subject']));
    }

}