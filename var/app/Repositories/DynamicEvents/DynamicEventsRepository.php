<?php

namespace App\Repositories\DynamicEvents;


use App\Repositories\DynamicEvents\DynamicEventsRepositoryInterface;
use App\Models\DynamicEvents;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use App\Models\DynamicEventRules;


class DynamicEventsRepository  implements DynamicEventsRepositoryInterface
{

    public static function  create(array $request)
    {
        $events = new DynamicEvents();
        $events->fill($request);

        if(!$events->isValid())
            throw new NotValidUserException;

        $events->save();
        $rule = [];

        if (count($request['rules']) > 0) {
            foreach ($request['rules'] as $item) {
                $eventsRules = new DynamicEventRules();
                $item->idevent = $events->id;
                $eventsRules->fill((array)$item);
                $eventsRules->save();
                $rule[] = $item;
            }
        }

        return $events['rules'] = $rule;
    }



    public static function toSelect()
    {
        $events = DynamicEvents::toSelect();
        if (!$events)
            throw new NotFoundException('No se ha encontrado el evento');
        return $events;
    }


    public static function get($idevent)
    {   
        $events = DynamicEvents::get($idevent);
        if (!$events)
            throw new NotFoundException('No se ha encontrado el evento');

        if(!$events->isValid())
            throw new NotValidUserException;

        return $events;
    }



   

    public static function byPlayer($idcustomer, $idsite, $idplayer )
    {
       

        $events = DynamicEvents::byPlayer($idcustomer, $idsite, $idplayer);
        
        if (count($events) <= 0)
            throw new NotFoundException('No se ha encontrado el evento');
            return $events;
    }


    public function tagPlayer($id) {
        $events = DynamicEvents::tagPlayer($id);
        if (count($events) <= 0)
            throw new NotFoundException('No se ha encontrado el evento');
            return $events;
    }



   public static function list()
    {
        $events = DynamicEvents::list();
        if (!$events)
            throw new NotFoundException('No se ha encontradoel evento');
        return $events;
    }


   public static function update(array $request, $id)
    {
        $events = DynamicEvents::find($id);
        if (!$events)
            throw new NotFoundException('No se ha encontrado el evento');
        if(!$events->isValid())
            throw new NotValidUserException;

        $events->fill($request);
        $events->save();

        DynamicEventRules::where('idevent', $id)->delete();
        $rule = [];
        if (count($request['rules']) > 0) {

            foreach ($request['rules'] as $item) {
                $eventsRules = new DynamicEventRules();
                $item->idevent = $id;
                $eventsRules->fill((array)$item);
                $eventsRules->save();
                $rule[] = $item;
            }
        }

        $events = DynamicEvents::with('rules')->find($id);
        return $events;
    }

    public static function delete($id)
    {    
        $events = DynamicEvents::find($id);
        if (!$events)
            throw new NotFoundException('No se ha encontrado el evento');
        if(!$events->isValid())
            throw new NotValidUserException;

        DynamicEvents::where('id', $id)->delete();
        DynamicEventRules::where('idevent', $id)->delete();
        return $events;
    }
}
