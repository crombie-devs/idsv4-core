<?php

namespace App\Repositories\DynamicEvents;



interface DynamicEventsRepositoryInterface {
    public static function create(array $request);
    public static function toSelect();
    public static function list();
    public static function update(array $request, $id);
    public static function delete($id);
    
}