<?php

namespace App\Repositories\Role;

use Illuminate\Http\Request;
use App\Repositories\Role\RoleRepositoryInterface;
use Spatie\Permission\Models\Role;

class RoleRepository implements RoleRepositoryInterface {
    const SUCCESS_STATUS_CODE = 200;
    const NOT_FOUND_STATUS_CODE = 404;

    public function create(Request $request){
        $role = Role::create(['name' => $request['name']]);
        return $this->response(true, $role, self::SUCCESS_STATUS_CODE);
    }

    public function update(int $id, Request $request){
        $role = Role::where('id', $id)->first();
        if(!$role){
            return $this->response(false, ['message' => 'Rol no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        $role->name = $request['name'];
        $role->save();
        return $this->response(true, $role, self::SUCCESS_STATUS_CODE);
    }

    public function delete(int $id){
        $permission = Role::where('id', $id)->first();
        if(!$permission){
            return $this->response(false, ['message' => 'Rol no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        $permission->delete();
        return $this->response(true, null, self::SUCCESS_STATUS_CODE);
    }

    public function role(int $id){
        $rol = Role::where('id', $id)->first();
        if(!$rol){
            return $this->response(false, ['message' => 'Rol no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        return $this->response(true, $rol, self::SUCCESS_STATUS_CODE);
    }

    public function roles(){
        $roles = Role::all();
        if(!$roles){
            return $this->response(false,['message' => 'No se han encontrado roles.'], self::NOT_FOUND_STATUS_CODE);
        }
        return $this->response(true, $roles, self::SUCCESS_STATUS_CODE);
    }

    public function response(bool $success, $data, int $statusCode) {
        $response = ["success" => $success, "data"=>$data, "statusCode"=>$statusCode];
        return $response;
    }
}