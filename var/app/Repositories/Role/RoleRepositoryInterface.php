<?php

namespace App\Repositories\Role;

use Illuminate\Http\Request;

interface RoleRepositoryInterface {
    public function create(Request $request);
    public function update(int $id, Request $request);
    public function delete(int $id);

    public function roles();
    public function role(int $id);

    public function response(bool $success, $data, int $statusCode);
}