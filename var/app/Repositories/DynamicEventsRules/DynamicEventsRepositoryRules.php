<?php

namespace App\Repositories\DynamicEventsRules;


use App\Repositories\DynamicEventsRules\DynamicEventsRulesRepositoryInterface;
use App\Models\DynamicEventRules;
use App\Exceptions\NotFoundException;




class AudioRepository  implements DynamicEventsRulesRepositoryInterface
{

    public static function  create(array $request)
    {

        $events = new DynamicEventRules();
        $events->fill($request);
        $events->save();
        return $events;
    }

  

    public static function toSelect($idcustomer)
    {
       
        $events = DynamicEventRules::filtertByCustomer($idcustomer);
        if (!$events)
            throw new NotFoundException('No se ha encontrado el evento');
        return $events;
    }



    
    public static function toSelectAll()
    {
       
        $events = DynamicEventRules::toSelectAll();
        if (!$events)
            throw new NotFoundException('No se ha encontradoel evento');
        return $events;
    }




  public static function update(array $request, $id)
    {
        $events = DynamicEventRules::find($id);

        if (!$events) {
            throw new NotFoundException('No se ha encontrado el evento');
        }

        $events->fill($request);
        $events->save();
        return $events;
    }

    public static function delete($id)
    {

        $events = DynamicEventRules::where('iddynamicevents', $id)->delete();
        if (!$events)
            throw new NotFoundException('No se ha encontrado ningun evento');

        return $events;
    }
}
