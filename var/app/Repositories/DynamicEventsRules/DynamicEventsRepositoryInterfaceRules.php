<?php

namespace App\Repositories\DynamicEventsRules;



interface DynamicEventsRulesRepositoryInterface {
    public static function create(array $request);
    public static function toSelect($id);
    public static function toSelectAll();
    public static function update(array $request, $id);
    public static function delete($id);
    
}