<?php

namespace App\Repositories\Recomendations;

use Illuminate\Http\Request;

interface RecomendationsRepositoryInterface {
    public function categories();
    public function probabilidadCondicionada(int $idcategory);
    public function recomendationsCategories(int $month);
    public function recomendationsTrendsCategories();
    public function getPatronVentas(Request $request);
    public function getFilasDiasSemana(Request $request);
    public function getTemperatura(Request $request);
    public function getProductsFromCategory(Request $request);
    public function getSociodemographic(Request $request);
    public function getProvinces();
    public function getSitesFromProvinces(Request $request);
    public function getRatius(Request $request);
    public function getSocialEconomicAdminData(Request $request);
    public function getAgeVars();
    public function getIncomeRanges(Request $request);
    public function getDemographicSegmentation(Request $request);
    public function getEconomicSegmentation(Request $request);
    public function getTrendsProvinces();
}