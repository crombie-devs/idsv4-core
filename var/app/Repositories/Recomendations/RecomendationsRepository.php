<?php

namespace App\Repositories\Recomendations;

use Illuminate\Http\Request;
use App\Repositories\Recomendations\RecomendationsRepositoryInterface;
use App\Models\Association;
use App\Models\Site;
use App\Models\City;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Province;
use App\Exceptions\NotFoundException;
use App\Exceptions\NotValidUserException;
use Illuminate\Support\Facades\Auth;
use App\Traits\TicketWs;
use App\Traits\PeopleWs;
use stdClass;
use Carbon\Carbon;
use App\Models\PatternData;
use App\Models\PatternDataRecomendations;
use App\Models\RecomendationsTrend;

class RecomendationsRepository implements RecomendationsRepositoryInterface
{
    use TicketWs;
    use PeopleWs;

    public function categories()
    {
        $user = Auth::user();
        $association = new Association($user->customer->idcustomer);
        $as = $association->associations;
        $getCategorycol = $as->where('tipo', 'P')->where('nivelagregacion', $user->customer->category_level)->unique('idcategoryrow')->pluck('idcategoryrow');
        $categories = ProductCategory::where('idcustomer', $user->idcustomer)->whereIn('code', $getCategorycol)->get();
        return $categories;
    }

    public function probabilidadCondicionada(int $idcategory)
    {
        $category = ProductCategory::find($idcategory);
        $user = Auth::user();

        $idcustomer = $user->idcustomer;
        $association = new Association($idcustomer);
        $as = $association->associations;
        $ascimp = $as->where('nivelagregacion', $user->customer->category_level)
            ->where('idcategorycol', $category['code'])
            ->where('idcategoryrow', '!=', $category['code']);

        $categories = ProductCategory::where('idcustomer', $idcustomer)->whereIn('code', $ascimp->pluck('idcategoryrow'))->get();
        $categorias = $categories;
        $procond = $ascimp->where('tipo', "P");
        $impacto = $ascimp->where('tipo', "I");
        $recomendaciones = $ascimp->where('tipo', 'R');
        $a_tmpprobcon = [];

        foreach ($categorias as $key => $cat) {
            $tmpprocon = $procond->where('idcategoryrow', $cat['code']);
            $tmpproimp = $impacto->where('idcategoryrow', $cat['code']);
            $tmprecome = $recomendaciones->where('idcategoryrow', $cat['code']);
            $object = new StdClass;
            $object->name = $cat['name'];
            if ($tmpprocon->count() != 0) {
                $object->procon = $tmpprocon->first()['valor'];
            } else {
                $object->procon = 0;
            }
            if ($tmpproimp->count() != 0) {
                $object->impacto = $tmpproimp->first()['valor'];
            } else {
                $object->impacto = 0;
            }
            if ($tmprecome->count() != 0) {
                $object->recomendaciones = $tmprecome->first()['valor'];
            } else {
                $object->recomendaciones = 0;
            }

            $object->category_code = $cat['code'];
            $object->customer_id = $idcustomer;
            array_push($a_tmpprobcon, $object);
        }

        $a_tmpprobcontmp = collect($a_tmpprobcon)->sortByDesc('procon');

        $a_final = [];
        foreach ($a_tmpprobcontmp as $pct) {
            array_push($a_final, $pct);
        }
        return $a_final;
    }

    public function recomendationsCategories(int $month)
    {
        $user = Auth::user();
        $in = '$in';
        $idcustomer = $user->idcustomer;
        $patternData =  new PatternData($idcustomer, $month);
        $pd = $patternData->pd;
        $getCategorycol = $pd->where('idcategory', '<>', '0')->sortBy('idcategory')->pluck('idcategory')->toArray();
        $getCategorycol = array_values(array_unique($getCategorycol));
        $categories = ProductCategory::where('idcustomer', $idcustomer)->whereIn('code', $getCategorycol)->get();
        $c_categories = collect($categories);

        $patterndatarecomendation = new PatternDataRecomendations($idcustomer, $month);
        $pdr = $patterndatarecomendation->pdr;

        $getCategoval = $pdr->where('nivelagregacion', $user->customer->category_level)->sortByDesc('value');

        $dat_cat = [];
        if (!empty($getCategoval)) {
            foreach ($getCategoval as $key => $gc) {
                $tmp_cat = $c_categories->where('code', $gc['idagregacion']);
                if (isset($tmp_cat->first()['name'])) {
                    $object = new StdClass;
                    $object->code = $gc['idagregacion'];
                    $object->idcategoria = $tmp_cat->first()['idcategory'];
                    $object->value = $gc['value'];
                    $object->name = $tmp_cat->first()['name'];
                    array_push($dat_cat, $object);
                }
            }
        }
        return $dat_cat;
    }

    public function recomendationsTrendsCategories()
    {
        $user = Auth::user();
        $in = '$in';
        $idcustomer = $user->idcustomer;
        $categories = RecomendationsTrend::where('idcustomer', $idcustomer)->get();
        foreach ($categories as $category) {
            $category['category'] = ProductCategory::where('idcustomer', $idcustomer)->where('code', $category['idcategory'])->first();
        }
        if (!$categories)
            throw new NotFoundException('No se han encontrado categorías');
        return $categories;
    }

    public function getFilasDiasSemana(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $a_fin_group = [];
        $categories = ProductCategory::where('idcustomer', $user->idcustomer)->whereIn('code', $data['categoria'])->pluck('code');
        foreach ($categories as $datc) {
            $patterndata = new PatternData($user->customer->idcustomer, $data['from']);
            $pd = $patterndata->pd;
            $datData = $pd->where('weekday', 9)->where('idcategory', $datc)->sortBy('weekday');
            $tmpdt = [];
            foreach ($datData as $dt) {
                if (!isset($tmpdt[$dt['weekday']])) {
                    $tmpdt[$dt['weekday']] = [];
                }
                array_push($tmpdt[$dt['weekday']], $dt['value']);
            }
            $a_order = [];
            foreach ($tmpdt as $ke => $tmt) {
                if (!isset($a_order[$ke])) {
                    $a_order[$ke] = [];
                }
                sort($tmt);
                array_push($a_order[$ke], $tmt);
            }
            $tmp_data = [];
            foreach ($a_order as $y => $tdt) {
                $tmpcont = [];
                $cont = 1;
                foreach ($tdt[0] as $t) {
                    $tdat = $datData->where('weekday', $y)->where('value', $t);
                    if ($tdat->count() > 1) {
                        if (!isset($tmpcont[$t])) {
                            $tmpcont[$t] = 0;
                        } else {
                            $tmpcont[$t] = $tmpcont[$t] + 1;
                        }
                        $ndat = $tdat->values();
                        $ndat->get($tmpcont[$t])['color'] = $this->degradado($cont);
                        if ($ndat->get($tmpcont[$t]) != null)
                            array_push($tmp_data, $ndat->get($tmpcont[$t]));
                    } else {
                        $ndat = $tdat->first();
                        $ndat['color'] = $this->degradado($cont);
                        if ($ndat != null)
                            array_push($tmp_data, $ndat);
                    }
                    $cont++;
                }
            }
            $tmpfin = [];
            foreach ($tmp_data as $k => $dt) {
                if (!isset($tmpfin[$dt['weekday']])) {
                    $tmpfin[$dt['weekday']] = [];
                }
                array_push($tmpfin[$dt['weekday']], $dt);
            }
            $a_fin = [];
            foreach ($tmpfin as $kf => $tf) {
                $ctf = collect($tf);
                $ordertf = $ctf->sortBy('hour')->toArray();
                foreach ($ordertf as $or) {
                    array_push($a_fin, $or);
                }
            }
            array_push($a_fin_group, $a_fin);
        }
        if (sizeof($a_fin_group) == 1) {
            if (count($a_fin_group[0]) < 24) {
                $tmpobj = [];
                $c_afingroup = collect($a_fin_group[0]);
                $ahour = $c_afingroup->pluck('hour');
                for ($i = 0; $i < 24; $i++) {
                    $tres = $ahour->search($i);
                    if ($tres !== false) {
                        $object = $c_afingroup[$tres];
                    } else {
                        $object = new StdClass;
                        $object->value = 0;
                        $object->hour = $i;
                        $object->color = '#FFF';
                    }
                    array_push($tmpobj, $object);
                }
                $a_fin_group = [];
                array_push($a_fin_group, $tmpobj);;
            }
        } else {
            $cnt = sizeof($a_fin_group);
            $tmpa_fin_group = [];
            for ($x = 0; $x < $cnt; $x++) {
                if (count($a_fin_group[$x]) < 24) {
                    $tmpobj = [];
                    $c_afingroup = collect($a_fin_group[$x]);
                    $ahour = $c_afingroup->pluck('hour');
                    for ($i = 0; $i < 24; $i++) {
                        $tres = $ahour->search($i);
                        if ($tres) {
                            $object = $c_afingroup[$tres];
                        } else {
                            $object = new StdClass;
                            $object->value = 0;
                            $object->hour = $i;
                            $object->color = '#FFF';
                        }
                        array_push($tmpobj, $object);
                    }
                    array_push($tmpa_fin_group, $tmpobj);
                } else {
                    array_push($tmpa_fin_group, $a_fin_group[$x]);
                }
            }
            $a_fin_group = [];
            $a_fin_group = $tmpa_fin_group;
        }
        $a_max = [];
        foreach ($a_fin_group as $ky => $afin) {
            $tmphourmax = 0;
            $maxvalue = 0;
            foreach ($afin as $key => $af) {
                if (isset($af->value)) {
                    if ($af->value >= $maxvalue) {
                        $tmphourmax = $af->hour;
                        $maxvalue = $af->value;
                    }
                } else if (isset($af['value'])) {
                    if ($af['value'] >= $maxvalue) {
                        $tmphourmax = $af['hour'];
                        $maxvalue = $af['value'];
                    }
                }
            }
            array_push($a_max, $tmphourmax);
        }
        $afinidades = $this->getAfinFilasSemana(["idcustomer" => $user->idcustomer, "mes" => $data["from"]]);
        $res_data = ["data" => $a_fin_group, "categories" => $data['categoria'], "maxhour" => $a_max, "afinidades" => $afinidades];
        return $res_data;
    }

    public function degradado($val)
    {
        switch ($val) {
            case 1:
                //return '#df3f34';
                return '#ffffff';
                break;
            case 2:
                //return '#d74334';
                return '#ffffff';
                break;
            case 3:
                //return '#cf4634';
                return '#ffffff';
                break;
            case 4:
                //return '#be4b32';
                return '#ffffff';
                break;
            case 5:
                //return '#b54e32';
                return '#ffffff';
                break;
            case 6:
                //return '#ac5230';
                return '#ffffff';
                break;
            case 7:
                //return '#a45530';
                return '#ffffff';
                break;
            case 8:
                //return '#9b5730';
                return '#ffffff';
                break;
            case 9:
                //return '#935b2f';
                return '#ffffff';
                break;
            case 10:
                //return '#8a5e2f';
                return '#ffffff';
                break;
            case 11:
                //return '#82612f';
                return '#ffffff';
                break;
            case 12:
                //return '#79632e';
                return '#ffffff';
                break;
            case 13:
                //return '#71672d';
                return '#ffffff';
                break;
            case 14:
                //return '#696a2d';
                return '#ffffff';
                break;
            case 15:
                return '#5f6d2c';
                break;
            case 16:
                return '#576f2c';
                break;
            case 17:
                return '#4f722b';
                break;
            case 18:
                return '#45762a';
                break;
            case 19:
                return '#3d792b';
                break;
            case 20:
                return '#357b2a';
                break;
            case 21:
                return '#2f7d2a';
                break;
            case 22:
                return '#2f7d29';
                break;
            case 23:
                return '#2f7d2a';
                break;
            case 24:
                return '#1f8328';
                break;
        }
    }

    public function converMonth($mes)
    {
        switch ($mes) {
            case 'Jan':
                return 1;
                break;
            case 'Feb':
                return 2;
                break;
            case 'Mar':
                return 3;
                break;
            case 'Apr':
                return 4;
                break;
            case 'May':
                return 5;
                break;
            case 'Jun':
                return 6;
                break;
            case 'Jul':
                return 7;
                break;
            case 'Aug':
                return 8;
                break;
            case 'Sep':
                return 9;
                break;
            case 'Oct':
                return 10;
                break;
            case 'Nov':
                return 11;
                break;
            case 'Dec':
                return 12;
                break;
            case 'Todos':
                return 13;
                break;
        }
    }

    public function getPatronVentas(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $categories = ProductCategory::whereIn('code', $data['categoria'])->pluck('code');
        $patternData = new PatternData($user->customer->idcustomer, $data['from']);
        $pd = $patternData->pd;
        $datData = $pd->whereIn('idcategory', $categories)
            ->where('weekday', '<', 9)
            ->where('hour', 0)
            ->sortBy('hour')
            ->unique();
        return $datData->values()->all();
    }

    public function getSociodemographic(Request $request)
    {
        $user = Auth::user();
        $req = $request->all();
        $ranges = [[0, 4], [5, 14], [15, 29], [30, 64], [65, 999]];
        if ($user->site) {
            $economic = \DB::table('profile_economic')
                ->join('sites', 'profile_economic.idsite', '=', 'sites.idsite')
                ->select('income_person', 'income_home')
                ->where('profile_economic.idsite', $user->site->idsite)
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->where('radio', $req['ratius'])
                ->get()->toArray();
            $rangeP = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $economic[0]->income_person)->where('rank_to', '>=', $economic[0]->income_person)->where('rank_type', 'P')->first();
            $rangeH = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $economic[0]->income_home)->where('rank_to', '>=', $economic[0]->income_home)->where('rank_type', 'H')->first();
            $economic[0]->income_person = $rangeP;
            $economic[0]->income_home = $rangeH;

            $demographic = \DB::table('profile_demographic')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->where('profile_demographic.idsite', $user->site->idsite)
                ->where('radio', $req['ratius'])
                ->orderBy('age_from')
                ->get()->toArray();
        } else if ($user->customer) {
            $sites = Site::select('idsite')->where('idcity', $req['city'])->where('idcustomer', $user->customer->idcustomer)->get()->toArray();
            $economic = \DB::table('profile_economic')
                ->select('income_person', 'income_home')
                ->whereIn('idsite', $sites)
                ->where('radio', $req['ratius'])
                ->get()->toArray();

            $demographic = \DB::table('profile_demographic')
                ->whereIn('idsite', $sites)
                ->where('radio', $req['ratius'])
                ->get();
        }
        if (count($economic) > 1) {
            $countSites = count($economic);
            $da = $economic;
            $economic = [];
            $economic[0]["income_person"] = 0;
            $economic[0]["income_home"] = 0;
            foreach ($da as $ec) {
                $economic[0]["income_person"] += $ec->income_person;
                $economic[0]["income_home"] += $ec->income_home;
            }
            $economic[0]["income_person"] = $economic[0]["income_person"] / $countSites;
            $economic[0]["income_home"] = $economic[0]["income_home"] / $countSites;
            $rangeP = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $economic[0]['income_person'])->where('rank_to', '>=', $economic[0]['income_person'])->where('rank_type', 'P')->first()->toArray();
            $rangeH = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $economic[0]['income_home'])->where('rank_to', '>=', $economic[0]['income_home'])->where('rank_type', 'H')->first()->toArray();
            $economic[0]["income_person"] = $rangeP['rank_from'] . ' - ' . $rangeP['rank_from'] . ' ' . $rangeP['rank_text'];
            $economic[0]["income_home"] = $rangeH['rank_from'] . ' - ' . $rangeH['rank_from'];
        }
        $data['economic'] = $economic;
        $data['demographic'] = [];
        $total = $this->getTotales($demographic);
        foreach ($ranges as $v => $range) {
            if ($v == count($ranges) - 1) {
                $rangeText = '>65';
            } else {
                $rangeText = $range[0] . '-' . $range[1];
            }
            foreach ($demographic as $demo) {
                if (!isset($data['demographic'][$demo->sex])) {
                    $data['demographic'][$demo->sex]['count'] = $this->getTotalSex($demographic, $demo->sex);
                    $data['demographic'][$demo->sex]['percentage'] = $this->getPercentage($data['demographic'][$demo->sex]['count'], $total);
                }
                if (!isset($data['demographic'][$demo->sex]['range'][$rangeText])) {
                    if (floatval($demo->age_from) >= $range[0] && floatval($demo->age_to) <= $range[1]) {
                        $data['demographic'][$demo->sex]['range'][$rangeText]['count'] = $demo->number;
                        $data['demographic'][$demo->sex]['range'][$rangeText]['range'] = $rangeText;
                        $data['demographic'][$demo->sex]['range'][$rangeText]['percentage'] = $this->getPercentage($demo->number, $data['demographic'][$demo->sex]['count']);
                        $stars = \DB::table('profile_demographic_star')->select('star_number')->where('sex', $demo->sex)->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->where('value', '<=', $data['demographic'][$demo->sex]['range'][$rangeText]['percentage'])->orderBy('value', 'DESC')->get()->toArray();
                        if (count($stars) > 0)
                            $data['demographic'][$demo->sex]['range'][$rangeText]['stars'] = $stars[0]->star_number;
                        $totalStars =  \DB::table('profile_demographic_star')->select('star_number', 'value')->where('sex', 'T')->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->orderBy('value', 'ASC')->get()->toArray();
                        $data['demographic'][$demo->sex]['range'][$rangeText]['totals_stars'] = $totalStars;
                    }
                } else {
                    if (floatval($demo->age_from) >= $range[0] && floatval($demo->age_to) <= $range[1]) {
                        $data['demographic'][$demo->sex]['range'][$rangeText]['count'] += $demo->number;
                        $data['demographic'][$demo->sex]['range'][$rangeText]['percentage'] = $this->getPercentage($data['demographic'][$demo->sex]['range'][$rangeText]['count'], $data['demographic'][$demo->sex]['count']);
                        $stars = \DB::table('profile_demographic_star')->select('star_number')->where('sex', $demo->sex)->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->where('value', '<=', $data['demographic'][$demo->sex]['range'][$rangeText]['percentage'])->orderBy('value', 'DESC')->get()->toArray();
                        if (count($stars) > 0)
                            $data['demographic'][$demo->sex]['range'][$rangeText]['stars'] = $stars[0]->star_number;
                    }
                }
            }
        }
        return $data;
    }

    private function getTotalSex($data, $sex)
    {
        $total = 0;
        foreach ($data as $d) {
            if ($d->sex == $sex) {
                $total += $d->number;
            }
        }
        return $total;
    }

    private function getTotales($data)
    {
        $total = 0;
        foreach ($data as $d) {
            $total += $d->number;
        }
        return $total;
    }

    private function getPercentage($value, $total)
    {
        return ($value * 100) / $total;
    }

    public function getProvinces()
    {
        $user = Auth::user();
        $idCustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idCustomer = 7;
        $sites = \DB::table('profile_demographic')->select('profile_demographic.idsite')
            ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
            ->where('profile_demographic.idcustomer', $idCustomer)
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->distinct()->get()->pluck('idsite');

        $idprovinces = Site::select('provincies.idprovince')
            ->join('cities', 'sites.idcity', '=', 'cities.idcity')
            ->join('provincies', 'cities.idprovince', '=', 'provincies.idprovince')
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->whereIn('sites.idsite', $sites)->distinct()->get();
        $provinces = Province::whereIn('provincies.idprovince', $idprovinces)->get();

        foreach ($provinces as $province) {
            $sites = Site::join('cities', 'sites.idcity', '=', 'cities.idcity')->join('provincies', 'cities.idprovince', '=', 'provincies.idprovince')->where('provincies.idprovince', $province->idprovince)->where('idcustomer', $idCustomer)->where('sites.deleted', 0)->where('sites.status', 1)->pluck('idsite');
            $province['sites'] = count($sites);
        }

        return $provinces;
    }

    public function getTrendsProvinces()
    {
        $user = Auth::user();
        $idCustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idCustomer = 7;

        $idcountries = Site::select('countries.idcountry')
            ->join('cities', 'sites.idcity', '=', 'cities.idcity')
            ->join('provincies', 'cities.idprovince', '=', 'provincies.idprovince')
            ->join('countries', 'countries.idcountry', '=', 'provincies.idcountry')
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->where('sites.idcustomer', $idCustomer)->distinct()->get();
        $provinces = \DB::table('local_area')->whereIn('idcountry', $idcountries)->get();
        return $provinces;
    }

    public function getSitesFromProvinces(Request $request = null)
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idcustomer = 7;
        $data = $request->all();
        if (!isset($data['province'])) {
            if ($user->is_admin()) {
                $sites = Site::where('idcustomer', $idcustomer)
                    ->where('deleted', 0)
                    ->where('status', 1)
                    ->get();
            } else if ($user->is_user()) {
                $sites = Site::where('idsite', $user->site->idsite)->first();
            }
        } else {
            $sites = Site::select('sites.code', 'sites.name', 'sites.idsite')
                ->join('cities', 'sites.idcity', '=', 'cities.idcity')
                ->join('provincies', 'cities.idprovince', '=', 'provincies.idprovince')
                ->whereIn('provincies.idprovince', $data['province'])
                ->where('sites.idcustomer', $idcustomer)
                ->where('deleted', 0)
                ->where('status', 1)
                ->get();
        }
        return $sites;
    }

    public function getRatius(Request $request = null)
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idcustomer = 7;
        $data = $request->all();
        if (!isset($data['site'])) {
            if ($user->is_admin()) {
                $sites = Site::where('idcustomer', $idcustomer)
                    ->where('deleted', 0)
                    ->where('status', 1)
                    ->distinct()
                    ->pluck('idsite');
            } else if ($user->is_user()) {
                $sites = Site::where('idsite', $user->site->idsite)
                    ->pluck('idsite');
            }
            $ratius = \DB::table('profile_demographic')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->whereIn('profile_demographic.idsite', $sites)
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->distinct()
                ->pluck('radio');
        } else {
            if (env('DEMO')) {
                $sites = \DB::table('profile_demographic')->select('idsite')
                    ->where('idcustomer', 7)
                    ->distinct()->get()->pluck('idsite');
                $sitescount = count($data['site']);
                $data['site'] = [];
                for ($i = 0; $i <= $sitescount; $i++) {
                    if (isset($sites[$i]))
                        $data['site'][] = $sites[$i];
                }
            }

            $ratius = \DB::table('profile_demographic')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->whereIn('profile_demographic.idsite', $data['site'])
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->distinct()
                ->pluck('radio');
        }
        return $ratius;
    }
    public function getAfinidadesRecomendaciones(Request $request)
    {
        $data = $request->all();
        $getAfin = $this->getAfin($data);
        if ($getAfin->isNotEmpty()) {
            $getAfinidades = $getAfin[$data['hour']];
            $a_data = ["data" => $getAfinidades, "horapunta" => $data['hour']];
            return json_encode(['success' => true, 'data' => $a_data]);
        }
        return json_encode(['success' => false]);
    }

    public function getAfin($data)
    {
        $date  = Carbon::now();
        $year = $date->year;
        $month = $date->month;
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO')) {
            $idcustomer = 31;
        }
        $diasmes = cal_days_in_month(CAL_GREGORIAN, $data['mes'], $year); // 31
        $tmpcero = '';
        if (strlen($data['mes'] < 10)) {
            $tmpcero = '0';
        }
        if ($data['mes'] >= $month)
            $year--;

        $minDate_ = $year . "-" . $tmpcero . $data['mes'] . "-01";
        $maxDate_ = $year . "-" . $tmpcero . $data['mes'] . "-" . $diasmes;

        $dat = [
            "customer_id" => $idcustomer,
            "date" => ['$gte' => $minDate_, '$lte' => $maxDate_]
        ];
        if ($user->idsite && !env('DEMO')) {
            $dat['site_id'] = $user->idsite;
        }
        $getAfinidades = $this->getWsPeople('people', $idcustomer, 'POST', $dat, true);
        return $getAfinidades;
    }

    public function getAfinFilasSemana($data)
    {
        $date  = Carbon::now();
        $year = $date->year;
        $month = $date->month;
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO')) {
            $idcustomer = 31;
        }
        $diasmes = cal_days_in_month(CAL_GREGORIAN, $data['mes'], $year); // 31
        $tmpcero = '';
        if (strlen($data['mes'] < 10)) {
            $tmpcero = '0';
        }
        if ($data['mes'] >= $month)
            $year--;

        $minDate_ = $year . "-" . $tmpcero . $data['mes'] . "-01";
        $maxDate_ = $year . "-" . $tmpcero . $data['mes'] . "-" . $diasmes;

        $dat = [
            "filter" => [
                "customer_id" => $idcustomer,
                "date" => ['$gte' => $minDate_, '$lte' => $maxDate_]
            ],
            "group" => [
                "_id" => ["gender" => '$gender', "hour" => '$hour', "type" => '$type'],
                "count" => ['$sum' => 1]
            ]
        ];
        if ($user->idsite && !env('DEMO')) {
            $dat['filter']['site_id'] = $user->idsite;
        }
        $getAfinidades = $this->getWsPeople('people/filterAndGroup', $idcustomer, 'POST', $dat, true);
        return $getAfinidades;
    }

    public function getTemperatura(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $datData = \DB::table('erp_category_temperatura')
            ->select('estado', 'incr_sobre_media')
            ->where('idcustomer', $user->idcustomer)
            ->where('idcategory', $data['idcategory'])
            ->get();
        if ($datData->count() == 0) {
            return false;
        } else {
            $df = $datData->first();
            return ['estado' => $df->estado, 'inc' => $df->incr_sobre_media];
        }
    }

    public function getProductsFromCategory(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $category = ProductCategory::where('idcategory', $data['category'])->first();
        $products = Product::with('category')->where('idcustomer', $user->idcustomer)->where('idcategory', $category->idcategory)->get();
        $patternDataRecomendation = new PatternDataRecomendations($user->customer->idcustomer, $data['mes']);
        $pdr = $patternDataRecomendation->pdr;
        $patterndat = $pdr->where('nivelagregacion', "PR")
            ->sortByDesc('value');

        $a_product_list = [];
        foreach ($patterndat as $valpat) {
            $nameprod = $products->where('code', $valpat['code_reference'])->first();
            if (!empty($nameprod)) {
                $tmpprod = ['name' => $nameprod['name'], 'code' => $valpat['code_reference'], 'value' => $valpat['value'], 'category' => $nameprod['category'], 'idproduct' => $nameprod['idproduct']];
                array_push($a_product_list, $tmpprod);
            }
        }
        return $a_product_list;
    }

    public function getSocialEconomicAdminData(Request $request)
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        $data = $request->all();
        $ranges = [[0, 4], [5, 14], [15, 29], [30, 64], [65, 999]];

        if (env('DEMO')) {
            $idcustomer = 7;
            $sites = \DB::table('profile_demographic')->select('idsite')
                ->where('idcustomer', 7)
                ->distinct()->get()->pluck('idsite');

            $sitescount = count($data['site']);
            $data['site'] = [];
            for ($i = 0; $i <= $sitescount; $i++) {
                if (isset($sites[$i]))
                    $data['site'][] = $sites[$i];
            }
        }

        $totalDemographic = \DB::table('profile_demographic')
            ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
            ->groupBy('sex')
            ->groupBy('age_from')
            ->select('sex', 'age_from', 'age_to')
            ->selectRaw('sum(number) as count')
            ->where('profile_demographic.idcustomer', $idcustomer)
            ->where('radio', $data['ratius'])
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->orderBy('sex', 'ASC')
            ->orderBy('age_from', 'ASC')
            ->get()->toArray();

        $selectedDemographic = \DB::table('profile_demographic')
            ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
            ->groupBy('age_from')
            ->groupBy('sex')
            ->select('sex', 'age_from', 'age_to')
            ->selectRaw('sum(number) as count')
            ->where('profile_demographic.idcustomer', $idcustomer)
            ->whereIn('profile_demographic.idsite', $data['site'])
            ->where('radio', $data['ratius'])
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->orderBy('sex', 'ASC')
            ->orderBy('age_from', 'ASC')
            ->get()->toArray();

        $demographic = [];
        $sDemographic = [];

        $total['total'] = 0;
        foreach ($totalDemographic as $demo) {
            if (!isset($total[$demo->sex])) {
                $total[$demo->sex] = $demo->count;
            } else {
                $total[$demo->sex] += $demo->count;
            }
            $total['total'] += $demo->count;
        }
        $totalSelec = [];
        $totalSelec['total'] = 0;
        foreach ($selectedDemographic as $demo) {
            if (!isset($totalSelec[$demo->sex])) {
                $totalSelec[$demo->sex] = $demo->count;
            } else {
                $totalSelec[$demo->sex] += $demo->count;
            }
            $totalSelec['total'] += $demo->count;
        }

        $demographic['total'] = $total['total'];
        $sDemographic['total'] = $totalSelec['total'];
        $demoTotales['total'] = $total['total'];
        foreach ($ranges as $v => $range) {
            if ($v == count($ranges) - 1) {
                $rangeText = '>65';
            } else {
                $rangeText = $range[0] . '-' . $range[1];
            }

            foreach ($totalDemographic as $demo) {
                if (!isset($demographic[$demo->sex])) {
                    $demographic[$demo->sex] = [];
                    $demographic[$demo->sex]['total'] = $total[$demo->sex];
                    $demographic[$demo->sex]['percentage'] = ($total[$demo->sex] * 100) / $total['total'];
                }
                if (!isset($demographic[$demo->sex]['range'][$rangeText])) {
                    $demographic[$demo->sex]['range'][$rangeText]['range'] = $rangeText;
                    $demographic[$demo->sex]['range'][$rangeText]['count'] = 0;
                }
                if (floatval($demo->age_from) >= $range[0] && floatval($demo->age_to) <= $range[1]) {
                    $demographic[$demo->sex]['range'][$rangeText]['count'] += $demo->count;
                    $demographic[$demo->sex]['range'][$rangeText]['total_percentage'] = ($demographic[$demo->sex]['range'][$rangeText]['count'] * 100) / $total[$demo->sex];
                }
            }
            foreach ($selectedDemographic as $demo) {
                if (!isset($sDemographic[$demo->sex])) {
                    $sDemographic[$demo->sex] = [];
                    $sDemographic[$demo->sex]['total'] = $totalSelec[$demo->sex];
                    $sDemographic[$demo->sex]['percentage'] = ($totalSelec[$demo->sex] * 100) / $totalSelec['total'];
                }
                if (!isset($sDemographic[$demo->sex]['range'][$rangeText])) {
                    $sDemographic[$demo->sex]['range'][$rangeText]['range'] = $rangeText;
                    $sDemographic[$demo->sex]['range'][$rangeText]['count'] = 0;
                    $sDemographic[$demo->sex]['range'][$rangeText]['total_percentage'] = ($demographic[$demo->sex]['range'][$rangeText]['count'] * 100) / $total[$demo->sex];
                    $sDemographic[$demo->sex]['range'][$rangeText]['select_percentage'] = ($sDemographic[$demo->sex]['range'][$rangeText]['count'] * 100) / $totalSelec[$demo->sex];
                    $stars = \DB::table('profile_demographic_star')->select('star_number')->where('sex', $demo->sex)->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->where('value', '<=', $sDemographic[$demo->sex]['range'][$rangeText]['select_percentage'])->orderBy('value', 'DESC')->get()->toArray();
                    if (count($stars) > 0)
                        $sDemographic[$demo->sex]['range'][$rangeText]['stars'] = $stars[0]->star_number;
                }
                if (floatval($demo->age_from) >= $range[0] && floatval($demo->age_to) <= $range[1]) {
                    $sDemographic[$demo->sex]['range'][$rangeText]['count'] += $demo->count;
                    $sDemographic[$demo->sex]['range'][$rangeText]['total_percentage'] = ($demographic[$demo->sex]['range'][$rangeText]['count'] * 100) / $total[$demo->sex];
                    $sDemographic[$demo->sex]['range'][$rangeText]['select_percentage'] = ($sDemographic[$demo->sex]['range'][$rangeText]['count'] * 100) / $totalSelec[$demo->sex];
                    $stars = \DB::table('profile_demographic_star')->select('star_number')->where('sex', $demo->sex)->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->where('value', '<=', $sDemographic[$demo->sex]['range'][$rangeText]['select_percentage'])->orderBy('value', 'DESC')->get()->toArray();
                    $sDemographic[$demo->sex]['range'][$rangeText]['stars'] = null;
                    if (count($stars) > 0)
                        $sDemographic[$demo->sex]['range'][$rangeText]['stars'] = $stars[0]->star_number;
                }
                if (floatval($demo->age_from) >= $range[0] && floatval($demo->age_to) <= $range[1]) {
                    if (!isset($demoTotales['range'][$rangeText])) {
                        $demoTotales['range'][$rangeText]['range'] = $rangeText;
                        $demoTotales['range'][$rangeText]['count'] = $demo->count;
                        $demoTotales['range'][$rangeText]['total_percentage'] = (($demographic['H']['range'][$rangeText]['count'] + $demographic['H']['range'][$rangeText]['count']) * 100) / $total['total'];
                        $demoTotales['range'][$rangeText]['select_percentage'] = (($sDemographic['H']['range'][$rangeText]['count'] + $sDemographic['H']['range'][$rangeText]['count']) * 100) / $totalSelec['total'];
                    } else {
                        $demoTotales['range'][$rangeText]['count'] += $demo->count;
                        $demoTotales['range'][$rangeText]['total_percentage'] = (($demographic['H']['range'][$rangeText]['count'] + $demographic['M']['range'][$rangeText]['count']) * 100) / $total['total'];
                        $demoTotales['range'][$rangeText]['select_percentage'] = ($demoTotales['range'][$rangeText]['count'] * 100) / $totalSelec['total'];
                    }
                    $stars = \DB::table('profile_demographic_star')->select('star_number')->where('sex', 'T')->where('age_from', '>=', $range[0])->where('age_to', '<=', $range[1])->where('value', '<=', $demoTotales['range'][$rangeText]['select_percentage'])->orderBy('value', 'DESC')->get()->toArray();
                    $demoTotales['range'][$rangeText]['stars'] = null;
                    if (count($stars) > 0)
                        $demoTotales['range'][$rangeText]['stars'] = $stars[0]->star_number;
                }
            }
        }
        $response['totalDemo'] = $demoTotales;
        $response['selectedDemo'] = $sDemographic;

        $economic = \DB::table('profile_economic')
            ->join('sites', 'profile_economic.idsite', '=', 'sites.idsite')
            ->selectRaw('sum(numhombres+nummujeres) as population, income_person, income_home')
            ->groupBy('profile_economic.idsite')
            ->where('profile_economic.idcustomer', $idcustomer)
            ->whereIn('profile_economic.idsite', $data['site'])
            ->where('sites.deleted', 0)
            ->where('sites.status', 1)
            ->where('radio', $data['ratius'])
            ->get();

        $population = 0;
        $populationXincomePerson = 0;
        $populationXincomeHome = 0;
        foreach ($economic as $eco) {
            $population += $eco->population;
            $populationXincomePerson += $eco->population * $eco->income_person;
            $populationXincomeHome += $eco->population * $eco->income_home;
        }

        $income_person = $populationXincomePerson / $population;
        $income_home = $populationXincomeHome / $population;

        $rangeP = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $income_person)->where('rank_to', '>=', $income_person)->where('rank_type', 'P')->first();
        $rangeH = \DB::table('profile_economic_ranks')->where('rank_from', '<=', $income_home)->where('rank_to', '>=', $income_home)->where('rank_type', 'H')->first();
        $response['economic']['income_person'] = $rangeP;
        $response['economic']['income_home'] = $rangeH;
        return $response;
    }

    public function getAgeVars()
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idcustomer = 7;
        $ageVars = \DB::table('profile_demographic')
            ->select('age_from', 'age_to')
            ->where('idcustomer', $idcustomer)
            ->distinct()
            ->get();
        return $ageVars;
    }

    public function getIncomeRanges(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $incomeRanges = \DB::table('profile_economic_ranks')
            ->select(\DB::raw("CONCAT(min(rank_from),'-',max(rank_to)) as rank"), 'rank_text')
            ->where('rank_type', 'P')
            ->groupBy('rank_text')
            ->orderBy('rank_from');
        if (isset($data['ratius']) || isset($data['province'])) {
            $res = \DB::table('profile_economic');
            if (isset($data['province'])) {
                $res->join('sites', 'profile_economic.idsite', '=', 'sites.idsite')
                    ->join('cities', 'sites.idcity', '=', 'cities.idcity')
                    ->where('sites.deleted', 0)
                    ->where('sites.status', 1)
                    ->whereIn('cities.idprovince', $data['province']);
            }
            if (isset($data['ratius'])) {
                $res->where('profile_economic.radio', $data['ratius']);
            }

            $res->where('profile_economic.idcustomer', $user->customer->idcustomer);
            $res->get();
            $incomeRanges->when(request('rank_type') == 'P', function ($query) use ($res) {
                $query->where('rank_from', '>=', $res->min('income_person'));
                $query->where('rank_to', '<=', $res->max('income_person'));
            });
            $incomeRanges->when(request('rank_type') == 'H', function ($query) use ($res) {
                $query->where('rank_from', '>=', $res->min('income_home'));
                $query->where('rank_to', '<=', $res->max('income_home'));
            });
        }

        return $incomeRanges->get();
    }

    public function getDemographicSegmentation(Request $request)
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idcustomer = 7;
        $data = $request->all();

        if (!isset($data['sites'])) {
            $res = \DB::table('profile_demographic')
                ->select('profile_demographic.idsite')
                ->selectRaw('sum(number) as count')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->join('cities', 'sites.idcity', '=', 'cities.idcity')
                ->whereIn('cities.idprovince', $data['common']['province'])
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->where('profile_demographic.idcustomer', $idcustomer)
                ->where('profile_demographic.radio', $data['common']['ratius'])
                ->whereIn('profile_demographic.sex', $data['demographic']['sex'])
                ->where('profile_demographic.age_from', '>=', $data['demographic']['age_from'])
                ->where('profile_demographic.age_to', '<=', $data['demographic']['age_to'])
                ->groupBy('profile_demographic.idsite')
                ->get();
            $all = \DB::table('profile_demographic')
                ->select('profile_demographic.idsite')
                ->selectRaw('sum(number) as count')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->join('cities', 'sites.idcity', '=', 'cities.idcity')
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->whereIn('cities.idprovince', $data['common']['province'])
                ->where('profile_demographic.idcustomer', $idcustomer)
                ->where('profile_demographic.radio', $data['common']['ratius'])
                ->groupBy('profile_demographic.idsite')
                ->get();
        } else {
            $res = \DB::table('profile_demographic')
                ->select('idsite')
                ->selectRaw('sum(number) as count')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->whereIn('profile_demographic.idsite', $data['sites'])
                ->where('profile_demographic.idcustomer', $idcustomer)
                ->where('profile_demographic.radio', $data['common']['ratius'])
                ->whereIn('profile_demographic.sex', $data['demographic']['sex'])
                ->where('profile_demographic.age_from', '>=', $data['demographic']['age_from'])
                ->where('profile_demographic.age_to', '<=', $data['demographic']['age_to'])
                ->groupBy('profile_demographic.idsite')
                ->get();
            $all = \DB::table('profile_demographic')
                ->select('profile_demographic.idsite')
                ->selectRaw('sum(number) as count')
                ->join('sites', 'profile_demographic.idsite', '=', 'sites.idsite')
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->whereIn('profile_demographic.idsite', $data['sites'])
                ->where('profile_demographic.idcustomer', $idcustomer)
                ->where('profile_demographic.radio', $data['common']['ratius'])
                ->groupBy('profile_demographic.idsite')
                ->get();
        }
        $percentages = [];
        foreach ($all as $tot) {
            foreach ($res as $un) {
                if ($tot->idsite === $un->idsite) {
                    $percentages[$tot->idsite] = ($un->count * 100) / $tot->count;
                }
            }
        }
        $total = 0;
        $numSites = count($res);
        $sites = [];
        if ($numSites > 0) {
            foreach ($percentages as $r) {
                $total += $r;
            }
            $media = $total / $numSites;

            $sumatorio = 0;
            foreach ($percentages as $r) {
                $sumatorio += (($r - $media) * ($r - $media));
            }
            $desviacion = sqrt($sumatorio / $numSites);
            $media += ($desviacion * 0.5);
            foreach ($percentages as $v => $r) {
                if ($r >= $media) {
                    $sites[] = $v;
                }
            }
        }
        return Site::whereIn('idsite', $sites)->get();
    }

    public function getEconomicSegmentation(Request $request)
    {
        $user = Auth::user();
        $idcustomer = $user->customer->idcustomer;
        if (env('DEMO'))
            $idcustomer = 7;
        $data = $request->all();
        if (isset($data['economic']['income_person'])) {
            $rangePersonal = [];
            foreach ($data['economic']['income_person'] as $income) {
                $ranges = explode('-', $income);
                foreach ($ranges as $range) {
                    $rangePersonal[] = $range;
                }
            }
        }
        if (isset($data['economic']['income_home'])) {
            $rangeHome = explode('-', $data['economic']['income_home']);
        }
        if (!isset($data['sites'])) {
            $res = \DB::table('profile_economic')
                ->select('profile_economic.idsite')
                ->join('sites', 'profile_economic.idsite', '=', 'sites.idsite')
                ->join('cities', 'sites.idcity', '=', 'cities.idcity')
                ->whereIn('cities.idprovince', $data['common']['province'])
                ->where('profile_economic.idcustomer', $idcustomer)
                ->where('sites.deleted', 0)
                ->where('sites.status', 1);
            if (isset($rangePersonal)) {
                $res->where('income_person', '>=', floatval($rangePersonal[0]))
                    ->where('income_person', '<=', floatval($rangePersonal[1]));
            }
            if (isset($rangeHome)) {
                $res->where('income_home', '>=', floatval($rangeHome[0]))
                    ->where('income_home', '<=', floatval($rangeHome[1]));
            }

            $res->get()->pluck('profile_economic.idsite');
        } else {
            $res = \DB::table('profile_economic')
                ->select('profile_economic.idsite')
                ->join('sites', 'profile_economic.idsite', '=', 'sites.idsite')
                ->where('sites.deleted', 0)
                ->where('sites.status', 1)
                ->whereIn('profile_economic.idsite', $data['sites'])
                ->where('profile_economic.idcustomer', $idcustomer)
                ->where('sites.deleted', 0)
                ->where('sites.status', 1);

            if (isset($rangePersonal)) {
                $res->where('income_person', '>=', floatval($rangePersonal[0]))
                    ->where('income_person', '<=', floatval($rangePersonal[1]));
            }
            if (isset($rangeHome)) {
                $res->where('income_home', '>=', floatval($rangeHome[0]))
                    ->where('income_home', '<=', floatval($rangeHome[1]));
            }
            $res->get()->pluck('profile_economic.idsite');
        }

        $sites = Site::whereIn('idsite', $res)->get();

        return $sites;
    }
}
