<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Repositories\User\UserRepositoryInterface;
use App\Exceptions\NotFoundException;
use App\Exceptions\UserLoginException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use App\Exceptions\NotValidUserException;

class UserRepository implements UserRepositoryInterface
{

    public static function register(array $data, string $role)
    {
        $user = User::create($data);
        if (!$user)
            throw new NotFoundException('Error al insertar usuario');

        $user->assignRole($role);

        return $user;
    }

    public static function update(int $id, Request $request)
    {
        $user = User::find($id);
        if (!$user) {
            throw new NotFoundException('No se ha encontrado el usuario.');
        }
        $input = $request->all();

        if ($user['email'] != $input['email']) {
            $user->email = $input['email'];
        }

        if (isset($input['password'])) {
            $user->password = bcrypt($request['password']);
        }


        $user->idcategories = $input['idcategories'] ? $input['idcategories'] : null;
        $user->idcircuits = $input['idcircuits'] ? $input['idcircuits'] : null;
        $user->idsite = $input['idsite'] ? $input['idsite'] : null;


        if (isset($input['name'])) $user->name = $input['name'];
        if (isset($input['email'])) $user->email = $input['email'];
        if (isset($input['idcustomer'])) $user->idcustomer = $input['idcustomer'];

        if (isset($input['status'])) $user->status = $input['status'];
        $user->save();

        if (!$user->hasRole($input['role'])) {
            $user->roles()->detach();
        }
        if (isset($input['role'])) $user->assignRole($input['role']);
        return $user;
    }

    public static function delete(int $id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new NotFoundException('No se ha encontrado el usuario.');
        }
        $user->deleted = true;
        $user->save();
        return $user;
    }

    public static function addPermission(Request $request)
    {
        $user = User::find($request->userId);
        if (!$user) {
            throw new NotFoundException('No se ha encontrado el usuario.');
        }
        $user->givePermissionTo($request->permissionName);
        $permissions = $user->getAllPermissions();
        return $permissions;
    }

    public static function revokePermission(Request $request)
    {
        $user = User::find($request->userId);
        if (!$user) {
            throw new NotFoundException('No se ha encontrado el usuario.');
        }
        $user->revokePermissionTo($request->permissionName);
        $permissions = $user->getAllPermissions();
        return $permissions;
    }

    public static function login(Request $request)
    {
        $name = $request->name;
        $password = $request->password;

        if (Auth::attempt(['name' => $name, 'password' => $password])) {
            $user = $request->user();
            $response = self::getTokenAndRefreshToken($user);
        } else {
            throw new UserLoginException;
        }
        return $response;
    }

    public static function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return true;
    }

    public static function me()
    {
        $user = Auth::user();

        $finalUser['user'] = User::with('site')
            ->with('customer')
            ->with('config_customer')
            ->with('langs')
            ->find($user->iduser)->toArray();
        $finalUser['user']['permissions'] = $user->getAllPermissions();
        $finalUser['user']['roles'] = $user->roles;

        return $finalUser;
    }

    public static function user($user)
    {
        $user = User::with('site')->with('customer')->find($user);
        $finalUser['user'] = $user->toArray();
        $finalUser['user']['permissions'] = $user->getAllPermissions();
        $finalUser['user']['roles'] = $user->roles;

        return $finalUser;
    }

    public static function users()
    {
        $users = User::with('site')->with('customer')->with('roles')->where('deleted', 0)->get();
        return $users;
    }

    public static function getTokenAndRefreshToken(User $user)
    {
        $tokenResult = $user->createToken($user->email);
        $token = $tokenResult->token;
        $token->save();
        $response = [
            'data' => [
                'access_token' => $tokenResult->accessToken,
                'token_type'   => 'Bearer',
                'expires_at'   => Carbon::parse(
                    $tokenResult->token->expires_at
                )
                    ->toDateTimeString(),
            ]
        ];

        return $response;
    }
}
