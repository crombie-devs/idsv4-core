<?php

namespace App\Repositories\User;

use Illuminate\Http\Request;
use App\Models\User;

interface UserRepositoryInterface {
    
    public static function register(array $data, string $role);
    public static function update(int $id, Request $request);
    public static function delete(int $id);
    public static function addPermission(Request $request);

    public static function login(Request $request);
    public static function logout(Request $request);

    public static function me();
    public static function user(int $id);
    public static function users();
    
    public static function getTokenAndRefreshToken(User $user);
}