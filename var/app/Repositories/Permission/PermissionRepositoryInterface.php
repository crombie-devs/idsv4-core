<?php

namespace App\Repositories\Permission;

use Illuminate\Http\Request;

interface PermissionRepositoryInterface {
    public function create(Request $request);
    public function update(int $id, Request $request);
    public function delete(int $id);

    public function permissions();
    public function permission(int $id);

    public function response(bool $success, $data, int $statusCode);
}