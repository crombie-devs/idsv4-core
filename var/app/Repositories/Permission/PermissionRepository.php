<?php

namespace App\Repositories\Permission;

use Illuminate\Http\Request;
use App\Repositories\Permission\PermissionRepositoryInterface;
use Spatie\Permission\Models\Permission;

class PermissionRepository implements PermissionRepositoryInterface {
    const SUCCESS_STATUS_CODE = 200;
    const NOT_FOUND_STATUS_CODE = 404;

    public function create(Request $request){
        $permission = Permission::create(['name' => $request['name']]);
        return $this->response(true, $permission, self::SUCCESS_STATUS_CODE);
    }

    public function update(int $id, Request $request){
        $permission = Permission::where('id', $id)->first();
        if(!$permission){
            return $this->response(false, ['message' => 'Permiso no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        $permission->name = $request['name'];
        $permission->save();
        return $this->response(true, $permission, self::SUCCESS_STATUS_CODE);
    }

    public function delete(int $id){
        $permission = Permission::where('id', $id)->first();
        if(!$permission){
            return $this->response(false, ['message' => 'Permiso no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        $permission->delete();
        return $this->response(true, null, self::SUCCESS_STATUS_CODE);
    }

    public function permission(int $id){
        $permission = Permission::where('id', $id)->first();
        if(!$permission){
            return $this->response(false, ['message' => 'Permiso no encontrado'], self::NOT_FOUND_STATUS_CODE);
        }
        return $this->response(true, $permission, self::SUCCESS_STATUS_CODE);
    }

    public function permissions(){
        $permissions = Permission::all();
        if(!$permissions){
            return $this->response(false,['message' => 'No se han encontrado permisos.'], self::NOT_FOUND_STATUS_CODE);
        }
        return $this->response(true, $permissions, self::SUCCESS_STATUS_CODE);
    }

    public function response(bool $success, $data, int $statusCode) {
        $response = ["success" => $success, "data"=>$data, "statusCode"=>$statusCode];
        return $response;
    }
}