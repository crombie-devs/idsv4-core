<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;

class ProcessPlayCircuits implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $idsite;
    protected $idcircuits;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $idsite, array $idcircuits)
    {
        $this->idsite = $idsite;
        $this->idcircuits = $idcircuits;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        \DB::table('circuits_has_sites')->where('idsite',$this->idsite)->delete();
        foreach ($this->idcircuits as $idcircuit) {
            \DB::table('circuits_has_sites')
            ->insert(
                ['idsite' => $this->idsite,
                'idcircuit' => $idcircuit]);

        }
    }

   
}
