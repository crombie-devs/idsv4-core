<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;

class ProcessProfileDemographicDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $idsite;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $idsite)
    {
        $this->idsite = $idsite;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        \DB::table('profile_demographic')->where('idsite',$this->idsite)->delete();
        \DB::table('profile_economic')->where('idsite',$this->idsite)->delete();
        
    }

   
}
