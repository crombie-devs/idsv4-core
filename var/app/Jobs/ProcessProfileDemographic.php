<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;
use GuzzleHttp\Exception\RequestException;

class ProcessProfileDemographic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $idsite;
    protected $idcustomer;
    protected $longitude;
    protected $latitude;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $idsite, int $idcustomer, $longitude, $latitude)
    {
        $this->idsite = $idsite;
        $this->idcustomer = $idcustomer;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!empty($longitude) && !empty($latitude))
            $data = self::getData($this->idcustomer, $this->longitude, $this->latitude);
       
        \DB::table('profile_demographic')->where('idsite',$this->idsite)->where('idcustomer',$this->idcustomer)->delete();
        \DB::table('profile_economic')->where('idsite',$this->idsite)->where('idcustomer',$this->idcustomer)->delete();
        if(!empty($data)){
            foreach ($data->demografico as $demografico) {
                \DB::table('profile_demographic')
                                    ->insert(
                                        ['idsite' => $this->idsite,
                                        'idcustomer' => $this->idcustomer,
                                        'radio' => $demografico->radio,
                                        'sex' => $demografico->sex,
                                        'age_from' => $demografico->age_from,
                                        'age_to' => $demografico->age_to,
                                        'number' => $demografico->number]);
            }
            foreach ($data->socioeconomico as $socioeconomico) {
                \DB::table('profile_economic')
                                    ->insert(
                                        ['idsite' => $this->idsite,
                                        'idcustomer' => $this->idcustomer,
                                        'radio' => $socioeconomico->radio,
                                        'income_person' => $socioeconomico->income_person,
                                        'income_home' => $socioeconomico->income_home,
                                        'numhombres' => $socioeconomico->numhombres,
                                        'nummujeres' => $socioeconomico->nummujeres]);
            }
            \DB::table('sites')->where('idsite',$this->idsite)->update(['has_socioeconomic' => 1]);
            
        }

        \DB::table('sites')->where('idsite',$this->idsite)->update(['has_socioeconomic' => 0]);       

    }

    public function getData(int $idcustomer, float $longitude, float $latitude){

        try{
            $client = new \GuzzleHttp\Client();
            $data = [
                "idcustomer" => $idcustomer,
                "longitude" => $longitude,
                "latitude" => $latitude        
            ];

            $response = $client->request('POST','http://82.223.69.121:5000/site-economic', ['json' => $data]);
            
            if(!empty($response->getBody())){
                $json = json_decode($response->getBody());
                return $json->data;
            }
            return null;
        }
        catch (RequestException $exception) {
            return null;
        }

    }
}
