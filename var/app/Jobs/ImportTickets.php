<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Repositories\Product\ProductRepositoryInterface;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TicketsImport;

class ImportTickets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $productrepository;
    private $idcustomer;
    private $tickets;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProductRepositoryInterface $productRepository, int $idcustomer, string $tickets)
    {
        $this->productrepository = $productRepository;
        $this->idcustomer = $idcustomer;
        $this->tikets = $tickets;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo $this->tickets . PHP_EOL;
        Excel::import(new TicketsImport($this->productrepository, $this->idcustomer), $this->tickets, null, \Maatwebsite\Excel\Excel::XLSX);
    }
}
