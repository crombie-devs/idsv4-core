<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetRequestNotification extends Notification
{
    use Queueable;
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('https://'.request()->getHttpHost().'/panel/login?token='.$this->token.'&email='.$notifiable->email);

        return (new MailMessage)
            ->from('soporte@ladorian.com','Soporte LadorianIds')
            ->subject('Reseteo Password Ladorian Ids')
            ->greeting('Hola')
            ->line('Has recibido este email porque hemos recibido una petición de cambio de contraseña en nuestra web. Para continuar haz clic en el siguiente botón.')
            ->action('Cambiar contraseña', url($url))
            ->line('Si no has pedido tu el cambio de contraseña, no es necesario que realices ninguna acción.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
