<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Province\ProvinceToSelectRequest;
use App\Repositories\Province\ProvinceRepositoryInterface;


class ProvinceController extends Controller
{
    protected $provinceRepository;

    public function __construct(ProvinceRepositoryInterface $provinceRepository)
    {
        $this->provinceRepository = $provinceRepository;   
    }


    /**
     * Acceso a todos las provinces de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group Province management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->provinceRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a todos las provinces de la base de datos desde un select By Country
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group Province management
     */
    public function toSelectByCountry($idcountry){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->provinceRepository->toSelect($idcountry);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
