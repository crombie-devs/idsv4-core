<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagCategory\TagCategoryCreateRequest;
use App\Http\Requests\TagCategory\TagCategoryUpdateRequest;
use App\Repositories\TagCategory\TagCategoryRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;

use App\Repositories\Log\LogRepository;

class TagCategoryController extends Controller
{
    private $tagCategoryRepository;
    use ImageUpload;

    public function __construct(
        TagCategoryRepositoryInterface $tagCategoryRepository
    ) {
        $this->tagCategoryRepository = $tagCategoryRepository;
    }
    /** 
     * Creación de una nueva categoría de tag
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden crear "tagcategory" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string
     * @bodyParam name string required
     * 
     * @group TagCategory management
     */
    public function create(TagCategoryCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];
        if($request->name) $data['name'] = $request->name;
        if($request->idcustomer) $data['idcustomer'] = $request->idcustomer;
        if($request->tagtype) $data['tagtype'] = $request->tagtype;

        if($request->image_url){
            $data['image_url'] = $this->uploadImage($request->image_url);
        }

        try{
            $response = $this->tagCategoryRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('TagCategory', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('TagCategory', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una categoría de tag
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden updatear "tagcategory" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string
     * @bodyParam name string
     * 
     * @group TagCategory management
     */
    public function update(int $idcategory, TagCategoryUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];
        if($request->name) $data['name'] = $request->name;
        if($request->tagtype) $data['tagtype'] = $request->tagtype;
        if($request->idcustomer) $data['idcustomer'] = $request->idcustomer;

        if($request->image_url){
            $data['image_url'] = $this->uploadImage($request->image_url);
            if(!$data['image_url']['success']){
                $data['image_url']=null;
            }
        }

        try{
            $response = $this->tagCategoryRepository->update($idcategory, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('TagCategory', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('TagCategory', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una categoría de tag de la base de datos
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden borrar "tagcategory" con su mismo idcustomer.
     * @authenticated
     *
     * @urlParam id required 
     * 
     * @group TagCategory management
     */
    public function delete(int $idcategory) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagCategoryRepository->delete($idcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('TagCategory', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('TagCategory', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de una categoría de tag
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idcustomer, solo pueden visualizar "tagcategory" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam id int required
     * @group TagCategory management
     */
    public function tagCategory(int $idcategory) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagCategoryRepository->tagCategory($idcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las categorías de tag de la base de datos
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden visualizar "tagcategory" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @group TagCategory management
     */
    public function tagCategories(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagCategoryRepository->tagCategories();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function uploadImage($image){
        try {
            return $this->TagCategoryImageUpload($image);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }

    /**
     * Acceso a todos los categorías de tag de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Los usuarios con idcustomer, solo pueden visualizar "tagcategory" con su mismo idcustomer.
     * 
     * 
     * @group TagCategory management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagCategoryRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    
}
