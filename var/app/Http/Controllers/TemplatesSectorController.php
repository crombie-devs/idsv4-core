<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemplatesSector\TemplatesSectorCreateRequest;
use App\Http\Requests\TemplatesSector\TemplatesSectorUpdateRequest;
use App\Repositories\TemplatesSector\TemplatesSectorRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class TemplatesSectorController extends Controller
{
    private $templatesSectorRepository;

    public function __construct(TemplatesSectorRepositoryInterface $templatesSectorRepository) {
        $this->templatesSectorRepository = $templatesSectorRepository;
    }
    /** 
     * Creación de una nueva TemplatesSector
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string 
     * @bodyParam idcustomer string required
     * @bodyParam icon string 
     * 
     * @group TemplatesSector management
     */
    public function create(TemplatesSectorCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->templatesSectorRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una TemplatesSector
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplatesector required
     * @bodyParam name string
     * @bodyParam idcustomer string
     * @bodyParam icon string
     * 
     * @group TemplatesSector management
     */
    public function update(int $idtemplateSector, TemplatesSectorUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesSectorRepository->update($idtemplateSector, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una TemplatesSector de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idtemplatesector required 
     * 
     * @group TemplatesSector management
     */
    public function delete(int $idtemplatesector) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesSectorRepository->delete($idtemplatesector);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una TemplatesSector
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplatesector int required
     * @group TemplatesSector management
     */
    public function templatesSector(int $idtemplatesector) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesSectorRepository->templatesSector($idtemplatesector);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las TemplatesSector de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group TemplatesSector management
     */
    public function templatesSectors(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templatesSectorRepository->templatesSectors();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a las TemplatesSector de customer de la base de datos
     * Esta acción la pueden realizar el admin y el superadmin
     * 
     * @authenticated
     * 
     * @group TemplatesSector management
     */
    public function templatesSectorsToSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templatesSectorRepository->templatesSectorsToSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
