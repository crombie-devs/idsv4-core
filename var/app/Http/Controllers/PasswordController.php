<?php

namespace App\Http\Controllers;

use App\Http\Requests\Password\PasswordRememberRequest;
use App\Http\Requests\Password\PasswordUpdateRequest;
use App\Repositories\Password\PasswordRepositoryInterface;

class PasswordController extends Controller
{
    protected $passwordRepository;

    public function __construct(PasswordRepositoryInterface $passwordRepository){
        $this->passwordRepository = $passwordRepository;
    }
    /**
    * Llamada para enviar el correo al usuario con la ruta para cambiar la contraseña.
    *
    * @bodyParam email email required
    *
    * @group Password reset
    */
    public function remember(PasswordRememberRequest $request){
        $response = $this->passwordRepository->remember($request);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Llamada para actualizar la contraseña del usuario.
     * 
     * @bodyParam token string  required Token de petición de reseteo de contraseña
     * @bodyParam password string required
     * @bodyParam c_password string required
     * @bodyParam email string required
     * 
     * 
     * @group Password reset
     */
    public function update(PasswordUpdateRequest $request){
        $response = $this->passwordRepository->update($request);
        return response()->json($response, $response['statusCode']);
    }
}
