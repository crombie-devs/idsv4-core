<?php

namespace App\Http\Controllers;

use App\Http\Requests\Country\CountryCreateRequest;
use App\Http\Requests\Country\CountryUpdateRequest;
use App\Repositories\Country\CountryRepositoryInterface;
use App\Exceptions\DatabaseErrorException;


class CountryController extends Controller
{
    protected $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;   
    }


    /** 
     * Creación de un nuevo country 
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * 
     * @group Country management
     */
    public function create(CountryCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        if($request->name) $data['name'] = $request->name;

        if(!isset($data['image_url']['success'])){
            try{
                $response = $this->countryRepository->create($request);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }catch(DatabaseErrorException $e){
                $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            }
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un country
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idcountry required 
     * @bodyParam name string
     * 
     * @group Country management
     */
    public function update(int $idcountry, CountryUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        
        $data = [];
        if($request->name) $data['name'] = $request->name;

        if(!isset($data['image_url']['success'])){
            try{
                $response = $this->countryRepository->update($idcountry, $data);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }catch(DatabaseErrorException $e){
                $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            }
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un country de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idcountry required 
     * 
     * @group Country management
     */
    public function delete(int $idcountry) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->countryRepository->delete($idcountry);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Aceso a los datos de un country
     * Esta acción sólo superadmin
     * 
     * @authenticated
     * 
     * @urlParam idcountry int required
     * @group Country management
     */
    public function country(int $idcountry) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->customerRepository->customer($idcountry);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los countries de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Country management
     */
    public function countries(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->countryRepository->countries();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a todos los countries de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group Country management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->countryRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
