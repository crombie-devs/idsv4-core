<?php

namespace App\Http\Controllers;


use App\Exceptions\DatabaseErrorException;
use App\Repositories\ContentType\ContentTypeRepositoryInterface;



class ContentTypeController extends Controller
{
    protected $cityRepository;

    public function __construct(ContentTypeRepositoryInterface $contentTypeRepository)
    {
        $this->contentTypeRepository = $contentTypeRepository;   
    }


    /**
     * Acceso a todos las content type de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group ContentType management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->contentTypeRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
