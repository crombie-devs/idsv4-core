<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

use App\Models\TicketImage;

use App\Http\Requests\Ticket\TicketCreateRequest;
use App\Http\Requests\Ticket\TicketUpdateRequest;
use App\Http\Requests\Ticket\TicketCreateCommentRequest;
use App\Http\Requests\Ticket\TicketUpdateCommentRequest;
use App\Http\Requests\Ticket\TicketMiscUpdateRequest;
use App\Http\Requests\Ticket\TicketMiscCreateRequest;
use App\Http\Requests\Ticket\TicketDeleteRequest;
use App\Http\Requests\Ticket\TicketSupportRequest;
use App\Http\Requests\Ticket\TicketArrayRequest;
use App\Exceptions\DatabaseErrorException;
use App\Repositories\Ticket\TicketRepository;
use App\Mail\AlertSupport;
use App\Mail\AlertSupportClose;
use App\Mail\AlertOpenTicket;
use App\Mail\AlertSupportResponsible;



class TicketController extends Controller
{
    protected $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function list()
    {
        $response = [
            'success' => false,
            'data' => 'Error desconocido',
            'statusCode' => 503
        ];

        try {
            $response = $this->ticketRepository->list();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getAll(Request $request)
    {
        $orderBy    = filter_var($request->orderBy, FILTER_SANITIZE_STRING);
        $sortMethod = filter_var($request->sortMethod, FILTER_SANITIZE_STRING);
        $column     = filter_var($request->column, FILTER_SANITIZE_STRING);
        $condition  = filter_var($request->condition, FILTER_SANITIZE_STRING);

        $orderBy = ($orderBy) ? $orderBy : 'id';
        $sortMethod = ($sortMethod) ? $sortMethod : 'desc';

        if ($column && $condition) {
            $response = $this->ticketRepository->getBy($orderBy, $column, $condition, $sortMethod);
        } else {
            $response = $this->ticketRepository->getBy($orderBy, false, false, $sortMethod);
        }
        return response()->json($response);
    }

    public function groupBy(Request $request)
    {
        $groupBy = filter_var($request->column, FILTER_SANITIZE_STRING);
        $response = $this->ticketRepository->groupBy($groupBy);
        return response()->json($response);
    }

    public function create(TicketCreateRequest $request)
    {
        $data = [];

        $data['phone'] = $request->phone;
        $data['title'] = $request->title;
        if($request->email)
            $data['email'] = $request->email;
        $data['point_address'] = $request->point_address;
        $data['description'] = $request->description;
        $data['id_priority'] = $request->id_priority;
        $data['id_category'] = $request->id_category;
        $data['id_department'] = $request->id_department;
        $data['id_responsible'] = $request->id_responsible;
        $data['id_site'] = $request->id_site;
        $data['id_status'] = $request->id_status;
        $data['id_customer'] = $request->id_customer;
        $data['id_user'] = ($request->id_user) ? $request->id_user : 1;
        $data['finished'] = $request->finished;
        $data['deleted'] = 0;
        $data['accept'] = 0;
        $data['expected_resolution_date'] = $request->expected_resolution_date;
        $data['notify_user'] = ($request->notify_user) ? 1 : 0;
        $data['comments'] = $request->comments;

        $created_at = Carbon::parse($request->created_at);
        $updated_at = Carbon::parse($request->updated_at);
        $data['created_at'] = $created_at;
        $data['updated_at'] = $updated_at;
        if($request->date_alert != 'null'){
            $date_alert = Carbon::parse($request->date_alert);
            $data['date_alert'] = $date_alert;
        }

        $files = $request->files;

        try {
            $ticket = $this->ticketRepository->create($data, $files);
            $response = ['success' => true, 'data' => $ticket, 'statusCode' => 200];
            $this->prepareEmail($response['data']);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function update(TicketUpdateRequest $request)
    {
        $data = [];

        $data['id'] = $request->id;
        $data['phone'] = $request->phone;
        $data['title'] = $request->title;
        if($request->email)
            $data['email'] = $request->email;
        $data['point_address'] = $request->point_address;
        $data['description'] = $request->description;
        $data['id_priority'] = $request->id_priority;
        $data['id_category'] = $request->id_category;
        $data['id_department'] = $request->id_department;
        $data['id_responsible'] = $request->id_responsible;
        $data['id_site'] = $request->id_site;
        $data['id_status'] = $request->id_status;
        $data['id_customer'] = $request->id_customer;
        $data['id_user'] = ($request->id_user) ? $request->id_user : 1;
        $data['finished'] = $request->finished;
        $data['deleted'] = 0;
        $data['accept'] = 0;
        $data['expected_resolution_date'] = $request->expected_resolution_date;
        $data['notify_user'] = ($request->notify_user) ? 1 : 0;

        $created_at = Carbon::parse($request->created_at);
        $updated_at = Carbon::parse($request->updated_at);
        $date_alert = Carbon::parse($request->date_alert);

        $data['created_at'] = $created_at;
        $data['updated_at'] = $updated_at;
        $data['date_alert'] = $date_alert;

        $files = ($request->hasfile('files')) ? $request->files : null;
        $comments = ($request->comments) ? $request->comments : null;
        
        try {
            $response = $this->ticketRepository->update($data, $files, $comments);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['error' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        // envio de mail.
        if ($response['data'][0]['responsible']['email']) {
            
            $responsible = $response['data'][0]['responsible']['email'];

            if ($request->id_status == 5 && $response['data'][0]['email']) {
                $this->sendEmailClose($response['data'][0]);
            } else {
                $this->sendEmailResponsible($response['data'][0], $responsible);
            }
        }
        
        return response()->json($response, $response['statusCode']);
    }

    public function support(TicketSupportRequest $request)
    {
        $data = [];
        $data['titulo'] = $request->titulo;
        $data['descripcion'] = $request->descripcion;
        $files = $request->files;
        $data['phone'] = $request->phone;
        $data['point_address'] = $request->point_address;
        $data['email'] = $request->email;
        $data['id_status'] = 1;
        $data['finalizado'] = 1;
        $email['email'] = $request->email ? $request->email : null;
        $data['accept'] = $request->accept ? 1 : 0;
        try {
            $response = $this->ticketRepository->support($data, $files);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        if($email['email']){
            $email['id_ticket'] = $response['data']['id_ticket'];
            $email['created_at'] = $response['data']['created_at'];
    
            $this->sendOpenTicket($email);
        }

        return response()->json($response, $response['statusCode']);
    }

    public function deleteImages($id)
    {
        try {
            TicketImage::where('id_ticket', $id)->delete();
            $response = ['success' => true, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function close($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->close($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        $this->sendEmailClose($response['data']);

        return response()->json($response, $response['statusCode']);
    }

    public function byCustomer($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->byCustomer($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function deleteComment($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->deleteComment($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getComments($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->getComments($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    private function sendOpenTicket($sendMail)
    {
        if ($sendMail) {
            $env = config('app.env');
            if($sendMail['email'])
                Mail::send(new AlertOpenTicket($sendMail, $env, $sendMail));
        }
    }


    private function sendEmailResponsible($sendMail, $responsible)
    {
        if ($sendMail) {
            Mail::send(new AlertSupportResponsible($responsible, '', $sendMail));
        }
    }

    private function sendEmail($sendMail, $email, $responsible)
    {

        if ($sendMail) {
            if (isset($sendMail['user'][0])) {
                $subject = $sendMail['user'][0]['name'];
            } else {
                $subject = $sendMail['user']['name'];
            }

            $arr = [$responsible, $email];
            foreach ($arr as $key => $value) {

                Mail::send(new AlertSupport($subject, '', $sendMail, $value));
            }
        }
    }

    private function sendEmailClose($data)
    {
        if ($data) {
            $env = config('app.env');
            Mail::send(new AlertSupportClose($data['email'], $env, $data));
        }
    }

    public function createComment(TicketCreateCommentRequest $request)
    {
        $data = [];

        $data['mensaje'] = $request->descripcion;
        $data['id_ticket'] = $request->id_ticket;
        $files = $request->files;
        $data['id_status'] = $request->id_status;
        $data['notificar_usuario'] = $request->notificar_usuario;
        $data['id_created_by'] = $request->id_created_by;
        $data['deleted'] = 1;
        $data['idresponsible'] = $request->idresponsible;


        try {
            $response = $this->ticketRepository->createComment($data, $files);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function updateComment(TicketUpdateCommentRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['descripcion'] = $request->descripcion;
        $data['id'] = $request->id;
        $data['idresponsible'] = $request->idresponsible;

        try {
            $response = $this->ticketRepository->updateComment($data);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getTicket($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->get($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function delete(TicketArrayRequest $ids) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->delete($ids);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function byType($type)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->byType($type);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function deleteStatus(TicketDeleteRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->deleteStatus($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function updateStatus(TicketMiscUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['color'] = $request->color;
        try {
            $response = $this->ticketRepository->updateStatus($data);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function createStatus(TicketMiscCreateRequest $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['color'] = $request->color;
        try {
            $response = $this->ticketRepository->createStatus($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function status()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->status();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function deletePriority(TicketDeleteRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->deletePriority($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function updatePriority(TicketMiscUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['color'] = $request->color;
        $data['hours_max_resolution'] = $request->hours_max_resolution;
        try {
            $response = $this->ticketRepository->updatePriority($data);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function createPriority(TicketMiscCreateRequest $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['color'] = $request->color;
        $data['hours_max_resolution'] = $request->hours_max_resolution;
        try {
            $response = $this->ticketRepository->createPriority($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function priority()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->priority();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function deleteCategory(TicketDeleteRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ticketRepository->deleteCategory($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function updateCategory(TicketMiscUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['color'] = $request->color;

        try {
            $response = $this->ticketRepository->updateCategory($data);

            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function createCategory(TicketMiscCreateRequest $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['color'] = $request->color;
        try {
            $response = $this->ticketRepository->createCategory($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function category()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ticketRepository->category();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function prepareEmail($response)
    {
        $responsible =  $response['responsible']['email'];
        $this->sendEmailResponsible($response,  $responsible);
        $this->sendOpenTicket($response);
    }
}
