<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\UserLoginRequest;

use App\Repositories\Support\SupportRepository;
use App\Exceptions\DatabaseErrorException;
use App\Exceptions\NotFoundException;
use Illuminate\Auth\AuthenticationException;


class SupportController extends Controller {

    public $userRepository; 
    
    public function __construct(SupportRepository $userRepository) {
        $this->userRepository = $userRepository;
    }
    


    public function authenticated() {
        
        return response()->json(['succees' => true, 'statusCode' => 200 ]);
   }
   
    public function login(UserLoginRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->login($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(AuthenticationException $e){
            $response = ['success' => false, 'data' => $e->getMessage(), 'statusCode' => 200];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * logout de la plataforma
     * 
     * @authenticated
     *  
     * @group User access
     */
    public function logout(Request $request) {
        $response = ['success' => false, 'data' => ["message" => "Error desconocido"], 'statusCode' => 503];
        try{
            $response = $this->userRepository->logout($request);
            $response = ['success' => true, 'data' => null, 'statusCode' => 200];
        }catch(AuthenticationException $e){
            $response = ['success' => false, 'data' => $e->getMessage(), 'statusCode' => 200];
        }
        return response()->json($response, $response['statusCode']);
    }
   
}