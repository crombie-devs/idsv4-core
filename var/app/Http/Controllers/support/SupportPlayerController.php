<?php

namespace App\Http\Controllers\support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Player\PlayerFindByEmailRequest;
use App\Http\Requests\Player\PlayerGetPlaylistRequest;
use App\Repositories\Player\SupportPlayerRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Player\PlayerAuthRequest;
use Carbon\Carbon;
use App\Http\Requests\Player\PlayerPlayLogicRequest;
use App\Http\Requests\Player\PlayerContentRequest;

class SupportPlayerController extends Controller
{
    private $SupportPlayerRepository;

    public function __construct(SupportPlayerRepositoryInterface $SupportPlayerRepository) {
        $this->SupportPlayerRepository = $SupportPlayerRepository;
    }

    /**
     * Acceso a los player 
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function getPlayer(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->SupportPlayerRepository->getPlayer();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
           
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

  /**
     * Acceso a los player por centro
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function getPlayerBySite($idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idsite) && $idsite != ''){
                $response = $this->SupportPlayerRepository->getPlayerBySite($idsite);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El centro es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a los player por cliente
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Camera
     */
    public function getPlayerByCustomer($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idcustomer) && $idcustomer != ''){
                $response = $this->SupportPlayerRepository->getPlayerByCustomer($idcustomer);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El cliente es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * Acceso al total de players agrupados por customer
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function totalPlayerGroupByCustomer(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->SupportPlayerRepository->totalPlayerGroupByCustomer();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * Acceso al total de players agrupados por site
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function totalPlayerGroupBySite($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idcustomer) && $idcustomer != ''){
                $response = $this->SupportPlayerRepository->totalPlayerGroupBySite($idcustomer);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El cliente es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function getTokenGenerate(){
        $ts =  Carbon::now()->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);

        return "?timestamp=".$current_timestamp."&keyhash=".$keyhash;
    }
}
