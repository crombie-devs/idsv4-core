<?php

namespace App\Http\Controllers\support;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\Camera\SupportCameraRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Camera\CameraAuthRequest;
use Carbon\Carbon;

class SupportCameraController extends Controller
{
    private $SupportCameraRepository;

    public function __construct(SupportCameraRepositoryInterface $SupportCameraRepository) {
        $this->SupportCameraRepository = $SupportCameraRepository;
    }

    /**
     * Acceso a las camaras 
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function getCamera(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->SupportCameraRepository->getCamera();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
           
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a las cameras por centro
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Camera
     */
    public function getCameraBySite($idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idsite) && $idsite != ''){
                $response = $this->SupportCameraRepository->getCameraBySite($idsite);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El centro es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a las cameras por cliente
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Camera
     */
    public function getCameraByCustomer($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idcustomer) && $idcustomer != ''){
                $response = $this->SupportCameraRepository->getCameraByCustomer($idcustomer);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El cliente es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * Acceso al total de cameras agrupados por customer
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Camera
     */
    public function totalCameraGroupByCustomer(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->SupportCameraRepository->totalCameraGroupByCustomer();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * Acceso al total de cameras agrupados por site
     * Esta acción se realiza desde support
     * 
     * @acceso por token
     * 
     * @group WebService Camera
     */
    public function totalCameraGroupBySite($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(isset($idcustomer) && $idcustomer != ''){
                $response = $this->SupportCameraRepository->totalCameraGroupBySite($idcustomer);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            }
            else
                $response = ['success' => false, 'data' => 'El cliente es necesario', 'statusCode' => 503];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function getTokenGenerate(){
        $ts =  Carbon::now()->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);

        return "?timestamp=".$current_timestamp."&keyhash=".$keyhash;
    }
}
