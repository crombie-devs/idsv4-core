<?php

namespace App\Http\Controllers;

use App\Http\Requests\AudioCategory\AudioCategoryCreateRequest;
use App\Http\Requests\AudioCategory\AudioCategoryUpdateRequest;
use App\Exceptions\DatabaseErrorException;
use App\Repositories\AudioCategory\AudioCategoryRepository;
use App\Traits\ImageUpload;
use App\Traits\UploadGoogleBucket;
use App\Repositories\Log\LogRepository;

class AudioCategoryController extends Controller
{
    protected $audioCategoryRepository;
    use ImageUpload;
    use UploadGoogleBucket;

    public function __construct(
        AudioCategoryRepository $audioCategoryRepository
    ) {
        $this->audioCategoryRepository = $audioCategoryRepository;
    }


    public function uploadImage($file)
    {
        try {
            return $this->ImageBucketUpload($file);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda el video', 'statusCode' => 503];
        }
    }





    public function create(AudioCategoryCreateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];


        if ($request->image_url) {
            $request->image_url = $this->uploadImage($request->image_url);
        }

        $data = [];
        $data['image_url'] = $request->image_url;
        $data['name'] = $request->name;
        $data['status'] = $request->status;

        try {
            $response = $this->audioCategoryRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('AudioCategory', 'Create', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('AudioCategory', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }



    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioCategoryRepository->audioCategoriesToSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function list()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioCategoryRepository->audioCategories();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }





    public function audioCategory($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioCategoryRepository->audioCategory($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function delete($id)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->audioCategoryRepository->deleteAudioCategory($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('AudioCategory', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('AudioCategory', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }


    public function update(AudioCategoryUpdateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];

        if ($request->image_url) {
            if ($request->hasFile('image_url')) {
                $data['image_url']   = $this->uploadImage($request->image_url);

                if (empty($request->image_url)) {
                    $data['image_url'] = null;
                }
            } else {
                $data['image_url'] = $request->image_url;
            }
        }

        $data['idcustomer'] = $request->idcustomer;
        $data['idcategory'] = $request->idcategory;
        $data['name'] = $request->name;
        $data['status'] = $request->status;
        $data['idcategory'] = $request->idcategory;



        try {
            $response = $this->audioCategoryRepository->updateAudioCategory($data, $request->idcategory);

            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
            LogRepository::logger('AudioCategory', 'Update', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('AudioCategory', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
}
