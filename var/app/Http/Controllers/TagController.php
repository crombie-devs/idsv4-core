<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag\TagCreateRequest;
use App\Http\Requests\Tag\TagUpdateRequest;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

use App\Repositories\Log\LogRepository;

class TagController extends Controller
{
    private $tagRepository;

    public function __construct(
        TagRepositoryInterface $tagRepository
    ) {
        $this->tagRepository = $tagRepository;
    }
    /** 
     * Creación de un nuevo tag
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden crear tag con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcategory string
     * @bodyParam name string required
     * 
     * @group Tag management
     */
    public function create(TagCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Tag', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Tag', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un tag
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden updatear tag con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idtag string
     * @bodyParam name string
     * 
     * @group Tag management
     */
    public function update(int $idtag, TagUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->update($idtag, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Tag', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Tag', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un tag de la base de datos
     * Esta acción sólo la puede realizar el superadmin y admin
     * Los usuarios con idcustomer, solo pueden borrar tag con su mismo idcustomer.
     * 
     * @authenticated
     *
     * @urlParam idtag required 
     * 
     * @group Tag management
     */
    public function delete(int $idtag) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->delete($idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Tag', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Tag', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un tag
     * Esta acción sólo la puede realizar el superadmin, el admin y user
     * Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam idtag int required
     * @group Tag management
     */
    public function tag(int $idtag) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->tag($idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los tags de la base de datos
     * Esta acción sólo la puede realizar el superadmin, el admin
     * Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @group Tag management
     */
    public function tags(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->tags();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }



    /**
     * Acceso a todos los tags de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Los usuarios con idcustomer, solo pueden visualizar tag con su mismo idcustomer.
     * 
     * 
     * @group Tag management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function byType(string $type){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->tagRepository->byType($type);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

}
