<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContentCategory\ContentCategoryCreateRequest;
use App\Http\Requests\ContentCategory\ContentCategoryUpdateRequest;
use App\Repositories\ContentCategory\ContentCategoryRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;

use App\Repositories\Log\LogRepository;

class ContentCategoryController extends Controller
{
    protected $contentcategoryRepository;
    use ImageUpload;

    public function __construct(
        ContentCategoryRepositoryInterface $contentcategoryRepository
    ) {
        $this->contentcategoryRepository = $contentcategoryRepository;
    }

    /** 
     * Creación de un nuevo contentcategory 
     * Esta acción sólo la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer
     * @bodyParam name string
     * @bodyParam image_url File
     * 
     * @group ContentCategory management
     */
    public function create(ContentCategoryCreateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];

        if ($request->name) $data['name'] = $request->name;
        if ($request->idcustomer) $data['idcustomer'] = $request->idcustomer;
        if ($request->duration) $data['duration'] = $request->duration;
        if ($request->random) $data['random'] = $request->random ? 1 : 0;


        if ($request->image_url) {
            $data['image_url'] = $this->uploadImage($request->image_url);
        }

        try {
            $response = $this->contentcategoryRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('ContentCategory', 'Create', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('ContentCategory', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un contentcategory
     * Esta acción sólo la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @urlParam idcontentcategory required 
     * @bodyParam idcustomer integer
     * @bodyParam name string
     * @bodyParam image_url File
     * 
     * @group ContentCategory management
     */
    public function update(int $idcontentcategory, ContentCategoryUpdateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];

        if ($request->name) $data['name'] = $request->name;
        if ($request->idcustomer) $data['idcustomer'] = $request->idcustomer;
        if ($request->duration) $data['duration'] =  $request->duration;

        if ($request->image_url) {
            if ($request->hasFile('image_url')) {
                $data['image_url'] = $this->uploadImage($request->image_url);
                if (empty($data['image_url'])) {
                    $data['image_url'] = null;
                }
            }
        }

        try {
            $response = $this->contentcategoryRepository->update($idcontentcategory, $data);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
            LogRepository::logger('ContentCategory', 'Update', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('ContentCategory', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un contentcategory de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idcontentcategory required 
     * 
     * @group ContentCategory management
     */
    public function delete(int $idcontentcategory)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->contentcategoryRepository->delete($idcontentcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('ContentCategory', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('ContentCategory', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Aceso a los datos de un contentcategory
     * Esta acción sólo superadmin
     * 
     * @authenticated
     * 
     * @urlParam idcontentcategory int required
     * @group ContentCategory management
     */
    public function contentcategory(int $idcontentcategory)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->contentcategoryRepository->contentcategory($idcontentcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos las categorias de contenidos de la base de datos
     * Esta acción sólo la puede realizar el user
     * 
     * @authenticated
     * 
     * @group ContentCategory management
     */
    public function contentCategories()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->contentcategoryRepository->contentCategories();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contentcategories de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group ContentCategory management
     */
    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->contentcategoryRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function uploadImage($image)
    {
        try {
            return $this->ContentCategoryImageUpload($image);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }
}
