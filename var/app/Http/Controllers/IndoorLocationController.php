<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndoorLocation\IndoorLocationCreateRequest;
use App\Http\Requests\IndoorLocation\IndoorLocationUpdateRequest;
use App\Repositories\IndoorLocation\IndoorLocationRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class IndoorLocationController extends Controller
{
    private $indoorLocationRepository;

    public function __construct(IndoorLocationRepositoryInterface $indoorLocationRepository) {
        $this->indoorLocationRepository = $indoorLocationRepository;
    }
    /** 
     * Creación de un nuevo indoor location
     * Esta acción sólo la puede realizar el superadmin, admin
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string
     * @bodyParam name string required
     * 
     * @group IndoorLocation management
     */
    public function create(IndoorLocationCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un indoor location
     * Esta acción sólo la puede realizar el superadmin,admin
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string
     * @bodyParam name string
     * 
     * @group IndoorLocation management
     */
    public function update(int $idlocation, IndoorLocationUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->update($idlocation, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un indoor location de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin
     * 
     * @authenticated
     *
     * @urlParam idlocation required 
     * 
     * @group IndoorLocation management
     */
    public function delete(int $idlocation) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->delete($idlocation);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un indoor location
     * Esta acción sólo la puede realizar el superadmin, el admin de los indoor location que pertenecen a su customer o no pertenecen a ninún customer y el user a los que pertenecen a su site o no pertenecen a ningún customer
     * 
     * @authenticated
     * 
     * @urlParam idlocation int required
     * @group IndoorLocation management
     */
    public function indoorlocation(int $idlocation) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->indoorlocation($idlocation);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los indoor location de la base de datos
     * Esta acción sólo la puede realizar el superadmin, el admin verá el listado de indoor location pertenecientes a su customer
     * 
     * @authenticated
     * 
     * @group IndoorLocation management
     */
    public function indoorlocations(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->indoorlocations();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contentcategories de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group IndoorLocation management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Debe eliminar:registro en indoor_locations (idlocation) play_areas (idlocation) contents_has_areas (idarea) contents_order (idarea) play_logics (idarea) players (idarea)
     * Esta acción la puede realizar cualquier usuario
     * Devuelve indoorlocation 
     * 
     * @urlParam idlocation required int
     * 
     * @authenticated
     * 
     * @group IndoorLocation management
     */
    public function deleteIndoorLocationForce(int $idlocation){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->indoorLocationRepository->deleteIndoorLocationForce($idlocation);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

}
