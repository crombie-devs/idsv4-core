<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\UserRegisterRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserPermissionRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Exceptions\NotFoundException;

use App\Repositories\Log\LogRepository;

class UserController extends Controller {

    protected $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }
    /** 
     * Registro de un nuevo usuario 
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam email email required
     * @bodyParam password string required
     * @bodyParam role string required
     * @bodyParam idcustomer int 
     * @bodyParam idsite int
     * 
     * @group User management
     */
    public function register(UserRegisterRequest $request) {       
        $input = $request->all();

        $role = $input['role'];
        $data['name'] = $input['name'];
        $data['email'] = $input['email'];

        if(isset($input['idcustomer'])) $data['idcustomer'] = $input['idcustomer'];
        if(isset($input['idsite'])) $data['idsite'] = $input['idsite'];
        $data['password'] = bcrypt($input['password']);
        $data['idcategories'] = $input['idcategories'];
        $data['idcircuits'] = $input['idcircuits'];

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->userRepository->register($data, $role);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('User', 'Register', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('User', 'Register', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un usuario ya registrado
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam iduser required
     * @bodyParam name string 
     * @bodyParam email email 
     * @bodyParam role string 
     * @bodyParam idcustomer int 
     * @bodyParam idsite int
     * @bodyParam status boolean
     * 
     * @group User management
     */
    public function update(int $iduser, UserUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->update($iduser, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('User', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('User', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un usuario de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam iduser required 
     * 
     * @group User management
     */
    public function delete(int $iduser) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->delete($iduser);
            $response = ['success' => true, 'data' => null, 'statusCode' => 200];
            LogRepository::logger('User', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('User', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Acceso de usuario a la web
     * 
     * @bodyParam name string required
     * @bodyParam password string required
     * 
     * @group User access
     */
    public function login(UserLoginRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->userRepository->login($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];

            LogRepository::logger('User', 'Login', true);

        } catch(AuthenticationException $e) {
            $response = ['success' => false, 'data' => $e->getMessage(), 'statusCode' => 200];
            // LogRepository::logger('User', 'Login', false);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * logout de la plataforma
     * 
     * @authenticated
     *  
     * @group User access
     */
    public function logout(Request $request) {
        $response = ['success' => false, 'data' => ["message" => "Error desconocido"], 'statusCode' => 503];
        try {
            $response = $this->userRepository->logout($request);
            $response = ['success' => true, 'data' => null, 'statusCode' => 200];

            LogRepository::logger('User', 'Logout', true);

        } catch(AuthenticationException $e) {
            $response = ['success' => false, 'data' => $e->getMessage(), 'statusCode' => 200];
            LogRepository::logger('User', 'Logout', false);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos del usuario me
     * 
     * @authenticated
     * 
     * @group User access
     */
    public function me() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->me();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de un usuario que no es me
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam iduser int required
     * @group User management
     */
    public function user(int $iduser) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->user($iduser);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(NotFoundException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los usuarios de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group User management
     */
    public function users(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->users();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(NotFoundException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Consulta que agrega un permiso a un usuario
     * Esta acción solo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group User management
     */
    public function addPermission(UserPermissionRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->addPermission($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Consulta que quita un permiso a un usuario
     * Esta acción solo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group User management
     */
    public function revokePermission(UserPermissionRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->userRepository->revokePermission($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

}