<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemplatesCustomer\TemplatesCustomerCreateRequest;
use App\Http\Requests\TemplatesCustomer\TemplatesCustomerUpdateRequest;
use App\Repositories\TemplatesCustomer\TemplatesCustomerRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class TemplatesCustomerController extends Controller
{
    private $templatesCustomerRepository;

    public function __construct(TemplatesCustomerRepositoryInterface $templatesCustomerRepository) {
        $this->templatesCustomerRepository = $templatesCustomerRepository;
    }
    /** 
     * Creación de una nueva TemplatesCustomer
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer_master string 
     * @bodyParam idcustomer string required
     * 
     * @group TemplatesCustomer management
     */
    public function create(TemplatesCustomerCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->templatesCustomerRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una TemplatesCustomer
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplatecustomer required
     * @bodyParam idcustomer_master string
     * @bodyParam idcustomer string required
     * 
     * @group TemplatesCustomer management
     */
    public function update(int $idtemplateCustomer, TemplatesCustomerUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesCustomerRepository->update($idtemplateCustomer, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una TemplatesCustomer de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idtemplatecustomer required 
     * 
     * @group TemplatesCustomer management
     */
    public function delete(int $idtemplatesector) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesCustomerRepository->delete($idtemplatecustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una TemplatesCustomer
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplatecustomer int required
     * @group TemplatesCustomer management
     */
    public function app(int $idtemplateCustomer) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesCustomerRepository->templatesCustomer($idtemplatecustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las TemplatesCustomer de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group TemplatesCustomer management
     */
    public function templatesCustomers(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templatesCustomerRepository->templatesCustomers();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a las TemplatesCustomer de customer de la base de datos
     * Esta acción la pueden realizar el admin y el superadmin
     * 
     * @authenticated
     * 
     * @group TemplatesCustomer management
     */
    public function templatesCustomersToSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templatesCustomerRepository->templatesCustomersToSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function createOrUpdate(int $idtemplateCustomer, TemplatesCustomerUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templatesCustomerRepository->createOrUpdate($idtemplateCustomer, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getTemplatesCustomerByIdcustomerMaster(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templatesCustomerRepository->getTemplatesCustomerByIdcustomerMaster();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
