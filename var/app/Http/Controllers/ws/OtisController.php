<?php 
namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Repositories\Otis\OtisRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Otis\OtisAuthRequest;
use Carbon\Carbon;
use File;

use App\Traits\CountryTrait;
use App\Traits\ProvinceTrait;
use App\Traits\CityTrait;

use App\Models\Country;
use App\Models\Province;
use App\Models\City;
use App\Models\Customer;
use App\Models\Site;
use App\Models\Player;

use Exception;

class OtisController extends Controller
{

    use CountryTrait;
    use ProvinceTrait;
    use CityTrait;

    private $otisRepository;

    public function __construct(OtisRepositoryInterface $otisRepository) {
        $this->otisRepository = $otisRepository;
    }
    
     /**
     * WS Auth for Player Otis
     * Debe devolver el **detalle del player de otis** que cumpla:
     * player.email = $request.email
     * player.status = 1
     * player.deleted = 0
     * 
     * @acceso por token
     * @urlParam email string required
     * 
     * @group Otis
     */
    public function auth(OtisAuthRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $email = $this->generateEmail($request['mpd_id']);
        try{
            $response = $this->otisRepository->auth($email);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }catch(NotFoundException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    private function generateEmail($id,$simb='@',$ext='.com'){
        $email = $id . $simb .'otis'.$ext;

        return $email;
    }

    private function getFile(Request $request, $param_name) {
        if(!$request->file($param_name)) {
            throw new Exception('Debes aportar el fichero CSV');
        } 
        
        try {
            $path = storage_path('tmp/uploads/');
            $filename = time().'_'.$request->file($param_name)->getClientOriginalName();
            $request->file($param_name)->move($path, $filename);
        }
        catch(Exception $ex) {
            throw new Exception('No ha sido posible guardar el fichero');
        }

        $filepath = $path . $filename;
        $file = fopen($filepath, "r");
        if($file === FALSE){
            throw new Exception('Debes aportar el fichero CSV');
        }

        return $file;
    }

    private function getZipcode($value) {
        $zipcode = substr(str_replace(' ', '', str_pad($value, 5, "0", STR_PAD_LEFT)), 0, 5);
        return $zipcode;
    }

    private function getCountry($country_value) {
        $country_name = $this->countryNameNormalize($country_value);
        $country = Country::findByName($country_name);
        if(!$country)
            throw new Exception("Pais desconocido (". $country_value .")");

        return $country;
    }

    private function getProvince($province_value, $country_name) {
        $province_name = $this->provinceNameNormalize($province_value);
        $province = City::findProvinceByNames($province_name, $country_name);
        if(!$province)
            throw new Exception("Provincia desconocida (". $province_value ." - ". $country_name .")");
        
        return $province;
    }

    private function getCity($zipcode_value, $city_value, $province_name, $country_name) {
        $city_name = $this->cityNameNormalize($city_value);
        $city = City::findByNames($city_value, $province_name, $country_name);
        if(!$city) {
            $names = [];
            $zipcode = $zipcode_value;
            $city_names = \DB::table("zipcodes")->select(\DB::raw("city_name as name"))->where("zipcode", $zipcode)->get();
            foreach($city_names as $c_name) {
                $names []= $c_name->name;
                if(!$city)
                    $city = City::findByNames($c_name->name, $province_name, $country_name);
            }       
            $names_str = implode(", ", $names);
            if(!$city)
                throw new Exception("Ciudad desconocidad ($zipcode_value - $city_value - $province_name - $country_name) - ($zipcode - $names_str)");
        }

        return $city;
    }

    public function checkDevices(Request $request) {
        try {
            $file = $this->getFile($request, 'devices');
        }
        catch(Exception $ex) {
            $response = array('success' => false, 'data' => $ex->getMessage());
            return response($response, 401)->header('Content-Type', 'application/json');
        }

        $line_number = 1;

        $success = 0;
        $errors = [];
        $header = fgetcsv($file, 0, ";");
        while(($line = fgetcsv($file, 0, ";")) !== FALSE) {
            try {
                if(count($line) < 4 )
                    throw new Exception("Formato CSV incorrecto");

                $zipcode = $this->getZipcode($line[2]);
                $country = $this->getCountry($line[5]);
                $province = $this->getProvince($line[4], $country->name);
                $city = $this->getCity($zipcode, $line[3], $province->province_name, $country->name);  
                
                $viewer = Player::findByEmail($line[0]."@otis.com");
                if(strcasecmp($line[6], 'b') == 0) {
                    // Eliminar
                    if(empty($viewer))
                        throw new Exception("Borrar: Player desconocido (". $line[0] .")");
                }
                else if(strcasecmp($line[6], 'm') == 0) {
                    // Actualizar
                    if(empty($viewer))
                        throw new Exception("Modificar: Player desconocido (". $line[0] .")");
                }
                else {
                    // Insertar
                    if(!empty($viewer))
                        throw new Exception("Insertar: Player ya existe (". $line[0] .")");
                }

                $success++;
            } 
            catch(Exception $ex) {
                $errors[] = ["line" => $line_number, "data" => $ex->getMessage()];
            }
            $line_number++;
        }

        $is_success = empty($errors);
        $response = array('success' => $is_success, 'data' => "Correctos: $success | Erroneos: ". count($errors), 'errors' => $errors);
        return response($response, 200)->header('Content-Type', 'application/json'); 
    }

    public function updateDevices(Request $request) {
        try {
            $file = $this->getFile($request, 'devices');
        }
        catch(Exception $ex) {
            $response = array('success' => false, 'data' => $ex->getMessage());
            return response($response, 401)->header('Content-Type', 'application/json');
        }

        $line_number = 1;
        $insert_number = 0;
        $update_number = 0;
        $delete_number = 0;

        $errors = [];
        $header = fgetcsv($file, 0, ";");
        while(($line = fgetcsv($file, 0, ";")) !== FALSE) {
            try {
                if(count($line) < 7 )
                    throw new Exception("Formato CSV incorrecto");

                $idcustomer = 55;
                $zipcode = $this->getZipcode($line[2]);
                $country = $this->getCountry($line[5]);
                $province = $this->getProvince($line[4], $country->name);
                $city = $this->getCity($zipcode, $line[3], $province->province_name, $country->name);
                
                $site = Site::findByAddress($line[1], $zipcode, $city->idcity, $city->idprovince, $idcustomer);
                if(!$site) {
                    $time = Carbon::now()->toDateTimeString();
                    $idsite = \DB::table('sites')->insertGetId([
                        "idcustomer" => $idcustomer,
                        "name" => $line[1],
                        "address" => $line[1],
                        "zipcode" => $zipcode,
                        "idcity" => $city->idcity,
                        "latitude" => 0,
                        "longitude" => 0,
                        "status" => 1,
                        "deleted" => 0
                    ]);
                    if(!$idsite)
                        throw new Exception("Centro no ha sido creado (". $line[0] .")");

                    $site = Site::findById($idsite);
                    if(!$site)
                        throw new Exception("Centro desconocido (". $line[0] ." - ". $idsite .")");
                }

                if(strcasecmp($line[6], 'b') == 0) {
                    // Dar de baja
                    $viewer = Player::findByEmail($line[0]."@otis.com");
                    if(!$viewer)
                        throw new Exception("Player desconocido (". $line[0] .")");

                    $time = Carbon::now()->toDateTimeString();
                    $viewer = \DB::table('players')->where('idplayer', $viewer->idplayer)->update([
                        "status" => 0,
                        "deleted" => 1
                    ]);
                    $delete_number++;
                }
                else if(strcasecmp($line[6], 'm') == 0) {
                    // Actualizar
                    $viewer = Player::findByEmail($line[0]."@otis.com");
                    if(!$viewer)
                        throw new Exception("Player desconocido (". $line[0] .")");

                    $time = Carbon::now()->toDateTimeString();
                    $usite = \DB::table('sites')->where('idsite', $site->idsite)->update([
                        "status" => 1,
                        "deleted" => 0
                    ]);
                    $viewer = \DB::table('players')->where('idplayer', $viewer->idplayer)->update([
                        "code" => $line[0],
                        "status" => 1,
                        "deleted" => 0
                    ]);

                    $update_number++;
                }
                else {
                    $viewer = Player::findByEmail($line[0]."@otis.com");
                    if($viewer && $viewer->deleted == 0) {
                        throw new Exception("Player ya existe (". $line[0] .")");
                    }
                    else if($viewer && $viewer->deleted == 1) {
                        $time = Carbon::now()->toDateTimeString();
                        $usite = \DB::table('sites')->where('idsite', $site->idsite)->update([
                            "status" => 1,
                            "deleted" => 0
                        ]);
                        $viewer = \DB::table('players')->where('idplayer', $viewer->idplayer)->update([
                            "code" => $line[0],
                            "status" => 1,
                            "deleted" => 0
                        ]);
                    } 
                    else {
                        $time = Carbon::now()->toDateTimeString();
                        $usite = \DB::table('sites')->where('idsite', $site->idsite)->update([
                            "status" => 1,
                            "deleted" => 0
                        ]);
                        $viewer = \DB::table('players')->insertGetId([
                            "idsite" => $site->idsite,
                            "idarea" => 523,
                            "code" => $line[0],
                            "idlang" => 1,
                            "idtemplate" => 1,
                            "email" => $line[0]."@otis.com",
                            "is_sync" => 0,
                            "status" => 1,
                            "deleted" => 0
                        ]);
                    }
                    if(!$viewer)
                        throw new Exception("Player no ha sido creado (". $line[0] .")");

                    $insert_number++;
                }                
            } 
            catch(Exception $ex) {
                $errors[] = ["line" => $line_number, "action" => $line[6], "data" => $ex->getMessage()];
            }
            $line_number++;
        }

        $is_success = empty($errors);
        $response = array('success' => $is_success, 'data' => "Insertados: $insert_number | Actualizados: $update_number | Eliminados: $delete_number | Erroneos: ". count($errors), 'errors' => $errors);
        return response($response, 200)->header('Content-Type', 'application/json'); 
    }
       


}