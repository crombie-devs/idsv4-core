<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use App\Repositories\AudioCategory\AudioCategoryRepository;
use App\Repositories\Audio\AudioRepository;


class WSAudiosController extends Controller
{
    private $audioRepository;
    private $audioCategoryRespository;

    public function __construct(AudioCategoryRepository $audioCategoryRespository, AudioRepository $audioRepository) {
        $this->audioRepository = $audioRepository;
        $this->audioCategoryRespository = $audioCategoryRespository;
    }
    

    public function getAudiosCategoriesCustomer($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->audioCategoryRespository->audioCategory($idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(\Exception $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function getAudiosCustomer($idcustomer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->audioCategoryRespository->audiosCustomer($idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(\Exception $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }




  
    

  

 
}
