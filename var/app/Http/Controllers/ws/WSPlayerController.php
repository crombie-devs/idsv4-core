<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Player\PlayerFindByCodeRequest;
use App\Http\Requests\Player\PlayerFindByEmailRequest;
use App\Http\Requests\Player\PlayerGetPlaylistRequest;
use App\Repositories\Player\WSPlayerRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Player\PlayerAuthRequest;
use Carbon\Carbon;
use App\Http\Requests\Player\PlayerPlayLogicRequest;
use App\Http\Requests\Player\PlayerContentRequest;
use App\Http\Requests\Player\PlayerEmissionsRequest;

use App\Console\Commands\BuildPlayList;


class WSPlayerController extends Controller
{
    private $WSPlayerRepository;

    public function __construct(WSPlayerRepositoryInterface $WSPlayerRepository) {
        $this->WSPlayerRepository = $WSPlayerRepository;
    }

    /**
     * Recoge playlist del player
     * 
     * @acceso por token
     * 
     * @group WebService Player
     */
    public function playlist(PlayerGetPlaylistRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $data = $request->all();
            $response = $this->WSPlayerRepository->getPlaylist($data['idcustomer'], $data['idarea']);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * WS Auth for Player
     * Debe devolver el **detalle del player** que cumpla:
     * player.email = $request.email
     * player.status = 1
     * player.deleted = 0
     * 
     * @acceso por token
     * @urlParam email string required
     * 
     * @group WebService Player
     */
    public function auth(PlayerAuthRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSPlayerRepository->auth($request->email);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }catch(NotFoundException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function findByCode(PlayerFindByCodeRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSPlayerRepository->findByCode($request->code);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }catch(NotFoundException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * WS Playlogics for Player:
     * Debe devolver el listado de playlogics que cumpla:
     * 
     * player.id = $request.idplayer
     * player.status = 1
     * player.deleted = 0
     * player.idplayer == playlogic.idplayer ó (si el array es vacio) player.idarea == playlogic.idarea  
     * 
     * @acceso por token
     * @urlParam idplayer integer required
     * 
     * @group WebService Player
     */
    public function playlogics(PlayerPlayLogicRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSPlayerRepository->playlogics($request->idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * WS Contents for Player:
     * Debe devolver el listado de "contents" que cumpla:
     * 
     * content.idcustomer = $player.site.idcustomer | NULL
     * content.idsite = $player.idsite | NULL
     * content.date_on >= $TODAY
     * content.date_off <= $TODAY
     * content.status = 1
     * content.deleted = 0
     * 
     * @acceso por token
     * @urlParam idplayer integer required
     * 
     * @group WebService Player
     */
    public function contents(PlayerContentRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            if(empty($request->date))
                $date = date("Y-m-d");
            else
                $date =  $request->date;  
                    
            $response = $this->WSPlayerRepository->contents($request->idplayer,$date);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function contentsBySite($idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSPlayerRepository->contentsBySite($idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function saveEmissions($idplayer, PlayerEmissionsRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSPlayerRepository->saveEmissions($idplayer, $request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function getTokenGenerate(){
        $ts =  Carbon::now()->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);

        return "?timestamp=".$current_timestamp."&keyhash=".$keyhash;
    }

    public function generatePlaylist(){
        $gen = new BuildPlayList();        

        $gen->getContentsBySite();
        $gen->getCircuitsByCustomer();
        $gen->getPlayers();
        $gen->getPlayLogics();

        $response = ['success' => true, 'data' => 'Playlist generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
   
    }

    public function generatePlaylistByCustomer($idcustomer){
        $gen = new BuildPlayList();        

        $gen->getContentsBySite(null, $idcustomer);
        $gen->getCircuitsByCustomer($idcustomer);
        $gen->getPlayers(null, null, $idcustomer);
        $gen->getPlayLogics(null, null, $idcustomer);

        $response = ['success' => true, 'data' => 'Playlist generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function generatePlaylistBySite($idsite){
        $gen = new BuildPlayList();        

        $gen->getContentsBySite($idsite, null);
        $gen->getCircuitsByCustomer($idcustomer);
        $gen->getPlayers(null, $idsite, null);
        $gen->getPlayLogics(null, $idsite, null);

        $response = ['success' => true, 'data' => 'Playlist generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function generatePlaylistByPlayer($idplayer){
        $gen = new BuildPlayList();        

        $gen->getContentsBySite(null, null);
        $gen->getCircuitsByCustomer(null);
        $gen->getPlayers($idplayer, null, null);
        $gen->getPlayLogics($idplayer, null, null);

        $response = ['success' => true, 'data' => 'Playlist generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function enviarNotificacionPlayer($idplayer){
        $gen = new BuildPlayList();   
        $gen->getNotification($idplayer, null, null, null);

        $response = ['success' => true, 'data' => 'Notificacion generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function enviarNotificacionSite($idsite){
        $gen = new BuildPlayList();   
        $gen->getNotification(null, $idsite, null, null);

        $response = ['success' => true, 'data' => 'Notificacion generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function enviarNotificacionArea($idarea){
        $gen = new BuildPlayList();   
        $gen->getNotification(null, null, $idarea, null);

        $response = ['success' => true, 'data' => 'Notificacion generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

    public function enviarNotificacionCustomer($idcustomer){
        $gen = new BuildPlayList();   
        $gen->getNotification(null, null, null, $idcustomer);

        $response = ['success' => true, 'data' => 'Notificacion generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }
}
