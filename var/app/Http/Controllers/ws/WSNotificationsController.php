<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Notifications\WSNotificationsRepositoryInterface;
//use App\Http\Requests\Notifications\NotificationsNotificationRequest;
use App\Traits\GenericPusher;

class WSNotificationsController extends Controller
{
    private $WSNotificationsRepository;
    use GenericPusher;

    public function __construct(WSNotificationsRepositoryInterface $WSNotificationsRepository
     ) {
        $this->WSNotificationsRepository = $WSNotificationsRepository;
        
    }

    public function notificate(Request $request){
        $this->WSNotificationsRepository->notificate($request);
    }


    public function sendPush( Request $request) {
      
       $socket = $request->socket;
       $event = $request->event;
       $data = $request->data;
       $from = $request->from;

       $status = $this->send($socket, $event, $data, $from);


       return response()->json($status, 200);
    }



}
