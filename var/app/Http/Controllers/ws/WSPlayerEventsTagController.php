<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use App\Repositories\DynamicEvents\DynamicEventsRepository;
use Illuminate\Http\Request;
use App\Exceptions\DatabaseErrorException;

class WSPlayerEventsTagController extends Controller
{
    private $dynamicEventsRepository;
    public function __construct(
        DynamicEventsRepository $dynamicEventsRepository
     ) {
        $this->dynamicEventsRepository = $dynamicEventsRepository;
    }
    public function  tagsPlayerEvent($idplayer) {
        try { 
            $data = $this->dynamicEventsRepository->tagPlayer($idplayer);
        } catch(DatabaseErrorException $e) {
            $response = ['success' => false, 'data' => [], 'statusCode' => 401];
        }
        $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }
}
