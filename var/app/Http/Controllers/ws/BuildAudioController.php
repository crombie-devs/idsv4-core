<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use App\Console\Commands\GenerateAudioList;


class BuildAudioController extends Controller
{
    public function __construct() {        
    }
   
   

    public function generateAudiolistByCustomer($idcustomer){
        $gen = new GenerateAudioList();        
        $gen->getAudiosByCustomer($idcustomer);
        $response = ['success' => true, 'data' => 'Playlist generada', 'statusCode' => 200];
        return response()->json($response, $response['statusCode']);
    }

   
}
