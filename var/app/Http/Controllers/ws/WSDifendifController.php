<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use App\Repositories\Difendif\WSDifendifRepository;

use App\Exceptions\DatabaseErrorException;

class WSDifendifController extends Controller
{

    protected $wsDifendifRepository;

    public function __construct(WSDifendifRepository $wsDifendifRepository)
    {
        $this->wsDifendifRepository = $wsDifendifRepository;
    }

    public function list(array $request)
    {
        try {
            if ($request->idcampaign) {
                $response = $this->wsDifendifRepository->list($request->idcustomer, $request->ejercicio, $request->mes, $request->idcampaign);
            } else {
                $response = $this->wsDifendifRepository->list($request->idcustomer, $request->ejercicio, $request->mes);
            }

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function add(array $request)
    {
        try {
            $response = $this->wsDifendifRepository->add($request);

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function delete(array $request)
    {        
        try {

            if ($request->idcampaign) {
                $response = $this->wsDifendifRepository->delete($request->idcustomer, $request->ejercicio, $request->mes, $request->idcampaign);
            } else {
                $response = $this->wsDifendifRepository->delete($request->idcustomer, $request->ejercicio, $request->mes);
            }

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

}
