<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\DatabaseErrorException;
use Carbon\Carbon;
use App\Repositories\Customer\WSCustomerRepositoryInterface;
use App\Http\Requests\Customer\CustomerCircuitsRequest;


class WSCustomerController extends Controller
{
    private $WSCustomerRepository;

    public function __construct(WSCustomerRepositoryInterface $WSCustomerRepository) {
        $this->WSCustomerRepository = $WSCustomerRepository;
    }
   
    /**
     * WS Debe devolver el listado de circuitos asociados a un idcustomer con la condicion
     * playcircuit.status = 1 y playcircuit.deleted = 0
     * 
     * 
     * 
     * @acceso por token
     * @urlParam idcustomer integer required
     * 
     * @group WebService Customer
     */
    public function getCircuit(CustomerCircuitsRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSCustomerRepository->getCircuit($request->idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /** 
     * WS de importación de productos y categorias (de Tickets)
     * 
     * 
     * @acceso por token
     * @urlParam idcustomer integer required
     *  
     * @group WebService Customer
     */
    public function importProducts(CustomerProductsRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $customer = Customer::find($request->idcustomer);

            $count = 0;
            $file = $request->file('csv');
            $reader = Reader::createFromFileObject($file->openFile());
            foreach ($reader as $index => $row) {
                $category = null;
                if(isset($row->category1_id) && strcmp($customer->category_level, 'N1') === 0)
                    $category = ProductCategory::updateOrCreate(
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category1_id],
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category1_id, 'name' => $row->category1_name],
                    );
                else if(isset($row->category2_id) && strcmp($customer->category_level, 'N2') === 0)
                    $category = ProductCategory::updateOrCreate(
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category2_id],
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category2_id, 'name' => $row->category2_name],

                    );
                else if(isset($row->category3_id) && strcmp($customer->category_level, 'N3') === 0)
                    $category = ProductCategory::updateOrCreate(
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category3_id],
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category3_id, 'name' => $row->category3_name],

                    );
                else if(isset($row->category4_id) && strcmp($customer->category_level, 'N4') === 0)
                    $category = ProductCategory::updateOrCreate(
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category4_id],
                        ['idcustomer' => $request->idcustomer, 'code' => $row->category4_id, 'name' => $row->category4_name],

                    );
                else 
                    throw new \Exception("No se ha creado categoría (linea: $count)");

                if(isset($row->product_id))
                    $product = Product::updateOrCreate(
                        ['idcustomer' => $request->idcustomer, 'code' => $row->product_id],
                        ['idcustomer' => $request->idcustomer, 'code' => $row->product_id, 'name' => $row->product_name, 'idcategory' => $category->idcategory],
                    );
                else 
                    throw new \Exception("No se ha creado producto (linea: $count)");
                
                $count++;
            }
            $response = ['success' => false, 'data' => "$count productos (y sus categorias) insertados con exito", 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
