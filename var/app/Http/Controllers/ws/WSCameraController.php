<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\DatabaseErrorException;
use Carbon\Carbon;
use App\Repositories\Camera\WSCameraRepositoryInterface;
use App\Http\Requests\Camera\CameraAuthRequest;


class WSCameraController extends Controller
{
    private $WSCameraRepository;

    public function __construct(WSCameraRepositoryInterface $WSCameraRepository) {
        $this->WSCameraRepository = $WSCameraRepository;
    }
   
    /**
     * WS Auth for Camera
     * Debe devolver el **detalle del camera** que cumpla:
     * camera.email = $request.email
     * camera.status = 1
     * camera.deleted = 0
     * 
     * @acceso por token
     * @urlParam email string required
     * 
     * @group WebService Camera
     */
    public function auth(CameraAuthRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->WSCameraRepository->auth($request->email);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     
}
