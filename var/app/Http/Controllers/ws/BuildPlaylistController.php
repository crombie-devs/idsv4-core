<?php

namespace App\Http\Controllers\ws;

use App\Http\Controllers\Controller;
use App\Console\Commands\BuildPlayList;
use App\Console\Commands\BuildExternalPlayList;
use App\Traits\NotificationsPusher;
//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\AlertBuildPlaylist;

//Models
use App\Models\Customer;

class BuildPlaylistController extends Controller
{
    public function __construct() {   
        set_time_limit(0);     
    } 
    public function generateContentsByCustomer($idcustomer){
        $customer = Customer::with('config_customer')->where('idcustomer', $idcustomer)->first();
        $gen = new BuildPlayList();
        $success = ($gen->getContentsByCustomer($idcustomer) == 0);
        if($customer->config_customer->has_external_cms){
            $genExternal = new BuildExternalPlaylist();
            $success = ($genExternal->getContentsByCustomer($idcustomer) == 0);
        }
        return self::responseCache($success); 
    }
    public function generateContentsBySite($idsite){
        $gen = new BuildPlayList();  
        $success = ($gen->getContentsBySite($idsite) == 0);
        return self::responseCache($success); 
    }
    public function generateCircuitsByCustomer($idcustomer){
        $gen = new BuildPlayList(); 
        $success = ($gen->getCircuitsByCustomer($idcustomer) == 0);
        return self::responseCache($success); 
    }
    public function generateCircuitsBySite($idsite){
        $gen = new BuildPlayList(); 
        $success = ($gen->getCircuitsBySite($idsite) == 0);
        return self::responseCache($success); 
    }
    public function generatePlayersByCustomer($idcustomer){
        $gen = new BuildPlayList();
        $success = ($gen->getPlayersByCustomer($idcustomer) == 0);
        return self::responseCache($success); 
    }
    public function generatePlayersBySite($idsite){
        $gen = new BuildPlayList();
        $success = ($gen->getPlayersBySite($idsite) == 0);
        return self::responseCache($success); 
    } 
    public function generatePlayersByPlayer($idplayer){
        $gen = new BuildPlayList();        
        $success = ($gen->getPlayersByPlayer($idplayer) == 0); 
        return self::responseCache($success);      
    }
    public function generatePlayLogicsByCustomer($idcustomer){
        $gen = new BuildPlayList();
        $success = ($gen->getPlayLogicsByCustomer($idcustomer) == 0);
        return self::responseCache($success); 
    }
    public function generatePlayLogicsBySite($idsite){
        $gen = new BuildPlayList();
        $success = ($gen->getPlayLogicsBySite($idsite) == 0);
        return self::responseCache($success); 
    }
    public function generatePlayLogicsByPlayer($idplayer){
        $gen = new BuildPlayList(); 
        $success = ($gen->getPlayLogicsByPlayer($idplayer) == 0);
        return self::responseCache($success); 
    }
    public function generatePlayLogicsByArea($idarea){
        $gen = new BuildPlayList(); 
        $success = ($gen->getPlayLogicsByArea($idarea) == 0);
        return self::responseCache($success); 
    } 
    public function generatePlaylist(){
        $gen = new BuildPlayList();
        $success = $gen->getContents();          
        $success += $gen->getCircuits();
        $success += $gen->getPlayers();
        $success += $gen->getPlayLogics();
        return self::responseCache($success == 0);   
    }
    public function generatePlaylistByCustomer($idcustomer){
        $gen = new BuildPlayList(); 
        $success = $gen->getContentsByCustomer($idcustomer);
        $success += $gen->getCircuitsByCustomer($idcustomer);
        $success += $gen->getPlayersByCustomer($idcustomer);
        $success += $gen->getPlayLogicsByCustomer($idcustomer);
        return self::responseCache($success == 0); 
    }
    public function generatePlaylistBySite($idsite){
        $gen = new BuildPlayList();
        $success =  $gen->getContentsBySite($idsite);
        $success +=  $gen->getCircuits();
        $success +=  $gen->getPlayers();
        $success +=  $gen->getPlayLogics();
        return self::responseCache($success == 0); 
    }
    public function generatePlaylistByPlayer($idplayer){
        $gen = new BuildPlayList();  
        $success = $gen->getContents();
        $success += $gen->getCircuits();
        $success += $gen->getPlayersByPlayer($idplayer);
        $success += $gen->getPlayLogicsByPlayer($idplayer);
        return self::responseCache($success  == 0);
    }
    public function generatePlayersByOtis(){
        $gen = new BuildPlayList();  
        $success = ($gen->getPlayersByOtis() == 0);
        return self::responseCache($success); 
    }
    public function generateContentsByOtis(){
        $gen = new BuildPlayList();  
        $success = ($gen->getContentsByOtis() == 0);
        return self::responseCache($success); 
    }
    public function sendNotificacionByPlayer($idplayer){
        $success = NotificationsPusher::send("viewer-".$idplayer, "viewer.update");
        return self::responseNotificacion($success);
    }
    public function sendNotificacionUpdateBySite($idsite){
        $success = NotificationsPusher::send("site-".$idsite, "viewer.update");        
        return self::responseNotificacion($success);
    }
    public function sendNotificacionRefreshBySite($idsite){
        $success = NotificationsPusher::send("site-".$idsite, "playlist.refresh");        
        return self::responseNotificacion($success);
    }
    public function sendNotificacionByArea($idarea){
        $success = NotificationsPusher::send("area-".$idarea, "playlist.refresh");        
        return self::responseNotificacion($success);
    }
    public function sendNotificacionUpdateByCustomer($idcustomer){        
        $success = NotificationsPusher::send("customer-".$idcustomer, "viewer.update");        
        return self::responseNotificacion($success);
    }
    public function sendNotificacionRefreshByCustomer($idcustomer){
        $success = NotificationsPusher::send("customer-".$idcustomer, "playlist.refresh");      
        return self::responseNotificacion($success);
    }
    public function sendMail($idcontent){
        Mail::send(new AlertBuildPlaylist($idcontent, "Error generar playlist"));
    }
    private static function responseCache($success){
        if($success) 
            $response = ['success' => $success, 'data' => 'Cache generada'];
        else
            $response = ['success' => $success, 'data' => 'Error al generar cache']; 
        return response()->json($response, ($success)? 200 : 503);
    }
    private static function responseNotificacion($success){
        if (!$success)
            $response = ['success' => $success, 'data' => 'Error en el envío'];
        else
            $response = ['success' => $success, 'data' => 'Envío correcto'];  
        return response()->json($response, ($success)? 200 : 503);
    }
}
