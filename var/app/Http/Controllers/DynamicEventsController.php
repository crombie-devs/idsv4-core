<?php

namespace App\Http\Controllers;



use App\Http\Requests\DynamicEvents\DynamicEventsCreateRequest;
use App\Http\Requests\DynamicEvents\DynamicEventsUpdateRequest;
use App\Exceptions\DatabaseErrorException;
use App\Repositories\DynamicEvents\DynamicEventsRepository;
use Exception;


class DynamicEventsController extends Controller
{
    protected $dynamicRepository;
    

    public function __construct(DynamicEventsRepository $dynamicRepository)
    {
        $this->dynamicRepository = $dynamicRepository;
    }


    public function get($id)  {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->dynamicRepository->get($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    
    public function byPlayer($idcustomer, $idsite, $idplayer)  {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->dynamicRepository->byPlayer($idcustomer, $idsite, $idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
  

    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->dynamicRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function list()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->dynamicRepository->list();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function create(DynamicEventsCreateRequest $request)
    {

        $data = [];

        $status = $request->status == "true" ? 1 : 0;
        $is_right_now = $request->is_right_now == "true" ? 1 : 0;

        $data['name'] = $request->name;
        $data['source_url'] = $request->source_url;
        $data['is_right_now'] = $is_right_now;
        $data['frecuency'] = $request->frecuency;
        $data['status'] = $status;
        $data['idcustomer'] = $request->idcustomer;
        $data['idsite'] = $request->idsite;
        $data['idplayer'] = $request->idplayer;
        $data['rules'] = json_decode($request->rules);
        
        try {
            $response = $this->dynamicRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (Exception $e) {
            $response = ['success' => $data['rules'], 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function delete($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->dynamicRepository->delete($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function update(DynamicEventsUpdateRequest $request)
    {

        $data = [];

        $status = $request->status == "true" ? 1 : 0;
        $is_right_now = $request->is_right_now == "true" ? 1 : 0;

        
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['source_url'] = $request->source_url;
        $data['is_right_now'] = $is_right_now;
        $data['frecuency'] = $request->frecuency;
        $data['status'] = $status;
        $data['idcustomer'] = $request->idcustomer;
        $data['idsite'] = $request->idsite;
        $data['idplayer'] = $request->idplayer;
        $data['rules'] = json_decode($request->rules);
        try {
            $response = $this->dynamicRepository->update($data, $request->id);

            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
