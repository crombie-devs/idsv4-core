<?php

namespace App\Http\Controllers;

use App\Http\Requests\People\PeopleCreateRequest;
use App\Http\Requests\People\PeopleUpdateRequest;
use App\Repositories\People\PeopleRepository;
use Illuminate\Http\Request;
use App\Models\MessageDestinations;
use DateTime;
use DB;

class PeopleController extends Controller
{

    private $peopleRepository;

    public function __construct(PeopleRepository $peopleRepository)
    {
        $this->peopleRepository = $peopleRepository;
    }


    public function uploadCSV(Request $request)
    {
        $file = $request->file('csv');
        $idmessage = $request->idmessage;
        $datetime = $request->datetime;
        if ($file) {
            $filename = $file->getClientOriginalName();
            $location = 'peoples\csv';
            $file->move($location, $filename);
            $filepath = public_path($location . "/" . $filename);
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);

                if ($i == 0) {
                    $i++;
                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file);
            unlink($filepath);
            $j = 0;
            foreach ($importData_arr as $importData) {
                $j++;
                try {
                    $response =  DB::beginTransaction();
                    MessageDestinations::create([
                        'email' => $importData[1],
                        'phone' => $importData[2],
                        'datetime' =>  new DateTime($datetime),
                        'idmessage' => $idmessage,
                    ]);
                    DB::commit();
                } catch (\Exception $e) {
                    //throw $th;
                    DB::rollBack();
                }
            }
        }

        try {

            $response = ['success' => true, 'data' => $j, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }



    public function create(PeopleCreateRequest $request)
    {
        $data = [];
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['idsite'] = $request->idsite;
        $data['idcustomer'] = $request->idcustomer;
        $data['tags'] = json_decode($request->tags);
        $data['created_at'] =  new DateTime();
        $data['updated_at'] = new DateTime();


        try {
            $response = $this->peopleRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function update(PeopleUpdateRequest $request)
    {
        $data = [];
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['idsite'] = $request->idsite;
        $data['idcustomer'] = $request->idcustomer;
        $data['tags'] = $request->tags;
        $data['id'] = $request->id;
        $data['updated_at'] = new DateTime();

        try {
            $response = $this->peopleRepository->update($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function delete(Request $id)
    {


        try {
            $response = $this->peopleRepository->delete($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function get(int $id)
    {
        try {
            $response = $this->peopleRepository->get($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function toSelect()
    {
        try {
            $response =  $response = $this->peopleRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
}
