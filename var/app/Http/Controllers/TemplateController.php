<?php

namespace App\Http\Controllers;

use App\Http\Requests\Template\TemplateCreateRequest;
use App\Http\Requests\Template\TemplateUpdateRequest;
use App\Repositories\Template\TemplateRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class TemplateController extends Controller
{
    private $templateRepository;

    public function __construct(TemplateRepositoryInterface $templateRepository) {
        $this->templateRepository = $templateRepository;
    }
    /** 
     * Creación de una nueva template
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string 
     * @bodyParam html string 
     * 
     * @group Template management
     */
    public function create(TemplateCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->templateRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una template
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplate required
     * @bodyParam name string
     * @bodyParam html string 
     * 
     * @group Template management
     */
    public function update(int $idtemplate, TEmplateUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templateRepository->update($idtemplate, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una template de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idtemplate required 
     * 
     * @group Template management
     */
    public function delete(int $idtemplate) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templateRepository->delete($idtemplate);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una template
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idtemplate int required
     * @group Template management
     */
    public function template(int $idtemplate) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->templateRepository->template($idtemplate);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las templates de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Template management
     */
    public function templates(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->templateRepository->templates();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
   
}
