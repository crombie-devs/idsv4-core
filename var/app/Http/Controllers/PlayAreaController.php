<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayArea\PlayAreaCreateRequest;
use App\Http\Requests\PlayArea\PlayAreaUpdateRequest;
use App\Repositories\PlayArea\PlayAreaRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;
use App\Http\Requests\PlayArea\PlayAreaUpdatePlayLogicsRequest;
use App\Http\Requests\PlayArea\PlayAreaUpdateContentsOrder;
use App\Http\Requests\PlayArea\PlayAreaTocircuitRequest;

use App\Repositories\Log\LogRepository;

class PlayAreaController extends Controller
{
    private $playAreaRepository;

    public function __construct(
        PlayAreaRepositoryInterface $playAreaRepository
    ) {
        $this->playAreaRepository = $playAreaRepository;
    }

    /** 
     * Creación de un nuevo playArea
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam idlocation string 
     * @bodyParam location object
     * @bodyParam idformat string
     * @bodyParam format object
     * 
     * @group PlayArea management
     */
    public function create(PlayAreaCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playAreaRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayArea', 'Create', true, $request, $response);
        }
        catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayArea', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un playArea
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idarea required
     * @bodyParam idlocation string 
     * @bodyParam location object
     * @bodyParam idformat string
     * @bodyParam format object
     * 
     * @group PlayArea management
     */
    public function update(int $idarea, PlayAreaUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playAreaRepository->update($idarea, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayArea', 'Update', true, $request, $response);
        }
        catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayArea', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un playArea de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idarea required 
     * 
     * @group PlayArea management
     */
    public function delete(int $idarea) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->delete($idarea);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayArea', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayArea', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un playArea
     * Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user de la misma tienda
     * 
     * @authenticated
     * 
     * @urlParam idarea int required
     * @group PlayArea management
     */
    public function playArea(int $idarea) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->playArea($idarea);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los playAreas de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group PlayArea management
     */
    public function playAreas(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->playAreas();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los playAreas de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group PlayArea management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los playAreas de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group PlayArea management
     */
    public function toShow(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->toShow();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

      /**
     * Acceso a todos los playAreas de los circuitos seleccionado
     * Esta acción la puede realizar cualquier usuario
     * @bodyParam playcircuits required array idcircuits integer required
     * 
     * 
     * @group PlayArea management
     */
    public function toCreatePlaylist(PlayAreaTocircuitRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->toCreatePlaylist($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los playlogic de un playarea
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playarea con todos los nuevos playlogic.
     * 
     * @bodyParam playlogics required array idlogic integer required
     * @urlParam idarea required int
     * 
     * @authenticated
     * 
     * @group PlayArea management
     */
    public function updatePlaylogics(PlayAreaUpdatePlayLogicsRequest $request, int $idarea){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playAreaRepository->updatePlaylogics($request, $idarea);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playlogic/playarea
     * Esta acción la puede realizar cualquier usuario
     * Devuelve playarea con nuevos playlogic
     * 
     * @urlParam idarea required int
     * @urlParam idlogic required int
     * 
     * @authenticated
     * 
     * @group PlayArea management
     */
    public function deletePlaylogic(int $idarea, int $idlogic){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->deletePlaylogic($idarea,$idlogic);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los playAreas de un usuario
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group PlayArea management
     */
    public function toUser(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->toUser();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Actualización de los area order de contents
     * Esta acción la puede realizar superadmin y admin
     * Debe devolver el nuevo content order del area
     * 
     * @bodyParam contents required array idcontent integer required
     * @urlParam idarea required int
     * @urlParam idcategory required int
     * 
     * @authenticated
     * 
     * @group PlayArea management
     */
    public function updateContentsOrder(PlayAreaUpdateContentsOrder $request, int $idarea, int $idcategory){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playAreaRepository->updateContentsOrder($request, $idarea, $idcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Debe eliminar: play_areas (idarea) / contents_has_areas (idarea) / contents_order (idarea) / play_logics (idarea) / players (idarea)
     * Esta acción la puede realizar cualquier usuario
     * Devuelve playarea 
     * 
     * @urlParam idarea required int
     * 
     * @authenticated
     * 
     * @group PlayArea management
     */
    public function deletePlayAreaForce(int $idarea){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playAreaRepository->deletePlayAreaForce($idarea);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
