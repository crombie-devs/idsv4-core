<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\DatabaseErrorException;
use App\Repositories\Recomendations\RecomendationsRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Traits\FileUpload;
use App\Jobs\ImportTickets;


class RecomendationsController extends Controller
{
    private $recomendationsRepository;
    private $productrepository;
    use FileUpload;

    public function __construct(RecomendationsRepositoryInterface $recomendationsRepository, ProductRepositoryInterface $productRepository) {
        $this->recomendationsRepository = $recomendationsRepository;
        $this->productrepository = $productRepository;
    }

    /** 
     * Consulta para recoger las categorías de la sección recomendaciones/asociaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * 
     * @group Recomendations Consult
     */
    public function categories() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->categories();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Consulta para recoger los datos de la sección recomendaciones/asociaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * 
     * @group Recomendations Consult
     */
    public function probabilidadCondicionada(int $idcategory){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->probabilidadCondicionada($idcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function recomendationsCategories(int $month){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->recomendationsCategories($month);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger las categorías de la sección recomendaciones/recomendaciones con opción de tendencias
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function recomendationsTrendsCategories(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->recomendationsTrendsCategories();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getPatronVentas(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getPatronVentas($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger las provincias de la sección recomendaciones/recomendaciones en tendencias
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getTrendsProvinces(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getTrendsProvinces();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger las categorías de la sección recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getFilasDiasSemana(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getFilasDiasSemana($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger la temperatura de la la categoría seleccionada en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getTemperature(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getTemperatura($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Consulta para recoger los productos de la categoría seleccionada en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getProductsFromCategory(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getProductsFromCategory($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger los datos socio-económicos en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getSocialDemographic(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getSociodemographic($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger las ciudades de un customer en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getProvinces(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getProvinces();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    
    /** 
     * Consulta para recoger los radios de una ciudad en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getRatius(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getRatius($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Consulta para recoger los sites según las provincias seleccionadas en recomendaciones/recomendaciones
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * @urlParam month integer required
     * 
     * @group Recomendations Consult
     */
    public function getSitesFromProvinces(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getSitesFromProvinces($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Consulta para recoger los datos para mostrar el análisis socioeconómico en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getSocialEconomicAdminData(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getSocialEconomicAdminData($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }

    /** 
     * Consulta para recoger los rangos de edades para análisis de segmentación en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getAgeVars(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getAgeVars();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }

    /** 
     * Consulta para recoger los rangos de salarios para mostrar el análisis de segmentación en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getIncomeRanges(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getIncomeRanges($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }

    /** 
     * Consulta para recoger los datos según consulta el análisis de segmentación en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getDemographicSegmentation(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getDemographicSegmentation($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }
    /** 
     * Consulta para recoger los datos según consulta el análisis de segmentación en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function getEconomicSegmentation(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->recomendationsRepository->getEconomicSegmentation($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }
    
    /** 
     * Consulta para recoger los datos según consulta el análisis de segmentación en análisis/socioeconómico
     * Esta acción la puede realizar el superadmin y admin
     * 
     * @authenticated
     * 
     * @group Recomendations Consult
     */
    public function update(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $client = new \GuzzleHttp\Client();
            $url = "http://82.223.69.121:5000/recomendations";
            //$url = "http://localhost:5000/recomendations";
            $data = json_encode($request->all(), true);
            $data = json_decode($data);
            $responsePython = $client->request('POST',$url, ['json'=>$data, 'timeout' => 60, 'verify' => false]);
            $data = $responsePython->getBody(true)->getContents();
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);        
    }

    public function uploadXlsx(int $idcustomer, Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = ['success' => true, 'data' => 'Proceso iniciado', 'statusCode' => 200];
            $file = $this->uploadFile($request->tickets);
            ImportTickets::dispatchAfterResponse($this->productrepository,$idcustomer, $file);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']); 
    }

    public function uploadFile($file){
        try {
            return $this->TicketsExcelUpload($file);
        } catch (\Exception $e) {
            return false;
        }
    }
}