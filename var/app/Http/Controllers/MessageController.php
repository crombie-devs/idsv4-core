<?php

namespace App\Http\Controllers;

use App\Http\Requests\Message\MessageCreateRequest;
use App\Http\Requests\Message\MessageUpdateRequest;
use App\Repositories\Messages\MessageRepository;
use Illuminate\Http\Request;

use DateTime;

class MessageController extends Controller
{

    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }



    public function messageByCustomer(int $id)
    {
        try {
            $response = $this->messageRepository->messageByCustomer($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }


        return response()->json($response, $response['statusCode']);
    }


    public function deleteMsg(int $id)
    {
        try {
            $response = $this->messageRepository->deleteMsg($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }


        return response()->json($response, $response['statusCode']);
    }

    public function deleteTemplates(int $id)
    {

        try {
            $response = $this->messageRepository->deleteTemplates($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }


        return response()->json($response, $response['statusCode']);
    }

    public function getTemplates()
    {
        try {
            $response = $this->messageRepository->getTemplates();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function createTemplate(Request $request)
    {
        $data = [];
        $data['nameTemplate'] = $request->nameTemplate;

        try {
            $response = $this->messageRepository->createTemplate($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function updateTemplate(Request $request)
    {
        $data = [];
        $data['nameTemplate'] = $request->nameTemplate;
        $data['id'] = $request->id;

        try {
            $response = $this->messageRepository->updateTemplate($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    public function create(Request $request)
    {


        $data = $request->all();
        $msg = [];

        foreach ($data as $value) {
            $msg['idformat'] = $value['idformat'];
            $msg['body'] = $value['message'];
            $msg['idlang'] = $value['idlang'];
            $msg['idcontent'] = $value['idcontent'];
            $msg['datetime']  = new Datetime($value['datetime']);
            $msg['subject']  = $value['subject'];
            $response = $this->messageRepository->create($msg);
        }
        try {
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    private function isContact($key)
    {

        if ($key == 'sms' || $key == 'whatsapp' || $key == 'email')
            return true;
        return false;
    }

    public function update(MessageUpdateRequest $request)
    {
        $data = [];
        $data['subject'] = $request->subject;
        $data['datetime'] = new DateTime($request->datetime);
        $data['body'] = $request->body;
        $data['tags'] = $request->tags;
        $data['id'] = $request->id;
        $data['nameTemplate'] = $request->nameTemplate ? $request->nameTemplate : false;

        try {
            $response = $this->messageRepository->update($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function delete(int $id)
    {
        $response = $this->messageRepository->delete($id);
        return response()->json($response, $response['statusCode']);
    }

    public function get(int $id)
    {
        try {
            $response = $this->messageRepository->get($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function toSelect()
    {
        try {
            $response =       $response = $this->messageRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function getFormats()
    {
        try {
            $response = $this->messageRepository->getFormats();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
}
