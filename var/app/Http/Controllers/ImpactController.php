<?php

namespace App\Http\Controllers;

use App\Http\Requests\Impact\ImpactRequest;
use App\Repositories\Impact\ImpactRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class ImpactController extends Controller
{
    private $impactRepository;

    public function __construct(ImpactRepositoryInterface $impactRepository) {
        $this->impactRepository = $impactRepository;
    }
    /** 
     * Llamada para recoger datos resumen de Emisiones y ventas
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string required
     * @bodyParam year string required
     * @bodyParam month string required
     * 
     * @group Impact Consult
     */
    public function summary(ImpactRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->impactRepository->summary($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
}
