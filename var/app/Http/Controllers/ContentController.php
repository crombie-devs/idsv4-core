<?php

namespace App\Http\Controllers;

use App\Http\Requests\Content\ContentsDeleteRequest;
use App\Http\Requests\Content\ContentCreateRequest;
use App\Http\Requests\Content\ContentUpdateRequest;
use App\Http\Requests\Content\ContentToEmissionRequest;
use App\Http\Requests\Content\ContentUpdatePowersRequest;
use App\Http\Requests\Content\ContentUpdateAssetsRequest;
use App\Http\Requests\Content\ContentUpdatePlayareasRequest;
use App\Http\Requests\Content\ContentUpdatePlaycircuitsRequest;
use App\Http\Requests\Content\ContentUpdateSitesRequest;
use App\Http\Requests\Content\ContentUpdateProductsRequest;
use App\Http\Requests\Content\ContentUpdateTagsRequest;
use App\Http\Requests\Content\ContentUpdateOrdersRequest;
use App\Http\Requests\Content\ContentGetOrdersRequest;
use App\Http\Requests\Content\ContentProductPrimetimeRequest;
use App\Http\Requests\Content\ContentContentsPrimetimeRequest;
use App\Http\Requests\Content\ContentUpdateDatesRequest;
use App\Http\Requests\Content\ContentsRequest;
use App\Repositories\Content\ContentRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Content\ContentToSelectRequest;

use App\Repositories\Log\LogRepository;

class ContentController extends Controller
{
    private $ContentRepository;

    public function __construct(ContentRepositoryInterface $ContentRepository)
    {
        $this->ContentRepository = $ContentRepository;
    }



    public function contentStatus(string $id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contentStatus($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }




    /** 
     * Creación de un nuevo content
     * Esta acción la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden crear "contents" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden crear "contents" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer
     * @bodyParam idsite integer
     * @bodyParam idtype integer required
     * @bodyParam idcategory integer 
     * @bodyParam code string
     * @bodyParam name string required
     * @bodyParam thumbnail_url string
     * @bodyParam tracker_url string
     * @bodyParam date_on date required
     * @bodyParam date_off date required
     * @bodyParam max_emissions integer
     * @bodyParam frequency integer
     * @bodyParam frequency_method integer
     * @bodyParam is_exclusive integer
     * @bodyParam status integer 
     * @bodyParam deleted integer 
     * @bodyParam primetime integer 
     * 
     * @group Content management
     */
    public function create(ContentCreateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];

        try {
            $data = $request->toArray();
            $content = $this->ContentRepository->create($data['form']);
            if (!empty($data['assets']))
                $assets = $this->ContentRepository->updateAssets(['assets' => $data['assets']], $content->idcontent);
            if (!empty($data['circuits']))
                $circuits = $this->ContentRepository->updatePlaycircuits(['playcircuits' => $data['circuits']], $content->idcontent);
            if (!empty($data['areas']))
                $areas = $this->ContentRepository->updatePlayAreas(['areas' => $data['areas']], $content->idcontent);
            if (!empty($data['tags']))
                $tags = $this->ContentRepository->updateTags(['tags' => $data['tags']], $content->idcontent);
            if (!empty($data['powers']))
                $powers = $this->ContentRepository->updatePowers(['powers' => $data['powers']], $content->idcontent);
            if (!empty($data['products']))
                $products = $this->ContentRepository->updateProducts(['products' => $data['products']], $content->idcontent);
            if (!empty($data['dates']))
                $dates = $this->ContentRepository->updateDates(['dates' => $data['dates']], $content->idcontent);
            if (!empty($data['sites']))
                $sites = $this->ContentRepository->updateSites(['sites' => $data['sites']], $content->idcontent);

            $response = ['success' => true, 'data' => $content, 'statusCode' => 200];
            LogRepository::logger('Content', 'Create', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Content', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un content
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden editar "contents" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden editar "contents" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer
     * @bodyParam idsite integer
     * @bodyParam idtype integer required
     * @bodyParam idcategory integer 
     * @bodyParam code string
     * @bodyParam name string required
     * @bodyParam thumbnail_url string
     * @bodyParam tracker_url string
     * @bodyParam date_on date required
     * @bodyParam date_off date required
     * @bodyParam max_emissions integer
     * @bodyParam frequency integer
     * @bodyParam frequency_method integer
     * @bodyParam is_exclusive integer
     * @bodyParam status integer 
     * @bodyParam deleted integer 
     * 
     * @group Content management
     */
    public function update(int $idcontent, ContentUpdateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];

        try {

            $data = $request->toArray();
            $content = $this->ContentRepository->update($idcontent, $data['form']);

            if (array_key_exists('assets', $data))
                $assets = $this->ContentRepository->updateAssets(['assets' => $data['assets']], $idcontent);
            if (array_key_exists('circuits', $data))
                $circuits = $this->ContentRepository->updatePlaycircuits(['playcircuits' => $data['circuits']], $idcontent);
            if (array_key_exists('areas', $data))
                $areas = $this->ContentRepository->updatePlayAreas(['areas' => $data['areas']], $idcontent);
            if (array_key_exists('tags', $data))
                $tags = $this->ContentRepository->updateTags(['tags' => $data['tags']], $idcontent);
            if (array_key_exists('powers', $data))
                $powers = $this->ContentRepository->updatePowers(['powers' => $data['powers']], $idcontent);
            if (array_key_exists('products', $data))
                $products = $this->ContentRepository->updateProducts(['products' => $data['products']], $idcontent);
            if (array_key_exists('dates', $data))
                $dates = $this->ContentRepository->updateDates(['dates' => $data['dates']], $idcontent);
            if (array_key_exists('sites', $data))
                $sites = $this->ContentRepository->updateSites(['sites' => $data['sites']], $idcontent);

            $response = ['success' => true, 'data' => $content, 'statusCode' => 200];
            LogRepository::logger('Content', 'Update', true, $request, $response);
        } catch (DatabaseErrorException $e) {

            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Content', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un content de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden eliminar "contents" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden eliminar "contents" con su mismo idcustomer.
     * 
     * @authenticated
     *
     * @urlParam idcontent required 
     * 
     * @group Content management
     */
    public function delete(int $idcontent)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];

        try {
            $response = $this->ContentRepository->delete($idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Content', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Content', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un content
     * Esta acción sólo la puede realizar el superadmin, admin  y user
     * Los usuarios con idsite, solo pueden visualizar "contents" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden visualizar "contents" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam idcontent int required
     * @group Content management
     */
    public function content(int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->content($idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contents de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin, user
     * delete = 0
     * idsite de usuario (solo si usuario tiene idsite)
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * @urlParam date array
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function contents(ContentsRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contents($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function contentsDelete(ContentsDeleteRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contentsDelete($request->idContents);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contents de la base de datos creados un mes en concreto
     * Esta acción sólo la puede realizar el superadmin, admin, user
     * delete = 0
     * idsite de usuario (solo si usuario tiene idsite)
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * @urlParam date array
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function contentsByMonth(string $month)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contentsByMonth($month);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los contents de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el listado de todos los contents y demas asociados filtrados por: status = 1 - delete = 0  
     * content.date_on <= $TODAY // ó parámetro date
     * content.date_off >= $TODAY // ó parámetro date
     * 
     * idsite de usuario (solo si usuario tiene idsite)
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * @urlParam date array
     * 
     * @authenticated
     
     * @group Content management
     */
    public function toSelect(ContentToSelectRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->toSelect($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contents de la base de datos desde el select para estadísticas/emisiones
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el listado de todos los contents del customer en el mes y año seleccionado
     * @bodyParam idcustomer integer required
     * @bodyParam idsite integer
     * @bodyParam idplayer integer
     * @bodyParam year integer 
     * @bodyParam month integer 
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function toEmission(ContentToEmissionRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->toEmission($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los powers de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos powers.
     * 
     * @bodyParam powers array required weekday integer, time_on date con formato H:i,time_off date con formato H:i
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * @group Content management
     */
    public function updatePowers(ContentUpdatePowersRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->updatePowers($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra un power
     * Esta acción la puede realizar cualquier usuario
     * Devuelve null en data
     * 
     * @urlParam idcontent required int
     * @urlParam idpower required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deletePower(int $idcontent, int $idpower)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deletePower($idcontent, $idpower);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Selecciona los productos de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos productos.
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function getProducts(int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->getProducts($idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los productos de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos productos.
     * 
     * @bodyParam products array required product_code string required, name string required
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateProducts(ContentUpdateProductsRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateProducts($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra la relación de un producto con su contenido asociado
     * Esta acción la puede realizar cualquier usuario
     * Devuelve null en data
     * 
     * @urlParam idcontent required int
     * @urlParam product_code required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deleteProduct(int $idcontent, int $product_code)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deleteProduct($idcontent, $product_code);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los playareas de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos playareas.
     * 
     * @bodyParam areas array required idarea int required
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updatePlayareas(ContentUpdatePlayareasRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updatePlayareas($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playarea - contenido
     * Esta acción la puede realizar cualquier usuario
     * Devuelve contenido con playareas
     * 
     * @urlParam idcontent required int
     * @urlParam idarea required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deletePlayarea(int $idcontent, int $idarea)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deletePlayarea($idcontent, $idarea);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los tags de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos tags.
     * 
     * @bodyParam tags required array idtag integer required
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateTags(ContentUpdateTagsRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateTags($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra un una relación tag/contenido
     * Esta acción la puede realizar cualquier usuario
     * Devuelve null en data
     * 
     * @urlParam idcontent required int
     * @urlParam idtag required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deleteTag(int $idcontent, int $idtag)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deleteTag($idcontent, $idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Añadir una nueva relación asset/contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos assets.
     * 
     * @urlParam idcontent required int
     * @urlParam idasset required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function createAsset(int $idcontent, int $idasset)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->createAsset($idcontent, $idasset);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los assets de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos assets.
     * 
     * @bodyParam assets required array idasset integer required, idcircuit integer optional
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateAssets(ContentUpdateAssetsRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateAssets($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación asset/contenido
     * Esta acción la puede realizar cualquier usuario
     * Devuelve content con nuevos assets
     * 
     * @urlParam idcontent required int
     * @urlParam idasset required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deleteAsset(int $idcontent, int $idasset)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deleteAsset($idcontent, $idasset);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los playcircuits de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos assets.
     * 
     * @bodyParam playcircuits required array idcircuit integer required
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updatePlayCircuits(ContentUpdatePlayCircuitsRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updatePlaycircuits($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los sites de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos assets.
     * 
     * @bodyParam sites required array idsite integer required
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateSites(ContentUpdateSitesRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateSites($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Actualización de los dates de un contenido
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el content con todos los nuevos assets.
     * 
     * @bodyParam dates array 
     * 
     * @urlParam idcontent required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateDates(ContentUpdateDatesRequest $request, int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateDates($request->all(), $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playcircuit/contenido
     * Esta acción la puede realizar cualquier usuario
     * Devuelve content con nuevos playcircuits
     * 
     * @urlParam idcontent required int
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function deletePlaycircuit(int $idcontent, int $idcircuit)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->deletePlaycircuit($idcontent, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los contents de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Devolverá un listado de weekdays y horas, que serán los powers.
     * @urlParam idcontent required int
     * 
     * @authenticated
     
     * @group Content management
     */
    public function primeTime(int $idcontent)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->primeTime($idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los horas de mayor venta de un producto
     * Esta acción la puede realizar cualquier usuario
     * Devolverá un listado de weekdays y horas, que serán los powers.
     * 
     * @authenticated
     * 
     * @bodyParam code_site required number
     * @bodyParam idcategoryticket required number
     * 
     * @group Content management
     */
    public function productsPrimeTime(ContentProductPrimetimeRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->productPrimeTime($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los horas de mayor venta de varios productos relacionados con varias tiendas
     * Esta acción la puede realizar cualquier usuario
     * Devolverá un listado de weekdays y horas, que serán los powers.
     * 
     * @authenticated
     * 
     * @bodyParam code_site required number
     * @bodyParam idcategoryticket required number
     * 
     * @group Content management
     */
    public function contentsPrimeTime(ContentContentsPrimetimeRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contentsPrimeTime($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los order de un contenido
     * Esta acción la puede realizar superadmin y amdin
     * 
     * @bodyParam contents required array idcontent integer required
     * 
     * 
     * @authenticated
     * 
     * @group Content management
     */
    public function updateContentOrder(ContentUpdateOrdersRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->ContentRepository->updateContentOrder($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Debe devolver el listado de "orders" que cumpla:
     * Esta acción la puede realizar cualquier usuario
     * 
     * content.idcustomer = $user.idcustomer | NULL
     * content.idsite = $user.idsite | NULL
     * content.date_on <= $TODAY   // ó parámetro date
     * content.date_off >= $TODAY   // ó parámetro date
     * content.status = 1
     * content.deleted = 0
     * 
     * @urlParam date array
     * 
     * @authenticated
     * @group Content management
     */
    public function getOrder(ContentGetOrdersRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->getOrder($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function contentsStatus(string $status)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->ContentRepository->contentsStatus($status);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
