<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Log;
use App\Repositories\Log\LogRepository;

use App\Exceptions\DatabaseErrorException;

use App\Http\Requests\Log\LogCreateRequest;

class LogController extends Controller
{

    protected $logRepository;

    public function __construct(LogRepository $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function list()
    {
        try {
            $response = $this->logRepository->list();

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function get($id)
    {
        try {
            $response = $this->logRepository->get($id);

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function create(LogCreateRequest $request)
    {
        $data = [];

        $data['iduser'] = $request->iduser;
        $data['identity'] = $request->identity;
        $data['entity'] = $request->entity;
        $data['action'] = $request->action;
        $data['ip'] = $request->ip;
        $data['status'] = ($request->status) ? $request->status : 1;
        $data['path'] = $request->path;
        $data['params'] = ($request->params) ? $request->params : null;
        $data['response'] = ($request->response) ? $request->response : null;

        $datetime = ($request->datetime) ? Carbon::parse($request->datetime) : Carbon::now();

        $data['datetime'] = $datetime;

        try {
            $response = $this->logRepository->create($data);

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

}
