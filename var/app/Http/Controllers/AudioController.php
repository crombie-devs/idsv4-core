<?php

namespace App\Http\Controllers;

use App\Http\Requests\Audio\AudioCreateRequest;
use App\Http\Requests\Audio\AudioUpdateRequest;
use App\Exceptions\DatabaseErrorException;
use App\Repositories\Audio\AudioRepository;
use App\Traits\ImageUpload;

use App\Repositories\Log\LogRepository;

class AudioController extends Controller
{
    protected $audioRepository;
    use ImageUpload;

    public function __construct(
        AudioRepository $audioRepository
    ) {
        $this->audioRepository = $audioRepository;
    }


    public function uploadAudio($file)
    {
        try {
            return $this->audioBucketUpload($file);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda el audio', 'statusCode' => 503];
        }
    }

    public function toSelect($id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioRepository->audioToSelect($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function toSelectAll()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioRepository->audioToSelectAll();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }





    public function create(AudioCreateRequest $request)
    {

        $data = [];

        if ($request->audio_url) {
            $data['audio_url']  = $this->uploadAudio($request->audio_url);
        } else {
            $data['audio_url'] = null;
        }


        $data['name'] = $request->name;
        $data['isrc'] = $request->isrc;
        $data['duration'] = $request->duration;
        $data['idcategory'] = $request->idcategory;

        try {
            $response = $this->audioRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Audio', 'Create', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Audio', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }




    public function list()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->audioRepository->audioList();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }






    public function delete($id)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->audioRepository->deleteAudio($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Audio', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Audio', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }


    public function update(AudioUpdateRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];


        if ($request->audio_url) {
            if ($request->hasFile('audio_url')) {
                $data['audio_url']   = $this->uploadAudio($request->audio_url);

                if (empty($request->audio_url)) {
                    $data['audio_url'] = null;
                }
            } else {
                $data['audio_url'] = $request->audio_url;
            }
        }
        $data['idcategory'] = $request->idcategory;
        $data['name'] = $request->name;
        $data['isrc'] = $request->isrc;
        $data['duration'] = $request->duration;
        $data['idaudio'] = $request->idaudio;
        $data['status'] = $request->status;
        try {
            $response = $this->audioRepository->updateAudio($data, $request->idaudio);

            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
            LogRepository::logger('Audio', 'Update', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Audio', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
}
