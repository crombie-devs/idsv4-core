<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\DashboardMessage;
use App\Repositories\DashboardMessage\DashboardMessageRepository;

use App\Exceptions\DatabaseErrorException;

class DashboardMessageController extends Controller
{

    protected $dashboardMessageRepository;

    public function __construct(DashboardMessageRepository $dashboardMessageRepository)
    {
        $this->dashboardMessageRepository = $dashboardMessageRepository;
    }

    public function list()
    {
        try {
            $response = $this->dashboardMessageRepository->list();

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function get($id)
    {
        try {
            $response = $this->dashboardMessageRepository->get($id);

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

    public function latest()
    {
        try {
            $response = $this->dashboardMessageRepository->latest();

        } catch (DatabaseErrorException $e) {
            
            $response = [
                'success' => false, 
                'message' => $e->getMessage(), 
                'statusCode' => 503
            ];
        }

        return response()->json($response);
    }

}
