<?php

namespace App\Http\Controllers;

use App\Http\Requests\Departments\DepartmentCreateRequest;
use App\Http\Requests\Departments\DepartmentUpdateRequest;
use App\Http\Requests\Departments\DepartmentDeleteRequest;
use App\Repositories\Departments\DepartmentRepository;
use App\Exceptions\DatabaseErrorException;

class DepartmentController extends Controller
{
    private $departmentRepository;

    public function __construct(DepartmentRepository $departmentRepository) {
        $this->departmentRepository =$departmentRepository;
    }
    /** 
     * Creación de una nueva app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam idcustomer string required
     * @bodyParam apikey string required
     * 
     * @group App management
     */
    public function create(DepartmentCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->departmentRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp required
     * @bodyParam name string
     * @bodyParam idcustomer string
     * @bodyParam apikey string
     * @bodyParam status boolean 
     * 
     * @group App management
     */
    public function update(DepartmentUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->departmentRepository->update($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una app de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idapp required 
     * 
     * @group App management
     */
    public function delete(DepartmentDeleteRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->departmentRepository->delete($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp int required
     * @group App management
     */
    public function departments() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->departmentRepository->departments();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
   
}
