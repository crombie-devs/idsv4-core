<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\PermissionCreateRequest;
use App\Http\Requests\Permission\PermissionUpdateRequest;
use App\Repositories\Permission\PermissionRepositoryInterface;

class PermissionController extends Controller {

    private $permissionRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository) {
        $this->permissionRepository = $permissionRepository;
    }
    /** 
     * Creación de un nuevo permiso
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * 
     * @group Permission management
     */
    public function create(PermissionCreateRequest $request) {
        $response = $this->permissionRepository->create($request);
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un permiso
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idpermission required
     * @bodyParam name string 
     * 
     * @group Permission management
     */
    public function update(int $idpermission, PermissionUpdateRequest $request) {
        $response = $this->permissionRepository->update($idpermission, $request);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un permiso de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idpermission required 
     * 
     * @group Permission management
     */
    public function delete(int $idpermission) {
        $response = $this->permissionRepository->delete($idpermission);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de un permiso
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idpermission int required
     * @group Permission management
     */
    public function permission(int $idpermission) {
        $response = $this->permissionRepository->permission($idpermission);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los permisos de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Permission management
     */
    public function permissions(){
        $response = $this->permissionRepository->permissions();
        return response()->json($response, $response['statusCode']);
    }
}