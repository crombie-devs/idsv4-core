<?php

namespace App\Http\Controllers;

use App\Http\Requests\Player\PlayerCreateRequest;
use App\Http\Requests\Player\PlayerUpdateRequest;
use App\Http\Requests\Player\PlayerGetToAnalyticsRequest;
use App\Repositories\Player\PlayerRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Player\PlayerUpdatePlayLogicsRequest;
use App\Http\Requests\Player\PlayerUpdateContentsOrder;
use App\Http\Requests\Player\PlayerUpdateTagsRequest;
use App\Http\Requests\Player\PlayerUpdatePowersRequest;

use App\Repositories\Log\LogRepository;

class PlayerController extends Controller
{
    private $playerRepository;

    public function __construct(
        PlayerRepositoryInterface $playerRepository
    ) {
        $this->playerRepository = $playerRepository;
    }


    
    public function playerStatus(string $id){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->playerStatus($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    /** 
     * Creación de un nuevo player
     * Esta acción sólo la puede realizar el superadmin, admin
     * 
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * 
     * @authenticated
     * 
     * @bodyParam email string required
     * @bodyParam idsite int required
     * @bodyParam idarea int required
     * @bodyParam idlang int required
     * @bodyParam idtemplate int required
     * 
     * 
     * @group Player management
     */
    public function create(PlayerCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Player', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Player', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un player
     * Esta acción sólo la puede realizar el superadmin, el admin
     * 
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * 
     * @authenticated
     * 
     * @urlParam idplayer required
     * @bodyParam email string 
     * @bodyParam status string
     * @bodyParam idsite int
     * @bodyParam idarea int
     * @bodyParam idlang int
     * @bodyParam idtemplate int
     * 
     * @group Player management
     */
    public function update(int $idplayer, PlayerUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->update($idplayer, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Player', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Player', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un player de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin
     * 
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * 
     * @authenticated
     *
     * @urlParam idplayer required 
     * 
     * @group Player management
     */
    public function delete(int $idplayer) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->delete($idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Player', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Player', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un player
     * Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user
     * 
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * user -> Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer
     * 
     * @authenticated
     * 
     * @urlParam idplayer int required
     * @group Player management
     */
    public function player(int $idplayer) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->player($idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los players de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * 
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * user -> Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function players(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->players();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a los players para estadísticas
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * Filtrar por deleted = 0;
     * superadmin -> TODOS
     * admin -> Solo los Players con site.idcustomer = admin.idcustomer
     * user ->  Solo los Players con idsite= user.idsite && site.idcustomer = user.idcustomer
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function toAnalytics(PlayerGetToAnalyticsRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->toAnalytics($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los player de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group Player management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Actualización de los playlogic de un player
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el player con todos los nuevos playlogic.
     * 
     * @bodyParam playlogics required array idlogic integer required
     * @urlParam idplayer required int
     * 
     * Debe devolver el listado de playlogics que cumpla:
     * player.id = $request.idplayer
     * player.status = 1
     * player.deleted = 0
     * player.idplayer == playlogic.idplayer
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function updatePlaylogics(PlayerUpdatePlayLogicsRequest $request, int $idplayer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playerRepository->updatePlaylogics($request, $idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playlogic/player
     * Esta acción la puede realizar cualquier usuario
     * Devuelve player con nuevos playlogic
     * 
     * @urlParam idplayer required int
     * @urlParam idlogic required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function deletePlaylogic(int $idplayer, int $idlogic){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->deletePlaylogic($idplayer,$idlogic);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los player order de contents
     * Esta acción la puede realizar superadmin y admin
     * Debe devolver el nuevo content order del player
     * 
     * @bodyParam contents required array idcontent integer required
     * @urlParam idplayer required int
     * @urlParam idcategory required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function updateContentsOrder(PlayerUpdateContentsOrder $request, int $idplayer, int $idcategory){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playerRepository->updateContentsOrder($request, $idplayer, $idcategory);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los tag de un player
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el player con todos los nuevos tag.
     * 
     * @bodyParam tags required array idtag integer required
     * @urlParam idplayer required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function updateTags(PlayerUpdateTagsRequest $request, int $idplayer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playerRepository->updateTags($request, $idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación tag/player
     * Esta acción la puede realizar cualquier usuario
     * Devuelve player con nuevos tag
     * 
     * @urlParam idplayer required int
     * @urlParam idtag required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function deleteTag(int $idplayer, int $idtag){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->deleteTag($idplayer,$idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los power de un player
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el player con todos los nuevos power.
     * 
     * @bodyParam powers required array idpower integer required
     * @urlParam idplayer required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function updatePowers(PlayerUpdatePowersRequest $request, int $idplayer){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playerRepository->updatePowers($request, $idplayer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación power/player
     * Esta acción la puede realizar cualquier usuario
     * Devuelve player con nuevos power
     * 
     * @urlParam idplayer required int
     * @urlParam idpower required int
     * 
     * @authenticated
     * 
     * @group Player management
     */
    public function deletePower(int $idplayer, int $idpower){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playerRepository->deletePower($idplayer,$idpower);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

}
