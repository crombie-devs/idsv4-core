<?php

namespace App\Http\Controllers;

use App\Http\Requests\Responsibles\ResponsibleCreateRequest;
use App\Http\Requests\Responsibles\ResponsibleUpdateRequest;
use App\Http\Requests\Responsibles\ResponsibleDeleteRequest;
use App\Repositories\Responsibles\ResponsibleRepository;
use App\Exceptions\DatabaseErrorException;

class ResponsibleController extends Controller
{
    private $responsibleRepository;

    public function __construct(ResponsibleRepository $responsibleRepository) {
        $this->responsibleRepository =$responsibleRepository;
    }
    /** 
     * Creación de una nueva app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam idcustomer string required
     * @bodyParam apikey string required
     * 
     * @group App management
     */
    public function create(ResponsibleCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->responsibleRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp required
     * @bodyParam name string
     * @bodyParam idcustomer string
     * @bodyParam apikey string
     * @bodyParam status boolean 
     * 
     * @group App management
     */
    public function update(ResponsibleUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->responsibleRepository->update($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una app de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idapp required 
     * 
     * @group App management
     */
    public function delete(ResponsibleDeleteRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->responsibleRepository->delete($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp int required
     * @group App management
     */
    public function responsibles() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->responsibleRepository->responsibles();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
   
}
