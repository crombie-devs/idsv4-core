<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayLogic\PlayLogicCreateRequest;
use App\Http\Requests\PlayLogic\PlayLogicUpdateRequest;
use App\Repositories\PlayLogic\PlayLogicRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use Illuminate\Http\Request;

use App\Repositories\Log\LogRepository;

class PlayLogicController extends Controller
{
    private $PlayLogicRepository;

    public function __construct(
        PlayLogicRepositoryInterface $PlayLogicRepository
    ) {
        $this->PlayLogicRepository = $PlayLogicRepository;
    }
    /** 
     * Creación de un nuevo playlogic
     * Esta acción la puede realizar el superadmin, admin
     * Los usuarios con idcustomer, solo pueden crear "playlogics" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer required
     * @bodyParam idarea integer required
     * @bodyParam idtype integer required
     * @bodyParam idcategory integer
     * @bodyParam order integer 
     * 
     * @group PlayLogic management
     */
    public function create(PlayLogicCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayLogic', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayLogic', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un playlogic
     * Esta acción sólo la puede realizar el superadmin, admin
     * Los usuarios con idcustomer, solo pueden editar "playlogics" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer required
     * @bodyParam idarea integer required
     * @bodyParam idtype integer required
     * @bodyParam idcategory integer
     * @bodyParam order integer 
     * 
     * @group PlayLogic management
     */
    public function update(int $idplaylogic, PlayLogicUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->update($idplaylogic, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayLogic', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayLogic', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un playlogic de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin
     * Los usuarios con idcustomer, solo pueden eliminar "playlogics" con su mismo idcustomer.
     * 
     * @authenticated
     *
     * @urlParam idplaylogic required 
     * 
     * @group PlayLogic management
     */
    public function delete(int $idplaylogic) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->delete($idplaylogic);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayLogic', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayLogic', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un playlogic
     * Esta acción sólo la puede realizar el superadmin, admin  y user
     * Los usuarios con idcustomer, solo pueden visualizar "playlogics" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam idplaylogic int required
     * 
     * @group PlayLogic management
     */
    public function playlogic(int $idplaylogic) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->playlogic($idplaylogic);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los playlogics de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * @authenticated
     * 
     * @group PlayLogic management
     */
    public function playlogics(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->playlogics();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a todos los playlogics de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group PlayLogic management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->PlayLogicRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    
}
