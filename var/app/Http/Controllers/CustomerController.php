<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\CustomerDeleteRequest;
use App\Http\Requests\Customer\CustomerCreateRequest;
use App\Http\Requests\Customer\CustomerUpdateRequest;
use App\Repositories\Customer\CustomerRepositoryInterface;
use App\Repositories\Customer\CustomerRepository;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;
use App\Http\Requests\Customer\CustomerUpdateFormatsRequest;

use App\Repositories\Log\LogRepository;

class CustomerController extends Controller
{
    protected $customerRepository;
    use ImageUpload;

    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function customerStatus(string $id)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->customerRepository->customerStatus($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /** 
     * Creación de un nuevo cliente
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam email string
     * @bodyParam phone string
     * @bodyParam gaid string
     * @bodyParam image_url File
     * @bodyParam gaid string
     * @bodyParam has_sales boolean
     * @bodyParam has_influxes boolean
     * @bodyParam has_trends boolean
     * @bodyParam status boolean
     * @bodyParam deleted boolean
     * 
     * 
     * @group Customer management
     */
    public function create(CustomerCreateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['config_customer'] = [];
        if ($request->name) $data['name'] = $request->name;
        if ($request->phone) $data['phone'] = $request->phone;
        if (!empty($request->image_url)) {
            if ($request->hasFile('image_url')) {
                $data['image_url'] = $this->uploadImage($request->image_url);
                if (!$data['image_url']) {
                    $data['image_url'] = null;
                }
            }
        } else {
            $data['image_url'] = null;
        }


        if ($request->streaming) $data['streaming'] = $request->streaming;
        if (isset($request->has_sales)) $data['has_sales'] = $request->has_sales;
        if (isset($request->has_influxes)) $data['has_influxes'] = $request->has_influxes;
        if (isset($request->has_emissions)) $data['has_emissions'] = $request->has_emissions;
        if (isset($request->has_trends)) $data['has_trends'] = $request->has_trends;
        if (isset($request->status)) $data['status'] = $request->status;
        if (isset($request->deleted)) $data['deleted'] = $request->deleted;
        if (isset($request->influxes_type)) $data['influxes_type'] = $request->influxes_type;
        if (isset($request->has_audioasync)) $data['has_audioasync'] = $request->has_audioasync;
        if (isset($request->has_external_cms)) $data['config_customer']['has_external_cms'] = $request->has_external_cms;
        if (isset($request->has_algorithms)) $data['config_customer']['has_algorithms'] = $request->has_algorithms;
        if (isset($request->category_level)) $data['category_level'] = $request->category_level;
        if (isset($request->max_file_size)) $data['max_file_size'] = $request->max_file_size;


        $data['langs'] = json_decode($request->langs);
        try {
            $response = $this->customerRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Customer', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un cliente
     * Esta acción sólo la puede realizar el superadmin y el admin del mismo cliente
     * 
     * @authenticated
     * 
     * @urlParam idcustomer required
     * @bodyParam name string
     * @bodyParam email string
     * @bodyParam phone string
     * @bodyParam gaid string
     * @bodyParam image_url File
     * @bodyParam gaid string
     * @bodyParam has_sales boolean
     * @bodyParam has_influxes boolean
     * @bodyParam status boolean
     * @bodyParam deleted boolean
     * 
     * @group Customer management
     */
    public function update(int $idcustomer, CustomerUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        $data['config_customer'] = [];
        if ($request->name) $data['name'] = $request->name;
        if ($request->phone) $data['phone'] = $request->phone;
        if ($request->streaming) $data['streaming'] = $request->streaming;
        if (isset($request->has_sales)) $data['has_sales'] = $request->has_sales;
        if (isset($request->has_influxes)) $data['has_influxes'] = $request->has_influxes;
        if (isset($request->has_emissions)) $data['has_emissions'] = $request->has_emissions;
        if (isset($request->has_audioasync)) $data['has_audioasync'] = $request->has_audioasync;
        if (isset($request->has_external_cms)) $data['config_customer']['has_external_cms'] = $request->has_external_cms;
        if (isset($request->has_algorithms)) $data['config_customer']['has_algorithms'] = $request->has_algorithms;
        if (isset($request->has_trends)) $data['has_trends'] = $request->has_trends;
        if (isset($request->influxes_type)) $data['influxes_type'] = $request->influxes_type;
        if (isset($request->category_level)) $data['category_level'] = $request->category_level;
        if (isset($request->status)) $data['status'] = $request->status;
        if (isset($request->deleted)) $data['deleted'] = $request->deleted;
        if (isset($request->max_file_size)) $data['max_file_size'] = $request->max_file_size;


        $data['langs'] = json_decode($request->langs);

        if (!empty($request->image_url)) {
            if ($request->hasFile('image_url')) {
                $data['image_url'] = $this->uploadImage($request->image_url);
                if (!$data['image_url']) {
                    $data['image_url'] = null;
                }
            }
        }

        try {
            $response = $this->customerRepository->update($idcustomer, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Customer', 'Update', false, $request, $response);
        }


        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un cliente de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idcustomer required 
     * 
     * @group Customer management
     */
    public function delete(int $idcustomer)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->delete($idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Customer', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Customer', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de un cliente
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idcustomer, solo pueden visualizar "customers" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam idcustomer int required
     * 
     * @group Customer management
     */
    public function customer(int $idcustomer)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->customer($idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los clientes de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Customer management
     */
    public function customers()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->customers();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function customerDelete(CustomerDeleteRequest $request)
    {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->customerDelete($request->idcustomers);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los customers de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el listado de todos los customers filtrados por: status = 1 delete = 0 idcustomer de usuario (solo si usuario != superadmin)
     * 
     * 
     * @group Customer management
     */
    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }



    public function uploadImage($image)
    {
        try {
            return $this->CustomerImageUpload($image);
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     * Borra la  relacion en customer_format
     * Esta acción la puede realizar cualquier usuario
     * Devuelve customer con sus formats
     * 
     * @urlParam idcustomer required int
     * @urlParam idformat required int
     * 
     * @authenticated
     * 
     * @group Customer management
     */
    public function deleteCustomerFormat(int $idcustomer, int $idformat)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->deleteCustomerFormat($idcustomer, $idformat);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los format de un customer
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el customer con todos los nuevos format.
     * 
     * @bodyParam formats required array idformat integer required
     * @urlParam idcustomer required int
     * 
     * @authenticated
     * 
     * @group Customer management
     */
    public function updateFormats(CustomerUpdateFormatsRequest $request, int $idcustomer)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try {
            $response = $this->customerRepository->updateFormats($request, $idcustomer);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
