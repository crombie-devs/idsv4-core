<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\RoleCreateRequest;
use App\Http\Requests\Role\RoleUpdateRequest;
use App\Repositories\Role\RoleRepositoryInterface;


class RoleController extends Controller {

    public function __construct(RoleRepositoryInterface $roleRepository) {
        $this->roleRepository = $roleRepository;
    }
    /** 
     * Creación de un nuevo rol
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * 
     * @group Role management
     */
    public function create(RoleCreateRequest $request) {
        $response = $this->roleRepository->create($request);
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un rol
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idrole required
     * @bodyParam name string 
     * 
     * @group Role management
     */
    public function update(int $idrole, RoleUpdateRequest $request) {
        $response = $this->roleRepository->update($idrole, $request);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un rol de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idrole required 
     * 
     * @group Role management
     */
    public function delete(int $idrole) {
        $response = $this->roleRepository->delete($idrole);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de un rol
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idrole int required
     * @group Role management
     */
    public function role(int $idrole) {
        $response = $this->roleRepository->role($idrole);
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los roles de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Role management
     */
    public function roles(){
        $response = $this->roleRepository->roles();
        return response()->json($response, $response['statusCode']);
    }
}