<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayCircuit\PlayCircuitCreateRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateTagsRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateCountriesRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateProvincesRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateCitiesRequest;
use App\Repositories\PlayCircuit\PlayCircuitRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateSitesRequest;
use App\Http\Requests\PlayCircuit\PlayCircuitUpdateLangsRequest;
use App\Traits\ImageUpload;

use App\Repositories\Log\LogRepository;

class PlayCircuitController extends Controller
{
    private $playCircuitRepository;
    use ImageUpload;

    public function __construct(
        PlayCircuitRepositoryInterface $playCircuitRepository
    ) {
        $this->playCircuitRepository = $playCircuitRepository;
    }

    /** 
     * Creación de un nuevo playcircuit
     * Esta acción la puede realizar superadmin, (admin y user para su customer)
     * 
     * @authenticated
     * 
     * @bodyParam name required string
     * @bodyParam image File 
     * @bodyParam idcustomer required int
     * 
     * @group PlayCircuit management
     */
    public function create(PlayCircuitCreateRequest $request) { 

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data=[];
        $data['idcustomer'] = $request->idcustomer;

        if($request->name) $data['name'] = $request->name;
        if($request->status) $data['status'] = $request->status;
        
        if($request->image){
            $data['image_url'] = $this->uploadImage($request->image);
        }

        try{
            $response = $this->playCircuitRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayCircuit', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayCircuit', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un playcircuit
     * Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)
     * 
     * @authenticated
     * 
     * @urlParam idcircuit required
     * @bodyParam image File 
     * @bodyParam status string
     * 
     * @group PlayCircuit management
     */
    public function update(int $idcircuit, PlayCircuitUpdateRequest $request) {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
         
        $data['idcustomer'] = $request->idcustomer;

        if($request->name) $data['name'] = $request->name;
        if($request->status) $data['status'] = $request->status;
        $data['image_url'] ="";
        if($request->image){
            $data['image_url'] = $this->uploadImage($request->image);
        }
        try{
            $response = $this->playCircuitRepository->update($idcircuit, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayCircuit', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayCircuit', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un playcircuit de la base de datos
     * Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)
     * 
     * @authenticated
     *
     * @urlParam idcircuit required 
     * 
     * @group PlayCircuit management
     */
    public function delete(int $idcircuit) {

        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->delete($idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('PlayCircuit', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('PlayCircuit', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un playcircuit
     * Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)
     * 
     * @authenticated
     * 
     * @urlParam idcircuit int required
     * @group PlayCircuit management
     */
    public function playcircuit(int $idcircuit) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->playcircuit($idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los playcircuits de la base de datos
     * Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function playcircuits(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->playcircuits();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a los playcircuits con status 1 y deleted 0 de la base de datos
     * Esta acción sólo la puede realizar el superadmin, (admin y user para su customer)
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los tags de un circuito
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playcircuit con los nuevos tags.
     * 
     * @bodyParam tags required array idtag integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateTags(PlayCircuitUpdateTagsRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateTags($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playcircuit/tag
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos tags
     * 
     * @urlParam idcircuit required int
     * @urlParam idtag required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function deleteTag(int $idcircuit, int $idtag){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteTag($idcircuit,$idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    
    /**
     * Actualización de los countries de un circuito
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playcircuit con los nuevos countries.
     * 
     * @bodyParam countries required array idcountry integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateCountries(PlayCircuitUpdateCountriesRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateCountries($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playcircuit/country
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos countries
     * 
     * @urlParam idcircuit required int
     * @urlParam idcountry required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function deleteCountry(int $idcircuit, int $idcountry){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteCountry($idcircuit,$idcountry);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Actualización de las provinces de un circuito
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playcircuit con las nuevas provinces.
     * 
     * @bodyParam provinces required array idprovince integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateProvinces(PlayCircuitUpdateProvincesRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateProvinces($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playcircuit/province
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos provinces
     * 
     * @urlParam idcircuit required int
     * @urlParam idprovince required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function deleteProvince(int $idcircuit, int $idtag){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteProvince($idcircuit,$idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Actualización de los cities de un circuito
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playcircuit con los nuevos cities.
     * 
     * @bodyParam cities required array idcity integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateCities(PlayCircuitUpdateCitiesRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateCities($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación playcircuit/city
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos cities
     * 
     * @urlParam idcircuit required int
     * @urlParam idcity required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function deleteCity(int $idcircuit, int $idcity){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteCity($idcircuit,$idcity);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los sites de un circuito
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el playcircuit con los nuevos sites.
     * 
     * @bodyParam sites required array idcircuit integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateSites(PlayCircuitUpdateSitesRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateSites($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

 
    /**
     * Borra una relación playcircuit/site
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos sites
     * 
     * @urlParam idcircuit required int
     * @urlParam idsite required int
     * 
     * @group PlayCircuit management
     */
    public function deleteSite(int $idcircuit, int $idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteSite($idcircuit,$idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


     /**
     * Actualización de los langs de un circuito
     * Esta acción la puede realizar superadmin y admin
     * Debe devolver el playcircuit con los nuevos langs.
     * 
     * @bodyParam langs required array idcircuit integer required
     * 
     * @urlParam idcircuit required int
     * 
     * @authenticated
     * 
     * @group PlayCircuit management
     */
    public function updateLangs(PlayCircuitUpdateLangsRequest $request, int $idcircuit){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->playCircuitRepository->updateLangs($request, $idcircuit);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

 
    /**
     * Borra una relación playcircuit/lang
     * Esta acción la puede realizar cualquier usuario
     * Devuelve circuitos con nuevos superadmin y admin
     * 
     * @urlParam idcircuit required int
     * @urlParam idlang required int
     * 
     * @group PlayCircuit management
     */
    public function deleteLang(int $idcircuit, int $idlang){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->playCircuitRepository->deleteLang($idcircuit,$idlang);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    public function uploadImage($image){
        try {   
            return $this->PlayCircuitImageUpload($image);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }

}
