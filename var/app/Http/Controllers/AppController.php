<?php

namespace App\Http\Controllers;

use App\Http\Requests\App\AppCreateRequest;
use App\Http\Requests\App\AppUpdateRequest;
use App\Repositories\App\AppRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class AppController extends Controller
{
    private $appRepository;

    public function __construct(AppRepositoryInterface $appRepository) {
        $this->appRepository = $appRepository;
    }
    /** 
     * Creación de una nueva app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam idcustomer string required
     * @bodyParam apikey string required
     * 
     * @group App management
     */
    public function create(AppCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->appRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp required
     * @bodyParam name string
     * @bodyParam idcustomer string
     * @bodyParam apikey string
     * @bodyParam status boolean 
     * 
     * @group App management
     */
    public function update(int $idapp, AppUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->appRepository->update($idapp, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una app de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idapp required 
     * 
     * @group App management
     */
    public function delete(int $idapp) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->appRepository->delete($idapp);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una app
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idapp int required
     * @group App management
     */
    public function app(int $idapp) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->appRepository->app($idapp);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las apps de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group App management
     */
    public function apps(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->appRepository->apps();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a las apps de customer de la base de datos
     * Esta acción la pueden realizar el admin y el superadmin
     * 
     * @authenticated
     * 
     * @group App management
     */
    public function appsToSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->appRepository->appsToSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
