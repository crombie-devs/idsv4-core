<?php

namespace App\Http\Controllers;

use App\Http\Requests\Lang\LangCreateRequest;
use App\Http\Requests\Lang\LangUpdateRequest;
use App\Repositories\Lang\LangRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;

use App\Repositories\Log\LogRepository;

class LangController extends Controller
{
    private $langRepository;
    use ImageUpload;

    public function __construct(
        LangRepositoryInterface $langRepository
    ) {
        $this->langRepository = $langRepository;
    }
    /** 
     * Creación de un nuevo lang
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam image_url File required
     * 
     * @group Lang management
     */
    public function create(LangCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];
        if($request->name) $data['name'] = $request->name;

        if($request->image_url){
            $data['image_url'] = $this->uploadImage($request->image_url);
        }

        if(isset($data['image_url'])){
            try{
                $response = $this->langRepository->create($data);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
                LogRepository::logger('Lang', 'Create', true, $request, $response);
            }catch(DatabaseErrorException $e){
                $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
                LogRepository::logger('Lang', 'Create', false, $request, $response);
            }
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un lang
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam idlang string
     * @bodyParam name string
     * @bodyParam image_url File
     * 
     * @group Lang management
     */
    public function update(int $idlang, LangUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = [];

        if($request->name) $data['name'] = $request->name;
        if($request->image_url){
            $data['image_url'] = $this->uploadImage($request->image);
        }

        try{
            $response = $this->langRepository->update($idlang, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Lang', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Lang', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un lang de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idlang required 
     * 
     * @group Lang management
     */
    public function delete(int $idlang) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->langRepository->delete($idlang);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Lang', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Lang', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un lang
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idlang int required
     * @group Lang management
     */
    public function lang(int $idlang) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->langRepository->lang($idlang);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los langs de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Lang management
     */
    public function langs(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->langRepository->langs();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los langs de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group Lang management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->langRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los langs de los players asociados al usuario
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group PlayArea management
     */
    public function toUser(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->langRepository->toUser();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function uploadImage($image){
        try {   
            return $this->LangImageUpload($image);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }
}
