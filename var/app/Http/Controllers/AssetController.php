<?php

namespace App\Http\Controllers;

use App\Http\Requests\Asset\AssetCreateRequest;
use App\Http\Requests\Asset\AssetUpdateRequest;
use App\Repositories\Asset\AssetRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;
use App\Traits\UploadGoogleBucket;
use Illuminate\Http\Request;

use App\Repositories\Log\LogRepository;

class AssetController extends Controller
{
    private $AssetRepository;
    use ImageUpload;
    use UploadGoogleBucket;

    public function __construct(AssetRepositoryInterface $AssetRepository)
    {
        $this->AssetRepository = $AssetRepository;
    }
    /** 
     * Creación de un nuevo asset
     * Esta acción la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden crear "assets" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden crear "assets" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer
     * @bodyParam idsite integer
     * @bodyParam idtype integer required
     * @bodyParam name string required
     * @bodyParam asset File
     * @bodyParam duration integer
     * @bodyParam idformat integer required
     * @bodyParam size integer
     * @bodyParam idlang integer required
     * @bodyParam default integer
     * @bodyParam has_audio integer 
     * @bodyParam status integer 
     * @bodyParam deleted integer 
     * @bodyParam asset_url string
     *  
     * @group Asset management
     */
    public function create(AssetCreateRequest $request)
    {


        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = $request->toArray();
        if ($request->asset) {
            if ($request->idtype == 1) {
                $imageData = $this->ImageBucketUpload($request->asset);
                $data['asset_url'] = $imageData[0];
                $data['cover_url'] = $imageData[1];
                $data['size'] = $imageData[2];

                if (!$data['asset_url']) {
                    $data['asset_url'] = null;
                    $data['cover_url'] = null;
                    $data['size'] = 0;
                }
            } else if ($request->idtype == 2) {


                $videoData = $this->videoBucketUpload($request->asset, $request->delay);


                $data['asset_url'] = $videoData[0];
                $data['cover_url'] = $videoData[1];
                $data['size'] = $videoData[2];

                if (!$data['asset_url']) {
                    $data['asset_url'] = null;
                    $data['cover_url'] = null;
                    $data['size'] = 0;
                }
            } else if ($request->idtype == 5) {
                $audioData = $this->audioBucketUpload($request->asset);
                $data['asset_url'] = $audioData[0];
                $data['cover_url'] = $audioData[1];
                $data['size'] = $audioData[2];

                if (!$data['asset_url']) {
                    $data['asset_url'] = null;
                    $data['cover_url'] = null;
                    $data['size'] = 0;
                }
            }
        } else if (isset($request->asset_url) && !empty($request->asset_url)) {
            $data['asset_url'] = $request->asset_url;
            // $data['cover_url'] =  $this->thumbBucketUrl($data['asset_url'], $data['delay']);
            $data['cover_url'] =  $this->thumbUrl($data['asset_url'], $data['delay']);
            $data['size'] = 0;
        } else {
            $data['asset_url'] = null;
            $data['cover_url'] = null;
        }

        try {
            $response = $this->AssetRepository->create($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Asset', 'Create', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un asset
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden editar "assets" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden editar "assets" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer integer
     * @bodyParam idsite integer
     * @bodyParam idtype integer required
     * @bodyParam name string required
     * @bodyParam asset File
     * @bodyParam duration integer
     * @bodyParam idformat integer required
     * @bodyParam size integer 
     * @bodyParam idlang integer required
     * @bodyParam default integer 
     * @bodyParam has_audio integer
     * @bodyParam status integer 
     * @bodyParam deleted integer 
     * @bodyParam asset_url string
     * 
     * @group Asset management
     */
    public function update(int $idasset, AssetUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        $data = $request->toArray();
        try {


            $asset  = $this->AssetRepository->asset($idasset);

            if ($request->asset) {
                if ($request->idtype == 1) {
                    $imageData =  $this->ImageBucketUpload($request->asset);
                    $data['asset_url'] = $imageData[0];
                    $data['cover_url'] = $imageData[1];
                    $data['size'] = $imageData[2];

                    if (!$data['asset_url']) {
                        $data['asset_url'] = null;
                        $data['cover_url'] = null;
                        $data['size'] = 0;
                    }
                } else if ($request->idtype == 2) {
                    $videoData = $this->videoBucketUpload($request->asset, $request->delay);
                    $data['asset_url'] = $videoData[0];
                    $data['cover_url'] = $videoData[1];
                    $data['size'] = $videoData[2];
                    if (!$data['asset_url']) {
                        $data['asset_url'] = null;
                        $data['cover_url'] = null;
                        $data['size'] = 0;
                    }
                } else if ($request->idtype == 5) {
                    $audioData = $this->audioBucketUpload($request->asset);
                    $data['asset_url'] = $audioData[0];
                    $data['cover_url'] = $audioData[1];
                    $data['size'] = $audioData[2];

                    if (!$data['asset_url']) {
                        $data['asset_url'] = null;
                        $data['cover_url'] = null;
                        $data['size'] = 0;
                    }
                }


                if ($data['size']) {
                    $this->removeOldFiles($asset->asset_url, $asset->cover_url);
                }
            } else if (isset($request->asset_url) && !empty($request->asset_url) && ($request->idtype == 3 || $request->idtype == 4)) {
                $data['asset_url'] = $request->asset_url;
                // $data['cover_url'] =  $this->thumbBucketUrl($data['asset_url'], $data['delay']);
                $data['cover_url'] =  $this->thumbUrl($data['asset_url'], $data['delay']);
                $data['size'] = 0;
            } else if (isset($request->asset_url) && !empty($request->asset_url) && ($request->idtype == 2)) {
                $videoData = $this->thumbVideo($request->name, $request->asset_url, $request->delay);
                $data['cover_url'] = $videoData[0];
            }
            $response = $this->AssetRepository->update($idasset, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Asset', 'Update', false, $request, $response);
        }

        return response()->json($response, $response['statusCode']);
    }

    private function removeOldFiles($path, $tpath)
    {
        $path  = explode('https://storage.googleapis.com/ladorian-assets-store-bucket', $path);
        $tpath = explode('https://storage.googleapis.com/ladorian-assets-store-bucket', $tpath);
        $this->removeBucketFile($path[1]);
        $this->removeBucketFile($tpath[1]);
    }


    /**
     * Borrado de un asset de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin y user
     * Los usuarios con idsite, solo pueden eliminar "assets" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden eliminar "assets" con su mismo idcustomer.
     * 
     * @authenticated
     *
     * @urlParam idasset required 
     * 
     * @group Asset management
     */
    public function delete(int $idasset)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->AssetRepository->delete($idasset);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Asset', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Asset', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un asset
     * Esta acción sólo la puede realizar el superadmin, admin  y user
     * Los usuarios con idsite, solo pueden visualizar "assets" con su mismo idsite.
     * Los usuarios con idcustomer, solo pueden visualizar "assets" con su mismo idcustomer.
     * 
     * @authenticated
     * 
     * @urlParam idasset int required
     * @group Asset management
     */
    public function asset(int $idasset)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->AssetRepository->asset($idasset);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos los assets de la base de datos
     * Esta acción sólo la puede realizar el superadmin, admin, user
     * delete = 0
     * idsite de usuario (solo si usuario tiene idsite)
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * @authenticated
     * 
     * @group Asset management
     */
    public function assets()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->AssetRepository->assets();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a todos los assets de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el listado de todos los assets filtrados por: status = 1 delete = 0
     * idsite de usuario (solo si usuario tiene idsite)
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group Asset management
     */
    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->AssetRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function uploadImage($file)
    {
        try {

            return $this->AssetImageUpload($file);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }

    public function uploadVideo($file, $time)
    {
        try {
            return $this->AssetVideoUpload($file, $time);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda el video', 'statusCode' => 503];
        }
    }

    public function uploadAudio($file)
    {
        try {
            return $this->AssetAudioUpload($file);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda el audio', 'statusCode' => 503];
        }
    }

    /**
     * Debe eliminar:registro en asset (idasset) contents_has_assets (idasset)
     * Esta acción la puede realizar cualquier usuario
     *
     * 
     * @urlParam idasset required int
     * 
     * @authenticated
     * 
     * @group Asset management
     */
    public function deleteAssetForce(int $idasset)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $asset = $this->AssetRepository->asset($idasset);

            $response = $this->AssetRepository->deleteAssetForce($idasset);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];

            if ($asset->idtype == 1)
                $this->AssetImageDelete(basename($asset->asset_url));
            else if ($asset->idtype == 2)
                $this->AssetVideoDelete(basename($asset->asset_url));
            else if ($asset->idtype == 5)
                $this->AssetAudioDelete(basename($asset->asset_url));
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
