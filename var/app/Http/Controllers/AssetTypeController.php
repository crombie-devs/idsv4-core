<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\AssetType\AssetTypeToSelectRequest;
use App\Repositories\AssetType\AssetTypeRepositoryInterface;


class AssetTypeController extends Controller
{
    protected $assetTypeRepository;

    public function __construct(AssetTypeRepositoryInterface $assetTypeRepository)
    {
        $this->assetTypeRepository = $assetTypeRepository;   
    }


    /**
     * Acceso a todos los asset type de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group AssetType management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->assetTypeRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
