<?php

namespace App\Http\Controllers;

use App\Http\Requests\Camera\CameraCreateRequest;
use App\Http\Requests\Camera\CameraUpdateRequest;
use App\Http\Requests\Camera\CameraGetToAnalyticsRequest;
use App\Repositories\Camera\CameraRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Camera\CameraUpdatePowersRequest;

use App\Repositories\Log\LogRepository;

class CameraController extends Controller
{
    private $cameraRepository;

    public function __construct(
        CameraRepositoryInterface $cameraRepository
    ) {
        $this->cameraRepository = $cameraRepository;
    }

    public function cameraStatus(string $id){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->cameraStatus($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    /** 
     * Creación de un nuevo camera
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam email string required
     * @bodyParam idsite integer required
     * @bodyParam code string
     * @bodyParam name string
     * 
     * @group Camera management
     */
    public function create(CameraCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->create($request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Camera', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Camera', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un camera
     * Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el usuario de la misma tienda
     * 
     * @authenticated
     * 
     * @urlParam idcamera required
     * @bodyParam email string 
     * @bodyParam status string
     * 
     * @group Camera management
     */
    public function update(int $idcamera, CameraUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->update($idcamera, $request->toArray());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Camera', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Camera', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un camera de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idcamera required 
     * 
     * @group Camera management
     */
    public function delete(int $idcamera) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->delete($idcamera);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Camera', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Camera', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de un camera
     * Esta acción sólo la puede realizar el superadmin, el admin del mismo cliente o el user de la misma tienda
     * 
     * @authenticated
     * 
     * @urlParam idcamera int required
     * @group Camera management
     */
    public function camera(int $idcamera) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->camera($idcamera);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los cameras de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Camera management
     */
    public function cameras(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->cameras();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a las cámaras para estadísticas
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * @group Camera management
     */
    public function toAnalytics(CameraGetToAnalyticsRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->toAnalytics($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Acceso a todos las camaras de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el listado de todos las camaras filtrados por: status = 1 delete = 0
     * 
     * 
     * @group Camera management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los power de un camera
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el camera con todos los nuevos power.
     * 
     * @bodyParam powers required array idpower integer required
     * @urlParam idcamera required int
     * 
     * @authenticated
     * 
     * @group Camera management
     */
    public function updatePowers(CameraUpdatePowersRequest $request, int $idcamera){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->cameraRepository->updatePowers($request, $idcamera);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación power/camera
     * Esta acción la puede realizar cualquier usuario
     * Devuelve camera con nuevos power
     * 
     * @urlParam idcamera required int
     * @urlParam idpower required int
     * 
     * @authenticated
     * 
     * @group Camera management
     */
    public function deletePower(int $idcamera, int $idpower){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->cameraRepository->deletePower($idcamera,$idpower);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
