<?php

namespace App\Http\Controllers;

use App\Http\Requests\Format\FormatCreateRequest;
use App\Http\Requests\Format\FormatUpdateRequest;
use App\Repositories\Format\FormatRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Traits\ImageUpload;
use App\Models\Format;
use Illuminate\Http\Request;

use App\Repositories\Log\LogRepository;

class FormatController extends Controller
{
    protected $formatRepository;
    use ImageUpload;

    public function __construct(FormatRepositoryInterface $formatRepository)
    {
        $this->formatRepository = $formatRepository;
    }


    /** 
     * Creación de un nuevo formato de player
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam image_url File required
     * @bodyParam description text
     * 
     * @group Format Player management
     */
    public function create(FormatCreateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        if ($request->name) $data['name'] = $request->name;
        if ($request->description) $data['description'] = $request->description;
        if ($request->width) $data['width'] = $request->width;
        if ($request->height) $data['height'] = $request->height;
        if ($request->broadcasting) $data['broadcasting'] = $request->broadcasting;

        if ($request->image_url) {
            $data['image_url'] = $this->uploadImage($request->image_url);
        }

        if ($request->frame_image_url) {
            $data['frame_image'] = $this->uploadImage($request->frame_image);
        }

        if (isset($data['image_url'])) {
            try {
                $response = $this->formatRepository->create($data);
                $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
                LogRepository::logger('Format', 'Create', true, $request, $response);
            } catch (DatabaseErrorException $e) {
                $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
                LogRepository::logger('Format', 'Create', false, $request, $response);
            }
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de un formato de player
     * Esta acción sólo la puede realizar el superadmin y el admin del mismo cliente
     * 
     * @authenticated
     * 
     * @urlParam idformat required 
     * @bodyParam name string
     * @bodyParam image File
     * @bodyParam description text
     * 
     * @group Format Player management
     */
    public function update(int $idformat, FormatUpdateRequest $request)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data = [];
        if ($request->name) $data['name'] = $request->name;
        if ($request->description) $data['description'] = $request->description;
        if ($request->width) $data['width'] = $request->width;
        if ($request->height) $data['height'] = $request->height;
        if ($request->broadcasting) $data['broadcasting'] = $request->broadcasting;

        if ($request->image_url) {
            if ($request->hasFile('image_url')) {
                $data['image_url'] = $this->uploadImage($request->image_url);
                if (empty($data['image_url'])) {
                    $data['image_url'] = null;
                }
            }
        }
        if ($request->frame_image) {
            $data['frame_image_url'] = $this->uploadImage($request->frame_image);
        }

        try {
            $response = $this->formatRepository->update($idformat, $data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Format', 'Update', true, $request, $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Format', 'Update', false, $request, $response);
        }


        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de un formato de player de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam idformat required 
     * 
     * @group Format Player management
     */
    public function delete(int $idformat)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->formatRepository->delete($idformat);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            LogRepository::logger('Format', 'Delete', true, $request = '-', $response);
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Format', 'Delete', false, $request = '-', $response);
        }

        return response()->json($response, $response['statusCode']);
    }

    /**
     * Aceso a los datos de un formato de player
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam idformat int required
     * @group Format Player management
     */
    public function format(int $idformat)
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->customerRepository->customer($idformat);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todos los formatos de player de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Format Player management
     */
    public function formats()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->formatRepository->formats();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }

    public function uploadImage($image)
    {
        try {
            return $this->FormatImageUpload($image);
        } catch (\Exception $e) {
            return ['success' => false, 'message' => 'No se ha podido guarda la imagen', 'statusCode' => 503];
        }
    }

    /**
     * Acceso a todos los formats de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group Format Player management
     */
    public function toSelect()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->formatRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Acceso a todos los formats de los players asociados al usuario
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group PlayArea management
     */
    public function toUser()
    {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try {
            $response = $this->formatRepository->toUser();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        } catch (DatabaseErrorException $e) {
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
