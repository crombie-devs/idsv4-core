<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Http\Requests\EmissionSale\EmissionSaleRequest;
use App\Repositories\EmissionSale\EmissionSaleRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class EmissionSaleController extends Controller
{
    private $emissionSaleRepository;

    public function __construct(EmissionSaleRepositoryInterface $emissionSaleRepository) {
        $this->emissionSaleRepository = $emissionSaleRepository;
    }
    /** 
     * Consulta datos para las gráficas de emisión y ventas
     * Esta acción la puede realizar el superadmin, admin y user
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam idcustomer string required
     * @bodyParam apikey string required
     * 
     * @group EmissionSale Consult
     */
    public function data(Request $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->emissionSaleRepository->data($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
}
