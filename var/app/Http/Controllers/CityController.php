<?php

namespace App\Http\Controllers;


use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\City\CityToSelectRequest;
use App\Repositories\City\CityRepositoryInterface;


class CityController extends Controller
{
    protected $cityRepository;

    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->cityRepository = $cityRepository;   
    }


    /**
     * Acceso a todos las cities de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * 
     * 
     * @group City management
     */
    public function toSelect(CityToSelectRequest $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        $data=[];
        if (!empty( $request->except('_token'))){
            if(isset($request['idcountry']) ){
                $data['idcountry'] = $request['idcountry'];
            }
            if(isset($request['idprovince'])){
                $data['idprovince'] = $request['idprovince'];
            }   
        }

        try{
            $response = $this->cityRepository->toSelect($data);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
