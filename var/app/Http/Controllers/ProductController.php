<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductTags\ProductTagsRequest;
use App\Http\Requests\ProductTags\ProductTagsUpdateRequest;
use App\Repositories\Product\ProductRepositoryInterface;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;   
    }
    public function getAllProductTags(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->getAllProductTags($request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getProductTags(int $idproduct) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->getProductTags($idproduct);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function updateProductTags(string $idproduct, Request $request ) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->updateProductTags($idproduct, $request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function categoriesToSelect() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->categoriesToSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function toSelect() {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function getCategoriesWithProductsWithContents(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->getCategoriesWithProductsWithContents();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function create(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->create($request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    public function update(int $id, Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->update($id, $request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    public function delete(int $id){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->delete($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function createCategory(Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->createCategory($request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    public function updateCategory(int $id, Request $request){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->updateCategory($id, $request->all());
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    public function deleteCategory(int $id){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->deleteCategory($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    public function detachFromContent(int $idproduct, int $idcontent){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->productRepository->detachFromContent($idproduct, $idcontent);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
}
