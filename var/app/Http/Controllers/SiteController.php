<?php

namespace App\Http\Controllers;

use App\Http\Requests\Site\SiteCreateRequest;
use App\Http\Requests\Site\SiteUpdateRequest;
use App\Repositories\Site\SiteRepositoryInterface;
use App\Exceptions\DatabaseErrorException;
use App\Http\Requests\Site\SiteUpdateHolidaysRequest;
use App\Http\Requests\Site\SiteUpdateTagsRequest; 

use App\Jobs\ProcessProfileDemographic;
use App\Jobs\ProcessProfileDemographicDelete;
use App\Jobs\ProcessPlayCircuits;
use App\Jobs\ProcessPlayCircuitsDelete;

use App\Repositories\Log\LogRepository;

class SiteController extends Controller
{
    private $siteRepository;

    public function __construct(
        SiteRepositoryInterface $siteRepository
    ) {
        $this->siteRepository = $siteRepository;
    }

    public function siteStatus(string $id) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->siteRepository->siteStatus($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }




    /** 
     * Creación de una nueva tienda
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam idcustomer string required
     * @bodyParam name string required
     * @bodyParam idcity string required
     * @bodyParam phone string
     * @bodyParam email string
     * @bodyParam address string
     * @bodyParam zipcode string
     * @bodyParam code string
     * @bodyParam latitud string
     * @bodyParam longitude string
     * 
     * @group Site management
     */
    public function create(SiteCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $data = $this->siteRepository->create($request);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
            ProcessProfileDemographic::dispatchAfterResponse($data->idsite, $data->idcustomer, $data->longitude, $data->latitude);
            $all =  $request->all();
            if(!empty($all['idcircuit']))
                ProcessPlayCircuits::dispatchAfterResponse($data->idsite, $all['idcircuit']);
            LogRepository::logger('Site', 'Create', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Site', 'Create', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una tienda
     * Esta acción sólo la puede realizar el superadmin o un admin del mismo idcustomer o un usuario con el mismo idsite
     * 
     * @authenticated
     * 
     * @urlParam idsite string required
     * @bodyParam idcustomer string
     * @bodyParam name string
     * @bodyParam idcity string require
     * @bodyParam phone string
     * @bodyParam email string
     * @bodyParam address string
     * @bodyParam zipcode string
     * @bodyParam code string
     * @bodyParam latitud string
     * @bodyParam longitude string
     * @bodyParam status boolean
     * 
     * @group Site management
     */
    public function update(int $idsite, SiteUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $data = $this->siteRepository->update($idsite, $request);
            $response = ['success' => true, 'data' => $data, 'statusCode' => 200];
            ProcessProfileDemographic::dispatchAfterResponse($data->idsite, $data->idcustomer, $data->longitude, $data->latitude);
            $all =  $request->all();
            if(!empty($all['idcircuit']))
                ProcessPlayCircuits::dispatchAfterResponse($data->idsite, $all['idcircuit']);
            LogRepository::logger('Site', 'Update', true, $request, $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Site', 'Update', false, $request, $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una tienda de la base de datos
     * Esta acción sólo la puede realizar el superadmin.
     * 
     * @authenticated
     *
     * @urlParam id required 
     * 
     * @group Site management
     */
    public function delete(int $idsite) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            ProcessPlayCircuitsDelete::dispatch($idsite)->onQueue('db');
            $response = $this->siteRepository->delete($idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
            ProcessProfileDemographicDelete::dispatchAfterResponse($idsite);
            LogRepository::logger('Site', 'Delete', true, $request = '-', $response);
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
            LogRepository::logger('Site', 'Delete', false, $request = '-', $response);
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a los datos de una tienda
     * Esta acción sólo la puede realizar el superadmin, el admin si es de una tienda con su mismo idcustomer o un usuario con su mismo idsite
     * 
     * @authenticated
     * 
     * @urlParam id int required
     * @group Site management
     */
    public function site(int $idsite) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->siteRepository->site($idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las tiendas de la base de datos
     * Esta acción sólo la puede realizar el superadmin, el admin accede a las tiendas de is cliente y el user a las tiendas de si tienda
     * 
     * @authenticated
     * 
     * @group Site management
     */
    public function sites(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->siteRepository->sites();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

     /**
     * Acceso a todos los site de la base de datos desde un select
     * Esta acción la puede realizar cualquier usuario
     * idcustomer de usuario (solo si usuario tiene idcustomer)
     * 
     * 
     * @group Site management
     */
    public function toSelect(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->siteRepository->toSelect();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }


    /**
     * Actualización de los holidays de un site
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el site con todos los nuevos holidays.
     * 
     * @bodyParam site_holidays required array date integer required
     * 
     * @urlParam idsite required int
     * 
     * @authenticated
     * 
     * @group Site management
     */
    public function updateHolidays(SiteUpdateHolidaysRequest $request, int $idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->siteRepository->updateHolidays($request, $idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación holiday/site
     * Esta acción la puede realizar cualquier usuario
     * Devuelve site con nuevos holidays
     * 
     * @urlParam idsite required int
     * @urlParam idholiday required int
     * 
     * @authenticated
     * 
     * @group Site management
     */
    public function deleteHoliday(int $idsite, int $idholiday){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->siteRepository->deleteHoliday($idsite,$idholiday);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Actualización de los tags de un site
     * Esta acción la puede realizar cualquier usuario
     * Debe devolver el site con todos los nuevos tags.
     * 
     * @bodyParam tags required array tag integer required
     * 
     * @urlParam idsite required int
     * 
     * @authenticated
     * 
     * @group Site management
     */
    public function updateTags(SiteUpdateTagsRequest $request, int $idsite){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->siteRepository->updateTags($request, $idsite);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }

    /**
     * Borra una relación tag/site
     * Esta acción la puede realizar cualquier usuario
     * Devuelve site con nuevos tags
     * 
     * @urlParam idsite required int
     * @urlParam idtag required int
     * 
     * @authenticated
     * 
     * @group Site management
     */
    public function deleteTag(int $idsite, int $idtag){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->siteRepository->deleteTag($idsite,$idtag);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    
}
