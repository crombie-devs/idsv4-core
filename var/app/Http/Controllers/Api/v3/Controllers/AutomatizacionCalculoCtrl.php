<?php 
namespace App\Http\Controllers\Api\v3\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\v3\Controllers\BaseCtrl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use Carbon\Carbon;

use App\Models\NTLMSoapClient;
use App\Models\NTLMStream;
use App\Models\Content;
use App\Models\Customer;
use App\Models\Site;
use App\Models\DifendifPythonImpacto;
use App\Models\User;

use GuzzleHttp\Client;
use URL;
use DateTime;
use StdClass;
use Storage;
use File;
use Mail;
use DirectoryIterator;
use Artisan;
use App\Traits\TicketWs;
use App\Traits\PythonWs;
use Illuminate\Support\Facades\Auth;

class AutomatizacionCalculoCtrl extends BaseCtrl{
    use TicketWs;
    use PythonWs;

    public function saveDataProccess($num = null){
		ini_set('memory_limit', '64380M');
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '-1');		
        ini_set('upload_max_filesize', '6400M');
        ini_set('post_max_size', '6400M');
        $customers = $this->getCustomers();
        $modContent = new Content();

        $customers_to_generate = [75];
        $minDate="2019-01-01";
        $maxDate=date('2021-11-17');
        
        $minDate_="2021-01-01";
        $maxDate_="2021-11-17";
       
        $a_dates = ['minDate_' => $minDate_, 'maxDate_' => $maxDate_];
        foreach ($customers as $key => $cust) {
			if(count($customers_to_generate) === 0 || in_array($cust->idcustomer, $customers_to_generate)){
                echo "customer ->" . $cust->idcustomer . PHP_EOL;
				$a_contents = $modContent->getContentsAndProdsFromCustomer($cust->idcustomer, $minDate_, $maxDate_); 
				if($num){
                    $portion = ceil(count($a_contents)/10);
                    $c_ini = intval($num*$portion);
                    $c_fin = intval($c_ini + $portion)-1;
                    $tmp_a_content = [];
					echo "inicio y fin ->" . $c_ini . " - " . $c_fin . PHP_EOL;
                    for($i = $c_ini; $i < $c_fin; $i++) { 
                        if(isset($a_contents[$i])){
                            array_push( $tmp_a_content, $a_contents[$i] );
							echo "revisando contenidos->  " . $i . " - " . $a_contents[$i] . PHP_EOL;
                        }
                    }
                    unset($a_contents);
                    $a_contents = $tmp_a_content;
                }
                echo "num y contenidos ->" . $num . " - " . count($a_contents) . PHP_EOL;
                if(!empty($a_contents)){
                    foreach ($a_contents as $key => $content) {
                            try{							
                                $archivo = "difendif-" . $content->idcontent . "-" . $content->idcustomer . ".json"; 
                                if(!Storage::disk('public')->exists("filesautocalculo/".$archivo)){
                                    echo PHP_EOL . PHP_EOL . "INICIO->" . $key . ' - ' . $archivo . "__" . $content->idcontent . "__" . $content->dates[0]->date_on . "__" . $content->dates[0]->date_off . ' - ' . date("h:i:s") . PHP_EOL;    
                                    echo "Antes de Emisiones ...." . PHP_EOL;
                                    $data = [];
                                    $a_emisiones_content = $this->getEmisionesByContent($content->idcontent,$content->idcustomer,$minDate,$maxDate);
                                    echo "Despues de Emisiones....".PHP_EOL;
                                    if($a_emisiones_content['success'] && !empty($a_emisiones_content['data'])){
                                        foreach($a_emisiones_content['data'] as $emision){
                                            $emision['idcontent'] = $content->idcontent;
                                            $data[] = $emision;
                                        }
                                    }
                                    $json = $this->AtomatizacionCalculo($a_dates,$data,$content);
                                    echo "Voy a guardar el archivo.".PHP_EOL;
                                    Storage::disk('public')->put("filesautocalculo/".$archivo,json_encode($json));
                                    echo "FIN->". $content->idcontent . ' - ' . date("h:i:s") . PHP_EOL;
                                }
                            }catch(Exception $e){
                                "Error en campaña: " . $content->idcontent . " Error: " . $e->getMessage();
                            }
                    }
                }
                echo "fin del customer...:".$cust->idcustomer. PHP_EOL;
            } // del if de customers
        } // del foreach de customers
    }  // de la function

    public function saveDataProccessDaily(){
        $customers = $this->getCustomers();
        $modContent = new Content();
        $customers_to_generate = [61];

        $fecha = date('Y-m-d');
        //Se seleccionan los ficheros de antes de ayer
        $nuevafecha = strtotime('-2 day', strtotime($fecha));
        $nuevafecha = date('Y-m-d',$nuevafecha );
        $to = strtotime('-1 day',strtotime($fecha));
        $minDate_= $nuevafecha;
        $maxDate_= date('Y-m-d', $to);
        $a_dates = ['minDate_' => $minDate_, 'maxDate_' => $maxDate_];
        foreach ($customers as $key => $cust) {
			if(count($customers_to_generate) === 0 || in_array($cust->idcustomer, $customers_to_generate)){
                $a_contents = $modContent->getContentsAndProdsFromCustomer($cust->idcustomer,$minDate_, $maxDate_);
                if(!empty($a_contents)){
                    foreach ($a_contents as $key => $content) {
                        try{
                            $archivo= "difendif-".$content->idcontent."-".$content->idcustomer.".json";    
                            echo "INICIO->".$archivo. ' ' . $key . ' ' . $content->idcontent. ' ' .date("h:i:s"). PHP_EOL;
                            $a_emisiones_content = $this->getEmisionesByContent($content->idcontent,$content->idcustomer,$minDate_,$maxDate_);
                            if($a_emisiones_content['success'] && !empty($a_emisiones_content['data'])){
                                if(Storage::disk('public')->exists("filesautocalculo/".$archivo)){   
                                    $data_cont = Storage::disk('public')->get("filesautocalculo/".$archivo);
                                    $data_cont =json_decode($data_cont);
                                    $date = date('Y-m-d', strtotime('+1 day', strtotime($data_cont[count($data_cont)-1][0]->fecha)));
                                    $to = date('Y-m-d', strtotime('-1 day', strtotime($fecha)));
                                    $dates=[];
                                    $fecha_ini_tope = $date." 00:00:00";   
                                    $fecha_tope = $to." 23:59:59";
                                    for($j = $fecha_ini_tope;$j <= $fecha_tope;$j = date("Y-m-d", strtotime($j ."+ 1 days"))){
                                        $encontrado = false;
                                        for($i = 0;$i < count($data_cont);$i++){    
                                            if(strtotime($data_cont[$i][0]->fecha) == strtotime ('-1 year' , strtotime ($j))){
                                                $encontrado=true;
                                                break;
                                            }       
                                        }
                                        if(!$encontrado)
                                            $dates[]=date("Y-m-d",strtotime ('-1 year' , strtotime ($j)));
                                    }
                                    $a_dates = ['minDate_' => $date , 'maxDate_' => $to];
                                    $json = $this->AtomatizacionCalculo($content->idcustomer,$content->idcontent,$a_dates,$a_emisiones_content['data'],$content, "daily", $dates);  
                                    Storage::disk('public')->delete("filesautocalculo/".$archivo);
                                    $tmp_data = array_merge($data_cont,$json);
                                    Storage::disk('public')->append("filesautocalculo/".$archivo,json_encode($tmp_data));
                                } else {
                                    //Añadir si es una Nueva campaña (Hay que añadir desde el inicio 2018)
                                    $json = $this->AtomatizacionCalculo($content->idcustomer,$content->idcontent,$a_dates,$a_emisiones_content['data'],$content, "daily"); 
                                    Storage::disk('public')->append("filesautocalculo/".$archivo,json_encode($json));
                                }
                            }
                            echo "FIN->".$content->idcontent. ' ' .date("h:i:s"). PHP_EOL;
                        }catch(Exception $e){
                            echo "Error en campaña: " . $content->idcontent ." error: " .$e->getMessage();
                        }
                    }
                }                            
                die("fin");
            }
        }
    }

    public function getEmisionesByContent($idcontent,$idcustomer,$minDate_,$maxDate_){
        $client = new \GuzzleHttp\Client();
        $json = array 
        (
            'idcustomer' => intval($idcustomer),
            "date_start" => $minDate_,
            "date_end" =>  $maxDate_,
            "idcampaign" => $idcontent,
        );
        $timhash = $this->getTimestampAndKeyhash();
        $responseCampaignEmisiones = $client->request('POST',env('URL_MONGO').'findAll?timestamp='.$timhash['timestamp'].'&keyhash='.$timhash['keyhash'], ['json' => $json, 'verify' => false]);
        $countByDateEmisiones = json_decode($responseCampaignEmisiones->getBody(), true);
        return $countByDateEmisiones;
    }

    public function getTimestampAndKeyhash(){
        $ts =  Carbon::now()->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = md5($string);
        $data = ["timestamp"=> $current_timestamp, "keyhash"=>$keyhash];
        return $data;
    }

    public function getCustomers(){
        $modCustomer = new Customer();
        return $modCustomer->where('has_sales', 1)->get();
    }

    /** 
     * Se miran las ventas que hay para la campaña en curso entre los periodos de inicio y finalizado
     *  de la campaña PERO SOLO TENIENDO EN CUENTA LOS DIAS QUE VERDADERAMENTE SE HA EMITIDO LA
     *  CAMPAÑA TANTO EN LAS TIENDAS DE EMISION, COMO EN LAS DE CONTRASTE, COMO EN EL PERIODO ANTERIOR
     *  Para cada campaña se hace un inventario de TODOS LOS DIAS EXISTENTES entre la fecha de 
     * inicio y fecha de fin para:  
     *      1) Si existen ventas para ese dia se cogen las ventas
     *       2) Si NO existen ventas para ese dia se genera tambien el registro pero poniendo
     *              "0" en el campo UNIDADES
     */
    public function AtomatizacionCalculo($a_dates,$countByDateEmisiones,$content, $type = null, $dates = []){
        $desde = $a_dates['minDate_'];
        $hasta =  $a_dates['maxDate_'];
        $c_content_emisiones = collect($countByDateEmisiones);
        $tmp_sites = $c_content_emisiones->pluck('idsite')->unique();
        $a_sites = [];
        foreach($content->playcircuits as $circuit){
            foreach($circuit->sites as $site){
                if($site->consider_in_sales){
                    $a_sites[] = $site;
                }
            }
        }
        $a_sites = collect($a_sites);
        /*$a_sites = Site::
            where('idcustomer',$content->idcustomer)
            ->where('status',1)
            ->where('deleted',0)
            ->where('code','!=',null)
            ->where('consider_in_sales', 1)
            ->select('code','idsite')
            ->get();*/
        //idsite 0 x que es contraste
        $a_sites_contraste = \DB::table('emision_contraste')
            ->where('idcustomer',$content->idcustomer)
            ->where('idsite',0)
            ->select('codesite as code')
            ->get();
        $a_sites_contraste_nc = \DB::table('emision_contraste')
        ->where('idcustomer',$content->idcustomer)
        ->where('idsite','!=',0)
        ->select('codesite as code')
        ->get();
        $sites_content = array_intersect($a_sites->pluck('code')->toArray(), $a_sites_contraste_nc->pluck('code')->toArray());
        $all_sites = array_merge($a_sites_contraste->pluck('code')->toArray(), $sites_content);
        $gte ='$gte'; $lt = '$lt'; $in = '$in'; $and = '$and';

        /*** Usar varias llamadas al WS por caida de mongo */
        // $byyears=[2019,2020,2021];
        $byyears=[2019,2020,2021];
        $tmpfilejson = [];

        // Hay que coger los tickets para tener el periodo anterior y SOLO hasta el
        // mes en que acaba la campaña, ya que los posteriores no hacen falta
        // Si la fecha de inicio de la campaña es < 2019 se coge desde el 1/1/2019
        $fechadeinicio = date ('Y-m-d',strtotime('2019-01-01'));
        $a_json=[];
        if(empty($type)){
            foreach($byyears as $byy) {
                echo "\n año: $byy \n";
				$mesesdecampania=$this->vermesesencampania($fechadeinicio, $content->dates[0]->date_off, $byy);			               
                $dat = [
                    "customer_id"=>$content->idcustomer,
                    "site_id" => ["$in" => $all_sites],
                    "product"=> ["$in" => $content->products] ,
                    "year" =>  $byy,
					"month" => ["$in" => $mesesdecampania],
                ];
		        echo "getWsTickets....idcampaign----byy---------->".$content->idcontent."___".$byy."__".json_encode($mesesdecampania)."__".date("h:i:s"). PHP_EOL;  
                $tmpagticket= $this->getWsTickets('tickets',$content->idcustomer,'GET',$dat);//****Attencion esta a false tira la de bd de local
		        $tmpfilejson = array_merge($tmpfilejson,$tmpagticket['data']);
            }
        }else{
            $fecha_ini_tope = $desde." 00:00:00";   // BP
            $fecha_tope 	= $hasta." 23:59:59";   // BP	
            if($content->dates[0]->date_off < $hasta){
                $fecha_tope = substr($content->dates[0]->date_off, 0, 4)."-". 
                            substr($content->dates[0]->date_off, 5, 2)."-". 
                            substr($content->dates[0]->date_off, 8, 2)." 23:59:59";
                $hasta = $content->dates[0]->date_off;
            }
            if(!empty($dates)){
                foreach($dates as $date){
                    $dat= [
                        "customer_id"=>$content->idcustomer,
                        "site_id" => ["$in" => $all_sites],
                        "product"=> ["$in" => $content->products] ,
                        "datetime" =>  $date
                    ]; 
                    $tmpagticket = $this->getWsTickets('tickets',$content->idcustomer,'GET',$dat);//****Attencion esta a false tira la de bd de local 
                    $tmp = $tmpagticket['data']; 
                    $ageticket = collect($tmp);        
                    $a_json[] = $this->genItem($date,$date, $ageticket, $a_sites, $content->idcontent, $countByDateEmisiones,$content->idcustomer,$content, $a_sites_contraste);
                }
            }
            foreach ($byyears as $key => $byy) {
                echo "\n año: $byy \n";
				$mesesdecampania = $this->vermesesencampania($fechadeinicio, $content->dates[0]->date_off, $byy);	
                $dat = [
                    "customer_id" => $content->idcontent,
                    "site_id" => ["$in" => $all_sites],
                    "product" => ["$in" => $content->products] ,
                    "year" =>  $byy,
                    "month" => ["$in" => $mesesdecampania],
                ];	
                echo "getWsTickets....idcampaign----byy---------->" . $content->idcontent . "___" . $byy . "__" . json_encode($mesesdecampania) . "__" . date("h:i:s") . PHP_EOL;  
                $tmpagticket = $this->getWsTickets('tickets',$content->idcustomer,'GET',$dat);//****Attencion esta a false tira la de bd de local
                $tmpfilejson = array_merge($tmpfilejson,$tmpagticket['data']); 
                echo "antes getWsTickets....idcampaign----byy---------->" . $content->idcontent . "___" . $desde . "<br>"; 
            }
        }    
        $ageticket = collect($tmpfilejson);
        $fecha_ini_tope = $fechadeinicio . " 00:00:00";// BP
        $fecha_tope = $hasta . " 23:59:59";// BP	
        if($content->dates[0]->date_off < $hasta)
			$fecha_tope = substr($content->dates[0]->date_off, 0, 4) . "-" . 
					      substr($content->dates[0]->date_off, 5, 2) . "-" . 
					      substr($content->dates[0]->date_off, 8, 2) . " 23:59:59";
        echo "Generando registros  dia a dia entre     desde .... hasta...".$fecha_ini_tope."___".$fecha_tope.PHP_EOL;
		$dates = []; 
		// se inicializa el array $dates donde estan todas las fechas secuenciales existentes entre las fechas inicio y fin
		for($i = $fecha_ini_tope;$i <= $fecha_tope;$i = date("Y-m-d", strtotime($i ."+ 1 days")))		
 			$dates[$i] = [];  
		// Se recorre el array $ageticket   donde estan las ventas  de 2019 y 2020 
		foreach($ageticket as $ticket){
			$date = $ticket["date"];     // fecha secuencial
			$siteid = $ticket["site_id"]."";       // site 
			if(!array_key_exists($date, $dates))    // si la fecha no existe en el array de fechas, se crea inicializandola
				$dates[$date] = [];	
			if(!array_key_exists($siteid, $dates[$date]))   // si el site no existe en el array de fechas para la fecha del ticket en curso, se crea inicializandola
				$dates[$date][$siteid] = [];
			$dates[$date][$siteid][] = $ticket;
		}
    	$a_json = [];  
		foreach($dates as $date => $tickets){
 			for($j = 0;$j < $a_sites->count();$j++){
				$ref = $a_sites[$j]->code."";
				$ticketref = (array_key_exists($ref,$tickets)) ? $tickets[$ref] : [];
				$tmpageticket = collect($ticketref); 
				$tmp_json = $this->ficherodecampaña($content->idcontent,$a_sites[$j],$date,$countByDateEmisiones,$content->idcustomer,$content->products,$tmpageticket);
				array_push($a_json,$tmp_json);               
		    }
			for($z = 0;$z < $a_sites_contraste->count();$z++){
				$ref = $a_sites_contraste[$z]->code."";
				$ticketref = (array_key_exists($ref,$tickets))? $tickets[$ref] : [];
				$tmpageticket = collect($ticketref);
				$tmp_json = $this->ficherodecampaña($content->idcontent,$a_sites_contraste[$z],$date,$countByDateEmisiones,$content->idcustomer,$content->products,$tmpageticket);
				array_push($a_json,$tmp_json);
		   	} 
		}
        return $a_json;
    }
	
    public function genItem($fecha_ini_tope,$fecha_tope, $ageticket, $a_sites, $idcontent, $countByDateEmisiones,$idcustomer,$content,$a_sites_contraste){
        echo "Generando registros  dia a dia entre desde .... hasta...".$fecha_ini_tope."___".$fecha_tope.PHP_EOL;
        $a_json = [];
        for($i = $fecha_ini_tope;$i <= $fecha_tope;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
            $i = date("Y-m-d", strtotime($i));
            $date_ageticket = $ageticket->where('date', $i);
            for($j = 0;$j < $a_sites->count();$j++){
                $tmpageticket = $date_ageticket->where('site_id',$a_sites[$j]->code);     
                $tmp_json = $this->ficherodecampaña($idcontent,$a_sites[$j],$i,$countByDateEmisiones,$idcustomer,$content->products,$tmpageticket);
                array_push($a_json,$tmp_json );          
            }
            for($z = 0;$z < $a_sites_contraste->count();$z++){
                $tmpageticket = $date_ageticket->where('site_id',$a_sites_contraste[$z]->code);               
                $tmp_json = $this->ficherodecampaña($idcontent,$a_sites_contraste[$z],$i,$countByDateEmisiones,$idcustomer,$content->products,$tmpageticket);
                array_push($a_json,$tmp_json);
            }   
        }
        return $a_json;     
    }

 	public	function vermesesencampania($f1,$f2,$byy){
		// se coge el dia 1 de los 2 meses para que no haya diferencia de dias ya que si por ejemplo el dia del mes inicial 
		// es 9 y el dia delmes final es 7, ese ultimo mes ya no se cogeria
		$f1 = substr($f1,0,4)."-". substr($f1,5,2)."-". "01";    
		$f2 = substr($f2,0,4)."-". substr($f2,5,2)."-". "01";  
		$month = strtotime($f1);
		$end = strtotime($f2);
		$mesesenaño=array();
		while($month <= $end)
		{
			if($byy != date('Y', $month)) {
				$month = strtotime("+1 month", $month);
			    continue;
			}
			array_push($mesesenaño,date('m', $month) );   
			$month = strtotime("+1 month", $month);
		}
		return $mesesenaño;
	}

    public function ficherodecampaña($idcontent,$a_site,$diasecuencial,$countByDateEmisiones,$idcustomer,$productos,$ageticket) {  
        $a_json = [];
        $codesite = $a_site->code;
        $res = collect();
        $fechaaconsiderar = $diasecuencial;
        if(isset($a_site->idsite)){
            $countByDateEmisiones = collect($countByDateEmisiones);
            $res = $countByDateEmisiones->where('idcustomer', $idcustomer)->whereIn('idsite',$a_site->idsite)->where('idcontent',$idcontent)->where('date',$fechaaconsiderar); 
        }
        if(isset($res))
            if($res->count() == 0)
                $tieneemision = 0;
            else
                $tieneemision = 1;
        else
            $tieneemision = 0;
        if ($ageticket->count() > 0){
            $resgrpByProd = [];
            foreach($ageticket as $restck){
                if(!isset($resgrpByProd[$restck['product']][$restck['hour']])){
                    $resgrpByProd[$restck['product']][$restck['hour']]=['product'=>$restck['product'], 'units'=>0, 'total_amount'=>0, 'cost_price'=>0];
                }
                if($restck['units']){ 
                    $resgrpByProd[$restck['product']][$restck['hour']]['units']+=$restck['units'];
                }
                if($restck['total_amount']){
                    $resgrpByProd[$restck['product']][$restck['hour']]['total_amount']+= $restck['total_amount'];
                }
                if($restck['cost_price']){ 
                    $resgrpByProd[$restck['product']][$restck['hour']]['cost_price']+=$restck['cost_price'];
                }
            }
            $tmpweekday= $ageticket->pluck('weekday')->first();
            foreach($resgrpByProd as $keycodeprod => $rsd){
                foreach ($rsd as $kh => $rgrpti) {
                    $tira = ['idcontent'=>$idcontent,'codesite'=>$codesite, 'codeprod'=>$keycodeprod, 
                    'fecha' => $fechaaconsiderar, 'hour'=> $kh,'weekday'=> $tmpweekday,
                    'total_units'=>str_replace(".", ",", $rgrpti['units']),
                    'total_amount'=>str_replace(".", ",",$rgrpti['total_amount']),'tieneemision'=>$tieneemision,
                    'cost_price'=>$rgrpti['cost_price']
                ]; 
                    array_push($a_json,$tira);
                }
            }
        } else{
            $dayofweek = date('w', strtotime($fechaaconsiderar))+1;
            $codeprod = $productos[0];
            $tira = ['idcontent'=>$idcontent,'codesite'=>$codesite, 'codeprod'=>$codeprod, 
            'fecha' => $fechaaconsiderar, 'time'=> '00:00:00', 'hour'=>0, 'weekday'=> $dayofweek,'total_units'=>0,
            'total_amount'=>0,'tieneemision'=>$tieneemision,
            'cost_price'=>0
            ];
            array_push($a_json,$tira);
        }
        return $a_json;
    }

    // Generación de archivos carpeta 'filesintegration'
    public function generateFilesintegration($idcustomer){
        ini_set('memory_limit', '64380M');
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '-1');		
        ini_set('upload_max_filesize', '6400M');
        ini_set('post_max_size', '6400M');
        $idcontent = 0;
        $listContent = $this->Fetch_ContentsSiteSinProddifenddif($idcustomer,0,0,0,0); //Abril 2021
        $listContent = json_decode($listContent);
		array_multisort(array_column($listContent, 'idcontent'), SORT_ASC, $listContent);	
        foreach ($listContent as $lc){
            $idcontent = $lc->idcontent;
                $date1 = $lc->dates[0]->date_on;
                $date2 = $lc->dates[0]->date_off;	
                $pestart = new \DateTime($date1);
                $peend = new \DateTime($date2);
                $days_between = $pestart->diff($peend)->format('%a');
                $data['date_time_peant'] = date('Y-m-d',strtotime('2019'.substr($date1, 4, 6)));
                $data['date_time_peant'] = date('Y/m/d', strtotime($data['date_time_peant']));
                $data['date_time_peant'] = $data['date_time_peant'] ." - ".  date('Y/m/d', strtotime($data['date_time_peant']. ' + '.$days_between.' days'));
                $minDate_ = $date1;
                $maxDate_ = $date2;    
                $explode_peant = explode(" - ",$data["date_time_peant"]);
                $date1_peant = str_replace('/', '-', $explode_peant[0]);
                $date2_peant= str_replace('/', '-', $explode_peant[1]);
                $minDate_peant = date('Y-m-d',strtotime($date1_peant));
                $maxDate_peant = date('Y-m-d',strtotime($date2_peant));   
                $hoy = date('2021-11-17');
                $strhoy = strtotime($hoy);
                $strMaxDate = strtotime($maxDate_);
                if($strMaxDate > $strhoy){
                    $maxDate_= $hoy;
                    $strMinDate = strtotime($minDate_);
                    $strMaxDate = strtotime($maxDate_);
                    $difdias = intval(($strMaxDate - $strMinDate) /60/60/24);
                    $maxDate_peant = date('Y-m-d', strtotime($minDate_peant. ' + '.$difdias.' day'));
                }
                $a_dates = ['minDate_' => $minDate_, 'maxDate_' => $maxDate_, 'minDate_peant' => $minDate_peant, 'maxDate_peant' => $maxDate_peant, 'minDate_content' => $lc->dates[0]->date_on, 'maxDate_content' => $lc->dates[0]->date_off];
                $datadifendif = $this->getDataJsonDifenDifIntegration($a_dates,$idcustomer,$idcontent,[0]);
                if($datadifendif !== false){
                    $fil = 'filesintegration/merge_sites_all_campaign_'.$idcontent.'.json';
                    Storage::disk('public')->put($fil,json_encode($datadifendif,JSON_UNESCAPED_UNICODE)); 
                }
        }
    }

    public function getDataJsonDifenDifIntegration($a_dates,$idcustomer,$idcontent,$idsite){
        $a_sites=[];    
        $desde_for = $a_dates['minDate_'];
        $hasta_for = $a_dates['maxDate_'];
        $desde_peant = $a_dates['minDate_peant'];
        $desde_content = $a_dates['minDate_content'];
        $hasta_content = $a_dates['maxDate_content'];
        $desde = $desde_for;
        if($desde_for <= $desde_content){
            $desde = $desde_content;
        }
        $hasta = $hasta_for;
        if($hasta_for >= $hasta_content){
            $hasta = $hasta_content;
        }
        $ddesde = Carbon::createFromFormat('Y-m-d', $desde);
        $dhasta = Carbon::createFromFormat('Y-m-d', $hasta);
        $ddesde_for = Carbon::createFromFormat('Y-m-d', $desde_for);
        $diferencia_en_dias_total = $ddesde->diffInDays($dhasta);
        $diferencia_dias_inicial = $ddesde_for->diffInDays($ddesde);        
        $desde__pa = date("Y-m-d", strtotime($desde_peant .' +'.$diferencia_dias_inicial.' day'));
        $hasta__pa = date("Y-m-d", strtotime($desde__pa .' +'.$diferencia_en_dias_total.' day'));
        if($idcontent == 0){
            $modContent= new Content();          
            $a_contents = json_decode($this->Fetch_ContentsSiteSinProddifenddif($idcustomer,0,0,0,0));
            $tmpfil='';
            $datafil=[];
            foreach ($a_contents as $ac) {
                if(Storage::disk('public')->exists('filesautocalculo/difendif-'.$ac->idcontent  . '-'. $idcustomer .'.json')){ 
                    $multifil = Storage::disk('public')->get('filesautocalculo/difendif-'.$ac->idcontent  . '-'. $idcustomer .'.json');
                    $tmpdatafil= json_decode($multifil);
                    $datafil = array_merge($datafil, $tmpdatafil);
                    $check=1;
                }              
            }
        } else{
            if(Storage::disk('public')->exists('filesautocalculo/difendif-'.$idcontent  . '-'. $idcustomer .'.json')){
                $datafil =  Storage::disk('public')->get('filesautocalculo/difendif-'.$idcontent  . '-'. $idcustomer .'.json');
                $datafil = json_decode($datafil);
            } else{
			   return false;
            }   
        }
        $a_sites_contraste = \DB::table('emision_contraste')
                            ->where('idcustomer',$idcustomer)
                            ->where('idsite',0)
                            ->select('codesite')
                            ->get()->pluck("codesite")->toArray();
		
        //Todos los sites
        if(count($idsite) == 1){
            if($idsite[0] == 0){
                $aa_sites = $this->Fetch_Sites($idcustomer);
                $c_sites = collect(json_decode($aa_sites))->pluck('code');
                $idsite = $c_sites->toArray();
            }
        }	
		
        foreach($datafil as $daf){ 
			if( !empty($daf)){
				if($idcustomer == 52)
					if($daf[0]->codesite != "144" && $daf[0]->codesite != "78")
						continue;
				if (
				     (!in_array($daf[0]->codesite, $idsite))  && 
				     (!in_array($daf[0]->codesite, $a_sites_contraste))
				   )
						continue;								
                if(($daf[0]->fecha >= $desde) && ($daf[0]->fecha <= $hasta)){
                    if(count($daf)==1){
                        $daf[0]->enperiodoemision = 1;						
					    if(in_array($daf[0]->codesite, $a_sites_contraste)){
                            $daf[0]->tiendaemision = 0;               
                        }
                        elseif(in_array($daf[0]->codesite, $idsite)){ 
                            $daf[0]->tiendaemision = 1;                            
                        }
						array_push($a_sites, $daf[0]);
                    } else {
                        foreach ($daf as  $dt) {
                            $dt->enperiodoemision = 1;
                            if(in_array($dt->codesite, $a_sites_contraste)){
                                $dt->tiendaemision = 0;
                            } elseif(in_array($dt->codesite, $idsite)){
                                $dt->tiendaemision = 1;
                            }
                            array_push($a_sites, $dt);
                        }
                    }
                } elseif(($daf[0]->fecha >= $desde__pa) && ($daf[0]->fecha <= $hasta__pa)){
                    if(count($daf) == 1){
						$daf[0]->enperiodoemision = 0;
                        if(in_array($daf[0]->codesite, $a_sites_contraste)){                           
                            $daf[0]->tiendaemision = 0;
                        } elseif(in_array($daf[0]->codesite, $idsite)){  
                            $daf[0]->tiendaemision = 1;
                        }
						array_push($a_sites, $daf[0]);
                    } else {
                        foreach ($daf as $dt) {
                            $dt->enperiodoemision = 0;
                            if(in_array($dt->codesite, $a_sites_contraste)){ 
                                $dt->tiendaemision = 0;
                            } elseif(in_array($dt->codesite, $idsite)){  
                                $dt->tiendaemision = 1;
                            }
                            array_push($a_sites, $dt);							
                        }
                    }
                }
            }
        }
        $a_sites = $this->normalizeJsonPython($a_sites);
        return $a_sites;
    }

    public function Fetch_Sites($customerid){
        $modSite = new Site();
        $user = Auth::user();
        if($user && $user->hasRole('partner') ){
            $sitesOfCustomer = $modSite->sitesOfCustomer($customerid);
        } elseif($user && $user->hasRole('userstats')) {
            if(isset($user->idsite)){
                $sitesOfCustomer = $modSite->siteOfSite($user->idsite);
            }
        } else{ 
            $sitesOfCustomer = $modSite->sitesOfCustomer($customerid);
        }
        return json_encode($sitesOfCustomer);
    }

    public function Fetch_ContentsSiteSinProddifenddif($idcustomer,$createuser=0,$siteid=0,$dateStart,$dateEnd){  
        $modContet = new Content();
        if($dateStart!=0){
            $dateStart = date('Y-m-d',intval($dateStart));
        }
        if($dateEnd!=0){
            $dateEnd = date('Y-m-d', intval($dateEnd));
        }

        $getCampaigns = $modContet->getContentsFromCustomerSiteNotProd($idcustomer,$createuser,$siteid,$dateStart,$dateEnd,$filtro=0);

        $strdates = explode("-", $dateStart);

        return json_encode($getCampaigns);
    }

    public function saveResumen($ejercicio, $month, $idcustomer){
        $modContent = new Content();
        $days_limit_duration = 2000000000; $namecontent = "";
        /*
        Contenidos “activos” el ejercicio/mes especificado excepto las campañas que tengan más de 200 días de duración.
		Para quitar esta restriccion en vez de poner 200 se pone 2000000000000
        El inicio del Periodo Anterior para cada campaña será la Fecha Desde de inicio de cada Campaña – 1 año
        */
		
    	echo "------MES-----...". $month .PHP_EOL;
		// borrar mes antes de grabar
		DB::table('difendif_python_impacto')
				->where('idcustomer',$idcustomer)
				->where('ejercicio',$ejercicio)
				->where('mes',$month)					
				->delete();
 
        $a_dates = $this->getFirstAndLastDayoftheMonth($ejercicio,$month);
        $tmp_a_contents = $modContent->getContentsByDate($idcustomer,$a_dates);          
        $a_contents = $this->getContentsLimitDuration($tmp_a_contents,$days_limit_duration,$month,$ejercicio);
        foreach ($a_contents as $key => $cm) { 
                $a_data_content = $modContent->getDateOnDateOffContents([$cm->idcontent]);      
                $dates = $a_data_content->where('idcontent',$cm->idcontent)->first();
                $a_dates["minDate_"]= $dates->dates[0]->date_on;
                $a_dates["maxDate_"]= $dates->dates[0]->date_off;
                $a_dates["minDate_content"]= $dates->dates[0]->date_on;
                $a_dates["maxDate_content"]= $dates->dates[0]->date_off;
                $tmpc = $this->getDataJsonDifenDif($a_dates,$idcustomer,$cm->idcontent,[0]);
                if($tmpc){
                    $fil = 'datajson/file_resumen_mensual-'.$ejercicio. '-'.$month.'-'.$cm->idcontent.'.json';
                    Storage::disk('public')->put($fil,json_encode($tmpc,JSON_UNESCAPED_UNICODE));
                    $tmpnamecontent = Content::where('idcontent',$cm->idcontent)
                                               ->where('deleted',0)
                                               ->select('name')
                                               ->first();
                    
                    if(isset($tmpnamecontent->name)){
                        $namecontent = $tmpnamecontent->name;
                    }
                    echo "Contenido...: ".$cm->idcontent.PHP_EOL;
                    try {
                        $tmppython = $this->getWsPython('apibp-resumen-simple','PUT',$tmpc);
                        $tmpjsonf = $this->convertJsonPython($tmppython->data[0],$namecontent);
                        $tmpjsonf->ejercicio = $ejercicio;
                        $tmpjsonf->mes = $month;
                        $tmpjsonf->status = 0;
                        $tmpjsonf->idcustomer = $idcustomer;
                        echo "Voy a guardar los datos en la tabla." . PHP_EOL;
                        DifendifPythonImpacto::create((array) $tmpjsonf);
                    } catch (\Throwable $th) {		
                        echo "catch....: ".$cm->idcontent . PHP_EOL;
                        echo "Mensaje: " . $th . PHP_EOL;
                    }
                }else{
                    //Contenidos que no esta cacheadas
                    echo "Contenido no cacheado...: ".$cm->idcontent . PHP_EOL;
                }
        }
        die("FINAL DEL PROCESO");
    }

    public function convertJsonPython($a_data,$namecampaign=""){
        echo $a_data->idcontent . PHP_EOL;
        $object = new StdClass;
        if($namecampaign !=""){
            $object->name = $namecampaign;
        }
        isset($a_data->idcontent) ? $object->idcampaign = intval($a_data->idcontent) : '';
        isset($a_data->IncVentasLd_prom) ? $object->diff_var_uni_em = $a_data->IncVentasLd_prom : '';
        isset($a_data->IncVentasCo_prom) ? $object->diff_var_uni_co = $a_data->IncVentasCo_prom : '';
        isset($a_data->ImpCamp_prom) ? $object->diff_inc_uni = str_replace(',','', number_format(floatval($a_data->ImpCamp_prom),2))  : '';
        isset($a_data->TotalVentaLdEm) ? $object->sum_uni_em_pEm= $a_data->TotalVentaLdEm : '';
        isset($a_data->TotalVentaLdnEm) ? $object->sum_uni_em_preEm= $a_data->TotalVentaLdnEm : '';
        isset($a_data->TotalVentaCoEm) ? $object->sum_var_uni_co_pEm=  ($a_data->TotalVentaCoEm == "NaN") ? 0 : $a_data->TotalVentaCoEm : '';
        isset($a_data->TotalVentaConEm) ? $object->sum_var_uni_co_preEm= $a_data->TotalVentaConEm : '';
        isset($a_data->TotalImporteLdEm) ? $object->sum_var_imp_em_pEm= $a_data->TotalImporteLdEm : '';
        isset($a_data->TotalImporteLdnEm) ? $object->sum_var_imp_em_preEm= $a_data->TotalImporteLdnEm : '';
        isset($a_data->TotalImporteCoEm) ? $object->sum_var_imp_co_pEm= ($a_data->TotalImporteCoEm == "NaN") ? 0 : $a_data->TotalImporteCoEm : '';
        isset($a_data->TotalImporteConEm) ? $object->sum_var_imp_co_preEm= $a_data->TotalImporteConEm : '';
        isset($a_data->IncVentaLdBP) ? $object->sum_inc_uni_em= $a_data->IncVentaLdBP : '';
        isset($a_data->IncVentaCoBP) ? $object->sum_inc_uni_co= ($a_data->IncVentaCoBP == "NaN") ? 0 : $a_data->IncVentaCoBP : '';
        isset($a_data->IncVentadBP) ? $object->sum_inc_uni= ($a_data->IncVentadBP == "NaN") ? 0 : $a_data->IncVentadBP : '';
        isset($a_data->IncImporteLdBP) ? $object->sum_inc_imp_em= $a_data->IncImporteLdBP : '';
        isset($a_data->IncImporteCoBP) ? $object->sum_inc_imp_co=  ($a_data->IncImporteCoBP == "NaN") ? 0 : $a_data->IncImporteCoBP: '';
        isset($a_data->IncImporteBP) ? $object->sum_inc_imp=  ($a_data->IncImporteBP == "NaN") ? 0 : $a_data->IncImporteBP : '';
        isset($a_data->Diff_var_uni_Em) ? $object->diff_var_uni_pEm = str_replace(',','', number_format($a_data->Diff_var_uni_Em,2)) : '';
        isset($a_data->Diff_var_uni_preEm) ? $object->diff_var_uni_preEm = str_replace(',','', number_format($a_data->Diff_var_uni_preEm,2)) : '';
        isset($a_data->Diff_var_uni) ? $object->diif_var_uni = str_replace(',','', number_format($a_data->Diff_var_uni,2)) : '';
        isset($a_data->ImpactoEuros) ? $object->diff_eur = str_replace(',','', number_format($a_data->ImpactoEuros,2)) : '';
        isset($a_data->MLe_prom) ? $object->diff_em_pEm	= str_replace(',','', number_format($a_data->MLe_prom,2)) : '';
        isset($a_data->MLne_prom) ? $object->diff_em_preEm	= str_replace(',','', number_format($a_data->MLne_prom,2)) : '';
        isset($a_data->MCne_prom) ? $object->diff_co_preEm = str_replace(',','', number_format($a_data->MCne_prom,2)) : ''; 
        isset($a_data->MCe_prom) ? $object->diff_co_pEm = str_replace(',','', number_format($a_data->MCe_prom,2)) : '';
        isset($a_data->MLe) ? $object->diff_em_pEm_r	= str_replace(',','', number_format($a_data->MLe,2)) : '';
        isset($a_data->MLne) ? $object->diff_em_preEm_r	= str_replace(',','', number_format($a_data->MLne,2)) : '';
        isset($a_data->MCne) ? $object->diff_co_preEm_r = str_replace(',','', number_format($a_data->MCne,2)) : '';
        isset($a_data->MCe) ? $object->diff_co_pEm_r = str_replace(',','', number_format($a_data->MCe,2)) : '';
        isset($a_data->ImpactoEuros) ? $object->impactoeuros = str_replace(',','', number_format($a_data->ImpactoEuros,2)) : ''; 
        isset($a_data->Peso_Camp) ? $object->pesocamp =  str_replace(',','', number_format($a_data->Peso_Camp,2)) : ''; 
        return $object;
    }

    public function getContentsLimitDuration($tmp_a_contents,$days_limit_duration,$mes,$anio){
        $a_contents = [];

        foreach ($tmp_a_contents as $key => $value) {
            $pestart = new \DateTime($value->date_on);
            $peend = new \DateTime($value->date_off);
            // si la fecha hasta d el acampaña es mayor que el ultimo dia que el mes en que es estas (elegido)
            // cojo como fecha hasta el ultimo dia del mes elegido
            $dateseleccionado = $anio.'-'.$mes.'-01';
            $ultimodiamesseleccionado =  date("Y-m-t",strtotime($dateseleccionado));
            $strultimodiamesseleccionado= strtotime($ultimodiamesseleccionado);
            $strcmpend = strtotime($peend->format('Y-m-d'));
            if($strcmpend>$strultimodiamesseleccionado){
                $datudms = new \DateTime($ultimodiamesseleccionado);
                $days_between = $pestart->diff($datudms)->format('%a');
            } else{
                $days_between = $pestart->diff($peend)->format('%a');
            }
            if($days_between<$days_limit_duration ){
                array_push($a_contents,$value);
            }
        }
        return $a_contents;
    }

    public function getFirstAndLastDayoftheMonth($year,$month){
        $timestamp    = strtotime($year.'-'.$month);
        $first_date = date('Y-m-01 00:00:00', $timestamp);
        $last_date  = date('Y-m-t 23:59:59', $timestamp);
        return ['date_on' => $first_date, 'date_off' => $last_date];
    }

    public function getDataJsonDifenDif($a_dates,$idcustomer,$idcontent,$idsite){
        $modSite = new Site();
        $a_sites = [];

        $desde_for =  date("Y-m-d", strtotime($a_dates['date_on']));
        $hasta_for = date("Y-m-d", strtotime($a_dates['date_off']));
        $desde_content = $a_dates['minDate_content'];
        $hasta_content = $a_dates['maxDate_content'];
        $desde = $desde_for;
        if($desde_for <= $desde_content){
            $desde = $desde_content;
        }
        $hasta = $hasta_for;
        if($hasta_for >= $hasta_content){
            $hasta = $hasta_content;
        }
        $desde_peant = date("Y-m-d", strtotime('2019'.substr($desde, 4)));
        $ddesde = Carbon::createFromFormat('Y-m-d', $desde);
        $dhasta = Carbon::createFromFormat('Y-m-d', $hasta);
        $diferencia_en_dias_total = $ddesde->diffInDays($dhasta);     
        $desde__pa = $desde_peant;
        $hasta__pa = date("Y-m-d", strtotime($desde__pa .' +'.$diferencia_en_dias_total.' day'));
        $datafil=[];
		ini_set('memory_limit', '64380M');
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '-1');		
        ini_set('upload_max_filesize', '6400M');
        ini_set('post_max_size', '6400M');
        if(Storage::disk('public')->exists("filesautocalculo/difendif-" . $idcontent  . "-" . $idcustomer . ".json")){
            $datafil = Storage::disk('public')->get('filesautocalculo/difendif-' . $idcontent  . '-'. $idcustomer .'.json');
            $datafil = json_decode($datafil);
        } else {
            return false;
        }
        $a_sites_contraste = \DB::table('emision_contraste')
                                    ->where('idcustomer',$idcustomer)
                                    ->where('idsite',0)
                                    ->select('codesite')
                                    ->get()->pluck("codesite")->toArray();
        //Todos los sites
        if(count($idsite) == 1 && $idsite[0] == 0){
            $aa_sites=json_encode($modSite->sitesOfCustomer($idcustomer));
            $c_sites = collect(json_decode($aa_sites))->pluck('code');
            $idsite = $c_sites->toArray();
        }
        if(empty($datafil))
            echo "idcontent error".$idcontent;
        else{    
            foreach($datafil as $daf){ 
			    if(!empty($daf)){
				    if($idcustomer == 52)
					    if($daf[0]->codesite != "144" && $daf[0]->codesite != "78")
						    continue;
				    if (
				        (!in_array($daf[0]->codesite, $idsite))  && 
				        (!in_array($daf[0]->codesite, $a_sites_contraste))
				    )
					    continue;								
				
                    if(($daf[0]->fecha >= $desde) && ($daf[0]->fecha <= $hasta)){
                        if(count($daf) == 1){
                            $daf[0]->enperiodoemision = 1;						
					        if(in_array($daf[0]->codesite, $a_sites_contraste)) {
                                $daf[0]->tiendaemision = 0;               
                            } elseif(in_array($daf[0]->codesite, $idsite)) { 
                                $daf[0]->tiendaemision = 1;                            
                            }
						    array_push($a_sites, $daf[0]);
                        } else{
                            foreach ($daf as $dt) {
                                $dt->enperiodoemision = 1;	
                                if(in_array($dt->codesite, $a_sites_contraste)) {
                                    $dt->tiendaemision = 0;
                                } elseif(in_array($dt->codesite, $idsite)) {
                                    $dt->tiendaemision = 1;
                                }
                                array_push($a_sites, $dt);
                            }
                        }
                    } elseif(($daf[0]->fecha >= $desde__pa) && ($daf[0]->fecha <= $hasta__pa)){
                        if(count($daf) == 1){
                            $daf[0]->enperiodoemision = 0;
                            if(in_array($daf[0]->codesite, $a_sites_contraste)){                           
                                $daf[0]->tiendaemision = 0;
                            } elseif(in_array($daf[0]->codesite, $idsite)){  
                                $daf[0]->tiendaemision=1;
                            }
                            array_push($a_sites, $daf[0]);
                        } else {
                            foreach ($daf as  $dt) {
                                $dt->enperiodoemision = 0;  
                                if(in_array($dt->codesite, $a_sites_contraste)){ 
                                    $dt->tiendaemision = 0;
                                } elseif(in_array($dt->codesite, $idsite)){  
                                    $dt->tiendaemision = 1;
                                }
                                array_push($a_sites, $dt);
                            }
                        }
                    }
                }
            }
            $a_sites = $this->normalizeJsonPython($a_sites);
        }
        return $a_sites;
    }

    public function normalizeJsonPython($a_sites){
        $a_normalize = [];
        foreach($a_sites as $value){
            $object['idcontent'] = $value->idcontent;
            $object['codestacion'] = $value->codesite;
            $object['idproducto'] = $value->codeprod;
            $object['fecha'] = date('Y-m-d',strtotime($value->fecha));
            $object['unidades'] = (string)$value->total_units;
            $object['importe'] = (string)$value->total_amount;
            $object['EnperiodoCampaña'] = $value->enperiodoemision;
            $object['TiendaLadorian'] = $value->tiendaemision;
            $object['HahabidoEmisiones'] = $value->tieneemision;
            $object['hour'] = $value->hour;
            $object['weekday'] = $value->weekday;
            $object['cost_price'] = $value->cost_price;
            array_push($a_normalize,$object);
        }
        return $a_normalize;
    }
}
