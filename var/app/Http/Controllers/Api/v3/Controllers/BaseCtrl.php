<?php 
namespace App\Http\Controllers\Api\v3\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Carbon\Carbon;

use App\Http\Controllers\Api\v3\Daos\LogDAO;
use App\Http\Controllers\Api\v3\Daos\ContentDAO;
use App\Http\Controllers\Api\v3\Daos\ViewerDAO;
use App\Http\Controllers\Api\v3\Daos\TagDAO;
use App\Http\Controllers\Api\v3\Daos\CampaignDAO;
use App\Models\Campaign_Type;

use App\Http\Controllers\Api\v3\Daos\VisualizationDAO;
use App\Models\Visualization_Type;
use DateTime;

class BaseCtrl extends Controller {

    protected function toJSON($response) {
        $response_code = ($response['success'])? 200 : 200;
        return response($response, $response_code)->header('Content-Type', 'application/json');
    }

    public function SendLog(){
        $params = Input::get();

        $add = LogDAO::AddLog($params);
        $response = array('success' => false, 'data' => 'Error al registrar LOG');
        if($add)
            $response = array('success'=>true, 'data'=>'Registro añadido con éxito');

        return $this->toJSON($response);
    } 

    public function GetVisualizations() {
        //{ idviewer }
        $params = Input::get();

        $response = null;
        if(empty($params['idviewer']))
            $response = array('success' => false, 'data' => 'Debes indicar ID Visor'); 
        else {
            $visualizations = VisualizationDAO::GetVisualizations($params['idviewer']);

            $response = array('success'=>false, 'data'=>'No ha sido posible obtener las visualizaciones');
            if($visualizations)
                $response = array('success'=>true, 'data'=>$visualizations);
        }

        return $this->toJSON($response);
    }
  
  
    /* ****    ADD COINS    **** */
    public function AddCoins(){
        $params = Input::get();

        $response = null;
        if(empty($params['idviewer']))
            $response = array('success' => false, 'data' => 'Debes indicar ID Visor'); 
        else if(empty($params['idcampaign']))
            $response = array('success' => false, 'data' => 'Debes indicar ID Campaña');
        else if(empty($params['idvisualizationtype']))
            $response = array('success' => false, 'data' => 'Debes indicar ID Tipo Visualización');
        else {
            $campaign = CampaignDAO::CampaignByID($params['idcampaign']);
            $params['coins_obtained'] = $this->getCoins($campaign, $params['idvisualizationtype']);
            $addView = $this->addVisualization($params);

            $response = array('success'=>false, 'data'=>'No ha sido posible registrar la visualizacion');
            if($addView) {
                $response = array('success'=>true, 'data'=>'Visualización registrada con éxito');
                if($params['idvisualizationtype'] == Visualization_Type::INDIVIDUAL) {
                    // AÑADIR COINS 
                    $viewer = $this->addNormalCoins($params, $campaign);
                    $response = array('success'=>true, 'data'=>$viewer);
                }
                else if($params['idvisualizationtype'] > Visualization_Type::INDIVIDUAL) {
                    // AÑADIR EXTRA COINS
                    $viewer = $this->addExtraCoins($params, $campaign);
                    $response = array('success'=>true, 'data'=>$viewer);
                }
            }
        }

        return $this->toJSON($response);
    }

    private function getCoins($campaign, $idvisualizationtype) {
        $coins = 0;
        if($idvisualizationtype == Visualization_Type::INDIVIDUAL)
            $coins = $campaign->c_coins;
        else if($idvisualizationtype > Visualization_Type::INDIVIDUAL)
            $coins = $campaign->c_extracoins;

        if($campaign->type == Campaign_Type::CUPON)
            $coins = $coins * -1;

        return $coins;
    }

    private function addVisualization($params) {
        $time = date("Y-m-d H:i:s"); //(!empty($params['timestamp']))? date("Y-m-d H:i:s", $params["timestamp"]/1000) : date("Y-m-d H:i:s");
        return VisualizationDAO::Insert($params["idviewer"], $params["idcampaign"], $params["idvisualizationtype"], $params["coins_obtained"], $time);
    }

    private function addNormalCoins($params, $campaign) {
        $decrement = ($campaign->type == Campaign_Type::CUPON);
        $viewer = ViewerDAO::UpdateCoins($params['idviewer'], $campaign->c_coins, $decrement);
        return $viewer;
    }

    private function addExtraCoins($params, $campaign) {
        $viewer = ViewerDAO::UpdateCoins($params['idviewer'], $campaign->c_extracoins);
        return $viewer;
    }


    /**
     * Retorna la PLAYLIST de campañas y contenidos, dependiendo si es solicitado desde el Backend o desde un Chromebox
     *
     * @param $paramsPost
     * @param $isBackend (0 chromebox, 1 backend) 
     */
    public function playlist($paramsPost = "", $isBackend=0) {
        if(empty($paramsPost)){
            $params = Input::get();
        }
        else{
            $params = $paramsPost;
        }
        
        $response = array('success' => false, 'data' => 'Debes indicar ID Viewer');
        if(!empty($params['idviewer'])) {
            $campaignTypes = [Campaign_Type::CONTENIDO_PROPIO, Campaign_Type::PUBLICIDAD];
            if(!empty($params['campaign_type'])){
                $campaignTypes = (is_array($params['campaign_type']))? $params['campaign_type'] : array($params['campaign_type']);
            }
                
            $date = Carbon::today();
            if(!empty($params['date'])) {
                $date = Carbon::parse($params['date']);
            }
                      
            $campaigns = $this->campaignsToPlaylist($params['idviewer'], $date, $campaignTypes);
            $contents = $this->contentsToPlaylist($campaigns, $date, $isBackend);
            $powers = $this->powersToPlaylist($contents);
            $tags = $this->tagsToPlaylist($campaigns);
            //$a_vitag = TagDAO::ViewerTagByViewer($idviewer);
            $data = array('campaign'=> $campaigns, 'content'=> $contents, 'power'=> $powers, 'tags' => $tags); 

           
            $response = array('success' => true, 'data' => $data);
        }

        return $this->toJSON($response);
    }

    

    private function campaignsToPlaylist($idviewer, $date, $campaignTypes) {
        return CampaignDAO::CampaingsByViewer($idviewer, $date, $campaignTypes);
    }

    private function contentsToPlaylist($campaigns, $date, $isBackend) {
        return ContentDAO::ContentsByDate($date, $campaigns, $isBackend);
    }

    private function tagsToPlaylist($campaigns) {
        return TagDAO::TagsByCampaignsVTres($campaigns);
    }

    private function powersToPlaylist($contents) {
        return ContentDAO::powersByContents($contents);
    }

}