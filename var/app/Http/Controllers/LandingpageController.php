<?php

namespace App\Http\Controllers;

use App\Http\Requests\Landingpage\LandingpageCreateRequest;
use App\Http\Requests\Landingpage\LandingpageUpdateRequest;
use App\Repositories\Landingpage\LandingpageRepositoryInterface;
use App\Exceptions\DatabaseErrorException;

class LandingpageController extends Controller
{
    private $landingpageRepository;

    public function __construct(LandingpageRepositoryInterface $landingpageRepository) {
        $this->landingpageRepository = $landingpageRepository;
    }
    /** 
     * Creación de una nueva landing page
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @bodyParam name string required
     * @bodyParam thumbnail_url string required
     * @bodyParam base_url string required
     * @bodyParam confi string required
     * 
     * @group Landingpage management
     */
    public function create(LandingpageCreateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];
        try{
            $response = $this->landingpageRepository->create($request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }

        return response()->json($response, $response['statusCode']);
    }
    /** 
     * Actualización de una landing page
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam id required
     * @bodyParam name string
     * @bodyParam thumbnail_url string
     * @bodyParam base_url string
     * @bodyParam config boolean 
     * 
     * @group Landingpage management
     */
    public function update(int $id, LandingpageUpdateRequest $request) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->landingpageRepository->update($id, $request);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Borrado de una landing page de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     *
     * @urlParam id required 
     * 
     * @group Landingpage management
     */
    public function delete(int $id) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->landingpageRepository->delete($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Aceso a los datos de una landing page
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @urlParam id int required
     * @group Landingpage management
     */
    public function landingpage(int $id) {
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{
            $response = $this->landingpageRepository->landingpage($id);
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    /**
     * Acceso a todas las landing pages de la base de datos
     * Esta acción sólo la puede realizar el superadmin
     * 
     * @authenticated
     * 
     * @group Landingpage management
     */
    public function landingpages(){
        $response = ['success' => false, 'data' => 'Error desconocido', 'statusCode' => 503];

        try{ 
            $response = $this->landingpageRepository->landingpages();
            $response = ['success' => true, 'data' => $response, 'statusCode' => 200];
        }catch(DatabaseErrorException $e){
            $response = ['success' => false, 'message' => $e->getMessage(), "statusCode" => 503];
        }
        return response()->json($response, $response['statusCode']);
    }
    
}
