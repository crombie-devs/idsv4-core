<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class WebserviceOtisMiddleware
{
    private $apikey = 'OTISXKWCS1SYAUPAADF7';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {      
        $params = $request->all();
        $timestamp = isset($params['timestamp']) ? $params['timestamp'] : null;
        $keyhash = isset($params['keyhash']) ? $params['keyhash'] : null;

        $error = ["success" => false, "data" => "Tu petición no tiene cabecera de autorización", "status" => 403];
        if(!empty($timestamp) && !empty($keyhash)) {
            $ts_server = Carbon::now()->timestamp * 1000; 
            $diff = abs($ts_server - $timestamp);
            $max_diff = 1000 * 60 * 60 * 12; // 12h de validez

            $error = ["success" => false, "data" => "El token ha expirado ($ts_server)", "status" => 403];
            if($diff <= $max_diff) {
                $string = $timestamp.$this->apikey;
                $keyhash_server = hash('sha256', $string);

                $error = ["success" => false, "data" => "El token no es válido", "status" => 403];
                if(strcmp($keyhash, $keyhash_server) === 0)
                    return $next($request);
            }            
        }

        return response()->json($error);

    }
}