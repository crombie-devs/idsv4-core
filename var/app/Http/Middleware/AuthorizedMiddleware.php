<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;
class AuthorizedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {      
        $user = Auth::user();
        $route = $request->route();
        $method = $route->methods[0];
        
        $name = explode('/', $request->path());
        $permission = strtolower($method);
        foreach($name as $v=>$n){
            if($v != 0){    
                $permission .= '.' .$n;
            }
        }

        //dd($user->getAllPermissions()->pluck('name'),$permission,$user->can($permission));
        if($user->can($permission)){
            return $next($request);  
        }  
        throw UnauthorizedException::forPermissions([$permission]);
    }
}
