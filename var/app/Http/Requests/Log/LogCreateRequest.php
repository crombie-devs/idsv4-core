<?php

namespace App\Http\Requests\Log;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LogCreateRequest extends FormRequest {

    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'isuser' => 'required',
            'identity' => 'required',
            'entity' => 'required',
            'action' => 'required',
            'ip' => 'required',
            'path' => 'required'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}