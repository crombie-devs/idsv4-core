<?php

namespace App\Http\Requests\Asset;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idcustomer' => 'nullable|exists:customers,idcustomer',
            'idsite' => 'nullable|exists:sites,idsite',
            'idtype' => 'required:asset_types,idtype',
            'name' => 'required',
            'asset' => 'nullable|mimes:jpeg,jpg,png,gif,mp4,mpeg,mp3,webm|max:307200',
            'idformat' =>'required:formats,idformat',
            'idlang' => 'required:langs,idlang',
            'asset_url' => 'nullable',
            'duration' => 'numeric',
            'delay' => 'required|min:0|max:30'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}