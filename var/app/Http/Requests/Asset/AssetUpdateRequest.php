<?php

namespace App\Http\Requests\Asset;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idcustomer' => 'exists:customers,idcustomer',
            'idsite' => 'nullable|exists:sites,idsite',
            'idtype' => 'exists:asset_types,idtype',
            'idformat' =>'exists:formats,idformat',
            'idlang' => 'exists:langs,idlang',
            'asset' => 'nullable|mimes:jpeg,jpg,png,gif,mp4,mpeg,mp3,webm|max:307200',
            'asset_url' => 'nullable',
            'duration' => 'numeric',
            'delay' => 'required|min:0|max:30'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }

    
}