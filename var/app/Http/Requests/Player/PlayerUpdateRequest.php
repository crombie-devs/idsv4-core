<?php

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PlayerUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idsite' => 'exists:sites,idsite',
            'email' => 'email',
            'idarea' => 'exists:play_areas,idarea',
            'idlang' => 'exists:langs,idlang',
            
          //  'idtemplate' => 'exists:player_templates,idtemplate',
           /* 'powers.*.weekday' => 'required|numeric',
            'powers.*.time_on' => 'required|time',
            'powers.*.time_off' => 'required|time'*/
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}