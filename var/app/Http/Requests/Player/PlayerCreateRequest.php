<?php

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PlayerCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idsite' => 'required|exists:sites,idsite',
            'idarea' => 'required|exists:play_areas,idarea',
            'idlang' => 'required|exists:langs,idlang',
          //  'idtemplate' => 'required|exists:player_templates,idtemplate',
            'email' => 'required|email',
          /*  'powers.*.weekday' => 'required|numeric',
            'powers.*.time_on' => 'required|time',
            'powers.*.time_off' => 'required|time'*/
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}