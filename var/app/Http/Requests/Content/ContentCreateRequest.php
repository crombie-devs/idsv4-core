<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ContentCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'form.idcustomer' => 'exists:customers,idcustomer',
            'form.idsite' => 'exists:sites,idsite',
            'form.idtype' => 'required:asset_types,idtype',
            'form.idcategory' => 'required:exists:content_categories,idcategory',
            'form.name' => 'required|max:100',
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}