<?php

namespace App\Http\Requests\PlayLogic;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PlayLogicUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idcustomer' => 'exists:customers,idcustomer',
            'idarea' => 'exists:play_areas,idarea',
            'idtype' => 'exists:content_types,idtype',
            'idcategory' =>'exists:content_categories,idcategory',
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}