<?php

namespace App\Http\Requests\PlayLogic;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PlayLogicCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idcustomer' => 'required:customers,idcustomer',
            'idarea' => 'nullable|exists:play_areas,idarea',
            'idplayer' => 'nullable|exists:players,idplayer',
            'idtype' => 'required:content_types,idtype',
            'idcategory' =>'exists:content_categories,idcategory'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}