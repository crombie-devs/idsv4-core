<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomerUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'image' =>  'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'email' => 'nullable|email',
            'langs.*.idlang' => 'exists:langs',
            'influxes_type' => 'required|in:Ladorian Básico,Ladorian Bee The Data',
            'category_level' => 'in:null,N1,N2,N3,N4'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}