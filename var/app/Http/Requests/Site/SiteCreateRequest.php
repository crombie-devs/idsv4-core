<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SiteCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idcustomer' => 'required|exists:customers,idcustomer',
            'idcity' => 'required|exists:cities,idcity',
            'name' => 'required',
            'email' => 'nullable|email',
            'zipcode' => 'nullable|max:10',
            'holidays.*.date' => 'required|date'
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}