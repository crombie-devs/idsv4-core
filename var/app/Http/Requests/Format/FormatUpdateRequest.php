<?php

namespace App\Http\Requests\Format;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class FormatUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
           // 'image_url' =>  'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'frame_image' =>  'nullable|image|mimes:jpeg,png,jpg|max:2048',
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}