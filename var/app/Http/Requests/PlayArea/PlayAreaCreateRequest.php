<?php

namespace App\Http\Requests\PlayArea;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PlayAreaCreateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'idlocation' => 'required|exists:indoor_locations,idlocation',
            'idformat' => 'required|exists:formats,idformat',
            /*'location.idcustomer' => 'required_without:idlocation|exists:customers,idcustomer',
            'location.name' => 'required_without:idlocation',
            'format.name' => 'required_without:idformat',
            'format.image' =>  'required_without:idformat|image|mimes:jpeg,png,jpg|max:2048',*/
          ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}