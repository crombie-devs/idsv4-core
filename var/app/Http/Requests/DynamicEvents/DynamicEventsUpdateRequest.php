<?php

namespace App\Http\Requests\DynamicEvents;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class  DynamicEventsUpdateRequest extends FormRequest {
    const UNPROCESSABLE_ENTITY = 422;

    public function rules() {
        return [
            'id' => 'required',
            'idcustomer' => 'required',
            'is_right_now' => 'required',
            'name' => 'required',
            'frecuency' => 'required',
            'status' => 'required',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['success' => false, 'data' => $validator->errors()], self::UNPROCESSABLE_ENTITY));
    }
}