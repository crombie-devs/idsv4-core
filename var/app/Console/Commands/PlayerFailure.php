<?php

namespace App\Console\Commands;

use App\Models\Player;

use App\Models\Support;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use App\Mail\AlertDownPlayer;
use App\Models\PlayerPower;
use Illuminate\Support\Facades\Mail;

class PlayerFailure extends Command
{

    private $minutes;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'player:failure {minutes=15}  ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comprueba los players inactivos en un rango de tiempo';



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->minutes = intval($this->argument('minutes'));
        $params = 'minutes=' . $this->minutes;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, "http://analytics.ladorianids.com/visualization/datetimes?timestamp=1514563704601&keyhash=f445657474657fd8288015500c8e4b0c");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);
        $players = $this->filterPlayers($result['data']);
        if (!empty($players)) {
            $this->sendMessage($players);
            $this->info("Se han enviado " . count($players) . " players sin emision");
        } else {
            $this->info("No hay players sin emisiones");
        }
    }


    private function filterPlayers($players)
    {
        $invalid = [];
        foreach ($players as $player) {
            $support = Support::where('idcustomer', $player['idcustomer'])->first();
            $timezone = $support && $support['timezone'] ? $support['timezone'] : 'Europe/Madrid';
            $today =   Carbon::now()->setTimezone($timezone);
            $weekday = $today->dayOfWeek;
            $powers = PlayerPower::where('idplayer', $player['idviewer'])->get();

            if (empty($powers) && $this->is_monitored($player['email'])) {
                $invalid[] = $player;
            } else {
                $powerWeekDay = Collect($powers)->where('weekday', $weekday);
                foreach ($powerWeekDay as $power) {
                    if ($this->checkPower($power, $today, $timezone) && $this->is_monitored($player['email']))
                        $invalid[] = $player;
                }
            }
        }
        $this->info(json_encode($invalid));
        return $invalid;
    }


    private function is_monitored($email)
    {

        return Player::where('email', $email)->where('is_monitored', 1)->first();
    }


    private function sendMessage($players)
    {
        $text = "<p>Estos players estan parados desde hace " . $this->minutes . " minutos</p>";
        $text .= "<ul>";
        foreach ($players as $player) {

            $text .= '<li>' . $player['idviewer'] . ' - ' . $player['email'] . '</li>';
        }
        $text .= "</ul>";

        Mail::send(new AlertDownPlayer("Players sin emision", $text));
    }

    private function checkPower($power, $today, $timezone)
    {
        $init = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $power->time_on)->settings(['timezone'  =>  $timezone])->addMinutes($this->minutes);
        $end  = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $power->time_off)->settings(['timezone'  =>  $timezone]);
        return $today->between($init, $end);
    }
}
