<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Storage;
use App\Http\Controllers\Api\v3\Controllers\AutomatizacionCalculoCtrl;


class AutomatizacionCalculo extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'autocalculo:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera Json de todas campañas para graficos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
       
    }
    public static function autoGenData(){
        $autocal = new AutomatizacionCalculoCtrl();
        $autocal->saveDataProccess();
    }

    public static function generateFilesintegration($idcustomer){
        $autocal = new AutomatizacionCalculoCtrl();
        $autocal->generateFilesintegration($idcustomer);
    }

    public static function saveResumen($ejercicio, $month, $idcustomer){
        $autocal = new AutomatizacionCalculoCtrl();
        $autocal->saveResumen($ejercicio, $month, $idcustomer);
    }

    public static function autoGenDataNum($num){
        $autocal = new AutomatizacionCalculoCtrl();
        $autocal->saveDataProccess($num);
    }

    public static function generacsv(){
        ini_set('memory_limit', '181900M');
        ini_set('max_execution_time', '0');
        ini_set('upload_max_filesize', '2000M');
        ini_set('post_max_size', '2000M');
        $cont=0;
        
        for ($i=0; $i <= 7500000 ; $i=$i+50000) { 
            $tempProductos = \DB::table('faces_by_day')
            ->select('faces_by_day.idfacesbyday','faces_by_day.idviewer','faces_by_day.date','faces_by_day.idcustomer','faces_by_day.url',
                'faces_by_day_data.idfacesbydaydata','faces_by_day_data.idfacesbyday','faces_by_day_data.sex','faces_by_day_data.age' );

            $tempProductos = $tempProductos->join('faces_by_day_data','faces_by_day.idfacesbyday','faces_by_day.idfacesbyday')
            ->where('faces_by_day.idcustomer',31)

            ->where('faces_by_day.date','>=','2020-01-01 00:00:00')
            ->where('faces_by_day.date','<=','2020-02-01 00:00:00')
            
            ->skip($i)->take(50000);

            $tmp_dat='';
            $fil= 'scripts/faces_date_2020_01.csv';
            foreach($tempProductos->get() as $key => $tmp){
                $dat =$tmp->idfacesbyday .','. $tmp->idviewer .','.  $tmp->date .','.$tmp->idcustomer .','. $tmp->url . ','.$tmp->idfacesbydaydata .','.$tmp->sex .','. $tmp->age.PHP_EOL;
                $tmp_dat =$tmp_dat .$dat;
            }
            Storage::disk('public')->append($fil,$tmp_dat);
            echo $cont++.PHP_EOL;
        }
        echo "fin";
       die;
    }



}