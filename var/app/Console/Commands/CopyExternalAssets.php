<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use App\Models\Asset;

class CopyExternalAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:copy-external-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command copy external assets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $domain = request()->getHttpHost();
        $host = "https://". request()->getHttpHost();
    
        $assets = Asset::whereIn('idtype', array(1,2))->where('asset_url', 'not like', "%://$domain%")->where('deleted', 0)->get();
        $bar = $this->output->createProgressBar(count($assets));
        $bar->start();

        foreach($assets as $asset) {
            try {
                if(!empty($asset->asset_url) && !Str::startsWith($asset->asset_url, $host)) {
                    $paths = explode("/", $asset->asset_url);
                    $filename = end($paths);
                    $filename = urlencode($filename);
                    $path = ($asset->idtype === 1)? '/images/assets/' : '/videos/assets/';
                    $final_url = $host . $path . $filename;

                    Log::info("Copy external asset", ["idasset" => $asset->idasset, "origen" => $asset->asset_url, "destino" => $final_url]);
                    if(copy($asset->asset_url, public_path($path).$filename)) {
                        $asset->asset_url = $final_url;
                        $asset->save();
                    } else {
                        throw new \Exception("Error copy $filename");
                    }
                    
                }
            } catch (\Exception $e) {
                Log::error("Copy external asset: ". $e->getMessage(), ["idasset" => $asset->idasset]);
            }
            $bar->advance();
        }

        $bar->finish();
        echo "\n";
        return 0;
    }
}
