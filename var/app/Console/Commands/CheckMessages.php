<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class CheckMessages extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'send-message:check';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza las fechas de los contenidos para permitir varias ';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $messages = \DB::table('messages')->where('status', 2)->where('is_sync', 0)->get();
        $mail =  $this->mailApi();
        $whatsapp = $this->whatsappApi();
        $sms = $this->messageBirdApi();
        foreach ($messages as $key => $message) {
            $this->filterSms($sms, $message);
        }
    }




    private function messageBirdApi()
    {
        $urls = [
            "https://rest.messagebird.com/messages",
        ];
        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $url = $urls[0] . '?access_key=' . $access_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data);
    }



    private function filterSms($sms, $message)
    {
        $affected = [];
        foreach ($sms->items as $key => $value) {
            if ($value->reference == $message->id) {
                $affected = DB::table('messages')
                    ->where('id', $value->reference)
                    ->update([
                        'num_received' => $value->recipients->totalDeliveredCount,
                        'num_opened' => $value->recipients->totalCount,
                        'num_sends' => $value->recipients->totalSentCount,
                        'is_sync' => 1
                    ]);
            }
        }
        return $affected;
    }
    public function  mailApi()
    {
        $urls = [
            "https://rest.messagebird.com/reporting/conversations/email?periodStart=2018-08-01T00%3A00%3A00%2B08%3A00&periodEnd=2022-09-01T00%3A00%3A00%2B08%3A00&groupBy=status"
            // "https://conversations.messagebird.com/v1/conversations"
            //"https://conversations.messagebird.com/v1/conversations/e7eafa6df9d74c24b13c84364a3ee673"
        ];
        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_URL, $urls[0]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data);
    }


    public function whatsappApi()
    {
        // $this->apiWhatsapp();

        $urls = [
            "https://conversations.messagebird.com/v1/conversations"
        ];
        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_URL, $urls[0]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);

        $msg = json_decode($data);
        $response = [];
        foreach ($msg->items as $value) {
            $this->info($value->messages->href);
            $response[] = $this->whatsappDetail($value->messages->href);
        }
        $this->info(json_encode($response));
        curl_close($ch);
        return json_decode($data);
    }




    private function whatsappDetail($url)
    {

        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);

        curl_close($ch);
        return $data;
    }
}
