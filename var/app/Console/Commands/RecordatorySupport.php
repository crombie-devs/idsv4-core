<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Mail\AlertSupportResponsible;
use Illuminate\Support\Facades\Mail;
use App\Models\Ticket;



class RecordatorySupport extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'recordatory-support:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia un email recordatorio de la tarea asignada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
       
     
        $now = DB::raw('NOW()');
        $users = Ticket::where('deleted', 1)->where('notificar_usuario', 1)->where('fecha_alerta' ,'<=' , $now)->with('category')->with('priority')->with('status')
        ->with('user')->with('comments')->with('department')->with('responsible')->get();
       
         foreach($users as $key => $value) {
                 
                 $this->sendEmail($value->responsible->email, $value);
          }

    }  
    public function load(){
      
         $this->handle();

    }


    
   private function sendEmail($email,$sendMail) {
    
    if($sendMail){
        $env = config('app.env');
        Mail::send(new AlertSupportResponsible($email, $env, $sendMail));
    }
   }
}