<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use App\Models\Content;

class GenerateContentThumbnails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-content-thumbnails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command generate content thumbnails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $contents = Content::where('thumbnail_url', null)->where('deleted', 0)->get();
        $bar = $this->output->createProgressBar(count($contents));
        $bar->start();

        foreach($contents as $content) {
            try {
                $assets = $content->assets()->get();
                if(count($assets) > 0)
                    foreach($assets as $asset){
                        if($asset->cover_url) {
                            Log::info("Generar thumbnail by content", ["idcontent" => $content->idcontent, "idasset" => $asset->idasset, "thumbnail" => $asset->cover_url]);
                            $content->thumbnail_url = $asset->cover_url;
                            $content->save();
                            break;
                        }
                    }
            } catch (\Exception $e) {
                Log::error("Generar thumbnail by content: ". $e->getMessage(), ["idcontent" => $content->idcontent]);
            }
            $bar->advance();
        }

        $bar->finish();
        echo "\n";
        return 0;
    }
}
