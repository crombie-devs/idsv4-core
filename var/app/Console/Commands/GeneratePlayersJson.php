<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\Player;

class GeneratePlayersJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:players-json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera un json de todos los players';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $players = Player::select('customers.idcustomer', 'sites.idsite', 'players.idplayer', 'players.email', 'players.code')
            ->join('sites', 'players.idsite', '=', 'sites.idsite')
            ->join('customers', 'customers.idcustomer', '=', 'sites.idcustomer')
            ->where('customers.status', 1)
            ->where('customers.deleted', 0)
            ->where('sites.status', 1)
            ->where('sites.deleted', 0)
            ->where('players.status', 1)
            ->where('players.deleted', 0)
            ->orderBy('customers.idcustomer')
            ->orderBy('sites.idsite')
            ->orderBy('players.idplayer')
            ->get();


        Storage::disk('cache')->put('external/players/actives.json', json_encode($players));
    }
}
