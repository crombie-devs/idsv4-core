<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Messages;
use App\Models\Format;

use App\Models\MsgRelation;

class SendMessage extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'send-message:send';



    protected $signature = 'send-message:send {action=empty}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia un mensaje cada 30 minutos';

    protected $idcontent;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $action = $this->argument('action');


        // $fields  = [1 => 'email', 2 => 'phone', 3 => 'phone'];
        $fields =   $this->getFormatsDispositive();
        $actions =  $this->getFormats();;

        // Obtengo los mensajes que cumplen los parametro de fecha y estado
        $now = DB::raw('NOW()');
        $messages = Messages::where('status', 0)->where('datetime', '<=', $now)->with('contents')->get();


        $count = [];
        if (count($messages) > 0) {
            foreach ($messages as $mk => $mv) {
                $field  = $fields[$mv->idformat];
                $action = $actions[$mv->idformat];
                $tags = [];
                $idcustomer = $mv->contents[0]->idcustomer;
                $this->idcontent =  $mv->contents[0]->idcontent;
                $idsite = $mv->idsite ? $mv[0]->idsite : false;
                $idmessage = $mv->id;
                $body = $mv->body;
                $subject = $mv->subject;

                // Creo una matriz de tags , para buscar los peoples con estas tags
                foreach ($messages as $key => $value) {
                    if (count($value->contents->first()->tags) > 0) {
                        foreach ($value->contents->first()->tags as $k => $v) {
                            $tags[] = $v->idtag;
                        }
                        $tags = array_unique($tags);
                    }
                }


                if ($tags) {
                    // Obtegno los usuarios que coinciden con los tags
                    if ($idsite) {
                        $peoples = DB::table('peoples')->select('peoples.' . $field . '')->join('people_has_tags', 'people_has_tags.idpeople', '=', 'peoples.id')->where('peoples.idcustomer', $idcustomer)
                            ->whereIn('people_has_tags.idtag', $tags)
                            ->where('peoples.idsite', $idsite)->get();
                    } else {
                        $peoples = DB::table('peoples')->select('peoples.' . $field . '')->join('people_has_tags', 'people_has_tags.idpeople', '=', 'peoples.id')->where('peoples.idcustomer', $idcustomer)
                            ->whereIn('people_has_tags.idtag', $tags)->get();
                    }
                } else {
                    if ($idsite) {
                        $peoples = DB::table('peoples')->select('peoples.' . $field . '')
                            ->join('people_has_tags', 'people_has_tags.idpeople', '=', 'peoples.id')
                            ->where('peoples.idcustomer', $idcustomer)
                            ->where('peoples.idsite', $idsite)->get();
                    } else {
                        $peoples = DB::table('peoples')->select('peoples.' . $field . '')
                            ->join('people_has_tags', 'people_has_tags.idpeople', '=', 'peoples.id')
                            ->where('peoples.idcustomer', $idcustomer)->get();
                    }
                }

                $peoples = array_map('unserialize', array_unique(array_map('serialize', $peoples->toArray())));

                // Agrupo en paquetes de n cantidad
                $package = $this->createPackage($peoples, $action, $field);

                // Lanza los envios
                $this->loop($package, $action, $idmessage,  $body, $subject);

                $count[] = $mv;
            }
        } else {
            $this->info('No hay mensajes que cumplan los terminos de busqueda');
        }


        foreach ($count as $mv) {
            $this->updateMessageStatus($mv);
        }
    }


    private function getFormats()
    {
        $formats = [];
        $f = Format::where('broadcasting', 1)->get();

        foreach ($f as $key => $value) {
            $formats[$value['idformat']] = $value['name'];
        }

        return $formats;
    }


    private function getFormatsDispositive()
    {
        $formats = [];
        $f = Format::where('broadcasting', 1)->get();

        foreach ($f as $key => $value) {
            if ($value['name'] == 'email') {
                $formats[$value['idformat']] = 'email';
            } else {
                $formats[$value['idformat']] = 'phone';
            }
        }

        return $formats;
    }



    private function  updateMessageStatus($messages)
    {

        $peoples = DB::table('messages')->where('id', $messages->id)
            ->update(['status' => 2]);
    }


    private function createPackage($peoples, $action, $field)
    {

        $package = [[]];
        if (count($peoples) > 5) {

            $index = -1;
            $loop = substr(count($peoples), 0, -3);
            $resto = substr(count($peoples), -3, 3);

            for ($i = 0; $i < $loop; $i++) {
                $e = 0;
                do {
                    $index++;
                    if ($field == 'phone') {
                        $package[$i][$e] = '+34' . $peoples[$index][$action];
                    } else {
                        $package[$i][$e] = $peoples[$index][$action];
                    }
                    $e++;
                } while ($e <= 5);
            }

            if (intval($resto) > 0) {
                array_push($package[count($package) - 1], intval($resto));
            }
        } else {

            foreach ($peoples as $key => $value) {
                if ($field == "phone") {
                    $package[0][$key] = '+34' . $value->$field;
                } else {
                    $package[0][$key] = $value->$field;
                }
            }
        }

        return $package;
    }


    private function loop($matriz, $action, $idmessage, $body, $subject)
    {


        switch ($action) {

            case 'email':
                foreach ($matriz as $key => $value) {
                    $this->sendEmail($value, $body, $subject);
                }
                break;
            case 'sms':
                foreach ($matriz as $key => $value) {
                    $this->sendSms($value, $idmessage, $body);
                }
                break;
            case 'whatsapp':
                foreach ($matriz as $key => $value) {
                    $this->sendWhatsapp($value, $body);
                }
                break;
        }
    }


    public function load()
    {
        $this->handle();
    }


    private function  sendEmail($mailtos, $body, $subject)
    {

        $mailtos = array_map(function ($e) {
            return array('address' => $e, 'name' => 'jose campos');
        }, $mailtos);

        $mailtos = array_values($mailtos);
        $params = [
            "subject" => $subject,
            "to" => $mailtos,
            "from" => [
                "name" => "Ladorian",
                "address" => "developer@ladorian.com"
            ],
            "content" => [
                "html" => $body
            ]

        ];

        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://email.messagebird.com/v1/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec($ch);
        $this->info($remote_server_output);
        curl_close($ch);
        return json_decode($remote_server_output);
    }

    private function sendWhatsapp($value, $body)
    {

        $this->createStart();
        die();

        // $value = ['+34699564222'];
        // $value = ['+34699934787'];
        // $value = ['+34601200406'];
        // $value = ['+34651331325'];

        $value = array_values($value);



        foreach ($value as $phone) {

            $params  = [
                'to' => $phone,
                'channelId' => '04987107-0476-4e3a-af55-c1555e6fe02d',
                'type' => 'hsm',
                'content' => [
                    "hsm" => [
                        "namespace" => "f89d97ae_4e01_406f_830e_e137a76e165d",
                        "language" =>
                        [
                            "code" => "es"
                        ],
                        "templateName" => $body
                    ],
                ]
            ];
            $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://conversations.messagebird.com/v1/conversations/start");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-type: application/json',
                'Authorization: AccessKey ' . $access_key
            ));

            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $remote_server_output = curl_exec($ch);
            $this->info($remote_server_output);

            curl_close($ch);

            $this->info($remote_server_output);
            $data = json_decode($remote_server_output);

            $obj = ["idmessage" => $data->id, "idcontent" => $this->idcontent, "type" => "whatsapp"];

            $msgRelation = new MsgRelation();
            $msgRelation->fill($obj);
            $msgRelation->save();
        }

        return $remote_server_output;
    }

    private function sendSms($arrayPhones, $idmessage)
    {
        $phones = [];
        foreach ($arrayPhones as $key => $value) {
            $phones[] = $value;
        }
        $MessageBird = new \MessageBird\Client('17ILEMu4wIC3UV8skUZZyX7If');
        $Message = new \MessageBird\Objects\Message();
        $Message->originator = 'Ladorian';
        $Message->recipients = $phones;
        $Message->reference = $idmessage;
        $Message->body = 'Test test test666';
        $response =  $MessageBird->messages->create($Message);
        return $response;
    }


    private function createStart()
    {



        $params = [
            "to" => "34601200406",
            "from" => "04987107-0476-4e3a-af55-c1555e6fe02d",
            "type" => "text",
            "content" =>
            [
                "text" => "Hello!",
                "disableUrlPreview" => false
            ]
        ];

        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://conversations.messagebird.com/v1/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec($ch);
        $this->info($remote_server_output);

        curl_close($ch);

        $this->info($remote_server_output);
    }
}
