<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Storage;
use App\Models\Customer;
use App\Models\Site;

class SitesByCustomer extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:sitesbycustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera Json de los sites por customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
       
    }

    public static function generatedata(){
        $customers = Customer::where('status', 1)->where('deleted', 0)->get();
        foreach($customers as $customer){
            $tmp_dat = [];
            $sites = Site::where('status', 1)->where('deleted', 0)->where('idcustomer', $customer->idcustomer)->get();
            foreach($sites as $site){
                $tmp_dat[] = [
                    "site_id"=> $site->idsite, 
                    "site_code" => $site->code,
                    "customer_id" => $customer->idcustomer,
                    "customer_code" => $customer->code
                ];
            }
            $fil= 'sites/sites.bycustomer.'.$customer->idcustomer.'.json';
            Storage::disk('cache')->put($fil,json_encode($tmp_dat));
        }
        echo "fin";
       die;
    }
}