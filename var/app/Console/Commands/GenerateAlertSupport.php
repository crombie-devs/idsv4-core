<?php namespace App\Console\Commands;
use Illuminate\Console\Command;

use Illuminate\Support\Collection;

use App\Models\Site;
use App\Models\Player;
use App\Models\Camera;
use App\Models\Support;
use App\Models\SupportPlayer;
use App\Models\SupportCamera;
use App\Models\SupportHistorical;
use App\Models\Customer;

use App\Exports\AlertExport;

use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;

use DB;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\AlertSocket;

//Excel
use Maatwebsite\Excel\Facades\Excel;

class GenerateAlertSupport extends Command {
   // use NotificationsTrait;
   private $playerRepository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertsupport:generate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alertas de Support';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {}

    public function getAlertSupportPlayer(){
        $response = array('success' => false, 'data' => '');
        $cu = new Customer;
        $customers = $cu->getCustomers();
        if (!empty($customers)){
            foreach ($customers as $customer) { 
                $rowsPlayer = [];
                $headerPlayer = [];             
                $su = new Support();
                $support = $su->getSupportByCustomer($customer->idcustomer);
                $pl = new Player();
                $players = $pl->getPlayerByCustomer($customer->idcustomer);
                if(!empty($support)){
                    $date = Carbon::now()->setTimezone($support->timezone);
                    if(!empty($support->last_email)){
                        $frecuency = $support->last_email->addMinutes($support->repetition_frecuency);   
                        if($date < $frecuency)
                            continue;  
                    }
                    $sh = new SupportHistorical();
                    foreach ($players as $player) {
                        if (!self::ckeckPower($player->powers, $date)){
                            continue;
                        }
                        $sp = new SupportPlayer();
                        $supportPlayer = $sp->getSupportPlayerByCustomerByPlayer($customer->idcustomer, $player->idplayer); 
                        if(!empty($supportPlayer)){
                            $socket = "S";
                            $emission = "S";
                            $add = false;
                            if(!self::ckeckSocket($player->idplayer)){
                                if ($supportPlayer->sendmail == 1) {
                                    $sp->updateP($player->idplayer, ['sendmail' => 0]);
                                    $socket = "N";
                                }else{
                                    $contSendMail = $player->sendmail+1;
                                    $sp->updateP($player->idplayer, ['sendmail' => $contSendMail]);
                                }
                            }else{
                                $sp->updateP($player->idplayer, ['sendmail' => 0]);
                                $add = true;
                            }
                            if ($add == true || $socket == "N") {
                                $emissions = self::getEmission($player->idplayer, $date);
                                if (empty($emissions)) {
                                    $id = $sp->updateP($player->idplayer, ['emission' => 0]);
                                    $emission = "N";
                                }
                            }
                            $lastEmission = 'Emitiendo';
                            if (($emission == "N" &&  $socket == "N") || ($emission == "S" && $socket == "N") || ($emission == "N" && $socket == "S")) {
                                if ($emission == "N") {
                                    $lastEmissions = self::getLastEmission($player->idplayer);
                                    if (count($lastEmissions) > 0 && count($lastEmissions[0]->hours) > 0) {
                                        $h = [];
                                        foreach ($lastEmissions[0]->hours as $hour) {
                                            $h[] = $hour->hour;
                                        }
                                        $dt = Carbon::create($lastEmissions[0]->date);
                                        $min = "00";
                                        $sec = "00";
                                        if (!empty($lastEmissions[0]->updated_at)) {
                                            $updated_at = Carbon::create($lastEmissions[0]->updated_at)->setTimezone($support->timezone);
                                            $min = $updated_at->minute;
                                            $sec = $updated_at->second;
                                        }
                                        $lastEmission = $dt->format('d-m-Y').' '.str_pad(max($h), 2, "0", STR_PAD_LEFT).':'.str_pad($min, 2, "0", STR_PAD_LEFT).':'.str_pad($sec, 2, "0", STR_PAD_LEFT);
                                    }
                                }
                                $rowsPlayer[] = array($customer->idcustomer, $customer->name, $player->idsite, $player->name,  $player->idplayer, $player->email, $socket, $emission, $lastEmission);
                                $data = [   
                                    'idcustomer' => $customer->idcustomer,
                                    'idsite' =>  $player->idsite,
                                    'idplayer' => $player->idplayer,
                                    'date_incident' => $date,
                                    'socket' => $socket == 'S'? true: false,
                                    'value' =>  $emission == 'S'? true: false,
                                ];
                                $id = $sh->insertGetId($data);
                            }
                            $id = $sp->updateP($player->idplayer,['last_test_hour' => $date]);        
                        } else {
                            $data = [ 
                                'idcustomer' => $customer->idcustomer,
                                'idsite' =>  $player->idsite,
                                'idplayer' => $player->idplayer,
                                'last_test_hour' => $date
                            ];
                            $id = $sp->insertGetId($data); 
                        }
                    }
                }                  
                if(count($rowsPlayer) > 0){
                    $headerPlayer[] = array("idcustomer", "name", "idsite", "name", "idplayer", "email", "Conexion Socket", "Emisiones", "Ultima Emision");
                    array_unshift($rowsPlayer, $headerPlayer);
                    $filename = $customer->name.'_Incidencias_emisiones_desde_'.$date->format('d-m-Y_H,i').".csv";
                    $csv = self::exportCsv($filename, $rowsPlayer);
                    $subject = $customer->name."_player con incidencias desde ".$date->format('d-m-Y H:i');                       
                    Mail::send(new AlertSocket($customer->idcustomer, $player->idplayer, $filename, $subject));
                    $id = $su->updateS($support->idsupport, ['last_email' => $date]);                        
                    Storage::disk('alert')->delete($filename);                              
                }
            }    
        }           
    }
    public function getAlertSupportCamera(){
        $response = array('success' => false, 'data' => ''); 
        $cu = new Customer;
        $customers = $cu->getCustomers();
        if (!empty($customers)){
            foreach ($customers as $customer) { 
                $rowsCamera = [];
                $headerCamera = [];            
                $su = new Support();
                $support = $su->getSupportByCustomer($customer->idcustomer);
                $ca = new Camera();
                $cameras = $ca->getCameraByCustomer($customer->idcustomer);
                if(!empty($support)){
                    $date = Carbon::now()->setTimezone($support->timezone);
                    $sh = new SupportHistorical();                    
                    foreach ($cameras as $camera) {
                        if (!self::ckeckPower($camera->powers, $date))
                            continue;
                        $sc = new SupportCamera();
                        $supportCamera = $sc->getSupportCameraByCustomerByCamera($customer->idcustomer, $camera->idcamera);          
                        if(!empty($supportCamera)){
                            $socket = "S";
                            $afluence = "S";
                            $add = false;                                                
                            if (!self::ckeckSocketCameras($camera->email)) {
                                $id = $sc->updateC($camera->idcamera, ['socket' => 0]);
                                $socket = "N";
                            }
                            $afluences = self::getAfluences($camera->idcamera, $date);
                            if (empty($afluences)) {
                                $id = $sc->updateC($camera->idcamera, ['camera' => 0]);
                                $afluence = "N";
                                $add = true;
                            }
                            $lastAfluence = '';
                            if($add){
                                $lastAfluences = self::getLastAfluences($camera->idcamera); 
                                if(count($lastAfluences)>0){
                                    $dt = Carbon::create($lastAfluences[0]->date);
                                    $lastAfluence = $dt->format('d-m-Y').' '.$lastAfluences[0]->time;
                                }
                            }
                            $id = $sc->updateC($camera->idcamera,['last_test_hour' => $date]);
                            if($add){
                                $rowsCamera[] = array($customer->idcustomer, $customer->name, $camera->idsite, $camera->name,  $camera->idcamera, $camera->email, $socket, $afluence, $lastAfluence); 
                                $data = [ 'idcustomer' => $customer->idcustomer,
                                    'idsite' =>  $camera->idsite,
                                    'idcamera' => $camera->idcamera,
                                    'date_incident' => $date,
                                    'socket' => $socket == 'S'? true: false,
                                    'value' =>  $afluence == 'S'? true: false,
                                ];
                                $id = $sh->insertGetId($data);                             
                            }
                        } else {                        
                            $data = [ 'idcustomer' => $customer->idcustomer,
                                    'idsite' =>  $camera->idsite,
                                    'idcamera' => $camera->idcamera,
                                    'last_test_hour' => $date
                            ];
                            $id = $sc->insertGetId($data); 
                        }
                    }
                    if(count($rowsCamera)>0){
                        $headerCamera[] = array("idcustomer", "name", "idsite", "name", "idcamera", "email", "Conexion Socket", "Afluencias", "Ultima Afluencia");
                        array_unshift($rowsCamera, $headerCamera);
                        $filename =  $customer->name.'_Incidencias_afluencias_desde_'.$date->format('d-m-Y_H,i').".csv";
                        $csv = self::exportCsv($filename, $rowsCamera);
                        $subject =  $customer->name."_Camaras con incidencias desde ".$date->format('d-m-Y H:i'); 
                        Mail::send(new AlertSocket($customer->idcustomer, $camera->idcamera, $filename, $subject));  
                        $id = $su->updateS($support->idsupport,['last_email' => $date]);            
                        Storage::disk('alert')->delete($filename); 
                    } 
                }    
            }           
        }
    }
    public static function ckeckSocket($idplayer){
        // PAra local
        //$client = new \GuzzleHttp\Client(['verify' => 'C:\wamp64\bin\php\php7.4.0\extras\ssl\cacert.pem']);  
        $client = new \GuzzleHttp\Client();  

        $room = "viewer-".$idplayer; 
        $data = [
            "publish_key" => "pub-c-2478datb-aeba-425t-dt52-9d6b6c68id53",
            "subscribe_key" => "sub-c-se83462d-cv32-89s6-a9dy-03vf4jdio3sk",
            "room" => $room           
        ];
        $response = $client->request('POST','https://pusher.ladorianids.com/private/room', ['json' => $data]);
        $json = json_decode($response->getBody());
        $found = array_search($room,$json->data );
        return $found;
    }
    public static function ckeckSocketCameras($email){
        $client = new \GuzzleHttp\Client();  
        $room = "camera-".$email; 
        $data = [
            "publish_key" => "pub-c-2478datb-aeba-425t-dt52-9d6b6c68id53",
            "subscribe_key" => "sub-c-se83462d-cv32-89s6-a9dy-03vf4jdio3sk"          
        ];        
        $response = $client->request('POST','https://pusher.ladorianids.com/private/cameras/room', ['json' => $data]);
        $json = json_decode($response->getBody());
        $found = array_search($room,$json->data );
        if(!$found){
            $room = "camera.email.".$email;
            $found = array_search($room,$json->data); 
        }
        return $found;
    }
    public static function getEmission($idplayer, $date){
        $client = new \GuzzleHttp\Client();
        $ts =  Carbon::now('UTC')->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = md5($string);
        $data = [
            "idviewer" => $idplayer,
            "date" => $date->toDateString()         
        ];
        $response = $client->request('POST','http://analytics.ladorianids.com/viewer/byViewer?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $data]);
        $json = json_decode($response->getBody());
        return $json->data;
    }
    public static function getLastEmission($idplayer){
        $client = new \GuzzleHttp\Client();
        $ts =  Carbon::now('UTC')->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = md5($string);
        $date = Carbon::now('UTC');
        $data = [
            "idviewer" => $idplayer        
        ];
        $response = $client->request('POST','http://analytics.ladorianids.com/viewer/last?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $data]);
        $json = json_decode($response->getBody());
        return $json->data;
    }
    public static function getAfluences($idcamera, $date){
        $client = new \GuzzleHttp\Client();
        $ts =  Carbon::now('UTC')->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);
        $data = [
            "camera_id" => $idcamera,
            "date" => $date->toDateString()         
        ];
        $response = $client->request('POST','https://people.ladorianids.es/ws/people/groupByCamera?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $data]);
        if(empty($response->getBody())){
            $data = [
                "camera_id" => $idcamera,
                "date" => $date->subDays(1)->toDateString()         
            ];
            $response = $client->request('POST','https://people.ladorianids.es/ws/people/groupByCamera?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $data]);
        }
        $json = json_decode($response->getBody());
        return $json->data;
    }
    public static function getLastAfluences($idcamera){
        $client = new \GuzzleHttp\Client();
        $ts =  Carbon::now('UTC')->timestamp;
        $current_timestamp = $ts*1000;
        $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
        $keyhash = hash('sha256',$string);
        $date = Carbon::now('UTC');
        $data = [
            "camera_id" => $idcamera        
        ];
        $response = $client->request('POST','https://people.ladorianids.es/ws/people/last?timestamp='.$current_timestamp.'&keyhash='.$keyhash, ['json' => $data]);
        $json = json_decode($response->getBody());
        return $json->data;
    }
    public static function ckeckPower($powers, $date){
        $weekday = [];
        if(count($powers) == 0)
            return true;
        foreach($powers as $power){  
            $weekday[]= $power->weekday;
        }
        $diff = array_diff($weekday, [1, 2, 3, 4, 5, 6, 7]);
        foreach($powers as $power){   
            if (in_array($date->dayOfWeekIso, $diff)) 
                return false; 
            if ($power->weekday == $date->dayOfWeekIso) {
                $h = explode(":", $power->time_on);
                if ($h[0] == str_pad($date->hour, 2, "0", STR_PAD_LEFT)) {
                    return false;
                }
                return $date->between($power->time_on, $power->time_off, false);
            }  
        }
        return false;
    }
    public static function exportCSV(string $name, $data){
        $export = new AlertExport($data);        
        return Excel::store($export, $name, 'alert');
    }
}