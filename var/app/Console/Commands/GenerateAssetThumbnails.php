<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use App\Traits\ImageUpload;
use App\Models\Asset;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;
use Image;
use File;
use Thumbnail;

class GenerateAssetThumbnails extends Command
{
    use ImageUpload;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-asset-thumbnails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command generate asset thumbnails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $host = request()->getSchemeAndHttpHost();
        $path = '/thumbnail/assets/';
        
        $assets = Asset::where('cover_url', null)->where('deleted', 0)->get();
        $bar = $this->output->createProgressBar(count($assets));
        $bar->start();

        foreach($assets as $asset) {
            try {
                $url = explode('/', $asset->asset_url);
                $name = array_pop($url);
                array_push($url, rawurlencode($name)); 

                $asset_url = implode('/', $url);
                $filename = "$asset->idasset.jpg";
                $thumbnail_url = $host . $path . "$asset->idasset.jpg";
                if($asset->idtype === 1) { 
                    // Image
                    Log::info("Generar image by asset", ["idasset" => $asset->idasset, "thumbnail" => $thumbnail_url, "assetURL" => $asset_url, "idtype" => $asset->idtype]);
                    Image::make($asset_url)->resize(320, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(public_path($path).$filename);
                    $asset->cover_url = $thumbnail_url; 
                } else if($asset->idtype === 2) { 
                    // Video
                    Log::info("Generar video by asset", ["idasset" => $asset->idasset, "thumbnail" => $thumbnail_url, "assetURL" => $asset_url, "idtype" => $asset->idtype]);
                    Thumbnail::getThumbnail($asset_url, public_path($path), $filename, 3);
                    $asset->cover_url = $thumbnail_url;                
                } else { 
                    // Web
                    Log::info("Generar webpage by asset", ["idasset" => $asset->idasset, "thumbnail" => $thumbnail_url, "assetURL" => $asset_url, "idtype" => $asset->idtype]);
                    Browsershot::url($asset_url)
                        ->windowSize(1920, 1080)
                        ->fit(Manipulations::FIT_CONTAIN, 640, 360)
                        ->setOption('args', ['--no-sandbox'])
                        ->setScreenshotType('jpeg', 100)
                        ->setDelay(3000) // miliseconds
                        ->save(public_path($path).$filename);
                    $asset->cover_url = $thumbnail_url; 
                }
                $asset->save();
            } catch (\Exception $e) {
                Log::error("Generar cover by asset: ". $e->getMessage(), ["idasset" => $asset->idasset]);
            }
            $bar->advance();
        }

        $bar->finish();
        echo "\n";
        return 0;
    }
}
