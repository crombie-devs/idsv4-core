<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Site;
use App\Models\Audio;
use App\Models\Content;
use App\Models\AudioCategory;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;



use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
/* use Illuminate\Support\Facades\Log; */
use App\Traits\NotificationsPusher;

class GenerateAudioList extends Command
{
    // use NotificationsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'audio:generate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear json audios';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*  $date = Carbon::today();
        Log::info("Iniciando Cron GenerarPlaylist".$date->toISOString()); */
    }


    public function getAudiosByCustomer(int $idcustomer)
    {

        $date = Carbon::today();
        $players = [];
        //Log::info("Generar Players: Inicio ".$date->toISOString());        
        $players = AudioCategory::audioToCacheByCustomer($idcustomer);

        $audios = [];

        foreach ($players as $audio) {

            foreach ($audio->audio as $val) {
                $audios[] = $val;
            }
        }

        $name_file = "audios/audio-" . $players[0]->customer[0]->email . ".json";
        try {
            if (Storage::disk('cache')->exists($name_file))
                Storage::delete($name_file);
            if (!empty($audios))
                Storage::disk('cache')->put($name_file, json_encode($audios));
        } catch (\Exception $e) {
            // Log::error("Generar Players: ".$e->getMessage());
        }
    }
}
