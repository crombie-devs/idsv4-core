<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use App\Models\RecomendationsTrend;

class RecomendationsTrendsLoader extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recomendations-trends:loader';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loads recomendations trends';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return 0;
    }
    public function loadTrends(){
        $customers = Customer::select('idcustomer','category_level')->where('has_trends',1)->get();
        try {
            $client = new \GuzzleHttp\Client();
            //$url = "http://localhost:5000/recomendations-trends";
            $url = "http://82.223.69.121:5000/recomendations-trends";
            $data = json_encode($customers, true);
            $data = json_decode($data);
            $responsePython = $client->request('POST',$url, ['json'=>$data, 'timeout' => 0]);
            $data = json_decode($responsePython->getBody(true)->getContents());
            $this->saveRecomendationsTrends($data->data, $customers);
        } catch (\GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $responsePython = $exception->getResponse()->getBody(true);
            $data = $responsePython->getBody(true)->getContents();
        }
    }

    private function saveRecomendationsTrends($data){
        $customer = '';
        foreach($data as $tr){
            if($customer != $tr->idcustomer){
                $deletedRows = RecomendationsTrend::where('idcustomer', $tr->idcustomer)->delete();
                $customer = $tr->idcustomer;
            }

            $d['idcustomer'] = $tr->idcustomer;
            $d['idcategory'] = $tr->id_category;
            $d['sales_last_4_weeks'] = $tr->sales_last_4_weeks;
            $d['trend'] = $tr->trend;
            $d['weight'] = $tr->weight;
            $d['trend_last_07days'] = $tr->trend_last_07days;
            $d['trend_last_30days'] = $tr->trend_last_30days;
            $d['trend_last_90days'] = $tr->trend_last_90days;
            $d['previous_day_menos1'] = $tr->previous_day_menos1;
            $d['previous_day_menos2'] = $tr->previous_day_menos2;
            $d['previous_day_menos3'] = $tr->previous_day_menos3;
            $d['previous_day_menos4'] = $tr->previous_day_menos4;
            $d['previous_day_menos5'] = $tr->previous_day_menos5;
            $d['previous_day_menos6'] = $tr->previous_day_menos6;
            $d['previous_day_menos7'] = $tr->previous_day_menos7;
            $d['idlocalarea'] = $tr->local_area;
            if(isset($tr->brands_number) || $tr->brands_number > 0){
                $d['colour_index'] = $tr->colour_index;
                $d['brands_number'] = $tr->brands_number;
                $d['name_brand_1'] = $tr->name_brand_1;
                $d['weight_brand_1'] = $tr->weight_brand_1;
                $d['name_brand_2'] = $tr->name_brand_1;
                $d['weight_brand_2'] = $tr->weight_brand_1;
                $d['name_brand_3'] = $tr->name_brand_1;
                $d['weight_brand_3'] = $tr->weight_brand_1;
            }
            $trend = new RecomendationsTrend();
            $trend->fill($d);
            $trend->save();
        }
    }
}
