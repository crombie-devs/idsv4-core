<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Storage;
use App\Models\Asset;
use Illuminate\Support\Carbon;

class ArchiveExpiredAssets extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'archive:expiredassets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera Json de los sites por customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
       
    }

    public static function archiveExpiredAssets(){
        $today = Carbon::today();
        $assets = Asset::whereNotNull('expired_date')->where('expired_date','<', $today)->where('deleted', 0)->get();
        foreach($assets as $asset){
            $asset->deleted = true;
            $asset->save();
        }
        echo "Se han archivado " . count($assets) . " assets.";
        die;
    }
}