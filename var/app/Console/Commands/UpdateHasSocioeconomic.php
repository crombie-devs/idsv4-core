<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class UpdateHasSocioeconomic extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'update:hassocioeconomic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el campo has_socioeconomic de la tabla site atendiendo si hay datos en las tablas profile-demografic y profile_economic ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        
       
    }  
    public function load(){

        $sites = \DB::table('sites')->get();   
        
       
 
        foreach($sites as $site){
            
            $profile = \DB::table('profile_economic')->where('idsite', $site->idsite)->exists();
           
            if($profile)
                \DB::table('sites')->where('idsite',$site->idsite)->update(['has_socioeconomic' => 1]);       

        }
        

    }





}