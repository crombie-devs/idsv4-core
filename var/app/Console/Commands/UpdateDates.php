<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class UpdateDates extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'update:dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza las fechas de los contenidos para permitir varias ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
    }  
    public function load(){
        $contents = \DB::table('contents')->get();   
        foreach($contents as $content){ 
            $data = [ 'idcontent' => $content->idcontent,
            'date_on' =>  $content->date_on,
            'date_off' => $content->date_off
            ];
            $profile = \DB::table('contents_dates')->insert($data);
        }
    }
}