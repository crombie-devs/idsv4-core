<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Storage;
use App\Models\Product;
use App\Models\Customer;
use App\Models\ProductCategory;

class UpdateProductsData extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'updateproducts:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza los datos de las tablas products y product_categories para que carguen con idcategory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
       
    }

    public static function updateProductCategories(){
        $categories = ProductCategory::all();
        foreach($categories as $cat){
            if(isset($cat['idparent'])){
                $parent = \DB::table('product_categories')->where('idcustomer', $cat['idcustomer'])->where('code', $cat['idparent'])->first();
                $cat['idparent'] = $parent->idcategory;
                $cat->save();
            }
        }
        echo "Tabla 'product_categories' actualizada";
    }

    public static function updateProducts(){
        $products = Product::all();
        foreach($products as $prod){
            if(isset($prod['idcategory'])){
                $category = \DB::table('product_categories')->where('idcustomer', $prod['idcustomer'])->where('code', $prod['idcategory'])->first();
                if($category)
                    $prod['idcategory'] = $category->idcategory;
                else
                    echo "No se ha encontrado la categoría: " . json_encode($prod);
            }
            if(isset($prod['idsubcategory'])){
                $subcategory = \DB::table('product_categories')->where('idcustomer', $prod['idcustomer'])->where('code', $prod['idsubcategory'])->first();
                if($subcategory)
                    $prod['idsubcategory'] = $subcategory->idcategory;
                else
                    echo "No se ha encontrado la subcategoría: " . json_encode($prod);
            }
            $prod->save();
        }
        echo "Tabla 'products' actualizada";
    }

    public static function updateContentsProducts(){
        $cp = \DB::table('contents_has_products')->get();
        foreach($cp as $c){
            $product = Product::where('code', $c->product_code)->first();
            if($product)
                DB::table('contents_has_products_new')->insert(
                    ['idcontent' => $c->idcontent, 'idproduct' => $product['idproduct']]
                );
        }
        echo "Tabla 'contents_has_products' actualizada";
    }

    public static function updateProductsTags(){
        $cp = \DB::table('products_has_tags')->get();
        foreach($cp as $c){
            $product = Product::where('code', $c->idproduct)->first();
            if($product){
                \DB::table('products_has_tags')->where('idproduct', $c->idproduct)->where('idtag',$c->idtag)->update(['idproduct' => $product->idproduct, 'idtag' => $c->idtag]);
            }
        }
        echo "Tabla 'products_has_tags' actualizada";
    }

    public static function updateProductCategory(){
        $csv = array_map('str_getcsv', file(public_path('data.csv')));
        foreach($csv as $i=>$c){
            if($i > 0){
                if($c[6] == 31){
                    $category = ProductCategory::where('code', $c[1])->where('idcustomer', $c[6])->first();
                }else{
                    $category = ProductCategory::where('code', $c[0])->where('idcustomer', $c[6])->first();
                }
                if($category){
                    $product = Product::where('idcustomer', $c[6])->where('code',$c[5])->first();
                    if($product){
                        $product->idcategory = $category->idcategory;
                        $product->save();
                    }else{
                        $customer = Customer::find($c[6]);
                        if($customer){
                            $data['idcustomer'] = $c[6];
                            $data['idcategory'] = $category->idcategory;
                            $data['code'] = $c[5];
                            $data['name'] = $c[8];
                            Product::create($data);
                        }
                    }
                }else{
                    echo "No he encontrado la categoria con code " . $c[6] == 31 ? $c[1] : $c[0] . "\n";
                }
            }
        }    
        echo "Ha guardado correctamente los productos";
    }

}