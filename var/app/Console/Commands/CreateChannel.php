<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use function PHPSTORM_META\map;

class CreateChannel extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'send-message:create-channel {name}  {domainName}';






    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un canal de email en message bird';



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   

        $name = $this->argument('name');
        $domainName = $this->argument('domainName');
   
        $this->info($name);
        $this->info($domainName);


        $params = [ "name" => $name , "domainName"=> $domainName ];

        $access_key = "17ILEMu4wIC3UV8skUZZyX7If";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://email.messagebird.com/v1/channels");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'Authorization: AccessKey ' . $access_key
        ));

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec($ch);
        $this->info($remote_server_output);
        curl_close($ch);

     }

}