<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Site;
use App\Models\Player;
use App\Models\Content;
use App\Models\PlayCircuit;
use App\Models\Customer;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Traits\NotificationsPusher;

//Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\AlertGeneratePlaylist;
use Str;

class BuildExternalPlayList extends Command
{
    // use NotificationsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contents:generate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear json contents';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::today();
        Log::info("Iniciando Cron GenerarPlaylist" . $date->toISOString());
    }
    
    public function getPlayersByCustomer(int $idcustomer) {
        $players = [];
        $errors = [];
        $customer = Customer::where('idcustomer', $idcustomer)->first();
        Log::info("Generar Players: Inicio " . Carbon::today()->toISOString());
        $players = Player::getDetailExternalAllPlayers($idcustomer);
        $errors = self::generateFilePlayers($players, $customer);
        if (count($errors) > 0) {
            $env = config('app.env');
            Mail::send(new AlertGeneratePlaylist(array_slice($errors, 0, 20), "Players de " . $customer->name, $env));
        }
        return count($errors);
    }

    private static function generateFilePlayers($players, $customer) {
        $date = Carbon::today();
        $errors = [];
        foreach ($players as $player) {
            //$reference = $player['viewer']['reference'];
            $reference = $player['viewer']['reference'] ? $player['viewer']['reference'] : $player['viewer']['email'];
            $name_file = Str::slug($customer->name) . "/player/" . $reference . ".json";
            try {
                if (Storage::disk('cache')->exists($name_file))
                    Storage::delete($name_file);
                if (!empty($player))
                    Storage::disk('cache')->put($name_file, json_encode($player));
            } catch (\Exception $e) {
                Log::error("Generar Players: " . $e->getMessage());
                $errors[] = "Error players-" . $player->email . ": " . $e->getMessage() . " Date:" . $date->toDateTimeString();
            }
        }
        return $errors;
    }

    public function getPlayersBySite(int $idsite, bool $sendMail = false)
    {
        $date = Carbon::today();
        $players = [];
        $errors = [];
        Log::info("Generar Players: Inicio " . $date->toISOString());
        $site = Site::width('customer')->where('idsite', $idsite)->first();
        $players = Player::getDetailPlayerBySite($idsite);
        $errors = self::generateFilePlayers($players, $site->customer());
        if ($sendMail) {
            $env = config('app.env');
            Mail::send(new AlertGeneratePlaylist($errors, "Players", $env));
        }
        return count($errors);
    }

    public function getPlayersByPlayer(int $idplayer, bool $sendMail = false)
    {
        $date = Carbon::today();
        $players = [];
        $errors = [];
        Log::info("Generar Players: Inicio " . $date->toISOString());
        $players = Player::getDetailPlayerById($idplayer);
        $player = Player::with('site.customer')->where('idplayer', $idplayer)->firs();
        $errors = self::generateFilePlayers($players, $player->customer());
        if ($sendMail) {
            $env = config('app.env');
            Mail::send(new AlertGeneratePlaylist($errors, "Players", $env));
        }
        return count($errors);
    }

    public function getContentsBySite(int $idsite, $date = null, bool $sendMail = false)
    {
        $date = empty($date) ? Carbon::today() : $date;
        $sites = [];
        $errors = [];
        Log::info("Generar Content: Inicio " . $date->toISOString());
        $sites = Site::filterBySite($idsite);
        $errors = self::generateFileContents($sites,  $date);
        if ($sendMail) {
            $env = config('app.env');
            Mail::send(new AlertGeneratePlaylist($errors, "Contents", $env));
        }
        return count($errors);
    }

    public function getContentsByCustomer($idcustomer) {
        $errors = [];
        Log::info("Generar Content: Inicio " . Carbon::today()->toISOString());
        //Obtener los player de otis
        $players =  Player::getDetailPlayerByCustomer($idcustomer);
        //Obtener para otis para mañana
        $contents = Content::getContentsTomorrowByCustomer($idcustomer);
        //Obtener circuitos
        $circuits = PlayCircuit::getCircuitsByCustomer($idcustomer);
        $errors = self::generateFile($players, $contents, $circuits, $idcustomer);
        if (count($errors) > 0) {
            $env = config('app.env');
            Mail::send(new AlertGeneratePlaylist(array_slice($errors, 0, 20), "Contents de OTIS", $env));
        }
        return count($errors);
    }

    private static function generateFile($players, $contents, $circuits, $idcustomer) {
        $date = Carbon::today();
        $errors = [];
        foreach ($players as $player) {
            $data = self::getValidData($contents, $circuits, $player, $date);
            $categories = Player::getPlayLogicsExternal($player,$idcustomer);

            $obj = [
                'playlogic' => $categories,
                'contents' => $data['validContents'],
                'powers' => $data['validPowers']
            ];
            $customer = Customer::where('idcustomer', $idcustomer)->first();
            $reference = $player->code ? $player->code : $player->email;

            $name_file = Str::slug($customer->name) . "/playlist/" . $reference . ".json";
            array_merge($errors, $data['errors']);
            try {
                if (Storage::disk('cache')->exists($name_file))
                    Storage::delete($name_file);
                if (!empty($player))
                    Storage::disk('cache')->put($name_file, json_encode($obj));
            } catch (\Exception $e) {
                Log::error("Generar Players: " . $e->getMessage());
                $errors[] = "Error players-" . $player->email . ": " . $e->getMessage() . " Date:" . $date->toDateTimeString();
            }
        }
        return $errors;
    }

    private static function getValidData($contents, $circuits, $player, $date) {
        $validContents = [];
        $validPowers = [];
        $errors = [];

        foreach ($contents as $content) {
            if (!empty($content->asset_url)) {
                $has_area = self::checkAreas($player->area, $content);
                $has_site = self::checkSiteContent($player->site, $content);
                $has_all = self::checkAll($player->site, $player->city, $player->province, $player->country, $content);
                $result = $has_area && ($has_site || $has_all);
                if ($result) {
                    $validContents[] = [
                        "idcategory" => $content->idcategory,
                        "idcampaign" => NULL,
                        "idtype" => $content->idtype,
                        "idcontent" => $content->idcontent,
                        "name" => $content->name,
                        "reference" => $content->code,
                        'date_on' => $content->date_on,
                        'date_off' => $content->date_off,
                        'duration' => $content->duration,
                        'asset_idtype' => $content->asset_idtype,
                        'asset_type' => $content->asset_type,
                        'asset_url' => $content->asset_url,
                        'size' => $content->size
                    ];
                    if (!empty($content->powers) && count($content->powers) > 0)
                        $validPowers = array_merge($validPowers, $content->powers->toArray());
                }
            } else {
                $errors[] = "Error players-" . $player->email . ": No hay asset válido para el contenido " . $content->idcontent . " Date:" . $date->toDateTimeString();
            }
        }
        $result = ['validContents' => $validContents, 'validPowers' => $validPowers, 'errors' => $errors];
        return $result;
    }
    private static function checkAreas($area, $content) {
        $valid = true;
        if ($content->playareas && count($content->playareas) > 0)
            $valid = (array_search($area->idarea, array_column($content->playareas->toArray(), 'idarea')) !== false) ? true : false;
        return $valid;
    }

    private static function checkSiteContent($site, $content) {
        $valid = true;
        if ($content->sites && count($content->sites) > 0)
            $valid = array_search($site->idsite, array_column($content->sites->toArray(), 'idsite')) ? true : false;
        return $valid;
    }

    private static function checkSiteCircuit($site, $content) {
        $valid = true;
        if ($content->playcircuits && count($content->playcircuits) > 0) {
            foreach ($content->playcircuits as $circuit) {
                if (count($circuit->sites) > 0)
                    $valid = array_search($site->idsite, array_column($circuit->sites->toArray(), 'idsite')) ? true : false;
                if ($valid)
                    return true;
            }
        }
        return $valid;
    }

    private static function checkCityCircuit($city, $content) {
        $valid = true;
        if ($content->playcircuits && count($content->playcircuits) > 0) {
            foreach ($content->playcircuits as $circuit) {
                $valid = !(count($circuit->sites) > 0);
                if (count($circuit->cities) > 0)
                    $valid = array_search($city->idcity, array_column($circuit->cities->toArray(), 'idcity')) ? true : false;
                if ($valid)
                    return true;
            }
        }
        return $valid;
    }

    private static function checkProvinceCircuit($province, $content) {
        $valid = true;
        if ($content->playcircuits && count($content->playcircuits) > 0) {
            foreach ($content->playcircuits as $circuit) {
                $valid = !(count($circuit->sites) > 0);
                if (count($circuit->provinces) > 0)
                    $valid = array_search($province->idprovince, array_column($circuit->provinces->toArray(), 'idprovince')) ? true : false;
                if ($valid)
                    return true;
            }
        }
        return $valid;
    }

    private static function checkCountryCircuit($country, $content) {
        $valid = true;
        if ($content->playcircuits && count($content->playcircuits) > 0) {
            foreach ($content->playcircuits as $circuit) {
                $valid = !(count($circuit->sites) > 0);
                if (count($circuit->countries) > 0)
                    $valid = array_search($country->idcountry, array_column($circuit->countries->toArray(), 'idcountry')) ? true : false;
                if ($valid)
                    return true;
            }
        }
        return $valid;
    }

    private static function checkLocation($city, $province, $country, $content) {
        return self::checkCityCircuit($city, $content) && self::checkProvinceCircuit($province, $content) && self::checkCountryCircuit($conuntry, $content);
    }

    private static function checkAll($site, $city, $province, $country, $content) {
        $check_sites = self::checkSiteCircuit($site, $content);
        $check_locations = self::checkLocation($city, $province, $country, $content);
        return $check_sites || $check_locations;
    }
    private static function generateFileContents($sites, $date = null)
    {
        $date = empty($date) ? Carbon::today() : $date;
        $errors = [];
        $weekday = $date->weekday() + 1;
        foreach ($sites as $site) {
            $contents = Content::getContentsForSite($site->idsite, $date);
            $name_file = "contents/" . $weekday . "/contents-" . $site->idsite . ".json";
            try {
                if (Storage::disk('cache')->exists($name_file))
                    Storage::delete($name_file);
                Storage::disk('cache')->put($name_file, json_encode($contents));
            } catch (\Exception $e) {
                Log::error("Generar Contents: " . $e->getMessage());
                echo "Generar Contents: " . $e->getMessage();
                $errors[] = "Error content-" . $site->idsite . ": " . $e->getMessage() . " Date:" . $date->toDateTimeString();
            }
        }
        return $errors;
    }
}
