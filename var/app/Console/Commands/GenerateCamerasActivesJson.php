<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\Camera;

class GenerateCamerasActivesJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:cameras-actives-json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera un json de todas las camaras activas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cameras = Camera::select('customer.idcustomer as customer_id', 'customer.code as customer_code', 'site.idsite as site_id', 'site.code as site_code', 'idcamera as camera_id', 'code as camera_code', 'email')
            ->with('site')
            ->with('customer')
            ->where('status', true)
            ->where('deleted', false)
            ->where('site.status', true)
            ->where('site.deleted', false)
            ->where('customer.status', true)
            ->where('customer.deleted', false)
            ->get();

        Storage::disk('cache')->put('external/cameras/actives.json', json_encode($cameras));
    }
}
