<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Storage;
use App\Models\Customer;
use App\Models\Camera;

class CamerasByCustomer extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:camerasbycustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera Json de las cámaras por customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
       
    }

    public static function generatedata(){
        $customers = Customer::where('status', 1)->where('deleted', 0)->get();
        foreach($customers as $customer){
            $tmp_dat = [];
            $cameras = Camera::where('status', 1)->where('deleted', 0)->with('site')->whereHas('customer', function ($query) use($customer){
                return $query->where('customers.idcustomer', '=', $customer->idcustomer);
            })->get();
            foreach($cameras as $camera){
                $tmp_dat[] = [
                    "camera_id"=> $camera->idcamera, 
                    "camera_code" => $camera->code,
                    "camera_email" => $camera->email,
                    "site_id" => $camera->site->idsite,
                    "site_code" => $camera->site->code,
                    "customer_id" => $customer->idcustomer,
                    "customer_code" => $customer->code
                ];
            }
            $fil= 'cameras/cameras.bycustomer.'.$customer->idcustomer.'.json';
            Storage::disk('cache')->put($fil,json_encode($tmp_dat));
        }
        echo "fin";
       die;
    }
}