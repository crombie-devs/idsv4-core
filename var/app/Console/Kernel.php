<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CopyExternalAssets::class,
        Commands\GenerateAssetThumbnails::class,
        Commands\GenerateContentThumbnails::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('player:failure')->everyFiveMinutes();
        $schedule->command('generate:players-json')->dailyAt('02:00');
        $schedule->command('generate:cameras-actives-json')->dailyAt('02:00');

        $schedule->command('send-message:check')->everyTwoMinutes()->sendOutputTo(public_path() . '/task/log.txt');
        $schedule->command('send-message:send')->everyTwoMinutes()->sendOutputTo(public_path() . '/task/log.txt');
        $schedule->command('recordatory-support:send')->dailyAt('00:07');

        // $schedule->command('inspire')->hourly();      
        $schedule->command('circuits:generate')->dailyAt('00:10');
        $schedule->command('playlogics:generate')->dailyAt('00:12');
        $schedule->command('players:generate')->dailyAt('00:14');
        $schedule->command('contents:generate')->dailyAt('00:00');

        $schedule->command('contents:generate:tomorrow')->dailyAt('00:20');

        $schedule->command('contents:generate')->dailyAt('03:00');

        $schedule->command('circuits:generate')->dailyAt('06:10');
        $schedule->command('playlogics:generate')->dailyAt('06:12');
        $schedule->command('players:generate')->dailyAt('06:14');
        $schedule->command('contents:generate')->dailyAt('06:00');

        $schedule->command('circuits:generate')->dailyAt('12:10');
        $schedule->command('playlogics:generate')->dailyAt('12:12');
        $schedule->command('players:generate')->dailyAt('12:14');
        $schedule->command('contents:generate')->dailyAt('12:00');

        $schedule->command('contents:generate:tomorrow')->dailyAt('12:20');

        $schedule->command('contents:generate')->dailyAt('15:00');

        $schedule->command('circuits:generate')->dailyAt('18:10');
        $schedule->command('playlogics:generate')->dailyAt('18:12');
        $schedule->command('players:generate')->dailyAt('18:14');
        $schedule->command('contents:generate')->dailyAt('18:00');

        $schedule->command('contents:generate')->dailyAt('21:00');

        $schedule->command('contents:generate:tomorrow')->dailyAt('21:20');

        $schedule->command('recomendations-trends:load')->dailyAt('11:00');
        $schedule->command('generate:sitesbycustomer')->dailyAt('08:00');
        $schedule->command('generate:camerasbycustomer')->dailyAt('08:05');
        $schedule->command('generate:camerasbycustomer')->dailyAt('00:05');

        if (config('app.env') == 'PROD') {
            $schedule->command('alertsupport:generate')->hourlyAt(15);
            $schedule->command('alertsupport:generate')->hourlyAt(20);
            $schedule->command('alertcameras:generate')->dailyAt('20:05');
        }

        /* OTIS */
        $schedule->command('otis:generate:players')->dailyAt('18:00');
        $schedule->command('otis:generate:playlist')->dailyAt('19:00');
        /* SONAE */
        $schedule->command('sonae:generate:players')->dailyAt('20:00');
        $schedule->command('sonae:generate:playlist')->dailyAt('21:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
