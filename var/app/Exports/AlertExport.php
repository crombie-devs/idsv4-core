<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class AlertExport implements FromArray
{
    protected $alert;

    public function __construct(array $alert)
    {
        $this->alert = $alert;
    }

    public function array(): array
    {
        return $this->alert;
    }
}