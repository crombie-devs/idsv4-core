<?php

namespace App\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    protected $message;
    public function __construct($message){
        $this->message = $message;
    }

    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function render($request)
    {
        return response()->json([
            'success' => false,
            'data' => $this->message,
        ], 200);
    }
}
