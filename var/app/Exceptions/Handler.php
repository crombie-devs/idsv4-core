<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof \App\Exceptions\DatabaseErrorException){
            return $exception->render($request);
        }
        if($exception instanceof \App\Exceptions\NotValidUserException){
            return $exception->render($request);
        }
        if($exception instanceof \App\Exceptions\NotFoundException){
            return $exception->render($request);
        }
        if($exception instanceof \App\Exceptions\UserLoginException){
            return $exception->render($request);
        }
        if ($exception instanceof \PDOException) {
            return response()->json([
                'success' => false,
                'message' => 'Error en base de datos:::' . $exception->getMessage(),
            ], 403,[],JSON_UNESCAPED_UNICODE);
        }
        if ($exception instanceof \TypeError) {
            return response()->json([
                'success' => false,
                'message' => 'Parameters Error::: ' . $exception->getMessage(),
            ], 403,[],JSON_UNESCAPED_UNICODE);
        }
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->json([
                'success' => false,
                'message' => 'No tienes permisos para realizar esta acción.',
            ], 403,[],JSON_UNESCAPED_UNICODE);
        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception) {
            return response()->json(["success" => false, "message" => "Usuario no autenticado."], 401);
    }
}
