<?php

namespace App\Exceptions;

use Exception;

class UserLoginException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function render($request)
    {
        return response()->json([
            'success' => false,
            'message' => 'Nombre de usuario o contraseña incorrectos.',
        ], 403,[],JSON_UNESCAPED_UNICODE);
    }
}
