<?php

namespace App\Exceptions;

use Exception;

class DatabaseErrorException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function render($request)
    {
        return response()->json([
            'success' => false,
            'data' => 'Ha habido un error al guardar en la base de datos.',
        ], 503);
    }
}
