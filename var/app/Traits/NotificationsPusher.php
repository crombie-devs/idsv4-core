<?php

namespace App\Traits;

use Carbon\Carbon;
use Storage;
use App\Models\Channel;
use App\Models\Campaign_Channel;

trait NotificationsPusher {   

  public static function send(string $room, string $event, string $data = ''){
    $postFiled = [
        "publish_key" => "pub-c-2478datb-aeba-425t-dt52-9d6b6c68id53",
        "subscribe_key" => "sub-c-se83462d-cv32-89s6-a9dy-03vf4jdio3sk",
        "room" => $room,
        "message"=> [
            "from" => "console-server",
            "event" => $event, 
            "data" => $data,
        ]
    ];

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pusher.ladorianids.com/private/push-message",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30000, 
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postFiled),
        CURLOPT_HTTPHEADER => array(
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if (!$response)
      return false;

    return true; 
  } 
 
}

?>