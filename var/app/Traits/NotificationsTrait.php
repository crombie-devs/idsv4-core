<?php

namespace App\Traits;

use Carbon\Carbon;
use Storage;
use App\Models\Channel;
use App\Models\Campaign_Channel;

trait NotificationsTrait { 
  
  public function DeleteContentNoficiation($idcustomer=0){
    $tmpfiles =   Storage::disk('public_root')->allFiles('notifications');
    $files=[];

    if($idcustomer!=0){
      foreach($tmpfiles as $f){
        $afil = explode("/",$f);
        $cust = explode("_",$afil[1]); 
        $idcu = $cust[0];

        if($idcu==$idcustomer){
          array_push($files,$f);
        }
      }
    }
    else{
      $files=$tmpfiles;
    }

   // dd($files,$idcustomer);
    Storage::disk('public_root')->delete($files);
  }  
 
}

?>