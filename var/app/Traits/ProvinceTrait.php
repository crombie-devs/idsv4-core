<?php

namespace App\Traits;

trait ProvinceTrait {

    private $province_names = [
        "GERONA" => "Girona",
        "S.CRUZ TENERIFE" => "Santa Cruz de Tenerife",
        "CASTELLON" => "Castellón",
        "BIZKAIA" => "Vizcaya",
        "ORENSE" => "Ourense",
        "LA CORUÑA" => "A Coruña",
        "LA CORUÃA" => "A Coruña",
        "LA CORUÑA" => "A Coruña",
        "A CORUÃA" => "A Coruña",
        "LOGROÑO" => "La Rioja",
        "CASTELLÃN" => "Castellón",
        "ISLAS BALEARES" => "Baleares",
        "VAENCIA" => "Valencia",
        "LAS PALMAS DE G. C." => "Las Palmas",
        "LLEIDA" => "Lerida",
        "TORTOSA" => "Tarragona",
        "GIPUZKOA" => "Guipuzcoa",
        "LINARES" => "Jaen",
        "MÃLAGA" => "Malaga",
        "JAÃN" => "Jaen",
        "S/C DE TENERIFE" => "Santa Cruz de Tenerife",
    ];

    public function provinceNameNormalize($name) {
        $retorno = $name;
        if(!empty($this->province_names[$name]))
            $retorno = $this->province_names[$name];

        return $retorno;
    }
}
