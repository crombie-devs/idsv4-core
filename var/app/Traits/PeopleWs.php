<?php
namespace App\Traits;
 
use GuzzleHttp\Client;
use Carbon\Carbon;
 
trait PeopleWs
{
    public function getWsPeople($service,$idcustomer,$methods='GET',$dat="",$loc=true){
        try {
            $client = new \GuzzleHttp\Client();
            $ts =  Carbon::now()->timestamp;
            $current_timestamp = $ts*1000;
            $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
            $keyhash = hash('sha256',$string);

            $url = "https://people.ladorianids.es/ws/".$service;

            $tmpjson= ["customer_id"=>(int)$idcustomer];

            if(!empty($dat)){
                $tmpjson=array_merge($dat,$tmpjson);
            }
            $responseWS = $client->request($methods,$url.'?timestamp='.$current_timestamp.'&keyhash='.$keyhash, 
            ['json'=>$tmpjson, 'timeout' => 0]);
            $res = json_decode($responseWS->getBody(), true);

        } 
        catch (\GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $responseBody = $exception->getResponse()->getBody(true);
            dd($responseBody,$tmpjson);
        }
        if($res["success"]){
            return $res;
        }
        else{
            return "error\n";
        }
    }
}