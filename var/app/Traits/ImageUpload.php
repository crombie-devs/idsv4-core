<?php

namespace App\Traits;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Image;
use File;
use GuzzleHttp\Psr7\Message;
use Thumbnail;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;
use Illuminate\Support\Str;

trait ImageUpload
{

    public function PlayCircuitImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/circuits/';
        return $this->subeImage($query, $dir);
    }

    public function FormatImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/formats/';
        return $this->subeImage($query, $dir);
    }
    public function CustomerImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/customers/';
        return $this->subeImage($query, $dir);
    }

    public function ContentCategoryImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/content-category/';
        return $this->subeImage($query, $dir);
    }

    public function TagCategoryImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/tag-category/';
        return $this->subeImage($query, $dir);
    }

    public function LangImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/assets/flags/';
        return $this->subeImage($query, $dir);
    }





    public function AssetImageUpload($query) // Taking input image as parameter
    {
        $dir = '/images/assets/';
        $dir_path = public_path($dir);
        $ImageUpload = Image::make($query);
        $image_name = preg_replace('([^A-Za-z0-9 ])', '', str_replace(' ', '_', $query->getClientOriginalName()));
        $image_name = time() . '_' . $image_name;
        $ImageUpload->save($dir_path . $image_name);
        $size = round($ImageUpload->filesize() / 1024);
        $host = request()->getSchemeAndHttpHost();
        $image_url = $host . $dir . $image_name;
        $thDir = '/thumbnail/images/assets/';
        $thumbnailPath = public_path($thDir);
        $ImageUpload->heighten(200);
        $ImageUpload = $ImageUpload->save($thumbnailPath . $image_name);
        $tumbnail_url = $host . $thDir . $image_name;

        return [$image_url, $tumbnail_url, $size];
    }

    public function AssetVideoUpload($query, $time) // Taking input image as parameter
    {
        $dir = '/videos/assets/';
        return $this->subeVideoThumb($query, $dir, $time);
    }

    public function AssetAudioUpload($query) // Taking input image as parameter
    {
        $dir = '/audios/assets/';
        return $this->subeAudioThumb($query, $dir);
    }

    public function AssetImageDelete($name)
    {
        $dir = '/images/assets/' . $name;
        $dir_path = public_path($dir);

        if (file_exists($dir_path)) {
            File::delete($dir_path);
        }
    }

    public function AssetVideoDelete($name)
    {
        $dir = '/videos/assets/' . $name;
        $dir_path = public_path($dir);

        if (file_exists($dir_path)) {
            File::delete($dir_path);
        }
    }

    public function AssetAudioDelete($name)
    {
        $dir = '/audios/assets/' . $name;
        $dir_path = public_path($dir);

        if (file_exists($dir_path)) {
            File::delete($dir_path);
        }
    }

    public function subeImage($query, $dir)
    {
        $image_name =  time() . '_' . $query->getClientOriginalName();
        $upload_path = public_path($dir);

        $host = request()->getSchemeAndHttpHost();
        $image_url = $host . $dir . $image_name;
        $success = $query->move($upload_path, $image_name);

        return $image_url;
    }



    public function subeVideoThumb($query, $dir, $time)
    {
        $video_name = $query->getClientOriginalName();
        $video_name = strtolower($video_name);
        $video_name = str_replace(' ', '-', $video_name);
        $video_name = str_replace(['á', 'é', 'í', 'ó', 'ú'], ['a', 'e', 'i', 'o', 'u'], $video_name);

        $video_name =  time() . '_' . $video_name;

        $upload_path = public_path($dir);
        $host = request()->getSchemeAndHttpHost();
        $video_url = $upload_path . $video_name;
        $success = $query->move($upload_path, $video_name);

        $size = round(filesize($video_url) / 1024);
        $thDir = '/thumbnail/videos/assets/';
        $thumbnailPath = public_path($thDir);
        $name = explode('.', $video_name);
        $thumb_name = '';
        foreach ($name as $n) {
            if ($n != end($name)) {
                $thumb_name .= $n;
            }
        }
        $thumb_name .= '.jpg';
        $second = $time;
        try {
            $thumbnail_status = Thumbnail::getThumbnail($video_url, $thumbnailPath, $thumb_name, $time);
            return [$host . $dir . $video_name, $host . $thDir . $thumb_name, $size];
        } catch (Exception $e) {
            return [$host . $dir . $video_name, $host . $thDir . $thumb_name, $size];
        }
    }

    public function subeAudioThumb($query, $dir)
    {
        $audio_name = $query->getClientOriginalName();
        $upload_path = public_path($dir);
        $host = request()->getSchemeAndHttpHost();
        $audio_url = $upload_path . $audio_name;
        $success = $query->move($upload_path, $audio_name);
        $size = round(filesize($audio_url) / 1024);
        $thDir = '/thumbnail/audios/assets/';
        $thumbnailPath = public_path($thDir);
        $thumb_name = 'fondomusica.jpg';
        $tumbnail_url = $thumbnailPath . $thumb_name;
        //$second = 5;
        //$thumbnail_status = Thumbnail::getThumbnail($video_url, $thumbnailPath, $thumb_name,$second);
        return [$host . $dir . $audio_name, $host . $thDir . $thumb_name, $size];
    }

    public function thumbUrl($url, $time)
    {
        $host = request()->getSchemeAndHttpHost();
        $ruta_image = '/thumbnail/images/asset_url/' . uniqid() . '.jpg';
        $upload_path = public_path($ruta_image);
        try {
            Browsershot::url($url)
                ->fit(Manipulations::FIT_CONTAIN, 200, 200)
                ->setOption('args', ['--no-sandbox'])
                ->setScreenshotType('jpeg', 100)
                ->delay($time * 1000)
                ->save($upload_path);
            return $host . $ruta_image;
        } catch (Exception $e) {
            return $host . '/images/thumbnail/images/asset_url/default.jpg';
        }
    }

    public function thumbVideo($video_name, $video_url, $time)
    {

        $host = request()->getSchemeAndHttpHost();

        $thDir = '/thumbnail/videos/assets/';
        $thumbnailPath = public_path($thDir);
        $name = explode('.', $video_name);
        $thumb_name = time() . '_';
        foreach ($name as $n) {
            if ($n != end($name)) {
                $thumb_name .= $n;
            }
        }
        $thumb_name .= '.jpg';
        $second = $time;
        try {
            $thumbnail_status = Thumbnail::getThumbnail($video_url, $thumbnailPath, $thumb_name, $time);
            return [$host . $thDir . $thumb_name];
        } catch (Exception $e) {
            return [$host . $thDir . $thumb_name];
        }
    }
}
