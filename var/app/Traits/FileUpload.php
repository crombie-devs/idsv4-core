<?php
namespace App\Traits;
 
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;

trait FileUpload
{

    public function TicketsExcelUpload($query) // Taking input image as parameter
    { 
        $dir = '/tickets/excel/';   
        return $this->subeFile($query, $dir);
    }

    public function TicketsExceldelete($name){
        $dir = '/tickets/excel/'.$name;   
        $dir_path = public_path($dir); 

        if(file_exists($dir_path)) {
            File::delete($dir_path);
        } 
    }

    public function subeFile($query, $dir){
        $file_name = $query->getClientOriginalName(); 
        $upload_path = public_path($dir); 
        $host = request()->getSchemeAndHttpHost(); 
        $file_url = $host.$dir.$file_name;
        $success = $query->move($upload_path,$file_name);
        
        return $file_url;
    }
}