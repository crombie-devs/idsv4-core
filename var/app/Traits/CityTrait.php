<?php

namespace App\Traits;

trait CityTrait {

    /*
    private $cities_names = [
        "SAN PEDRO ALCANTARA" => "SAN PEDRO DE ALCANTARA",
        "LA CALA DE MIJAS" => "MIJAS",
        "MIJAS(COSTA)" => "MIJAS",
        "MIJAS-COSTA" => "MIJAS",
        "LAS LAGUNAS DE MIJAS" => "MIJAS",
        "BENALMADENA(COSTA)" => "BENALMADENA",
        "BURRIANA" => "Borriana/Burriana",	
        "CASTELLON DE LA PLANA" => "CASTELLÓN",
        "CASTELLON DE LA PLA" => "CASTELLÓN",
        "PEÑISCOLA" => "Peníscola/Peñíscola",
        "PLAYA DE NULES" => "Nules",	
        "MONCOFAR" => "MONCOFA",
        "HOSPITALET DE LLOBRE" => "Hospitalet del Llobregat",
        "L'HOSPITALET DE LLO" => "Hospitalet del Llobregat",
        "L`HOSPITALET DEL LLOBREGAT" => "Hospitalet del Llobregat",
        "HOSPITALET DE LLOBREGAT" => "Hospitalet del Llobregat",
        "L HOSPITALET DE LLOBREGAT" => "Hospitalet del Llobregat",
        "SERGORBE" => "Segorbe",
        "CALDETES" => "Caldes d Estrac",
        "LA CALA DEL MOR" => "La cala del Moral",
        "SANT SADURNI D=B4" => "Sant Sadurní d Anoia",
        "GRAO DE CASTELLON" => "CASTELLÓN",
        "TAIA" => "BARCELONA",
        "ARAVACA" => "Madrid",
        "ALICANTE" => "Alicante/Alacant",
        "JATIVA" => "Xativa",
        "ELCHE" => "Elche/Elx",
        "BALSAPINTADA" => "Fuente alamo de Murcia",
        "A CORUÑA" => "Coruña, A",
        "PLAYA DE SAN JUAN" => "Sant Joan d Alacant",
        "PLAYA DE LA ALBUFERA" => "Alicante/Alacant",
        "LAS PALMAS DE GRAN CANARIA" => "Palmas de Gran Canaria, Las",
        "ORENSE" => "Ourense",
        "MATALASCAÑAS" => "Almonte",
        "PALMA" => "Palma de Mallorca",
        "LAS ROZAS DE MADRID" => "Las Rozas",
        "SOTOGRANDE", "San Roque",
        "ZARAUZ" => "Zarautz",
        "ALBORAYA" => "Alboraia/Alboraya",
        "LA TOJA" => "Grove, O",
        "LO FERRO" => "Torre-Pacheco",
        "VILASECA" => "Vila-seca",
        "BURGO DE OSMA" => "Burgo de Osma-Ciudad de Osma",
        "PAMPLONA" => "Pamplona/Iruña",
        "VELEZ MALAGA" => "Velez",
        "VILLALBA" => "Vilalba",
    ];*/
    private $cities_names = [
        "PUERTO DE ANDRAITX" => "Andratx"
    ];

    public function cityNameNormalize($name) {
        $retorno = $name;
        if(!empty($this->cities_names[$name]))
            $retorno = $this->cities_names[$name];

        return $retorno;
    }
}
