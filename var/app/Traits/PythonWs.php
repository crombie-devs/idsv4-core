<?php
namespace App\Traits;
 
use GuzzleHttp\Client;
use Carbon\Carbon;
 
trait PythonWs
{
    function getWsPython($service,$methods='GET',$dat=""){
        try {
            $client = new \GuzzleHttp\Client();   
            //apibp
            //$url = "http://localhost:5000/".$service;
            $url = "http://82.223.69.121:5000/".$service;
            $responsePython = $client->request($methods,$url, ['json'=>$dat, 'timeout' => 60]);
            $data = $responsePython->getBody(true)->getContents();
            $res = json_decode($data);
        } catch (\GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $res = $exception->getResponse()->getBody(true);
        }
        return $res;
    }
}