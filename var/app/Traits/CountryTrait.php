<?php

namespace App\Traits;

trait CountryTrait {

    private $country_names = [
        "Spain" => "España"
    ];

    public function countryNameNormalize($name) {
        $retorno = $name;
        if(!empty($this->country_names[$name]))
            $retorno = $this->country_names[$name];

        return $retorno;
    }
}
