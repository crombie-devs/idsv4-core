<?php

namespace App\Traits;

use Exception;
use Image;
use Thumbnail;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;

trait UploadGoogleBucket
{



    public function __construct()
    {
    }

    private function generateNameFile($file)
    {
        $image_name = preg_replace('([^A-Za-z0-9 ])', '', str_replace(' ', '_', $file->getClientOriginalName()));
        $image_name = time() . '_' . $image_name . "." . $file->getClientOriginalExtension();
        return $image_name;
    }

    private function generateVideoName($file)
    {
        $video_name = $file->getClientOriginalName();
        $video_name = strtolower($video_name);
        $video_name = str_replace(' ', '-', $video_name);
        $video_name = str_replace(['á', 'é', 'í', 'ó', 'ú'], ['a', 'e', 'i', 'o', 'u'], $video_name);
        $video_name =  time() . '_' . $video_name;
        return $video_name;
    }

    public function ImageBucketUpload($file)
    {
        $image_name = $this->generateNameFile($file);
        $disk = \Storage::disk('gcs');
        $path = $disk->put('/assets/images/' . $image_name, file_get_contents($file));
        $imagePath = 'https://storage.googleapis.com/ladorian-assets-store-bucket/assets/images/' . $image_name;


        $thumbPath = $this->thumbnailBucketUpload($file, $image_name);


        $size = $this->sizeFile($file);
        return [$imagePath, $thumbPath, $size];
    }

    private function thumbnailBucketUpload($file, $image_name)
    {
        $file = Image::make($file);
        $file->heighten(200);
        $disk = \Storage::disk('gcs');
        $disk->put('/assets/images/thumbnails/' . $image_name, $file);
        return 'https://storage.googleapis.com/ladorian-assets-store-bucket/assets/images/thumbnails/' . $image_name;
    }


    private function sizeFile($file)
    {
        $size = round(filesize($file) / 1024);
        return $size;
    }





    public function videoBucketUpload($file, $time)
    {

        $video_name =  $this->generateVideoName($file);
        $upload_path = '/assets/videos/';
        $size = $this->sizeFile($file);
        $disk = \Storage::disk('gcs');
        $disk->put($upload_path, $file);
        $video_url_thumbnail = $this->videoThumbnail($file, $video_name, $time);
        return [$video_name, $video_url_thumbnail, $size];
    }


    private function videoThumbnail($file, $video_name, $time)
    {
        $thDir = '/assets/videos/thumbnails/';
        $dir = '/assets/videos/';

        $name = explode('.', $video_name);
        $thumb_name = $name[count($name) - 2];
        $thumb_name .= '.jpg';
        $filePath = public_path($dir);
        $file->move($filePath, $video_name);
        $video_url = $filePath . $video_name;
        $thumbPath = public_path($thDir);

        try {
            Thumbnail::getThumbnail($video_url, $thumbPath, $thumb_name, $time);
            $disk = \Storage::disk('gcs');
            $disk->put($thDir . $thumb_name, file_get_contents($thDir . $thumb_name));

            unlink($video_url);

            return $thDir . $thumb_name;
        } catch (Exception $e) {
            unlink($video_url);
            return $thumb_name;
        }
    }


    public function audioBucketUpload($file)
    {
        $dir = "assets/audios/";
        $audio_name = $file->getClientOriginalName();
        $size = round(filesize($file) / 1024);
        $disk = \Storage::disk('gcs');
        $disk->put($dir . $audio_name, file_get_contents($file));
        $thDir = 'assets/audios/thumbnails/';
        $thumb_name = 'fondomusica.jpg';
        return [$dir . $audio_name, $thDir . $thumb_name, $size];
    }



    public function removeBucketFile($filePath)
    {
        // $filePath = ['assets/videos/8POTbXX3HFBjwbx0r3EqaBZiPw3rynNYmZvUwJSS.mp4'];
        $disk = \Storage::disk('gcs');
        return  $disk->delete($filePath);
    }


    public function thumbBucketUrl($url, $time)
    {
        $ruta_image = '/assets/asset_url/thumbnails/' . uniqid() . '.jpg';
        $upload_path = public_path($ruta_image);

        try {
            Browsershot::url($url)
                ->fit(Manipulations::FIT_CONTAIN, 200, 200)
                ->setOption('args', ['--no-sandbox'])
                ->setScreenshotType('jpg', 100)
                ->delay($time * 1000)
                ->save($upload_path);

            $disk = \Storage::disk('gcs');
            $disk->put($ruta_image, file_get_contents($upload_path));

            return $upload_path;
        } catch (Exception $e) {
            return '/images/thumbnail/images/asset_url/default.jpg';
        }
    }
}
