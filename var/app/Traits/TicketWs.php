<?php
namespace App\Traits;
 
use GuzzleHttp\Client;
use Carbon\Carbon;
 
trait TicketWs
{
    public function getWsTickets($service,$idcustomer,$methods='GET',$dat="",$loc=true){
        try {
            $client = new \GuzzleHttp\Client();
            $ts =  Carbon::now()->timestamp;
            $current_timestamp = $ts*1000;
            $string = $current_timestamp.'ALRIDKJCS1SYADSKJDFS';
            $keyhash = hash('sha256',$string);

            if($loc){
                $url = "https://tickets.ladorianids.es/ws/".$service;
            }
            else{
                $url = "http://localhost:3000/ws/".$service;
            }

            $tmpjson= ["customer_id"=>(int)$idcustomer];

            if(!empty($dat)){
                $tmpjson=array_merge($dat,$tmpjson);
            }

            $responseWS = $client->request($methods,$url.'?timestamp='.$current_timestamp.'&keyhash='.$keyhash, 
            ['json'=>$tmpjson, 'timeout' => 0, 'verify' => false]);
            $res = json_decode($responseWS->getBody(), true);

        } 
        catch (\GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $responseBody = $exception->getResponse()->getBody(true);
        }

        if($res["success"]){
            return $res;
        }
        else{
            return "error\n";
        }
    }
}