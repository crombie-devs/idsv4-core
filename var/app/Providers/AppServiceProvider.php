<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;

use App\Repositories\Recomendations\RecomendationsRepository;
use App\Repositories\Recomendations\RecomendationsRepositoryInterface;
use App\Repositories\App\AppRepository;
use App\Repositories\Tag\TagRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\City\CityRepository;
use App\Repositories\Lang\LangRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Site\SiteRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Asset\AssetRepository;
use App\Repositories\Camera\CameraRepository;
use App\Repositories\Format\FormatRepository;
use App\Repositories\Impact\ImpactRepository;
use App\Repositories\Player\PlayerRepository;
use App\Repositories\Camera\WSCameraRepository;
use App\Repositories\Content\ContentRepository;
use App\Repositories\Country\CountryRepository;
use App\Repositories\Player\WSPlayerRepository;
use App\Repositories\App\AppRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Repositories\Customer\CustomerRepository;
use App\Repositories\Password\PasswordRepository;
use App\Repositories\PlayArea\PlayAreaRepository;
use App\Repositories\Province\ProvinceRepository;
use App\Repositories\City\CityRepositoryInterface;
use App\Repositories\Lang\LangRepositoryInterface;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\Site\SiteRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\AssetType\AssetTypeRepository;
use App\Repositories\PlayLogic\PlayLogicRepository;
use App\Repositories\Asset\AssetRepositoryInterface;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Camera\CameraRepositoryInterface;
use App\Repositories\Format\FormatRepositoryInterface;
use App\Repositories\Impact\ImpactRepositoryInterface;
use App\Repositories\Player\PlayerRepositoryInterface;
use App\Repositories\ContentType\ContentTypeRepository;
use App\Repositories\PlayCircuit\PlayCircuitRepository;
use App\Repositories\TagCategory\TagCategoryRepository;
use App\Repositories\Camera\WSCameraRepositoryInterface;
use App\Repositories\Content\ContentRepositoryInterface;
use App\Repositories\Country\CountryRepositoryInterface;
use App\Repositories\Player\WSPlayerRepositoryInterface;
use App\Repositories\EmissionSale\EmissionSaleRepository;
use App\Repositories\Customer\CustomerRepositoryInterface;
use App\Repositories\Password\PasswordRepositoryInterface;
use App\Repositories\PlayArea\PlayAreaRepositoryInterface;
use App\Repositories\Province\ProvinceRepositoryInterface;
use App\Repositories\AssetType\AssetTypeRepositoryInterface;
use App\Repositories\PlayLogic\PlayLogicRepositoryInterface;
use App\Repositories\IndoorLocation\IndoorLocationRepository;
use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\ContentCategory\ContentCategoryRepository;
use App\Repositories\ContentType\ContentTypeRepositoryInterface;
use App\Repositories\PlayCircuit\PlayCircuitRepositoryInterface;
use App\Repositories\TagCategory\TagCategoryRepositoryInterface;
use App\Repositories\EmissionSale\EmissionSaleRepositoryInterface;
use App\Repositories\IndoorLocation\IndoorLocationRepositoryInterface;
use App\Repositories\ContentCategory\ContentCategoryRepositoryInterface;
use App\Repositories\Customer\WSCustomerRepositoryInterface;
use App\Repositories\Customer\WSCustomerRepository;
use App\Repositories\Player\SupportPlayerRepositoryInterface;
use App\Repositories\Player\SupportPlayerRepository;
use App\Repositories\Camera\SupportCameraRepositoryInterface;
use App\Repositories\Camera\SupportCameraRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Notifications\WSNotificationsRepositoryInterface;
use App\Repositories\Notifications\WSNotificationsRepository;
use App\Repositories\TemplatesSector\TemplatesSectorRepository;
use App\Repositories\TemplatesSector\TemplatesSectorRepositoryInterface;
use App\Repositories\TemplatesCustomer\TemplatesCustomerRepository;
use App\Repositories\TemplatesCustomer\TemplatesCustomerRepositoryInterface;
use App\Repositories\Template\TemplateRepository;
use App\Repositories\Template\TemplateRepositoryInterface;
use App\Repositories\Landingpage\LandingpageRepository;
use App\Repositories\Landingpage\LandingpageRepositoryInterface;
use App\Repositories\Otis\OtisRepository;
use App\Repositories\Otis\OtisRepositoryInterface;
use App\Repositories\Log\LogRepository;
use App\Repositories\Log\LogRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryInterface::class, 
            UserRepository::class);

        $this->app->bind(
            PasswordRepositoryInterface::class,
            PasswordRepository::class);

        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class);  

        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class);  
        
        $this->app->bind(
            CustomerRepositoryInterface::class,
            CustomerRepository::class); 

        $this->app->bind(
            AppRepositoryInterface::class,
            AppRepository::class);

        $this->app->bind(
            SiteRepositoryInterface::class,
            SiteRepository::class);

        $this->app->bind(
            TagCategoryRepositoryInterface::class,
            TagCategoryRepository::class);

        $this->app->bind(
            TagRepositoryInterface::class,
            TagRepository::class);

        $this->app->bind(
            PlayerRepositoryInterface::class,
            PlayerRepository::class);

        $this->app->bind(
            CameraRepositoryInterface::class,
            CameraRepository::class);
        
        $this->app->bind(
            PlayAreaRepositoryInterface::class,
            PlayAreaRepository::class);

        $this->app->bind(
            WSPlayerRepositoryInterface::class,
            WSPlayerRepository::class);        
        
        $this->app->bind(
            ContentCategoryRepositoryInterface::class,
            ContentCategoryRepository::class);

        $this->app->bind(
            AssetRepositoryInterface::class,
            AssetRepository::class);
        
        $this->app->bind(
            IndoorLocationRepositoryInterface::class,
            IndoorLocationRepository::class);
            
        $this->app->bind(
            LangRepositoryInterface::class,
            LangRepository::class);

        $this->app->bind(
            PlayCircuitRepositoryInterface::class,
            PlayCircuitRepository::class);

        $this->app->bind(
            CountryRepositoryInterface::class,
            CountryRepository::class);

        $this->app->bind(
            ProvinceRepositoryInterface::class,
            ProvinceRepository::class);

        $this->app->bind(
            CityRepositoryInterface::class,
            CityRepository::class); 
        
        $this->app->bind(
            ContentTypeRepositoryInterface::class,
            ContentTypeRepository::class);
        
        $this->app->bind(
            AssetTypeRepositoryInterface::class,
            AssetTypeRepository::class);

        $this->app->bind(
            FormatRepositoryInterface::class,
            FormatRepository::class);

        $this->app->bind(
            ContentRepositoryInterface::class,
            ContentRepository::class);

        $this->app->bind(
            PlayLogicRepositoryInterface::class,
            PlayLogicRepository::class);

        $this->app->bind(
            ImpactRepositoryInterface::class,
            ImpactRepository::class);

        $this->app->bind(
            EmissionSaleRepositoryInterface::class,
            EmissionSaleRepository::class);
        
        $this->app->bind(
            WSCameraRepositoryInterface::class,
            WSCameraRepository::class); 
            
        $this->app->bind(
            RecomendationsRepositoryInterface::class,
            RecomendationsRepository::class); 

        $this->app->bind(
            WSCustomerRepositoryInterface::class,
            WSCustomerRepository::class); 

        $this->app->bind(
            SupportPlayerRepositoryInterface::class,
            SupportPlayerRepository::class); 
            
        $this->app->bind(
            SupportCameraRepositoryInterface::class,
            SupportCameraRepository::class);

        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class);
        
        $this->app->bind(
            WSNotificationsRepositoryInterface::class,
            WSNotificationsRepository::class);
        
        $this->app->bind(
            TemplatesSectorRepositoryInterface::class,
            TemplatesSectorRepository::class);
            
        $this->app->bind(
            TemplatesCustomerRepositoryInterface::class,
            TemplatesCustomerRepository::class);       
        
        $this->app->bind(
            TemplateRepositoryInterface::class,
            TemplateRepository::class); 

        $this->app->bind(
            LandingpageRepositoryInterface::class,
            LandingpageRepository::class);    
            
        $this->app->bind(
            OtisRepositoryInterface::class,
            OtisRepository::class); 

        $this->app->bind(
            LogRepositoryInterface::class,
            LogRepository::class); 
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
