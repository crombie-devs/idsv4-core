<?php

namespace App\Models;

class PatternDataRecomendations
{
    private $cols = ['nivelagregacion', 'idagregacion','code_reference','value'];
    public $pdr;

    public function __construct($idcustomer, $month){
        $csv = file('http://82.223.69.121:5000/data/' . $idcustomer . '/pattern-data-recomendations-' . $month . '.csv');
        $output = [];
        $separator = ';';
        foreach ($csv as $line_index => $line) {
            if($line_index == 0) {
                $values = explode(';', $line);
                if(count($values) < 2){
                    $separator = ',';
                }
            } else {
                $newLine = [];
                $values = explode($separator, $line);
                foreach ($values as $col_index => $value) {
                    if($col_index == 3){
                        $newLine[$this->cols[$col_index]] = floatval(trim($value));
                    }else{
                        $newLine[$this->cols[$col_index]] = trim($value);
                    }
                }
                $output[] = $newLine;
            }
        }
        $this->pdr = collect($output);
    }
}
