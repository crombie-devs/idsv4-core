<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Camera extends Model
{
    protected $primaryKey = 'idcamera';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsite', 'code', 'name', 'email', 'status', 'deleted', 'face_timeout',
        'section_start_x', 'section_start_y', 'section_width', 'section_height',
        'face_size', 'face_threshold', 'cosine_threshold', 'embedding_threshold', 'status', 'is_monitored'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted',
    ];

    public function site()
    {
        return $this->belongsTo(Site::class, 'idsite');
    }

    public function customer()
    {
        return $this->hasOneThrough(
            Customer::class,
            Site::class,
            'idsite', // Foreign key on sites table...
            'idcustomer', // Foreign key on customers table...
            'idsite', // Local key on cameras table...
            'idcustomer' // Local key on customers table...
        );
    }

    public function holiday()
    {
        return $this->hasManyThrough(
            SiteHoliday::Class,
            Site::class,
            'idsite', // Foreign key on play_area table...
            'idsite', // Foreign key on player table...
            'idsite', // Local key on cameras table...
            'idsite' // Local key on site_holidays table...
        );
    }

    public function powers()
    {
        return $this->hasMany(CameraPower::class, 'idcamera');
    }


    public static function getSuperAdminCameras($status = null)
    {
        $cameras = Camera::with('customer')
            ->with('site')
            ->with('powers')
            ->where('deleted', 0);

        if (!is_null($status)) {
            $cameras = $cameras->where('status', 1);
        }

        $cameras = $cameras->orderBy('email')->get();
        return $cameras;
    }

    public static function getAdminCameras($user, $status = null)
    {
        $cameras = Camera::with('site')
            ->with('customer')
            ->with('powers')
            ->join('sites', 'cameras.idsite', '=', 'sites.idsite')
            ->where('sites.idcustomer', $user->idcustomer)
            ->where('cameras.deleted', 0);

        if (!is_null($status)) {
            $cameras = $cameras->where('cameras.status', 1);
        }

        $cameras = $cameras->select('cameras.*')->orderBy('email')->get();
        return $cameras;
    }

    public static function getUserCameras($user, $status = null)
    {
        $cameras = Camera::with('site')
            ->with('customer')
            ->with('powers')
            ->where('cameras.idsite', $user->idsite)
            ->where('cameras.deleted', 0);
        if (!is_null($status)) {
            $cameras = $cameras->where('status', 1);
        }

        $cameras = $cameras->select('cameras.*')->orderBy('email')->get();
        return $cameras;
    }

    public static function getCameraData($id)
    {
        $camera = Camera::with('site')
            ->with('customer')
            ->with('powers')
            ->with('holiday')
            ->where('cameras.idcamera', $id)
            ->where('cameras.deleted', 0)
            ->select('cameras.*')
            ->first();
        return $camera;
    }

    public static function getToAnalytics($params)
    {
        $cameras = Camera::where('idcustomer', $params['idcustomer'])
            ->where('deleted', 0)
            ->where('status', 1);

        if ($params['idsite']) {
            $cameras->where('idsite', $params['idsite']);
        }
        $cameras->get();

        return $cameras;
    }

    public static function getDetailCamera($email)
    {
        $camera = Camera::with('site')
            ->with('customer')
            ->with('powers')
            ->where('email', $email)
            ->where('deleted', 0)
            ->where('status', 1)
            ->first();

        return $camera;
    }

    public function isValid()
    {
        return $this->site()->first()->isValid();
    }

    //Support

    public static function getCamera()
    {
        $player = Camera::select("cameras.email", "cameras.idcamera", "customers.idcustomer", "sites.idsite")
            ->join("sites", "sites.idsite", "=", "cameras.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->with("site")
            ->with("customer")
            ->where("cameras.status", 1)
            ->where("cameras.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->orderBy("cameras.email")
            ->get();
        return $player;
    }

    public static function getCameraBySite($idsite)
    {
        $camera = Camera::select("cameras.email", "cameras.idcamera", "cameras.name")
            ->join("sites", "sites.idsite", "=", "cameras.idsite")
            ->where("sites.idsite", "=", $idsite)
            ->where("cameras.status", 1)
            ->where("cameras.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->orderBy("cameras.email")
            ->get();
        return $camera;
    }

    public static function getCameraByCustomer($idcustomer)
    {
        $camera = Camera::select("cameras.email", "cameras.idcamera", "cameras.idsite", "sites.name")
            ->join("sites", "sites.idsite", "=", "cameras.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->with('powers')
            ->where('customers.idcustomer', '=', $idcustomer)
            ->where('cameras.status', 1)
            ->where('cameras.deleted', 0)
            ->orderBy('cameras.email')
            ->get();
        return $camera;
    }

    public static function totalCameraGroupByCustomer()
    {
        $camera = Camera::select("customers.idcustomer", "customers.name", "customers.email", Camera::raw("COUNT(cameras.idcamera) as total"))
            ->join("sites", "sites.idsite", "=", "cameras.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where("cameras.status", 1)
            ->where("cameras.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->where("customers.status", 1)
            ->where("customers.deleted", 0)
            ->groupBy("customers.idcustomer", "customers.idcustomer", "customers.email", "customers.name")
            ->orderBy("customers.name")
            ->get();
        return $camera;
    }

    public static function totalCameraGroupBySite($idcustomer)
    {
        $camera = Camera::select("sites.idsite", "sites.code", "sites.name", "sites.email", Camera::raw("COUNT(cameras.idcamera) as total"))
            ->join("sites", "sites.idsite", "=", "cameras.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where("customers.idcustomer", $idcustomer)
            ->where("cameras.status", 1)
            ->where("cameras.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->where("customers.status", 1)
            ->where("customers.deleted", 0)
            ->groupBy("sites.idsite", "sites.code", "sites.name", "sites.email", "sites.name")
            ->orderBy("sites.name")
            ->get();
        return $camera;
    }
}
