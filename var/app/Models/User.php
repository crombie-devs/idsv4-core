<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\PasswordResetRequestNotification as ResetPasswordNotification;


class User extends Authenticatable
{
    use HasRoles, Notifiable, HasApiTokens;
    protected $guard_name = 'api';
    protected $primaryKey = 'iduser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'idcustomer', 'idsite', 'status', 'deleted', 'idcategories', 'idcircuits'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at', 'created_at', 'updated_at',
        'idcustomer', 'idsite', 'status', 'deleted', 'roles', 'permissions'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }


    public function config_customer()
    {
        return $this->belongsTo(ConfigCustomer::class, 'idcustomer', 'idcustomer');
    }

    public function langs()
    {
        return $this->hasMany(CustomersHasLangs::class, 'idcustomer', 'idcustomer')->with('lang');
    }

    public function site()
    {
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }

    public function is_admin()
    {
        return $this->idcustomer && !$this->idsite;
    }

    public function is_user()
    {
        return $this->idcustomer && $this->idsite;
    }

    public function is_superadmin()
    {
        return !$this->idcustomer && !$this->idsite;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
}
