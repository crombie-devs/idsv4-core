<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplatesSector extends Model
{
    protected $primaryKey = 'idtemplatecustomer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idtemplatesector', 'idcustomer', 'name', 'icon'
    ];

   
    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function getTemplatesSectorByCustomer($idcustomer) {

        $query = TemplatesCustomer::where("idcustomer",$idcustomer);
          
        return $query->first();
        
    }
    
}
