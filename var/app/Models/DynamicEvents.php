<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;


class DynamicEvents extends Model
{
    protected $table = 'dynamic_events';

    protected $primaryKey = 'id';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer',  'source_url', 'name', 'is_right_now',  'frecuency', 'status', 'idsite', 'idplayer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function rules()
    {
        return $this->hasMany(DynamicEventRules::class, 'idevent', 'id');
    }


    


    

    public static function get($id)
    {
        return DynamicEvents::with('rules')->where("id", $id)->first();
    }


    public static function byPlayer($idcustomer, $idsite, $idplayer)
    {

        $request = DynamicEvents::with('rules')->with('rules.tags')->where('idcustomer', $idcustomer);

        if (!empty($idsite)) {
            $request->where(function ($query) use ($idsite) {
                $query->where('idsite', $idsite)->orWhereNull('idsite');
            });
        } else {
            $request->whereNull('idsite');
        }

        if (!empty($idplayer)) {
            $request->where(function ($query) use ($idplayer) {
                $query->where('idplayer', $idplayer)->orWhereNull('idplayer');
            });
        } else {
            $request->whereNull('idplayer');
        }
        return  $request->get();
    }


    public static function tagPlayer($id) {
        
        return DynamicEvents::where('idplayer', $id)->with('rules')->get();
    }



    public static function toSelect()
    {
        $request =  DynamicEvents::where('status', true);
        $user = Auth::user();

        if ($user->is_admin())
            $request->where('idcustomer', $user->idcustomer);
        else if ($user->is_user())
            $request->where('idcustomer', $user->idcustomer)->where('idsite', $user->idsite);

        return $request->get();
    }

    public static function list()
    {
        $user = Auth::user();
        if ($user->is_superadmin())
            return  DynamicEvents::all();
        else if ($user->is_admin())
            return DynamicEvents::where('idcustomer', $user->idcustomer)->get();
        else
            return DynamicEvents::where('idcustomer', $user->idcustomer)->where('idsite', $user->idsite)->get();
    }


    public function isValid()
    {
        $user = Auth::user();
        $result = true;

        if (!is_null($user->idsite)) {
            $result = ($user->idsite == $this->idsite);
        } else if (!is_null($user->idcustomer)) {
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }
}
