<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AudioCategory extends Model
{
    protected $table = 'audio_categories';
    public $timestamps = false;

    protected $primaryKey = 'idcategory';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer',  'name',  'image_url', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function audio()
    {
        return $this->hasMany(Audio::class, 'idcategory', 'idcategory');
    }

    public function customer()
    {
        return $this->hasMany(Customer::class, 'idcustomer', 'idcustomer');
    }



    public static function filtertByCustomer($idcustomer)
    {
        $audios = AudioCategory::with('audio')->where('idcustomer', $idcustomer);
        return $audios->get();
    }
    public static function wsFiltertByCustomer($idcustomer)
    {
        $audios = AudioCategory::with('audio')->where('idcustomer', $idcustomer)->where('status', 1)->where('deleted', 0);
        return $audios->get();
    }




    public static function audioToCacheByCustomer($idcustomer)
    {
        $audios = AudioCategory::with('customer')->with('audio')->where('idcustomer', $idcustomer);
        return $audios->get();
    }


    public static function toSelectAudioCategories()
    {

        $audios = AudioCategory::get();
        return $audios;
    }

    public static function getAdminAudioCategoriesList($user)
    {
        $audios = AudioCategory::where('idcustomer', $user->idcustomer);
        $audios = $audios->get();
        return $audios;
    }


    public static function getAdminAudioCategoriesToSelect($user)
    {
        $audios = AudioCategory::where('idcustomer', $user->idcustomer)->where('status', 1);
        $audios = $audios->get();
        return $audios;
    }



    public static function getCategory($id)
    {
        $audios = AudioCategory::where('idcategory', $id);
        $audios = $audios->get();
        return $audios;
    }
}
