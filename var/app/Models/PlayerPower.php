<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerPower extends Model
{
    protected $primaryKey = 'idtime';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idplayer','weekday', 'time_on', 'time_off'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','idplayer'
    ];

    public function player(){
        return $this->belongsTo(Player::class, 'idplayer');
    }
}
