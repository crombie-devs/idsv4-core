<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplatesCustomer extends Model
{
    protected $primaryKey = 'idtemplatecustomer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idtemplatecustomer', 'idcustomer', 'idcustomer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];

   
    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public static function getTemplatesCustomerByIdcustomerMaster() {

        $query = TemplatesCustomer::select( 'templates_customers.idtemplatecustomer', 
                                            'templates_customers.idcustomer', 
                                            'templates_customers.idcustomer_master',
                                            'customers.name' )
                                    ->join("customers", "customers.idcustomer", "=", "templates_customers.idcustomer_master")
                                    ->groupBy('templates_customers.idcustomer_master')
                                    ->get();
          
        return $query;
        
    }


    
}
