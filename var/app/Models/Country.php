<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'idcountry';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    public function provinces(){
        return $this->hasMany(Province::class, 'idcountry', 'idcountry');
    }
    
    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_countries','idcountry','idcircuit');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_countries','idcountry','idcontent');
    }

    public static function findByName($name) {
        $query = \DB::table('countries')->where('name', 'like', $name);
        return $query->first();
    }
}
