<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    protected $primaryKey = 'idlang';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','image_url'
    ];


    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_langs','idlang','idcircuit');
    }

    public function customers(){
        return $this->belongsToMany(Customer::class,'customers_has_langs','idlang','idcustomer');
    }

    public function player(){
        return $this->hasMany(Player::class, 'idlang', 'idlang');
    }

    public static function getLangsPlayerSuperAdmin(){
        $langs = Lang::with('player')->get();
        return $langs;
    }
    public static function getLangsByCustomer($user){
        if($user->customer){
            $customer = Customer::with('langs')->where('idcustomer', $user->customer->idcustomer)->first();
            $langs = $customer->langs;
        }else{
            $langs = Lang::all();
        }

        return $langs;
    }
    public static function getLangsPlayerAdmin($user){
        $sites = Site::where('idcustomer',$user->idcustomer)->get();
        if($sites->isNotEmpty()){
            $langs = Lang::with(['player' => function($query) use ($sites) {
                        $query->whereIn('idsite',$sites->pluck('idsite')); 
                    }
            ])->get();
        }
        return $langs;
    }

    public static function getLangsPlayerUser($user){
        $langs = Lang::with(['player' => function($query) use ($user) {
                        $query->where('idsite',$user->idsite); 
                    }
                ])->get();
 
        return $langs;
    }
    
}
