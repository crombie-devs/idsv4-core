<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $primaryKey = 'idsupport';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsupport', 'idcustomer', 'frecuency', 'repetition_frecuency', 'last_email', 'timezone'
    ];
    /**
     * The attributes that should be dates.
     *
     * @var array
    */
    protected $dates = [
        'created_at', 'updated_at', 'last_email'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }
    public function getSupportByCustomer($idcustomer) {
        $query = Support::where("idcustomer",$idcustomer);
        return $query->first();
    }
    public function updateS($idsupport,$data) {
        $id = Support::where('idsupport', $idsupport)
                            ->update($data);
        return $id;
    }
}
