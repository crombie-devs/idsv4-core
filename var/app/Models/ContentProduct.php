<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentProduct extends Model
{
    protected $table = "contents_has_products";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idcontent', 'idproduct'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function content(){
        return $this->belongsTo(Content::class, 'idcontent');
    }

}