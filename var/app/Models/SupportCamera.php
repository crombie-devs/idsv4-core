<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportCamera extends Model
{
    protected $primaryKey = 'idsupportcamera';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsupportcamera', 'idcustomer', 'idsite', 'idcamera', 'last_test_hour', 
        'internet', 'socket', 'camera'
    ];

    /**
     * The attributes that should be dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'last_test_hour'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function site(){
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }

    public function camera(){
        return $this->belongsTo(Camera::class, 'idcamera', 'idcamera');
    }

    public function getSupportCameraByCustomerByCamera($idcustomer, $idcamera) {

        $query = SupportCamera::where("idcustomer",$idcustomer)->where('idcamera',$idcamera);
          
        return $query->first();
        
    }

    public function insertGetId($data) {

        $id = SupportCamera::insert([$data]); 
          
        return $id;
        
    }

    public function updateC($idcamera,$data) {

        $id = SupportCamera::where('idcamera', $idcamera)
                            ->update($data);
          
        return $id;
        
    }
}
