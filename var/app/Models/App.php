<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $primaryKey = 'idapp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'apikey', 'idcustomer', 'status', 'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'status', 'deleted', 'idcustomer'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

}
