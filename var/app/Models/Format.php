<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    protected $primaryKey = 'idformat';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image_url', 'frame_image_url', 'description', 'width', 'height', 'broadcasting'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function player()
    {
        return $this->hasManyThrough(
            Player::Class,
            PlayArea::Class,
            'idformat', // Foreign key on play_area table...
            'idarea', // Foreign key on player table...
            'idformat', // Local key on format table...
            'idarea' // Local key on play_area table...
        );
    }


    public function lang()
    {
        return $this->hasOne(Lang::class, 'idlang', 'idlang');
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'customers_has_formats', 'idformat', 'idcustomer');
    }

    public static function getFormatsPlayerSuperAdmin()
    {
        $formats = Format::with('player')->get();
        return $formats;
    }

    public static function getFormatsPlayerAdmin($user)
    {
        $sites = Site::where('idcustomer', $user->idcustomer)->get();
        if ($sites->isNotEmpty()) {
            $formats = Format::with([
                'player' => function ($query) use ($sites) {
                    $query->whereIn('idsite', $sites->pluck('idsite'));
                }
            ])->get();
        }
        return $formats;
    }

    public static function getFormatsPlayerUser($user)
    {
        $formats = Format::with([
            'player' => function ($query) use ($user) {
                $query->where('idsite', $user->idsite);
            }
        ])->get();

        return $formats;
    }

    public static function getAllFormats()
    {
        $formats = Format::with('customers')->orderBy('name')->get();
        return $formats;
    }


    public static function getAdminFormats($user)
    {
        $customerformats = Customer::with('formats')->where('idcustomer', $user->idcustomer);
        $idformats = $customerformats->get()->pluck('formats')->first()->pluck('idformat');
        $formats = Format::with('customers')->whereIn('idformat', $idformats)->orderBy('name')->get();

        return $formats;
    }
}
