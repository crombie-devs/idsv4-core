<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentPower extends Model
{
    protected $primarykey = 'idtime';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weekday', 'idcontent', 'time_on','time_off'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function content(){
        return $this->belongsTo(Content::class, 'idcontent');
    }

}
