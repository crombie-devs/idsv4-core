<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicEventRules extends Model
{
    protected $table = 'dynamic_events_rules';

    protected $primaryKey = 'iddynamiceventsrules';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id', 'symbol', 'idevent', 'comp_value', 'entity','action', 'identity', 'status', 'deleted', 'created_at', 'updated_at' ,'times'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function tags () {
        return $this->hasMany(Tag::class, 'idtag', 'identity');
    }

    public function dynamicEvents(){
        return $this->hasMany(DynamicEvents::class, 'iddynamicevents', 'iddynamicevents');
    }

    public static function filtertByDynamicEvent($id){
        $events = DynamicEventRules::find(1)->dynamicEvents()->where('idcustomer', $id);  
        return $events->get();
    }


    public static function filterByEvent($idevent) {
        return DynamicEvents::with('tags')->where('idevent', $idevent)->get();
    }

  

    public static function toSelectAll(){
        $events = DynamicEventRules::all();
      
        return $events;
    }

}
