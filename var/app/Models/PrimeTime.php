<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrimeTime extends Model
{
    protected $primaryKey = 'idprimetime';
    public $timestamps = false;
    protected $table = "primetime";

}
