<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketResponsible extends Model
{
    protected $table = "tickets_responsibles";

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'deleted', 
        'email', 
        'id_department'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function department()
    {
        return $this->hasOne(TicketDepartment::class, 'id', 'id_department');
    }

}