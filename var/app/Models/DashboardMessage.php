<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardMessage extends Model
{
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'message',
        'date_on',
        'date_off'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 
        'updated_at'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
