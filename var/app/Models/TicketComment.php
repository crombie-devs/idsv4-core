<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    protected $table = 'tickets_comments';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_ticket', 
        'id_admin', 
        'mensaje', 
        'notificar_usuario', 
        'leido', 
        'created_at', 
        'updated_at', 
        'id_created_by' ,
        'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function ticket()
    {
        return $this->hasOne(Ticket::class, 'id', 'id_ticket');
    }

    public function status()
    {
        return $this->hasOne(TicketStatus::class, 'id', 'id_status');
    }

    public function images()
    {
        return $this->hasMany(TicketImage::class, 'id_comment', 'id');
    }

    public function created_by()
    {
        return $this->hasMany(User::class, 'iduser', 'id_created_by');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'iduser', 'id_created_by');
    }

    public function responsible()
    {
        return $this->hasOne(TicketResponsible::class, 'id', 'id_responsible');
    }
 
    public static function CommentClose($id) 
    {
        return DB::select('UPDATE ticket_comments SET deleted = 0 WHERE id = ?', [$id]);
    }

}
