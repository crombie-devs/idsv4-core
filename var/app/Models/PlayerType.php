<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerType extends Model
{
    protected $primaryKey = 'idtype';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function player(){
        return $this->belongsTo(Player::class, 'idplayer');
    }
}
