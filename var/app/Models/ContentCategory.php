<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ContentCategory extends Model
{
    protected $primaryKey = 'idcategory';
    protected $table = "content_categories";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer', 'name', 'image_url', 'duration', 'user_local', 'random'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function getUserContentCategories($user)
    {
        $contentcategories = ContentCategory::where('idcustomer', $user->idcustomer)->orderBy('name')->get();

        return $contentcategories;
    }


    public function contents()
    {
        return $this->hasMany(App::class, 'idcategory', 'idcategory');
    }

    public function playlogics()
    {
        return $this->hasMany(App::class, 'idcategory', 'idcategory');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

    public function isValid()
    {
        $user = Auth::user();
        $result = true;

        if (!is_null($user->idcustomer)) {
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }
}
