<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteHoliday extends Model
{
    protected $primaryKey = 'idholiday';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsite', 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function site(){
        return $this->belongsToMany(Site::class, 'idsite');
    }
}
