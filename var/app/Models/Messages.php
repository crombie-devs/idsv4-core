<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $primaryKey = 'id';
    protected $table = "messages";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'datetime', 'idlang', 'idformat', 'idcontent', 'subject', 'body', 'num_sends', 'num_received', 'num_opened', 'status', 'is_sync', 'created_at', 'updated_at'
    ];




    public function formats()
    {
        return $this->hasMany(Format::class, 'idformat', 'idformat');
    }

    public function lang()
    {
        return $this->hasOne(Lang::class, 'idlang', 'idlang');
    }




    public function contents()
    {
        return $this->hasMany(Content::class, 'idcontent', 'idcontent');
    }
}
