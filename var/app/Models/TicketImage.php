<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketImage extends Model
{
    protected $table = 'tickets_images';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_ticket',
        'url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function ticket() 
    {
        return $this->hasOne(Ticket::class, 'id', 'id_ticket');
    }

}
