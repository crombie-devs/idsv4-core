<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'phone',
        'title',
        'email',
        'point_address',
        'description',
        'id_priority',
        'id_category',
        'id_department',
        'id_responsible',
        'id_site',
        'id_status',
        'id_customer',
        'id_user',
        'finalized',
        'deleted',
        'accept',
        'expected_resolution_date',
        'notify_user',
        'date_alert',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function priority() 
    {
        return $this->hasOne(TicketPriority::class, 'id', 'id_priority');
    }

    public function category() 
    {
        return $this->hasOne(TicketCategory::class, 'id', 'id_category');
    }

    public function department() 
    {
        return $this->hasOne(TicketDepartment::class, 'id', 'id_department');
    }

    public function responsible() 
    {
        return $this->hasOne(TicketResponsible::class, 'id', 'id_responsible');
    }

    public function site() 
    {
        return $this->hasOne(Site::class, 'idsite', 'id_site');
    }

    public function status() 
    {
        return $this->hasOne(TicketStatus::class, 'id', 'id_status');
    }

    public function customer() 
    {
        return $this->hasOne(Customer::class, 'idcustomer', 'id_customer');
    }

    public function user() 
    {
        return $this->hasOne(User::class, 'iduser', 'id_user');
    }

    public function images() 
    {
        return $this->hasMany(TicketImage::class, 'id_ticket', 'id');
    }

    public function comments() 
    {
        return $this->hasMany(TicketComment::class, 'id_ticket', 'id')
            ->with('images')
            ->with('status')
            ->with('user')
            ->with('responsible');
    }
  
    public static function close(array $ids) 
    {
        $query = join(',' , array_fill(0, count($ids), '?'));  

        return DB::select('UPDATE ticket SET finalizado = 0, updated_at = NOW() WHERE id IN ('."$query".')', $ids);
    }

    public static function deleteTicket(array $ids) 
    {
        $query = join(',' , array_fill(0, count($ids), '?'));  

        return DB::select('UPDATE ticket SET deleted = 0 WHERE id IN ('."$query".')', $ids);
    }

}
