<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'guard_name', 'pivot', 'permissions'
    ];
}
