<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Impact extends Model
{
    protected $table = "difendif_python_impacto";
    protected $primaryKey = null;
    public $incrementing = false;

    public static function dataSumary($data){
        return Impact::where('idcustomer', $data['idcustomer'])
                        ->where('ejercicio',$data['year'])
                        ->where('mes', $data['month'])
                        ->where('status',0)->get()->toArray();
    }

    public static function dataGraph($data){
        return Impact::where('idcustomer',$data['idcustomer'])
        ->where('ejercicio',$data['year'])
        ->where('mes','>=',1)
        ->where('mes','<=',$data['month'])
        ->where('status',0)
        ->groupBy('ejercicio','mes')
        ->select('mes as mes','ejercicio as ejercicio')
        ->selectRaw('sum(pesocamp) as pesocamp')
        ->selectRaw('sum(diff_eur) as impactoeuros')
        ->get()
        ->toArray();
    }
}
