<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $primaryKey = 'id';
    protected $table = "peoples";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'phone', 'created_at', 'updated_at', 'idsite', 'idcustomer'
    ];



    public function tags()
    {
        return $this->hasMany(PeopleTags::class, 'idpeople', 'id')->with('tags');
    }
}
