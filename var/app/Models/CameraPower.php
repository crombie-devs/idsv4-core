<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CameraPower extends Model
{
    protected $primaryKey = 'idpower';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idpower', 'idcamera', 'weekday', 'time_on','time_off'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function camera(){
        $this->belongsTo(Camera::class, 'idcamera');
    }
}
