<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{   
    protected $table = "content_types";
    protected $primaryKey = 'idtype';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name'
    ];

    public function playlogics(){
        return $this->hasMany(PlayLogic::class, 'idtype', 'idtype');
    }

    public function contents(){
        return $this->hasMany(Content::class, 'idtype', 'idtype');
    }

}
