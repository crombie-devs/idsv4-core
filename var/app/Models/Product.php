<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    protected $primaryKey = 'idproduct';
    protected $table = "products";
    public $timestamps = false;

    protected $fillable = ['idcustomer', 'idcategory', 'code', 'name'];
    //  protected $hidden = ['pivot'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'idcategory');
    }

    public function tags()
    {
        $relation = $this->belongsToMany(Tag::class, 'products_has_tags', 'idproduct', 'idtag');
        $relation->getQuery()
            ->with('tagCategory');
        return $relation;
    }

    public function contents()
    {
        return $this->belongsToMAny(Content::class, 'contents_has_products', 'idproduct', 'idcontent');
    }

    public function isValid()
    {
        $result = true;

        $user = Auth::user();
        if (!is_null($user->idcustomer))
            $result = ($user->idcustomer == $this->idcustomer);

        dd($result, $user->idcustomer, $this->idcustomer);

        return $result;
    }
}
