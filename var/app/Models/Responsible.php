<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Responsible extends Model
{
    protected $primaryKey = 'idresponsible';
    protected $table = "responsibles";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'deleted', 'email', 'iddepartment'
    ];

    protected $hidden = [];


    public function department(){
        return $this->hasOne(Department::class, 'iddepartment', 'iddepartment');
    }

  

  
}