<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ContentOrder extends Model
{
 
    protected $table = "contents_order";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idarea', 'idplayer', 'idcontent', 'order'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      
    ];


    public function content(){
        return $this->hasOne(Content::class, 'idcontent', 'idcontent');
    }

    public function playarea(){
        return $this->belongsTo(PlayArea::class, 'idarea');
    }

    public function player(){
        return $this->belongsTo(Player::class, 'idplayer');
    }

}
