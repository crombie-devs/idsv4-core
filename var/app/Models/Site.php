<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $primaryKey = 'idsite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'idcity', 
        'address', 
        'zipcode', 
        'code', 
        'latitude', 
        'longitude', 
        'idcustomer',
        'idsite',
        'has_socioeconomic', 
        'status', 
        'deleted', 
        'license_expiration', 
        'closing_day' 
        ,'consider_in_sales'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function city(){
        return $this->belongsTo(City::class, 'idcity');
    }
    public function province(){
        return $this->hasOneThrough(
            Province::class,
            City::class,
            'idcity', // Foreign key on sites table...
            'idprovince', // Foreign key on customers table...
            'idcity', // Local key on cameras table...
            'idprovince' // Local key on customers table...
        );
    }
    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer');
       // return $this->hasOne(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function users(){
        return $this->hasMany(User::class, 'idsite');
    }

    public function siteHolidays(){
        return $this->hasMany(SiteHoliday::class, 'idsite');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'sites_has_tags','idsite','idtag');
    }

    public function content(){
        return $this->hasMany(Content::class, 'idsite');
    }

    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_sites','idsite','idcircuit');
    }

    public function circuit(){
        return $this->belongsToMany(PlayCircuit::class,'contents_has_sites','idcontent','idsite');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_sites','idsite','idcontent');
    }

    public function player(){
        return $this->hasMany(Player::class, 'idsite', 'idsite');
    }

    public function sitesOfCustomer($id){
        $query = Site::where("idcustomer",$id)
                     ->where('deleted',0)
                     ->where('status',1)
                     ->where(function($q){
                        $q->where('code','not like', 'ZZZZZ')    
                          ->orWhere('code', null);
                     })
                     ->select("name","idsite","code")->get();
        return $query;
    }
    public function sitesOfCustomerConsiderInSales($id){
        $query = Site::where("idcustomer",$id)
                     ->where('deleted',0)
                     ->where('status',1)
                     ->where('consider_in_sales',1)
                     ->where(function($q){
                        $q->where('code','not like', 'ZZZZZ')    
                          ->orWhere('code', null);
                     })
                     ->select("name","idsite","code")->get();
        return $query;
    }
    public static function getSuperAdminSites(){
        $sites = Site::where('sites.deleted', 0)->
                       where('sites.status', 1)->
                       select('sites.*')->
                       with('province')->
                       orderBy('name', 'ASC')->get();
        return $sites;
    }

    public static function getAdminSites($user){
        $idcustomer = $user->idcustomer;
        $sites = Site::where('sites.idcustomer', $idcustomer)
                           ->where('sites.deleted', 0)
                           ->where('sites.status', 1)
                           ->select('sites.*')
                           ->with('province')
                           ->orderBy('name', 'ASC')->get();
        return $sites;
    }

    public static function getUserSites($user){
        $sites = Site::where('sites.idsite', $user->idsite)
                            ->where('sites.deleted', 0)
                            ->where('sites.status', 1)
                            ->select('sites.*')
                            ->with('province')
                            ->with('playcircuits')
                            ->orderBy('name', 'ASC')->get();
        return $sites;
    }

    public static function getSite($idsite){
        $site = Site::where('sites.idsite', $idsite)
                            ->where('sites.deleted', 0)
                            ->where('sites.status', 1)
                            ->select('sites.*')
                            ->orderBy('idsite', 'ASC')->get()->first();
        return $site;
    }

    public static function getProvinceCountry($idcity){
        $provcount = City::where('idcity',$idcity)
        ->join('provincies', 'cities.idprovince', 'provincies.idprovince')
        ->join('countries', 'provincies.idcountry', 'countries.idcountry')
        ->select('provincies.idprovince','.countries.idcountry')
        ->first();

        return $provcount;
    } 

    public function isValid(){
        $user = Auth::user();
        $result=true;

        if(!is_null($user->idsite)){
            $result = ($user->idsite == $this->idsite);
        }
        else if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }

    public static function getSitesByCustomer($idcustomer){
        $sites = Site::where('sites.idcustomer', $idcustomer)
                           ->where('sites.deleted', 0)
                           ->where('sites.status', 1)
                           ->select('sites.*')
                           ->orderBy('name', 'ASC')->get();
        return $sites;
    }

    public static function filterByCustomer(int $idcustomer){
        return self::filter(['sites.idcustomer' => $idcustomer]);
    }

    public static function filterBySite(int $idsite){
        return self::filter(['sites.idsite' => $idsite]);
    }

    public static function filterAll(){
        return self::filter();
    }

    private static function filter($filter = null){
        $requests = Site::where('sites.deleted', 0)
                            ->where('sites.status', 1);
        if (!empty($filter)) 
            foreach($filter as $key => $value)
                $requests->where($key, $value);
                        
        $sites = $requests->select('sites.*')
                                ->orderBy('name', 'ASC')
                                ->get();
        return $sites;
    }

    public static function findById($idsite) {
        $sites = \DB::table('sites')
                ->join('cities', 'cities.idcity', '=', 'sites.idcity')
                ->join('province', 'provincies.idprovince', '=', 'cities.idprovince')
                ->join('countries', 'countries.idcountry', '=', 'provincies.idcountry')
                ->select('channel.idchannel', 'sites.idsite', 'sites.name as site_name', 'sites.address', 'sites.zipcode', 'cities.idcity', 'cities.name as city_name', 'provincies.idprovince', 'provincies.name as province_name', 'countries.idcountry', 'countries.name as country_name')
                ->where('sites.idsite', $idsite)
                ->first();

        return $sites;
    }

    public static function findByAddress($address, $zipcode, $idcity, $idprovince, $idcustomer) {
        $sites = \DB::table('sites')
                ->join('cities', 'cities.idcity', '=', 'sites.idcity')
                ->join('provincies', 'provincies.idprovince', '=', 'cities.idprovince')
                ->join('countries', 'countries.idcountry', '=', 'provincies.idcountry')
                ->select('sites.idsite', 'sites.name as site_name', 'sites.address', 'sites.zipcode', 'cities.idcity', 'cities.name as city_name', 'provincies.idprovince', 'provincies.name as province_name', 'countries.idcountry', 'countries.name as country_name')
                ->where('sites.idcustomer', $idcustomer)
                ->where('sites.address', $address)
                ->where('sites.zipcode', $zipcode)
                ->where('sites.idcity', $idcity)
                ->where('cities.idprovince', $idprovince)
                ->first();

        return $sites;
    }
    public function siteOfSite($idsite){
        $query = Site::where("idsite",$idsite)->where('deleted',0)->where('status',1)->select("name","idsite");              
        return $query->get();
    }
}   
