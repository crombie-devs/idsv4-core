<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecomendationsTrend extends Model
{
    protected $primaryKey = 'idtrendsrecomendations';
    public $timestamps = false;
    protected $table = "trends_recomendations";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer','idcategory','sales_last_4_weeks','trend','weight','trend_last_07days','trend_last_30days','trend_last_90days','colour_index','brands_number','name_brand_1','weight_brand_1','name_brand_2','weight_brand_2','name_brand_3','weight_brand_3','previous_day_menos1','previous_day_menos2','previous_day_menos3','previous_day_menos4','previous_day_menos5','previous_day_menos6','previous_day_menos7','idlocalarea'
    ];
}
