<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Symfony\Component\Console\Input\Input;


class Content extends Model
{
    protected $primaryKey = 'idcontent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcontent', 'idcustomer', 'idaudiocategory', 'idsite', 'idtype', 'idcategory', 'code', 'name', 'thumbnail_url', 'tracker_url',
        'max_emissions', 'frequency', 'frequency_method', 'is_exclusive', 'status', 'priority', 'primetime', 'idcampaign'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'order', 'pivot'
    ];


    public function customer()
    {
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function site()
    {
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }

    public function category()
    {
        return $this->belongsTo(ContentCategory::class, 'idcategory', 'idcategory');
    }

    public function type()
    {
        return $this->hasOne(ContentType::class, 'idtype', 'idtype');
    }

    public function assets()
    {
        $relation =  $this->belongsToMany(Asset::class, 'contents_has_assets', 'idcontent', 'idasset');
        $relation->getQuery()
            ->with('type')
            ->with('format')
            ->with('lang');
        return $relation;
    }

    public function sites()
    {
        return $this->belongsToMany(Site::class, 'contents_has_sites', 'idcontent', 'idsite');
    }

    public function powers()
    {
        return $this->hasMany(ContentPower::class, 'idcontent');
    }

    public function playcircuits()
    {
        $relation = $this->belongsToMany(PlayCircuit::class, 'contents_has_circuits', 'idcontent', 'idcircuit');
        $relation->getQuery()->with('langs')->with('sites')->with('cities')->with('provinces')->with('countries');
        return $relation;
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, 'contents_has_cities', 'idcontent', 'idcity');
    }
    public function playareas()
    {
        // return $this->belongsToMany(PlayArea::class,'contents_has_areas','idcontent','idarea');
        $relation =  $this->belongsToMany(PlayArea::class, 'contents_has_areas', 'idcontent', 'idarea');

        $relation->getQuery()
            ->with('format')
            ->with('location');
        return $relation;
    }



    public static function contentsDelete(array $idcontents)
    {
        $query = join(',', array_fill(0, count($idcontents), '?'));
        return DB::select('UPDATE contents SET deleted = 1 WHERE idcontent IN (' . "$query" . ')', $idcontents);
    }

    public function provinces()
    {
        return $this->belongsToMany(Province::class, 'contents_has_provincies', 'idcontent', 'idprovince');
    }

    public function dates()
    {
        return $this->hasMany(Date::class, 'idcontent', 'idcontent');
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class, 'contents_has_countries', 'idcontent', 'idcountry');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'contents_has_tags', 'idcontent', 'idtag');
    }

    public function products()
    {
        $relation = $this->belongsToMany(Product::class, 'contents_has_products', 'idcontent', 'idproduct');
        $relation->getQuery()->with('category');
        return $relation;
    }

    public function message()
    {
        return $this->hasMany(Messages::class, 'idcontent', 'idcontent')->with('formats')->with('lang');
    }

    public function orders()
    {

        return $this->hasMany(ContentOrder::class, 'idcontent', 'idcontent');
    }

    public static function getContentMessagesByCustomer($id)
    {
        $content = Content::has('message')
            ->with('message.lang')
            ->where('idcustomer', $id)
            ->where('deleted', 0);

        return $content;
    }



    public static function getDetail($id)
    {
        $content = Content::with('assets')
            ->with('tags')
            ->with('products')
            ->with('powers')
            ->with('playcircuits')
            ->with('playcircuits.sites')
            ->with('playareas')
            ->with('dates')
            ->with('message')
            ->with('message.lang')
            ->where('idcontent', $id)
            ->where('deleted', 0)->first();


        $content['sites'] = DB::table('contents_has_sites')->select('idsite', 'idcircuit')->where('idcontent', $content->idcontent)->get();

        return $content;
    }

    public static function getSuperAdmin($status = null, $date = null)
    {
        $contents = Content::with('playareas')
            ->with('playcircuits')
            ->with('assets')
            ->with('category')
            ->with('dates')
            ->where('deleted', 0);

        if (!is_null($date)) {
            $contents = $contents->whereExists(function ($query) use ($date) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            });
        }

        if (!is_null($status)) {
            $contents = $contents->where('status', 1);
        }

        $contents = $contents->orderBy('contents.idcontent', 'desc')->get();

        foreach ($contents as $content)
            $content['sites'] = DB::table('contents_has_sites')->select('idsite', 'idcircuit')->where('idcontent', $content->idcontent)->get();

        return $contents->unique('idcontent')->values();
    }

    public static function getContentsByMonth($user, $month)
    {
        $contents = Content::where('created_at', 'like', $month . '%')
            ->where('deleted', 0)
            ->where('status', 1)
            ->where('idcustomer', $user->idcustomer);

        if ($user->idsite)
            $contents->where('idsite', $user->idsite);
        return $contents->get();
    }
    public static function getAdmin($user, $status = null, $date = null)
    {
        $contents = Content::with('playareas')
            ->with('orders')
            ->with('playcircuits')
            ->with('assets')
            ->with('category')
            ->with('dates')
            ->where('idcustomer', $user->idcustomer)
            ->where('deleted', 0);

        if (!is_null($date)) {
            $contents = $contents->whereExists(function ($query) use ($date) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            });
        }

        if (!is_null($status)) {
            $contents = $contents->where('status', 1);
        }

        $contents = $contents->orderBy('contents.idcontent', 'desc')->get();

        foreach ($contents as $content)
            $content['sites'] = DB::table('contents_has_sites')->select('idsite', 'idcircuit')->where('idcontent', $content->idcontent)->get();

        return $contents->unique('idcontent')->values();
    }

    public static function getUser($user, $status = null, $date = null)
    {
        $contents = Content::with('playareas')
            ->with('playcircuits')
            ->with('category')
            ->with('assets')
            ->with('dates')
            ->join('contents_has_areas', 'contents.idcontent', 'contents_has_areas.idcontent')
            ->join('play_areas', 'contents_has_areas.idarea', 'play_areas.idarea')
            ->join('formats', 'play_areas.idformat', 'formats.idformat')
            ->where('idcustomer', $user->idcustomer)
            ->where('idsite', $user->idsite)
            ->where('deleted', 0)
            ->select(
                "contents.idcontent",
                "idcustomer",
                "idsite",
                "idtype",
                "idcategory",
                "code",
                "contents.name",
                "thumbnail_url",
                "tracker_url",
                "max_emissions",
                "frequency",
                "frequency_method",
                "is_exclusive",
                "status",
                "image_url",
                "priority"
            );

        if (!is_null($date)) {
            $contents = $contents->whereExists(function ($query) use ($date) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            });
        }

        if (!is_null($status)) {
            $contents = $contents->where('status', 1);
        }

        $contents = $contents->orderBy('contents.idcontent', 'desc')->get();

        foreach ($contents as $content)
            $content['sites'] = DB::table('contents_has_sites')->select('idsite', 'idcircuit')->where('idcontent', $content->idcontent)->get();

        return $contents->unique('idcontent')->values();;
    }

    public function isValid()
    {
        $user = Auth::user();
        $result = true;

        if (!is_null($user->idsite)) {
            $result = ($user->idsite == $this->idsite);
        } else if (!is_null($user->idcustomer)) {
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }

    public static function getDataPatternData($idcustomer, $idcategory)
    {
        $pattern = DB::table('pattern_data')
            ->where('idcustomer', $idcustomer)
            ->where('idcategory', $idcategory)
            ->where('month', 13)
            ->where('value', '>=', 9.5);

        return $pattern->get();
    }

    public static function getContentsForPlayer($idplayer, $date)
    {
        $datplayer = Player::join('sites', 'players.idsite',  'sites.idsite')
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->where('players.idplayer', $idplayer)
            ->with('site')
            ->first();



        $idcustomer = $datplayer->idcustomer;
        $idsite = $datplayer->idsite;

        $idarea = $datplayer->idarea;
        $codeSite = $datplayer->site->code;
        $weekday = new Carbon($date);

        $contents = Content::with('customer')
            ->with(['orders' => function ($query) use ($idplayer, $idarea) {
                $query->where('idplayer', $idplayer)->orWhere('idarea', $idarea);
            }])
            ->with('site')
            ->with('type')
            ->with('category')
            ->with('tags')
            ->with('powers')
            ->with('cities')
            ->with('provinces')
            ->with('countries')
            ->with('playcircuits')
            ->with('assets')
            ->with('products')
            ->with('playareas')
            ->with('dates')
            ->where(function ($query) use ($idcustomer, $idsite) {
                $query->whereNull('contents.idcustomer')
                    ->orWhere('contents.idcustomer', $idcustomer)
                    ->whereNull('contents.idsite')
                    ->orWhere('contents.idsite', $idsite);
            })
            ->whereExists(function ($query) use ($date) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            })
            ->where('status', 1)
            ->where('deleted', 0)
            ->get();

        foreach ($contents as $content) {
            $content['sites'] = DB::table('contents_has_sites')
                ->select('idsite', 'idcircuit')
                ->distinct()
                ->where('idcontent', $content->idcontent)
                ->get();

            $content['primetimes'] = [];

            if ($content['primetime']) {
                $primetime = [];
                $prod = [];
                $sites = [];

                foreach ($content['products'] as $product)
                    $prod[] = self::getCategory($product);

                $primetime = PrimeTime::where('idcustomer', $content->customer->idcustomer)
                    ->where('code_site', $codeSite)
                    ->whereIn('idcategory', $prod)
                    ->select('weekday', 'hour')
                    ->distinct()
                    ->orderBy('weekday', 'ASC')
                    ->orderBy('hour', 'ASC')
                    ->get()->toArray();

                $content['primetimes'] = self::organizePrimetime($primetime);
            }
        }

        $primetime = PrimeTime::where('idcustomer', $content->customer->idcustomer)
            ->where('code_site', $codeSite)
            ->whereIn('idcategory', $prod)
            ->select('weekday', 'hour')
            ->distinct()
            ->orderBy('weekday', 'ASC')
            ->orderBy('hour', 'ASC')
            ->get()->toArray();


        $content['primetimes'] = self::organizePrimetime($primetime);
        $contents = collect($contents)->except(
            'idcustomer',
            'idsite',
            'idtype',
            'idcategory',
            '
            thumbnail_url',
            'tracker_url',
            'max_emissions',
            'frequency',
            'frequency_method',
            'is_exclusive',
            'max_emissions',
            'frequency',
            'customer.email',
            "customer.phone",
            "customer.image_url",
            "customer.gaid",
            "customer.has_sales",
            "customer.has_influxes",
            "category.idcustomer",
            "category,image_url"
        );

        return $contents;
    }

    public function deleteContentPower($idtime)
    {
        $product = DB::table('content_powers')->where('idcontent', $this->idcontent)->where('idtime', $idtime);
        $product->delete();
    }

    public function deleteContentDate($idtime)
    {
        $product = DB::table('content_powers')->where('idcontent', $this->idcontent)->where('idtime', $idtime);
        $product->delete();
    }

    public static function getOrderSuperAdmin($date)
    {
        $orders = ContentOrder::with(['content' => function ($query) use ($date) {
            $query->whereExists(function ($query2) use ($date) {
                $query2->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            })
                ->where('deleted', 0);
        }])->get();

        return $orders;
    }

    public static function getOrderAdmin($user, $date)
    {
        $orders = ContentOrder::with(['content' => function ($query) use ($user, $date) {
            $query->where('idcustomer', $user->idcustomer)
                ->whereExists(function ($query2) use ($date) {
                    $query2->select(DB::raw(1))
                        ->from('contents_dates')
                        ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                        ->where('contents_dates.date_on', '<=', $date)
                        ->where('contents_dates.date_off', '>=', $date);
                })
                ->where('deleted', 0);
        }])->get();

        return $orders;
    }



    public static function getOrderUser($user, $date)
    {
        $orders = ContentOrder::with(['content' => function ($query) use ($user, $date) {
            $query->where('idcustomer', $user->idcustomer)
                ->where('idsite', $user->idsite)
                ->whereExists(function ($query2) use ($date) {
                    $query2->select(DB::raw(1))
                        ->from('contents_dates')
                        ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                        ->where('contents_dates.date_on', '<=', $date)
                        ->where('contents_dates.date_off', '>=', $date);
                })
                ->where('deleted', 0);
        }])->get();

        return $orders;
    }

    public function getContentsAndProdsFromCustomer($idcustomer, $dateStart = 0, $dateEnd = 0)
    {
        $query = Content::with("dates")->with('sites')->with('playcircuits')->join("contents_has_products", "contents_has_products.idcontent", "=", "contents.idcontent")
            ->select('contents.idcontent', 'contents.name', 'contents.idcustomer')
            ->groupBy('contents.idcontent')
            ->where("contents.deleted", 0)
            ->where('contents.idcustomer', $idcustomer);

        if ($dateStart != 0 && $dateEnd != 0) {
            $query->join('contents_dates', 'contents_dates.idcontent', '=', 'contents.idcontent')
                ->where('contents_dates.date_on', '<=', $dateEnd)
                ->where('contents_dates.date_off', '>=', $dateStart);
        }

        $query->orderby('contents.idcontent', 'ASC');

        $datacontents = $query->get();
        $a_contents = [];
        if ($datacontents->count() > 0) {
            foreach ($datacontents as $key => $datcont) {
                $queryprod = DB::table('products')->select('products.code')->join('contents_has_products', 'contents_has_products.idproduct', '=', 'products.idproduct')->where('contents_has_products.idcontent', $datcont->idcontent)->get()->pluck('code')->toArray();
                $datcont->products = $queryprod;
                array_push($a_contents, $datcont);
            }
        }
        return $a_contents;
    }

    public function getContentsAndProdsFromCustomerMultipleDate($idcustomer, $dateStart = 0, $dateEnd = 0)
    {
        $query = Content::join("contents_has_products", "contents_has_products.idcontent", "=", "contents.idcontent")
            ->select('contents.idcontent', 'contents.name', 'contents.idcustomer')
            ->with('dates')
            ->groupBy('contents.idcontent')
            ->where("contents.deleted", 0)
            ->where('contents.idcustomer', $idcustomer);


        if ($dateStart != 0 && $dateEnd != 0) {
            $query->whereExists(function ($query) use ($dateEnd, $dateStart) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $dateEnd)
                    ->where('contents_dates.date_off', '>=', $dateStart);
            });
        }

        $query->orderby('contents.idcontent', 'ASC');

        $datacontents = $query->get();
        $a_contents = [];
        if ($datacontents->count() > 0) {
            foreach ($datacontents as $key => $datcont) {
                $queryprod = DB::table('products')->select('products.code')->join('contents_has_products', 'contents_has_products.idproduct', '=', 'products.idproduct')->where('contents_has_products.idcontent', $datcont->idcontent)->get()->pluck('code')->toArray();
                $datcont->products = $queryprod;
                array_push($a_contents, $datcont);
            }
        }
        return $a_contents;
    }

    public static function getContentsForSite($idsite, $date = null)
    {
        $hoy = empty($date) ? Carbon::today() : $date;

        $datsite = Site::select('sites.idcustomer', 'sites.code')
            ->where('sites.idsite', $idsite)
            ->first();

        $idcustomer = $datsite->idcustomer;
        $codeSite = $datsite->code;

        $contents = Content::with('customer')
            ->with('orders')
            ->with('site')
            ->with('type')
            ->with('category')
            ->with('tags')
            ->with('powers')
            ->with('cities')
            ->with('provinces')
            ->with('countries')
            ->with('playcircuits')
            ->with('assets')
            ->with('products')
            ->with('playareas')
            ->with('dates')
            ->where(function ($query) use ($idcustomer, $idsite) {
                $query->whereNull('idcustomer')->orWhere('idcustomer', $idcustomer)->whereNull('idsite')->orWhere('idsite', $idsite);
            })
            ->whereExists(function ($query) use ($hoy) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $hoy)
                    ->where('contents_dates.date_off', '>=', $hoy);
            })
            ->where('status', 1)
            ->where('deleted', 0)
            ->get();

        foreach ($contents as $content) {
            $content['sites'] = DB::table('contents_has_sites')
                ->select('idsite', 'idcircuit')
                ->distinct()
                ->where('idcontent', $content->idcontent)
                ->get();

            $content['primetimes'] = [];

            if ($content['primetime']) {
                $primetime = [];
                $prod = [];
                $sites = [];

                foreach ($content['products'] as $product)
                    $prod[] = self::getCategory($product);

                $primetime = PrimeTime::where('idcustomer', $content->customer->idcustomer)
                    ->where('code_site', $codeSite)
                    ->whereIn('idcategory', $prod)
                    ->select('weekday', 'hour')
                    ->distinct()
                    ->orderBy('weekday', 'ASC')
                    ->orderBy('hour', 'ASC')
                    ->get()->toArray();

                $content['primetimes'] = self::organizePrimetime($primetime);
            }
        }

        $contents = collect($contents)->except(
            'idcustomer',
            'idsite',
            'idtype',
            'idcategory',
            '
            thumbnail_url',
            'tracker_url',
            'max_emissions',
            'frequency',
            'frequency_method',
            'is_exclusive',
            'max_emissions',
            'frequency',
            'customer.email',
            "customer.phone",
            "customer.image_url",
            "customer.gaid",
            "customer.has_sales",
            "customer.has_influxes",
            "category.idcustomer",
            "category,image_url"
        );
        return $contents;
    }

    public static function organizePrimetime($primetimes)
    {
        $p = [];
        foreach ($primetimes as $primetime) {
            $p[$primetime['weekday']][] = $primetime['hour'];
        }
        $result = [];
        $i = 0;

        foreach ($p as $weekday => $hours) {
            $time_on = 0;
            $time_off = 0;
            for ($i = 0; $i < count($hours); $i++) {
                $result[] = array("weekday" => $weekday, "time_on" => $hours[$i] < 10 ? "0$hours[$i]:00" : "$hours[$i]:00", "time_off" => $hours[$i] < 10 ? "0$hours[$i]:59" : "$hours[$i]:59");
            }
        }
        return $result;
    }

    private static function getSitesForPrimetime($content, $idsite)
    {
        $sites = [];
        if (count($content['sites']) > 0) { //tiene idsite, idcitcuits

            foreach ($content['sites'] as $site) {
                $s = Site::getSite($site->idsite);
                $sites[] = $s['code'];
            }
        } else {
            foreach ($content['playcircuits'] as $circuit) {
                foreach ($circuit['sites'] as $site) {
                    $sites[] = $site['code'];
                }
            }
        }

        return $sites;
    }

    private static function getCategory($product)
    {
        $category = DB::table('product_categories')->select('code')->where('idcategory', $product['idcategory'])->get()->first();
        if ($category) {
            return $category->code;
        } else {
            return null;
        }
    }




    public function getContentsFromCustomerSiteNotProd($idcustomer, $siteid, $dateStart, $dateEnd)
    {
        $user = auth()->user();
        $sfields = DB::Raw('contents.*');


        $query = Content::with('dates')->join("contents_has_products", "contents_has_products.idcontent", "=", "contents.idcontent")
            ->join("contents_dates", "contents_dates.idcontent", "=", "contents.idcontent")
            ->select($sfields)
            ->groupBy('contents.name')
            ->orderby('contents.name', 'ASC')
            ->where("contents.deleted", 0)
            ->where('contents.idcustomer', $idcustomer);

        if ($siteid > 0) {
            $a_siteid =  explode(",", $siteid);
            $nsiteid = Site::whereIn('code', $a_siteid)->select('idsite')
                ->get()->pluck('idsite');

            $query = Content::with('dates')->join("contents_has_products", "contents_has_products.idcontent", "=", "contents.idcontent")
                ->join("sites", "sites.idsite", "=", "contents.idsite")
                ->join('players', 'players.idsite', '=', 'sites.idsite')
                ->where("players.deleted", 0)
                ->where("players.status", 1)
                ->where("contents.deleted", 0)
                ->orderBy("contents.name", 'ASC')
                ->groupBy('contents.idcontent')
                ->select('contents.*');

            if ($dateStart != 0 && $dateEnd != 0) {
                $query->where('contents_dates.date_on', '<=', $dateEnd)
                    ->where('contents_dates.date_off', '>=', $dateStart);
            }
            $daf = $query;
            $result = $query->get();
            $tmp_content = [];
            $tmp_err = [];
            $client = new \GuzzleHttp\Client();
            foreach ($result as $res) {
                $ts =  Carbon::now()->timestamp;
                $current_timestamp = $ts * 1000;
                $string = $current_timestamp . 'ALRIDKJCS1SYADSKJDFS';
                $keyhash = md5($string);
                $json = array(
                    'idcustomer' => intval($idcustomer),
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd,
                    'idcampaign' => $res->idcampaign,
                    "idsite" => $nsiteid[0]
                );
                $responseCampaign = $client->request('POST', env('URL_MONGO') . 'countByCampaign?timestamp=' . $current_timestamp . '&keyhash=' . $keyhash, ['json' => $json, 'timeout' => 0, 'verify' => false]);
                $count = json_decode($responseCampaign->getBody(), true);
                if (count($count['data']) != 0) {
                    $getCount = $count['data'][0];
                    if ($getCount['count'] > 0) {
                        array_push($tmp_acampaign, $getCount["_id"]['idcampaign']);
                    }
                } else {
                    array_push($tmp_err, $res->idcontent);
                }
            }
            $result_final = $daf->whereIn('contents.idcontent', $tmp_content)->get();
            return $result_final;
        }
        if ($dateStart != 0 && $dateEnd != 0) {
            $query->whereExists(function ($query) use ($dateEnd, $dateStart) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $dateEnd)
                    ->where('contents_dates.date_off', '>=', $dateStart);
            });
        }
        $query->orderby('contents.idcontent', 'DESC');
        $result = $query->get();
        return $result;
    }

    public function getContentsByDate($idcustomer, $adates)
    {
        $sfields = DB::Raw('contents.*');

        $query = Content::join("contents_has_products", "contents_has_products.idcontent", "=", "contents.idcontent")
            ->join("contents_dates", "contents_dates.idcontent", '=', 'contents.idcontent')
            ->select($sfields)
            ->groupBy('contents.name')
            ->orderby('contents.name', 'ASC')
            ->where("contents.deleted", 0)
            ->where('contents.idcustomer', $idcustomer)
            ->where('contents_dates.date_on', '<=', $adates['date_off'])
            ->where('contents_dates.date_off', '>=', $adates['date_on']);

        $query->orderby('contents.idcontent', 'DESC');
        $result = $query->get();
        return $result;
    }

    public function getDateOnDateOffContents($a_contents)
    {
        $query = Content::with('dates')->whereIn('contents.idcontent', $a_contents)
            ->join("contents_dates", "contents_dates.idcontent", '=', 'contents.idcontent')
            ->select('contents.idcontent')
            ->get();
        return $query;
    }

    public function getDateOnDateOffContentsMultipleDate($a_contents)
    {
        $query = DB::table('contents_dates')->whereIn('idcontent', $a_contents)
            ->select('idcontent', 'date_on', 'date_off')
            ->get();
        return $query;
    }

    public static function getContentsTomorrowByCustomer($idcustomer)
    {
        $date = Carbon::tomorrow();
        $contents = Content::select(
            'contents.idcontent',
            'contents.idtype',
            'contents.code',
            'contents.name',
            'content_categories.idcategory',
            'contents_dates.date_on',
            'contents_dates.date_off',
            'assets.duration',
            'asset_types.idtype as asset_idtype',
            'asset_types.name as asset_type',
            'assets.asset_url as asset_url',
            'assets.size'
        )
            ->join('content_categories', 'content_categories.idcategory', '=', 'contents.idcategory')
            ->join('contents_dates', 'contents_dates.idcontent', '=', 'contents.idcontent')
            ->join('contents_has_assets', 'contents_has_assets.idcontent', '=', 'contents.idcontent')
            ->join('assets', 'assets.idasset', '=', 'contents_has_assets.idasset')
            ->join('asset_types', 'asset_types.idtype', '=', 'assets.idtype')
            ->with('playcircuits')
            ->with('playareas')
            ->with('powers')
            ->where('contents.idcustomer', $idcustomer)
            ->where('contents.deleted', 0)
            ->where('contents.status', 1)
            ->whereExists(function ($query) use ($date) {
                $query->select(DB::raw(1))
                    ->from('contents_dates')
                    ->whereColumn('contents_dates.idcontent', 'contents.idcontent')
                    ->where('contents_dates.date_on', '<=', $date)
                    ->where('contents_dates.date_off', '>=', $date);
            });

        $contents = $contents->orderBy('contents.idcontent', 'desc')->get();

        foreach ($contents as $content)
            $content['sites'] = DB::table('contents_has_sites')->select('idsite', 'idcircuit')->where('idcontent', $content->idcontent)->get();

        return $contents->unique('idcontent')->values();
    }
}
