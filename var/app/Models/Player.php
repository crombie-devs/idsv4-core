<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $primaryKey = 'idplayer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsite', 'email', 'idarea', 'idlang', 'status', 'deleted', 'idtemplate', 'is_sync',
        'instalation_date', 'support_end_date', 'hardware_type', 'software_version', 'serial_number',
        'screen_resolution', 'installer_name', 'connected_point_number', 'is_monitored'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'players_has_tags', 'idplayer', 'idtag');
    }

    public function area()
    {
        $relation = $this->hasOne(PlayArea::class, 'idarea', 'idarea');
        $relation->getQuery()
            ->with('format')
            ->with('location');
        return $relation;
    }

    public function lang()
    {
        return $this->hasOne(Lang::class, 'idlang', 'idlang');
    }

    public function powers()
    {
        return $this->hasMany(PlayerPower::class, 'idplayer');
    }

    public function site()
    {
        return $this->hasOne(Site::class, 'idsite', 'idsite');
    }

    public function template()
    {
        return $this->hasOne(Template::class, 'idtemplate', 'idtemplate');
    }

    public function playlogics()
    {
        return $this->hasMany(PlayLogic::class, 'idplayer', 'idplayer');
    }
    public function playlogicsByArea()
    {
        return $this->hasMany(PlayLogic::class, 'idarea', 'idarea');
    }
    public function contentsorder()
    {
        return $this->hasMany(ContentOrder::class, 'idplayer', 'idplayer');
    }

    public function customer()
    {
        return $this->hasOneThrough(
            Customer::class,
            Site::class,
            'idsite', // Foreign key on sites table...
            'idcustomer', // Foreign key on customers table...
            'idsite', // Local key on cameras table...
            'idcustomer' // Local key on customers table...
        );
    }

    public function widgets()
    {
        return $this->hasMany(Widget::class, 'idviewer');
    }

    public static function getSuperAdminPlayers()
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('players.deleted', 0)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getAdminPlayers($user)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('sites.idcustomer', $user->idcustomer)
            ->where('players.deleted', 0)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getUserPlayers($user)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('sites.idcustomer', $user->idcustomer)
            ->where('players.idsite', $user->idsite)
            ->where('players.deleted', 0)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getSuperAdminPlayersToselect()
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site')->with('site.province')
            ->with('area')
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getAdminPlayersToselect($user)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site')->with('site.province')
            ->with('area')
            ->where('sites.idcustomer', $user->idcustomer)
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getUserPlayersToselect($user)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site')->with('site.province')
            ->with('area')
            ->where('sites.idcustomer', $user->idcustomer)
            ->where('players.idsite', $user->idsite)
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getToAnalytics($params)
    {
        $players = Player::where('idsite', $params['idsite'])
            ->select('players.*')
            ->get();

        return $players;
    }

    public static function getPlayerData($id)
    {
        $player = Player::with('tags')
            ->with('powers')
            ->with('site:idsite,name')
            ->with('customer')

            ->where('players.idplayer', $id)
            ->where('players.deleted', 0)
            ->first();



        if (!is_null($player)) {

            return $player;
        } else {
            return false;
        }
    }

    public static function getPlaylist($idcustomer, $idarea)
    {
        $playlist = Player::where('idarea', $idarea)
            ->where('deleted', 0)
            ->first();

        return $playlist;
    }

    //falta template
    public static function getDetailPlayer($email)
    {

        $player = Player::with('site')
            // ->with('app')
            ->with('area')
            ->with('lang')
            ->with('tags')
            ->with('powers')
            ->with('widgets')
            ->where('email', $email)
            ->where('status', 1)
            ->where('deleted', 0)->first();

        if (empty($player))
            return null;


        unset($player['status'], $player['deleted']);

        if (isset($player['area']['idformat'])) {
            unset($player['area']['idformat']);
        }
        if (isset($player['area']['idlocation'])) {
            unset($player['area']['idlocation']);
        }
        if (isset($player['area']['description'])) {
            unset($player['area']['description']);
        }

        if (isset($player['lang']['image_url'])) {
            unset($player['lang']['image_url']);
        }


        $player['customer'] = collect(Customer::where('idcustomer', $player['site']->idcustomer)->first())->only('idcustomer', 'name', 'has_sales')->toArray();
        $player['app'] = collect(App::where('idcustomer', $player['site']->idcustomer)->first())->only('idapp', 'name')->toArray();

        $player['site']['holidays'] =  collect(SiteHoliday::where('idsite', $player['idsite'])->select('idholiday', 'date')->get())->toArray();

        $player['city'] = collect(City::where('idcity', $player['site']->idcity)->first())->toArray();
        $player['province'] = collect(Province::where('idprovince', $player['city']['idprovince'])->first())->toArray();
        $player['country'] = collect(Country::where('idcountry', $player['province']['idcountry'])->first())->toArray();

        return $player;
    }

    public static function getDetailExternalAllPlayers($idcustomer)
    {
        $players = \DB::table('players')
            ->select('players.idplayer as idviewer', 'players.code as reference', 'players.email', 'players.status', 'players.deleted', 'players.idsite', 'sites.name as site_name', 'customers.idcustomer', 'customers.name as customer_name')
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where('customers.idcustomer', $idcustomer)
            ->where('players.status', 1)
            ->where('players.deleted', 0)
            ->get();

        if (empty($players))
            return null;

        $result = [];
        foreach ($players as $player) {
            $channel['idcustomer'] =  $player->idcustomer;
            $channel['customer_name'] =  $player->customer_name;
            $channel['idsite'] =  $player->idsite;
            $channel['site_name'] =  $player->site_name;

            $data['viewer'] = collect($player)->only('idviewer', 'reference', 'email', 'status', 'deleted');
            $data['channel'] = $channel;
            $data['power'] = collect(PlayerPower::where('idplayer', $player->idviewer)->get())->toArray();
            $data['holiday'] = collect(SiteHoliday::where('idsite', $player->idsite)->select('idholiday', 'date')->get())->toArray();

            $result[] = $data;
        }

        return $result;
    }

    public static function getDetailOtisPlayer($email)
    {

        $player = \DB::table('players')
            ->select('players.idplayer as idviewer', 'players.code as reference', 'players.email', 'players.status', 'players.deleted', 'players.idsite', 'sites.name as site_name', 'customers.idcustomer', 'customers.name as customer_name')
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where('players.email', $email)
            ->where('players.status', 1)
            ->where('players.deleted', 0)
            ->first();

        if (empty($player))
            return null;

        $channel['idcustomer'] =  $player->idcustomer;
        $channel['customer_name'] =  $player->customer_name;
        $channel['idsite'] =  $player->idsite;
        $channel['site_name'] =  $player->site_name;

        $data['viewer'] = collect($player)->only('idviewer', 'reference', 'email', 'status', 'deleted');
        $data['channel'] = $channel;
        $data['power'] = collect(PlayerPower::where('idplayer', $player->idviewer)->get())->toArray();
        $data['holiday'] = collect(SiteHoliday::where('idsite', $player->idsite)->select('idholiday', 'date')->get())->toArray();

        return $data;
    }


    public static function getDetailPlayerById($idplayer)
    {
        return self::getDetailPlayerByFilter(['idplayer' => $idplayer]);
    }

    public static function getDetailPlayerByCustomer($idcustomer)
    {
        return self::getDetailPlayerByFilter(['site.idcustomer' => $idcustomer]);
    }

    public static function getDetailPlayerBySite($idsite)
    {
        return self::getDetailPlayerByFilter(['idsite' => $idsite]);
    }

    public static function getDetailPlayerAll()
    {
        return self::getDetailPlayerByFilter();
    }

    private static function getDetailPlayerByFilter($filter = null)
    {
        $request = Player::join("sites", "sites.idsite", "=", "players.idsite")->
                           join('customers', 'sites.idcustomer', 'customers.idcustomer')->
                           with('area')->
                           with('lang')->
                           with('tags')->
                           with('powers')->
                           with('widgets')->
                           where('players.status', 1)->
                           where('players.deleted', 0)->
                           where('sites.status', 1)->
                           where('sites.deleted', 0)->
                           where('customers.status', 1)->
                           where('customers.deleted', 0)->
                           select('players.*');

        if (!empty($filter))
            foreach ($filter as $key => $value)
                $request->where($key, $value);

        $players = $request->get();
        foreach ($players as $player) {
            unset($player['status'], $player['deleted']);
            if (isset($player['area']['idformat'])) {
                unset($player['area']['idformat']);
            }
            if (isset($player['area']['idlocation'])) {
                unset($player['area']['idlocation']);
            }
            if (isset($player['area']['description'])) {
                unset($player['area']['description']);
            }
            if (isset($player['lang']['image_url'])) {
                unset($player['lang']['image_url']);
            }
            $player['customer'] = collect(Customer::where('idcustomer', $player['site']->idcustomer)->first())->only('idcustomer', 'name', 'has_sales')->toArray();
            $player['app'] = collect(App::where('idcustomer', $player['site']->idcustomer)->first())->only('idapp', 'name')->toArray();
            $player['site'] =  Site::where('idsite', $player['idsite'])->first();
            $player['site']['holidays'] =  collect(SiteHoliday::where('idsite', $player['idsite'])->select('idholiday', 'date')->get())->toArray();
            $player['city'] = collect(City::where('idcity', $player['site']->idcity)->first())->toArray();
            $player['province'] = collect(Province::where('idprovince', $player['city']['idprovince'])->first())->toArray();
            $player['country'] = collect(Country::where('idcountry', $player['province']['idcountry'])->first())->toArray();
        }
        return $players;
    }


    public static function getListPlayLogics($idplayer)
    {
        $player = Player::where('idplayer', $idplayer)->where('status', 1)->where('deleted', 0)->get();

        if ($player->isNotEmpty()) {
            $playLogic = PlayLogic::with(['contentCategory:idcategory,name,image_url,random', 'type'])->where('idplayer', $idplayer)->orderBy('order', 'ASC')->get();
            if ($playLogic->isEmpty()) {
                $idarea = $player->first()->idarea;
                $playLogic = PlayLogic::with(['contentCategory:idcategory,name,image_url,random', 'type'])->where('idarea', $idarea)->orderBy('order', 'ASC')->get();
                if ($playLogic->isEmpty()) {
                    return false;
                }
            }

            return collect($playLogic)->except(['idcategory', 'content_category.idcustomer']);
        }

        return false;
    }

    public static function getPlayLogics()
    {
        $player = Player::where('status', 1)->where('deleted', 0)->get();

        if ($player->isNotEmpty()) {
            $playLogic = PlayLogic::with(['contentCategory:idcategory,name,image_url', 'type'])->orderBy('order', 'ASC')->get();
            if ($playLogic->isEmpty()) {
                $idarea = $player->first()->idarea;
                $playLogic = PlayLogic::with(['contentCategory:idcategory,name,image_url', 'type'])->where('idarea', $idarea)->orderBy('order', 'ASC')->get();
                if ($playLogic->isEmpty()) {
                    return false;
                }
            }
            return collect($playLogic)->except(['idcategory', 'content_category.idcustomer']);
        }
        return false;
    }

    public static function getPlayLogicsExternal($player, $idcustomer)
    {
        $playLogic = PlayLogic::select('content_categories.idcategory', 'content_categories.name')->join('content_categories', 'content_categories.idcategory', '=', 'play_logics.idcategory')->where('play_logics.idcustomer', $idcustomer)->distinct()->orderBy('content_categories.idcategory', 'ASC')->get();
        if ($playLogic->isEmpty()) {
            $idarea = $player->first()->idarea;
            $playLogic = PlayLogic::select('content_categories.idcategory', 'content_categories.name')->join('content_categories', 'content_categories.idcategory', '=', 'play_logics.idcategory')->where('play_logics.idcustomer', $idcustomer)->where('idarea', $idarea)->distinct()->orderBy('content_categories.idcategory', 'ASC')->get();
            if ($playLogic->isEmpty()) {
                return false;
            }
        }
        return collect($playLogic);
    }

    public function isValid()
    {
        return $this->site()->first()->isValid();
    }

    //Support

    public static function getPlayer()
    {
        $player = Player::select("players.email", "players.idplayer", "customers.idcustomer", "sites.idsite")
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->with("site")
            ->with("customer")
            ->where("players.status", 1)
            ->where("players.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->orderBy("players.email")
            ->get();
        return $player;
    }

    public static function getPlayerBySite($idsite)
    {
        $player = Player::select("players.email", "players.idplayer")
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where('sites.idsite', '=', $idsite)
            ->where("players.status", 1)
            ->where("players.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->orderBy("players.email")
            ->get();
        return $player;
    }

    public static function getPlayerByCustomer($idcustomer)
    {
        $player = Player::select("players.email", "players.idplayer", "players.idsite", "sites.name")
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->with('powers')
            ->where('customers.idcustomer', '=', $idcustomer)
            ->where('players.status', 1)
            ->where('players.deleted', 0)
            ->orderBy('players.email')
            ->get();
        return $player;
    }

    public static function totalPlayerGroupByCustomer()
    {
        $players = Player::select("customers.idcustomer", "customers.name", "customers.email", Player::raw("COUNT(players.idplayer) as total"))
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where("players.status", 1)
            ->where("players.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->where("customers.status", 1)
            ->where("customers.deleted", 0)
            ->groupBy("customers.idcustomer", "customers.idcustomer", "customers.email", "customers.name")
            ->orderBy("customers.name")
            ->get();

        return $players;
    }

    public static function totalPlayerGroupBySite($idcustomer)
    {
        $players = Player::select("sites.idsite", "sites.name", "sites.email", Player::raw("COUNT(players.idplayer) as total"))
            ->join("sites", "sites.idsite", "=", "players.idsite")
            ->join("customers", "customers.idcustomer", "=", "sites.idcustomer")
            ->where("customers.idcustomer", $idcustomer)
            ->where("players.status", 1)
            ->where("players.deleted", 0)
            ->where("sites.status", 1)
            ->where("sites.deleted", 0)
            ->where("customers.status", 1)
            ->where("customers.deleted", 0)
            ->groupBy("sites.idsite", "sites.name", "sites.email", "sites.name")
            ->orderBy("sites.name")
            ->get();

        return $players;
    }

    public static function getPlayersByCustomer($idcustomer)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('sites.idcustomer', $idcustomer)
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getPlayersBySite(int $idsite)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('sites.idsite', $idsite)
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function getPlayersByArea(int $idarea)
    {
        $players = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('players.idarea', $idarea)
            ->where('players.deleted', 0)
            ->where('players.status', 1)
            ->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function filterBySite(int $idsite)
    {
        return self::filter(['sites.idsite' => $idsite]);
    }

    public static function filterByCustomer(int $idcustomer)
    {
        return self::filter(['sites.idcustomer' => $idcustomer]);
    }

    public static function filterByPlayer(int $idplayer)
    {
        return self::filter(['idplayerr' => $idplayer]);
    }

    public static function filterAll()
    {
        return self::filter();
    }

    private static function filter($filter = null)
    {
        $requests = Player::join('sites', 'players.idsite', '=', 'sites.idsite')
            ->with('site:idsite,name')
            ->with('area')
            ->where('players.deleted', 0)
            ->where('players.status', 1);
        if (!empty($filter))
            foreach ($filter as $key => $value)
                $requests->where($key, $value);

        $players = $requests->select('players.*', 'sites.idcustomer as idcustomer')
            ->orderBy('email')
            ->get();
        return $players;
    }

    public static function findByEmail($email)
    {
        $query = \DB::table('players')
            ->join("sites", "sites.idsite", "=", "players.idsite")
            //->where("channel.deleted",0)
            //->where("viewer.deleted",0)
            //->where("viewer.status",1)
            //->where("site.deleted",0)
            ->where("players.email", 'like', $email)
            ->select("players.*");

        return $query->first();
    }
}
