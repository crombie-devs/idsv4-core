<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $primaryKey = 'idtag';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcategory','name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    public function tagCategory(){
        return $this->belongsTo(TagCategory::class, 'idcategory');
    }
    public function players(){
        return $this->belongsToMany(Player::class, 'players_has_tags', 'idtag', 'idplayer');
    }

    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_tags','idtag','idcircuit');
    }
    
    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_tags','idtag','idcontent');
    }

    public static function tagsProtect(){
        $tags = new Tag();         
        $tags = $tags->with('tagCategory');
        return $tags->get();
    }

    public static function byType($type, $idcustomer) {
        $tags = Tag::with(['tagCategory' => function($query) use ($type, $idcustomer) {
            $query->where('tagtype', $type)->where('idcustomer', $idcustomer); 
        }
    ])->get();
        
        return $tags;
    }

    public static function getSuperAdmin($withtags=null){
        $tags = new Tag();
         
        if(!is_null($withtags)){
            $tags = $tags->with('tagCategory');
        }
 
        return $tags->get();
    }

    public static function getAdmin($user,$withtags=null){
        $tags=[];
        $idtagscategory = TagCategory::where('idcustomer',$user->idcustomer)->pluck('idcategory');

        if($idtagscategory->isNotEmpty()){
            if(!is_null($withtags)){
                $tags = Tag::with('tagCategory')->whereIn('idcategory',$idtagscategory )->get()->toArray();
            }
            else{
                $tags = Tag::with('tagCategory')->whereIn('idcategory',$idtagscategory )->get()->toArray();
            }
        }
        return $tags;
    }

    public static function getDetail($id){
        $tag = Tag::with('tagCategory')
                        ->where('idtag', $id)
                        ->first();

        return $tag;
    }

    public function isValid(){
        return $this->tagCategory()->first()->isValid();
    }
}
