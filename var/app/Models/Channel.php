<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $primaryKey = 'idchannel';

    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_channels','idchannel','idcircuit');
    }

    public function app(){
        return $this->hasOne(App::class, 'idapp', 'idapp');
    }


    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_contents','idchannel','idcontent');
    }


    public function site(){
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }

    public function player(){
        return $this->hasMany(Player::class, 'idchannel', 'idchannel');
    }

    public function isValid(){
        return $this->site()->first()->isValid();
    }
}
