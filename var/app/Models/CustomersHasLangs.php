<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomersHasLangs extends Model
{

    protected $table = "customers_has_langs";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer', 'idlang'
    ];

    public function lang()
    {
        return $this->hasMany(Lang::class, 'idlang', 'idlang');
    }
}
