<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $primaryKey = 'idprovince';
    public $timestamps = false;
    protected $table = "provincies";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    public function cities(){
        return $this->hasMany(City::class, 'idprovince', 'idprovince');
    }

    public function countries(){
        return $this->belongsTo(Country::class,'idcountry');
    }

    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_provincies','idprovince','idcircuit');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_provincies','idprovince','idcontent');
    }
}
