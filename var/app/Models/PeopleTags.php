<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeopleTags extends Model
{
    protected $primaryKey = 'id';
    protected $table = "people_has_tags";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idpeople' , 'idtag'
    ];



    public function tags(){
        return $this->hasMany(Tag::class, 'idtag', 'idtag')->with('tagCategory');
    }

    public function peoples() {
        return $this->hasMany(People::class, 'id', 'idpeople');
    }

   



    

}
