<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    protected $primaryKey = 'idcustomer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone','code','image_url', 'has_sales', 'has_influxes','has_emissions','has_trends',
         'status', 'deleted', 'influxes_type', 'category_level', 'streaming', 'has_audioasync', 'max_file_size'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'laravel_through_key'
    ];

    public function users(){
        return $this->hasMany(User::class, 'idcustomer', 'idcustomer');
    }

    public function sites(){
        return $this->hasMany(Site::class, 'idcustomer', 'idcustomer');
    }

    public function apps(){
        return $this->hasMany(App::class, 'idapp', 'idapp');
    }

    public function tagCategories(){
        return $this->hasMany(TagCategory::class, 'idcategory', 'idcategory');
    }

    public function location(){
        return $this->hasMany(IndoorLocation::class, 'idcustomer', 'idcustomer');
    }

    public function playCircuits(){
        return $this->hasMany(PlayCircuit::class, 'idcustomer', 'idcustomer');
    }

    public function contentCategories(){
        return $this->hasMany(ContentCategory::class, 'idcustomer', 'idcustomer');
    }

    public function contents(){
        return $this->hasMany(Content::class, 'idcustomer');
    }

    public function playlogics(){
        return $this->hasMany(PlayLogic::class, 'idcustomer', 'idcustomer');
    }

    public function formats(){
        return $this->belongsToMany(Format::class,'customers_has_formats','idcustomer','idformat');
    }

    public function langs(){
        return $this->belongsToMany(Lang::class,'customers_has_langs','idcustomer','idlang');
    }

    public function config_customer(){
        return $this->hasOne(ConfigCustomer::class, 'idcustomer', 'idcustomer');
    }

    public function isValid(){
        $user = Auth::user();
        $result=true;

        if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }

    public function attachLang($lang){
        $data = array();
        $data['idcustomer'] = $this->idcustomer;
        $data['idlang'] = $lang->idlang;
        \DB::table('customers_has_langs')->insert($data);
    }

    public function attachConfigCustomer($data){
        $cc = new ConfigCustomer();
        $cc->fill($data);
        $this->config_customer()->save($cc);
    }

    public function updateConfigCustomer($data){
        $cc = ConfigCustomer::where('idcustomer', $this->idcustomer)->first();
        if(empty($cc))
            self::attachConfigCustomer($data);
        else{
            $cc->fill($data);
            $cc->save();
        }
    }
    
    public function getCustomers($idcustomer = null) {

        $query = Customer::where("deleted",0)->where('status',1);
          
        if($idcustomer != null)
                 $query->where([
                ['idcustomer', '=', $idcustomer],
                ['deleted', '=', 0],
                ['status', '=', 1]
            ]);
        // JCAM 4/1/2021
        $query->orderby('idcustomer','DESC');
  
        return $query->get();
        
    }

    public static function getCustomer(){
        $customers = Customer::where('customers.deleted', 0)
                        ->where('customers.status', 1)
                        ->select('customers.*')
                        ->orderBy('name', 'ASC')->get();
        return $customers;
    }
 
    public static function filterBySite(int $idsite){
       return self::filter(['sites.idsite' => $idsite]);
    }

    public static function filterByCustomer(int $idcustomer){
        return self::filter(['customers.idcustomer' => $idcustomer]);
    }

    public static function filterAll(){
        return self::filter();
    }

    private static function filter($filter = null){
        $requests = Customer::join('sites', 'customers.idcustomer', '=', 'sites.idcustomer')
                            ->where('customers.deleted', 0)
                            ->where('customers.status', 1)
                            ->where('sites.deleted', 0)
                            ->where('sites.status', 1);
        if (!empty($filter))                    
            foreach($filter as $key => $value)
                $requests->where($key, $value);
                        
        $customers = $requests  ->select('customers.*')
                                ->orderBy('name', 'ASC')
                                ->get();
        return $customers;
    }
    
    public static function customerDelete(array $idcustomers) {
        $query = join(',' , array_fill(0, count($idcustomers), '?'));  
        return DB::select('UPDATE customers SET deleted = 1 WHERE idcustomer IN ('."$query".')', $idcustomers);
     }
 
}
