<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $primaryKey = 'idcity';
    protected $table = "cities";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    public function playcircuits(){
        return $this->belongsToMany(PlayCircuit::class,'circuits_has_cities','idcity','idcircuit');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_cities','idcity','idcontent');
    }

    public function provinces(){
        return $this->belongsTo(Province::class,'idprovince');
    }

    public static function findByNames($city_name, $province_name, $country_name) {
        $model = new City();

        $sfields = \DB::raw('cities.idcity, cities.name, provincies.idprovince, provincies.name as province_name, countries.idcountry, countries.name as country_name');
        $query = \DB::table('cities')
            ->join("provincies", "provincies.idprovince", "=", "cities.idprovince")
            ->join("countries", "countries.idcountry", "=", "provincies.idcountry")
            ->where("cities.name", 'like', $city_name)
            ->where("provincies.name", 'like', $province_name)
            ->where("countries.name", 'like', $country_name)
            ->select($sfields);

        return $query->first();
    }

    public static function findByCityName($city_name) {
        $model = new City();

        $sfields = \DB::raw('idcity, name');
        $query = \DB::table($model->table)
            ->where("name", 'like', $city_name)
            ->select($sfields);

        return $query->first();
    }

    public static function findProvinceByNames($province_name, $country_name) {
        $sfields = \DB::raw('provincies.idprovince, provincies.name as province_name, countries.idcountry, countries.name as country_name');
        $query = \DB::table("provincies")
            ->join("countries", "countries.idcountry", "=", "provincies.idcountry")
            ->where("provincies.name", 'like', $province_name)
            ->where("countries.name", 'like', $country_name)
            ->select($sfields);

        return $query->first();
    }
   
}
