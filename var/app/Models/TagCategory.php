<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TagCategory extends Model
{
    protected $primaryKey = 'idcategory';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer','name', 'tagtype', 'color'
    ];

    public function tags(){
        return $this->hasMany(Tag::class, 'idcategory');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

    public static function getTagCategoriesByCustomer($idcustomer){
        $tagcategories = TagCategory::with('tags')->where('idcustomer', $idcustomer)->get();
        return $tagcategories;
    }

    public static function getSuperAdmin($withtags=null){
        $tagcategories = new TagCategory();
         
        if(!is_null($withtags)){
            $tagcategories = $tagcategories->with('tags');
        }
 
        return $tagcategories->get();
    }

    public static function getAdmin($user,$withtags=null){
        $tagcategories = new TagCategory();
              
        if(!is_null($withtags)){
            $tagcategories = $tagcategories->with('tags');
        }

        $tagcategories = $tagcategories->where('idcustomer', $user->idcustomer);

        return $tagcategories->get();
    }

    public static function getDetail($id){
        return TagCategory::with('tags')->where('idcategory', $id)->first();
    }

    public function isValid(){
        $user = Auth::user();
        $result=true;

        if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }
}
