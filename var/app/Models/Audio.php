<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Audio extends Model
{
    protected $table = 'audios';

    protected $primaryKey = 'idaudio';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcategory', 'isrc', 'name', 'duration', 'audio_url', 'idcustomer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    

    public function category(){
        return $this->hasOne(AudioCategory::class, 'idcategory', 'idcategory');
    }

    public static function filtertByCustomer($idCustomer){
        $audios = Audio::select('audios.*')
                        ->join('audio_categories', 'audio_categories.idcategory', '=', 'audios.idcategory')
                        ->where('audio_categories.idcustomer' , $idCustomer);    
        return $audios->get();
    }

    public static function getAudios($user){
        $audios = AudioCategory::where('idcategory', $user->idcategory);
        $audios = $audios->get();
        return $audios;
    }


    public static function getAudio($id){
        $audios = AudioCategory::where('idaudio', $id);
        $audios = $audios->get();
        return $audios;
    }



   

}
