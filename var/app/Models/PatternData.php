<?php

namespace App\Models;

class PatternData
{
    private $cols = ["idcategory","weekday","hour","value"];
    public $pd;

    public function __construct($idcustomer,$month){
        $csv = file('http://82.223.69.121:5000/data/' . $idcustomer . '/pattern-data-' . $month . '.csv');
        $separator = ';';
        foreach ($csv as $line_index => $line) {
            if($line_index == 0) {
                $values = explode(';', $line);
                if(count($values) < 2){
                    $separator = ',';
                }
            } else {
                $newLine = [];
                $values = explode($separator, $line);
                foreach ($values as $col_index => $value) {
                    if(isset($this->cols[$col_index])){
                        if($col_index == 2){
                            $newLine[$this->cols[$col_index]] = intval(trim($value));
                        }else if($col_index == 3){
                            $newLine[$this->cols[$col_index]] = floatval(trim($value));
                        }else{
                            $newLine[$this->cols[$col_index]] = trim($value);
                        }
                    }else{
                        dd($line, $col_index);
                    }
                }
                $output[] = $newLine;
            }
        }
        $this->pd = collect($output);    
    }
}
