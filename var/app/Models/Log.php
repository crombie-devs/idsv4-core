<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "logs";

    protected $primaryKey = 'idlog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iduser', 
        'identity', 
        'entity', 
        'action', 
        'ip',
        'datetime',
        'status',
        'path',
        'params',
        'response'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function user(){
        return $this->hasOne(User::class, 'iduser', 'iduser');
    }

}
