<?php

namespace App\Models;

class Association 
{
    public $associations;
    private $cols = ["tipo","idcategoryrow","idcategorycol","valor", 'nivelagregacion'];

    public function __construct($idcustomer){
        $csv = file('http://82.223.69.121:5000/data/' . $idcustomer . '/associations.csv');
        $separator = ';';
        $output = [];
        foreach ($csv as $line_index => $line) {
            if($line_index == 0) {
                $values = explode(';', $line);
                if(count($values) < 2){
                    $separator = ',';
                }
            } else {
                $newLine = [];
                $values = explode($separator, $line);
                foreach ($values as $col_index => $value) {
                    if($col_index == 3){
                        $newLine[$this->cols[$col_index]] = floatval($value);
                    }else{
                        $newLine[$this->cols[$col_index]] = trim($value);
                    }
                }
                $output[] = $newLine;
            }
        }
        $this->associations = collect($output);    
    }
}
