<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MsgRelation extends Model
{

    protected $table = "msg_relations";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idmessage', 'idcontent', 'type'
    ];
}
