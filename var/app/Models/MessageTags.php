<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageTags extends Model
{
    
    protected $table = "messages_has_tags";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idmessage' , 'idtag'
    ];

    
    public function tags(){
        return $this->hasMany(Tag::class, 'idtag', 'idtag')->with('tagCategory');
    }

}