<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PlayArea extends Model
{
    protected $primaryKey = 'idarea';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idlocation', 'idformat'
    ];
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'name'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'created_at', 'updated_at', 'idlocation', 'idformat'
        'created_at', 'updated_at', 'idlocation', 'pivot'
    ];

    public function location(){
        return $this->hasOne(IndoorLocation::class, 'idlocation', 'idlocation');
    }

    public function format(){
        return $this->hasOne(Format::class, 'idformat', 'idformat');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_areas','idarea','idcontent');
    }

    public function playlogics(){
        return $this->hasMany(PlayLogic::class, 'idarea', 'idarea');
    }

    public function player(){
        return $this->hasMany(Player::class, 'idarea', 'idarea');
    }

    public function contentsorder(){
        return $this->hasMany(ContentOrder::class, 'idarea', 'idarea');
    }


    public static function getSuperAdmin(){
        $playareas = PlayArea::with('format')->with('location')->get();
        return $playareas;
    }

    public static function getAdmin($user){
        $locations = IndoorLocation::where('idcustomer',$user->idcustomer);

        if($locations){
            $playareas = PlayArea::with(['format','location'])->whereIn('idlocation',$locations->pluck('idlocation'))->get();
            return $playareas;
        }
    }

    public static function getUser($user){
        $players = Player::where('idsite',$user->idsite);

        if($players){
            $playareas = PlayArea::with(['format','location'])->whereIn('idarea',$players->pluck('idarea'))->get();
            return $playareas;
        }
    }


    public static function getPlayAreasPlayerSuperAdmin(){
        $playareas = PlayArea::with('player')->get();
        return $playareas;
    }

    public static function getPlayAreasPlayerAdmin($user){
        $sites = Site::where('idcustomer',$user->idcustomer)->get();
        if($sites->isNotEmpty()){
            $playareas = PlayArea::with(['player' => function($query) use ($sites) {
                        $query->whereIn('idsite',$sites->pluck('idsite')); 
                    }
            ])->get();
        }
        return $playareas;
    }
    
    public static function getPlayAreasPlayerUser($user){
        $playareas = PlayArea::with(['player' => function($query) use ($user) {
                        $query->where('idsite',$user->idsite); 
                    }
            ])->get();

        return $playareas;
    }

    public function isValid(){
        return $this->location()->first()->isValid();
    }

    public static function toCreatePlaylist($circuits, $user){
        $arraySites = [];
        
     
        $circuits = PlayCircuit::whereIn('idcircuit',$circuits)->get();  
        foreach ($circuits as $circuit) {
            foreach ($circuit->sites as $site) {
                $arraySites[] = $site->idsite;
            }
        }        
        

        if (count($arraySites) == 0) {
            if($user->is_superadmin())
                $playareas = PlayArea::getSuperAdmin();            
            else 
                $playareas = PlayArea::getAdmin($user); 
        }else
            $playareas = PlayArea::join("players", "players.idarea", "=", "play_areas.idarea")
            ->join("sites","players.idsite", "=", "sites.idsite")
            ->whereIn("sites.idsite", $arraySites)->with('format')->with('location')->groupby('play_areas.idarea')->distinct()->get();
        
      

        return $playareas;


    }
}
