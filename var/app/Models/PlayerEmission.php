<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerEmission extends Model
{
    protected $primaryKey = null;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idplayer','weekday', 'num_loop', 'total_emissions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
