<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class IndoorLocation extends Model
{
    protected $primaryKey = 'idlocation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function isValid(){
        $result = true;
        $user = Auth::user();
       
        if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }
       
       return $result;   
    }

    public static function getDetail($id){
        $indoorlocation = IndoorLocation::where('idlocation', $id)->first();
        return $indoorlocation;
    }

    public static function deleteIndoorLocationForce($idlocation){
        $user = Auth::user();
        $indoorlocation = IndoorLocation::find($idlocation);
        $playarea = PlayArea::where('idlocation',$idlocation)->get();

        $idarea = $playarea->pluck('idarea');
           
        if(!empty($idarea)){
            foreach ($playarea as $key => $plaa) {
                foreach($plaa->contents as $content){
                    $plaa->contents()->detach($content->idcontent);
                }
            }
           
            ContentOrder::whereIn('idarea',$idarea)->delete();
           
            if($user->is_superadmin()){
                PlayLogic::whereIn('idarea',$idarea)->delete();
            }
            else if($user->is_admin()){
                PlayLogic::whereIn('idarea',$idarea)->where('idcustomer',$user->idcustomer)->delete();
            }
            else if($user->is_user()){
                PlayLogic::whereIn('idarea',$idarea)->where('idcustomer',$user->idcustomer)->delete();
            }

            $player = Player::whereIn('idarea',$idarea);
        
            $player->update(["deleted"=>true]);

            foreach ($idarea as $key => $ida) {
                $playarea = PlayArea::find($ida);
                $playarea->delete();
            }

            $indoorlocation->delete();
            return true;
        }

        return false;
    }
}
