<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Asset extends Model
{
    protected $primaryKey = 'idasset';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer', 
        'idsite', 
        'idtype', 
        'name', 
        'asset_url', 
        'cover_url', 
        'duration', 
        'idformat', 
        'size', 
        'idlang', 
        'default', 
        'has_audio', 
        'status', 
        'deleted',
        'is_widget', 
        'idlandingpage',
        'expired_date',
        'delay'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at', 'idsite', 'status', 'deleted', 'pivot'
    ];

    public function type(){
        return $this->hasOne(AssetType::class, 'idtype', 'idtype');
    }

    public function lang(){
        return $this->hasOne(Lang::class, 'idlang', 'idlang');
    }

    public function format(){
        return $this->hasOne(Format::class, 'idformat', 'idformat');
    }

    public function contents(){
        return $this->belongsToMany(Content::class, 'contents_has_assets','idasset','idcontent');
    }

    public static function getSuperAdmin($status=null){
        $assets = Asset::with('type')
                        ->with('lang')
                        ->with('format')
                        ->with('contents')
                        ->where('deleted', 0);    
        if(!is_null($status)){
            $assets = $assets->where('status',1);
        }

        return $assets->orderBy('updated_at', 'DESC')->get();
    }


    public static function getAdmin($user,$status=null){
        $assets = Asset::with('type')
                        ->with('lang')
                        ->with('format')
                        ->with('contents')
                        ->where('idcustomer', $user->idcustomer)
                        ->where('deleted', 0);
                        
        if(!is_null($status)){
            $assets = $assets->where('status',1);
                    
        }

        return $assets->orderBy('updated_at', 'DESC')->get();
    }

    public static function getUser($user,$status=null){
        $assets = Asset::with('type')
                        ->with('lang')
                        ->with('format')
                        ->with('contents')
                        ->where('idcustomer', $user->idcustomer)
                        ->where('idsite', $user->idsite)
                        ->where('deleted', 0);

        if(!is_null($status)){
            $assets = $assets->where('status',1);
        }
                        
        return $assets->orderBy('updated_at', 'DESC')->get();
    }


    public static function getDetail($id){
        $asset = Asset::with('type')
        ->with('lang')
        ->with('format')
        ->where('idasset', $id)
        ->where('deleted', 0);
        return $asset->first();
    }

    public function isValid(){
        $user = Auth::user();
        $result=true;

        if(!is_null($user->idsite)){
            $result = ($user->idsite == $this->idsite);
        }
        else if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }
}
