<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Difendif extends Model
{
    protected $table = "difendif_python_impacto";
    protected $primaryKey = null;
    protected $incrementing = false;
    protected $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer',
        'idcampaign',
        'name',
        'ejercicio',
        'mes',
        'diff_co_pEm_r',
        'diff_co_preEm_r',
        'diff_em_pEm_r',
        'diff_em_preEm_r',
        'diff_eur',
        'diff_inc_uni',
        'diff_var_uni_em',
        'diff_var_uni_pEm',
        'diff_var_uni_preEm',
        'diff_var_uni',
        'impactoeuros',
        'pesocamp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
