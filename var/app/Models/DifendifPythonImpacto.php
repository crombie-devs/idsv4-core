<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class DifendifPythonImpacto extends Model
{
    protected $table = 'difendif_python_impacto';

    protected $fillable = [
        'idcustomer','diff_co_pEm_r','diff_co_preEm_r','diff_em_pEm_r','diff_em_preEm_r','diff_eur','diff_inc_uni',
        'diff_var_uni_em','diff_var_uni_pEm','diff_var_uni_preEm','diif_var_uni','idcampaign','name',
        'status','pesocamp','impactoeuros','ejercicio','mes'
    ];
   
    public $timestamps = false;
}
