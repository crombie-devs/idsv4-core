<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageFormats extends Model
{
    protected $primaryKey = 'id';
    protected $table = "message_formats";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id' , 'name'
    ];

    

}