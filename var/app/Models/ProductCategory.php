<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProductCategory extends Model
{
    protected $primaryKey = 'idcategory';
    protected $table = "product_categories";
    public $timestamps = false;


    protected $fillable = ['idcategory', 'idcustomer', 'code', 'name'];
    protected $hidden = [];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

    public function parent() {
        return $this->belongsTo(ProductCategory::class, 'idparent');
    }

    public function products(){
        $user = Auth::user();
        $relation =  $this->hasMany(Product::class, 'idcategory', 'idcategory');
        $relation->getQuery()
            ->with('contents')
            ->where('idcustomer', $user->idcustomer);
        return $relation;
    }

    public function isValid(){
        $result = true;

        $user = Auth::user();
        if(!is_null($user->idcustomer))
            $result = ($user->idcustomer == $this->idcustomer);

        return $result;
    }
}
