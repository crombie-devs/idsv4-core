<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportHistorical extends Model
{
    protected $primaryKey = 'idsupporthistorical';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsupporthistorical', 'idcustomer', 'idsite', 'idplayer', 'idcamera', 'date_incident', 
        'socket', 'value'
    ];

    /**
     * The attributes that should be dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'date_incident'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function site(){
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }

    public function player(){
        return $this->belongsTo(Player::class, 'idplayer', 'idplayer');
    }

    public function camera(){
        return $this->belongsTo(Camera::class, 'idcamera', 'idcamera');
    }

    public function getSupportHistoricalByCustomerByPlayer($idcustomer, $idplayer) {

        $query = SupportHistorical::where("idcustomer",$idcustomer)->where('idplayer',$idplayer);
          
        return $query->first();
        
    }

    public function getSupportHistoricalByCustomerByCamera($idcustomer, $idcamera) {

        $query = SupportHistorical::where("idcustomer",$idcustomer)->where('idcamera',$idcamera);
          
        return $query->first();
        
    }

    public function insertGetId($data) {

        $id = SupportHistorical::insert([$data]); 
          
        return $id;
        
    }
    
}
