<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class SupportPlayer extends Model {
    protected $primaryKey = 'idsupportplayer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsupportplayer', 'idcustomer', 'idsite', 'idplayer', 'last_test_hour', 
        'internet', 'socket', 'emission', 'sendmail'
    ];
    /**
     * The attributes that should be dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'last_test_hour'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer', 'idcustomer');
    }
    public function site(){
        return $this->belongsTo(Site::class, 'idsite', 'idsite');
    }
    public function player(){
        return $this->belongsTo(Player::class, 'idplayer', 'idplayer');
    }
    public function getSupportPlayerByCustomerByPlayer($idcustomer, $idplayer) {
        $query = SupportPlayer::where("idcustomer",$idcustomer)->where('idplayer',$idplayer);
        return $query->first();   
    }
    public function insertGetId($data) {
        $id = SupportPlayer::insert([$data]); 
        return $id;   
    }
    public function updateP($idplayer,$data) {
        $id = SupportPlayer::where('idplayer', $idplayer)
                            ->update($data);
        return $id;
    }
}
