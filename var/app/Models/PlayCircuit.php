<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PlayCircuit extends Model
{
    protected $primaryKey = 'idcircuit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer', 'name', 'image_url', 'status','deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'idcustomer');
    }

    public function countries(){
        return $this->belongsToMany(Country::class,'circuits_has_countries','idcircuit','idcountry');
    }

    public function provinces(){
        return $this->belongsToMany(Province::class,'circuits_has_provincies','idcircuit','idprovince');
    }

    public function cities(){
        return $this->belongsToMany(City::class,'circuits_has_cities','idcircuit','idcity');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'circuits_has_tags','idcircuit','idtag');
    }

    public function sites(){
        return $this->belongsToMany(Site::class,'circuits_has_sites','idcircuit','idsite');
    }

    public function langs(){
        return $this->belongsToMany(Lang::class,'circuits_has_langs','idcircuit','idlang');
    }

    public function contents(){
        return $this->belongsToMany(Content::class,'contents_has_circuits','idcircuit','idcontent');
    }

    public function isValid(){
        $user = Auth::user();
        $result = true;
       
        if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }

    public static function getCircuitsByCustomer($idcustomer){
        
            $circuits = PlayCircuit::with('tags')
                                ->with('langs')
                                ->with('sites')
                                ->with('countries')
                                ->with('provinces')
                                ->with('cities');
                                
        if(!empty($idcustomer))
            return $circuits->where('idcustomer', $idcustomer)->get();
        else                                  
           return $circuits->get();    
    }

    public static function getCircuits(){        
        $circuits = PlayCircuit::with('tags')
                            ->with('langs')
                            ->with('sites')
                            ->with('countries')
                            ->with('provinces')
                            ->with('cities');   
        return $circuits->get();    
    }

}
