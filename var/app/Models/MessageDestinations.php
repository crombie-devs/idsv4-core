<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageDestinations extends Model
{
    protected $primaryKey = 'id';
    protected $table = "message_destinations";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'datetime', 'email', 'phone', 'idmessage'
    ];
}
