<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigCustomer extends Model
{
    protected $primaryKey = 'id';
    protected $table = "config_customer";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sms_apikey' , 'mailing_apikey', 'whatsapp_apikey', 'has_external_cms', 'has_algorithms'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'idcustomer'
    ];
}
