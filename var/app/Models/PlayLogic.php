<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class PlayLogic extends Model
{
    protected $primaryKey = 'idlogic';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idcustomer','idarea','idtype','idcategory','order','idplayer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 
    ];

    public function customer(){
        return $this->hasOne(Customer::class, 'idcustomer', 'idcustomer');
    }

    public function area(){
        return $this->hasOne(PlayArea::class, 'idarea', 'idarea');
    }

    public function player(){
        return $this->hasOne(PlayArea::class, 'idplayer', 'idplayer');
    }

    public function type(){
        return $this->hasOne(ContentType::class, 'idtype', 'idtype');
    }
    public function contentCategory(){
        return $this->belongsTo(ContentCategory::class,'idcategory');
    }

    public static function getplaylist($idcustomer, $idarea){
        $playlist = PlayLogic::with('customer')->with('area')->with('type')->where('idcustomer',$idcustomer)->where('idarea',$idarea)->first();
        return $playlist;
    }

    public static function getSuperAdmin(){
        $playlogic = PlayLogic::with('customer')
        ->with('area')
        ->with('type')
        ->with('contentCategory')
        ->orderBy('order', 'asc')->get();

        return $playlogic;
    }

    public static function getAdmin($user){
        $playlogic = PlayLogic::with('customer')
        ->with('area')
        ->with('type')
        ->with('contentCategory')
        ->where('idcustomer',$user->idcustomer)
        ->orderBy('order', 'asc')->get();

        return $playlogic;
    }

    public static function getDetail($id){
        $playlogic = PlayLogic::with('customer')
            ->with('area')
            ->with('type')
            ->with('contentCategory')
            ->where('idlogic',$id)
            ->first();

        return $playlogic;
    }

    public function isValid(){
        $user = Auth::user();
        $result=true;

        if(!is_null($user->idcustomer)){
            $result = ($user->idcustomer == $this->idcustomer);
        }

        return $result;
    }
}
