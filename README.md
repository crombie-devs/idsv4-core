1º Construir imagen y arrancar server laravel 
docker-compose up --build


2º Instalar vendors
docker-compose exec app_api composer update

3º Acceder a phpmyadmin y importar la bd

4º docker-compose exec  app_api php artisan passport:install



Arrancar servidor laravel
docker-compose up

Parar servidor laravel
docker-compose down

Eliminar todos los contenedores
docker rm -f $(docker ps -a -q)

Ejecutar un comando dentro del contenedor

docker-compose exec <nombre contenedor> <comando a ejecutar>


PHPMYADMIN

http://localhost:8600
user: root
pass: 12345



Crear ssh_private_key gitlab
Settings > CI/CD.
https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/



TERRAFOM
terraform init
terraform plan
terraform apply


SERVER
install docker https://docs.docker.com/engine/install/ubuntu/
install docker-compose https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-es




